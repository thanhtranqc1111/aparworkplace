<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.grantSubsidiary.title' /></title>

</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="../header.jsp"%>
			<input type="hidden" value="subsidiary" id="pageName"/>
			<input type="hidden" id="isConfirmChanged" value="true"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="">
										<a href="${pageContext.request.contextPath}/subsidiary/list" role="tab"><spring:message code='cashflow.subsidiary.header' /></a>
									</li>
									<li role="presentation" class="active">
										<a href="#" role="tab"><spring:message code='cashflow.grantSubsidiary.header' /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_content">
								<table id="multi-table" class="table table-bordered">
									<thead>
										<tr>
											<td></td>
											<th colspan="${fn:length(subsidiarys)}"><spring:message code='cashflow.grantSubsidiary.subsidiary' /></th>
										</tr>
										<tr>
											<th><spring:message code='cashflow.grantSubsidiary.useLogin' /></th>
											<c:forEach items="${subsidiarys}" var="lstSubsidiarys">
												<th>${lstSubsidiarys.name}</th>
											</c:forEach>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${userAccounts}" var="lstUserAccounts">
											<tr>
												<td style="">${lstUserAccounts.email}</td>
												<c:forEach items="${subsidiarys}" var="lstSubsidiarys">
													<c:set var="key">${lstSubsidiarys.id},${lstUserAccounts.id}</c:set>
													<c:set var="value">${subsidiaryUserMap[key]}</c:set>
													<td><c:choose>
															<c:when test="${value > 0}">
																<input subsidiaryId="${lstSubsidiarys.id}"
																	userAccountId="${lstUserAccounts.id}" type="checkbox"
																	name="subsidiary-user" checked>
															</c:when>
															<c:otherwise>
																<input subsidiaryId="${lstSubsidiarys.id}"
																	userAccountId="${lstUserAccounts.id}" type="checkbox"
																	name="subsidiary-user">
															</c:otherwise>
														</c:choose></td>
												</c:forEach>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								
								<div class="row pull-right">
									<div class="col-xs-12">
											<button type="button" class="btn btn-primary btn-flat btn-grant-sub pull-right"><spring:message code='cashflow.grantSubsidiary.reset' /></button>
											<button type="button" class="btn btn-primary btn-flat button-margin btn-save-all disabled">
												<i class="fa-save fa button-margin"></i><spring:message code='cashflow.grantSubsidiary.save' />
											</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceCSS("subsidiary.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<% out.println(getReferenceJS("grant-sub.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		grantSub.init();
		grantSub.eventListener();
	});
</script>
</html>