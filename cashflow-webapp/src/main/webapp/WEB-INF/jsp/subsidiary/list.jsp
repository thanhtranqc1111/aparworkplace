<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css"	rel='stylesheet'></link>
<title><spring:message code='cashflow.subsidiary.title' /></title>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="subsidiary" id="pageName"/>
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="active">
										<a href="#" role="tab"><spring:message code='cashflow.subsidiary.header' /></a>
									</li>
									<li role="presentation" class="">
										<a href="${pageContext.request.contextPath}/subsidiary/grantSub" role="tab"><spring:message code='cashflow.grantSubsidiary.header' /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="row">
								<div class="col-xs-6">
									<div class="col-xs-2">
										<h5 class="pull-right"><spring:message code='cashflow.subsidiary.nameOrCode' /></h5>
									</div>
									<div class="col-xs-4">
										<input type="text" class="form-control" id="txt-search" name="search" />
									</div>
									<div class="col-xs-2">
										<button type="button"
											class="btn btn-primary btn-flat btn-search pull-left">
											<i class="fa fa-search button-margin"></i><spring:message code='cashflow.subsidiary.search' />
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_content">
								<div class="row ap-search">
									<table id="sub-table"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><spring:message code='cashflow.subsidiary.code' /></th>
												<th><spring:message code='cashflow.subsidiary.name' /></th>
												<th><spring:message code='cashflow.subsidiary.address' /></th>
												<th><spring:message code='cashflow.subsidiary.status' /></th>
											</tr>
										</thead>
									</table>
								</div>
								<div class="row pull-right">
									<div class="col-xs-12">
										<div class="row">
											<a href="detail" class="btn btn-success  btn-save"><i class="fa fa-plus-square button-margin"></i><spring:message code='cashflow.subsidiary.newSubsidiary' /></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceCSS("subsidiary.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<% out.println(getReferenceJS("subsidiary-list.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		<c:if test="${not empty message}">
		 	showPopupMessage("${message}", function() {});
		</c:if>
		subList.init();
		subList.eventListener();
	});
</script>
</html>