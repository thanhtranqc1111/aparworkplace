<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<link href="${pageContext.request.contextPath}/resources/min/css/subsidiary.min.css" rel="stylesheet" />
<title><spring:message code='cashflow.subsidiaryDetail.title' /></title>

</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="subsidiary" id="pageName"/>
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="page-title">
					<div class="title_left">
						<h3><spring:message code='cashflow.subsidiaryDetail.header' /></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="error-area"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_content">
								<div class="form-horizontal">
									<fieldset>
										<legend><spring:message code='cashflow.subsidiaryDetail.header' /></legend>
									</fieldset>
									<input type="hidden" id="isConfirmChanged" value="true"/>
									<form:form id="subsidiaryForm" action="${pageContext.request.contextPath}/subsidiary/save" modelAttribute="subsidiaryForm" method="POST">
										<form:hidden path="id" id="subid"/>
										
										<div class="col-xs-12">
											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.enabled' /></label>
												<div class="col-xs-1">
													<form:radiobutton path="isActive" value="true" id="enabled"
														checked="checked" />
													<label for="enabled"> <spring:message code='cashflow.subsidiaryDetail.yes' /></label>
												</div>
												<div class="col-xs-1">
													<form:radiobutton path="isActive" value="false"
														id="disabled" />
													<label for="disabled"> <spring:message code='cashflow.subsidiaryDetail.no' /></label>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label code"> <spring:message code='cashflow.subsidiaryDetail.code' /> *</label>
												<div class="col-xs-3">
													<c:choose>
														<c:when test="${empty subsidiaryForm.id}">
												            <form:input path="code" class="form-control" name="code" id="code" />
												         </c:when>
														<c:otherwise>
															<form:input path="code" type="hidden" class="form-control" name="code" id="code" />
												           <span class="sub-code">${subsidiaryForm.code }</span>
												         </c:otherwise>
													</c:choose>
												</div>
											</div>

											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.currency' /> *</label>
												<div class="col-xs-3">
													<c:choose>
														<c:when test="${empty subsidiaryForm.id}">
												            <select id="currency" name="currency" class="form-control">
																<option value="">-- <spring:message code='cashflow.subsidiaryDetail.pleaseSelect' /> --</option>
																<option value="JPY">JPY</option>
																<option value="VND">VND</option>
																<option value="USD">USD</option>
																<option value="EUR">EUR</option>
																<option value="GBP">GBP</option>
																<option value="DZD">DZD</option>
																<option value="ARP">ARP</option>
																<option value="AUD">AUD</option>
																<option value="ATS">ATS</option>
																<option value="BSD">BSD</option>
																<option value="BBD">BBD</option>
																<option value="BEF">BEF</option>
																<option value="BMD">BMD</option>
																<option value="BRR">BRR</option>
																<option value="BGL">BGL</option>
																<option value="CAD">CAD</option>
																<option value="CLP">CLP</option>
																<option value="CNY">CNY</option>
																<option value="CYP">CYP</option>
																<option value="CSK">CSK</option>
																<option value="DKK">DKK</option>
																<option value="NLG">NLG</option>
																<option value="XCD">XCD</option>
																<option value="EGP">EGP</option>
																<option value="FJD">FJD</option>
																<option value="FIM">FIM</option>
																<option value="FRF">FRF</option>
																<option value="DEM">DEM</option>
																<option value="XAU">XAU</option>
																<option value="GRD">GRD</option>
																<option value="HKD">HKD</option>
																<option value="HUF">HUF</option>
																<option value="ISK">ISK</option>
																<option value="INR">INR</option>
																<option value="IDR">IDR</option>
																<option value="IEP">IEP</option>
																<option value="ILS">ILS</option>
																<option value="ITL">ITL</option>
																<option value="JMD">JMD</option>
																<option value="JOD">JOD</option>
																<option value="KRW">KRW</option>
																<option value="LBP">LBP</option>
																<option value="LUF">LUF</option>
																<option value="MYR">MYR</option>
																<option value="MXP">MXP</option>
																<option value="NLG">NLG</option>
																<option value="NZD">NZD</option>
																<option value="NOK">NOK</option>
																<option value="PKR">PKR</option>
																<option value="XPD">XPD</option>
																<option value="PHP">PHP</option>
																<option value="XPT">XPT</option>
																<option value="PLZ">PLZ</option>
																<option value="PTE">PTE</option>
																<option value="ROL">ROL</option>
																<option value="RUR">RUR</option>
																<option value="SAR">SAR</option>
																<option value="XAG">XAG</option>
																<option value="SGD">SGD</option>
																<option value="SKK">SKK</option>
																<option value="ZAR">ZAR</option>
																<option value="KRW">KRW</option>
																<option value="ESP">ESP</option>
																<option value="XDR">XDR</option>
																<option value="SDD">SDD</option>
																<option value="SEK">SEK</option>
																<option value="CHF">CHF</option>
																<option value="TWD">TWD</option>
																<option value="THB">THB</option>
																<option value="TTD">TTD</option>
																<option value="TRL">TRL</option>
																<option value="VEB">VEB</option>
																<option value="ZMK">ZMK</option>
																<option value="EUR">EUR</option>
																<option value="XCD">XCD</option>
																<option value="XDR">XDR</option>
																<option value="XAG">XAG</option>
																<option value="XAU">XAU</option>
																<option value="XPD">XPD</option>
																<option value="XPT">XPT</option>
															</select>
												         </c:when>
														<c:otherwise>
															<form:input path="currency" type="hidden" class="form-control" name="currency" id="currency" />
												           	<span class="sub-code">${subsidiaryForm.currency }</span>
												         </c:otherwise>
													</c:choose>
												</div>
											</div>

											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.name' /> *</label>
												<div class="col-xs-3">
													<form:input path="name" class="form-control" name="name"
														id="name" />
												</div>
											</div>
											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.address' /></label>
												<div class="col-xs-6">
													<form:input path="address" class="form-control"
														name="address" id="address" />
												</div>
											</div>
											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.website' /></label>
												<div class="col-xs-6">
													<form:input path="website" class="form-control"
														name="website" id="website" />
												</div>
											</div>
											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.phone' /></label>
												<div class="col-xs-3">
													<form:input path="phone" class="form-control" name="phone" id="phone" />
												</div>
											</div>

											<div class="form-group row">
												<div class="col-xs-1"></div>
												<label for="example-search-input"
													class="col-xs-2 col-form-label"><spring:message code='cashflow.subsidiaryDetail.description' /></label>
												<div class="col-xs-6">
													<form:textarea path="description" rows="7" cols="6"
														class="form-control" id="description" />
												</div>
											</div>
											<div class="ln_solid"></div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">
													<a href="${pageContext.request.contextPath}/subsidiary/list" class="btn btn-danger  btn-flat">
														<span class="glyphicon glyphicon-remove button-margin"></span><spring:message code='cashflow.subsidiaryDetail.cancel' />
													</a>
													<a href="#" onclick="subList.saveSubsidiary();" class="btn btn-primary btn-flat btn-apply">
														<i class="fa fa-save button-margin"></i><spring:message code='cashflow.subsidiaryDetail.save' />
													</a>
													<button type="reset" value="<spring:message code='cashflow.subsidiaryDetail.reset' />" class="btn btn-primary btn-flat btn-reset-sub"><spring:message code='cashflow.subsidiaryDetail.reset' /></button>
												</div>
											</div>
										</div>
									</form:form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
 <% out.println(getReferenceCSS("subsidiary.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/moment/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<% out.println(getReferenceJS("subsidiary-list.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		<c:if test="${not empty message}">
		 	showPopupMessage("${message}", function() {});
		</c:if>
		subList.eventListener();
	});
	
</script>
</html>