<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap-processbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" />


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:import var="staticBundles" url="/minify/static-bundles.json"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%
	// get profile
	ServletContext sc = request.getSession().getServletContext();
	WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
	String[] profiles = applicationContext.getEnvironment().getActiveProfiles();
	profile = (profiles.length > 0) ? profiles[0] : PROFILE_DEV;
	context = (String)pageContext.getAttribute("context");
	
	//get JsonArray
	if(sc.getAttribute("staticBundles") == null){
		String json = (String)pageContext.getAttribute("staticBundles");
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(json).getAsJsonObject();
		this.bundles = jsonObject.getAsJsonArray("bundles");
		sc.setAttribute("staticBundles", this.bundles);
	} else {
		this.bundles = (JsonArray)sc.getAttribute("staticBundles");
	} 
%>

<%!
	private static final String PROFILE_DEV = "dev";
	private String profile;
    private JsonArray bundles;
    private String context;
    
	public String getReferenceCSS(String name){
		for(int i = 0; i < bundles.size(); i++)
		{
			JsonObject bundleObject = bundles.get(i).getAsJsonObject();
			
			if(bundleObject.get("name").getAsString().equals(name))
			{
				if(PROFILE_DEV.equals(profile)){
					StringBuilder stringBuilder = new StringBuilder("");
					for(int j = 0; j < bundleObject.getAsJsonArray("files").size(); j++){
						String fileName = bundleObject.getAsJsonArray("files").get(j).getAsString();
						stringBuilder.append("<link href='"+context+"/resources/css/" + fileName +"' rel='stylesheet' />");
					}
					return stringBuilder.toString();
				}
				else {
					String[] tokens = bundleObject.get("name").getAsString().split("\\.");
					if(tokens.length == 2)
					{
						return "<link href='"+context+"/resources/min/css/" + tokens[0] + ".min." + tokens[1] +"' rel='stylesheet' />";
					}
				}
			}
		}
		return "";
	}

	public String getReferenceJS(String name){
		for(int i = 0; i < bundles.size(); i++)
		{
			JsonObject bundleObject = bundles.get(i).getAsJsonObject();
			if(bundleObject.get("name").getAsString().equals(name))
			{
				if(PROFILE_DEV.equals(profile)){
					StringBuilder stringBuilder = new StringBuilder("");
					for(int j = 0; j < bundleObject.getAsJsonArray("files").size(); j++){
						String fileName = bundleObject.getAsJsonArray("files").get(j).getAsString();
						stringBuilder.append("<script src='"+context+"/resources/js/" + fileName +"' rel='stylesheet'></script>");
					}
					return stringBuilder.toString();
				}
				else {
					String[] tokens = bundleObject.get("name").getAsString().split("\\.");
					if(tokens.length == 2)
					{
						return "<script src='"+context+"/resources/min/js/" + tokens[0] + ".min." + tokens[1] + "'></script>";
					}
				}
			}
		}
		return "";
	}
%>


<div id="header">
	<div id="loading">
	  <img id="loading-image" src="../../${pageContext.request.contextPath}/resources/css/img/ajax-loader.gif" alt="Loading..." />
	</div>
	
	<div class="col-md-3 left_col">
		<div class="left_col scroll-view">
			<div class="navbar nav_title logo" style="border: 0;">
				<img onclick="window.location ='/'"  src="${pageContext.request.contextPath}/resources/css/img/logo_tci_header.png">
			</div>
			<div class="clearfix"></div>
		
			<!-- sidebar menu -->
			<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
				<div class="menu_section">
					<ul class="nav side-menu">
						<li><a href="${pageContext.request.contextPath}/">
							<span><img src="${pageContext.request.contextPath}/resources/css/img/home.svg" class="icon-menu icon-home" /></span>
							<!-- <i class="fa fa-home"></i> -->
						<span><spring:message code='cashflow.common.dashboard' /></span> </a>
						</li>
						<li><a>
							<span><img src="${pageContext.request.contextPath}/resources/css/img/check-square.svg" class="icon-menu icon-check-square" /></span>
							<!-- <i class="fa fa-check-square-o"></i> -->
							<span><spring:message code='cashflow.common.approval' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="purchase request"><a href="${pageContext.request.contextPath}/approvalPurchaseRequest/list"><spring:message code='cashflow.common.purchaseRequest' /></a></li>
								<li title="sales order request"><a href="${pageContext.request.contextPath}/approvalSalesOrderRequest/list"><spring:message code='cashflow.common.salesOrderRequest' /></a></li>
								<li title="approval process"><a href="${pageContext.request.contextPath}/approval/process">Approval Process</a></li>
							</ul>
						</li>
						<li><a>
						<span><img src="${pageContext.request.contextPath}/resources/css/img/credit-card.svg" class="icon-menu icon-credit-card" /></span>
						<!-- <i class="fa fa-credit-card-alt"></i> -->
						<span><spring:message code='cashflow.common.accountPayable' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="ap invoice"><a href="${pageContext.request.contextPath}/apinvoice/list"><spring:message code='cashflow.common.apInvoice' /></a></li>
								<li title="payroll"><a href="${pageContext.request.contextPath}/payroll/list"><spring:message code='cashflow.common.payroll' /></a></li>
								<li title="reimbursement"><a href="${pageContext.request.contextPath}/reimbursement/list"><spring:message code='cashflow.common.reimbursement' /></a></li>
								<li title="payment order"><a href="${pageContext.request.contextPath}/payment/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
							</ul>
						</li>
						<li><a>
						<span><img src="${pageContext.request.contextPath}/resources/css/img/get-pocket.svg" class="icon-menu" /></span>
						<!-- <i class="fa fa-get-pocket"></i> -->
						<span><spring:message code='cashflow.common.accountReceivable' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="ar invoice"><a href="${pageContext.request.contextPath}/arinvoice/list"><spring:message code='cashflow.common.arInvoice' /></a></li>
								<li title="receipt order"><a href="${pageContext.request.contextPath}/receiptOrder/list"><spring:message code='cashflow.common.receiptOrder' /></a></li>
							</ul>
						</li>
						<li><a>
						<span><img src="${pageContext.request.contextPath}/resources/css/img/bank.svg" class="icon-menu" /></span>
							<!-- <i class="fa fa-university"></i> -->
							<span><spring:message code='cashflow.common.bank' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="bank statement"><a href="${pageContext.request.contextPath}/bankStatement/list"><spring:message code='cashflow.common.bankStatement' /></a></li>
								<!-- <li><a href="${pageContext.request.contextPath}/bankStatement/list">Bank statement</a></li>
								<li><a href="${pageContext.request.contextPath}/bankStatement/list">Bank statement</a></li> -->
							</ul></li>
						<li><a>
						<span><img src="${pageContext.request.contextPath}/resources/css/img/project.svg" class="icon-menu" /></span>
							<!-- <i class="fa fa-university"></i> -->
							<span><spring:message code='cashflow.common.projectAllocation' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="project allocation"><a href="${pageContext.request.contextPath}/projectAllocation/list"><spring:message code='cashflow.common.projectAllocation' /></a></li>
							</ul></li>
							
						<li><a>
						<span><img src="${pageContext.request.contextPath}/resources/css/img/cubes.svg" class="icon-menu" /></span>
						<!-- <i class="fa fa-cubes"></i> -->
						<span><spring:message code='cashflow.common.masterData' /></span><span
								class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li title="approval type master">
									<a><spring:message code='cashflow.common.approvalTypeMaster' /><span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
			                            <li title="approval type purchase master"><a href="${pageContext.request.contextPath}/admin/approvalTypePurchaseMaster/list"><spring:message code='cashflow.common.approvalTypePurchaseMaster' /></a>
			                            </li>
			                            <li title="approval type sales master"><a href="${pageContext.request.contextPath}/admin/approvalTypeSalesMaster/list"><spring:message code='cashflow.common.approvalTypeSalesMaster' /></a>
			                            </li>
			                        </ul>
								</li>
								<li title="account master"><a href="${pageContext.request.contextPath}/admin/accountMaster/list"><spring:message code='cashflow.common.accountMaster' /></a></li>
								<li title="bank master"><a href="${pageContext.request.contextPath}/admin/bankMaster/list"><spring:message code='cashflow.common.bankMaster' /></a></li>
								<li title="bank account master"><a href="${pageContext.request.contextPath}/admin/bankAccountMaster/list"><spring:message code='cashflow.common.bankAccountMaster' /></a></li>
								<li title="business type master"><a href="${pageContext.request.contextPath}/admin/businessTypeMaster/list"><spring:message code='cashflow.common.businessTypeMaster' /></a></li>
								<li title="currency master"><a href="${pageContext.request.contextPath}/admin/currencyMaster/list"><spring:message code='cashflow.common.currencyMaster' /></a></li>
								<li title="claim type master"><a href="${pageContext.request.contextPath}/admin/claimTypeMaster/list"><spring:message code='cashflow.common.claimTypeMaster' /></a></li>
                                <li title="division master"><a href="${pageContext.request.contextPath}/admin/divisionMaster/list"><spring:message code='cashflow.common.divisionMaster' /></a></li>
                                <li title="employee master"><a href="${pageContext.request.contextPath}/admin/employeeMaster/list"><spring:message code='cashflow.common.employeeMaster' /></a></li>
								<li title="gst master"><a href="${pageContext.request.contextPath}/admin/gstMaster/list"><spring:message code='cashflow.common.gstMaster' /></a></li>
								<li title="payee type master"><a href="${pageContext.request.contextPath}/admin/payeeTypeMaster/list"><spring:message code='cashflow.common.payeeTypeMaster' /></a></li>
								<li title="payee master"><a href="${pageContext.request.contextPath}/admin/payeeMaster/list"><spring:message code='cashflow.common.payeeMaster' /></a></li>
								<li title="project master"><a href="${pageContext.request.contextPath}/admin/projectMaster/list"><spring:message code='cashflow.common.projectMaster' /></a></li>
							</ul></li>

                            <li><a>
                            <span><img src="${pageContext.request.contextPath}/resources/css/img/history.svg" class="icon-menu" /></span>
                            <!-- <i class="fa fa-history"></i> -->
                            <span><spring:message code='cashflow.common.userHistory' /></span><span
                                class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li title="user history"><a href="${pageContext.request.contextPath}/userHistory"><spring:message code='cashflow.common.userHistory' /></a></li>
                                </ul>
                            </li>
                            
						<c:if test="${sessionScope.authorities['PMS-007'] or sessionScope.authorities['PMS-008']}">
							<li><a>
							<span><img src="${pageContext.request.contextPath}/resources/css/img/cogs.svg" class="icon-menu" /></span>
							<!-- <i class="fa fa-cogs"></i> -->
							<span><spring:message code='cashflow.common.system' /></span><span
									class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<c:if test="${sessionScope.authorities['PMS-007'] or sessionScope.authorities['PMS-008']}">
										<li title="manage users"><a href="${pageContext.request.contextPath}/userAccount/list"><spring:message code='cashflow.common.manageUsers' /></a></li>
									</c:if>
									<c:if test="${sessionScope.authorities['PMS-008']}">
										<li title="role & permission"><a href="${pageContext.request.contextPath}/role/list"><spring:message code='cashflow.common.rolePermission' /></a></li>
									</c:if>
									<c:if test="${sessionScope.authorities['PMS-008']}">
										<li title="subsidiary"><a href="${pageContext.request.contextPath}/subsidiary/list"><spring:message code='cashflow.common.subsidiary' /></a></li>
									</c:if>
								</ul></li>
						</c:if>
					</ul>
				</div>
			</div>
			<!-- /sidebar menu -->
			
			<input type="hidden" id="isChangedContent" value="false"/>
		</div>
	</div>


	<!-- top navigation -->
	<div class="top_nav">
		<div class="nav_menu">
			<nav class="navbar">
				<div class="nav toggle">
					<a id="menu_toggle"><i class="fa fa-bars"></i></a>
				</div>
				
				<ul class="nav navbar-nav navbar-left">
					<%@ include file="breadcrumb.jsp"%>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<li class="">
						<a href="javascript:;"class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						 	<!-- <i class="fa fa-user"></i>  --> 
						 	<img src="${pageContext.request.contextPath}/resources/css/img/user.svg" class="icon-menu" />
						 
						 	<span class="user-logged">${sessionScope.firstname} ${sessionScope.lastname}</span>
						 
						 	<span class=" fa fa-angle-down"></span>
						</a>
						<ul class="dropdown-menu dropdown-usermenu pull-right">
							<li><a href="${pageContext.request.contextPath}/userAccount/myProfile"><spring:message code='cashflow.common.profile' /></a></li>
							<li><a href="${pageContext.request.contextPath}/logout"><i
									class="fa fa-sign-out pull-right"></i><spring:message code='cashflow.common.logout' /></a></li>
						</ul>
					</li>
					<li role="presentation" class="dropdown">
						<a href="javascript:;" id="btn-change-tenancy" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> 
						<img src="${pageContext.request.contextPath}/resources/css/img/exchange.svg" class="icon-menu" />
						<!-- <i class="fa fa-exchange"></i>  -->
						<span class="user-logged">${currentSubsidiary.name}</span>
					</a></li>
					
					<li class="">
						<a href="javascript:;"class="lang-local dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="${pageContext.request.contextPath}/resources/css/img/globe.svg" class="icon-menu" />
						 	<!-- <i class="fa fa-globe"></i> -->
						 	<c:choose>
							  	<c:when test="${pageContext.response.locale == 'en'}">
					  				<span class="user-logged en"><spring:message code='cashflow.common.local.en' /></span>
							  	</c:when>
							  	<c:when test="${pageContext.response.locale == 'jp'}">
							    	<span class="user-logged jp"><spring:message code='cashflow.common.local.jp' /></span>
							  	</c:when>
							  	<c:otherwise>
							  		<span class="user-logged "><spring:message code='cashflow.common.local' /></span>
							  	</c:otherwise>
							</c:choose>
						</a>
						<ul class="dropdown-menu mul-language pull-right">
							<li><a onClick="changeLocal('en');"><spring:message code='cashflow.common.local.en' /></a></li>
							<li><a onClick="changeLocal('jp');"><spring:message code='cashflow.common.local.jp' /></a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<!-- /top navigation -->

	<!-- Popup change tenancy -->
	<div class="modal fade" id="ap-tanentcy-popup" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<h2><spring:message code='cashflow.common.changeTenancyMessage1' /></h2>
					<p><spring:message code='cashflow.common.changeTenancyMessage2' /></p>
					<div class="radio">
						<c:forEach items="${sessionScope.subsidiaries}" var="item">
							<label> <input type="radio" name="optionSubsidiaries"
								name="enable-option" value="${item.id}"
								<c:if test="${item.id == currentSubsidiary.id}">
									checked
								</c:if>>
								${item.name}
							</label>
						</c:forEach>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-apply-subsidiary"
						data-dismiss="modal"><spring:message code='cashflow.common.apply' /></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code='cashflow.common.cancel' /></button>
				</div>
			</div>
		</div>
	</div>

</div>
<div class="ap-indicator">
	<div class="foreground"></div>
</div>
<% out.println(getReferenceCSS("header.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/decimal.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-latest.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap-processbar/js/bootstrap-progressbar.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/css/iCheck/icheck.min.js"></script>
<% out.println(getReferenceJS("header.js")); %>
<script src="${pageContext.request.contextPath}/i18n.js?page=${requestScope['javax.servlet.forward.request_uri']}"></script>
<script>
	var GLOBAL_PERMISSION = [];
	<c:forEach items="${sessionScope.authorities}" var="item">
		GLOBAL_PERMISSION["${item.key}"] = ${item.value};
	</c:forEach>
	$(document).ready(function() {
		header.init();
		header.eventListener();
		
		$('#loading').hide();
	});
	//setup loading status for all site
	$(document).on('submit','form',function(){
		showAjaxStatus();
	});
	function setDefaultAjaxStatus(){
		$.ajaxSetup({
			beforeSend : function() {
				showAjaxStatus();
			},
			complete : function() {
				hideAjaxStatus();
			}
		});
	};
	function disableAjaxStatus(){
		$.ajaxSetup({
			beforeSend : function() {
			},
			complete : function() {
				setDefaultAjaxStatus();
			}
		});
	}
	setDefaultAjaxStatus();
	//Remember Collapse / Expand menu status
	if(localStorage.getItem("menuCollapse") && localStorage.getItem("menuCollapse") == 'on'){
		$('body').toggleClass('nav-md nav-sm');
		$('#sidebar-menu').find('li.active ul').hide();
		$('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
		$('#header .logo img').addClass('logo-image-sm').removeClass('logo-image');
	}
</script>

