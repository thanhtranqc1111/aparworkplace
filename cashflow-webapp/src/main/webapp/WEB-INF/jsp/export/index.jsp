<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
<title><spring:message code='cashflow.export.title' /></title>

</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="" id="pageName" />
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
			<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3><spring:message code='cashflow.export.namePage' /></h3>
						</div>
					</div>
				</div>
				
				<form:form id="exportForm" modelAttribute="exportForm" action="export.do"  method="POST" class="form-horizontal">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<div class="x_content">
											<div class="upload-form">
												<div class="col-xs-12">
													<p for="concept" class="col-xs-1 control-label"><spring:message code='cashflow.export.naming' /></p>
													<div class="col-xs-4">
														<form:input path="name" class="form-control" required="required"/>
													</div>
													<div class="col-xs-4">
														<button type="submit" class="btn btn-primary">
															<i class="glyphicon glyphicon-export button-margin"></i><spring:message code='cashflow.export.export' />
														</button>
														<a href="list" class="btn btn-danger btn-cancel"><span class="glyphicon glyphicon-remove button-margin"></span><spring:message code='cashflow.export.cancel' /></a>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.export.listExportFile' /></legend>
										<div class="x_content">
											<div class="panel-body">
												<table id="export-page"
													class="table  table-bordered dt-responsive nowrap"
													cellspacing="0" width="100%">
													<thead>
														<tr>
															<th><spring:message code='cashflow.export.name' /></th>
															<th><spring:message code='cashflow.export.fileSize' /></th>
															<th><spring:message code='cashflow.export.exportedDate' /></th>
															<th><spring:message code='cashflow.export.action' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${fileExportDetails}" var="detail"
															varStatus="status">
															<tr>
																<td><a href="export.download?fileName=${detail.name}">${detail.name}</a></td>
																<td>${detail.size }KB</td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${detail.date.time}" /></td>
																<%
																	String queryString = request.getQueryString();
																	if(StringUtils.isEmpty(queryString)){
																	    queryString = StringUtils.EMPTY;
																	}
																%>
																<td class="text-center"><a href="export.delete?fileName=${detail.name}&<%=queryString%>">
																	<!-- <i class="fa-trash fa"></i> -->
																	<img src="${pageContext.request.contextPath}/resources/css/img/trash.svg" class="icon-menu" />
																</a></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
					<c:forEach var="paramMap" items="${param}">
						<input type="hidden" class="form-control" name="${paramMap.key}" value="${paramMap.value}" readonly>
					</c:forEach>

				</form:form>
			</div>
			<script>
				var crURL = $('.breadcrumb').find('li').first().next().find('a').attr('href');
			    var pageName = $('#sidebar-menu').find("a[href='" + crURL + "']").parents('li').attr('title');
			    $('#pageName').val(pageName);
			</script>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceCSS("import.css")); %>
<% out.println(getReferenceJS("import.js")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	/* <c:if test="${not empty message}">
		showPopupMessage("${message}");
	</c:if> */
	<c:if test="${not empty activeMessage}">
		showPopupMessage("${activeMessage}", function(){});
	</c:if> 
	ImportExcel.init();
	ImportExcel.eventListener();
	ImportExcel.displayData();
});
</script>
</html>