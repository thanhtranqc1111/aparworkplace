<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Cash Flow</title>
<link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
html, body {
  height: 100%;
  background: #FFFBFF;
}

.jumbotron.vertical-center {
  margin-bottom: 0; /* Remove the default bottom margin of .jumbotron */
}

.jumbotron {
	background: #FFFBFF;
}

.vertical-center {
  min-height: 100%;  /* Fallback for vh unit */
  min-height: 100vh; 
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex; 
  
 
    -webkit-box-align : center;
  -webkit-align-items : center;
       -moz-box-align : center;
       -ms-flex-align : center;
          align-items : center;

  width: 100%;
  
 
         -webkit-box-pack : center;
            -moz-box-pack : center;
            -ms-flex-pack : center;
  -webkit-justify-content : center;
          justify-content : center;
}

.container {
  background-color: #FFFBFF;
}
</style>
</head>
<body>
<div class="jumbotron vertical-center">
	<div class="container">
		<div class="row">
			<!-- page content -->
			<div class="col-md-6 col-sm-6 col-xs-6 text-right">
				<img class="logo" src="${pageContext.request.contextPath}/resources/css/img/404.gif" width=60% />
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6  text-left">
				<div class="col-middle">
					
						<h1 class="error-number"><img class="logo" src="${pageContext.request.contextPath}/resources/css/img/404_4.png"  width=60%/></h1>
						<h2>Sorry but we couldn't find this page</h2>
						<p>
							This page you are looking for does not exist <a href="${pageContext.request.contextPath}/"><h2>Back
									to home</h2></a>
						</p>
					
				</div>
			</div>
			<!-- /page content -->
		</div>
	</div>
	</div>
</body>
</html>
