<%@page contentType="text/javascript" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
var GLOBAL_MESSAGES = new Array();

<c:forEach var="key" items="${keys}">
	GLOBAL_MESSAGES["<spring:message text='${key}' javaScriptEscape='true'/>"] = "<spring:message code='${key}' javaScriptEscape='true' />";
</c:forEach>


function i18n(key) {
    if (GLOBAL_MESSAGES[key]) {
        return GLOBAL_MESSAGES[key];
    }
    return key;
}

function label(key) {
    return i18n(key);
}

function title(key) {
    return i18n(key);
}