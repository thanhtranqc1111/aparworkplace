<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.bankStatement.title' /></title>
</head>
<body class="nav-md">
	<input type="hidden" id="currentSubsidiaryId" value="${currentSubsidiary.id}"/>
	<input type="hidden" id="transactionNumber" value="${transactionNumber}"/>
	<input type="hidden" id="transDateFilter" value="${transDateFilter}"/>
	<input id="search-currency" readonly="true" type="hidden" class="form-control" />
	<input id="search-beginning-balance" readonly="true" type="hidden" class="form-control ap-big-decimal" />
	<input id="search-ending-balance" type="hidden" readonly="true" class="form-control ap-big-decimal" />
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="bank statement" id="pageName"/>
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3><spring:message code='cashflow.bankStatement.title' /></h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="downTemplate" class="btn btn-flat btn-primary"><i
									class="fa fa-download button-margin"></i><spring:message code='cashflow.bankStatement.downloadTemplate' /></a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<button type="button"
									class="btn btn-primary btn-flat btn-import">
									<i class="glyphicon glyphicon-import button-margin"></i><spring:message code='cashflow.bankStatement.import' />
								</button>
							</c:if>
						
					</div>
				</div>
				<!-- Search panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<!-- BANK NAME -->
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-horizontal">
												<div class="form-group">
													<label class="control-label col-xs-4"><spring:message code='cashflow.bankStatement.bankname' /></label>
													<div class="col-xs-8">
														<select class="form-control" id="search-bank-statementType"
															name="selector01">
															<option value=""><spring:message code='cashflow.bankStatement.all' /></option>
															<c:forEach items="${bankMaster}" var="item">
																<c:choose >
																	<c:when test="${item.name == bankName}">
																		<option selected="true" bankCode="${item.code}" value="${item.name}">${item.name}</option>
																	</c:when>
																	<c:otherwise>
																		<option bankCode="${item.code}" value="${item.name}">${item.name}</option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
					
														</select>
													</div>
												</div>
										</div>
									</div>
									
									<!-- BANK ACCOUNT -->
									<div class="col-md-3 col-sm-3 col-xs-3">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-4"><spring:message code='cashflow.bankStatement.bankAccNo' /></label>
												<div class="col-xs-8">
													<select class="form-control" id="search-bank-statementAccount">
														<option value=""><spring:message code='cashflow.bankStatement.all' /></option>
														<c:forEach items="${bankAccountMasterFilter}" var="item">
															<c:choose >
																<c:when test="${item.bankAccountNo == bankAccNo}">
																	<option selected="true" currencyCode="${item.currencyCode}"
																			beginningBalance="${item.beginningBalance}"
																			endingBalance="${item.endingBalance}"
																			value="${item.bankAccountNo}">${item.bankAccountNo}</option>
																</c:when>
																<c:otherwise>
																	<option currencyCode="${item.currencyCode}"
																			beginningBalance="${item.beginningBalance}"
																			endingBalance="${item.endingBalance}"
																			value="${item.bankAccountNo}">${item.bankAccountNo}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>	
												</div>
											</div>	
										</div>
									</div>
									
									<!-- TRANSACTION PERIOD -->
									<div class="col-md-2 col-sm-2 col-xs-2">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-6"><spring:message code='cashflow.bankStatement.transactionPeriod' /></label>
												<div class="col-xs-6">
													<select class="form-control" id="search-bank-trans-date">
														<option value="3"><spring:message code='cashflow.bankStatement.last3months' /></option>
														<option value="6"><spring:message code='cashflow.bankStatement.last6months' /></option>
														<option value="12"><spring:message code='cashflow.bankStatement.last12months' /></option>
														<option value="0"><spring:message code='cashflow.bankStatement.specifyTime' /></option>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group panel-from-to-date">
												<div class="col-xs-9 col-xs-offset-1">
													<div class="col-sm-5" >
														<input id="search-transactionFrom" type="text" value="${transactionFrom}"
															class="form-control ap-date-picker form-group-transection-preiod" />
													</div>
													<div class="col-sm-1" style="text-align: center;">
														<p class="form-group-transection-preiod">~</p>
													</div>
													<div class="col-sm-5" >
														<input id="search-transactionTo" type="text" value="${transactionTo}"
															class="form-control ap-date-picker form-group-transection-preiod" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class=" col-xs-12">
													<div class="pull-right">
														<c:if test="${not (transactionNumber > 0)}">
															<button type="button"
																class="btn btn-primary btn-flat btn-search  pull-right btn-action-bs"><i class="fa fa-search button-margin"></i><spring:message code='cashflow.bankStatement.search' /></button>
														</c:if>
														<c:if test="${sessionScope.authorities['PMS-006']}">
															<button type="button"
																class="btn btn-primary btn-flat btn-export pull-right btn-action-bs">
																<i class="glyphicon glyphicon-export button-margin"></i><spring:message code='cashflow.bankStatement.export' />
															</button>
														</c:if>
														<c:if test="${sessionScope.authorities['PMS-003']  and (not (transactionNumber > 0))}">
															<button type="button" class="btn btn-primary btn-flat btn-auto-matching-payment pull-right btn-action-bs">
																<i class="fa fa-check button-margin"></i><spring:message code='cashflow.bankStatement.autoMatchingPayment' />
															</button>
														</c:if>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- list panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
						
						
							<div class="alert-warning" id="alert-warning-bank"></div>
							<!-- MAIN DATA TABLE -->
							<div class="ap-search ap-main-table">
								<table id="table_data"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th style="display: none;"><input name="select_all"
												value="1" type="checkbox"></th>
											<th><spring:message code='cashflow.bankStatement.transactionNo' /></th>
											<th><spring:message code='cashflow.bankStatement.transactionDate' /></th>
											<th><spring:message code='cashflow.bankStatement.valueDate' /></th>
											<th><spring:message code='cashflow.bankStatement.currency' /></th>
											<th><spring:message code='cashflow.bankStatement.beginningBalance' /></th>
											<th><spring:message code='cashflow.bankStatement.description1' /></th>
											<th><spring:message code='cashflow.bankStatement.description2' /></th>
											<th><spring:message code='cashflow.bankStatement.reference1' /></th>
											<th><spring:message code='cashflow.bankStatement.reference2' /></th>
											<th><spring:message code='cashflow.bankStatement.debit' /></th>
											<th><spring:message code='cashflow.bankStatement.credit' /></th>
											<th><spring:message code='cashflow.bankStatement.balance' /></th>
											<th><spring:message code='cashflow.bankStatement.paymentOrderNo' /></th>
											<th><spring:message code='cashflow.bankStatement.receiptOrderNo' /></th>
											<th><spring:message code='cashflow.bankStatement.action' /></th>
										</tr>
									</thead>
								</table>
							</div>
							
									<div class="row">
										<div class="col-xs-12 m-t">
											<div class="pull-right">
												<c:if test="${sessionScope.authorities['PMS-002'] or sessionScope.authorities['PMS-003'] or sessionScope.authorities['PMS-004']}">
													<button type="button" class="btn btn-primary btn-flat btn-save-all disabled" data-toggle="tooltip" title="<spring:message code='cashflow.bankStatement.tooltip1' />">
														<i class="fa fa-save button-margin"></i><spring:message code='cashflow.bankStatement.save' />
													</button>
												</c:if>
												<c:if test="${sessionScope.authorities['PMS-002']  and (not (transactionNumber > 0))}">
													<button type="button" class="btn btn-success btn-flat btn-add-new-row pull-left button-margin disabled" data-toggle="tooltip" title="<spring:message code='cashflow.bankStatement.tooltip2' />">
														<i class="fa fa-plus button-margin"></i><spring:message code='cashflow.bankStatement.add' />
													</button>
												</c:if>
											</div>
										</div>
										</div>
								
						
						</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("bank-statement.css")); %>

<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bank-statement/tabledit.min.js"></script>

<% out.println(getReferenceJS("bank-statement.js")); %>

<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		bankStatementWebSocket.init();
		bankStatement.init();
		bankStatement.eventListener();
	});
</script>
</html>