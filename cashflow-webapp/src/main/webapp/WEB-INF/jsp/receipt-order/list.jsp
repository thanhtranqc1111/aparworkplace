<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.receiptOrder.title' /></title>
</head>
<body class="nav-md">
	<input type="hidden" id="receiptOrderNo" value="${receiptOrderNo}"/>
	<input type="hidden" value="${currentSubsidiary.currency}" id="convertedCurrencyCode"/>
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="receipt order" id="pageName"/>
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="page-header">
						<h3><spring:message code='cashflow.receiptOrder.header' /></h3>
					</div>
				</div>
			</div>
			
				<!-- Search panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-3"><spring:message code='cashflow.receiptOrder.bankName' /></label>
												<div class="col-xs-9">
													<select class="form-control" id="search-bank-statementType" name="selector01">
														<option value=""><spring:message code='cashflow.receiptOrder.all' /></option>
														<c:forEach items="${bankMaster}" var="item">
															<option bankCode="${item.code}" value="${item.name}">${item.name}</option>
														</c:forEach>
				
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-xs-3"><spring:message code='cashflow.receiptOrder.bankAccount' /></label>
												<div class="col-xs-9">
													<select class="form-control" id="search-bank-statementAccount">
														<option value=""><spring:message code='cashflow.receiptOrder.all' /></option>
														<c:forEach items="${bankAccountMasterAll}" var="item">
															<option  value="${item.bankAccountNo}">${item.bankAccountNo}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-3"><spring:message code='cashflow.receiptOrder.transactionPeriod' /></label>
												<div class="col-xs-9">
													<div class="col-sm-5" >
														<input id="search-transactionFrom" type="text" class="form-control ap-date-picker" />
													</div>
													<div class="col-sm-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-sm-5">
														<input id="search-transactionTo" type="text" class="form-control ap-date-picker" />
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label class="control-label col-xs-3"><spring:message code='cashflow.receiptOrder.creditAmount' /></label>
												<div class="col-xs-9">
													<div class="col-sm-5" >
														<select id="searh-credit-option" class="form-control">
															<option value="1">>=</option>
															<option value="0">=</option>
															<option value="-1"><=</option>
														</select>
													</div>
													<div class="col-sm-7" >
													<input id="searh-credit" type="text" class="form-control ap-big-decimal" />
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.receiptOrder.status' /></label>
												<div class="col-xs-10">
													<select class="form-control" id="search-bank-status">
														<option value="0"><spring:message code='cashflow.receiptOrder.all' /></option>
														<option value="1"><spring:message code='cashflow.receiptOrder.matched' /></option>
														<option value="2"><spring:message code='cashflow.receiptOrder.matching' /></option>
													</select>
												</div>
											</div>
											
											<div class="form-group">
												<div class=" col-xs-12">
													<div class="pull-right">
														<button type="button" class="btn btn-primary btn-flat btn-search t"><i class="fa fa-search button-margin"></i><spring:message code='cashflow.receiptOrder.search' /></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- list panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
						
						
							<div class="ap-search ap-main-table">
								<table id="table_data"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="6"><spring:message code='cashflow.receiptOrder.collection' /></th>
											<th colspan="9"><spring:message code='cashflow.receiptOrder.aRInvoice' /></th>
										</tr>
										<tr>
											<th><spring:message code='cashflow.receiptOrder.bankTrans' /></th>
											<th><spring:message code='cashflow.receiptOrder.transactionDate' /></th>
											<th><spring:message code='cashflow.receiptOrder.valueDate' /></th>
											<th><spring:message code='cashflow.receiptOrder.currency' /></th>
											<th><spring:message code='cashflow.receiptOrder.creditAmount' /></th>
											<th><spring:message code='cashflow.receiptOrder.status' /></th>
											<th><spring:message code='cashflow.receiptOrder.aRNo' /></th>
											<th><spring:message code='cashflow.receiptOrder.month' /></th>
											<th><spring:message code='cashflow.receiptOrder.payerName' /></th>
											<th><spring:message code='cashflow.receiptOrder.invoiceNo' /></th>
											<th><spring:message code='cashflow.receiptOrder.totalAmountOriginal' /></th>
											<th><spring:message code='cashflow.receiptOrder.totalAmountConverted' /></th>
											<th><spring:message code='cashflow.receiptOrder.remainAmount' /></th>
											<th><spring:message code='cashflow.receiptOrder.varianceAmount' /></th>
											<th><spring:message code='cashflow.receiptOrder.status' /></th>
										</tr>
									</thead>
								</table>
							</div>
							
							
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
	<!-- Matching AR invoice popup -->
	<div class="modal fade" id="ap-receipt-order-matching" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><spring:message code='cashflow.receiptOrder.matchingARInvoice' /></h4>
		        </div>
				<div class="modal-body">
					<div class="alert-warning"></div>
					<div class="x_panel">
						<div class="row form-group">
							<div class="col-xs-1">
								<label><spring:message code='cashflow.receiptOrder.aRNo' /></label>
							</div>
							<div class="col-xs-3 panel-from-to-date">
								<input type="text" class="form-control arno-from" />
								<p>~</p>
								<input type="text" class="form-control arno-to" />
							</div>
							<div class="col-xs-1 col-xs-offset-1">
								<label><spring:message code='cashflow.receiptOrder.payerName' /></label>
							</div>
							<div class="col-xs-3">
								<input type="text" class="form-control" id="payername" />
							</div>
							<div class="col-xs-3">
								<div class="row">
									<div class="col-xs-5 col-xs-offset-1">
										<div class="checkbox">
										  <label><input checked id="isApproved" type="checkbox" value=""><spring:message code='cashflow.receiptOrder.isApproved' /></label>
										</div>
									</div>
									<div class="col-xs-6">
										<div class="checkbox">
										  <label><input checked id="isNotReceived" type="checkbox" value=""><spring:message code='cashflow.receiptOrder.isNotReceived' /></label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-1">
								<label><spring:message code='cashflow.receiptOrder.month' /></label>
							</div>
							<div class="col-xs-3 panel-from-to-date">
								<input type="text" class="form-control ap-date-picker-month-format txt-month-from" />
								<p>~</p>
								<input type="text" class="form-control ap-date-picker-month-format txt-month-to" />
							</div>
							<div class="col-xs-1 col-xs-offset-1">
								<label><spring:message code='cashflow.receiptOrder.totalAmount' /></label>
							</div>
							<div class="col-xs-1">
								<select class="form-control searh-credit-option">
									<option value="1">>=</option>
									<option value="0">=</option>
									<option value="-1"><=</option>
								</select>
							</div>
							<div class="col-xs-2">
								<input type="text" class="form-control ap-big-decimal searh-credit" />	
							</div>
							<div class="col-xs-1">
								<button type="button" class="btn btn-primary btn-flat btn-reset-popup pull-right"><spring:message code='cashflow.receiptOrder.reset' /></button>
							</div>
							<div class="col-xs-1">
								<button type="button" class="btn btn-primary btn-flat btn-search-popup"><i class="fa fa-search button-margin"></i><spring:message code='cashflow.receiptOrder.search' /></button>
							</div>
						</div>
					</div>
					<div class="x_panel">
						<div class="row form-group">
							<div class="col-xs-2">
								<label><spring:message code='cashflow.receiptOrder.bankCreditAmount' /></label>
							</div>
							<div class="col-xs-3"></div>
							<div class="col-xs-2">
								<label><spring:message code='cashflow.receiptOrder.totalSettedAmountOriginal' /></label>
							</div>
							<div class="col-xs-3"></div>
							<div class="col-xs-2">
								<label><spring:message code='cashflow.receiptOrder.totalSettedAmountConverted' /></label>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-2">
								<div class="input-group non-margin">
									<span class="input-group-addon ">${currentSubsidiary.currency}</span>
									<input disabled="disabled" type="text" class="form-control ap-big-decimal" id="txt-bank-credit-amount"/>	
								</div>
							</div>
							<div class="col-xs-3"></div>
							<div class="col-xs-2">
								<div class="input-group non-margin">
									<span class="input-group-addon ">${currentSubsidiary.currency}</span>
									<input disabled="disabled" type="text" class="form-control ap-big-decimal" id="txt-total-setted-amount-original"/>	
								</div>
							</div>
							<div class="col-xs-3"></div>
							<div class="col-xs-2">
								<div class="input-group non-margin">
									<span class="input-group-addon ">${currentSubsidiary.currency}</span>
									<input disabled="disabled" type="text" class="form-control ap-big-decimal" id="txt-total-setted-amount-converted"/>	
								</div>	
							</div>
						</div>
						<div class="row form-group">
							<table id="ar_table_data" class="table table-striped table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th></th>
										<th><spring:message code='cashflow.receiptOrder.aRNo' /></th>
										<th><spring:message code='cashflow.receiptOrder.month' /></th>
										<th><spring:message code='cashflow.receiptOrder.payerName' /></th>
										<th><spring:message code='cashflow.receiptOrder.invoiceNo' /></th>
										<th><spring:message code='cashflow.receiptOrder.description' /></th>
										<th><spring:message code='cashflow.receiptOrder.totalSettedAmountOriginal' /></th>
										<th><spring:message code='cashflow.receiptOrder.remainAmountOriginal' /></th>
										<th><spring:message code='cashflow.receiptOrder.settedAmountOriginal' /></th>
										<th><spring:message code='cashflow.receiptOrder.receiptRate' /></th>
										<th><spring:message code='cashflow.receiptOrder.settedAmountConverted' /></th>
										<th><spring:message code='cashflow.receiptOrder.varianceAmount' /></th>
										<th><spring:message code='cashflow.receiptOrder.varianceAccount' /></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-apply-matching"><spring:message code='cashflow.receiptOrder.apply' /></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code='cashflow.receiptOrder.cancel' /></button>
				</div>
			</div>
		</div>
	</div>
</body>
 <% out.println(getReferenceCSS("receipt-order.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<% out.println(getReferenceJS("receiptOrder.js")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.floatThead.js"></script>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		receiptOrder.init();
		receiptOrder.eventListener();
	});
</script>
</html>