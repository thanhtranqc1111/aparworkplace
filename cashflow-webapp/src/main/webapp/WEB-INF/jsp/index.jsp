<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><spring:message code='cashflow.login.login' /></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/login/login.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>
	<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet" />
	<style>
		footer{
			position: fixed;
		    bottom: 0;
		    text-align: center;
		    width: 100%;
		    padding-bottom: 10px;
		}
	</style>
</head>
<body>

<div class="jumbotron ">
<div class="cover"></div>
<div class="container bg-login">    
        <div id="loginbox" class="mainbox col-md-12  col-sm-12 ">
        	  <div class="loginbox">
            <div class="panel panel-info" >
             <div class="panel-heading">
             <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-6 text-left">
             	<div class="panel-title"><spring:message code='cashflow.login.signIn' /></div>
             </div>
             <div class="col-md-6 col-sm-6 col-xs-6 text-right">
             	<img class="logo" src="${pageContext.request.contextPath}/resources/css/img/logo_tci_header.png" class="img-responsive" />   
             </div>
             
             </div>
                 
             </div>     
             <div class="panel-body" >
                 <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                  <form th:action="@{/login}" method="post" id="form1" class="form-horizontal">
                  	<!-- EMAIL, PASSWORD FORM -->
                  	 <div class="div-normal-login">       
	                     <div class="input-group">
	                         <span class="input-group-addon">
	                         	<img src="${pageContext.request.contextPath}/resources/css/img/user_login.svg" class="icon-lock" />
	                         	<!-- <i class="glyphicon glyphicon-lock"></i> -->
	                         </span>
	                       <input type="text" name="email" id="email" class="form-control"
	                               placeholder="<spring:message code='cashflow.login.email' />" required="true" autofocus="true"/>                                 
	                     </div>
	                         
	                     <div class="input-group">
	                         <span class="input-group-addon">
	                         	<img src="${pageContext.request.contextPath}/resources/css/img/lock.svg" class="icon-lock" />
	                         	<!-- <i class="glyphicon glyphicon-lock"></i> -->
	                         </span>
	                         <input type="password" name="password" id="password" class="form-control"
	                               placeholder="<spring:message code='cashflow.login.password' />" required="true"/>
	                     </div>
	                     <c:if test="${not empty error}">
						   	<div class="input-group">
	                     		<p class="danger"><spring:message code='${error}' /></p>
	                     	</div>
						 </c:if>
						 <c:if test="${not empty msg}">
						   	<div class="input-group">
	                     		<p class="info"><spring:message code='${msg}' /></p>
	                     	</div>
						 </c:if>
	                     <div class="row">
		                     <div class="checkbox col-md-6 col-sm-6 col-xs-6">
		                         <label> 
		                           <input id="login-remember" type="checkbox" name="remember-me" value="1"> <spring:message code='cashflow.login.rememberMe' />
		                         </label>
		                     </div>
		                     <div class="checkbox col-md-6 col-sm-6 col-xs-6">
		                       <input type="button" id="pre-login" name="pre-login" class="btn btn-success btn-pre-login btn-block" value="<spring:message code='cashflow.login.login' />"/>
		                     </div>
		                     <input type="submit" id="login" name="login" class="btn btn-success btn-login btn-block" value="<spring:message code='cashflow.login.login' />"/>
	                     </div>
                     </div>
                     <!-- GA AUTHENTICATION CODE -->
                     <div class="div-otp-confirmation">
	                     <div class="input-group">
	                     		<p class="info"><spring:message code='cashflow.login.message1' /></p>
	                      </div>
	                     <div class="input-group">
	                         <span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-ok"></i></span>
	                         <input name="code" id="code" class="form-control" placeholder="<spring:message code='cashflow.login.enterCode' />"/>
	                     </div>
	                     <div class="row">
	                     	 <div class="col-md-4 col-md-offset-8"> 
		                     	<button type="button" class="btn btn-success btn-verify pull-right">
							      <span class="glyphicon glyphicon-ok"></span> <spring:message code='cashflow.login.verify' />
							    </button>
		                     </div>
	                     </div>
                     </div>
                 </form>
             </div>                     
        </div>  
        
        
        </div>
    </div>
    
</div>
</div>

<footer>
	<div>© <spring:message code='cashflow.common.footerInfo' /></div>
	<div class="clearfix"></div>
</footer>
</body>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		login.init();
		login.eventListener();
	});
</script>
</html>