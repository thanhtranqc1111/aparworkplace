<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<title><spring:message code='cashflow.apInvoice.detail.titlePage' /></title>

<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
<style>
	.gst-type-input {
		width: 100px !important;
	}
	
	.form-group {
		margin-bottom: 5px;
	}
	
	.form-control {
		height: 30px;
		padding: 2px 4px;
	}
	
	.disabled-input {
		pointer-events: none;
	}
</style>
</head>

<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="ap invoice" id="pageName" />
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						
							<c:if test="${sessionScope.authorities['PMS-002']}">
								<a href="${pageContext.request.contextPath}/apinvoice/detail" id="new-btn" class="btn btn-success" role="button">
									<i class="fa fa-plus-square button-margin"></i>
									<spring:message code='cashflow.apInvoice.detail.new' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-003']}">
								<c:choose>
									<c:when test="${apInvoiceForm.status == '004'}">
										<button type="button" class="btn btn-warning" disabled="disabled">
									</c:when>
									<c:otherwise>
										<button id="edit-btn" type="button" class="btn btn-warning">
									</c:otherwise>
								</c:choose>
								<i class="fa  fa-pencil-square-o button-margin"></i>
								<spring:message code='cashflow.apInvoice.detail.edit' />
								</button>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-002']}">
								<a href="${pageContext.request.contextPath}/apinvoice/copy/${apInvoiceForm.apInvoiceNo}" id="copy-btn" class="btn btn-primary" role="button">
									<i class="fa fa-clone button-margin"></i> <spring:message code='cashflow.apInvoice.detail.clone' />
								</a>
							</c:if>
					</div>
				</div>
				<form:form id="form1" modelAttribute="apInvoiceForm" action="${pageContext.request.contextPath}/apinvoice/save" method="POST" class="form-horizontal">
					<form:hidden path="id" />
					<input type="hidden" id="isConfirmChanged" value="true" />
					<form:hidden path="status" />
					<input type="hidden" value="${currentSubsidiary.currency}" id="convertedCurrencyCode" />

					<div class="error-area"></div>

					<!-- search content -->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<div class="page-header">
											<h4>
												<spring:message code='cashflow.apInvoice.detail.apHeader' />
												<small>
													<span class="ap-status api-${apInvoiceForm.status} ap-status-header ap-label-status">${apInvoiceForm.statusVal}</span>
													<c:if test="${apInvoiceForm.id > 0}">
														<div class="dropdown ap-dropdown-status">
															<button class="ap-status api-${apInvoiceForm.status} ap-status-header" data-toggle="dropdown">
																<span class="status-label">${apInvoiceForm.statusVal}</span>
																<span class="caret"></span>
															</button>
															<ul class="dropdown-menu dropdown-menu-ap-status">
																<li><a href="#" status="004">${mapAPIStatus.get('004')}</a></li>
															</ul>
														</div>
													</c:if>
												</small>
											</h4>
										</div>
										<div class="row">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.payeeName' />*</label>
														<div class="col-sm-10">
															<form:input type="hidden" path="payeeCode" id="payeeCode" />
															<form:input class="form-control" type="text" path="payeeName" id="payeeName" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.payeeType' /></label>
														<div class="col-sm-10">
															<form:input type="hidden" path="payeeTypeCode" id="payeeTypeCode" />
															<form:input path="payeeTypeName" id="payeeTypeName" readonly="true" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.apNo' /></label>
														<div class="col-sm-10">
															<form:input path="apInvoiceNo" id="apInvoiceNo" readonly="true" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.invoiceNo' /></label>
														<div class="col-sm-10">
															<form:input path="invoiceNo" id="invoiceNo" class="form-control" name="invoiceNo" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.currency' /></label>
														<div class="col-sm-10">
															<form:select path="originalCurrencyCode" class="form-control">
																<form:options items="${currencyMap}" />
															</form:select>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.fxRate' /> *</label>
														<div class="col-sm-10">
															<form:input path="fxRate" id="fxRate" name="fxRate"
															class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.apInvoice.detail.gst' /></label>
														<div class="col-sm-10">
															<form:input path="gstRate" id="gstRate" class="form-control" />
														</div>
													</div>
												</div>
											</div>
	
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.invoiceIssuedDate' /></label>
														<div class="col-sm-8">
															<form:input type="text" path="invoiceIssuedDate"
																id="invoiceIssuedDate" class="form-control" />
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.bookingDate' /></label>
														<div class="col-sm-8">
															<form:input type="text" path="bookingDate"
																id="bookingDate" class="form-control" />
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.month' /></label>
														<div class="col-sm-8">
															<form:input type="text" path="month" id="month" class="form-control" />
														</div>
													</div>	
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.claimType' /></label>
														<div class="col-sm-8">
															<form:select path="claimType" class="form-control">
																<form:options items="${claimTypeMap}" />
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.gstType' /></label>
														<div class="col-sm-8">
															<select name="gstType" id="gstType" class="form-control">
																<c:forEach items="${listGstMaster}" var="item">
																	<c:choose>
																		<c:when test="${item.type == apInvoiceForm.gstType}">
																			<option rate="${item.rate}" value="${item.type}" selected>${item.type}</option>
																		</c:when>
																		<c:otherwise>
																			<option rate="${item.rate}" value="${item.type}">${item.type}</option>
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.accountPayableCode' /></label>
														<div class="col-sm-8">
															<form:input path="accountPayableCode"
															id="accountPayableCode" class="form-control"
															list="listAccountMaster" />
															<datalist id="listAccountMaster">
																<c:forEach items="${listAccountMaster}" var="item">
																	<option name="${item.name}" value="${item.code}">${item.name}</option>
																</c:forEach>
															</datalist>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.apInvoice.detail.accountPayableName' /></label>
														<div class="col-sm-8">
															<form:input path="accountPayableName"
															id="accountPayableName" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.totalAmountExcludedGstOriginal' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon exclude-gst-original-currency"></span>
																<form:input path="excludeGstOriginalAmount" id="excludeGstOriginalAmount" readonly="true"
																		class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.totalAmountExcludedGstConverted' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="excludeGstConvertedAmount"
																	id="excludeGstConvertedAmount" readonly="true"
																	class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.gstAmountConverted' /></label>
														<div class="col-sm-6">
															<form:hidden path="gstOriginalAmount" />
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="gstConvertedAmount"
																	id="gstConvertedAmount" readonly="true"
																	class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.totalAmountIncludeGstConverted' /></label>
														<div class="col-sm-6">
															<form:hidden path="includeGstOriginalAmount" />
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="includeGstConvertedAmount"
																	id="includeGstConvertedAmount" readonly="true"
																	class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.paymentTerm' /></label>
														<div class="col-sm-6">
															<form:input path="paymentTerm" id="paymentTerm" readonly="true" class="form-control currency-number" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.apInvoice.detail.scheduledPaymentDate' /></label>
														<div class="col-sm-6">
															<form:input type="text" id="scheduledPaymentDate"
															path="scheduledPaymentDate" readonly="true"
															class="form-control" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8 col-sm-8 col-xs-8">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-1"><spring:message code='cashflow.apInvoice.detail.description' /></label>
														<div class="col-sm-11">
															<form:textarea path="description" class="form-control" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<!--DETAIL INVOICE-->
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="panel panel-default">
										<div class="panel-body">
											<fieldset>
												<legend>
													<spring:message code='cashflow.apInvoice.detail.invoiceDetail' />
												</legend>

												<!-- this list for status of approval -->
												<ul id="alertApprovalStatus"></ul>
												<br />
												<div class="detail-area detail-invoice-table-wrapper" style="width: 2300px;">
														<table id="detail-invoice-table"
															class="display table table-bordered" style="width: 100%">
															<thead>
																<tr>
																	<th><spring:message code='cashflow.apInvoice.detail.accCode' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.accountName' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.prjCode' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.prjName' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.description' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.poNo' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.approvalNo' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.amountExcludeGstOriginal' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.fxRate' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.amountExcludeGstConverted' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.gstType' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.gstRate' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.gstAmountConverted' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.amountIncludeGstOriginal' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.amountIncludeGstConverted' /></th>
																	<th class="status-header"><spring:message code='cashflow.apInvoice.detail.status' /></th>
																	<th><spring:message code='cashflow.apInvoice.detail.action' /></th>
																</tr>
															</thead>

															<tbody>
																<c:forEach items="${apInvoiceForm.detailForms}" var="detail" varStatus="status">
																	<c:choose>
																		<c:when test="${detail.approvalCode == approvalCode}">
																			<tr style="background: #bdd7ec !important;">
																		</c:when>
																		<c:otherwise>
																			<tr>
																		</c:otherwise>
																	</c:choose>

																	<input type="hidden"
																		id="apInvoiceAccountDetailId-${status.index}"
																		name="detailForms[${status.index}].apInvoiceAccountDetailId"
																		value="${detail.apInvoiceAccountDetailId}">
																	<input type="hidden"
																		id="apInvoiceClassDetailId-${status.index}"
																		name="detailForms[${status.index}].apInvoiceClassDetailId"
																		value="${detail.apInvoiceClassDetailId}">
																	<td><input type="text" index="${status.index}"
																		name="detailForms[${status.index}].accountCode"
																		value="${detail.accountCode}"
																		id="invoice-account-row-detail-${status.count-1}"
																		class="form-control invoice-account-row-detail">
																	</td>
																	<td><input type="text"
																		name="detailForms[${status.index}].accountName"
																		value="${detail.accountName}"
																		id="invoice-account-name-row-detail-${status.count-1}"
																		class="form-control"></td>
																	<td><input type="text"
																		name="detailForms[${status.index}].classCode"
																		value="${detail.classCode}"
																		id="invoice-project-row-detail-${status.count-1}"
																		class="form-control"></td>
																	<td><input type="text"
																		name="detailForms[${status.index}].className"
																		value="${detail.className}"
																		id="invoice-project-name-row-detail-${status.count-1}"
																		class="form-control"></td>
																	<td><form:input path="detailForms[${status.index}].description"
																			class="form-control"
																			id="description-${status.count-1}" /></td>
																	<td><form:input path="detailForms[${status.index}].poNo"
																			class="form-control" id="po-no-${status.count-1}" />
																	</td>
																	<td><form:input path="detailForms[${status.index}].approvalCode"
																			class="form-control"
																			id="approval-code-${status.count-1}" /></td>
																	<td>
																		<div class="input-group">
																			<span class="input-group-addon inv-currency"></span>
																			<input type="text"
																				name="detailForms[${status.index}].excludeGstOriginalAmount"
																				id="exclude-gst-original-amount-${status.count-1}"
																				value="${detail.excludeGstOriginalAmount}"
																				id="exclude-gst-original-amount-${status.count-1}"
																				class="form-control">
																		</div>
																	</td>
																	<td><input type="text"
																				name="detailForms[${status.index}].fxRate"
																				id="fx-rate-${status.count-1}"
																				value="${detail.fxRate}" class="form-control fx-rate" readonly="readonly" >
																	</td>
																	<td>
																		<div class="input-group">
																			<span class="input-group-addon sub-currency"></span>
																			<input type="text"
																				name="detailForms[${status.index}].excludeGstConvertedAmount"
																				id="exclude-gst-converted-amount-${status.count-1}"
																				value="${detail.excludeGstConvertedAmount}"
																				class="form-control">
																		</div>
																	</td>
																	<td>
																		<input type="text"
																		name="detailForms[${status.index}].gstType"
																		id="gst-type-${status.count-1}"
																		value="${detail.gstType}" class="gst-type form-control" readonly="readonly">
																	</td>
																	<td><input type="text"
																		name="detailForms[${status.index}].gstRate"
																		id="gst-rate-${status.count-1}"
																		value="${detail.gstRate}" class="gst-rate form-control" readonly="readonly">
																	</td>
																	<td><input type="hidden"
																		name="detailForms[${status.index}].gstOriginalAmount"
																		id="gst-original-amount-${status.count-1}"
																		value="${detail.gstOriginalAmount}">
																		<div class="input-group">
																			<span class="input-group-addon sub-currency"></span>
																			<input type="text"
																				name="detailForms[${status.index}].gstConvertedAmount"
																				id="gst-converted-amount-${status.count-1}"
																				value="${detail.gstConvertedAmount}"
																				class="form-control">
																		</div></td>
																	<td>
																		<div class="input-group">
																			<span class="input-group-addon sub-currency"></span>
																			<input type="text"
																				name="detailForms[${status.index}].includeGstOriginalAmount"
																				id="include-gst-original-amount-${status.count-1}"
																				value="${detail.includeGstOriginalAmount}"
																				class="form-control">
																		</div>
																	</td>
																	<td>
																		<div class="input-group">
																			<span class="input-group-addon sub-currency"></span>
																			<input type="text"
																				name="detailForms[${status.index}].includeGstConvertedAmount"
																				id="include-gst-converted-amount-${status.count-1}"
																				value="${detail.includeGstConvertedAmount}"
																				class="form-control">
																		</div>
																	</td>
																	<td>
																		<div class="ap-status apr-${detail.apprStatusCode} ap-detail">${detail.apprStatusVal}</div>
																	</td>
																	<td><span id="action-del-${status.count-1}"
																		class="action-del glyphicon glyphicon-trash"></span></td>
																	</tr>
																</c:forEach>
															</tbody>

														</table>

														<br />

													</div>
											</fieldset>

											<div class="col-sm-12 m-t">
												<div class="row">
													<button id="add-row-detail-invoice-btn" type="button" class="btn btn-success">
														<i class="fa fa-plus-square button-margin"></i>
														<spring:message code='cashflow.apInvoice.detail.add' />
													</button>
													<button id="copy-row-detail-invoice-btn" type="button" class="btn btn-success">
														<i class="fa fa-copy button-margin"></i>
														<spring:message code='cashflow.apInvoice.detail.copy' />
													</button>
													<div class="pull-right">
														<button id="btn-cancel" class="btn btn-danger" type="button">
															<i class="glyphicon glyphicon-remove button-margin"></i>
															<spring:message code='cashflow.apInvoice.detail.cancel' />
														</button>
														<c:if test="${sessionScope.authorities['PMS-002'] || sessionScope.authorities['PMS-003']}">
															<a href="#" id="save-btn" onClick="APInvoice.validationForm();" class="btn btn-primary"> 
																<i class="fa fa-save button-margin"></i>
																<spring:message code='cashflow.apInvoice.detail.save' />
															</a>
														</c:if>
														<input type="button" id="btn-reset" class="btn btn-primary" value="<spring:message code='cashflow.apInvoice.detail.reset' />">
													</div>
												</div>
											</div>
										
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>

				
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
	<select id="gst-master-select-hidden" hidden="true">
		<c:forEach items="${listGstMaster}" var="item">
			<c:choose>
				<c:when test="${item.type == detail.gstType}">
					<option rate="${item.rate}" value="${item.type}" selected>${item.type}</option>
				</c:when>
				<c:otherwise>
					<option rate="${item.rate}" value="${item.type}">${item.type}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
	<input type="hidden" code="${apr002.code}" id="apr002Value" value="${apr002.value}">
	<div class="modal fade" id="confirm-delete" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">

							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">
									<spring:message code='cashflow.apInvoice.detail.confirm' />
								</h4>
							</div>

							<div class="modal-body">
								<p><spring:message code='cashflow.apInvoice.detail.messageConfirm' /> </p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									<spring:message code='cashflow.apInvoice.detail.cancel' />
								</button>
								<a class="btn btn-danger btn-deleted"><spring:message code='cashflow.apInvoice.detail.delete' /></a>
							</div>
						</div>
					</div>
				</div>
</body>
<% out.println(getReferenceCSS("ap-invoice.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/moment/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<% out.println(getReferenceJS("ap-invoice-detail.js")); %>

<script type="text/javascript">
	var counter = ${apInvoiceForm.detailForms.size()};
	var modeEdit = ${apInvoiceForm.id > 0};
	var modeCopy = window.location.href.indexOf('apinvoice/copy') > 0;
	<c:forEach var="entry" items="${mapAPIStatus}">
		APInvoice.AP_STATUSES["${entry.key}"] = "${entry.value}";
	</c:forEach>
	$(document).ready(function() {
		<c:if test="${not empty message}">
			showPopupSave("${message}", function() {
 		   	window.location = "../list";
 	     });
		</c:if>
		
		<c:if test="${not empty approvalJson}">
			APInvoice.approvalJson = ${approvalJson};
		</c:if>
		
		APInvoice.init();
		APInvoice.eventListener();
		APInvoice.addCommasNumber();
		
		// click edit button when reload page from action delete row
		// in detail table.
		if(sessionStorage.getItem("editFlg")){
			$("#edit-btn").click();
			sessionStorage.removeItem("editFlg");
		}
	});
</script>

</html>