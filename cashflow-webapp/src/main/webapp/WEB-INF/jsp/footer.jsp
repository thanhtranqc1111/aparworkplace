
<!-- footer content -->
<footer>
	<div class="pull-right"><spring:message code='cashflow.common.footerInfo' /></div>
	<div class="clearfix"></div>
</footer>

<script src="${pageContext.request.contextPath}/resources/js/build/custom.js"></script>

<!-- /footer content -->