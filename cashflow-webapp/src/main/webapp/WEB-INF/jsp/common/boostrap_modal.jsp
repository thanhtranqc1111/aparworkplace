<!-- Modal for confirm delete -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="popConfirm">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="gridSystemModalLabel"><spring:message code='cashflow.common.popupDeleteTitle' /></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p id="message" />
					</div>
					<div class="col-md-12" style="margin-left: 20px">
						<label for="applyAllSubCategorize"
							class="msgApplyAllSubCategorize"> <spring:message code='cashflow.common.popupDeleteMessage' /></label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-flat"
					data-dismiss="modal"><spring:message code='cashflow.common.cancel' /></button>
				<button type="button" data-dismiss="modal" id="btnConfirm"
					name="btnConfirm" class="confirm btn btn-primary btn-flat"
					data-target="#confirm-submit"><spring:message code='cashflow.common.ok' /></button>
			</div>
		</div>
	</div>
</div>


<!-- Modal message 1 -->
<div class="modal fade" id="ap-message" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><spring:message code='cashflow.common.message' /></h4>
			</div>
			<div class="modal-body">
				<p id="ap-message-content"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message code='cashflow.common.ok' /></button>
			</div>
		</div>
	</div>
</div>


<!-- Modal save -->
<div class="modal fade" id="modal-save" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><spring:message code='cashflow.common.message' /></h4>
			</div>
			<div class="modal-body">
				<p id="message"></p>
				<p><spring:message code='cashflow.common.clickOkToReturnList' /></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><spring:message code='cashflow.common.cancel' /></button>
				<button type="button" data-dismiss="modal" id="btnOK" name="btnOK" 
							class="confirm btn btn-primary btn-flat"
								data-target="#confirm-submit"><spring:message code='cashflow.common.ok' /></button>
			</div>
		</div>
	</div>
</div>

<!-- Modal message 2 -->
<div class="modal fade" id="ap-message-2" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><spring:message code='cashflow.common.message' /></h4>
			</div>
			<div class="modal-body">
				<p id="ap-message-content-2"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message code='cashflow.common.ok' /></button>
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><spring:message code='cashflow.common.cancel' /></button>
			</div>
		</div>
	</div>
</div>