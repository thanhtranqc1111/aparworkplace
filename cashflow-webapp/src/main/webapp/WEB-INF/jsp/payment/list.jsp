<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.paymentOrder.list.titlePage' /></title>

</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="payment order" id="pageName" />
			<input type="hidden" value="${currentSubsidiary.currency}" id="currentCurrency" />
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3>
								<spring:message code='cashflow.paymentOrder.list.title' />
							</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">

						<c:if test="${sessionScope.authorities['PMS-002']}">
							<a href="detail" class="btn btn-success" name="action_new_ap">
								<i class="fa fa-plus-square"></i> <spring:message
									code='cashflow.paymentOrder.list.addNew' />
							</a>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-005']}">
							<a href="downTemplate" class="btn btn-primary"> <i
								class="fa fa-download"></i> <spring:message
									code='cashflow.paymentOrder.list.downTemplate' />
							</a>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-005']}">
							<a href="import" class="btn btn-primary"> <i
								class="glyphicon glyphicon-import"></i> <spring:message
									code='cashflow.paymentOrder.list.import' />
							</a>
						</c:if>

					</div>
				</div>
				<div class="clearfix"></div>

				<!-- search form -->
				<div class="row ap-search">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal text-left">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-3 text-left"><spring:message code='cashflow.paymentOrder.list.valueDate' /></label>
												<div class="col-md-9 col-sm-9 col-xs-9" id="datepicker">
													<div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0;">
														<input type="text" class="form-control" id="paidFromDate" name="paidFromDate"/>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0;">	
														<input type="text" class="form-control" id="paidToDate" name="paidToDate"/>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-3 text-left"><spring:message code='cashflow.paymentOrder.list.bankReferenceNo' /></label>
												<div class="col-xs-9">
													<input type="text" class="form-control" id="bankRef" name="bankRef"/>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code='cashflow.paymentOrder.list.paymentOrderNo' /></label>
												<div class="col-md-9 col-sm-9 col-xs-9">
													<div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0;">
														<input type="text" class="form-control" id="paymentOrderNoFrom" name="paymentOrderNoFrom"/>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0;">
														<input type="text" class="form-control" id="paymentOrderNoTo" name="paymentOrderNoTo"/>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code='cashflow.apInvoice.list.status' /></label>
												<div class="col-xs-10">
														<select class="form-control" id="status">
															<option value="" label="All" />
															<c:forEach items="${payStatus}" var="item">
																<option value="${item.key}">${item.value}</option>
															</c:forEach>
														</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="pull-right">
													<button type="button" id="btn-search" class="btn btn-primary">
															<i class="fa fa-search button-margin"></i>
															<spring:message code='cashflow.paymentOrder.list.search' />
													</button>
													<a id="btn-reset" class="btn btn-primary "><spring:message code='cashflow.paymentOrder.list.reset' /></a>
												</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- list resutl -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
							
							<div class="top-buffer">
								<table id="payment-tbl"
									class="table table-bordered dataTable ap-payment-list">
									<thead>
										<tr>
											<th colspan="8"><spring:message
													code='cashflow.paymentOrder.list.paymentOrder' /></th>
											<th colspan="5"><spring:message
													code='cashflow.paymentOrder.list.accountPayable' /></th>
										</tr>
										<tr>
											<th><spring:message
													code='cashflow.paymentOrder.list.paymentOrderNo' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.valueDate' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.bankName' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.bankAccount' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.bankTrans' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.bankRefNo' /></th>
											<th><spring:message
													code='cashflow.paymentOrder.list.totalPayment' /></th>
											<th class="status-header"><spring:message
													code='cashflow.paymentOrder.list.status' /></th>
											<th><spring:message code='cashflow.paymentOrder.list.no' /></th>
											<th><spring:message code='cashflow.paymentOrder.list.month' /></th>
											<th><spring:message code='cashflow.paymentOrder.list.claimType' /></th>
											<th><spring:message code='cashflow.paymentOrder.list.totalAmount' /></th>

											<th class="status-header"><spring:message
													code='cashflow.paymentOrder.list.status' /></th>
										</tr>
									</thead>

								</table>

								<div class="col-xs-12">
									<div class="row pull-right">
										<p class="button-bottom">
											<c:if test="${sessionScope.authorities['PMS-006']}">
												<a href="export" id="export" class="btn btn-primary"> <i
													class="glyphicon glyphicon-export button-margin"></i> <spring:message
														code='cashflow.paymentOrder.list.export' /></a>
											</c:if>
										</p>
									</div>
								</div>
							</div>
						
							</div>
						</div>
					</div>
				</div>

			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>


<style>
.datepicker {
	z-index: 1151 !important;
}
</style>
<% out.println(getReferenceCSS("payment.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<% out.println(getReferenceJS("payment-list.js")); %>

<script type="text/javascript">
	$(document).ready(function() {
		PaymentSearch.init();
		PaymentSearch.eventListener();
	});
</script>
</html>