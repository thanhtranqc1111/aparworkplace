<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ttv.cashflow.util.Constant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title><spring:message code='cashflow.paymentOrder.detail.titlePage' /></title>
<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
</head>

<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="payment order" id="pageName" />
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">

						<c:if test="${sessionScope.authorities['PMS-002']}">
							<a href="${pageContext.request.contextPath}/payment/detail" class="btn btn-success" role="button"><i
								class="fa fa-plus-square button-margin"></i> <spring:message
									code='cashflow.paymentOrder.detail.new' /></a>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-003']}">
							<c:choose>
								<c:when test="${paymentForm.statusCode == Constant.PAY_CANCELLED}">
									<button type="button" class="btn btn-warning" disabled="disabled">
								</c:when>
								<c:otherwise>
									<button id="edit-btn" type="button" class="btn btn-warning">
								</c:otherwise>
							</c:choose>
							<i class="fa  fa-pencil-square-o button-margin"></i>
							<spring:message code='cashflow.paymentOrder.detail.edit' />
 							</button>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-002']}">
							<a href="${pageContext.request.contextPath}/payment/copy/${paymentForm.paymentOrderNo}"  class="btn btn-primary" id="copy-btn" role="button">
								<i class="fa fa-clone button-margin"></i> <spring:message code='cashflow.paymentOrder.detail.clone' />
							</a>
						</c:if>

					</div>
				</div>
				<div class="error-area"></div>

				<form:form id="form1" modelAttribute="paymentForm" action="${pageContext.request.contextPath}/payment/action" method="POST" class="form-horizontal">
					<input type="hidden" id="action" name="action" value="save" />
					<input type="hidden" id="paymentType" name="paymentType"/>
					<form:hidden path="id" />
					<form:hidden path="statusCode" />
					
					<input type="hidden" id="isConfirmChanged" value="true" />
					<div class="row" id="pay-detail">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<div class="page-header">
											<h4>
												<spring:message code='cashflow.paymentOrder.detail.paymentOrder' />
												<small>
													<span class="ap-status pay-${paymentForm.statusCode} ap-status-header ap-label-status">${paymentForm.status}</span>
													<c:if test="${paymentForm.id > 0}">
														<div class="dropdown ap-dropdown-status">
															<button class="ap-status pay-${paymentForm.statusCode} ap-status-header" data-toggle="dropdown">
																<span class="status-label">${paymentForm.status}</span> <span class="caret"></span>
															</button>
															<ul class="dropdown-menu dropdown-menu-ap-status">
																<li><a href="#" status="003">${mapPaymentStatus.get('003')}</a></li>
															</ul>
														</div>
													</c:if>
												</small>
											</h4>
										</div>

										<div class="row">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-3"><spring:message code='cashflow.paymentOrder.detail.paymentOrderNo' /></label>
														<div class="col-sm-9">
															<form:input path="paymentOrderNo" class="form-control" readonly="true" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-3"><spring:message code='cashflow.paymentOrder.detail.bankName' /></label>
														<div class="col-sm-9">
															<form:select path="bankName" class="form-control" disabled="${paymentForm.id > 0}">
																<c:forEach items="${listBankMaster}" var="item">
																	<c:choose>
																		<c:when test="${item.code == paymentForm.bankName}">
																			<option bankcode="${item.code}" value="${item.code}"
																				selected="true">${item.code}</option>
																		</c:when>
																		<c:otherwise>
																			<option bankcode="${item.code}" value="${item.code}">${item.code}</option>
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
															</form:select>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-3"><spring:message code='cashflow.paymentOrder.detail.bankAccount' /></label>
														<div class="col-sm-9">
															<form:select path="bankAccount" class="form-control" disabled="${paymentForm.id > 0}">
																<c:forEach items="${listBankAccountMaster}" var="item">
																	<c:choose>
																		<c:when
																			test="${item.bankAccountNo == paymentForm.bankAccount}">
																			<option bankcode="${item.bankCode}"
																				value="${item.bankAccountNo}" selected="true">${item.bankAccountNo}</option>
																		</c:when>
																		<c:otherwise>
																			<option bankcode="${item.bankCode}"
																				value="${item.bankAccountNo}">${item.bankAccountNo}</option>
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
															</form:select>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-3"><spring:message code='cashflow.paymentOrder.detail.description' /></label>
														<div class="col-sm-9">
															<form:textarea path="description" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.totalAmountConverted' /></label>
														<div class="col-sm-8">
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="includeGstConvertedAmount"
																	readonly="true" class="form-control" />
															</div>
															<form:input type="hidden" path="includeGstOriginalAmount"
																readonly="true" class="form-control" />
															<form:input type="hidden" path="originalCurrencyCode"
																readonly="true" class="form-control" />
															<form:input type="hidden" path="paymentRate" readonly="true"
																class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.valueDate' /> *</label>
														<div class="col-sm-8">
															<form:input path="valueDate" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.bankRefNo' /></label>
														<div class="col-sm-8">
															<form:input path="bankRefNo" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.paymentOrderApprovalNo' /></label>
														<div class="col-sm-8">
															<form:input path="paymentApprovalNo" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.accountPayableCode' /> *</label>
														<div class="col-sm-8">
															<form:input path="accountPayableCode" class="form-control" list="listAccountMaster" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.accountPayableName' />*</label>
														<div class="col-sm-8">
															<form:input path="accountPayableName" class="form-control" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>

						<!-- seach -->
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div id="search-zone">
										<fieldset>
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.paymentOrder.detail.searchHeader' />
												</h4>
											</div>
											
											<div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.paymentOrder.detail.claimType' /></label>
															<div class="col-sm-8">
																<form:select id="searchForm_claimType" path="searchForm.claimType" class="form-control form-control-inline">
																	<form:option value="" label="---" />
																	<form:options items="${claimTypeMap}" />
																</form:select>
															
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-3"><spring:message code='cashflow.paymentOrder.detail.shedulePaymentDate' /></label>
															<div class="col-sm-4">
																<span><form:input id="searchForm_scheduledFrom" path="searchForm.scheduledPaymentDateFrom" class="form-control form-control-inline" /></span>
															</div>
															<div class="col-sm-1" style="text-align: center;">
																<span>~</span>
															</div>
															<div class="col-sm-4">
																<span> <form:input id="searchForm_scheduledTo" path="searchForm.scheduledPaymentDateTo" class="form-control form-control-inline" /></span>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.paymentOrder.detail.month' /></label>
															<div class="col-sm-4">
																<span><form:input id="searchForm_monthFrom" path="searchForm.monthFrom" class="form-control form-control-inline" /></span>
															</div>
															<div class="col-sm-2" style="text-align: center;">
																<span>~</span>
															</div>
															<div class="col-sm-4">
																<span> <form:input id="searchForm_monthTo" path="searchForm.monthTo" class="form-control form-control-inline" /></span>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-3 col-md-offset-2">
																<form:checkbox id="searchForm_isPaid" path="searchForm.isPaid" /> 
																<label class="lbl-status" for="searchForm_isPaid"><spring:message code='cashflow.paymentOrder.detail.onlyUnpaid' /></label>
															</div>
															<div class="col-sm-3">
																<form:checkbox id="searchForm_isApproval" path="searchForm.isApproval" />
																<label class="lbl-approved" for="searchForm_isApproval"><spring:message code='cashflow.paymentOrder.detail.approved' /></label>
															</div>
															<div class=" col-xs-4">
																<div class="pull-right">
																	<a href="#" id="search-btn" class="btn btn-primary pull-right" role="button"> 
																		<i class="fa fa-search button-margin"></i> <spring:message code='cashflow.paymentOrder.detail.search' />
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<div> <span id="warning-message"></span></div>
										<div> <span id="warning-message-currency"></span></div>
										<!-- Payment ap invoice -->
										<div class="header-detail" id="payment-apinvoice">
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.paymentOrder.detail.apInvoice' />
												</h4>
											</div>
											<div class="payment-table-wrapper" style="width: 2300px;">
												<table id="payment-table" class="display table table-bordered" style="width: 100%">
													<thead>
														<tr>
															<th><input type="checkbox" name="" value="" class="detail-id-all-cb"></th>
															<th><spring:message code='cashflow.paymentOrder.detail.apNo' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.shedulePaymentDate' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.payeeName' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.month' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.invoiceNo' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.apAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.paidAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.originalCurrency' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.paymentOrderAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.paymentRate' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.paymentOrderAmountConverted' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.payeeBankName' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.payeeBankAccNo' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.unpaidAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.varianceAmount' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.varianceAccount' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${paymentForm.detailApForms}" var="detail"
															varStatus="status">
															<tr>
																<td>
																	<input type="hidden" id="sub-payment-include-gst-original-amount-${status.count-1}" name="detailApForms[${status.index}].paidIncludeGstOriginalAmountWaiting" value="${detail.paidIncludeGstOriginalAmountWaiting}" />
																	<input type="hidden" id="total-payment-include-gst-original-amount-${status.count-1}" />
																	<input type="hidden" id="" name="detailApForms[${status.index}].detailId" value="${detail.detailId}" />
																	<input type="hidden" id="is-approval-${status.count-1}" name="detailApForms[${status.index}].isApproval" value="${detail.isApproval}" /> 
																	<input type="hidden" id="is-paid-${status.count-1}" value="${detail.isPaid}" />
																	<input type="hidden" id="is-pay-enough-${status.count-1}" value="${detail.isPayEnough}" /> 
																	<input type="hidden" id="account-master-code-${status.count-1}" value="${detail.accountCode}" class="account-master-code"/> 
																	<input type="hidden" id="account-master-name-${status.count-1}" value="${detail.accountName}" class="account-master-name"/> 
																	<input type="checkbox" id="ap-invoice-id-${status.count-1}" name="detailApForms[${status.index}].apInvoiceId" value="${detail.apInvoiceId}" class="detail-id-cb" />
																</td>
																<td class="text-left">
																	<a id="ap-invoice-no-${status.count-1}" style="pointer-events: all;" href="${pageContext.request.contextPath}/apinvoice/detail/${detail.apInvoiceNo}">${detail.apInvoiceNo}</a>
																</td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy" value="${detail.scheduledPaymentDate.time}" /></td>
																<td class="text-left">${detail.payeeName}</td>
																<td class="text-right"><fmt:formatDate pattern="MM/yyyy" value="${detail.month.time}" /></td>
																<td class="text-left">${detail.invoiceNo}</td>
																<td><span>${detail.originalCurrencyCode} </span><span class="currency-inline" id="ap-amount-original-${status.count-1}">${detail.includeGstOriginalAmount}</span></td>
																<td class="input-group-currency"><span class="input-group-addon" id="paid-unconverted-amount-currency-${status.count-1}"></span>
																	<input type="text" name="detailApForms[${status.index}].paidIncludeGstOriginalAmount" value="${detail.paidIncludeGstOriginalAmount}" id="paid-include-gst-original-amount-${status.count-1}" readonly="true" class="form-control"></td>
																<td id="ap-currency-${status.count-1}" class="original-currency">${detail.originalCurrencyCode}</td>
																<td class="input-group-currency"><span class="input-group-addon" id="payment-unconverted-amount-currency-${status.count-1}"></span>
																	<input type="text" name="detailApForms[${status.index}].paymentIncludeGstOriginalAmount" value="${detail.paymentIncludeGstOriginalAmount}" id="payment-include-gst-original-amount-${status.count-1}" class="form-control default pay-order-unconverted">
																</td>
																<td><input type="hidden" name="detailApForms[${status.index}].fxRate" value="${detail.fxRate}" id="fx-rate-${status.count-1}" />
																	<input type="text" name="detailApForms[${status.index}].paymentRate" value="${detail.paymentRate}" id="pay-rate-${status.count-1}" class="form-control default pay-rate"></td>
																<td class="input-group-currency"><span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="text" name="detailApForms[${status.index}].paymentIncludeGstConvertedAmount" value="${detail.paymentIncludeGstConvertedAmount}" id="payment-include-gst-converted-amount-${status.count-1}" class="form-control default"></td>
																<td><input type="text" name="detailApForms[${status.index}].payeeBankName" value="${detail.payeeBankName}" id="payee-bank-name-${status.count-1}" class="form-control payee-bank-name" readonly="true">
																</td>
																<td><input type="text" name="detailApForms[${status.index}].payeeBankAccNo" value="${detail.payeeBankAccNo}" id="payee-bank-acc-no-${status.count-1}" class="form-control payee-bank-acc-no" readonly="true">
																</td>
																<td class="input-group-currency"><span class="input-group-addon" id="unpaid-unconverted-amount-currency-${status.count-1}"></span>
																	<input type="text" name="detailApForms[${status.index}].unpaidIncludeGstOriginalAmount" value="${detail.unpaidIncludeGstOriginalAmount}" readonly="true" id="unpaid-include-gst-original-amount-${status.count-1}" class="form-control"></td>
																<td>
																	<div class="input-group non-margin input-group-currency">
																		<span class="input-group-addon">${currentSubsidiary.currency}</span>
																		<input type="text" name="detailApForms[${status.index}].varianceAmount" value="${detail.varianceAmount}" readonly="true" id="variance-amount-${status.count-1}" class="form-control">
																	</div>
																</td>
																<td><input type="text" name="detailApForms[${status.index}].varianceAccountCode" value="${detail.varianceAccountCode}" id="variance-account-code-${status.count-1}" class="form-control default" readonly="true"></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
										
										<!-- Payment payroll  -->
										<div class="header-detail" id="payment-payroll">
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.paymentOrder.detail.payroll' />
												</h4>
											</div>
											<div class="payment-pr-table-wrapper" >
												<table id="payment-pr-table" class="display table table-bordered dataTable">
													<thead>
														<tr>
															<th><input type="checkbox" name="" value=""
																class="detail-id-all-cb"></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.prNo' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.shedulePaymentDate' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.month' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.netPaymentAmount' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.paidAmount' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.currency' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.paymentOrderAmount' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.paymentRate' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.paymentOrderAmountConverted' /></th>
															<th><spring:message code='cashflow.paymentOrder.payRoll.detail.unpaidAmount' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${paymentForm.detailPayrollForms}" var="detail" varStatus="status">
															<tr>
																<td>
																	<input type="hidden" id="sub-net-payment-amount-${status.count-1}" name="detailPayrollForms[${status.index}].paidNetPaymentAmountWaiting" value="${detail.paidNetPaymentAmountWaiting}" />
																	<input type="hidden" id="total-net-payment-amount-original-${status.count-1}" />
																	<input type="hidden" id="" name="detailPayrollForms[${status.index}].detailId" value="${detail.detailId}" />
																	<input type="hidden" id="pr-is-approval-${status.count-1}" name="detailPayrollForms[${status.index}].isApproval" value="${detail.isApproval}" /> 
																	<input type="hidden" id="pr-is-paid-${status.count-1}" value="${detail.isPaid}" />
																	<input type="hidden" id="pr-is-paid-${status.count-1}" value="${detail.isPayEnough}" />
																	<input type="checkbox" id="payroll-id-${status.count-1}" name="detailPayrollForms[${status.index}].payrollId" value="${detail.payrollId}" class="detail-id-cb" />
																	<input type="hidden" id="account-payable-code-${status.count-1}" value="${detail.accountPayableCode}" class="account-master-code"/> 
																	<input type="hidden" id="account-payable-name-${status.count-1}" value="${detail.accountPayableName}" class="account-master-name"/> 
																</td>
																<td class="text-left">
																	<a id="payroll-no-${status.count-1}" style="pointer-events: all;" href="${pageContext.request.contextPath}/payroll/detail/${detail.payrollNo}">${detail.payrollNo}</a>
																</td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy" value="${detail.paymentScheduleDate.time}" /></td>
																<td class="text-right"><fmt:formatDate pattern="MM/yyyy" value="${detail.month.time}" /></td>
																<td><span>${detail.originalCurrencyCode} </span><span class="currency-inline" id="total-net-payment-${status.count-1}">${detail.totalNetPaymentOriginal}</span></td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span id="paid-net-payment-original-currency-${status.count-1}"></span></span>
																	<input type="text" name="detailPayrollForms[${status.index}].paidNetPayment" value="${detail.paidNetPayment}" id="paid-net-payment-amount-${status.count-1}" readonly="true" class="form-control">
																</td>
																<td id="payroll-currency-${status.count-1}" class="original-currency">${detail.originalCurrencyCode}</td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span id="net-payment-original-currency-${status.count-1}"></span></span>
																	<input type="text" name="detailPayrollForms[${status.index}].netPaymentAmountOriginal" value="${detail.netPaymentAmountOriginal}"
																	id="net-payment-amount-original-${status.count-1}" class="form-control default pay-order-unconverted">
																</td>
																<td><input type="text" name="detailPayrollForms[${status.index}].paymentRate" value="${detail.paymentRate}" id="pr-pay-rate-${status.count-1}" class="form-control default pay-rate"></td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span>${currentSubsidiary.currency} </span></span>
																	<input type="text" name="detailPayrollForms[${status.index}].netPaymentAmountConverted" value="${detail.netPaymentAmountConverted}" id="net-payment-amount-converted-${status.count-1}" class="form-control default"></td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span id="unpaid-net-payment-original-currency-${status.count-1}"></span></span>
																	<input type="text" name="detailPayrollForms[${status.index}].unpaidNetPayment" value="${detail.unpaidNetPayment}"
																	readonly="true" id="unpaid-net-payment-amount-${status.count-1}" class="form-control">
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>								
										
										
										<!-- Reimbursement  -->
										<div class="header-detail" id="payment-reimbursement">
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.paymentOrder.detail.reimbursement' />
												</h4>
											</div>
											<div class="payment-reim-table-wrapper" >
												<table id="payment-reim-table" class="display table table-bordered dataTable">
													<thead>
														<tr>
															<th><input type="checkbox" name="" value="" class="detail-id-all-cb"></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.reimNo' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.shedulePaymentDate' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.employee' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.month' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.reimAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.paidAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.originalCurrency' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.paymentOrderAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.paymentRate' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.paymentOrderAmountConverted' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.unpaidAmountOriginal' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.varianceAmount' /></th>
															<th><spring:message code='cashflow.paymentOrder.detail.reim.varianceAccount' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${paymentForm.detailReimForms}" var="detail" varStatus="status">
															<tr>
																<td>
																	<input type="hidden" id="sub-reim-payment-original-amount-${status.count-1}" name="detailReimForms[${status.index}].paidOriginalAmountWaiting" value="${detail.paidOriginalAmountWaiting}" />
																	<input type="hidden" id="total-reim-payment-original-amount-${status.count-1}" />
																	<input type="hidden" id="" name="detailReimForms[${status.index}].detailId" value="${detail.detailId}" />
																	<input type="hidden" id="reim-is-approval-${status.count-1}" name="detailReimForms[${status.index}].isApproval" value="${detail.isApproval}" /> 
																	<input type="hidden" id="reim-is-paid-${status.count-1}" value="${detail.isPaid}" />
																	<input type="hidden" id="reim-is-pay-enough-${status.count-1}" value="${detail.isPayEnough}" /> 
																	<input type="hidden" id="reim-account-payable-code-${status.count-1}" value="${detail.accountPayableCode}" class="account-master-code"/> 
																	<input type="hidden" id="reim-account-payable-name-${status.count-1}" value="${detail.accountPayableName}" class="account-master-name"/>
																	<input type="checkbox" id="reim-id-${status.count-1}" name="detailReimForms[${status.index}].reimId" value="${detail.reimId}" class="detail-id-cb" />
																</td>
																<td class="text-left">
																	<a id="reimbursement-no-${status.count-1}" style="pointer-events: all;" href="${pageContext.request.contextPath}/reimbursement/detail/${detail.reimbursementNo}">${detail.reimbursementNo}</a>
																</td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy" value="${detail.scheduledPaymentDate.time}" /></td>
																<td><span>${detail.employeeName} </span></td>
																<td class="text-right"><fmt:formatDate pattern="MM/yyyy" value="${detail.month.time}" /></td>
																<td><span>${detail.originalCurrencyCode} </span><span class="currency-inline" id="reim-original-amount-${status.count-1}">${detail.includeGstTotalOriginalAmount}</span></td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span id="reim-paid-original-amount-currency-${status.count-1}"></span></span>
																	<input type="text" name="detailReimForms[${status.index}].paidOriginalAmount" value="${detail.paidOriginalAmount}" id="reim-paid-original-amount-${status.count-1}" readonly="true" class="form-control">
																</td>
																<td id="reim-currency-${status.count-1}" class="original-currency">${detail.originalCurrencyCode}</td>
																<td class="input-group-currency">
																	<span class="input-group-addon"><span id="payment-reim-original-currency-${status.count-1}"></span></span>
																	<input type="text" name="detailReimForms[${status.index}].paymentReimOriginalAmount" value="${detail.paymentReimOriginalAmount}"
																	id="payment-reim-amount-original-${status.count-1}" class="form-control default pay-order-unconverted">
																</td>
																<td>
																	<input type="hidden" name="detailReimForms[${status.index}].fxRate" value="${detail.fxRate}" id="reim-fx-rate-${status.count-1}" />
																	<input type="text" name="detailReimForms[${status.index}].paymentRate" value="${detail.paymentRate}" id="reim-pay-rate-${status.count-1}" class="form-control default pay-rate">
																</td>
																<td class="input-group-currency"><span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="text" name="detailReimForms[${status.index}].paymentReimConvertedAmount" value="${detail.paymentReimConvertedAmount}" id="payment-reim-converted-amount-${status.count-1}" class="form-control default">
																</td>
																<td class="input-group-currency"><span class="input-group-addon" id="reim-unpaid-original-amount-currency-${status.count-1}"></span>
																	<input type="text" name="detailReimForms[${status.index}].unpaidOriginalAmount" value="${detail.unpaidOriginalAmount}" readonly="true" id="reim-unpaid-original-amount-${status.count-1}" class="form-control"></td>
																<td>
																	<div class="input-group non-margin input-group-currency">
																		<span class="input-group-addon">${currentSubsidiary.currency}</span>
																		<input type="text" name="detailReimForms[${status.index}].varianceAmount" value="${detail.varianceAmount}" readonly="true" id="reim-variance-amount-${status.count-1}" class="form-control">
																	</div>
																</td>
																<td><input type="text" name="detailReimForms[${status.index}].varianceAccountCode" value="${detail.varianceAccountCode}" id="reim-variance-account-code-${status.count-1}" class="form-control default" readonly="true"></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
										
										<div class="header-detail" id="result-nodata">
											<div class="text-center">
												<spring:message code='cashflow.paymentOrder.detail.noData' />
											</div>
										</div>
										
									</fieldset>
									<div class="pull-right" style="margin-top: 10px;">
										<button id="btn-cancel" class="btn btn-danger">
											<i class="glyphicon glyphicon-remove button-margin"></i>
											<spring:message code='cashflow.paymentOrder.detail.cancel' />
										</button>
										<c:if
											test="${sessionScope.authorities['PMS-002'] || sessionScope.authorities['PMS-003']}">
											<a href="#" id="save-btn"
												onClick="PaymentOrder.savePaymentOrder();"
												class="btn btn-primary"> <i
												class="fa fa-save button-margin"></i> <spring:message
													code='cashflow.paymentOrder.detail.save' />
											</a>
										</c:if>
										<input type="button" id="reset-btn" class="btn btn-primary"
											value="<spring:message code='cashflow.paymentOrder.detail.reset' />">
									</div>
								</div>

							</div>
						</div>
					</div>
				</form:form>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>

</body>
<% out.println(getReferenceCSS("payment.css")); %>
<link href="${pageContext.request.contextPath}/resources/css/payment.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<% out.println(getReferenceJS("payment-detail.js")); %>
<script type="text/javascript">
	<c:forEach var="entry" items="${mapPaymentStatus}">
		PaymentOrder.PAYMENT_STATUSES["${entry.key}"] = "${entry.value}";
	</c:forEach>
	var modeEdit = ${paymentForm.id > 0};
	var modeCopy = window.location.href.indexOf('payment/copy') > 0;
	$(document).ready(function() {

		<c:if test="${not empty message}">
		showPopupSave("${message}", function() {

			window.location = "${pageContext.request.contextPath}/payment/list";

		});
		</c:if>

		PaymentOrder.init();
		PayrollPaymentOrder.init();
		ReimPaymentOrder.init();
		
		PaymentOrder.eventListener();
		PayrollPaymentOrder.eventListener();
		ReimPaymentOrder.eventListener();

	});
</script>
</html>