<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.projectAllocation.title' /></title>
<style>
</style>
</head>

<body class="nav-md">
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="project allocation" id="pageName" />
			<input type="hidden" value="${currentSubsidiary.currency}" id="currentCurrency" />
			<input type="hidden" id="isConfirmChanged" value="true" />
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="page-header">
						<h3><spring:message code='cashflow.projectAllocation.title' /></h3>
				</div>
				</div>
			</div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="downTemplate" class="btn btn-primary"> <i
									class="fa fa-download button-margin"></i><spring:message code='cashflow.projectAllocation.downloadTemplate' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005'] and sessionScope.authorities['PMS-009']}">
								<a href="import" class="btn btn-primary"> <i
									class="glyphicon glyphicon-import button-margin"></i><spring:message code='cashflow.projectAllocation.import' />
								</a>
							</c:if>
						
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- Search criteria -->
					
				<!-- search content -->
				<div class="row ap-search">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.projectAllocation.employeeName' /></label>
												<div class="col-xs-10">
													<select class="form-control" id="select-employee-code">
														<option value="" label="<spring:message code='cashflow.projectAllocation.pleaseSelect' />" />
														<c:forEach items="${mapEmployee}" var="item">
															<c:choose>
																<c:when test="${item.key == employeeCode}">
																	<option selected="true" value="${item.key}">${item.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${item.key}">${item.value}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.projectAllocation.month' /></label>
												<div class="col-sm-10 datepicker">
													<div class="col-sm-5">
														<input type="text" class="form-control ap-date-picker-month-format" id="txt-fromMonth" name="fromMonth" value="<fmt:formatDate value="${fromMonth.time}" type="date" pattern="MM/yyyy"/>"/>
													</div>
													<div class="col-sm-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-sm-5">
														<input type="text" class="form-control ap-date-picker-month-format" id="txt-toMonth" name="toMonth" value="<fmt:formatDate value="${toMonth.time}" type="date" pattern="MM/yyyy"/>"/>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<div class=" col-xs-12">
												<div class="pull-right">
													<a  id="btn-reset" class="btn btn-primary "><spring:message code='cashflow.projectAllocation.reset' /></a>
													<button type="button" id="btn-search" class="btn btn-primary">
														<i class="fa fa-search button-margin"></i><spring:message code='cashflow.projectAllocation.search' />
													</button>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- ap list  -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">	
							<div class="alert-warning" id="alert-warning"></div>
							<div class="top-buffer">
								<c:if test="${sessionScope.authorities['PMS-009']}">
									<div class="col-xs-12">
										<div class="row pull-right">
											<p class="button-bottom">
												<a id="btn-go-to-project-payment" target="blank" class="btn btn-primary">
													<spring:message code='cashflow.projectAllocation.projectPaymentDetails' />
												</a>
											</p>
										</div>
									</div>
								</c:if>
								<table id="project-allocation-data-table" class="table dataTable table-bordered">
									<thead>
										<tr>
											<th><spring:message code='cashflow.projectAllocation.employeeName' /></th>
											<th><spring:message code='cashflow.projectAllocation.employeeCode' /></th>
											<th><spring:message code='cashflow.projectAllocation.team' /></th>
											<th><spring:message code='cashflow.projectAllocation.project' /></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								
								<div class="col-xs-12">
									<div class="row pull-right">
										<c:if test="${sessionScope.authorities['PMS-002'] or sessionScope.authorities['PMS-003'] or sessionScope.authorities['PMS-004']}">
											<button type="button" class="btn btn-primary btn-flat btn-save-all disabled" data-toggle="tooltip">
												<i class="fa fa-save button-margin"></i><spring:message code='cashflow.projectAllocation.save' />
											</button>
										</c:if>
										<c:if test="${sessionScope.authorities['PMS-006']}">
											<a href="export" id="export" class="btn btn-primary"><i
												class="glyphicon glyphicon-export button-margin"></i><spring:message code='cashflow.projectAllocation.export' /></a>
										</c:if>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
<!-- 				<input type="hidden" id="action" name="action" value="search" /> -->
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("project-allocation.css"));%>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<% out.println(getReferenceJS("project-allocation.js")); %>
<script type="text/javascript">
	$(document).ready(function() {
		projectAllocation.init();
		projectAllocation.eventListener();
	});
</script>
</html>