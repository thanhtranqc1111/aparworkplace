<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<title><spring:message code='cashflow.reimbursement.detail.title' /></title>

<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="reimbursement" id="pageName" />
			<input id="status" name="status" type="hidden" value="${reimbursement.status}">
			<input type="hidden" code="002" id="apr002Value" value="${mapApprovalStatus.get('002')}">
			<input type="hidden" value="${currentSubsidiary.currency}" id="currentCurrency" />
			<input type="hidden" value="${reimbursement.id}" id="reimbursementId" />
			<input type="hidden" id="isConfirmChanged" value="true" />
			<input type="hidden" id="txt-include-gst-total-original-amount" value="${reimbursement.includeGstTotalOriginalAmount}"/>
			<input type="hidden" id="txt-gst-original-amount" value="${reimbursement.gstOriginalAmount}"/>
			<c:set var="readonlyStatus" value=""></c:set>
			<c:set var="disableStatus" value=""></c:set>
			<c:set var="disableWhenAddNew" value="disabled"></c:set>
			<c:if test="${reimbursement.id > 0}">
				<c:set var="readonlyStatus" value="readonly"></c:set>
				<c:set var="disableStatus" value="disabled='disabled'"></c:set>
				<c:set var="disableWhenAddNew" value=""></c:set>
			</c:if>
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						
							<c:if test="${sessionScope.authorities['PMS-002'] and sessionScope.authorities['PMS-009']}">
								<a href="${pageContext.request.contextPath}/reimbursement/detail" id="new-btn" class="btn btn-success" role="button">
									<i class="fa fa-plus-square button-margin"></i>
									<spring:message code='cashflow.reimbursement.detail.new' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-003']}">
								<c:choose>
									<c:when test="${reimbursement.status == '004'}">
										<button id="edit-btn" type="button" class="btn btn-warning disabled">
											<i class="fa  fa-pencil-square-o button-margin"></i><spring:message code='cashflow.reimbursement.detail.edit' />
										</button>
									</c:when>
									<c:otherwise>
										<button id="edit-btn" type="button" class="btn btn-warning ${disableWhenAddNew}">
											<i class="fa  fa-pencil-square-o button-margin"></i><spring:message code='cashflow.reimbursement.detail.edit' />
										</button>
									</c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-002']}">
								<button id="btn-clone" type="button" class="btn btn-warning ${disableWhenAddNew}">
									<i class="fa fa-clone button-margin"></i><spring:message code='cashflow.reimbursement.detail.clone' />
								</button>
							</c:if>
					</div>
				</div>
				<div class="alert-warning" id="alert-warning-reimbursement"></div>
				<!-- search panel -->
				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form id="form-reimbursement-header">
										<fieldset>
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.reimbursement.detail.title' />
													<c:if test="${reimbursement.id > 0}">
														<small>
															<span class="ap-status api-${reimbursement.status} ap-status-header ap-label-status">${reimbursement.statusVal}</span>
															<c:if test="${reimbursement.id > 0}">
																<div class="dropdown ap-dropdown-status">
																	<button class="ap-status api-${reimbursement.status} ap-status-header" data-toggle="dropdown">
																		<span class="status-label">${reimbursement.statusVal}</span>
																		<span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu dropdown-menu-ap-status">
																		<li><a href="#" status="004">${mapReimbursementStatus.get('004')}</a></li>
																	</ul>
																</div>
															</c:if>
														</small>
													</c:if>
												</h4>
											</div>
											<div class="row">
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.employeeName' />*</label>
															<div class="col-sm-10">
																<input id="txt-employeeName" class="form-control" value="${reimbursement.employeeName}" ${readonlyStatus}/>
																<input id="txt-employeeCode" type="hidden" value="${reimbursement.employeeCode}" />
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.reimbursementNo' /></label>
															<div class="col-sm-10">
																<input id="txt-reimbursementNo" readonly class="form-control alway-readonly" value="${reimbursement.reimbursementNo}" />
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.invoiceNo' /></label>
															<div class="col-sm-10">
																<input id="txt-invoiceNo" class="form-control" value="${reimbursement.invoiceNo}" ${readonlyStatus}/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.currency' /></label>
															<div class="col-sm-10">
																<select id="select-currency-type" class="form-control" ${disableStatus} value="${reimbursement.originalCurrencyCode}">
																	<c:forEach var="entry" items="${currencyMap}">
																		<c:choose>
																			<c:when test="${reimbursement.originalCurrencyCode == entry.key}">
																				 <option selected value="${entry.key}">${entry.value}</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${entry.key}">${entry.value}</option>
																			</c:otherwise>
																		</c:choose>	
																	</c:forEach>	
																</select>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.fxRate' />*</label>
															<div class="col-sm-10">
																<input id="txt-re-fx-rate" class="form-control" value="${reimbursement.fxRate}" ${readonlyStatus}/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.reimbursement.detail.gstRate1' />*</label>
															<div class="col-sm-10">
																<input id="txt-re-gst-rate" class="form-control" value="${reimbursement.gstRate}" ${readonlyStatus}/>
															</div>
														</div>
													</div>
												</div>
		
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.payroll.detail.month' />*</label>
															<div class="col-sm-8">
																<input class="form-control ap-date-picker-month-format" type="text" id="txt-month" ${readonlyStatus} value="<fmt:formatDate value="${reimbursement.month.time}" type="date" pattern="MM/yyyy"/>"/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.reimbursement.detail.bookingdate' />*</label>
															<div class="col-sm-8">
																<input class="form-control ap-date-picker" type="text" id="txt-booking-date" ${readonlyStatus} value="<fmt:formatDate value="${reimbursement.bookingDate.time}" type="date" pattern="dd/MM/yyyy"/>"/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.reimbursement.detail.claimType' /></label>
															<div class="col-sm-8">
																<select id="select-claim-type" disabled class="form-control alway-readonly" value="${reimbursement.claimType}">
																	<c:forEach var="entry" items="${claimTypeMap}">
																		<c:choose>
																			<c:when test="${reimbursement.claimType == entry.key}">
																				 <option selected value="${entry.key}">${entry.value}</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${entry.key}">${entry.value}</option>
																			</c:otherwise>
																		</c:choose>	
																	</c:forEach>	
																</select>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.reimbursement.detail.gstType' /></label>
															<div class="col-sm-8">
																<select name="select-gst-type" id="select-gst-type" class="form-control" ${disableStatus}>
																	<c:forEach items="${listGstMaster}" var="item">
																		<c:choose>
																			<c:when test="${item.type == reimbursement.gstType}">
																				<option rate="${item.rate}" value="${item.type}" selected>${item.type}</option>
																			</c:when>
																			<c:otherwise>
																				<option rate="${item.rate}" value="${item.type}">${item.type}</option>
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.reimbursement.detail.accountPayableCode' />*</label>
															<div class="col-sm-8">
																<input id="txt-account-payable-code" class="form-control" ${readonlyStatus} value="${reimbursement.accountPayableCode}"/>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.reimbursement.detail.accountPayableName' />*</label>
															<div class="col-sm-8">
																<input readonly id="txt-account-payable-name" class="form-control alway-readonly" ${readonlyStatus} value="${reimbursement.accountPayableName}"/>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.reimbursement.detail.totalAmountExcludedGSTOriginal' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon original-currency-code">${reimbursement.originalCurrencyCode}</span>
																	<input id="txt-exclude-gst-total-original-amount" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${reimbursement.excludeGstTotalOriginalAmount}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.reimbursement.detail.totalAmountExcludedGSTConverted' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																	<input id="txt-exclude-gst-total-converted-amount" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${reimbursement.excludeGstTotalConvertedAmount}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.reimbursement.detail.gstAmountConverted' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																	<input id="txt-gst-converted-amount" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${reimbursement.gstConvertedAmount}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.reimbursement.detail.totalAmountIncludedGSTConverted' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																	<input id="txt-include-gst-total-converted-amount" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${reimbursement.includeGstTotalConvertedAmount}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.reimbursement.detail.scheduledPaymentDate' /></label>
															<div class="col-sm-6">
																<input id="txt-scheduled-payment-date" class="form-control ap-date-picker"  ${readonlyStatus} value="<fmt:formatDate value="${reimbursement.scheduledPaymentDate.time}" type="date" pattern="dd/MM/yyyy"/>"/>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8 col-sm-8 col-xs-8">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-1"><spring:message code='cashflow.reimbursement.detail.description' /></label>
															<div class="col-sm-11">
																<textarea style="resize: none;"  ${readonlyStatus} id="txt-description" rows="6" cols="50" class="form-control">${reimbursement.description}</textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>							
						</div>
					</div>
					<!-- list panel -->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.reimbursement.detail.reimbursementDetails' /></legend>
										<ul id="alertApprovalStatus"></ul>
										<br />
										<div class="table-scroll">
											<table id="reimbursement-table-data" class="table table-striped table-bordered table-hover">
												<thead>
													 <tr>
													 	<th><spring:message code='cashflow.reimbursement.detail.accountCode' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.accountName' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.projectCode' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.projectName' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.approvalNo' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.description' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.amountExcludedGSTOriginal' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.fxRate' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.amountExcludedGSTConverted' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.gstType' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.gstRate2' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.gstAmountConverted' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.amountIncludedGSTOriginal' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.amountIncludedGSTConverted' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.status' /></th>
													  	<th><spring:message code='cashflow.reimbursement.detail.action' /></th>
													 </tr>
												</thead>
												<tbody>
													<c:forEach var="item" varStatus="status" items="${reimbursement.reimbursementDetailsesList}">
														<c:choose>
															<c:when test="${reimbursement.id == 0}">
																<tr dataid="new-${status.index}">
															</c:when>
															<c:otherwise>
																<tr dataid="${item.id}">
															</c:otherwise>
														</c:choose>
															<td>
																<input type="text" class="form-control txt-account-code" value="${item.accountCode}" ${readonlyStatus}>
															</td>
															
															<td>
																<input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.accountName}">
															</td>
															
															<td>
																<input type="text" class="form-control txt-project-code" value="${item.projectCode}" ${readonlyStatus}>
															</td>
															
															<td>
																<input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.projectName}">
															</td>
															
															<td>
																<c:choose>
																	<c:when test="${empty fn:trim(item.approvalCode)}">
																		<input type="text" class="form-control txt-approval-code" value="${item.approvalCode}" ${readonlyStatus}>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="form-control txt-approval-code" approvalCode ="${item.approvalCode}" amount="${item.approvalIncludeGstOriginalAmount}" value="${item.approvalCode}" ${readonlyStatus}>
																	</c:otherwise>
																</c:choose>
															</td>
															
															<td>
																<input type="text" class="form-control txt-description" value="${item.description}" ${readonlyStatus}>
															</td>
															
															<td>
																<div class="input-group">
																	<span class="input-group-addon original-currency-code">${reimbursement.originalCurrencyCode}</span>
																	<input type="text" class="form-control ap-big-decimal txt-exclude-gst-original-amount" value="${item.excludeGstOriginalAmount}" ${readonlyStatus}>
																</div>
															</td>
															
															<td>
																<input type="text" class="form-control txt-re-fx-rate alway-readonly" readonly="readonly" value="${reimbursement.fxRate}">
															</td>
															
															<td>
																<div class="input-group">
																	<span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="text" class="form-control ap-big-decimal txt-exclude-gst-converted-amount" value="${item.excludeGstConvertedAmount}" ${readonlyStatus}>
																</div>
															</td>
															
															<td>
																<input type="text" class="form-control alway-readonly txt-re-gst-type" readonly="readonly" value="${reimbursement.gstType}">
															</td>
															
															<td>
																<input type="text" class="form-control alway-readonly txt-re-gst-rate" readonly="readonly" value="${reimbursement.gstRate}">
															</td>
															
															<td>
																<div class="input-group">
																	<span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="hidden" class="txt-gst-original-amount" value="${item.gstOriginalAmount}">
																	<input type="text" class="form-control ap-big-decimal txt-gst-converted-amount alway-readonly" readonly="readonly" value="${item.gstConvertedAmount}">
																</div>
															</td>
															
															<td>
																<div class="input-group">
																	<span class="input-group-addon">${reimbursement.originalCurrencyCode}</span>
																	<input type="text" class="form-control ap-big-decimal txt-include-gst-original-amount alway-readonly" readonly="readonly" value="${item.includeGstOriginalAmount}">
																</div>
															</td>
															
															<td>
																<div class="input-group">
																	<span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="text" class="form-control ap-big-decimal txt-include-gst-converted-amount alway-readonly" readonly="readonly" value="${item.includeGstConvertedAmount}">
																</div>
															</td>
															
															<td>
																<div class="ap-status apr-${item.apprStatusCode} ap-detail">${item.apprStatusVal}</div>
															</td>
															
															<td>
																<span><span class="btn-delete-detail-row glyphicon glyphicon-trash"></span></span>
															</td>
														</tr>
													</c:forEach>
													<!-- <tr class="odd tr-nodata"><td valign="top" colspan="9" class="dataTables_empty">No data</td></tr> -->		
												</tbody>
											</table>
										</div>
									</fieldset>
									<div class="col-sm-12 m-t">
										<div class="row panel-bottom-buttons">
											<button id="btn-add-row" type="button" class="btn btn-success" ${disableStatus}>
												<i class="fa fa-plus-square button-margin"></i>
												<spring:message code='cashflow.reimbursement.detail.add' />
											</button>
											<button id="btn-copy" type="button" class="btn btn-success disabled" ${disableStatus}>
												<i class="fa fa-copy button-margin"></i>
												<spring:message code='cashflow.reimbursement.detail.copy' />
											</button>
											
											<div class="pull-right">
												<button id="btn-cancel" class="btn btn-danger" type="button" ${disableStatus}>
													<i class="glyphicon glyphicon-remove button-margin"></i>
													<spring:message code='cashflow.reimbursement.detail.cancel' />
												</button>
												<c:if test="${sessionScope.authorities['PMS-002'] || sessionScope.authorities['PMS-003']}">
													<button id="btn-save" class="btn btn-primary" ${disableStatus}> 
														<i class="fa fa-save button-margin"></i>
														<spring:message code='cashflow.reimbursement.detail.save' />
													</button>
												</c:if>
												<button id="btn-reset-all" class="btn btn-primary" ${disableStatus}>
													<spring:message code='cashflow.reimbursement.detail.reset' />
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("reimbursement-detail.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<% out.println(getReferenceJS("reimbursement-detail.js")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.floatThead.js"></script>
<script>
	<c:forEach var="entry" items="${mapReimbursementStatus}">
		reimbursementDetail.REIMBURSEMENT_STATUSES["${entry.key}"] = "${entry.value}";
	</c:forEach>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";	
	$(document).ready(function() {
		reimbursementDetail.initData(${reimbursement.toJSON()});
		reimbursementDetail.init();
		reimbursementDetail.eventListener();
		reimbursementDetailDataTable.init();
		reimbursementDetailDataTable.eventListener();
	});
</script>
</html>