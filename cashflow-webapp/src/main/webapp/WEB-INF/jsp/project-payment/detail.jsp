<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<title><spring:message code='cashflow.project.payment.detail.titlePage' /></title>

<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
<!-- Import Css for fixedColumns-->
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/fixedColumns.dataTables.min.css" rel="stylesheet" />

<style>
</style>
</head>
<body class="nav-md">
<!-- popup confirm -->
    <!-- Content -->
    <div class="container body">
        <div class="main_container">
            <input type="hidden" value="project allocation" id="pageName" />
            <%@ include file="../header.jsp"%>
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="page-header">
                            <h3><spring:message code='cashflow.project.payment.detail.projectPayment' /></h3>
                        </div>
                    </div>
                </div>

                <c:forEach items="${projectPaymentLst}" var="projectPaymentInfo" varStatus="status">
                    <!-- content Detai -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <fieldset>
                                        <!-- this list for project management -->
                                        <legend class= "month">${projectPaymentInfo.month}</legend>

                                        <div class="row">
                                            <div class="detail-payment-table-wrapper">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <table id="payment-payroll-detail-${status.count-1}" class="table dataTable table-bordered table-striped nowrap payment-payroll-detail-table" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="employeeColumn "><spring:message code='cashflow.project.payment.detail.employeeName' /></th>
                                                                <th class="employeeColumn"><spring:message code='cashflow.project.payment.detail.employeeCode' /></th>
                                                                <th class="employeeColumn"><spring:message code='cashflow.project.payment.detail.totalSalary' /></th>
                                                                <c:forEach items="${projectPaymentInfo.mapProjectCodeAndProjectName}" var="entry">
                                                                        <th class="projectColumn">${entry.value}</th>
                                                                </c:forEach>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${projectPaymentInfo.listPaymentEmployeeDTO}" var="PaymentEmployeeInfor">
                                                                <tr class="rowtable">
                                                                    <td>${PaymentEmployeeInfor.employeeName}</td>
                                                                    <td>${PaymentEmployeeInfor.employeeCode}</td>
                                                                    <td class="projectAmount">${PaymentEmployeeInfor.employeeSalary}</td>
                                                                    <c:forEach items="${projectPaymentInfo.mapProjectCodeAndProjectName}" var="entry">
                                                                        <td class="projectAmount">${PaymentEmployeeInfor.mapProjectAssignedSalary.get(entry.key)}</td>
                                                                    </c:forEach>
                                                                </tr>
                                                            </c:forEach>
                                                            <tr class ="total ">
                                                                <td></td>
                                                                <td class="totalTitle"><spring:message code='cashflow.project.payment.detail.total' /></td>
                                                                <td class ="totalCol"></td>
                                                                <c:forEach items="${projectPaymentInfo.mapProjectCodeAndProjectName}" var="entry">
                                                                    <td class ="totalCol" ></td>
                                                                </c:forEach>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                    <!-- content Detai -->
                    <c:choose>
                        <c:when test="${empty projectPaymentLst}">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <spring:message code='cashflow.project.payment.detail.noData' />
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-xs-12">
                                        <div class="row pull-right">
                                            <p class="button-bottom">
                                                <c:if test="${sessionScope.authorities['PMS-006']}">
                                                    <a href="export" id="export" class="btn btn-primary"> <i
                                                        class="glyphicon glyphicon-export button-margin"></i>
                                                    <spring:message code='cashflow.project.payment.detail.export' /></a>
                                                </c:if>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
            </div>
            <%@ include file="../footer.jsp"%>
        </div>
    </div>
</body>
<% out.println(getReferenceCSS("project-payment.css"));%>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/dataTables.fixedColumns.min.js"></script>
<% out.println(getReferenceJS("project-payment.js")); %>
<script type="text/javascript">
    $(document).ready(function() {
        projectPayment.init();
        projectPayment.eventListener();
    });

</script>
</html>
