<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css"
	rel='stylesheet'></link>
<title><spring:message code='cashflow.approvalSalesOrderRequest.title' /></title>

</head>
<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="sales order request" id="pageName" />
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3>
								<spring:message code='cashflow.approvalSalesOrderRequest.header' />
							</h3>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">

						<c:if test="${sessionScope.authorities['PMS-002']}">
							<button type="button"
								class="btn btn-success btn-flat btn-new-approval">
								<i class="fa fa-plus-square"></i>
								<spring:message
									code='cashflow.approvalSalesOrderRequest.newPurchaseRequest' />
							</button>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-005']}">
							<a href="downTemplate" class="btn btn-primary"> <i
								class="fa fa-download"></i> <spring:message
									code='cashflow.approvalSalesOrderRequest.downloadTemplate' />
							</a>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-005']}">
							<a href="import" class="btn btn-primary btn-flat"> <i
								class="glyphicon glyphicon-import"></i> <spring:message
									code='cashflow.approvalSalesOrderRequest.import' />
							</a>
						</c:if>
					</div>
				</div>
				<!-- Search panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">

									<div class="col-md-5 col-sm-5 col-xs-5">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label"><spring:message
														code='cashflow.approvalSalesOrderRequest.approvalType' /></label>
												<div class="col-sm-10">
													<select class="form-control" id="searh-approvalType">
														<option value=""><spring:message
																code='cashflow.approvalSalesOrderRequest.all' /></option>
														<c:forEach items="${approvalTypeMaster}" var="item">
															<option value="${item.name}">${item.name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="form-group m-b-none">
												<label for="inputApprove" class="col-sm-2 control-label"><spring:message
														code='cashflow.approvalSalesOrderRequest.approvalNo' /></label>
												<div class="col-sm-10">
													<input id="search-approvalType" type="text"
														class="form-control" id="inputApprove" />
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-7 col-sm-7 col-xs-7 text-left">
										<div class="form-inline m-b">
											<div class="form-group">
												<label for="searh-validityFrom"><spring:message
														code='cashflow.approvalSalesOrderRequest.approvedPeriod' /></label>
												<input id="searh-validityFrom" type="text"
													class="form-control ap-date-picker" />
											</div>
											<div class="form-group">
												<label for="searh-validityTo">~</label> <input
													id="searh-validityTo" type="text"
													class="form-control ap-date-picker" />
											</div>

										</div>

										

									</div>
									<div class="col-md-7 col-sm-7 col-xs-7 text-right">
								
												<button type="button"
													class="btn btn-primary btn-flat btn-search pull-right">
													<i class="fa fa-search button-margin"></i>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.search' />
												</button>

									</div>
									
								</div>



							</div>
						</div>

					</div>
				</div>

				<!-- list panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<table id="table_data"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th><input name="select_all" value="1" type="checkbox"></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.approvalType' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.approvalNo' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.applicatioDate' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.validityPeriod' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.currency' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.amount' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.purpose' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.applicant' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.approvedDate' /></th>
											<th><spring:message
													code='cashflow.approvalSalesOrderRequest.appliedFor' /></th>
										</tr>
									</thead>
								</table>


								<c:if test="${sessionScope.authorities['PMS-004']}">
									<button type="button"
										class="btn btn-danger btn-flat btn-delete-approval disabled pull-right">
										<i class="glyphicon glyphicon-trash button-margin"></i>
										<spring:message
											code='cashflow.approvalSalesOrderRequest.delete' />
									</button>
								</c:if>
								<c:if test="${sessionScope.authorities['PMS-006']}">
									<button type="button"
										class="btn btn-primary btn-flat btn-export pull-right button-margin">
										<i class="glyphicon glyphicon-export button-margin"></i>
										<spring:message
											code='cashflow.approvalSalesOrderRequest.export' />
									</button>
								</c:if>


							</div>

						</div>
					</div>
				</div>
				<!-- detail panel -->
				<c:if
					test="${sessionScope.authorities['PMS-002'] or sessionScope.authorities['PMS-003']}">
					<div class="row" id="approvalForm">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default ap-panel-detail">
								<div class="panel-body">
									<div class="alert-warning"></div>
									<fieldset>
										<legend></legend>
										<div class="row form-group m-b-sm">
											<div class="col-xs-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.approvalNo' />
													*
												</label>
											</div>
											<div class="col-sm-3">
												<input id="approvalCode" readonly="true" type="text"
													class="form-control" />
												<!-- table info of ap invoice -->
												<!-- <div id="ap-approval-detail">
													<table class="table table-striped table-bordered table-hover dataTable no-footer">
														<thead>
															<tr>
																<th>Project Name</th>
																<th>Include GST Original Amount</th>
																<th>Description</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div> -->
											</div>
											<div class="col-sm-1">
												<a style="display: none;" id="link-view-detail"
													target="blank" href="#"><p>
														<spring:message
															code='cashflow.approvalSalesOrderRequest.viewDetail' />
													</p></a>
											</div>
											<div class="col-xs-2 col-xs-offset-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.applicationDate' />
												</label>
											</div>
											<div class="col-sm-2">
												<input readonly="true" id="applicationDate" type="text"
													class="form-control ap-date-picker" />
											</div>
										</div>

										<div class="row form-group">
											<div class="col-xs-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.approvalType' />
													*
												</label>
											</div>
											<div class="col-sm-3">
												<select class="form-control" id="approvalType" disabled>
													<c:forEach items="${approvalTypeMaster}" var="item">
														<option value="${item.name}">${item.name}</option>
													</c:forEach>
												</select>
											</div>

											<div class="col-xs-2 col-xs-offset-3">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.currency' />
												</label>
											</div>
											<div class="col-sm-2">
												<select class="form-control" id="input-approval-currency"
													disabled>
													<c:forEach items="${currencyMaster}" var="item">
														<option value="${item.name}">${item.name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.validityDate' />
													*
												</label>
											</div>
											<div class="col-sm-3 panel-from-to-date">
												<input id="validityFrom" type="text"
													class="form-control ap-date-picker" />
												<p>~</p>
												<input id="validityTo" type="text"
													class="form-control ap-date-picker" />
											</div>

											<div class="col-xs-2 col-xs-offset-3">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.amount' />
												</label>
											</div>
											<div class="col-sm-2">
												<input id="amount" readonly="true" type="text"
													class="form-control ap-big-decimal" />
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.approvedDate' />
													*
												</label>
											</div>
											<div class="col-sm-3">
												<input id="approvedDate" type="text"
													class="form-control ap-date-picker" />
											</div>

											<div class="col-xs-2 col-xs-offset-3">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.applicant' />
												</label>
											</div>
											<div class="col-sm-2">
												<input id="applicant" readonly="true" type="text"
													class="form-control" />
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-2">
												<label>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.purpose' />
												</label>
											</div>
											<div class="col-xs-10">
												<textarea id="purpose" readonly="true" rows="4" cols="50"
													class="form-control" style="resize: none;"> </textarea>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 pull-left">
												<button type="button"
													class="btn btn-primary btn-flat disabled btn-save-approval pull-right">
													<i class="fa fa-save button-margin"></i>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.save' />
												</button>
												<button type="button"
													class="btn btn-danger btn-flat disabled btn-cancel-approval pull-right button-margin">
													<i class="fa fa-close button-margin"></i>
													<spring:message
														code='cashflow.approvalSalesOrderRequest.cancel' />
												</button>
												<input type="hidden" name="mode" id="mode" value="New" /> <input
													type="hidden" id="id" />
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>

</body>
<% out.println(getReferenceCSS("sales-order-request.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<% out.println(getReferenceJS("sales-order-request.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		salesOrderRequest.init();
		salesOrderRequest.eventListener();
	});
</script>
</html>