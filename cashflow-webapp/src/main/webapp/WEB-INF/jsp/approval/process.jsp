<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.arInvoice.list.arInvoice' /></title>

<style>
</style>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="ar invoice" id="pageName" />
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3>
								Approval Process
							</h3>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<!-- ar list  -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
                                <iframe src="/camunda/app/tasklist/${currentSubsidiary.code}/#" width="100%" height="1500px" frameborder="0"></iframe>
						    </div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("ar-invoice.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<% out.println(getReferenceJS("ar-invoice-list.js")); %>
</html>