<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.userAccount.title' /></title>
<style type="text/css">
.dt-body-center{text-align: center;}
</style>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="manage users" id="pageName"/>
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="page-header">
						<h3><spring:message code='cashflow.userAccount.header' /></h3>
					</div>
				</div>
			</div>
						
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-xs-6">
									<div class="col-xs-1">
										<h5><spring:message code='cashflow.userAccount.name' /></h5>
									</div>
									<div class="col-xs-4">
										<input id="txt-search" type="text" class="form-control" />
									</div>
									<div class="col-xs-2">
										<button type="button" class="btn btn-primary btn-flat btn-search pull-left">
											 <i class="fa fa-search button-margin"></i><spring:message code='cashflow.userAccount.search' />
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
							<div class="ap-search">
								<table id="table_data"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><input name="select_all" value="1" type="checkbox"></th>
											<th><spring:message code='cashflow.userAccount.firstName' /></th>
											<th><spring:message code='cashflow.userAccount.lastName' /></th>
											<th><spring:message code='cashflow.userAccount.email' /></th>
											<th><spring:message code='cashflow.userAccount.status' /></th>
										</tr>
									</thead>
								</table>
							</div>
							<div class="pull-right">
									<a href="${pageContext.request.contextPath}/userAccount/addNew" type="button" class="btn btn-success "> 
										<i class="fa fa-plus-square"></i> <spring:message code='cashflow.userAccount.newUser' />
									</a>
									<button type="button" class="btn btn-danger btn-flat btn-delete disabled">
									<i class="fa-trash fa button-margin"></i><spring:message code='cashflow.userAccount.delete' />
								</button>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>

<script src="<c:url value="/resources/js/jquery.dataTables.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/dataTables.bootstrap.min.js" />"></script>
<% out.println(getReferenceJS("user-account-list.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		userAccountList.init();
		userAccountList.eventListener();
	});
</script>
</html>