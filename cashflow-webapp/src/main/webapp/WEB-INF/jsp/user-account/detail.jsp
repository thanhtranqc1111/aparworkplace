<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<title><spring:message code='cashflow.userAccount.userInformation' /></title>
<style>
.error-panel{
	color: red; border: 1px solid; margin-top: 20px; padding: 10px; display: none;
}
</style>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="manage users" id="pageName"/>
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="page-header">
						<h3><spring:message code='cashflow.userAccount.userInformation' /></h3>
					</div>
				</div>
			</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-horizontal">
									<fieldset>
										<div class="page-header">
												<h4>
													<spring:message code='cashflow.userAccount.userInformation' />
												</h4>
											</div>
									</fieldset>
									<div class="container">
										<div class="alert-warning"></div>
										<div class="form-group">
											<fieldset <c:if test="${role != 'SYS'}">disabled</c:if>>
												<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.enable' /></label>
												<div class="col-sm-5 radio">
													<label> 
															<input type="radio" name="optionsEnable"
															name="enable-option" value="1"
															<c:if test="${userAccount.isActive == true}">
																checked
															</c:if>>
															<spring:message code='cashflow.userAccount.yes' />
													</label> 
													<label>
															 <input type="radio" name="optionsEnable"
															name="enable-option" value="0"
															<c:if test="${!userAccount.isActive == true}">
																checked
															</c:if>>
															<spring:message code='cashflow.userAccount.no' />
													</label>
												</div>
											</fieldset>
										</div>
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.email' /> *</label>
											<div class="col-sm-3">
												<input type="text" class="form-control disabled" id="email" <c:if test="${role != 'SYS'}">disabled</c:if> value="${userAccount.email}">
											</div>
											<c:if test="${userAccount.id != 0}">
												<div class="col-sm-3">
													<button type="button" class="btn  btn-link btn-flat btn-change-pass"><spring:message code='cashflow.userAccount.changePassword' /></button>
												</div>
											</c:if>
										</div>
										<fieldset class="change-pass-fieldset"
											<c:if test="${userAccount.id != 0}">style="display: none;"</c:if>>
											<c:if test="${role != 'SYS'}">
												<div class="form-group">
													<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.yourPassword' /> *</label>
													<div class="col-sm-3">
														<input type="password" class="form-control disabled" id="password">
													</div>
												</div>
											</c:if>
											<div class="form-group disabled">
												<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.newPassword' /> *</label>
												<div class="col-sm-3">
													<input type="password" class="form-control disabled" id="new-password">
													<p id="passwordHelpBlock" class="form-text text-muted">
														<spring:message code='cashflow.userAccount.passwordHint' />
													</p>
												</div>
												<div class="col-sm-1">
													<button type="button" class="btn btn-info btn-flat btn-gen-pass"><spring:message code='cashflow.userAccount.generate' /></button>
												</div>
												<div class="col-sm-2 ">
													<input style="margin-left: 20px;" type="text" class="form-control" id="gen-password">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.confirmPassword' /> *</label>
												<div class="col-sm-3">
													<input type="password" class="form-control disabled" id="confirm-password">
												</div>
											</div>
										</fieldset>
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.role' /> *</label>
											<div class="col-sm-3">
												<c:choose>
													<c:when test="${role == 'SYS' and (sessionScope.authorities['PMS-008'] or userAccount.id == 0)}">
														<select class="form-control" id="select-role">
															<c:forEach items="${roleList}" var="item">
																<c:choose>
																	<c:when test="${userAccount.role.id == item.id}">
																		<option crrole="crrole" roleType="${item.roleType}" value="${item.id}" selected>
																	</c:when>
																	<c:otherwise>
																		<option roleType="${item.roleType}" value="${item.id}">
																	</c:otherwise>
																</c:choose>
											        	${item.name}
											        </option>
															</c:forEach>
														</select>
													</c:when>
													<c:otherwise>
														<input type="text" class="form-control" value="${userAccount.role.name}" disabled>
													</c:otherwise>
												</c:choose>
											</div>
										</div>
										<c:if test="${fn:contains(CURRENT_URL, '/userAccount/myProfile')}">
											<div class="form-group">
												<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.defaultSubsidiary' /> *</label>
												<div class="col-sm-3">
													<select class="form-control" id="select-default-sub">
														<c:forEach items="${userAccount.subsidiaryUserDetails}"
															var="item">
															<c:choose>
																<c:when test="${item.isDefault}">
																	<option value="${item.subsidiary.id}" selected>
																</c:when>
																<c:otherwise>
																	<option value="${item.subsidiary.id}">
																</c:otherwise>
															</c:choose>
														${item.subsidiary.name}
											        </option>
														</c:forEach>
													</select>
													<p class="form-text text-muted"><spring:message code='cashflow.userAccount.defaultSubsidiaryHint' /></p>
												</div>
											</div>
										</c:if>
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.firstName' /> *</label>
											<div class="col-sm-3">
												<input type="text" class="form-control disabled" id="first-name" value="${userAccount.firstName}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.lastName' /> *</label>
											<div class="col-sm-3">
												<input type="text" class="form-control disabled" id="last-name" value="${userAccount.lastName}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.phone' /> *</label>
											<div class="col-sm-3">
												<input type="text" class="form-control disabled" id="phone" value="${userAccount.phone}">
											</div>
										</div>
										<c:if test="${role == 'SYS'}">
											<div class="form-group">
												<label class="col-sm-2 col-sm-offset-2 control-label"><spring:message code='cashflow.userAccount.subsidiary' /> *</label>
												<div class="col-sm-3">
													<c:forEach items="${subsidiatyList}" var="item">
														<c:set var="checked" value="0"></c:set>
														<c:forEach items="${userAccount.subsidiaryUserDetails}" var="item2">
															<c:if test="${item2.subsidiary.id == item.id}">
																<label style="margin-right: 10px; margin-left: 0px;" class="checkbox-inline"><input crsub="crsub" subId="${item.id}" class="checkbox-subsidiaty" type="checkbox" checked="true">${item.name}</label>
																<c:set var="checked" value="1"></c:set>
															</c:if>
														</c:forEach>
														<c:if test="${checked == 0}">
															<label style="margin-right: 10px; margin-left: 0px;" class="checkbox-inline"><input subId="${item.id}" class="checkbox-subsidiaty" type="checkbox" value="">${item.name}</label>
														</c:if>
													</c:forEach>
												</div>
											</div>
										</c:if>
										<c:if test="${userAccount.id != 0}">
											<div class="form-group">
												<div class="col-sm-1 col-sm-offset-3"><img class="img-responsive pull-right" style="width: 65%;" src="${pageContext.request.contextPath}/resources/css/img/google-authenticator-icon.png"></div>
												<div class="col-sm-3 row">
													<c:if test="${userAccount.isUsing2fa == true}">
														<div class="col-sm-12 row">
															<div class="col-sm-9">
																<h4><spring:message code='cashflow.userAccount.message6' /></h4>
															</div>
															<div class="col-sm-3">
																<button type="button" value="false" class="btn btn-info btn-sm pull-left btn-toogle-2fa"><span class="glyphicon glyphicon-ban-circle button-margin"></span><spring:message code='cashflow.userAccount.disable' /></button>
															</div>
														</div>
														<div class="col-sm-12">
															<p class="text-info"><spring:message code='cashflow.userAccount.message7' /></p>
														</div>
														<div class="col-sm-12">
															<img alt="qr-code" src="${qr}">
														</div>
													</c:if>
													<c:if test="${!userAccount.isUsing2fa == true}">
														<div class="col-sm-12 row">
															<div class="col-sm-9">
																<h4><spring:message code='cashflow.userAccount.message8' /></h4>
															</div>
															<div class="col-sm-3">
																<button type="button" value="true" class="btn btn-info btn-sm pull-left btn-toogle-2fa"><span class="glyphicon glyphicon-ok-circle button-margin"></span><spring:message code='cashflow.userAccount.enable' /></button>
															</div>
														</div>
														<div class="col-sm-12">
															<p class="text-info"><spring:message code='cashflow.userAccount.message9' /></p>
														</div>
													</c:if>
												</div>
											</div>
										</c:if>
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-sm-offset-4 col-sm-4">
												<input type="hidden" id="user-account-id" value="${userAccount.id}">
												<button type="button" class="btn btn-danger btn-flat btn-cancel"><span class="glyphicon glyphicon-remove button-margin"></span><spring:message code='cashflow.userAccount.cancel' /></button>
												<button type="button" class="btn btn-primary btn-flat btn-apply"><i class="fa-save fa button-margin"></i><spring:message code='cashflow.userAccount.save' /></button>
												<button type="button" class="btn btn-primary btn-flat btn-reset"><spring:message code='cashflow.userAccount.reset' /></button>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceJS("user-account-detail.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		userAccountDetail.init('${role}');
		userAccountDetail.eventListener();
	});
</script>
</html>