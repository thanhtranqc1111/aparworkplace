<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
<title><spring:message code='cashflow.import.title' /></title>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="" id="pageName" />
			<%@ include file="../header.jsp"%>
			<!-- page content -->
			<div class="right_col" role="main">
			
			<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3><spring:message code='cashflow.import.namePage' /></h3>
						</div>
					</div>
				</div>
				
              	<!-- upload form -->
				<form:form id="uploadForm" modelAttribute="uploadForm" action="import.upload" method="POST" 
							class="form-horizontal" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<div>
											<div class=" upload-file ">
												<p for="concept" class="col-xs-1 control-label"><spring:message code='cashflow.import.uploadFile' /></p>
												<div class="col-xs-6">
													<div class="input-group">
														<label class="input-group-btn"> 
														<span class="btn btn-primary"> <spring:message code='cashflow.import.browse' />
															<input type="file" name="file" style="display: none;">
														</span>
														</label> <input type="text" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-2">
													<button type="submit" class="btn btn-primary">
														<i class="fa fa-upload button-margin"></i><spring:message code='cashflow.import.upload' />
													</button>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</form:form>
				<div class="clearfix"></div>
				<form:form id="importForm" modelAttribute="importForm" action="import.do"  method="POST" class="form-horizontal">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.import.selectFile' /></legend>
										<div class="x_content">
											<div class="table-import ">
												<table id="import-page"class="table  table-bordered dt-responsive nowrap"
													cellspacing="0" width="100%">
													<thead>
														<tr>
															<th><spring:message code='cashflow.import.select' /></th>
															<th><spring:message code='cashflow.import.name' /></th>
															<th><spring:message code='cashflow.import.fileSize' /></th>
															<th><spring:message code='cashflow.import.uploadedDate' /></th>
															<th><spring:message code='cashflow.import.action' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${fileImportDetails}" var="detail" varStatus="status">
															<tr>
																<td class="text-center"><input type="radio" name="fileName" value="${detail.fileName}"></td>
																<td><a href="import.download?fileName=${detail.fileName }">${detail.fileName }</a></td>
																<td>${detail.fileSize }KB</td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${detail.uploadedDate.time}" /></td>
																<td class="text-center"><a href="import.delete?fileName=${detail.fileName}"><i class="fa-trash fa"></i></a></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.import.selectType' /></legend>
										<div class="x_content">
											<div class="table-import select-type">
												<table class="table table-bordered">
													<thead></thead>
													<tbody>
														<tr>
															<td><input type="radio" name="mode" value="MERGE"
																checked="checked"></td>
															<td><spring:message code='cashflow.import.insert' /></td>
															<td class="text-left"><spring:message code='cashflow.import.insert.des' /></td>
														</tr>
														<c:if test="${!fn:contains(requestScope['javax.servlet.forward.request_uri'], 'bankStatement/import')}">
															<tr>
																<td><input type="radio" name="mode" value="DELETE"></td>
																<td><spring:message code='cashflow.import.delete' /></td>
																<td class="text-left"><spring:message code='cashflow.import.delete.des' /></td>
															</tr>
														</c:if>

													</tbody>
												</table>
												<div class=" pull-right">
													<a href="list" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-remove button-margin"></span><spring:message code='cashflow.import.cancel' /></a>
													<button type="submit" class="btn btn-primary button-margin  pull-right">
														<i class="glyphicon glyphicon-import"></i> <spring:message code='cashflow.import.import' />
													</button>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.import.importLogFile' /></legend>
										<div class="x_content">
											<div class="">
												<table id="import-log" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
													<thead>
														<tr>
															<th><spring:message code='cashflow.import.logFile' /></th>
															<th><spring:message code='cashflow.import.importDate' /></th>
															<th><spring:message code='cashflow.import.action' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${logFileDetails}" var="logDetail"
															varStatus="status">
															<tr>
																<td><a href="import.downLog?fileName=${logDetail.logName }">${logDetail.logName }</a></td>
																<td class="text-right"><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${logDetail.importedDate.time}" /></td>
																<td class="text-center"><a href="import.deleteLog?fileName=${logDetail.logName}"><i class="fa-trash fa"></i></a></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>

				</form:form>

			</div>
			<script>
				var crURL = $('.breadcrumb').find('li').first().next().find('a').attr('href');
			    var pageName = $('#sidebar-menu').find("a[href='" + crURL + "']").parents('li').attr('title');
			    $('#pageName').val(pageName);
			</script>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceCSS("import.css")); %>
<% out.println(getReferenceJS("import.js")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	<c:if test="${not empty message}">
		 showPopupMessage("${message}");
	</c:if>
	<c:if test="${not empty activeMessage}">
		showPopupMessage("${activeMessage}", function(){});
	</c:if>
	
	ImportExcel.init();
	ImportExcel.displayData();
});
</script>
</html>