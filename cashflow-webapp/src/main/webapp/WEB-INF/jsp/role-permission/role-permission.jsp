<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code='cashflow.rolePermission.title' /></title>

<style type="text/css">
#multi-table thead tr:first-child th:last-child{
	text-align: center;
	background: #158eb3; color: #fff;
}
#multi-table thead tr:last-child th:first-child{
	background: #158eb3; color: #fff;
}
#multi-table thead tr:last-child th {
	text-align: center;
	background: rgba(228, 240, 255, 0.71);;
}

#multi-table tbody tr td:first-child {
	background: rgba(228, 240, 255, 0.71);;
}
#multi-table td{
	text-align: center;
}
#multi-table thead tr:last-child th {
    text-align: center;
    background: rgba(228, 240, 255, 0.71);
}
</style>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="../header.jsp"%>
			<input type="hidden" value="role & permission" id="pageName"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation">
										<a href="${pageContext.request.contextPath}/role/list" role="tab"><spring:message code='cashflow.rolePermission.role' /></a>
									</li>
									<li role="presentation">
										<a href="${pageContext.request.contextPath}/permission/list" role="tab"><spring:message code='cashflow.rolePermission.permission' /></a>
									</li>
									<li role="presentation" class="active">
										<a href="#" role="tab"><spring:message code='cashflow.rolePermission.grantPermission' /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_content">
								<div class="row ap-search">
									<table id="multi-table" class="table table-bordered">
										<thead>
											<tr>
												<td></td>
												<th colspan="${fn:length(roles)}"><spring:message code='cashflow.rolePermission.role' /></th>
											</tr>
											<tr>
												<th><spring:message code='cashflow.rolePermission.permission' /></th>
												<c:forEach items="${roles}" var="role">
													<td style="background: rgba(228, 240, 255, 0.71); font-weight: bold;">${role.name}</td>
												</c:forEach>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${permissions}" var="permission">
												<tr>
													<td>${permission.name}</td>
													<c:forEach items="${roles}" var="role">
														<c:set var="key">${role.id},${permission.id}</c:set>
														<c:set var="value">${rolePermissionMap[key]}</c:set>
														<td style="text-align: center;">
															 <c:choose>
																<c:when test="${value > 0}">
																	<input roleId="${role.id}" permissionId="${permission.id}" type="checkbox" name="role-permission" checked>
																</c:when>
																<c:otherwise>
																	<input roleId="${role.id}" permissionId="${permission.id}" type="checkbox" name="role-permission">
																</c:otherwise>
															 </c:choose>
														</td>
													</c:forEach>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								<div class="row pull-right">
									<div class="col-xs-12">
									<div class="row">
										<button type="button"
											class="btn btn-primary btn-flat btn-save-all disabled"><i class="fa-save fa button-margin"></i><spring:message code='cashflow.rolePermission.save' /></button>
										<button type="button" class="btn btn-primary btn-flat btn-reset"><spring:message code='cashflow.rolePermission.reset' /></button>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>

<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<% out.println(getReferenceJS("role-permission.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		rolePermission.init();
		rolePermission.eventListener();
	});
</script>
</html>