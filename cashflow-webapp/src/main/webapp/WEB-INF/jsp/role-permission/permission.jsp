<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.permission.title' /></title>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="../header.jsp"%>
			<input type="hidden" value="role & permission" id="pageName"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_content">
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation">
										<a href="${pageContext.request.contextPath}/role/list" role="tab"><spring:message code='cashflow.rolePermission.role' /></a>
									</li>
									<li role="presentation" class="active">
										<a href="#" role="tab"><spring:message code='cashflow.rolePermission.permission' /></a>
									</li>
									<li role="presentation" class="">
										<a href="${pageContext.request.contextPath}/grantPermission/list" role="tab"><spring:message code='cashflow.rolePermission.grantPermission' /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="row">
								<div class="col-xs-6">
									<div class="col-xs-1">
										<h5 class="pull-right"><spring:message code='cashflow.permission.name' /></h5>
									</div>
									<div class="col-xs-4">
										<input id="txt-search" type="text" class="form-control" />
									</div>
									<div class="col-xs-2">
										<button type="button"
											class="btn btn-primary btn-flat btn-search pull-left">
											<i class="fa fa-search button-margin"></i><spring:message code='cashflow.permission.search' />
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_content">
								<div class="row ap-search">
									<table id="table_data"
										class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><input name="select_all" value="1" type="checkbox"></th>
												<th><spring:message code='cashflow.permission.code' /></th>
							                  	<th><spring:message code='cashflow.permission.name' /></th>
							                  	<th><spring:message code='cashflow.permission.description' /></th>
											</tr>
										</thead>
									</table>
								</div>
								<div class="row pull-right">
									<div class="col-xs-12">
										<div class="row">
											<button type="button"
												class="btn btn-success btn-flat btn-new">
												<i class="fa fa-plus-square button-margin"></i><spring:message code='cashflow.permission.newPermission' />
											</button>
											<button type="button"
												class="btn btn-danger btn-flat btn-delete disabled">
												<i class="fa-trash fa button-margin"></i><spring:message code='cashflow.permission.delete' />
											</button>
											<button type="button"
												class="btn btn-primary btn-flat btn-save-all disabled">
												<i class="fa-save fa button-margin"></i><spring:message code='cashflow.permission.save' />
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>

<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<% out.println(getReferenceJS("permission.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		permissionList.init();
		permissionList.eventListener();
	});
</script>

</html>