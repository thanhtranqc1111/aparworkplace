<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<title><spring:message code='cashflow.payroll.detail.title' /></title>

<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="payroll" id="pageName" />
			<input id="status" name="status" type="hidden" value="${payroll.status}">
			<input type="hidden" code="002" id="apr002Value" value="${mapApprovalStatus.get('002')}">
			<input type="hidden" value="${currentSubsidiary.currency}" id="currentCurrency" />
			<input type="hidden" value="${payroll.id}" id="payrollId" />
			<input type="hidden" id="isConfirmChanged" value="true" />
			<c:set var="readonlyStatus" value=""></c:set>
			<c:set var="disableStatus" value=""></c:set>
			<c:set var="disableWhenAddNew" value="disabled"></c:set>
			<c:if test="${payroll.id > 0}">
				<c:set var="readonlyStatus" value="readonly"></c:set>
				<c:set var="disableStatus" value="disabled='disabled'"></c:set>
				<c:set var="disableWhenAddNew" value=""></c:set>
			</c:if>
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						
							<c:if test="${sessionScope.authorities['PMS-002'] and sessionScope.authorities['PMS-009']}">
								<a href="${pageContext.request.contextPath}/payroll/detail" id="new-btn" class="btn btn-success" role="button">
									<i class="fa fa-plus-square button-margin"></i>
									<spring:message code='cashflow.payroll.detail.new' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-003']}">
								<c:choose>
									<c:when test="${payroll.status == '004'}">
										<button id="edit-btn" type="button" class="btn btn-warning disabled">
											<i class="fa  fa-pencil-square-o button-margin"></i><spring:message code='cashflow.payroll.detail.edit' />
										</button>
									</c:when>
									<c:otherwise>
										<button id="edit-btn" type="button" class="btn btn-warning ${disableWhenAddNew}">
											<i class="fa  fa-pencil-square-o button-margin"></i><spring:message code='cashflow.payroll.detail.edit' />
										</button>
									</c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-002']}">
								<button id="btn-clone" type="button" class="btn btn-warning ${disableWhenAddNew}">
									<i class="fa fa-clone button-margin"></i><spring:message code='cashflow.payroll.detail.clone' />
								</button>
							</c:if>
					</div>
				</div>
				<div class="alert-warning" id="alert-warning-payroll"></div>
				<!-- search panel -->
				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form id="form-payroll-header">
										<fieldset>
											<div class="page-header">
												<h4>
													<spring:message code='cashflow.payroll.detail.payrollHeaderInfo' />
													<c:if test="${payroll.id > 0}">
														<small>
															<span class="ap-status api-${payroll.status} ap-status-header ap-label-status">${payroll.statusVal}</span>
															<c:if test="${payroll.id > 0}">
																<div class="dropdown ap-dropdown-status">
																	<button class="ap-status api-${payroll.status} ap-status-header" data-toggle="dropdown">
																		<span class="status-label">${payroll.statusVal}</span>
																		<span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu dropdown-menu-ap-status">
																		<li><a href="#" status="004">${mapPayrollStatus.get('004')}</a></li>
																	</ul>
																</div>
															</c:if>
														</small>
													</c:if>
												</h4>
											</div>
											<div class="row">
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.payroll.detail.payrollNo' /></label>
															<div class="col-sm-10">
																<input id="txt-payrollNo" readonly class="form-control alway-readonly" value="${payroll.payrollNo}" />
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.payroll.detail.invoiceNo' /></label>
															<div class="col-sm-10">
																<input id="txt-invoiceNo" class="form-control" value="${payroll.invoiceNo}"  ${readonlyStatus} />
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.payroll.detail.claimType' /></label>
															<div class="col-sm-10">
																<select id="select-claim-type" disabled class="form-control alway-readonly" value="${payroll.claimType}">
																	<c:forEach var="entry" items="${claimTypeMap}">
																		<c:choose>
																			<c:when test="${payroll.claimType == entry.key}">
																				 <option selected value="${entry.key}">${entry.value}</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${entry.key}">${entry.value}</option>
																			</c:otherwise>
																		</c:choose>	
																	</c:forEach>	
																</select>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.payroll.detail.currency' />*</label>
															<div class="col-sm-10">
																<select id="select-currency-type" class="form-control" ${disableStatus} value="${payroll.originalCurrencyCode}">
																	<c:forEach var="entry" items="${currencyMap}">
																		<c:choose>
																			<c:when test="${payroll.originalCurrencyCode == entry.key}">
																				 <option selected value="${entry.key}">${entry.value}</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${entry.key}">${entry.value}</option>
																			</c:otherwise>
																		</c:choose>	
																	</c:forEach>	
																</select>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-2"><spring:message code='cashflow.payroll.detail.fxRate' />*</label>
															<div class="col-sm-10">
																<input id="txt-fx-rate" class="form-control currency-number" value="${payroll.fxRate}"  ${readonlyStatus} />
															</div>
														</div>
													</div>
												</div>
		
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.payroll.detail.month' />*</label>
															<div class="col-sm-8">
																<input class="form-control ap-date-picker-month-format" type="text" id="txt-month" ${readonlyStatus} value="<fmt:formatDate value="${payroll.month.time}" type="date" pattern="MM/yyyy"/>"/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.payroll.detail.bookingdate' />*</label>
															<div class="col-sm-8">
																<input class="form-control ap-date-picker" type="text" id="txt-booking-date" ${readonlyStatus} value="<fmt:formatDate value="${payroll.bookingDate.time}" type="date" pattern="dd/MM/yyyy"/>"/>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.payroll.detail.accountPayableCode' />*</label>
															<div class="col-sm-8">
																<input id="txt-account-payable-code" class="form-control" ${readonlyStatus} value="${payroll.accountPayableCode}"/>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-xs-4"><spring:message code='cashflow.payroll.detail.accountPayableName' />*</label>
															<div class="col-sm-8">
																<input readonly id="txt-account-payable-name" class="form-control alway-readonly"  value="${payroll.accountPayableName}"/>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.payroll.detail.totalLaborCostOriginal' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon original-currency-code">${payroll.originalCurrencyCode}</span>
																	<input id="txt-total-labor-cost" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${payroll.totalLaborCostOriginal}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.payroll.detail.totalDeductionOriginal' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon original-currency-code">${payroll.originalCurrencyCode}</span>
																	<input id="txt-deduction" readonly class="form-control currency-number ap-big-decimal alway-readonly" value="${payroll.totalDeductionOriginal}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.payroll.detail.totalNetPaymentOriginal' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon original-currency-code">${payroll.originalCurrencyCode}</span>
																	<input id="txt-net-payment" readonly class="form-control currency-number ap-big-decimal-allow-minus alway-readonly" value="${payroll.totalNetPaymentOriginal}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.payroll.detail.totalNetPaymentConverted' /></label>
															<div class="col-sm-6">
																<div class="input-group non-margin">
																	<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																	<input id="txt-net-payment-converted" readonly class="form-control currency-number ap-big-decimal-allow-minus alway-readonly" value="${payroll.totalNetPaymentConverted}"/>
																</div>
															</div>
														</div>
													</div>
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-6"><spring:message code='cashflow.payroll.detail.scheduledPaymentDate' /></label>
															<div class="col-sm-6">
																<input id="txt-scheduled-payment-date" class="form-control ap-date-picker"  ${readonlyStatus} value="<fmt:formatDate value="${payroll.paymentScheduleDate.time}" type="date" pattern="dd/MM/yyyy"/>"/>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8 col-sm-8 col-xs-8">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="control-label col-xs-1"><spring:message code='cashflow.payroll.detail.description' /></label>
															<div class="col-sm-11">
																<textarea style="resize: none;"  ${readonlyStatus} id="txt-description" rows="6" cols="50" class="form-control">${payroll.description}</textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>							
						</div>
					</div>
					<!-- list panel -->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.payroll.detail.payrollDetails' /></legend>
										<ul id="alertApprovalStatus"></ul>
										<br />
										<div class="table-scroll">
											<table id="payroll-table-data" class="table table-striped table-bordered table-hover">
												<thead>
													 <tr>
													  	<th rowspan="2"><spring:message code='cashflow.payroll.detail.employeeID' /></th>
													  	<th rowspan="2"><spring:message code='cashflow.payroll.detail.roleTitle' /></th>	
													  	<th rowspan="2"><spring:message code='cashflow.payroll.detail.division' /></th>	
													  	<th rowspan="2"><spring:message code='cashflow.payroll.detail.team' /></th>	
													  	<th rowspan="2"><spring:message code='cashflow.payroll.detail.name' /></th>	
													    <th colspan="11"><spring:message code='cashflow.payroll.detail.laborcost' /></th>
													    <th colspan="11"><spring:message code='cashflow.payroll.detail.deductions' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.netpaymentOriginal' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.fxRate' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.netpaymentConverted' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.approvalNo' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.status' /></th>
													    <th rowspan="2"><spring:message code='cashflow.payroll.detail.action' /></th>
													  </tr>
													  <tr>
													  	<!-- Labor cost -->
													    <th><spring:message code='cashflow.payroll.detail.baseSalaryPayment' /></th>
													    <th><spring:message code='cashflow.payroll.detail.additionalPayment1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.additionalPayment2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.additionalPayment3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherPayment1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherPayment2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherPayment3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.totalcostlabor' /></th>
													    <!-- Deductions -->
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployer3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployee1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployee2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.socialinsuranceonEmployee3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.wht' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherDeduction1' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherDeduction2' /></th>
													    <th><spring:message code='cashflow.payroll.detail.otherDeduction3' /></th>
													    <th><spring:message code='cashflow.payroll.detail.deductionTotal' /></th>
													  </tr>
												</thead>
												<tbody>
													<c:forEach var="item" varStatus="status" items="${payroll.payrollDetailsesList}">
														<c:choose>
															<c:when test="${payroll.id == 0}">
																<tr dataid="new-${status.index}">
															</c:when>
															<c:otherwise>
																<tr dataid="${item.id}">
															</c:otherwise>
														</c:choose>
															<td><input type="text" class="form-control txt-employee-id" value="${item.employeeCode}" ${readonlyStatus}></td>
															<td><input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.roleTitle}" ${readonlyStatus}></td>
															<td><input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.division}" ${readonlyStatus}></td>
															<td><input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.team}" ${readonlyStatus}></td>
															<td><input type="text" class="form-control alway-readonly" readonly="readonly" value="${item.name}" ${readonlyStatus}></td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-base-salary-payment" value="${item.laborBaseSalaryPayment}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-additional-payment-1" value="${item.laborAdditionalPayment1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-additional-payment-2" value="${item.laborAdditionalPayment2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-additional-payment-3" value="${item.laborAdditionalPayment3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-social-insurance-on-employer-1" value="${item.laborSocialInsuranceEmployer1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-social-insurance-on-employer-2" value="${item.laborSocialInsuranceEmployer2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-social-insurance-on-employer-3" value="${item.laborSocialInsuranceEmployer3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-other-payment-1" value="${item.laborOtherPayment1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-other-payment-2" value="${item.laborOtherPayment2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-other-payment-3" value="${item.laborOtherPayment3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-la-total-cost-labor alway-readonly" readonly="readonly" value="${item.totalLaborCostOriginal}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employer-1" value="${item.deductionSocialInsuranceEmployer1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employer-2" value="${item.deductionSocialInsuranceEmployer2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employer-3" value="${item.deductionSocialInsuranceEmployer3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employee-1" value="${item.deductionSocialInsuranceEmployee1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employee-2" value="${item.deductionSocialInsuranceEmployee2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-social-insurance-on-employee-3" value="${item.deductionSocialInsuranceEmployee3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-wht" value="${item.deductionWht}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-other-deduction-1" value="${item.deductionOther1}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-other-deduction-2" value="${item.deductionOther2}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-de-other-deduction-3" value="${item.deductionOther3}" ${readonlyStatus}>
															</td>
															<td>
																<input type="text" class="form-control ap-big-decimal txt-deduction-total alway-readonly" readonly="readonly" value="${item.totalDeductionOriginal}" ${readonlyStatus}>
															</td>
															<td>
																<div class="input-group">
																	<span class="input-group-addon original-currency-code">${payroll.originalCurrencyCode}</span>
																	<input type="text" class="form-control ap-big-decimal-allow-minus txt-net-payment alway-readonly" readonly="readonly" value="${item.netPaymentOriginal}" ${readonlyStatus}>
																</div>
															</td>
															<td><input type="text" class="form-control txt-re-fx-rate alway-readonly" readonly="readonly" value="${payroll.fxRate}"></td>
															<td>
																<div class="input-group">
																	<span class="input-group-addon">${currentSubsidiary.currency}</span>
																	<input type="text" class="form-control ap-big-decimal-allow-minus txt-net-payment-converted alway-readonly" readonly="readonly" value="${item.netPaymentConverted}" ${readonlyStatus}>
																</div>
															</td>
															<td>
																<c:choose>
																	<c:when test="${empty fn:trim(item.approvalCode)}">
																		<input type="text" class="form-control txt-approval-code" value="${item.approvalCode}" ${readonlyStatus}>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="form-control txt-approval-code" approvalCode ="${item.approvalCode}" amount="${item.approvalIncludeGstOriginalAmount}" value="${item.approvalCode}" ${readonlyStatus}>
																	</c:otherwise>
																</c:choose>
															</td>
															<td>
																<div class="ap-status apr-${item.apprStatusCode} ap-detail">${item.apprStatusVal}</div>
															</td>
															<td><span><span class="btn-delete-payment-detail-row glyphicon glyphicon-trash"></span></span></td>
														</tr>		
													</c:forEach>	
												</tbody>
											</table>
										</div>
									</fieldset>
									<div class="col-sm-12 m-t">
										<div class="row panel-bottom-buttons">
											<button id="btn-add-row" type="button" class="btn btn-success" ${disableStatus}>
												<i class="fa fa-plus-square button-margin"></i>
												<spring:message code='cashflow.payroll.detail.add' />
											</button>
											<button id="btn-copy" type="button" class="btn btn-success disabled" ${disableStatus}>
												<i class="fa fa-copy button-margin"></i>
												<spring:message code='cashflow.payroll.detail.copy' />
											</button>
											<button id="btn-load-all-emp" type="button" class="btn btn-success" ${disableStatus}>
												<spring:message code='cashflow.payroll.detail.loadAllemployee' />
											</button>
											<div class="pull-right">
												<button id="btn-cancel" class="btn btn-danger" type="button" ${disableStatus}>
													<i class="glyphicon glyphicon-remove button-margin"></i>
													<spring:message code='cashflow.payroll.detail.cancel' />
												</button>
												<c:if test="${sessionScope.authorities['PMS-002'] || sessionScope.authorities['PMS-003']}">
													<button id="btn-save" class="btn btn-primary" ${disableStatus}> 
														<i class="fa fa-save button-margin"></i>
														<spring:message code='cashflow.payroll.detail.save' />
													</button>
												</c:if>
												<button id="btn-reset-all" class="btn btn-primary" ${disableStatus}>
													<spring:message code='cashflow.payroll.detail.reset' />
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("payroll-detail.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.inputmask.bundle.js"></script>
<% out.println(getReferenceJS("payroll-detail.js")); %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.floatThead.js"></script>
<script>
	<c:forEach var="entry" items="${mapPayrollStatus}">
		payRollDetail.PAYROLL_STATUSES["${entry.key}"] = "${entry.value}";
	</c:forEach>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";	
	$(document).ready(function() {
		payRollDetail.initData(${payroll.toJSON()});
		payRollDetail.init();
		payRollDetail.eventListener();
		payRollDetailDataTable.init();
		payRollDetailDataTable.eventListener();
	});
</script>
</html>