<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code='cashflow.approvalTypePurchaseMaster.title' /></title>

<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
</head>
<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="approval type purchase master" id="pageName" />
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="page-header">
						<h3><spring:message code='cashflow.approvalTypePurchaseMaster.header' /></h3>
					</div>
				</div>
			</div>
			
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						
							<c:if test="${sessionScope.authorities['PMS-002']}">
								<button type="button" class="btn btn-success btn-flat"
									onclick="approvalTypeMaster.newApprovalTypeMaster()">
									<i class="fa fa-plus-square button-margin"></i> <spring:message code='cashflow.approvalTypePurchaseMaster.newApprovalTypeMaster' />
								</button>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="downTemplate" class="btn btn-primary btn-flat"><i
									class="fa fa-download button-margin"></i><spring:message code='cashflow.approvalTypePurchaseMaster.downloadTemplate' /></a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="import" class="btn btn-primary btn-flat"><i
									class="glyphicon glyphicon-import button-margin"></i><spring:message code='cashflow.approvalTypePurchaseMaster.import' /></a>
							</c:if>
						
					</div>
				</div>
				<!-- list panel -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
						
							
								<table id="table_data"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th></th>
											<th><spring:message code='cashflow.approvalTypePurchaseMaster.code' /></th>
											<th><spring:message code='cashflow.approvalTypePurchaseMaster.name' /></th>
											<th><spring:message code='cashflow.approvalTypePurchaseMaster.action' /></th>
										</tr>
									</thead>
								</table>
							
							</div>
						</div>
					</div>
				</div>
				<!-- detail panel -->
				<c:if
					test="${sessionScope.authorities['PMS-002'] or sessionScope.authorities['PMS-003']}">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
							<div class="panel-body">
									<form:form name='f' action=""
										commandName="approvalTypeMasterForm" method="POST"
										enctype="multipart/form-data" accept-charset="utf-8">
										<c:set var="domainNameErrors">
											<form:errors path="" />
										</c:set>
										<div class="admin-form-header">
											<legend class="the-legend"><spring:message code='cashflow.approvalTypePurchaseMaster.approvalTypeMasterDetail' /></legend>
										</div>
										<div class="box-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="require"><spring:message code='cashflow.approvalTypePurchaseMaster.code' />*</label>
														<form:input readonly="true" class="form-control" path="code" />
                                                         <p id="code-error" class="input-error error"></p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="require"><spring:message code='cashflow.approvalTypePurchaseMaster.name' />*</label>
														<form:input readonly="true" class="form-control" path="name" />
                                                         <p id="name-error" class="input-error error"></p>
													</div>
												</div>
											</div>
											<div class="box-footer">
												<button type="button"
													class="btn btn-primary btn-flat disabled pull-right" name="saveButton"
													onclick="approvalTypeMaster.save()"><i class="fa fa-save button-margin"></i><spring:message code='cashflow.approvalTypePurchaseMaster.save' /></button>
												<input type="hidden" name="mode" id="mode" value="New" />
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</div>

		</div>
		<%@ include file="../footer.jsp"%>
	</div>
</body>
<%
    out.println(getReferenceCSS("master-data.css"));
    out.println(getReferenceCSS("error.css"));
%>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/util.js"></script>
<% out.println(getReferenceJS("approval-type-purchase.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		approvalTypeMaster.init();
		approvalTypeMaster.eventListener();
	});
</script>