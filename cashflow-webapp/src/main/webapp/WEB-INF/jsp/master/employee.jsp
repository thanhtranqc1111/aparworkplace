<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code='cashflow.employeeMaster.title' /></title>

<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
</head>
<body class="nav-md">
    <!-- popup confirm -->
    <%@ include file="../common/boostrap_modal.jsp"%>
    <div class="container body">
        <div class="main_container">
            <input type="hidden" value="employee master" id="pageName" />
            <%@ include file="../header.jsp"%>
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="page-header">
                            <h3>
                                <spring:message code='cashflow.employeeMaster.header' />
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">

                        <c:if test="${sessionScope.authorities['PMS-002']}">
                            <button type="button" class="btn btn-success btn-flat" onclick="employeeMaster.newEmployeeMaster()">
                                <i class="fa fa-plus-square button-margin"></i>
                                <spring:message code='cashflow.employeeMaster.newEmployeeMaster' />
                            </button>
                        </c:if>
                        <c:if test="${sessionScope.authorities['PMS-005']}">
                            <a href="downTemplate" class="btn btn-primary btn-flat"><i class="fa fa-download button-margin"></i> <spring:message
                                    code='cashflow.employeeMaster.downloadTemplate' /></a>
                        </c:if>
                        <c:if test="${sessionScope.authorities['PMS-005']}">
                            <a href="import" class="btn btn-primary btn-flat"><i class="glyphicon glyphicon-import button-margin"></i> <spring:message
                                    code='cashflow.employeeMaster.import' /></a>
                        </c:if>

                    </div>
                </div>
                <!-- list panel -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <table id="table_data" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><spring:message code='cashflow.employeeMaster.code' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.divisionName' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.name' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.roleTitle' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.team' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.joinDate' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.resignDate' /></th>
                                            <th><spring:message code='cashflow.employeeMaster.action' /></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- detail panel -->
                <c:if test="${sessionScope.authorities['PMS-002'] or sessionScope.authorities['PMS-003']}">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form:form name='f' action="" commandName="employeeMasterForm" method="POST" enctype="multipart/form-data"
                                        accept-charset="utf-8">
                                        <c:set var="domainNameErrors">
                                            <form:errors path="" />
                                        </c:set>
                                        <div class="admin-form-header">
                                            <legend class="the-legend">
                                                <spring:message code='cashflow.employeeMaster.employeeMasterDetail' />
                                            </legend>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="require"><spring:message code='cashflow.employeeMaster.code' />*</label>
                                                        <form:input readonly="true" class="form-control" path="code" />
                                                         <p id="code-error" class="input-error error"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="require"><spring:message code='cashflow.employeeMaster.divisionName' /></label>
                                                        <form:select path="divisionMaster.code" class="form-control" id="divisionMaster">
                                                           <form:option value="" label=""/>
                                                           <form:options items="${divisionMasterList}" itemValue="code" itemLabel="name"/>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="require"><spring:message code='cashflow.employeeMaster.name' />*</label>
                                                        <form:input readonly="true" class="form-control" path="name" />
                                                         <p id="name-error" class="input-error error"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="require"><spring:message code='cashflow.employeeMaster.roleTitle' /></label>
                                                        <form:input readonly="true" class="form-control" path="roleTitle" />
                                                        <p id="roleTitle-error" class="input-error error"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="require"><spring:message code='cashflow.employeeMaster.team' /></label>
                                                        <form:input readonly="true" class="form-control" path="team" />
                                                        <p id="team-error" class="input-error error"></p>
                                                    </div>
                                                </div>
												<div class="col-md-3">
													<div class="form-group datepicker">
														<label class="require"><spring:message code='cashflow.employeeMaster.joinDate' /></label>
														<form:input readonly="true" type="text" id="joinDate" path="joinDate"
															class="form-control" />
														<p id="joinDate-error" class="input-error error"></p>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group datepicker">
														<label class="require"><spring:message code='cashflow.employeeMaster.resignDate' /></label>
														<form:input readonly="true" type="text" id="resignDate" path="resignDate"
															class="form-control" />
														<p id="resignDate-error" class="input-error error"></p>
													</div>
												</div>
											</div>

                                            <div class="box-footer">
                                                <button type="button" class="btn btn-primary btn-flat disabled pull-right" name="saveButton"
                                                    onclick="employeeMaster.save()">
                                                    <i class="fa fa-save button-margin"></i>
                                                    <spring:message code='cashflow.employeeMaster.save' />
                                                </button>
                                                <input type="hidden" name="mode" id="mode" value="New" />
                                            </div>
                                        </div>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>

        </div>
        <%@ include file="../footer.jsp"%>
    </div>
</body>
<%
    out.println(getReferenceCSS("master-data.css"));
    out.println(getReferenceCSS("error.css"));
%>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/util.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<%
    out.println(getReferenceJS("employee.js"));
%>
<script>
    var CONTEXT_PATH = "${pageContext.request.contextPath}";
    $(document).ready(function() {
        employeeMaster.init();
        employeeMaster.eventListener();
    });
</script>