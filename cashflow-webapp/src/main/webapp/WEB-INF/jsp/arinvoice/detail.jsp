<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<title><spring:message code='cashflow.arInvoice.detail.titlePage' /></title>

<!-- Import JQuery UI -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" />
</head>

<body class="nav-md">
	<!-- popup confirm -->
	<%@ include file="../common/boostrap_modal.jsp"%>
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="ar invoice" id="pageName"/>
			<%@ include file="../header.jsp"%>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						<c:if test="${sessionScope.authorities['PMS-002']}">
							<a href="${pageContext.request.contextPath}/arinvoice/detail" id="new-btn" class="btn btn-success" role="button">
							<i class="fa fa-plus-square button-margin"></i><spring:message code='cashflow.arInvoice.detail.new' /></a>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-003']}">
							<button id="edit-btn" type="button" class="btn btn-warning">
								<i class="fa  fa-pencil-square-o button-margin"></i><spring:message code='cashflow.arInvoice.detail.edit' />
							</button>
						</c:if>
						<c:if test="${sessionScope.authorities['PMS-002']}">
							<a href="${pageContext.request.contextPath}/arinvoice/copy/${arInvoiceForm.arInvoiceNo}"  class="btn btn-primary" id="copy-btn" role="button">
								<i class="fa fa-clone button-margin"></i> <spring:message code='cashflow.arInvoice.detail.clone' />
							</a>
						</c:if>
					</div>
				</div>
				<form:form id="form1" modelAttribute="arInvoiceForm" action="${pageContext.request.contextPath}/arinvoice/save" method="POST" class="form-horizontal">
					<form:hidden path="id" />
					<input type="hidden" id="isConfirmChanged" value="true"/>
					<input type="hidden" value="${currentSubsidiary.currency}" id="convertedCurrencyCode"/>
					<form:hidden path="status" />
					
					<div class="error-area"></div>
					
					
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<legend>
											<span class="ap-header-title"><spring:message code='cashflow.arInvoice.detail.arHeader' /></span>
											<c:if test="${arInvoiceForm.id > 0}">
												<span class="status ari-${arInvoiceForm.status} ap-status-header ap-label-status">${arInvoiceForm.statusName}</span>
											</c:if>
										</legend>

										<div class="row">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.detail.payerName' />*</label>
														<div class="col-sm-10">
															<form:input type="hidden" path="payerCode" id="payerCode" />
															<form:input class="form-control" type="text" path="payerName" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.detail.arNo' /></label>
														<div class="col-sm-10">
															<form:input path="arInvoiceNo" id="arInvoiceNo" readonly="true" class="form-control" />
															<form:input type="hidden" path="payerTypeCode" />
															<form:input type="hidden" path="payerTypeName" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.detail.invoiceNo' /></label>
														<div class="col-sm-10">
															<form:input path="invoiceNo" id="invoiceNo" class="form-control" name="invoiceNo" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.detail.currency' /></label>
														<div class="col-sm-10">
															<form:select path="originalCurrencyCode" class="form-control">
																<form:options items="${currencyMap}" />
															</form:select>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.detail.fxRate' />*</label>
														<div class="col-sm-10">
															<form:input path="fxRate" id="fxRate" name="fxRate" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.arInvoice.detail.invoiceIssuedDate' />*</label>
														<div class="col-sm-8">
															<form:input type="text" path="invoiceIssuedDate" id="invoiceIssuedDate" class="form-control" />
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.arInvoice.detail.claimType' /></label>
														<div class="col-sm-8">
															<form:select path="claimType" class="form-control">
																<form:options items="${claimTypeMap}" />
															</form:select>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.arInvoice.detail.accountReceivableCode' /></label>
														<div class="col-sm-8">
															<form:input path="accountReceivableCode" id="accountReceivableCode" class="form-control" list="listAccountMaster" />
															<datalist id="listAccountMaster">
																<c:forEach items="${listAccountMaster}" var="item">
																	<option name="${item.name}" value="${item.code}">${item.name}</option>
																</c:forEach>
															</datalist>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-4"><spring:message code='cashflow.arInvoice.detail.accountReceivableName' /></label>
														<div class="col-sm-8">
															<form:input path="accountReceivableName" id="accountReceivableName" readonly="true"  class="form-control"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.totalAmountExcludedGstOriginal' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon exclude-gst-original-currency"></span>
																<form:input path="excludeGstOriginalAmount" id="excludeGstOriginalAmount" readonly="true" class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.totalAmountExcludedGstConverted' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="excludeGstConvertedAmount" id="excludeGstConvertedAmount" readonly="true"
																	class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.gstAmountOriginal' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon exclude-gst-original-currency"></span>
																<form:input path="gstOriginalAmount" id="gstOriginalAmount" readonly="true" class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.gstAmountConverted' /></label>
														<div class="col-sm-6">
															<form:hidden path="gstOriginalAmount" />
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="gstConvertedAmount" id="gstConvertedAmount" readonly="true" class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.totalAmountIncludeGstOriginal' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon exclude-gst-original-currency"></span>
																<form:input path="includeGstOriginalAmount" id="includeGstOriginalAmount"
																readonly="true" class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8 col-sm-8 col-xs-8">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-1"><spring:message code='cashflow.arInvoice.detail.description' /></label>
														<div class="col-sm-11">
															<form:textarea path="description" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="control-label col-xs-6"><spring:message code='cashflow.arInvoice.detail.totalAmountIncludeGstConverted' /></label>
														<div class="col-sm-6">
															<div class="input-group non-margin">
																<span class="input-group-addon ">${currentSubsidiary.currency}</span>
																<form:input path="includeGstConvertedAmount" id="includeGstConvertedAmount"
																readonly="true" class="form-control currency-number" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<fieldset>
										<legend><spring:message code='cashflow.arInvoice.detail.invoiceDetail' /></legend>
										
										<!-- this list for status of approval -->
										<ul id="alertApprovalStatus"></ul><br/>
										<div class="detail-area detail-invoice-table-wrapper" style="width: 2300px;">
												<table id="detail-invoice-table" class="display table table-bordered dataTable no-footer"
													style="width: 100%">
													<thead>
														<tr>
															<th><spring:message code='cashflow.arInvoice.detail.accCode' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.accountName' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.prjCode' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.prjName' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.approvalNo' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.description' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.amountExcludeGstOriginal' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.fxRate' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.amountExcludeGstConverted' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.gstType' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.gstRate' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.gstAmountConverted' /></th>
															<th><spring:message code='cashflow.arInvoice.detail.amountIncludeGstConverted' /></th>
															<th class="status-header"><spring:message code='cashflow.arInvoice.detail.status' /></th>
															<th><spring:message code='cashflow.apInvoice.detail.action' /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${arInvoiceForm.detailArForms}" var="detail" varStatus="status">
															<c:choose>
																<c:when test="${detail.approvalCode == approvalCode}">
																	<tr style="background: #bdd7ec !important;">
																</c:when>
																<c:otherwise>
																	<tr>
																</c:otherwise>
															</c:choose>
																<input type="hidden" id="arInvoiceAccountDetailId-${status.index}" name="detailArForms[${status.index}].arInvoiceAccountDetailId" value="${detail.arInvoiceAccountDetailId}">
																<input type="hidden" id="arInvoiceClassDetailId-${status.index}" name="detailArForms[${status.index}].arInvoiceClassDetailId" value="${detail.arInvoiceClassDetailId}">
																<td>
																	<input type="text" name="detailArForms[${status.index}].accountCode" value="${detail.accountCode}" id="invoice-account-row-detail-${status.count-1}" class="form-control">
																</td>
																<td>
																	<input type="text" name="detailArForms[${status.index}].accountName" value="${detail.accountName}" id="invoice-account-name-row-detail-${status.count-1}" class="form-control">
																</td>
																<td>
																	<input type="text" name="detailArForms[${status.index}].classCode" value="${detail.classCode}" id="invoice-project-row-detail-${status.count-1}" class="form-control">
																</td>
																<td>
																	<input type="text" name="detailArForms[${status.index}].className" value="${detail.className}" id="invoice-project-name-row-detail-${status.count-1}" class="form-control">
																</td>
																<td>
																	<form:input path="detailArForms[${status.index}].approvalCode" class="form-control" id="approval-code-${status.count-1}"/>
																</td>
																<td>
																	<form:input path="detailArForms[${status.index}].description" class="form-control" id="description-${status.count-1}"/>
																</td>
																<td>
																	<div class="input-group">
																		<span class="input-group-addon inv-currency"></span>
																		<input type="text" name="detailArForms[${status.index}].excludeGstOriginalAmount" id="exclude-gst-original-amount-${status.count-1}" value="${detail.excludeGstOriginalAmount}" id="exclude-gst-original-amount-${status.count-1}" class="form-control">
																	</div>
																</td>
																<td>
																	<input type="text" name="detailArForms[${status.index}].fxRate" id="fx-rate-${status.count-1}" value="${detail.fxRate}" class="form-control fx-rate" >
																</td>
																<td>
																	<div class="input-group">
																		<span class="input-group-addon sub-currency"></span>
																		<input type="text" name="detailArForms[${status.index}].excludeGstConvertedAmount" id="exclude-gst-converted-amount-${status.count-1}" value="${detail.excludeGstConvertedAmount}"class="form-control">
																	</div>
																</td>
																<td>
																	<select name="detailArForms[${status.index}].gstType" id="gst-type-${status.count-1}" class="form-control gst-type-input">
																		<c:forEach items="${listGstMaster}" var="item">
																			<c:choose>
																				<c:when test="${item.type == detail.gstType}">
																					<option rate="${item.rate}" value="${item.type}" selected>${item.type}</option>
																				</c:when>
																				<c:otherwise>
																					<option rate="${item.rate}" value="${item.type}">${item.type}</option>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</select> 	
																</td>
																<td>
																	<input type="text" name="detailArForms[${status.index}].gstRate" id="gst-rate-${status.count-1}" value="${detail.gstRate}" class="form-control">
																</td>
																<td>
																	<input type="hidden" name="detailArForms[${status.index}].gstOriginalAmount" id="gst-original-amount-${status.count-1}" value="${detail.gstOriginalAmount}">
																	<div class="input-group">
																		<span class="input-group-addon sub-currency"></span>
																		<input type="text" name="detailArForms[${status.index}].gstConvertedAmount" id="gst-converted-amount-${status.count-1}" value="${detail.gstConvertedAmount}" class="form-control">
																	</div>
																</td>
																<td>
																	<input type="hidden" name="detailArForms[${status.index}].includeGstOriginalAmount" id="include-gst-original-amount-${status.count-1}" value="${detail.includeGstOriginalAmount}">
																	<div class="input-group">
																		<span class="input-group-addon sub-currency"></span>
																		<input type="text" name="detailArForms[${status.index}].includeGstConvertedAmount" id="include-gst-converted-amount-${status.count-1}" value="${detail.includeGstConvertedAmount}" class="form-control">
																	</div>
																</td>
																<td>
																	<div class="ap-status apr-${detail.isApprovalCode} ap-detail">${detail.isApprovalStatus}</div>
																</td>
																<td>
																	<span id="action-del-${status.count-1}" class="action-del glyphicon glyphicon-trash"></span>
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
									</fieldset>
									
									
									<div class="col-sm-12 m-t">
										<div class="row">
											<button id="add-row-detail-invoice-btn" type="button" class="btn btn-success">
												<i class="fa fa-plus-square button-margin"></i> <spring:message code='cashflow.apInvoice.detail.add' />
											</button>
											<button id="copy-row-detail-invoice-btn" type="button" class="btn btn-success">
												<i class="fa  fa-copy button-margin"></i> <spring:message code='cashflow.apInvoice.detail.copy' />
											</button>
											<div class="pull-right">
												<button  id="btn-cancel" class="btn btn-danger" type="button">
													<i class="glyphicon glyphicon-remove button-margin"></i><spring:message code='cashflow.apInvoice.detail.cancel' />
												</button>
												<c:if test="${sessionScope.authorities['PMS-002'] || sessionScope.authorities['PMS-003']}">
													<a href="#"  id="save-btn" onClick = "ARInvoiceDetail.validationForm();" class="btn btn-primary">
														<i class="fa fa-save button-margin"></i><spring:message code='cashflow.apInvoice.detail.save' />
													</a>
												</c:if>
												<input type="button" id="btn-reset" class="btn btn-primary" value="<spring:message code='cashflow.apInvoice.detail.reset' />">
											</div>
										</div>
									</div>
								
									
								</div>
								
							</div>
						</div>
					</div>
				</form:form>
			</div>
			<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
		            
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title" id="myModalLabel"><spring:message code='cashflow.arInvoice.detail.confirm' /></h4>
		                </div>
		            
		                <div class="modal-body">
		                    <p><spring:message code='cashflow.arInvoice.detail.messageConfirm' /></p>
		                </div>
		                
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code='cashflow.arInvoice.detail.cancel' /></button>
		                    <a class="btn btn-danger btn-deleted"><spring:message code='cashflow.arInvoice.detail.delete' /></a>
		                </div>
		            </div>
		        </div>
		    </div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
	<select id="gst-master-select-hidden" hidden="true">
		<c:forEach items="${listGstMaster}" var="item">
			<c:choose>
				<c:when test="${item.type == detail.gstType}">
					<option rate="${item.rate}" value="${item.type}" selected>${item.type}</option>
				</c:when>
				<c:otherwise>
					<option rate="${item.rate}" value="${item.type}">${item.type}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
	<input type="hidden" code="${apr002.code}" id="apr002Value" value="${apr002.value}">
	<input type="hidden" id="isCopy" value="${isCopy}">
</body>

<% out.println(getReferenceCSS("ar-invoice.css")); %>
<script src="${pageContext.request.contextPath}/resources/js/moment/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<% out.println(getReferenceJS("ar-invoice-detail.js")); %>
<script type="text/javascript">
	var counter = ${arInvoiceForm.detailArForms.size()};
	var modeEdit = ${arInvoiceForm.id > 0};
	var modeCopy = window.location.href.indexOf('arinvoice/copy') > 0;
	
	$(document).ready(function() {
		<c:if test="${not empty message}">
			showPopupSave("${message}", function() {
			   	window.location = "../list";
		    });
		</c:if>
		
		<c:if test="${not empty approvalJson}">
			ARInvoiceDetail.approvalJson = ${approvalJson};
		</c:if>
		
		ARInvoiceDetail.init();
		ARInvoiceDetail.eventListener();
	 	ARInvoiceDetail.addCommasNumber();
	}); 
</script>
</html>