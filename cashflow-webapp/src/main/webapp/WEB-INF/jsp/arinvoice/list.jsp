<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.arInvoice.list.arInvoice' /></title>

<style>
</style>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<input type="hidden" value="ar invoice" id="pageName" />
			<%@ include file="../header.jsp"%>
			<div class="right_col" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h3>
								<spring:message code='cashflow.arInvoice.list.arInvoice' />
							</h3>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">

							<c:if test="${sessionScope.authorities['PMS-002']}">
								<a href="detail" class="btn btn-success" name="action_new_ar">
									<i class="fa fa-plus-square button-margin"></i> <spring:message
										code='cashflow.arInvoice.list.newArInvoice' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="downTemplate" class="btn btn-primary"> <i
									class="fa fa-download button-margin"></i>
								<spring:message code='cashflow.arInvoice.list.downTemplate' />
								</a>
							</c:if>
							<c:if test="${sessionScope.authorities['PMS-005']}">
								<a href="import" class="btn btn-primary"> <i
									class="glyphicon glyphicon-import button-margin"></i>
								<spring:message code='cashflow.arInvoice.list.import' />
								</a>
							</c:if>
						
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- search content -->
				<div class="row ar-search">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.list.month' /></label>
												<div class="col-sm-10 datepicker">
													<div class="col-sm-5">
														<input type="text" class="form-control" id="fromMonth"
															name="fromMonth" />
													</div>
													<div class="col-sm-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-sm-5">
														<input type="text" class="form-control" id="toMonth"
															name="toMonth" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.list.invoiceNo' /></label>
												<div class="col-xs-10">
													<input type="text" class="form-control" id="invoiceNo"
														name="invoiceNo" />
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.list.arNo' /></label>
												<div class="col-sm-10">
													<div class="col-sm-5" style="padding: 0;">
														<input type="text" class="form-control" id="fromArNo"
															name="fromArNo" />
													</div>
													<div class="col-sm-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-sm-5" style="padding: 0;">
														<input type="text" class="form-control" id="toArNo"
															name="toArNo" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.list.payerName' /></label>
												<div class="col-xs-10">
													<select class="form-control" id="payerName">
														<option value=""><spring:message code='cashflow.arinvoice.list.all'/></option>
														<c:forEach items="${lstPayeePayer}" var="item">
															<option value="${item.name}">${item.name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.arInvoice.list.status' /></label>
												<div class="col-xs-10">
													<select class="form-control" id="status">
														<option value=""><spring:message code='cashflow.arinvoice.list.all' /></option>
														<c:forEach items="${mapStatus}" var="item">
															<option value="${item.key}">${item.value}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class=" col-xs-12">
													<div class="pull-right">
														<a id="btn-reset" class="btn btn-primary "><spring:message
																code='cashflow.arInvoice.list.reset' /></a>
														<button type="button" id="btn-search" class="btn btn-primary">
															<i class="fa fa-search button-margin"></i>
															<spring:message code='cashflow.arInvoice.list.search' />
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<!-- ar list  -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
						
						
							<div class="ap-search ap-main-table">
								<table id="table_data_ar"
									class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="9"><spring:message code='cashflow.arInvoice.list.arLedger' /></th>
											<th colspan="6"><spring:message code='cashflow.arInvoice.list.receiptOrder' /></th>
										</tr>
										<tr>
											<th><spring:message code='cashflow.arInvoice.list.arNo' /></th>
											<th><spring:message code='cashflow.arInvoice.list.month' /></th>
											<th><spring:message code='cashflow.arInvoice.list.payerAccount' /></th>
											<th><spring:message code='cashflow.arInvoice.list.claimType' /></th>
											<th><spring:message code='cashflow.arInvoice.list.payerName' /></th>
											<th><spring:message code='cashflow.arInvoice.list.invoiceNo' /></th>
											<th><spring:message code='cashflow.arInvoice.list.totalAmountOriginal' /></th>
											<th><spring:message code='cashflow.arInvoice.list.remainAmountOriginal' /></th>
											<th><spring:message code='cashflow.arInvoice.list.status' /></th>
											<th><spring:message code='cashflow.arInvoice.list.bankTrans' /></th>
											<th><spring:message code='cashflow.arInvoice.list.bankName' /></th>
											<th><spring:message code='cashflow.arInvoice.list.bankAccount' /></th>
											<th><spring:message code='cashflow.arInvoice.list.transDate' /></th>
											<th><spring:message code='cashflow.arInvoice.list.bankRefNo' /></th>
											<th><spring:message code='cashflow.arInvoice.list.creditAmount' /></th>
										</tr>
									</thead>
								</table>
								<div class="col-xs-12">
									<div class="row pull-right">
										<p class="button-bottom">
											<c:if test="${sessionScope.authorities['PMS-006']}">
												<a href="export" id="export" class="btn btn-primary"> <i
													class="glyphicon glyphicon-export button-margin"></i>
												<spring:message code='cashflow.paymentOrder.list.export' /></a>
											</c:if>
										</p>
									</div>
								</div>
							</div>
						
						</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../footer.jsp"%>
		</div>
	</div>
</body>
<% out.println(getReferenceCSS("ar-invoice.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<% out.println(getReferenceJS("ar-invoice-list.js")); %>
<script type="text/javascript">
	$(document).ready(function() {
		ARInvoice.init();
		ARInvoice.eventListener();
	});
</script>
</html>