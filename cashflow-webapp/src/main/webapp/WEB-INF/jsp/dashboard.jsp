<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.dashboard.title' /></title>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="header.jsp"%>
			<input type="hidden" value="dashboard" id="pageName" />
			<!-- page content -->
			<div class="right_col" role="main">
				<!-- top tiles -->
				<div class="row dashboard">
					<div class="col-md-12 col-sm-12 col-xs-12 ">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4><spring:message code='cashflow.dashboard.totalRevenue' /></h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-xs-2"><spring:message code='cashflow.dashboard.month' /></label>
												<div class="col-sm-10 datepicker">
													<div class="col-sm-5">
														<input type="text" class="form-control ap-date-picker-month-format" id="txt-fromMonth" name="fromMonth" />
													</div>
													<div class="col-sm-2" style="text-align: center;">
														<p>~</p>
													</div>
													<div class="col-sm-5">
														<input type="text" class="form-control ap-date-picker-month-format" id="txt-toMonth" name="toMonth" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="alert-warning" id="alert-warning"></div>
								<div id="myChartThreeD"></div>
							</div>
						</div>
					</div>
				</div>


			</div>
			<%@ include file="footer.jsp"%>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/accounting.min.js"></script>
<% out.println(getReferenceJS("dashboard.js")); %>
<script>
	var CONTEXT_PATH = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		dashboard.init();
		dashboard.eventListener();
	});
</script>
</html>