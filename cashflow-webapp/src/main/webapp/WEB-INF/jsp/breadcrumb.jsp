<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<li>
<ol class="breadcrumb">
	<li><a href="${pageContext.request.contextPath}/"><spring:message code='cashflow.common.dashboard' /></a></li>
	<c:set var="CURRENT_URL"
	value="${requestScope['javax.servlet.forward.request_uri']}" />
	<c:choose>
		<c:when test="${fn:contains(CURRENT_URL, 'admin/bankAccountMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/bankAccountMaster/list"><spring:message code='cashflow.common.bankAccountMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/gstMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/gstMaster/list"><spring:message code='cashflow.common.gstMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/claimTypeMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/claimTypeMaster/list"><spring:message code='cashflow.common.claimTypeMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/projectMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/projectMaster/list"><spring:message code='cashflow.common.projectMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/businessTypeMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/businessTypeMaster/list"><spring:message code='cashflow.common.businessTypeMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
				
		<c:when test="${fn:contains(CURRENT_URL, 'admin/payeeTypeMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/payeeTypeMaster/list"><spring:message code='cashflow.common.payeeTypeMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/payeeMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/payeeMaster/list"><spring:message code='cashflow.common.payeeMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/bankMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/bankMaster/list"><spring:message code='cashflow.common.bankMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/currencyMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/currencyMaster/list"><spring:message code='cashflow.common.currencyMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/accountMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/accountMaster/list"><spring:message code='cashflow.common.accountMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/approvalTypePurchaseMaster/import')}">
				<li><a href="${pageContext.request.contextPath}/admin/approvalTypePurchaseMaster/list"><spring:message code='cashflow.common.approvalTypePurchaseMaster' /></a></li>
				<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/approvalTypeSalesMaster/import')}">
			<li><a href="${pageContext.request.contextPath}/admin/approvalTypeSalesMaster/list"><spring:message code='cashflow.common.approvalTypeSalesMaster' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin/employeeMaster/import')}">
            <li><a href="${pageContext.request.contextPath}/admin/employeeMaster/list"><spring:message code='cashflow.common.employeeMaster' /></a></li>
            <li class="active"><spring:message code='cashflow.common.import' /></li>
        </c:when>

        <c:when test="${fn:contains(CURRENT_URL, 'admin/divisionMaster/import')}">
	           <li><a href="${pageContext.request.contextPath}/admin/divisionMaster/list"><spring:message code='cashflow.common.divisionMaster' /></a></li>
	           <li class="active"><spring:message code='cashflow.common.import' /></li>
        </c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'admin')}">
		 	<li class="active"><spring:message code='cashflow.common.masterData' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'apinvoice/list')}">
			<li class="active"><spring:message code='cashflow.common.apInvoice' /></li>
		</c:when>
	
		<c:when test="${fn:contains(CURRENT_URL, 'apinvoice/detail')}">
			<li><a href="${pageContext.request.contextPath}/apinvoice/list" ><spring:message code='cashflow.common.apInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payment/list')}">
			<li class="active"><spring:message code='cashflow.common.paymentOrder' /></li>
		</c:when>
	
		<c:when test="${fn:contains(CURRENT_URL, 'payment/detail')}">
			<li><a href="${pageContext.request.contextPath}/payment/list" ><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payment/action')}">
			<li><a href="${pageContext.request.contextPath}/payment/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>

		<c:when test="${fn:contains(CURRENT_URL, 'approvalPurchaseRequest/list')}">
			<li class="active"><spring:message code='cashflow.common.purchaseRequest' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'bankStatement/list')}">
			<li class="active"><spring:message code='cashflow.common.bankStatement' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payment/import')}">
			<li><a href="${pageContext.request.contextPath}/payment/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'bankStatement/import')}">
			<li><a href="${pageContext.request.contextPath}/bankStatement/list"><spring:message code='cashflow.common.bankStatement' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'userAccount/myProfile')}">
			<li class="active"><spring:message code='cashflow.common.userInformation' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'userAccount/detail') or fn:contains(CURRENT_URL, 'userAccount/addNew')}">
			<li><a href="${pageContext.request.contextPath}/userAccount/list"><spring:message code='cashflow.common.manageUsers' /></a></li>
			<li class="active"><spring:message code='cashflow.common.userInformation' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'userAccount/list')}">
			<li class="active"><spring:message code='cashflow.common.manageUsers' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'role/list') or fn:contains(CURRENT_URL, 'permission/list') or fn:contains(CURRENT_URL, 'grantPermission/list')}">
			<li class="active"><spring:message code='cashflow.common.rolePermission' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'subsidiary/list') or fn:contains(CURRENT_URL, 'subsidiary/grantSub')}">
			<li class="active"><spring:message code='cashflow.common.subsidiary' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'subsidiary/detail')}">
			<li><a href="${pageContext.request.contextPath}/subsidiary/list">Subsidiary</a></li>
			<li class="active"><spring:message code='cashflow.common.createUpdateSubsidiary' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payment/export')}">
			<li><a href="${pageContext.request.contextPath}/payment/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payment/copy')}">
			<li><a href="${pageContext.request.contextPath}/payment/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/apinvoice/import')}">
			<li><a href="${pageContext.request.contextPath}/apinvoice/list"><spring:message code='cashflow.common.apInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/apinvoice/copy')}">
			<li><a href="${pageContext.request.contextPath}/apinvoice/list"><spring:message code='cashflow.common.apInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/apinvoice/export')}">
			<li><a href="${pageContext.request.contextPath}/apinvoice/list"><spring:message code='cashflow.common.apInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/approvalPurchaseRequest/export')}">
			<li><a href="${pageContext.request.contextPath}/approvalPurchaseRequest/list"><spring:message code='cashflow.common.purchaseRequest' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/approvalPurchaseRequest/import')}">
			<li><a href="${pageContext.request.contextPath}/approvalPurchaseRequest/list"><spring:message code='cashflow.common.purchaseRequest' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/bankStatement/export')}">
			<li><a href="${pageContext.request.contextPath}/bankStatement/list"><spring:message code='cashflow.common.bankStatement' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/apinvoice/search')}">
			<li class="active"><spring:message code='cashflow.common.apInvoice' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/payment/search')}">
			<li class="active"><spring:message code='cashflow.common.paymentOrder' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/receiptOrder/list')}">
			<li class="active"><spring:message code='cashflow.common.receiptOrder' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/arinvoice/list')}">
			<li class="active"><spring:message code='cashflow.common.arInvoice' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'arinvoice/detail')}">
			<li><a href="${pageContext.request.contextPath}/arinvoice/list" ><spring:message code='cashflow.common.arInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/arinvoice/import')}">
			<li><a href="${pageContext.request.contextPath}/arinvoice/list"><spring:message code='cashflow.common.arInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/arinvoice/copy')}">
			<li><a href="${pageContext.request.contextPath}/arinvoice/list"><spring:message code='cashflow.common.arInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/arinvoice/export')}">
			<li><a href="${pageContext.request.contextPath}/arinvoice/list"><spring:message code='cashflow.common.arInvoice' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'approvalSalesOrderRequest/list')}">
			<li class="active"><spring:message code='cashflow.common.salesOrderRequest' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/approvalSalesOrderRequest/export')}">
			<li><a href="${pageContext.request.contextPath}/approvalSalesOrderRequest/list"><spring:message code='cashflow.common.salesOrderRequest' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, '/approvalSalesOrderRequest/import')}">
			<li><a href="${pageContext.request.contextPath}/approvalSalesOrderRequest/list"><spring:message code='cashflow.common.salesOrderRequest' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<%-- <c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/list')}">
			<li class="active"><spring:message code='cashflow.common.paymentOrder.payroll.payment' /></li>
		</c:when>
	
		<c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/detail')}">
			<li><a href="${pageContext.request.contextPath}/paymentPayroll/list" ><spring:message code='cashflow.common.paymentOrder.payroll.payment' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/copy')}">
			<li><a href="${pageContext.request.contextPath}/paymentPayroll/list"><spring:message code='cashflow.common.paymentOrder' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when> --%>
		
		<%-- <c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/action')}">
			<li><a href="${pageContext.request.contextPath}/paymentPayroll/list"><spring:message code='cashflow.common.paymentOrder.payroll.payment' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/import')}">
			<li><a href="${pageContext.request.contextPath}/paymentPayroll/list"><spring:message code='cashflow.common.paymentOrder.payroll.payment' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when> --%>
		
		<%-- <c:when test="${fn:contains(CURRENT_URL, 'paymentPayroll/export')}">
			<li><a href="${pageContext.request.contextPath}/paymentPayroll/list"><spring:message code='cashflow.common.paymentOrder.payroll.payment' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when> --%>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payroll/list')}">
			<li class="active"><spring:message code='cashflow.common.payroll' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payroll/detail') or fn:contains(CURRENT_URL, 'payroll/copy')}">
			<li><a href="${pageContext.request.contextPath}/payroll/list" ><spring:message code='cashflow.common.payroll' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payroll/import')}">
			<li><a href="${pageContext.request.contextPath}/payroll/list"><spring:message code='cashflow.common.payroll' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'payroll/export')}">
			<li><a href="${pageContext.request.contextPath}/payroll/list"><spring:message code='cashflow.common.payroll' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'reimbursement/list')}">
			<li class="active"><spring:message code='cashflow.common.reimbursement' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'reimbursement/detail') or fn:contains(CURRENT_URL, 'reimbursement/copy')}">
			<li><a href="${pageContext.request.contextPath}/reimbursement/list" ><spring:message code='cashflow.common.reimbursement' /></a></li>
			<li class="active"><spring:message code='cashflow.common.details' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'reimbursement/import')}">
			<li><a href="${pageContext.request.contextPath}/reimbursement/list"><spring:message code='cashflow.common.reimbursement' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'reimbursement/export')}">
			<li><a href="${pageContext.request.contextPath}/reimbursement/list"><spring:message code='cashflow.common.reimbursement' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'projectAllocation/list')}">
			<li class="active"><spring:message code='cashflow.common.projectAllocation' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'projectAllocation/import')}">
			<li><a href="${pageContext.request.contextPath}/projectAllocation/list"><spring:message code='cashflow.common.projectAllocation' /></a></li>
			<li class="active"><spring:message code='cashflow.common.import' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'projectAllocation/export')}">
			<li><a href="${pageContext.request.contextPath}/projectAllocation/list"><spring:message code='cashflow.common.projectAllocation' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:when test="${fn:contains(CURRENT_URL, 'projectPayment/detail')}">
			<li><a href="${pageContext.request.contextPath}/projectAllocation/list"><spring:message code='cashflow.common.projectAllocation' /></a></li>
			<li class="active"><spring:message code='cashflow.common.projectPayment' /></li>
		</c:when>
		<c:when test="${fn:contains(CURRENT_URL, '/projectPayment/export')}">
			<c:set var="toUrl" value="/projectPayment/detail?${requestScope['javax.servlet.forward.query_string']}" />
			
			<li><a href="${pageContext.request.contextPath}/projectAllocation/list"><spring:message code='cashflow.common.projectAllocation' /></a></li>
			<li><a href="${toUrl}"><spring:message code='cashflow.common.projectPayment' /></a></li>
			<li class="active"><spring:message code='cashflow.common.export' /></li>
		</c:when>
		
		<c:otherwise>
	
		</c:otherwise>
	</c:choose>
</ol>
</li>

