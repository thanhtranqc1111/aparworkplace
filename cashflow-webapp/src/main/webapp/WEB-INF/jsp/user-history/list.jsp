<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/dataTables.bootstrap.css" rel='stylesheet'></link>
<title><spring:message code='cashflow.userHistory.title' /></title>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <input type="hidden" value="user history" id="pageName" />
            <%@ include file="../header.jsp"%>
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="page-header">
                            <h3>
                                <spring:message code='cashflow.userHistory.header' />
                            </h3>
                        </div>
                    </div>
                </div>

                <!-- Search panel -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="row">
                                    <!-- First name -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.firstName' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <input class="form-control" id="firstName" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Module -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.module' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <select id="module" class="form-control">
                                                        <option value="All">
                                                            <spring:message code='cashflow.userHistory.list.all' />
                                                        </option>
                                                        <c:forEach items="${moduleList}" var="item">
                                                            <option value="${item}">${item}</option>
                                                        </c:forEach>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- Last name -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.lastName' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <input class="form-control" id="lastName" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Tenant -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.tenant' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <select id="tenantCode" class="form-control">
                                                        <option value="<c:forEach varStatus="status" items="${subsidiaryList}" var="item">${item.code}<c:if test="${not status.last}">,</c:if></c:forEach>">
                                                            <spring:message code='cashflow.userHistory.list.all' />
                                                        </option>
                                                        <c:forEach items="${subsidiaryList}" var="item">
                                                            <option value="${item.code}">${item.code} - ${item.name}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- First name -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.email' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <input class="form-control" id="email" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Module -->
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code='cashflow.userHistory.actionDate' /></label>
                                                <div class="col-md-8 col-sm-8 col-xs-8 datepicker wrapprer-month">
                                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                                        <input type="text" class="form-control" id="fromMonth" name="fromMonth" />
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-2 tilde">
                                                        <span>~</span>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                                        <input type="text" class="form-control" id="toMonth" name="toMonth" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" col-xs-12 wrapper-btn-search">
                                        <div class="pull-right">
                                            <button type="button" id="btn-search-user-history" class="btn btn-primary">
                                                <i class="fa fa-search button-margin"></i>
                                                <spring:message code='cashflow.userHistory.search' />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- user history list  -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div>
                                    <table id="table_data_user_history" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><spring:message code='cashflow.userHistory.firstName' /></th>
                                                <th><spring:message code='cashflow.userHistory.lastName' /></th>
                                                <th><spring:message code='cashflow.userHistory.email' /></th>
                                                <th><spring:message code='cashflow.userHistory.module' /></th>
                                                <th><spring:message code='cashflow.accountMaster.action' /></th>
                                                <th><spring:message code='cashflow.userHistory.tenantCode' /></th>
                                                <th><spring:message code='cashflow.userHistory.tenantName' /></th>
                                                <th><spring:message code='cashflow.userHistory.actionDate' /></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="../footer.jsp"%>
        </div>
    </div>
</body>
<!-- popup confirm -->
<%@ include file="../common/boostrap_modal.jsp"%>
<% out.println(getReferenceJS("user-history.js")); %>
<% out.println(getReferenceCSS("user-history.css")); %>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script>
    var CONTEXT_PATH = "${pageContext.request.contextPath}";
    $(document).ready(function() {
        UserHistory.init();
        UserHistory.eventListener();
    });
</script>
</html>