/**
 * @author ThoaiNH 
 * create Apr 06, 2018
 */
(function () {
	reimbursementDetailDataTable = {
			init: function(){
			},
			eventListener: function(){
				$(document).on('change','#reimbursement-table-data .txt-exclude-gst-original-amount',function(){
					var id = $(this).parents('tr').attr('dataId');
		        	var dataItem = reimbursementDetail.getDetailItemById(id);
		        	
		        	//update excludeGstOriginalAmount
		        	var excludeGstOriginalAmountOld = dataItem.excludeGstOriginalAmount;
		        	dataItem.excludeGstOriginalAmount = $(this).val();
		        	$('#txt-exclude-gst-total-original-amount').val(getNewDecimal($('#txt-exclude-gst-total-original-amount').val())
		        															.minus(excludeGstOriginalAmountOld)
						          											.plus(getNewDecimal(dataItem.excludeGstOriginalAmount))
						          											.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
		        	reimbursementDetailDataTable.onReimbursementDetailChange(dataItem);
					//show approval info
		        	reimbursementDetailDataTable.showApprovalInfo();
				});
				
				$(document).on('change','#reimbursement-table-data .txt-exclude-gst-converted-amount',function(){
					var id = $(this).parents('tr').attr('dataId');
		        	var dataItem = reimbursementDetail.getDetailItemById(id);
		        	var self = $(this);
		        	//when excludeGstOriginalAmount == 0, prevent change value
		        	if(getNewDecimal(dataItem.excludeGstOriginalAmount).comparedTo(getNewDecimal('0')) == 0){
		        		reimbursementDetail.showWarning([label('cashflow.reimbursement.detail.mess2')]);
		        		$(this).val(dataItem.excludeGstConvertedAmount);
		        	}
		        	else {
		        		if(getNewDecimal(dataItem.excludeGstConvertedAmount).comparedTo(getNewDecimal($(this).val())) != 0)
		        		{
		        			var fxRateNew = getNewDecimal($(this).val()).div(getNewDecimal(dataItem.excludeGstOriginalAmount)).toDecimalPlaces(10, Decimal.ROUND_DOWN).valueOf();
		        			var mess = label('cashflow.reimbursement.detail.mess3').replace("%", fxRateNew);
		        			showPopupMessage2(mess, function(){
		        				$('#txt-re-fx-rate').val(fxRateNew);
		        				reimbursementDetail.updateDataByFxRate();
		        			}, function(){
		        				self.val(dataItem.excludeGstConvertedAmount);
		        			});
		        		}
		        	}
				});
				
				$(document).on('change','#reimbursement-table-data .txt-description',function(){
					var id = $(this).parents('tr').attr('dataId');
		        	var dataItem = reimbursementDetail.getDetailItemById(id);
		        	dataItem.description = $(this).val();
				});
				
				$(document).on('click','#reimbursement-table-data .btn-delete-detail-row',function(){
					$('#popConfirm').modal('toggle');
		        	$('#popConfirm button[name="btnConfirm"]').off("click");
		        	var id = $(this).parents('tr').attr('dataId');
		        	$('#popConfirm button[name="btnConfirm"]').click(function() {
		        		reimbursementDetailDataTable.deleteDetailRow(id);
			        });
				});
			},	
			addNewDetailRow: function(dataItem){
				/*
				 * append row to data table
				 */
				var tableRe = $('#reimbursement-table-data');
				var tableRef = document.getElementById(tableRe.attr('id')).getElementsByTagName('tbody')[0];
				var newRow   = tableRef.insertRow(reimbursementDetail.totalDetails++);
				newRow.setAttribute("dataId", dataItem.id);
				newRow.insertCell(0).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-account-code', null, dataItem.accountCode));
				newRow.insertCell(1).appendChild(reimbursementDetailDataTable.createInputCell('form-control','readonly', dataItem.accountName));
				newRow.insertCell(2).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-project-code', null, dataItem.projectCode));
				newRow.insertCell(3).appendChild(reimbursementDetailDataTable.createInputCell('form-control','readonly', dataItem.projectName));
				newRow.insertCell(4).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-approval-code', null, dataItem.approvalCode));
				newRow.insertCell(5).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-description', null, dataItem.description));
				
				var newCell6  = newRow.insertCell(6);
				var newText6 = document.createElement('div');
				newText6.className  = "input-group";
				newText6.innerHTML = '<span class="input-group-addon original-currency-code">' + $("#select-currency-type").val() + '</span><input type="text" class="form-control ap-big-decimal txt-exclude-gst-original-amount" value="' + dataItem.excludeGstOriginalAmount + '">';
				newCell6.appendChild(newText6);
				
				newRow.insertCell(7).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-re-fx-rate', 'readonly', $('#txt-re-fx-rate').val()));
				
				var newCell8  = newRow.insertCell(8);
				var newText8 = document.createElement('div');
				newText8.className  = "input-group";
				newText8.innerHTML = '<span class="input-group-addon">' + $("#currentCurrency").val() + '</span><input type="text" class="form-control ap-big-decimal txt-exclude-gst-converted-amount" value="' + dataItem.excludeGstConvertedAmount +'">';
				newCell8.appendChild(newText8);
				
				newRow.insertCell(9).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-re-gst-type', 'readonly', $('#select-gst-type').val()));
				
				newRow.insertCell(10).appendChild(reimbursementDetailDataTable.createInputCell('form-control txt-re-gst-rate', 'readonly', $('#txt-re-gst-rate').val()));
				
				var newCell11  = newRow.insertCell(11);
				var newText11 = document.createElement('div');
				newText11.className  = "input-group";
				newText11.innerHTML = '<span class="input-group-addon">' + $("#currentCurrency").val() + '</span>' + 
									   '<input type="hidden" class="txt-include-gst-original-amount" value="' + dataItem.gstOriginalAmount +'">' + 
								      '<input readonly type="text" class="form-control ap-big-decimal txt-gst-converted-amount alway-readonly" value="' + dataItem.gstConvertedAmount +'">';
				newCell11.appendChild(newText11);
				
				var newCell12  = newRow.insertCell(12);
				var newText12 = document.createElement('div');
				newText12.className  = "input-group";
				newText12.innerHTML = '<span class="input-group-addon">' + $("#select-currency-type").val() + '</span><input readonly type="text" class="form-control ap-big-decimal txt-include-gst-original-amount alway-readonly" value="' + dataItem.includeGstOriginalAmount +'">';
				newCell12.appendChild(newText12);
				
				var newCell13  = newRow.insertCell(13);
				var newText13 = document.createElement('div');
				newText13.className  = "input-group";
				newText13.innerHTML = '<span class="input-group-addon">' + $("#currentCurrency").val() + '</span>' +  
									   '<input readonly type="text" class="form-control ap-big-decimal txt-include-gst-converted-amount alway-readonly" value="' + dataItem.includeGstConvertedAmount +'">';
				newCell13.appendChild(newText13);
				
				var newCell14  = newRow.insertCell(14);
				var newText14  = document.createElement('span');
				newText14.innerHTML = '<div class="ap-status apr-002 ap-detail">' + $('#apr002Value').val() + '</div>';
				newCell14.appendChild(newText14);
				
				var newCell15  = newRow.insertCell(15);
				var newText15 = document.createElement('span');
				newText15.innerHTML = '<span class="btn-delete-detail-row glyphicon glyphicon-trash"></span>';
				newCell15.appendChild(newText15);
				/*
				 * setup for some component
				 */
				reimbursementDetailDataTable.setUpTableRow($('#reimbursement-table-data tbody tr:last'));
				reimbursementDetail.renderCopyButton();
			},
			createInputCell: function(className, readonly, value){
				var newInput  = document.createElement("input");
				newInput.setAttribute('type', 'text');
				if(className){
					newInput.setAttribute('class', className);
				}
				if(readonly){
					newInput.setAttribute('readonly', readonly);
				}
				if(value){
					newInput.setAttribute('value', value);
				}
				return newInput;
			},
			deleteDetailRow: function(id){
				$('#reimbursement-table-data').find('tr[dataId="' + id +'"]').remove();
				reimbursementDetail.totalDetails--;
				for(i = 0; i < reimbursementDetail.dataListDetails.length; i++)
				{
					var item = reimbursementDetail.dataListDetails[i];
					if(item.id == id)
					{
						item['removed'] = true;
						//update header info
						$('#txt-exclude-gst-total-original-amount').val(getNewDecimal($('#txt-exclude-gst-total-original-amount').val())
																				.minus(getNewDecimal(item.excludeGstOriginalAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						$('#txt-exclude-gst-total-converted-amount').val(getNewDecimal($('#txt-exclude-gst-total-converted-amount').val())
																				.minus(getNewDecimal(item.excludeGstConvertedAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						$('#txt-gst-original-amount').val(getNewDecimal($('#txt-gst-original-amount').val())
																				.minus(getNewDecimal(item.gstOriginalAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						$('#txt-gst-converted-amount').val(getNewDecimal($('#txt-gst-converted-amount').val())
																				.minus(getNewDecimal(item.gstConvertedAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						$('#txt-include-gst-total-converted-amount').val(getNewDecimal($('#txt-include-gst-total-converted-amount').val())
																				.minus(getNewDecimal(item.includeGstConvertedAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						$('#txt-include-gst-total-original-amount').val(getNewDecimal($('#txt-include-gst-total-original-amount').val())
																				.minus(getNewDecimal(item.includeGstOriginalAmount))
																				.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
						if(id.indexOf('new-') == 0)
						{
							reimbursementDetail.dataListDetails.splice(i, 1);
						}
						break;
					}
				}
				reimbursementDetailDataTable.showApprovalInfo();
				reimbursementDetail.renderCopyButton();
			},
			setUpTableRow: function(tableRow){
				tableRow.find('input.ap-big-decimal').setupInputNumberMask({allowMinus:false});
				
				tableRow.find('input.txt-re-fx-rate, input.txt-re-gst-rate').inputmask("decimal", {
					alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false
				});
				
				var newTxtApproval = tableRow.find('.txt-approval-code');
				var approvalTautocomplete = newTxtApproval.tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [GLOBAL_MESSAGES['cashflow.reimbursement.detail.approvalNo'], GLOBAL_MESSAGES['cashflow.reimbursement.detail.amount'], 
							  GLOBAL_MESSAGES['cashflow.reimbursement.detail.approvalDate'], GLOBAL_MESSAGES['cashflow.reimbursement.detail.applicant'], GLOBAL_MESSAGES['cashflow.reimbursement.detail.purpose']],
					hide: [true, true, true, true, true],
					dataIndex: 0,
			        data: function(){
			        		var x = { keyword: approvalTautocomplete.searchdata(), bookingDate: $("#txt-booking-date").val()}; 
			        		return x;
			        	},
			        ajax: {
			                url: "/apinvoice/getListApprovalAvailable",
			                type: "GET",
			                data: {
			                	keyword: function(){
			                		var keyword = approvalTautocomplete.searchdata();
			                		return keyword;
			                	},
			                	bookingDate : function(){
			                		return $("#txt-booking-date").val();
			                	}
			                },
			                success: function (ret) {
			                    return ret;
			                }
			            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = approvalTautocomplete.all();
			        	var id = newTxtApproval.parents('tr').attr('dataId');
			        	var dataItem = reimbursementDetail.getDetailItemById(id);
			        	//when set an approval code
			        	if(newTxtApproval.next().next().val().length > 0)
			        	{
			        		dataItem.approvalCode = selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.approvalNo']];
				        	newTxtApproval.attr('approvalcode', selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.approvalNo']]);
				        	newTxtApproval.attr('amount', selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.amount']].split(" ")[1]);
			        	}
			        	//when remove approval code
			        	else {
			        		dataItem.approvalCode = "";
				        	newTxtApproval.attr('approvalcode',"");
				        	newTxtApproval.attr('amount',"");
			        	}
			        	reimbursementDetailDataTable.showApprovalInfo();
			        }
				});
				approvalTautocomplete.settext(newTxtApproval.val());
				
				var newTxtProjectCode = tableRow.find('.txt-project-code');
				var projectCodeTautocomplete = newTxtProjectCode.tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [label('cashflow.reimbursement.detail.projectCode'), label('cashflow.reimbursement.detail.projectName'), "", "", "", "", "", ""],
					hide: [true, true, false, false, false, false, false, false],
					dataIndex: 0,
					addNewDataLink: '/admin/projectMaster/list',
			        data: function(){
			        		var x = { keyword: projectVal.searchdata()}; 
			        		return x;
			        	},
			        ajax: {
			                url: "/manage/projectMasters",
			                type: "GET",
			                data: {
			                	keyword: function(){
			                		var keyword = projectCodeTautocomplete.searchdata();
			                		return keyword;
			                	}
			                },
			                success: function (data) {
			                    return data;
			                }
			            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = projectCodeTautocomplete.all();
			        	newTxtProjectCode.parents('tr').find('td:nth-child(4) input').val(selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.projectName']]);
			        	var id = newTxtApproval.parents('tr').attr('dataId');
			        	var dataItem = reimbursementDetail.getDetailItemById(id);
			        	dataItem.projectCode = selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.projectCode']];
			        	dataItem.projectName = selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.projectName']];
			        }
				});
				projectCodeTautocomplete.settext(newTxtProjectCode.val());
				
				var newTxtAccountCode = tableRow.find('.txt-account-code');
			    var accountCodeTautocomplete = newTxtAccountCode.tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [label('cashflow.reimbursement.detail.accountCode'),label('cashflow.reimbursement.detail.parentCode'), label('cashflow.reimbursement.detail.accountName'), label('cashflow.reimbursement.detail.type')],
					hide: [true, false, true, true],
					dataIndex: 0,
					addNewDataLink: '/admin/accountMaster/list',
			        data: function(){
			        		var x = { keyword: accountCodeTautocomplete.searchdata()}; 
			        		return x;
			        	},
			        ajax: {
			                url: "/manage/accountMasters",
			                type: "GET",
			                data: {
			                	keyword: function(){
			                		var keyword = accountCodeTautocomplete.searchdata();
			                		return keyword;
			                	}
			                },
			                success: function (data) {
			                    return data;
			                }
			            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = accountCodeTautocomplete.all();
			        	newTxtAccountCode.parents('tr').find('td:nth-child(2) input').val(selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountName']]);
			        	var id = newTxtAccountCode.parents('tr').attr('dataId');
			        	var dataItem = reimbursementDetail.getDetailItemById(id);
			        	dataItem.accountCode = selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountCode']];
			        	dataItem.accountName = selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountName']];
			        }
				});
			    accountCodeTautocomplete.settext(newTxtAccountCode.val());
			},
			showApprovalInfo: function(){
				var showingList = reimbursementDetail.dataListDetails.filter(function(item){
    				return item['removed'] != true;
    			});
				var approvalInfo = {};
				for(i = 0; i < showingList.length; i++){
					var item = showingList[i];
					if(item.approvalCode){
						if(approvalInfo[item.approvalCode]){
							approvalInfo[item.approvalCode] = getNewDecimal(approvalInfo[item.approvalCode]).plus(getNewDecimal(item.includeGstOriginalAmount)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						}
						else {
							approvalInfo[item.approvalCode] = item.includeGstOriginalAmount;
						}
					}
				}
				console.log(approvalInfo);
				var htmlString = "";
				for (var key in approvalInfo) {
				    if (approvalInfo.hasOwnProperty(key)) {
				        console.log(key + " -> " + approvalInfo[key]);
				        var approvalAmount = $('.txt-approval-code[approvalcode="' + key + '"]').attr('amount');
				        htmlString += '<li>' + GLOBAL_MESSAGES['cashflow.reimbursement.detail.mess1'].replace("%1", key).replace("%2", $('#select-currency-type').val())
				        																	   .replace("%3", formatStringAsNumber(approvalInfo[key])).replace("%4", formatStringAsNumber(approvalAmount)) + '</li>';
				    }
				}
				$('#alertApprovalStatus').html(htmlString);
			},
			onReimbursementDetailChange: function(dataItem){
				//update excludeGstConvertedAmount
	        	var excludeGstConvertedAmountOld = dataItem.excludeGstConvertedAmount;
	        	dataItem.excludeGstConvertedAmount = getNewDecimal(dataItem.excludeGstOriginalAmount).mul(getNewDecimal($('#txt-re-fx-rate').val())).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
	        	$("#reimbursement-table-data tr[dataid='" + dataItem.id + "']").find('.txt-exclude-gst-converted-amount').val(dataItem.excludeGstConvertedAmount);
	        	$('#txt-exclude-gst-total-converted-amount').val(getNewDecimal($('#txt-exclude-gst-total-converted-amount').val())
																		.minus(excludeGstConvertedAmountOld)
																		.plus(getNewDecimal(dataItem.excludeGstConvertedAmount))
																		.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
	        	
	        	//update gstOriginalAmount
	        	var gstOriginalAmountOld = dataItem.gstOriginalAmount;
	        	dataItem.gstOriginalAmount = getNewDecimal(dataItem.excludeGstOriginalAmount).mul(getNewDecimal($('#txt-re-gst-rate').val())).div(getNewDecimal('100')).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
	        	$("#reimbursement-table-data tr[dataid='" + dataItem.id + "']").find('.txt-gst-original-amount').val(dataItem.gstOriginalAmount);
				$('#txt-gst-original-amount').val(getNewDecimal($('#txt-gst-original-amount').val())
																		.minus(gstOriginalAmountOld)
																		.plus(getNewDecimal(dataItem.gstOriginalAmount))
																		.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
	        	//update gstConvertedAmount
				var gstConvertedAmountOld = dataItem.gstConvertedAmount;
				dataItem.gstConvertedAmount = getNewDecimal(dataItem.excludeGstConvertedAmount).mul(getNewDecimal($('#txt-re-gst-rate').val())).div(getNewDecimal('100')).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				$("#reimbursement-table-data tr[dataid='" + dataItem.id + "']").find('.txt-gst-converted-amount').val(dataItem.gstConvertedAmount);
				$('#txt-gst-converted-amount').val(getNewDecimal($('#txt-gst-converted-amount').val())
																		.minus(gstConvertedAmountOld)
																		.plus(getNewDecimal(dataItem.gstConvertedAmount))
																		.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
				//update includeGstConvertedAmount
				var includeGstConvertedAmountOld = dataItem.includeGstConvertedAmount
				dataItem.includeGstConvertedAmount = getNewDecimal(dataItem.excludeGstConvertedAmount).add(getNewDecimal(dataItem.gstConvertedAmount)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				$("#reimbursement-table-data tr[dataid='" + dataItem.id + "']").find('.txt-include-gst-converted-amount').val(dataItem.includeGstConvertedAmount);
				$('#txt-include-gst-total-converted-amount').val(getNewDecimal($('#txt-include-gst-total-converted-amount').val())
																		.minus(includeGstConvertedAmountOld)
																		.plus(getNewDecimal(dataItem.includeGstConvertedAmount))
																		.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
				
				//update includeGstOriginalAmount
				var includeGstOriginalAmountOld = dataItem.includeGstOriginalAmount;
				dataItem.includeGstOriginalAmount = getNewDecimal(dataItem.excludeGstOriginalAmount).add(getNewDecimal(dataItem.gstOriginalAmount)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				$("#reimbursement-table-data tr[dataid='" + dataItem.id + "']").find('.txt-include-gst-original-amount').val(dataItem.includeGstOriginalAmount);
				$('#txt-include-gst-total-original-amount').val(getNewDecimal($('#txt-include-gst-total-original-amount').val())
																		.minus(includeGstOriginalAmountOld)
																		.plus(getNewDecimal(dataItem.includeGstOriginalAmount))
																		.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
			}
	};
}());