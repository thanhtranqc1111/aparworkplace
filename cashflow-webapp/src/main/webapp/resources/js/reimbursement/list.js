/**
 * @author ThoaiNH 
 * create Apr 06, 2018
 */
(function () {
	reimbursementList = {
			dataTable: null,
			init: function(){
				$('.datepicker input').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				
				reimbursementList.dataTable = $('#reimbursement-data-table').DataTable({
			        "ajax": {
			            url: "/reimbursement/api/getAllPagingReimbursement",
			            type: "GET",
			            data: function ( d ) {
			            	d.fromMonth = $('#txt-fromMonth').val();
			                d.toMonth = $('#txt-toMonth').val();
			                d.status = $('#select-status').val();
			                d.fromReimbursementNo = $('#txt-fromApNo').val();
			                d.toReimbursementNo = $('#txt-toApNo').val();
			                d.employeeCode = $('#select-employee-code').val();
			            },
			            error: function() {
			                $("#reimbursement-data-table").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
			            },
			            complete: function(data){
							hideAjaxStatus();
							reimbursementList.mergeRows('#reimbursement-data-table', 1, 8);
				        	reimbursementList.mergeRows('#reimbursement-data-table', 9, 15);
			            }
			        },
			        columns: [
			        	{
				            "data" : "reimbursementNo",
				            "width" : "10%",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	return "<a href='/reimbursement/detail/" + data +"' >" + data + "</a>";
				             }
				        },
				        {
				            "data" : "month",
				            "orderable": true,
				            "width" : "5%",
				            "render": function(data, type,full, meta) {
				            	return (full.month.month + 1) + "/" + full.month.year;
				             }
				        },
				        {
				            "data" : "claimTypeVal",
				            "orderable": false,
				            "width" : "8%",
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "employeeCode",
				            "orderable": false,
				            "width" : "5%"
				        },
				        {
				            "data" : "employeeName",
				            "orderable": false,
				            "width" : "8%"
				        },
				        {
				            "data" : "invoiceNo",
				            "orderable": false,
				            "width" : "5%"
				        },
				        {
				            "data" : "includeGstTotalAmountConverted",
				            "orderable": false,
				            "width" : "8%",
				            "render": function(data, type,full, meta) {
				            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
		            				   + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				             }
				        },
				        {
				            "data" : "reimbursementStatus",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(full.reimbursementStatus != undefined){
				            		return "<div class='ap-status  api-" + full.reimbursementStatusCode + "'>" + full.reimbursementStatus + "</div>";
				            	}
				             }
				        },
				        {
				            "data" : "paymentOrderNo",
				            "width" : "6%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
				            		return "<a href='/payment/detail/" + data +"' >" + data + "</a>";
				            	else
				            		return "";
				             }
				        },
				        {
				            "data" : "valueDate",
				            "orderable": false,
				            "width" : "6%",
				            "render": function(data, type,full, meta) {
				            	if(full.valueDate)
				            		return full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year;
				            	else
				            		return "";
				             }	
				        },
				        {
				            "data" : "bankName",
				            "width" : "6%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankAccount",
				            "width" : "7%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankStatementIds",
				            "width" : "6%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            	{
				            		var trans = "";
					            	var length = data.length;
					            	$.each(data, function(i, obj) {
					            		trans += "<a href='/bankStatement/list/" + obj +"' target='_blank'>" + obj + "</a>";
					            		if ( i != length - 1){
					            			trans += ", ";
					            		}
					            	});
					            	return trans;
				            	}
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "includeGstConvertedAmount",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
					            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
		            				        + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankRefNo",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "paymentOrderStatus",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var paymentStatus = "";
				            	if(full.paymentOrderStatus != undefined){
				            		paymentStatus = "<div class='ap-status  pay-" + full.paymentOrderStatusCode + "'>" + full.paymentOrderStatus + "</div>";
				            	}
				            	return paymentStatus;
				             }
				        }
			        ],
			        searching: false,
			        "order": [[ 0, "desc" ]],
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 'All']],
					"lengthChange" : true,
			        "columnDefs": [
			        	{
			                "targets": [0],
			                "className": "text-center",
			            },
			            {
			                "targets": [1],
			                "className": "text-right",
			            }
			        ],
				 });
			},
			eventListener: function(){
				$(document).on('click','#btn-reset',function(){
					$('.ap-search input').val('');	
					$('#select-employee-code').val('');
					$('#select-status').val('');
				});
				
				$('#export').click(function(){
					var href = $(this).attr("href");
					var param = "";
					
					var fromMonth = $('#txt-fromMonth').val();
					var toMonth = $('#txt-toMonth').val();
					var fromReimbursementNo = $('#txt-fromApNo').val();
					var toReimbursementNo = $('#txt-toApNo').val();
					var status = $('#select-status').val();
					var employeeCode = $('#select-employee-code').val();
					
					if (fromMonth.length > 0) {
						param += "&fromMonth=" + fromMonth;
					}
					if (toMonth.length > 0) {
						param += "&toMonth=" + toMonth;
					}
					if (fromReimbursementNo.length > 0) {
						param += "&fromReimbursementNo=" + fromReimbursementNo;
					}
					if (toReimbursementNo.length) {
						param += "&toReimbursementNo=" + toReimbursementNo;
					}
					if (status.length) {
						param += "&status=" + status;
					}
					if (employeeCode.length > 0) {
						param += "&employeeCode=" + employeeCode;
					}

					if (param.length > 0) {
						href = href + "?" + param.slice(1);
					}
					$("#export").attr("href", href);
				});
				
				$(document).on('click','#btn-search',function(){
					reimbursementList.dataTable.ajax.reload();
				});
			},
			mergeRows: function (table, mergeFrom, mergeTo) {
				var rowsList = $(''+table+ ' > tbody').find('tr');
				var previous = null, cellToExtend = null, rowspan = 1;
				
				rowsList.each(function(index, e) {
					var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
					if (previous == content && content !== "" ) {
						rowspan = rowspan + 1;
						
						for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
							if(i >= mergeFrom && i <= mergeTo) {
								$(this).find('td:nth-child(' + i + ')').addClass('hidden');
								padding = rowspan * 16;
								cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
								cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
							} 
						}
					} else {
			             rowspan = 1;
			             previous = content;
			             cellToExtend = elementTab;
					}
				});
				$(table + ' td.hidden').remove();
			}
	};
}());