/**
 * @author ThoaiNH 
 * create Mar 15, 2018
 */
(function () {
	reimbursementDetail = {
			id: 0,
			crNewId: 0,
			dataListDetails: [],
			totalDetails: 0,
			REIMBURSEMENT_STATUSES: {},
			initData: function(data){
				if(localStorage.getItem("reimbursementShowConfirm") && localStorage.getItem("reimbursementShowConfirm") == $('#txt-reimbursementNo').val()){
					showPopupSave($('#txt-reimbursementNo').val() + " " + GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'].toLowerCase() + ".", function() {
	    				$('#isChangedContent').val('false');
	 		 		   	window.location = "/reimbursement/list";
	 				});
					localStorage.removeItem("reimbursementShowConfirm");
				}
				if(data){
					reimbursementDetail.id = data.id;
					reimbursementDetail.dataListDetails = data.reimbursementDetailses;
					reimbursementDetail.totalDetails = reimbursementDetail.dataListDetails.length;
					//show leave confirm POPUP when add new
					if(data.id == 0)
					{
						//setup for copy case
						$('#reimbursement-table-data tr').each(function(){
							reimbursementDetailDataTable.setUpTableRow($(this));
						});
						//setup new data id when copy because all id will be = 0 when copy
						for(i = 0; i < reimbursementDetail.dataListDetails.length; i++){
							reimbursementDetail.dataListDetails[i].id = "new-" + i;
						}
						reimbursementDetail.crNewId = reimbursementDetail.dataListDetails.length;
						reimbursementDetail.renderCopyButton();
					}
					else {
						//disable delete when edit
						$('#reimbursement-table-data .btn-delete-detail-row').hide();
					}
				}
			},
			init: function(){
				$('#form-reimbursement-header').on('keyup change', 'input, select, textarea', function(){
					$('#isChangedContent').val('true');
				});
				
				$('input.ap-big-decimal').setupInputNumberMask({allowMinus:false});
				$('#txt-re-fx-rate, #txt-re-gst-rate').inputmask("decimal", {
					alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false, allowMinus: false
				});
				
				$('.ap-date-picker').datepicker({
			    	format: 'dd/mm/yyyy',
			        orientation: 'auto bottom',
			        todayHighlight: true
			    });
				
				$('.ap-date-picker-month-format').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				
				var accountTautocomplete = $('#txt-account-payable-code').tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [label('cashflow.reimbursement.detail.accountCode'), label('cashflow.reimbursement.detail.parentCode'), label('cashflow.reimbursement.detail.accountName'), label('cashflow.reimbursement.detail.type')],
					hide: [true, false, true, true],
					dataIndex: 0,
					addNewDataLink: '/admin/accountMaster/list',
			        data: function(){
		        		var x = { keyword: accountTautocomplete.searchdata()}; 
		        		return x;
		        	},
			        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountTautocomplete.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = accountTautocomplete.all();
			        	$('#txt-account-payable-code').val(selectedData[label('cashflow.reimbursement.detail.accountCode')]);
			        	$('#txt-account-payable-name').val(selectedData[label('cashflow.reimbursement.detail.accountName')]);
			        }
				});
				accountTautocomplete.settext($('#txt-account-payable-code').val());
				
				var employeeTautocomplete = $('#txt-employeeName').tautocomplete({
			        highlight: "",
			        autobottom: true,
			        columns: ["ID", GLOBAL_MESSAGES['cashflow.reimbursement.detail.employeeID'], GLOBAL_MESSAGES['cashflow.reimbursement.detail.name'],
						 GLOBAL_MESSAGES['cashflow.reimbursement.detail.roleTitle'], GLOBAL_MESSAGES['cashflow.reimbursement.detail.team'],
						 GLOBAL_MESSAGES['cashflow.reimbursement.detail.division'], "divisionCode", "joinDate", "resignDate"],
					hide: [false, true, true, true, true, true, false, false, false],
					dataIndex: 0,
					addNewDataLink: '/admin/employeeMaster/list',
			        data: function(){
		        		var x = { keyword: employeeTautocomplete.searchdata()}; 
		        		return x;
		        	},
			        ajax: {
		                url: "/manage/getEmployeeMaster",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = employeeTautocomplete.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = employeeTautocomplete.all();
			        	$('#txt-employeeName').val(selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.name']]);
			        	$('#txt-employeeCode').val(selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.employeeID']]);
			        	employeeTautocomplete.settext(selectedData[GLOBAL_MESSAGES['cashflow.reimbursement.detail.name']]);
			        }
				});
				employeeTautocomplete.settext($('#txt-employeeName').val());
				
				if(this.id > 0){
					$('input[autocomplete]').attr('readonly','');
				}
				var $table = $('#reimbursement-table-data');
				$table.floatThead({
				    scrollContainer: function($table){
				        return $table.closest('.table-scroll');
				    }
				});	
			},
			eventListener: function(){
				$(document).on('click','#btn-add-row',function(){
					var data = reimbursementDetail.createDetailsJSON();
					reimbursementDetailDataTable.addNewDetailRow(data);
				});
				
				// change invoice status
				$(document).on('click','.ap-dropdown-status li',function(){
					const NOTPAID = '002';
					const CANCELLED = '004';
					
					var statusCode = $(this).find('a').attr('status');
					$(this).closest('.ap-dropdown-status').find('button').attr('class','ap-status api-' + statusCode + ' ap-status-header');
					$(this).closest('.ap-dropdown-status').find('.status-label').html($(this).find('a').html());
					
					// update dropdown
					if(statusCode == CANCELLED){
						$(this).find('a').attr('status', NOTPAID);
						$(this).find('a').html(reimbursementDetail.REIMBURSEMENT_STATUSES[NOTPAID]);

					} else if(statusCode == NOTPAID){
						$(this).find('a').attr('status', CANCELLED);
						$(this).find('a').html(reimbursementDetail.REIMBURSEMENT_STATUSES[CANCELLED]);
					}
					
					$('#status').val(statusCode);
				});
				
				// reset btn
				$(document).on('click','#btn-reset-all',function(){
					if(!$(this).hasClass('disabled')){
						if(reimbursementDetail.id > 0 || window.location.href.indexOf('reimbursement/copy') > 0){
							window.location.reload();
						}
						else {
							document.getElementById("form-reimbursement-header").reset();
							$('#reimbursement-table-data tbody').html('');
							reimbursementDetail.dataListDetails = [];
							reimbursementDetail.totalDetails = 0;
							$('#alertApprovalStatus').html('');
						}
					}
				});
				
				$(document).on('click','#btn-cancel',function(e){
					e.preventDefault();
					window.location = "/reimbursement/list";
				});
				
				$(document).on('click','#btn-copy',function(){
					if(!$(this).hasClass('disabled')){
						reimbursementDetail.copyDetails();
					}
				});
				
				$(document).on('click','#btn-clone',function(){
					if(!$(this).hasClass('disabled')){
						 window.location.href = "/reimbursement/copy/" + $('#txt-reimbursementNo').val();
					}
				});
				
				$(document).on('click','#btn-save',function(){
					if(!$(this).hasClass('disabled')){
						 reimbursementDetail.save();
					}
				});
				
				$(document).on('click','#edit-btn',function(){
					$(this).attr('disabled','');
					//only allow to update detail when status is not paid
					if($('#status').val() == '002'){
						$('.ap-label-status').hide();
						$('.ap-dropdown-status').show();
						$('#reimbursement-table-data .btn-delete-detail-row').show();
						$('.panel-bottom-buttons button').removeAttr('disabled');
						$('#select-gst-type, #select-currency-type').removeAttr('disabled');
						$('input:not(.alway-readonly), select:not(.alway-readonly), textarea:not(.alway-readonly)').attr('readonly',false);
						reimbursementDetail.renderCopyButton();
						$('#reimbursement-table-data tr').each(function(){
							reimbursementDetailDataTable.setUpTableRow($(this));
						});
					}
					//partial paid status will be allowed to edit reimbursement header
					else {
						$('.panel-bottom-buttons .pull-right button').removeAttr('disabled');
						$('#form-reimbursement-header #txt-month, #form-reimbursement-header #txt-booking-date,' + 
						  '#form-reimbursement-header #txt-invoiceNo, ' + 
						  '#form-reimbursement-header #select-claim-type, #form-reimbursement-header #txt-description').attr('readonly',false);
						$('#form-reimbursement-header #txt-account-payable-code').next().next().attr('readonly',false);
					}
				});
				
				$(document).on('change','#select-currency-type',function(){
					$(".original-currency-code").html($(this).val());
				});
				
				$(document).on('change','#txt-re-fx-rate',function(){
					reimbursementDetail.updateDataByFxRate();
				});
				
				$(document).on('change','#txt-re-gst-rate',function(){
					reimbursementDetail.updateDataByGstRate();
				});
				
				$(document).on('change','#select-gst-type',function(){
					$('#txt-re-gst-rate').val($(this).find(":selected").attr('rate'));
					reimbursementDetail.updateDataByGstRate();
				});
			},
			getDetailItemById: function(id){
				var data = null;
				this.dataListDetails.forEach(function(item){
					if(item.id == id)
					{
						data = item;
						return;
					}
				});
				return data;
			},
			save: function(){
				if(GLOBAL_PERMISSION["PMS-003"]){
					var dataList = jQuery.extend(true, [], reimbursementDetail.dataListDetails);
					for(var j = 0; j < dataList.length; j++)
					{
						if(typeof dataList[j].id == 'string'){
							dataList[j].id = 0;
						}
					}
					var data = {
						id: reimbursementDetail.id,
						reimbursementNo: $('#txt-reimbursementNo').val(),
						bookingDate: $('#txt-booking-date').val(),
						claimType: $('#select-claim-type').val(),
						accountPayableCode: $('#txt-account-payable-code').val(),
						accountPayableName: $('#txt-account-payable-name').val(),
						paymentScheduleDate: $('#txt-scheduled-payment-date').val(),
						status: $('#status').val(),
						description: $('#txt-description').val(),
						fxRate: $('#txt-re-fx-rate').val(),
						employeeName: $('#txt-employeeName').val(),
						employeeCode: $('#txt-employeeCode').val(),
						originalCurrencyCode: $('#select-currency-type').val(),
						month: $('#txt-month').val(),
						invoiceNo: $('#txt-invoiceNo').val(),
						gstType: $('#select-gst-type').val(),
						gstRate: $('#txt-re-gst-rate').val(),
						includeGstTotalConvertedAmount: $('#txt-include-gst-total-converted-amount').val(),
						includeGstTotalOriginalAmount: $('#txt-include-gst-total-original-amount').val(),
						excludeGstTotalOriginalAmount: $('#txt-exclude-gst-total-original-amount').val(),
						excludeGstTotalConvertedAmount: $('#txt-exclude-gst-total-converted-amount').val(),
						gstConvertedAmount: $('#txt-gst-converted-amount').val(),
						gstOriginalAmount: $('#txt-gst-original-amount').val(),
						reimbursementDetailses: dataList
					};
					console.log(data);
					var arrMess = reimbursementDetail.validateDataBeforeSave(data);
					if(arrMess.length == 0)
					{
						var formData = new FormData();
						formData.append('data', JSON.stringify(data));
						$.ajax({
				            type : "POST",
				            url : CONTEXT_PATH + '/reimbursement/save',
				            cache : false,
							contentType : false,
							processData : false,
							data : formData,
				            success : function(ajaxResult) {
				            	 var resultData = JSON.parse(ajaxResult);
				            	 console.log(resultData);
				            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0) {
				            		 $(".alert-warning").hide();
				            		 $('#isChangedContent').val('false');
			         		 		 window.location = "/reimbursement/detail/" +  $("#txt-reimbursementNo").val();
			         		 		 localStorage.setItem("reimbursementShowConfirm", $('#txt-reimbursementNo').val());
					             }
				            	 else {
				            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
				            	 }
				            }
				        });
					}
					else 
					{
						reimbursementDetail.showWarning(arrMess);
					}
				}
			},
			createDetailsJSON: function(){
				/*
				 * insert data list JSON
				 */
				var data = {
					id: "new-" + this.crNewId++,
					accountCode: "",
					accountName: "",
					projectCode: "",
					projectName: "",
					includeGstOriginalAmount: 0,
					includeGstConvertedAmount: 0,
					gstOriginalAmount: 0,
					gstConvertedAmount: 0,
					excludeGstOriginalAmount: 0,
					excludeGstConvertedAmount: 0,
					approvalCode: "",
					description: $('#txt-description').val()
				};
				this.dataListDetails.push(data);
				return data;
			},
			renderCopyButton: function(){
				var showingList = this.dataListDetails.filter(function(item){
    				return item['removed'] != true;
    			});
				if(showingList.length > 0)
				{
					$('#btn-copy').removeClass('disabled');
				}
				else {
					$('#btn-copy').addClass('disabled');
				}
			},
			copyDetails: function(){
				var lastItem = null;
				for(i = 0; i < this.dataListDetails.length; i++){
					var item = this.dataListDetails[i];
					if(item['removed'] != true)
					{
						lastItem = item;
					}
				}
				var newItem = jQuery.extend(true, {}, lastItem);
				if(newItem != null)
				{
					newItem.id = "new-" + this.crNewId ++;
					this.dataListDetails.push(newItem);
					reimbursementDetailDataTable.addNewDetailRow(newItem);
					//update header info
					$('#txt-exclude-gst-total-original-amount').val(getNewDecimal($('#txt-exclude-gst-total-original-amount').val())
																			.plus(getNewDecimal(newItem.excludeGstOriginalAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					$('#txt-exclude-gst-total-converted-amount').val(getNewDecimal($('#txt-exclude-gst-total-converted-amount').val())
																			.plus(getNewDecimal(newItem.excludeGstConvertedAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					$('#txt-gst-original-amount').val(getNewDecimal($('#txt-gst-original-amount').val())
																			.plus(getNewDecimal(newItem.gstOriginalAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					$('#txt-gst-converted-amount').val(getNewDecimal($('#txt-gst-converted-amount').val())
																			.plus(getNewDecimal(newItem.gstConvertedAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					$('#txt-include-gst-total-converted-amount').val(getNewDecimal($('#txt-include-gst-total-converted-amount').val())
																			.plus(getNewDecimal(newItem.includeGstConvertedAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					$('#txt-include-gst-total-original-amount').val(getNewDecimal($('#txt-include-gst-total-original-amount').val())
																			.plus(getNewDecimal(newItem.includeGstOriginalAmount))
																			.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
					reimbursementDetailDataTable.showApprovalInfo();
				}
			},
			showWarning:function(message){
				$('.alert-warning').showAlertMessage(message);
				$('#alert-warning-reimbursement').goTo();
				
			},
			hideWarning: function(){
				$('.alert-warning').hide();
			},
			validateDataBeforeSave: function(data){
				var arrMess = [];
				var showingList = data.reimbursementDetailses.filter(function(item){
    				return item['removed'] != true;
    			});
				if(data.employeeName.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.employeeName']));
				}
				if(data.month.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.month']));
				}
				if(data.bookingDate.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.bookingdate']));
				}
				if(data.accountPayableCode.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountPayableCode']));
				}
				if(data.accountPayableName.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountPayableName']));	
				}
				if(showingList.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.reimbursementDetails']));	
				}
				if(/^(.{0,40})$/.test($('#txt-invoiceNo').val()) == false) {
					arrMess.push(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.reimbursement.detail.invoiceNo'], '40'));
	            } 
				else {
					for(i = 0; i < showingList.length; i++){
						var item = showingList[i];
						if(!item.projectCode || item.projectCode.trim().length == 0)
						{
							arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.reimbursementDetails'] + " : " + GLOBAL_MESSAGES['cashflow.reimbursement.detail.projectCode']));
							break;
						}
						if(!item.accountCode || item.accountCode.trim().length == 0)
						{
							arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.reimbursement.detail.reimbursementDetails'] + " : " + GLOBAL_MESSAGES['cashflow.reimbursement.detail.accountCode']));
							break;
						}
					}
				}
				return arrMess;
			},
			updateDataByFxRate: function(){
				var fxRate = $('#txt-re-fx-rate').val();
				$('.txt-re-fx-rate').val(fxRate);
				for(i = 0; i < reimbursementDetail.dataListDetails.length; i++){
					var data = reimbursementDetail.dataListDetails[i];
					data.fxRate = fxRate;
					reimbursementDetailDataTable.onReimbursementDetailChange(data);
				}
				//show approval info
	        	reimbursementDetailDataTable.showApprovalInfo();
			},
			updateDataByGstRate: function(){
				var gstRate = $('#txt-re-gst-rate').val();
				$('.txt-re-gst-rate').val(gstRate);
				for(i = 0; i < reimbursementDetail.dataListDetails.length; i++){
					var data = reimbursementDetail.dataListDetails[i];
					data.gstRate = gstRate;
					reimbursementDetailDataTable.onReimbursementDetailChange(data);
				}
				//show approval info
	        	reimbursementDetailDataTable.showApprovalInfo();
			}
	};
}());