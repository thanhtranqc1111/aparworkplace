/**
 * @author ThoaiNH 
 * create Oct 30, 2017
 */
(function () {
	permissionList = {
		dataTable: null,
		rows_selected: [], //id of checked row in data table
		dataList: null,//list data of main table
		txtSearch: "",
		crNewId: 0,//id of new role
		fieldIndex: {1: "code", 2: "name", 3: "description"},
		init: function () {
			jsModel  = permissionList;
			//set up data table
			permissionList.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/permission/api/getAllPagingPermission",
		            type: "GET",
		            data: function ( d ) {
		                d.keyword = permissionList.txtSearch;
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	permissionList.dataList = data.responseJSON.data;
		            	console.log(permissionList.dataList);
		            	$('#table_data').Tabledit({
		    				editButton: false,
		    			    deleteButton: false,
		    			    hideIdentifier: true,
		    			    initedTableEditor: false,
		    			    columns: {
		    			    	identifier: [20, 'id'],
		    			        editable: [[1], [2], [3]]
		    			    },
		    			    submitFunction: permissionList.updateDataList
		    			});
		            	
		            	$('.btn-save-all').addClass('disabled');
		            	$('#table_data').tableCheckable(permissionList.dataTable, permissionList.rows_selected, permissionList.renderDeleteButton);
		            	hideAjaxStatus();
		            }
		        },
		        columns: [
		        	{
			            "data" : "id",
			            "width" : "1%"
			        },
			        {
			            "data" : "code"
			        }, 
			        {
			            "data" : "name"
			        }, 
			        {
			            "data" : "description"
			        }
		        ],
		        "columnDefs": [
		            {
		                'targets' : 0,
		                'searchable' : false,
		                'orderable' : false,
		                'className' : 'dt-body-center',
		                'render' : function(data, type, full, meta) {
			                  return '<input bankAccountNo="' + full.bankAccountNo + '" bankName="' + full.bankName + '" type="checkbox" name="selectedCheckBox" class="select" value="' + full.id+ '">';
		                 }
		            }
		        ],
		        searching: false,
		        "ordering" : false,
		        "paging": false,
		        "info" : false
			 });
			
		},
		eventListener: function () {
			$(document).on('click','.btn-search',function(){
				permissionList.txtSearch = $('#txt-search').val();
				permissionList.dataTable.ajax.reload();
			});

			$(document).on('keyup','#txt-search',function(e){
				if(e.keyCode == 13){
					permissionList.txtSearch = $(this).val();
					permissionList.dataTable.ajax.reload();
				}
			});
			
			$(document).on("click",".btn-new",function(){
		    	 permissionList.addNewRow();
		    });
			
			$(document).on("click",".btn-save-all",function(){
				 if(!$(this).hasClass('disabled')){
					 console.log(permissionList.dataList);
					 permissionList.save();
				 }
		    });
			
			$(document).on("click",".btn-delete",function(){
				if(!$(this).hasClass('disabled'))
		    	{
		    		$('#popConfirm').modal('toggle');
			    	$('button[name="btnConfirm"]').on('click', function() {
			    		permissionList.deleteRole();
			        });
		    	}
		    });
		},
		updateDataList: function(self){
			console.log(self);
			//update data model
			var id = self.closest('tr').find("input[name='selectedCheckBox']").attr('value');
			var index = self.closest('td')[0].cellIndex;
			var value = self.val();
			var dataList = permissionList.dataList;
			console.log("id:" + id);
			console.log(dataList);
			dataList.forEach(function(item){
				if(item.id == id || item.idNew == id)
				{
					item[permissionList.fieldIndex[index]] = value;
					//enable save button
					$('.btn-save-all').removeClass('disabled');
					return;
				}
			});
		},
		renderDeleteButton: function(){
			if(permissionList.rows_selected.length > 0)
				$('.btn-delete').removeClass('disabled');
			else
				$('.btn-delete').addClass('disabled');
		},
		addNewRow: function(){
			$("#table_data .dataTables_empty").parents('tr').remove();
			//prepare data
			permissionList.crNewId ++;

			//append row to data table
			var tableRef = document.getElementById("table_data").getElementsByTagName('tbody')[0];
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.setAttribute('dataId', "new-" + permissionList.crNewId);
			var newCell0  = newRow.insertCell(0);
			var checkbox = document.createElement('input');
			checkbox.type = "checkbox";
			checkbox.name = "selectedCheckBox";
			checkbox.value = "new-" + permissionList.crNewId;
			checkbox.id = "id";  
			checkbox.class = "tabledit-input form-control input-sm";
			newCell0.appendChild(checkbox);
			
			var newCell1  = newRow.insertCell(1);
			var newText1  = document.createTextNode("");
			newCell1.appendChild(newText1);
			
			var newCell2  = newRow.insertCell(2);
			var newText2  = document.createTextNode("");
			newCell2.appendChild(newText2);	
			
			var newCell3  = newRow.insertCell(3);
			var newText3  = document.createTextNode("");
			newCell3.appendChild(newText3);	
			
			$('#table_data').Tabledit({
				editButton: false,
			    deleteButton: false,
			    hideIdentifier: true,
			    initedTableEditor: true,
			    columns: {
			    	identifier: [20, 'id'],
			        editable: [[1], [2]]
			    }
			});
			permissionList.dataList.push({
				id: 0,
				idNew: "new-" + permissionList.crNewId,
				code: "",
				name: "",
				description: ""
			});
			$('.btn-save-all').removeClass('disabled');
		},
		deleteRole: function(){
			var deleteIds = [];
			permissionList.rows_selected.forEach(function(id){
				console.log(id);
				if(id > 0)
				{
					deleteIds.push(id);
				}
				else
				{
					//remove in ui and model
					$("tr[dataid='" + id + "']").remove();
					for(i = 0; i < permissionList.dataList.length; i++){
			    		if(permissionList.dataList[i].idNew && permissionList.dataList[i].idNew == id){
			    			permissionList.dataList.splice(i, 1);
			    			return;
			    		}
			    	}
				}
			});
			if(deleteIds.length > 0) {
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/permission/delete',
		            data: "&id=" +  deleteIds,
		            success : function(result) {
		            	showPopupMessage(GLOBAL_MESSAGES['cashflow.common.deletedSuccessfully'], function(){
		            		location.reload();
		            	});	               
		            }
		        });
			}
		},
		save: function(){
			var formData = new FormData();
			formData.append('data', JSON.stringify(permissionList.dataList));
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/permission/save',
	            cache : false,
				contentType : false,
				processData : false,
				data : formData,
	            success : function(ajaxResult) {
	            	 var resultData = JSON.parse(ajaxResult);
	            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
            				 window.location.reload();
	            		 });
	            	 }
	            	 else
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
	            	 }
	            }
	        });
		}
	};
}());