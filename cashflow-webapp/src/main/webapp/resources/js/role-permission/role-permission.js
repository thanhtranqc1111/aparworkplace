/**
 * @author ThoaiNH 
 * create Oct 30, 2017
 */
(function () {
	rolePermission = {
		dataList: [],
		init: function () {
			
		},
		eventListener: function () {
			$(document).on('change','input[name="role-permission"]',function(){
				$('.btn-save-all').removeClass('disabled');
				var roleId = $(this).attr('roleId');
				var permissionId = $(this).attr('permissionId');
				var value = this.checked?1:0;
				var flag = 0;
				rolePermission.dataList.forEach(function(item){
					if(item.permissionId == permissionId && item.roleId == roleId)
					{
						item.value = value;
						flag = 1;
						return;
					}
				});
				if(flag == 0)
				{
					rolePermission.dataList.push({
						roleId: roleId,
						permissionId: permissionId,
						value: value
					})
				}
			});
			
			$(document).on('click','.btn-save-all',function(){
				if(!$(this).hasClass('disabled')){
					console.log(rolePermission.dataList);
					rolePermission.save();
				}
			});
			
			$(document).on('click','.btn-reset',function(){
				window.location.reload();
			});
		},
		save: function(){
			var formData = new FormData();
			formData.append('data', JSON.stringify(rolePermission.dataList));
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/grantPermission/save',
	            cache : false,
				contentType : false,
				processData : false,
				data : formData,
	            success : function(ajaxResult) {
	            	 var resultData = JSON.parse(ajaxResult);
	            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
            				 window.location.reload();
	            		 });
	            	 }
	            	 else
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
	            	 }
	            }
	        });
		}
	};
}());