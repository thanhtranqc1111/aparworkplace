/**
 * @author TruongHV 
 * create June 06, 2018
 */
(function() {
    purchaseForm = {
        init: function(types, callback) {
            Formio.createForm(document.getElementById('purchaseRequestForm'), {
                "components": [
                {
                    "clearOnHide": false,
                    "label": "Columns",
                    "input": false,
                    "tableView": false,
                    "key": "undefinedColumns",
                    "columns": [
                    {
                        "components": [
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "inputType": "text",
                            "inputMask": "",
                            "label": "ID",
                            "key": "id",
                            "placeholder": "",
                            "prefix": "",
                            "suffix": "",
                            "multiple": false,
                            "defaultValue": "",
                            "protected": false,
                            "unique": false,
                            "persistent": true,
                            "hidden": true,
                            "clearOnHide": true,
                            "spellcheck": true,
                            "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                            },
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "type": "textfield",
                            "labelPosition": "top",
                            "tags": [],
                            "properties": {},
                            "lockKey": true
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "inputType": "text",
                            "inputMask": "",
                            "label": "Approval No",
                            "key": "approvalCode",
                            "placeholder": "",
                            "prefix": "",
                            "suffix": "",
                            "multiple": false,
                            "defaultValue": "",
                            "protected": false,
                            "unique": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "spellcheck": true,
                            "validate": {
                            "required": true,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                            },
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "type": "textfield",
                            "labelPosition": "left-left",
                            "tags": [],
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "lockKey": true
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Approval Type",
                            "key": "approvalType",
                            "placeholder": "",
                            "data": {
                            "values": [],
                            "json": types,
                            "url": "",
                            "resource": "",
                            "custom": "",
                            "headers": [
                                {
                                "value": "",
                                "key": ""
                                }
                            ]
                            },
                            "dataSrc": "json",
                            "valueProperty": "code",
                            "defaultValue": "",
                            "refreshOn": "",
                            "filter": "",
                            "authenticate": false,
                            "template": "<span>{{ item.name }}</span>",
                            "multiple": false,
                            "protected": false,
                            "unique": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": true
                            },
                            "type": "select",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "lockKey": true,
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "selectValues": ""
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Validity From",
                            "key": "validityFrom",
                            "placeholder": "",
                            "format": "d/M/yyyy",
                            "enableDate": true,
                            "enableTime": false,
                            "defaultDate": "",
                            "datepickerMode": "day",
                            "datePicker": {
                            "showWeeks": true,
                            "startingDay": 0,
                            "initDate": "",
                            "minMode": "day",
                            "maxMode": "year",
                            "yearRows": 4,
                            "yearColumns": 5,
                            "minDate": null,
                            "maxDate": null,
                            "datepickerMode": "day"
                            },
                            "timePicker": {
                            "hourStep": 1,
                            "minuteStep": 1,
                            "showMeridian": true,
                            "readonlyInput": false,
                            "mousewheel": true,
                            "arrowkeys": true
                            },
                            "protected": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": true,
                            "custom": ""
                            },
                            "type": "datetime",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "lockKey": true,
                            "labelWidth": 30,
                            "labelMargin": 3
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Validity To",
                            "key": "validityTo",
                            "placeholder": "",
                            "format": "d/M/yyyy",
                            "enableDate": true,
                            "enableTime": false,
                            "defaultDate": "",
                            "datepickerMode": "day",
                            "datePicker": {
                            "showWeeks": true,
                            "startingDay": 0,
                            "initDate": "",
                            "minMode": "day",
                            "maxMode": "year",
                            "yearRows": 4,
                            "yearColumns": 5,
                            "minDate": null,
                            "maxDate": null,
                            "datepickerMode": "day"
                            },
                            "timePicker": {
                            "hourStep": 1,
                            "minuteStep": 1,
                            "showMeridian": true,
                            "readonlyInput": false,
                            "mousewheel": true,
                            "arrowkeys": true
                            },
                            "protected": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": true,
                            "custom": ""
                            },
                            "type": "datetime",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Approved Date",
                            "key": "approvedDate",
                            "placeholder": "",
                            "format": "d/M/yyyy",
                            "enableDate": true,
                            "enableTime": false,
                            "defaultDate": "",
                            "datepickerMode": "day",
                            "datePicker": {
                            "showWeeks": true,
                            "startingDay": 0,
                            "initDate": "",
                            "minMode": "day",
                            "maxMode": "year",
                            "yearRows": 4,
                            "yearColumns": 5,
                            "minDate": null,
                            "maxDate": null,
                            "datepickerMode": "day"
                            },
                            "timePicker": {
                            "hourStep": 1,
                            "minuteStep": 1,
                            "showMeridian": true,
                            "readonlyInput": false,
                            "mousewheel": true,
                            "arrowkeys": true
                            },
                            "protected": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": true,
                            "custom": ""
                            },
                            "type": "datetime",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "lockKey": true
                        }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0
                    },
                    {
                        "components": [
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Application Date",
                            "key": "applicationDate",
                            "placeholder": "",
                            "format": "d/M/yyyy",
                            "enableDate": true,
                            "enableTime": false,
                            "defaultDate": "",
                            "datepickerMode": "day",
                            "datePicker": {
                            "showWeeks": true,
                            "startingDay": 0,
                            "initDate": "",
                            "minMode": "day",
                            "maxMode": "year",
                            "yearRows": 4,
                            "yearColumns": 5,
                            "minDate": null,
                            "maxDate": null,
                            "datepickerMode": "day"
                            },
                            "timePicker": {
                            "hourStep": 1,
                            "minuteStep": 1,
                            "showMeridian": true,
                            "readonlyInput": false,
                            "mousewheel": true,
                            "arrowkeys": true
                            },
                            "protected": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": false,
                            "custom": ""
                            },
                            "type": "datetime",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "lockKey": true
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "label": "Currency",
                            "key": "currencyCode",
                            "placeholder": "",
                            "data": {
                            "values": [
                                {
                                "value": "JPY",
                                "label": "JPY"
                                },
                                {
                                "value": "SGD",
                                "label": "SGD"
                                },
                                {
                                "value": "THB",
                                "label": "THB"
                                },
                                {
                                "value": "USD",
                                "label": "USD"
                                },
                                {
                                "value": "VND",
                                "label": "VND"
                                }
                            ],
                            "json": "",
                            "url": "",
                            "resource": "",
                            "custom": ""
                            },
                            "dataSrc": "values",
                            "valueProperty": "",
                            "defaultValue": "",
                            "refreshOn": "",
                            "filter": "",
                            "authenticate": false,
                            "template": "<span>{{ item.label }}</span>",
                            "multiple": false,
                            "protected": false,
                            "unique": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": false
                            },
                            "type": "select",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "lockKey": true,
                            "labelWidth": 30,
                            "labelMargin": 3
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "inputType": "number",
                            "label": "Amount",
                            "key": "includeGstOriginalAmount",
                            "placeholder": "",
                            "prefix": "",
                            "suffix": "",
                            "defaultValue": "",
                            "protected": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "validate": {
                            "required": false,
                            "min": "",
                            "max": "",
                            "step": "any",
                            "integer": "",
                            "multiple": "",
                            "custom": ""
                            },
                            "type": "number",
                            "labelPosition": "left-left",
                            "tags": [],
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "lockKey": true
                        },
                        {
                            "autofocus": false,
                            "input": true,
                            "tableView": true,
                            "inputType": "text",
                            "inputMask": "",
                            "label": "Applicant",
                            "key": "applicant",
                            "placeholder": "",
                            "prefix": "",
                            "suffix": "",
                            "multiple": false,
                            "defaultValue": "",
                            "protected": false,
                            "unique": false,
                            "persistent": true,
                            "hidden": false,
                            "clearOnHide": true,
                            "spellcheck": true,
                            "validate": {
                            "required": false,
                            "minLength": "",
                            "maxLength": "",
                            "pattern": "",
                            "custom": "",
                            "customPrivate": false
                            },
                            "conditional": {
                            "show": "",
                            "when": null,
                            "eq": ""
                            },
                            "type": "textfield",
                            "labelPosition": "left-left",
                            "tags": [],
                            "properties": {},
                            "labelWidth": 30,
                            "labelMargin": 3,
                            "lockKey": true
                        }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0
                    }
                    ],
                    "type": "columns",
                    "hideLabel": true,
                    "tags": [],
                    "conditional": {
                    "show": "",
                    "when": null,
                    "eq": ""
                    },
                    "properties": {}
                },
                {
                    "autofocus": false,
                    "input": true,
                    "tableView": true,
                    "label": "Purpose",
                    "key": "purpose",
                    "placeholder": "",
                    "prefix": "",
                    "suffix": "",
                    "rows": 3,
                    "multiple": false,
                    "defaultValue": "",
                    "protected": false,
                    "persistent": true,
                    "hidden": false,
                    "wysiwyg": false,
                    "clearOnHide": true,
                    "spellcheck": true,
                    "validate": {
                    "required": false,
                    "minLength": "",
                    "maxLength": "",
                    "pattern": "",
                    "custom": ""
                    },
                    "type": "textarea",
                    "labelPosition": "top",
                    "tags": [],
                    "conditional": {
                    "show": "",
                    "when": null,
                    "eq": ""
                    },
                    "properties": {},
                    "labelWidth": 30,
                    "labelMargin": 3,
                    "lockKey": true
                },
                {
                    "autofocus": false,
                    "input": true,
                    "tableView": true,
                    "label": "Attachment",
                    "key": "attachment",
                    "image": false,
                    "imageSize": "200",
                    "placeholder": "",
                    "multiple": false,
                    "defaultValue": "",
                    "protected": false,
                    "persistent": true,
                    "hidden": false,
                    "clearOnHide": true,
                    "filePattern": "*",
                    "fileMinSize": "0KB",
                    "fileMaxSize": "1GB",
                    "type": "file",
                    "labelPosition": "top",
                    "tags": [],
                    "conditional": {
                    "show": "",
                    "when": null,
                    "eq": ""
                    },
                    "properties": {},
                    "lockKey": true,
                    "storage": "url",
                    "url": "https://camunda-dev.tctav.com/eform/content/temporary/create"
                },
                {
                    "autofocus": false,
                    "input": true,
                    "label": "Save",
                    "tableView": false,
                    "key": "save",
                    "size": "md",
                    "leftIcon": "",
                    "rightIcon": "",
                    "block": false,
                    "action": "event",
                    "disableOnInvalid": false,
                    "theme": "primary",
                    "type": "button",
                    "tags": [],
                    "conditional": {
                    "show": "",
                    "when": null,
                    "eq": ""
                    },
                    "properties": {},
                    "event": "savePurchaseRequest",
                    "customClass": "pull-right"
                },
                {
                    "autofocus": false,
                    "input": true,
                    "label": "Submit",
                    "tableView": false,
                    "key": "submit",
                    "size": "md",
                    "leftIcon": "",
                    "rightIcon": "",
                    "block": false,
                    "action": "event",
                    "disableOnInvalid": false,
                    "theme": "primary",
                    "type": "button",
                    "tags": [],
                    "conditional": {
                    "show": "",
                    "when": null,
                    "eq": ""
                    },
                    "properties": {},
                    "event": "submitPurchaseRequest",
                    "customClass": "pull-right button-margin"
                }
                ],
                "page": 0,
                "display": "form"
            }).then(calback);
        }
    }
}());