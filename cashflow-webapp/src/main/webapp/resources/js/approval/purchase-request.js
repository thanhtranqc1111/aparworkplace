/**
 * @author ThoaiNH 
 * create Jan 12, 2018
 */
(function () {
	purchaseRequest = {
		dataTable: null,
		rows_selected: [],
		approvalAutoComplete: null,
		dataTableRowClickable: true,
		form: null,
		formatDate: function(date) {
			var newDate = new Date(date);
			var date = newDate.getDate();
			var month = newDate.getMonth() + 1;
			var year = newDate.getFullYear();
			return date + '/' + month + '/' + year;
		},
		getFormUrl: function() {
			// var eformPath = 'http://localhost:8080';
			var eformPath = 'http://camunda.local.tctav.com';
			switch (window.location.hostname) {
			case 'cashflow.local.tctav.com':
			  eformPath = 'http://camunda.local.tctav.com';
			  break;
			case 'cashflow-dev.tctav.com':
			  eformPath = 'https://camunda-dev.tctav.com';
			  break;
			case 'cashflow.tctav.com':
			  eformPath = 'https://camunda.tctav.com';
			  break;
			default:
			  break;
			}
			return eformPath;
		},
		isDate: function(string) {
			// string: format ISOString
			console.log(string);
			if (!moment(string, 'YYYY-MM-DDTHH:mm:ss.SSSSZ').isValid()) {
			  return false;
			}
			var newDate = '';
			try {
			  newDate = new Date(string).toISOString();
			} catch (error) {
			  return false;
			}
			return newDate === string;
		},
		checkType: function(data) {
			let type = 'String';
			switch (typeof data) {
			  case 'string':
				type = 'String';
				if (purchaseRequest.isDate(data)) {
				  data = moment(data).format('YYYY-MM-DD');
				  type = 'Date';
				}
				break;
			  case 'number':
				type = 'Long';
				break;
			  case 'boolean':
				type = 'Boolean';
				break;
			  case 'object':
				type = 'String';
				data = JSON.stringify(data);
				break;
			  default:
				break;
			}
			return { type, data };
		},
		transformFormDataToCamundaData: function(formKey, data) {
			// Sanity check
			const isObj = typeof data === "object";
			if (!isObj) {
			  throw new Error("Please submit data obj");
			}
		  
			const keys = formKey.split('_');
			let formId = '';
			if (keys.length === 1) {
			  formId = keys[0].substring(0, 3).toUpperCase();
			} else {
				for (var key in keys) {
					formId += keys[key][0];	
				}
			}
		  
			// Do transformFormDataToCamundaData
			const variables = Object.keys(data).reduce((carry, key) => {
			  const _val = data[key];
			  const obj =  purchaseRequest.checkType(_val);
			  const valObj = {
				value: obj.data,
				type: obj.type,
				valueInfo: {}
			  }
			  carry[`${formId}_${key}`] = valObj;
			  return carry;
			}, {});
		  
			return { variables };
		  },
		submitPurchaseRequest: function(data) {
			console.log('submitPurchaseRequest', data);
			var url = purchaseRequest.getFormUrl() + '/eform/process-definition/key/Purchase_Request/tenant-id/TTV/submit-form';
			data = purchaseRequest.transformFormDataToCamundaData('Purchase_Request', data);
			console.log('transformFormDataToCamundaData', data);
			$.ajax({
				type: 'POST',
				url: url,
				headers: { 
					'Content-Type': 'application/json' 
				},
				dataType: 'json',
				data: JSON.stringify(data),
				success: function(result) {
					console.log('submitPurchaseRequest result', result);
					showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
						window.location.reload();
					});
				}, 
				error: function(err) {
					var error = JSON.parse(err.responseText);
					if (error.message) {
						var arrMessages = [error.message];
						$(".alert-warning").showAlertMessage(arrMessages);
					}
				}
			});
		},
		savePurchaseRequest: function(data) {
			console.log('savePurchaseRequest', data);
			data.applicationDate = purchaseRequest.formatDate(data.applicationDate);
			data.validityFrom = purchaseRequest.formatDate(data.validityFrom);
			data.validityTo = purchaseRequest.formatDate(data.validityTo);
			data.approvedDate = purchaseRequest.formatDate(data.approvedDate);
			data.id = 0;
			delete data.save;
			$.ajax({
				type : "POST",
				url : CONTEXT_PATH + '/approvalPurchaseRequest/save',
				data: data,
				success : function(ajaxResult) {
				console.log(ajaxResult);
				var search = JSON.parse(ajaxResult);
				console.log(search);
				if(search.STATUS == "SUCCESS") {
					$(".alert-warning").hide();
					showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
						window.location.reload();
					});
				}
				else
				{
					var arrMessages = [];
					search.RESULT.forEach(function(item){
						arrMessages.push(item.defaultMessage);
					});
					$(".alert-warning").showAlertMessage(arrMessages);
				}
				}
			});
		},
		init: function () {
			$('input.ap-big-decimal').setupInputNumberMask({allowMinus: false});
			$('#ar_table_data .ap-big-decimal').setupInputNumberMask({
        		allowMinus: false
        	});
			purchaseRequest.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/approvalPurchaseRequest/api/getAllPagingApproval",
		            type: "GET",
		            data: function ( d ) {
		                d.approvalType = $('#searh-approvalType').val();
		                d.approvalCode = $('#search-approvalType').val();
		                d.validityFrom = $('#searh-validityFrom').val();
		                d.validityTo = $('#searh-validityTo').val();
		                d.status = $('#select-status').val();
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData']+'</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data) {
		            	purchaseRequest.rows_selected = [];
		            	$('#table_data').tableCheckable(purchaseRequest.dataTable, purchaseRequest.rows_selected, purchaseRequest.renderDeleteButton);
		            	$('.btn-delete-approval').addClass('disabled');
		            	hideAjaxStatus();
		            	/*$("#table_data tbody tr").each(function(){
		            		if($(this).find('input[type="checkbox"]').length > 0)
		            		{
		            			$(this).addClass('editable');
		            		}
		            	});*/
		            	$("#table_data tbody tr td:not('.dataTables_empty')").attr('title', GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']).css('cursor','pointer');
		            }
		        },
		        columns: [
		        	{
			            "data" : "id",
			            "width" : "1%"
			        }, 
			        {
			            "data" : "approvalType",
			            "orderable": false,
			            "width" : "10%"
			        }, 
			        {
			            "data" : "approvalCode",
			            "orderable": true,
			            "width" : "8%",
			            "render": function(data, type,full, meta){
			            	if(full.apInVoicesApplied.length > 0)
			            	{
			            		return "<a target='blank' href='/apinvoice/detail/" + full.apInVoicesApplied[0] + "/with/" + data + "'>" + data + "</a>";
			            	}
			            	else
			            		return data;
			            }
			        },
			        {
			            "data" : "applicationDate",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return data.dayOfMonth + "/" + (data.month + 1) + "/" + data.year;
			            	else
			            		return "";
			             },
				         "width" : "8%"
			        },
			        {
			            "data" : "id",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return full.validityFrom.dayOfMonth + "/" + (full.validityFrom.month + 1) + "/" + full.validityFrom.year + " ~ " + full.validityTo.dayOfMonth + "/" + (full.validityTo.month + 1) + "/" + full.validityTo.year;
			             },
				         "width" : "10%"
			        },
			        {
			            "data" : "currencyCode",
			            "orderable": false,
			            "width" : "5%"
			        },	
			        {
			            "data" : "includeGstOriginalAmount",
			            "orderable": true,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "purpose",
			            "orderable": false,
			            "width" : "17%"
			        },
			        {
			            "data" : "applicant",
			            "orderable": false,
			            "width" : "10%"
			        },
			        {
			            "data" : "",
			            "orderable": false,
			            "width" : "8%",
			            "render": function(data, type,full, meta){
			            	if(full.status)
			            	{
			            		return "<div class='ap-status  api-" + full.status + "'>" + full.statusValue + "</div>";
			            	}
			            	else
			            		return "";
			            }
			        },
			        {
			            "data" : "approvedDate",
			            "orderable": false	,
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return data.dayOfMonth + "/" + (data.month + 1) + "/" + data.year;
			            	else
			            		return "";
			             },
				         "width" : "8%"
			        },
			        {
			            "data" : "",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.apInVoicesApplied.length > 0 || full.payrollsApplied.length > 0 || full.reimbursementsApplied.length > 0){
					        	var html = "";
					        	$.each(full.apInVoicesApplied, function(i, obj) {
					        		html += "<a href='/apinvoice/detail/" + obj + "' target='_blank'>" + obj + "</a>";
				            		if ( i != full.apInVoicesApplied.length - 1){
				            			html += ", ";
				            		}
				            	});
					        	
					        	if(html.length > 0 && full.payrollsApplied.length > 0)
				        			html += ", ";
					        	
					        	$.each(full.payrollsApplied, function(i, obj) {
					        		html += "<a href='/payroll/detail/" + obj + "' target='_blank'>" + obj + "</a>";
				            		if ( i != full.payrollsApplied.length - 1){
				            			html += ", ";
				            		}
				            	});
					        	
					        	if(html.length > 0 && full.reimbursementsApplied.length > 0)
				        			html += ", ";
					        	
					        	$.each(full.reimbursementsApplied, function(i, obj) {
					        		html += "<a href='/reimbursement/detail/" + obj + "' target='_blank'>" + obj + "</a>";
				            		if ( i != full.reimbursementsApplied.length - 1){
				            			html += ", ";
				            		}
				            	});
					        	return html;
				        	}
				        	else {
				        		return "";
				        	}
			             },
				         "width" : "13%"
			        }
		        ],
		        "searching": false	,
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.approvalPurchaseRequest.all']]],
				"lengthChange" : true,
				"order": [[ 2, "asc" ]],
		        "columnDefs": [
		            {
		            	'targets' : 0,
		                'searchable' : false,
		                'orderable' : false,
		                'className' : 'dt-body-center',
		                'render' : function(data, type, full, meta) {
		                	if(full.apInVoicesApplied.length > 0 || full.payrollsApplied.length > 0  || full.reimbursementsApplied.length > 0 || full.status == '002' || full.status == '003')
		                	  return '<input type="hidden" class="approval-id" value="' + data +'" />';
		                	else
		                	  return '<input type="checkbox" name="selectedCheckBox" class="select" value="' + $('<div/>').text(data).html() + '">';
		                }
		            },
		            {
		                "targets": [3,4,6,9],
		                "className": "text-right",
		            }
		        ]
			});

		    $('.ap-date-picker').datepicker({
		    	format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
		        todayHighlight: true
		    });
			$('.ap-panel-detail .ap-date-picker').prop('disabled', true);
			
			// get approval types
			$.ajax({
				type : "GET",
				url : '/approvalPurchaseRequest/api/findAllApprovalTypePurchaseMasters',
				success : function(types) {
				   console.log('ajaxResult', types);
			
					// create form
					purchaseForm.init(types, function(form) {
						purchaseRequest.form = form;
						form.on('submitPurchaseRequest', (submission) => {
							purchaseRequest.submitPurchaseRequest(submission);
						});
						form.on('savePurchaseRequest', (submission) => {
							purchaseRequest.savePurchaseRequest(submission);
						});
						form.on('error', (errors) => {
							console.log('We have errors!');
						});
					});
				}
			});
		},
		eventListener: function () {
			// Handle click on table cells with checkboxes
			if(GLOBAL_PERMISSION["PMS-003"])
			{
			    $('#table_data').on('dblclick', 'tbody tr', function(e){
			    	// get row data
					function getRowData(row) {
						//parse Validity Period
						var strValidityPeriod = row.children('td:nth-child(5)').text();
				    	var strVals = strValidityPeriod.split("~");
						return {
							approvalCode: row.children('td:nth-child(3)').text(),
							approvalType: row.children('td:nth-child(2)').text(),
							applicationDate: row.children('td:nth-child(4)').text(),
							validityFrom: getDateFromString(strVals[0]),
							validityTo: getDateFromString(strVals[1]),
							currency: row.children('td:nth-child(6)').text(),
							includeGstOriginalAmount: row.children('td:nth-child(7)').text(),
							purpose: row.children('td:nth-child(8)').text(),
							applicant: row.children('td:nth-child(9)').text(),
							approvedDate: getDateFromString(row.children('td:nth-child(10)').text()),
							id: row.children('td:nth-child(1)').find('input').val()
						}
					}

			    	if(purchaseRequest.dataTableRowClickable){
						var rowData = getRowData($(this));
				    	purchaseRequest.newApproval();
				    	$('#mode').val("Edit");
				    	$('#approvalType').val(rowData.approvalType);
				    	$('#approvalCode').val(rowData.approvalCode).attr("readonly", true);
				    	$('#applicationDate').val(rowData.applicationDate);
				    	$('#validityFrom').datepicker('setDate', rowData.validityFrom);
				    	$('#validityTo').datepicker('setDate', rowData.validityTo);
				    	$('#input-approval-currency').val(rowData.currency);
				    	$('#amount').val(rowData.includeGstOriginalAmount);
				    	$('#purpose').val(rowData.purpose);
				    	$('#applicant').val(rowData.applicant);
				    	$('#approvedDate').datepicker('setDate', rowData.approvalDate);
						$('#id').val(rowData.id);
						
						// set formio data
						purchaseRequest.form.submission = {
							data: rowData
						};

						// disable data[approvalCode]
						$('input[name="data[approvalCode]"]').attr('disabled', 'disabled');
			    	}
			    });
			}
		    $(document).on("click",".btn-new-approval",function(){
		    	 purchaseRequest.newApproval();
		    	 $('#mode').val("New");
		    	 $('#id').val(0);
		    	 purchaseRequest.dataTableRowClickable = true;
				//  purchaseRequest.dataTableRowClickable = false;

				//  set default for data: id
				purchaseRequest.form.submission = {
					data: {
						id: 0
					}
				};
				$('#purchaseRequestForm').goTo();
				$('input[name="data[approvalCode]"]').focus();
				$('input[name="data[approvalCode]"]').attr('disabled', false);
		    });
		    
		    $(document).on("click",".btn-cancel-approval",function(){
		    	purchaseRequest.cancelFormApproval();
		    });
		    
		    $(document).on("click",".btn-save-approval",function(){
		    	if(!$(this).hasClass('disabled'))
		    	{
		    		$.ajax({
			            type : "POST",
			            url : CONTEXT_PATH + '/approvalPurchaseRequest/save',
			            data: {
		    				id: $("#id").val(),
		    				applicationDate: $("#applicationDate").val(),
		    				approvalCode: $("#approvalCode").val(),
		    				approvalType: $('#approvalType').val(),
		    				validityFrom: $("#validityFrom").val(),
		    				validityTo: $("#validityTo").val(),
		    				includeGstOriginalAmount: $("#amount").getNumberValue(),
		    				approvedDate: $("#approvedDate").val(),
		    				applicant: $("#applicant").val(),
		    				purpose: $("#purpose").val(),
		    				currencyCode: $("#input-approval-currency").val(),
		    				mode: $("#mode").val()
			            },
			            success : function(ajaxResult) {
			               console.log(ajaxResult);
			               var search = JSON.parse(ajaxResult);
			               console.log(search);
			               if(search.STATUS == "SUCCESS") {
			            	   $(".alert-warning").hide();
			            	   showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
			            		   window.location.reload();
			            	   });
			               }
			               else
			               {
			            	   var arrMessages = [];
			            	   search.RESULT.forEach(function(item){
			            		   arrMessages.push(item.defaultMessage);
			            	   });
			            	   $(".alert-warning").showAlertMessage(arrMessages);
			               }
			            }
			        });
		    	}
		    });
		    
		    $(document).on("click",".btn-delete-approval",function(){
		    	if(!$(this).hasClass('disabled'))
		    	{
		    		$('#popConfirm').modal('toggle');
			    	$('button[name="btnConfirm"]').on('click', function() {
			    		purchaseRequest.deleteApproval();
			        });
		    	}
		    });
		    
		    $(document).on("click",".btn-search",function(){
		    	console.log("---searh-approvalType:" + $('#searh-approvalType').val());
		    	purchaseRequest.dataTable.ajax.reload();
		    });
		    
		    $(document).on("click",".btn-export",function(){
		    	if(purchaseRequest.dataTable.page.info().recordsDisplay == 0)
		    	{
		    		showPopupMessage(GLOBAL_MESSAGES['noRecordWhenExport']);
		    	} else {
		    		var approvalType = $('#searh-approvalType').val();
	                var approvalCode = $('#search-approvalType').val();
	                var validityFrom = $('#searh-validityFrom').val();
	                var validityTo = $('#searh-validityTo').val();
	                var url = "/approvalPurchaseRequest/export?approvalType=" + approvalType + 
	                		  "&approvalCode=" + approvalCode +
	                		  "&validityFrom=" + validityFrom +
	                		  "&validityTo=" + validityTo;
	                window.location = url;
		    	}
		    });
		},
		newApproval: function(){
			$('.ap-panel-detail input, .ap-panel-detail textarea').val("").attr("readonly", false);
			$('.ap-panel-detail button').removeClass('disabled');
			$('#approvalType').attr('disabled',false);
			$('#input-approval-currency').attr('disabled',false);
			$('#approvalForm').goTo();
			$("#approvalCode").focus();
			$("#link-view-detail").hide();
			$('.ap-panel-detail .ap-date-picker').prop('disabled', false);
			//default is today
			 $('#approvedDate').datepicker('setDate', new Date());
			 $('#ap-approval-detail').hide();
		},
		cancelFormApproval: function(){
			$('.ap-panel-detail input,.ap-panel-detail textarea').val("").attr("readonly", true);
			$('.ap-panel-detail button').addClass('disabled');
			$('.ap-panel-detail .ap-date-picker').prop('disabled', true);
			$("#link-view-detail").hide();
			$('#approvalType').attr('disabled',true);
			$('#input-approval-currency').attr('disabled',true);
			purchaseRequest.dataTableRowClickable = true;
			$('#ap-approval-detail').hide();
		},
		deleteApproval: function(){
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/approvalPurchaseRequest/delete',
	            data: "&id=" +  purchaseRequest.rows_selected,
	            success : function(result) {
	               console.log(result);
	               showPopupMessage(GLOBAL_MESSAGES['cashflow.common.deletedSuccessfully'], function(){
            		   window.location.reload();
            	   });
	            }
	        });
		},
		renderDeleteButton: function(){
			if(purchaseRequest.rows_selected.length > 0)
				$('.btn-delete-approval').removeClass('disabled');
			else
				$('.btn-delete-approval').addClass('disabled');
		}
	};
}());