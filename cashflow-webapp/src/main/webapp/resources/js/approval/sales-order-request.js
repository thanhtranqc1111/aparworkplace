/**
 * @author ThoaiNH 
 * create Jan 12, 2018
 */
(function () {
	salesOrderRequest = {
		dataTable: null,
		rows_selected: [],
		approvalAutoComplete: null,
		dataTableRowClickable: true,
		init: function () {
			$('input.ap-big-decimal').setupInputNumberMask({allowMinus: false});
			$('#ar_table_data .ap-big-decimal').setupInputNumberMask({
        		allowMinus: false
        	});
			salesOrderRequest.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/approvalSalesOrderRequest/api/getAllPagingApproval",
		            type: "GET",
		            data: function ( d ) {
		                d.approvalType = $('#searh-approvalType').val();
		                d.approvalCode = $('#search-approvalType').val();
		                d.validityFrom = $('#searh-validityFrom').val();
		                d.validityTo = $('#searh-validityTo').val();
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData']+'</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data) {
		            	salesOrderRequest.rows_selected = [];
		            	$('#table_data').tableCheckable(salesOrderRequest.dataTable, salesOrderRequest.rows_selected, salesOrderRequest.renderDeleteButton);
		            	$('.btn-delete-approval').addClass('disabled');
		            	hideAjaxStatus();
		            	/*$("#table_data tbody tr").each(function(){
		            		if($(this).find('input[type="checkbox"]').length > 0)
		            		{
		            			$(this).addClass('editable');
		            		}
		            	});*/
		            	$("#table_data tbody tr td:not('.dataTables_empty')").attr('title', GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']).css('cursor','pointer');
		            }
		        },
		        columns: [
		        	{
			            "data" : "id",
			            "width" : "1%"
			        }, 
			        {
			            "data" : "approvalType",
			            "orderable": false,
			            "width" : "10%"
			        }, 
			        {
			            "data" : "approvalCode",
			            "orderable": true,
			            "width" : "8%",
			            "render": function(data, type,full, meta){
			            	if(full.arInVoicesApplied.length > 0)
			            	{
			            		return "<a target='blank' href='/arinvoice/detail/" + full.arInVoicesApplied[0] + "/" + data + "'>" + data + "</a>";
			            	}
			            	else
			            		return data;
			            }
			        },
			        {
			            "data" : "applicationDate",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return formatCalendar(data);
			            	else
			            		return "";
			             },
				         "width" : "8%"
			        },
			        {
			            "data" : "id",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatCalendar(full.validityFrom) + " ~ " + formatCalendar(full.validityTo);
			             },
				         "width" : "10%"
			        },
			        {
			            "data" : "currencyCode",
			            "orderable": false,
			            "width" : "5%"
			        },	
			        {
			            "data" : "includeGstOriginalAmount",
			            "orderable": true,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "purpose",
			            "orderable": false,
			            "width" : "17%"
			        },
			        {
			            "data" : "applicant",
			            "orderable": false,
			            "width" : "10%"
			        },
			        {
			            "data" : "approvedDate",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return formatCalendar(data);
			             },
				         "width" : "8%"
			        },
			        {
			            "data" : "",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.arInVoicesApplied.length > 0){
					        	var html = "";
					        	$.each(full.arInVoicesApplied, function(i, obj) {
					        		html += "<a href='/arinvoice/detail/" + obj + "' target='_blank'>" + obj + "</a>";
				            		if ( i != full.arInVoicesApplied.length - 1){
				            			html += ", ";
				            		}
				            	});
					        	return html;
				        	}
				        	else {
				        		return "";
				        	}
			             },
				         "width" : "13%"
			        }
		        ],
		        "searching": false	,
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.approvalSalesOrderRequest.all']]],
				"lengthChange" : true,
				"order": [[ 9, "asc" ]],
		        "columnDefs": [
		            {
		            	'targets' : 0,
		                'searchable' : false,
		                'orderable' : false,
		                'className' : 'dt-body-center',
		                'render' : function(data, type, full, meta) {
		                  if(full.arInVoicesApplied.length > 0)
		                	  return '<input type="hidden" class="approval-id" value="' + data +'" />';
		                  else
		                	  return '<input type="checkbox" name="selectedCheckBox" class="select" value="' + $('<div/>').text(data).html() + '">';
		                }
		            },
		            {
		            	"targets": [3,4,6,9],
		                "className": "text-right",
		            }
		        ]
			});

		    $('.ap-date-picker').datepicker({
		    	format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
		        todayHighlight: true
		    });
		    $('.ap-panel-detail .ap-date-picker').prop('disabled', true);
		},
		eventListener: function () {
			// Handle click on table cells with checkboxes
			if(GLOBAL_PERMISSION["PMS-003"])
			{
			    $('#table_data').on('dblclick', 'tbody tr', function(e){
			    	if(salesOrderRequest.dataTableRowClickable){
				    	salesOrderRequest.newApproval();
				    	$('#mode').val("Edit");
				    	$('#approvalType').val($(this).children('td:nth-child(2)').text());
				    	$('#approvalCode').val($(this).children('td:nth-child(3)').text()).attr("readonly", true);
				    	$('#applicationDate').val($(this).children('td:nth-child(4)').text());
				    	//parse Validity Period
				    	var strValidityPeriod = $(this).children('td:nth-child(5)').text();
				    	var strVals = strValidityPeriod.split("~");
				    	$('#validityFrom').datepicker('setDate', getDateFromString(strVals[0]));
				    	$('#validityTo').datepicker('setDate', getDateFromString(strVals[1]));
				    	$('#input-approval-currency').val($(this).children('td:nth-child(6)').text());
				    	$('#amount').val($(this).children('td:nth-child(7)').text());
				    	$('#purpose').val($(this).children('td:nth-child(8)').text());
				    	$('#applicant').val($(this).children('td:nth-child(9)').text());
				    	$('#approvedDate').datepicker('setDate', getDateFromString($(this).children('td:nth-child(10)').text()));
				    	$('#id').val($(this).children('td:nth-child(1)').find('input').val());
			    	}
			    });
			}
		    $(document).on("click",".btn-new-approval",function(){
		    	 salesOrderRequest.newApproval();
		    	 $('#mode').val("New");
		    	 $('#id').val(0);
		    	 salesOrderRequest.dataTableRowClickable = false;
		    });
		    
		    $(document).on("click",".btn-cancel-approval",function(){
		    	salesOrderRequest.cancelFormApproval();
		    });
		    
		    $(document).on("click",".btn-save-approval",function(){
		    	if(!$(this).hasClass('disabled'))
		    	{
		    		$.ajax({
			            type : "POST",
			            url : CONTEXT_PATH + '/approvalSalesOrderRequest/save',
			            data: {
		    				id: $("#id").val(),
		    				applicationDate: $("#applicationDate").val(),
		    				approvalCode: $("#approvalCode").val(),
		    				approvalType: $('#approvalType').val(),
		    				validityFrom: $("#validityFrom").val(),
		    				validityTo: $("#validityTo").val(),
		    				includeGstOriginalAmount: $("#amount").getNumberValue(),
		    				approvedDate: $("#approvedDate").val(),
		    				applicant: $("#applicant").val(),
		    				purpose: $("#purpose").val(),
		    				currencyCode: $("#input-approval-currency").val(),
		    				mode: $("#mode").val()
			            },
			            success : function(ajaxResult) {
			               console.log(ajaxResult);
			               var search = JSON.parse(ajaxResult);
			               console.log(search);
			               if(search.STATUS == "SUCCESS") {
			            	   $(".alert-warning").hide();
			            	   showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
			            		   window.location.reload();
			            	   });
			               }
			               else
			               {
			            	   var arrMessages = [];
			            	   search.RESULT.forEach(function(item){
			            		   arrMessages.push(item.defaultMessage);
			            	   });
			            	   $(".alert-warning").showAlertMessage(arrMessages);
			               }
			            }
			        });
		    	}
		    });
		    
		    $(document).on("click",".btn-delete-approval",function(){
		    	if(!$(this).hasClass('disabled'))
		    	{
		    		$('#popConfirm').modal('toggle');
			    	$('button[name="btnConfirm"]').on('click', function() {
			    		salesOrderRequest.deleteApproval();
			        });
		    	}
		    });
		    
		    $(document).on("click",".btn-search",function(){
		    	console.log("---searh-approvalType:" + $('#searh-approvalType').val());
		    	salesOrderRequest.dataTable.ajax.reload();
		    });
		    
		    $(document).on("click",".btn-export",function(){
		    	if(salesOrderRequest.dataTable.page.info().recordsDisplay == 0)
		    	{
		    		showPopupMessage(GLOBAL_MESSAGES['noRecordWhenExport']);
		    	} else {
		    		var approvalType = $('#searh-approvalType').val();
	                var approvalCode = $('#search-approvalType').val();
	                var validityFrom = $('#searh-validityFrom').val();
	                var validityTo = $('#searh-validityTo').val();
	                var url = "/approvalSalesOrderRequest/export?approvalType=" + approvalType + 
	                		  "&approvalCode=" + approvalCode +
	                		  "&validityFrom=" + validityFrom +
	                		  "&validityTo=" + validityTo;
	                window.location = url;
		    	}
		    });
		},
		newApproval: function(){
			$('.ap-panel-detail input, .ap-panel-detail textarea').val("").attr("readonly", false);
			$('.ap-panel-detail button').removeClass('disabled');
			$('#approvalType').attr('disabled',false);
			$('#input-approval-currency').attr('disabled',false);
			$('#approvalForm').goTo();
			$("#approvalCode").focus();
			$("#link-view-detail").hide();
			$('.ap-panel-detail .ap-date-picker').prop('disabled', false);
			//default is today
			 $('#approvedDate').datepicker('setDate', new Date());
			 $('#ap-approval-detail').hide();
		},
		cancelFormApproval: function(){
			$('.ap-panel-detail input,.ap-panel-detail textarea').val("").attr("readonly", true);
			$('.ap-panel-detail button').addClass('disabled');
			$('.ap-panel-detail .ap-date-picker').prop('disabled', true);
			$("#link-view-detail").hide();
			$('#approvalType').attr('disabled',true);
			$('#input-approval-currency').attr('disabled',true);
			salesOrderRequest.dataTableRowClickable = true;
			$('#ap-approval-detail').hide();
		},
		deleteApproval: function(){
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/approvalSalesOrderRequest/delete',
	            data: "&id=" +  salesOrderRequest.rows_selected,
	            success : function(result) {
	               showPopupMessage(GLOBAL_MESSAGES['cashflow.common.deletedSuccessfully'], function(){
            		   window.location.reload();
            	   });
	            }
	        });
		},
		renderDeleteButton: function(){
			if(salesOrderRequest.rows_selected.length > 0)
				$('.btn-delete-approval').removeClass('disabled');
			else
				$('.btn-delete-approval').addClass('disabled');
		}
	};
}());