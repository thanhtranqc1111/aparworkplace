/**
 * @author ThoaiNH 
 * create Apr 27, 2018
 */
(function () {
	projectAllocation = {
			crNewId: 0,//id of new record
			listDataUpdated: [],
			MAXIMUM_ALLOWANCE_MONTHS_SHOWING: 12,
			defaultFromMonth: 0,
			defaultToMonth: 0,
			mapMonths: {
				0: "Jan",
				1: "Feb",
				2: "Mar",
				3: "Apr",
				4: "May",
				5: "Jun",
				6: "Jul",
				7: "Aug",
				8: "Sep",
				9: "Oct",
				10: "Nov",
				11: "Dec",
			},
			init: function(){
				projectAllocation.defaultFromMonth = "01/" + (new Date()).getFullYear();
				projectAllocation.defaultToMonth = "12/" + (new Date()).getFullYear();
				$('.ap-date-picker-month-format').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				projectAllocation.renderMonthCol();
				projectAllocation.loadData();
			},
			eventListener: function(){
				$(document).on('click','#btn-search',function(){
					var months = projectAllocation.renderMonthCol();
					if(months <= 0){
						projectAllocation.showWarning(GLOBAL_MESSAGES['cashflow.projectAllocation.mess1']);
					}
					else if(months > projectAllocation.MAXIMUM_ALLOWANCE_MONTHS_SHOWING){
						projectAllocation.showWarning(GLOBAL_MESSAGES['cashflow.projectAllocation.mess2']);
					}
					else {
						projectAllocation.loadData();
						projectAllocation.listDataUpdated = [];
						projectAllocation.hideWarning();
						projectAllocation.renderSaveButton();
					}
				});
			
				$(document).on('click','.btn-add-inline',function(){
					var index = $(this).closest('tr')[0].rowIndex;
					var item = {
						projectCode: "",
						employeeId: $(this).closest('tr').attr('empid'),
						mapMonthPercent: {},
					}
					projectAllocation.createProjectAllocationRow(item, index);
				});
				
				$('#export').click(function(){
					//$('#reset').click();
					var href = $(this).attr("href");
					var param = "";
					
					var employeeCode = $('#select-employee-code').val();
					var fromMonth = $('#txt-fromMonth').val();
					var toMonth = $('#txt-toMonth').val();
					
					//
					if (employeeCode.length) {
						param += "&employeeCode=" + employeeCode;
					}
					if (fromMonth.length > 0) {
						param += "&fromMonth=" + fromMonth;
					}
					if (toMonth.length > 0) {
						param += "&toMonth=" + toMonth;
					}
					
					if (param.length > 0) {
						href = href + "?" + param.slice(1);
					}
					
					$("#export").attr("href", href);
				});
				
				$(document).on('click','.btn-edit-inline',function(){
					//$('#project-allocation-data-table input').attr('readonly', true);
					$(this).closest('tr').find('input').attr('readonly', false);
					var txtProject = $(this).closest('tr').find('.txt-project');
					if(txtProject.closest('td').find('input[autocomplete]').length == 0)
					{
						var projectCodeTautocomplete = txtProject.tautocomplete({
					        highlight: "",
					        autobottom: true,
					        width: "250px",
							columns: [label('cashflow.projectAllocation.projectCode'), label('cashflow.projectAllocation.projectName'), "", "", "", "", "", ""],
							hide: [true, true, false, false, false, false, false, false],
							dataIndex: 0,
							addNewDataLink: '/admin/projectMaster/list',
					        data: function(){
				        		var x = { keyword: projectCodeTautocomplete.searchdata()}; 
				        		return x;
				        	},
					        ajax: {
				                url: "/manage/projectMasters",
				                type: "GET",
				                data: {
				                	keyword: function(){
				                		var keyword = projectCodeTautocomplete.searchdata();
				                		return keyword;
				                	}
				                },
				                success: function (data) {
				                    return data;
				                }
				            },
					        delay: 1000,
					        onchange: function () {
					        	var selectedData = projectCodeTautocomplete.all();
					        	//handle case user clear autocomplete
					        	var inputedValue = "";
					        	if(selectedData[label('cashflow.projectAllocation.projectName')])
					        	{
					        		inputedValue = selectedData[label('cashflow.projectAllocation.projectName')]
					        	}
					        	var projectCode = "";
					        	if(selectedData[label('cashflow.projectAllocation.projectCode')])
					        	{
					        		projectCode = selectedData[label('cashflow.projectAllocation.projectCode')]
					        	}
					        	var crProjectCode = txtProject.closest('tr[projectcode]').attr('projectcode');
					        	//console.log("inputedValue:" + inputedValue);
					        	//console.log("projectCode:" + projectCode);
					        	//console.log("crProjectCode:" + txtProject.closest('tr[projectcode]').attr('projectcode'));
					        	//validate duplicate project
					        	if(inputedValue != '' && crProjectCode != projectCode && $('#project-allocation-data-table tr[empid="' + txtProject.closest('tr').attr('empid') +'"][projectcode="' + projectCode + '"]').length > 0){
					        		projectAllocation.showWarning(GLOBAL_MESSAGES['cashflow.projectAllocation.mess3']);
					        		projectCodeTautocomplete.settext("");	
					        	}
					        	else {
					        		txtProject.closest('tr').find('input[projectallocationid]').each(function(){
						        		$(this).attr('projectcode', projectCode);
						        		projectAllocation.updateData($(this));
						        	});
						        	txtProject.closest('tr').attr('projectcode', projectCode);
						        	txtProject.val(inputedValue).attr('projectcode', projectCode);
						        	projectAllocation.renderSaveButton();
						        	projectAllocation.hideWarning();
						        	projectCodeTautocomplete.settext(inputedValue);
					        	}
					        }
						});
						projectCodeTautocomplete.settext($(this).closest('tr').find('.txt-project').val());	
					}
				});
				
				$(document).on('change','.txt-percent',function(){
					//remove data when user clears input
					if($(this).val() == '' && $(this).attr('projectallocationid'))
					{
						projectAllocation.removeCellData($(this));
					}		
					else{
						if(!$(this).attr('projectallocationid')){
							var crRow = $(this).closest('tr');
							$(this).attr('projectcode', crRow.attr('projectcode'));
							$(this).attr('empid', crRow.attr('empid'));
							$(this).attr('projectallocationid', "new-" + (++projectAllocation.crNewId));
							var month = $('#project-allocation-data-table').find('th').eq($(this).closest('td').index()).attr('val');
							$(this).attr('month', month);
						}
						projectAllocation.updateData($(this));
					}
					projectAllocation.renderSaveButton();
				});
				
				$(document).on('click','.btn-remove-inline',function(){
					var self = $(this);
					$('#popConfirm').modal('toggle');
		        	$('#popConfirm button[name="btnConfirm"]').off("click");
		        	$('#popConfirm button[name="btnConfirm"]').click(function() {
		        		self.closest('tr').find('input[projectallocationid]').each(function(){
		        			projectAllocation.removeCellData($(this));
						});
		        		self.closest('tr').remove();
		        		projectAllocation.renderSaveButton();
			        });
				});
				
				$(document).on("click",".btn-save-all",function(){
					if(!$(this).hasClass('disabled'))
						projectAllocation.save();
				});
				
				$(document).on('click','#btn-reset',function(){
					$('#select-employee-code').val('');
					$('#txt-fromMonth').val(projectAllocation.defaultFromMonth);
					$('#txt-toMonth').val(projectAllocation.defaultToMonth);
				});
			},
			renderMonthCol: function(){
				if($("#txt-fromMonth").val().trim() == '' || $("#txt-toMonth").val().trim() == ''){
					projectAllocation.showWarning(validate.require(GLOBAL_MESSAGES['cashflow.projectAllocation.month']));
					return -1;
				}
				var fromMonth = getDateFromString("01/" + $("#txt-fromMonth").val());
				var toMonth = getDateFromString("01/" + $("#txt-toMonth").val());
				var months;
			    months = (toMonth.getFullYear() - fromMonth.getFullYear()) * 12;
			    months -= fromMonth.getMonth();
			    months += toMonth.getMonth()  + 1;
			    if(months > 0 && months <= projectAllocation.MAXIMUM_ALLOWANCE_MONTHS_SHOWING)
			    {
			    	var crMonth = new Date(fromMonth.getTime());
				    var colTitle = "";
				    $('#project-allocation-data-table thead tr .th-month, #project-allocation-data-table thead tr .th-action').remove();
				    for(i = 0; i < months; i++){
				    	crMonth.setMonth(crMonth.getMonth() + (i > 0 ? 1 : 0));
				    	colTitle =  projectAllocation.mapMonths[crMonth.getMonth()] + "-" + crMonth.getFullYear() + "<br> (%)";
				    	$('#project-allocation-data-table thead tr').append("<th val='" + formatDateFull(crMonth) + "' class='th-month'>" + colTitle + "</th>")
				    }
				    $('#project-allocation-data-table thead tr').append("<th class='th-action'>" + GLOBAL_MESSAGES['cashflow.projectAllocation.action'] + "</th>")
			    }
			    return months;
			},
			loadData: function(){
				$('#project-allocation-data-table tbody').html('');
				$.ajax({
		            type : "GET",
		            url : CONTEXT_PATH + '/projectAllocation/api/findProjectAllocation',
		            data: "employeeCode=" + $("#select-employee-code").val() + "&fromMonth=" + $("#txt-fromMonth").val() + "&toMonth=" + $("#txt-toMonth").val(),
		            success : function(result) {
		            	var dataList = result.RESULT;
		            	var arrEmployeeIdAdded = [];
		            	for(i = 0; i < dataList.length; i++){
		            		var item = dataList[i];
		            		//console.log(item);
		            		var tableRe = $('#project-allocation-data-table');
		    				var tableRef = document.getElementById(tableRe.attr('id')).getElementsByTagName('tbody')[0];
		    				//when append header info
		    				if(arrEmployeeIdAdded.indexOf(item.employeeId) < 0)
		    				{
		    					var newRow = tableRef.insertRow($('#project-allocation-data-table tbody tr').length);
		    					newRow.className  = "tr-employee-header";
		    					newRow.id =  "tr-employee-header-" + item.employeeId;
		    					newRow.setAttribute("empId",item.employeeId);
		    					newRow.insertCell(0).appendChild(document.createTextNode(item.employeeName));
			    				newRow.insertCell(1).appendChild(document.createTextNode(item.employeeCode));
			    				newRow.insertCell(2).appendChild(document.createTextNode(item.team));
			    				newRow.insertCell(3).appendChild(document.createTextNode(""));
			    				for(j = 4; j < $('.th-month').length + 4; j++){
			    					newRow.insertCell(j).appendChild(document.createTextNode(""));
			    				}
			    				//append action cell
			    				var newCell  = newRow.insertCell($('.th-month').length + 4);
			    				var newText  = document.createElement('span');
			    				var htmlString = '<div class="row">';
			    				if(GLOBAL_PERMISSION["PMS-002"])
			    				{
			    					htmlString += '<div class="col-md-12 text-center btn-add-inline"><i style="cursor:pointer;" class="fa fa-plus-square-o "></i></div>';
			    				}
			    				htmlString += '</div>';
			    				newText.innerHTML = htmlString;
			    				newCell.appendChild(newText);
			    				//append project row
			    				if(item.projectCode)
			    				{
			    					projectAllocation.createProjectAllocationRow(item);
			    				}
			    				arrEmployeeIdAdded.push(item.employeeId);
		    				}
		    				//when append details info
		    				else {
		    					projectAllocation.createProjectAllocationRow(item);
		    				}
		            	}
		            	//render payment detail button
		            	if($('#project-allocation-data-table tr[projectcode]').length == 0)
		            	{
							$('#btn-go-to-project-payment').addClass('disabled');
						}
						else {
							$('#btn-go-to-project-payment').removeClass('disabled');
							$('#btn-go-to-project-payment').attr('href','/projectPayment/detail?monthFrom=' + $('#txt-fromMonth').val() + "&monthTo=" + $('#txt-toMonth').val());
						}
		            }
		         });
			},
			createInputCell: function(className, readonly, value){
				var newInput  = document.createElement("input");
				newInput.setAttribute('type', 'text');
				if(className){
					newInput.setAttribute('class', className);
				}
				if(readonly){
					newInput.setAttribute('readonly', readonly);
				}
				if(value){
					newInput.setAttribute('value', value);
				}			
				return newInput;
			},
			createInputCellPercent: function(className, readonly, value, item, thMonth){
				var newInput  = document.createElement("input");
				newInput.setAttribute('type', 'text');
				if(className){
					newInput.setAttribute('class', className);
				}
				if(readonly){
					newInput.setAttribute('readonly', readonly);
				}
				if(value){
					newInput.setAttribute('value', value);
				}
				if(item)
				{
					newInput.setAttribute("projectCode",item.projectCode);
					newInput.setAttribute("empId",item.employeeId);
					newInput.setAttribute("projectAllocationId", item.mapMonthPercent[thMonth].id);
					newInput.setAttribute("month", thMonth);
				}
				
				return newInput;
			},
			createProjectAllocationRow: function(item, positionToInsert){
				var tableRe = $('#project-allocation-data-table');
				var tableRef = document.getElementById(tableRe.attr('id')).getElementsByTagName('tbody')[0];
				var insertAt = $('#project-allocation-data-table tbody tr').length;
				if(positionToInsert)
				{
					insertAt = positionToInsert;
				}
				var newRow = tableRef.insertRow(insertAt);
				newRow.setAttribute("projectCode",item.projectCode);
				newRow.setAttribute("empId",item.employeeId);
				newRow.insertCell(0).appendChild(document.createTextNode(""));
				newRow.insertCell(1).appendChild(document.createTextNode(""));
				newRow.insertCell(2).appendChild(document.createTextNode(""));
				var txtPercent = projectAllocation.createInputCell('form-control txt-project', 'readonly', item.projectName);
				txtPercent.setAttribute('projectcode', item.projectCode);
				newRow.insertCell(3).appendChild(txtPercent);
				for(j = 4; j < $('.th-month').length + 4; j++){
					var thMonth = $('.th-month')[j - 4].getAttribute('val');
					if(item.mapMonthPercent[thMonth]){
						newRow.insertCell(j).appendChild(projectAllocation.createInputCellPercent('form-control txt-percent', 'readonly', item.mapMonthPercent[thMonth].percentAllocation, item, thMonth));
					}
					else
					{
						newRow.insertCell(j).appendChild(projectAllocation.createInputCellPercent('form-control txt-percent', 'readonly', ""));
					}
				}
				//append action cell
				var newCell  = newRow.insertCell($('.th-month').length + 4);
				var newText  = document.createElement('span');
				var htmlString = '<div class="row">';
				if(GLOBAL_PERMISSION["PMS-003"]){
					htmlString += '<div class="col-md-6 col-xs-6 text-center btn-edit-inline"><i style="cursor:pointer;" class="fa fa-pencil-square-o "></i></div>';
				}
				
				if(GLOBAL_PERMISSION["PMS-004"]){
					htmlString += '<div class="col-md-6 col-xs-6 text-center btn-remove-inline"><i style="cursor:pointer;" class="fa fa-trash-o "></i></div>';
				}
				htmlString += '</div>';
				newText.innerHTML = htmlString;
				//get input element inserted
				var insertdInput  = $('#project-allocation-data-table tr:last-child .txt-percent');
				//when add new row
				if(positionToInsert)
				{
					insertdInput = $('#project-allocation-data-table tr:nth-child(' + (insertAt + 1) +') .txt-percent');
				}
				//set up number input mask
				setTimeout(function(){
					insertdInput.inputmask("decimal", {
						alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false, allowMinus: false, min: 0, max: 100
					});
				}, 50);
				//trigger edit when add new
				if(positionToInsert)
				{
					setTimeout(function(){
						$('#project-allocation-data-table tr:nth-child(' + (insertAt + 1) +') .btn-edit-inline').trigger('click');
					}, 50);
				}
				newCell.appendChild(newText);
			},
			updateData: function(dataCell){
				var item = {};
				var exists = false;
				for(i = 0; i < projectAllocation.listDataUpdated.length; i++){
					if(dataCell.attr('projectallocationid') == projectAllocation.listDataUpdated[i].id)
					{
						item = projectAllocation.listDataUpdated[i];
						exists = true;
						break;
					}
				}
				item.id = dataCell.attr('projectallocationid');
				item.empId = dataCell.attr('empid');
				item.projectCode = dataCell.attr('projectcode');
				item.month = dataCell.attr('month');
				item.percent = dataCell.val();
				if(exists == false){
					projectAllocation.listDataUpdated.push(item);
				}
			},
			removeCellData: function(selft){
				var id = selft.attr('projectallocationid');
				var addedBefore = false;
				for(i = 0; i < projectAllocation.listDataUpdated.length; i++)
				{
					var item = projectAllocation.listDataUpdated[i];
					if(item.id == id)
					{
						addedBefore = true;
						item['removed'] = true;
						if(id.indexOf('new-') == 0)
						{
							projectAllocation.listDataUpdated.splice(projectAllocation.listDataUpdated.indexOf(item), 1);
						}
						break;
					}
				}
				if(addedBefore == false){
					var item = {
						id: id,
						removed: true
					}
					projectAllocation.listDataUpdated.push(item);
				}
			},
			renderSaveButton: function(){
				if(projectAllocation.listDataUpdated.length == 0)
				{
					$('.btn-save-all').addClass('disabled');
					$('#isChangedContent').val('false');
				}
				else {
					$('.btn-save-all').removeClass('disabled');
					$('#isChangedContent').val('true');
				}
			},
			save: function(){
				if(projectAllocation.validateBeforeSave() == true){
					var formData = new FormData();
					for(j = 0; j < projectAllocation.listDataUpdated.length; j++)
					{
						if(projectAllocation.listDataUpdated[j].id.indexOf('new-') == 0){
							projectAllocation.listDataUpdated[j].id = 0;
						}
					}
					formData.append('dataList', JSON.stringify(projectAllocation.listDataUpdated));
					$.ajax({
			            type : "POST",
			            url : CONTEXT_PATH + '/projectAllocation/save',
			            cache : false,
						contentType : false,
						processData : false,
						data : formData,
			            success : function(ajaxResult) {
			            	 var resultData = JSON.parse(ajaxResult);
			            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
			            	 {
			            		 projectAllocation.hideWarning();
			            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
			            			 $('#isChangedContent').val('false');
		            				 window.location.reload();
			            		 });
			            	 }
			            	 else
			            	 {
			            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
			            	 }
			            }
			        });	
				}
			},
			showWarning:function(message){
				$('.alert-warning').showAlertMessage([message]);
				$('#alert-warning').goTo();
			},
			hideWarning: function(){
				$('.alert-warning').hide();
			},
			validateBeforeSave: function(){
				//validate project field must not be empty
				if($('#project-allocation-data-table tr[projectcode=""]').length > 0){
					projectAllocation.showWarning(GLOBAL_MESSAGES['cashflow.projectAllocation.mess6']);
					return false;
				}
				//validate total percent of a month must be 100%
				else {
					var arrInvaildMess = [];
					for(i = 0; i < $('#project-allocation-data-table .tr-employee-header').length; i++){
						var empId = $('#project-allocation-data-table .tr-employee-header')[i].getAttribute('empid');
						var empName = $($('#project-allocation-data-table .tr-employee-header')[i]).find('td:nth-child(' + 1 + ')').html()
						for(j = 0; j < $('.th-month').length; j++){
							var month = $('.th-month')[j].getAttribute('val');
							var monthName = $($('.th-month')[j]).html().split('<br>')[0];
							//console.log(month + "," + empId);
							var percentInput = $("#project-allocation-data-table .txt-percent[month='" + month + "'][empid='" + empId + "']");
							var totalPercent = getNewDecimal(0);
							for(k = 0; k < percentInput.length; k++){
								var percent = percentInput[k].value;
								//console.log(percent);
								totalPercent = totalPercent.plus(getNewDecimal(percent));
							}
							//total percent of project must be 100
							if(totalPercent.comparedTo(getNewDecimal('0')) == 1)
							{
								if(totalPercent.comparedTo(getNewDecimal('100')) < 0)
								{
									var mess = GLOBAL_MESSAGES['cashflow.projectAllocation.mess5'].replace("%1", empName).replace("%2", monthName);
									arrInvaildMess.push(mess);
									percentInput.addClass('invalid');
								}
								else if(totalPercent.comparedTo(getNewDecimal('100')) > 0)
								{
									var mess = GLOBAL_MESSAGES['cashflow.projectAllocation.mess7'].replace("%1", empName).replace("%2", monthName);
									arrInvaildMess.push(mess);
									percentInput.addClass('invalid');
								}
								else {
									percentInput.removeClass('invalid');
								}
							}
						}
					}
					if(arrInvaildMess.length > 0){
	            		$('.alert-warning').showAlertMessage(arrInvaildMess);
		    			$('#alert-warning').goTo();
		    			return false;
	            	}
					else
					{
						return true;
					}
				}
			}
	};
}());