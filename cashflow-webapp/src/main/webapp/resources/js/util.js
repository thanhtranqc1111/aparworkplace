function isEmpty(val){
	return (val == null || val.trim().length == 0);
}

(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
        return this; // for chaining...
    }
})(jQuery);

function getDateFromString(strDate){
	var dateParts = strDate.split("/");
	var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
	return date;
}

//format string number
function formatStringAsNumber(numberString){
	return accounting.formatMoney(numberString, '', 2);
}

function setupInputNumberMask(){
	$('input.ap-big-decimal').inputmask("decimal", {
	    placeholder: "0",
	    digits: 2,
	    digitsOptional: false,
	    radixPoint: ".",
	    groupSeparator: ".",
	    autoGroup: true,
	    allowPlus: false,
	    allowMinus: true,
	    clearMaskOnLostFocus: false,
	    removeMaskOnSubmit: true,
	    autoUnmask: true,
	    rightAlign: false,
	    /*onUnMask: function(maskedValue, unmaskedValue) {
	        var x = maskedValue.split(',');
	        return x[0].replace(/\./g, '') + '.' + x[1];
	    }*/
	});
}

$.fn.getNumberValue = function(){
	return $(this).val().replace(/\,/g,'');
};

function showPopupMessage(message, callBack){
	$('#ap-message #ap-message-content').html(message);
	$('#ap-message').modal('toggle');
	$('#ap-message').on('hidden.bs.modal', callBack)
}

function showPopupSave(message, callBack){
	$('#modal-save #message').html(message);
	$('#modal-save').modal('toggle');
	$('#modal-save #btnOK').click(callBack);
}

function showPopupMessage2(message, onSubmit, onCancel){
	$('#ap-message-2 #ap-message-content-2').html(message);
	$('#ap-message-2').modal({
	    backdrop: 'static',
	    keyboard: false
	})
	$('#ap-message-2 .btn-default, #ap-message-2 .close').unbind('click');
	$('#ap-message-2 .btn-default, #ap-message-2 .close').click(onCancel)
	$('#ap-message-2 .btn-primary').unbind('click');
	$("#ap-message-2 .btn-primary").click(onSubmit);   
}

function formatDate(date){
	return date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
}

function formatDateFull(date){
	var dd = date.getDate();
	var mm = date.getMonth() + 1;
	var yyyy = date.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	return dd + '/' + mm + '/' + yyyy;
}

function getParameter(paramName){
	var url_string = window.location.href
	var url = new URL(url_string);
	var c = url.searchParams.get(paramName);
	return c;
}

function formatCalendar(objCalendar){
	/*var d = objCalendar.dayOfMonth < 10 ? "0" + objCalendar.dayOfMonth : objCalendar.dayOfMonth;
	var m = (objCalendar.month + 1) < 10 ? "0" + (objCalendar.month + 1) : (objCalendar.month + 1);*/
	return objCalendar.dayOfMonth + "/" + (objCalendar.month + 1) + "/" + objCalendar.year;
}

$.fn.setupInputNumberMask = function(options){
	var allowMinus = true;
	if(options && typeof options.allowMinus != 'undefined')
	{
		$(this).inputmask("decimal", {
		    placeholder: "0",
		    digits: 2,
		    digitsOptional: false,
		    radixPoint: ".",
		    groupSeparator: ".",
		    autoGroup: true,
		    allowPlus: false,
		    allowMinus: true,
		    clearMaskOnLostFocus: false,
		    removeMaskOnSubmit: true,
		    autoUnmask: true,
		    rightAlign: false
		});
	}
	else 
	{
		$(this).attr("data-inputmask","'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'rightAlign': 'false', 'allowMinus' : 'false'").inputmask();
	}
};

//convert string to number
function formatNumber(str){
	return  Number((str + "").replace(/\,/g,''))
}

/*
 * setup first column checkable for data table
 * pram: 
 * - data table: data table object
 * - rows_selected: array id selected rows
 */
$.fn.tableCheckable = function(datatable,rows_selected,callBack){
	var self = $(this);
	// Handle click on checkbox
	function updateDataTableSelectAllCtrl(table){
	   var $table             = table.table().node();
	   var $chkbox_all        = $(self.find('tbody input[type="checkbox"]'), $table);
	   var $chkbox_checked    = $(self.find('tbody input[type="checkbox"]:checked'), $table);
	   var chkbox_select_all  = $(self.find('thead input[name="select_all"]'), $table).get(0);

	   // If none of the checkboxes are checked
	   if($chkbox_checked.length === 0){
	      chkbox_select_all.checked = false;
	      if('indeterminate' in chkbox_select_all){
	      }

	   // If all of the checkboxes are checked
	   } else if ($chkbox_checked.length === $chkbox_all.length){
	      chkbox_select_all.checked = true;
	      if('indeterminate' in chkbox_select_all){
	         chkbox_select_all.indeterminate = false;
	      }

	   // If some of the checkboxes are checked
	   } else {
	      chkbox_select_all.checked = true;
	      if('indeterminate' in chkbox_select_all){
	         chkbox_select_all.indeterminate = true;
	      }
	}};
    $(this).find('tbody').on('click', 'input[type="checkbox"]', function(e){
       var $row = $(this).closest('tr');

       // Get row data
       var data = datatable.row($row).data();

       // Get row ID
       var rowId = $(this).val();

       // Determine whether row ID is in the list of selected row IDs
       var index = $.inArray(rowId, rows_selected);

       // If checkbox is checked and row ID is not in list of selected row IDs
       if(this.checked && index === -1){
    	   rows_selected.push(rowId);

       // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
       } else if (!this.checked && index !== -1){
    	   rows_selected.splice(index, 1);
       }

       if(this.checked){
          $row.addClass('selected');
       } else {
          $row.removeClass('selected');
       }

       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(datatable);

       // Prevent click event from propagating to parent
       e.stopPropagation();
       
       callBack($(this));
    });
   
    // Handle click on "Select all" control
    $($(this).find('thead input[name="select_all"]'), datatable.table().container()).on('click', function(e){
       if(this.checked){
    	   self.find('tbody input[type="checkbox"]:not(:checked)').trigger('click');
       } else {
    	   self.find('tbody input[type="checkbox"]:checked').trigger('click');
       }
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
};

//shake effect for element
jQuery.fn.shake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css("position","relative"); 
        for (var x=1; x<=intShakes; x++) {
        $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
    .animate({left:intDistance}, ((intDuration/intShakes)/2))
    .animate({left:0}, (((intDuration/intShakes)/4)));
    }
  });
return this;
};

$.fn.getNumberValue = function(){
	return $(this).val().replace(/\,/g,'');
};

//show error message
$.fn.showAlertMessage = function(messages){
	if($(this).find('a.close').length == 0){
		var htmlCode = '<a class="close"><i class="fa fa-close button-margin"></i></a><div class="ap-message-panel"></div>';
		$(this).html(htmlCode);
		$(this).addClass('alert alert-warning');
		var self = $(this);
		$(this).on('click','.close', function(){
			self.hide();
		});
	}
	var messageHtml = "<ul>";
	for( i = 0;i < messages.length; i++){
		messageHtml += ('<li>' + messages[i] + '</li>');
	}
	messageHtml += "</ul>";
	$(this).find('.ap-message-panel').html(messageHtml);
	$(this).show();
};

(function () {
	validate = {
		require : function (field) {
			return  field + " " + GLOBAL_MESSAGES['cashflow.common.isRequired'];
		}
	};
}());

function getNewDecimal(value){
	try {
		if(typeof value == 'string')
		{
			value = (value).replace(/\,/g,'');
		}
		return new Decimal(value);
	} catch (e) {
		return new Decimal(0);
	}
}

function toDecimal(value){
	
	if(value == null || value == undefined) return null;
	
	try {
		if(typeof value == 'string') {
			value = (value).replace(/\,/g,'');
		}
		return new Decimal(value);
	} catch (e) {
		return null;
	}
}

function detectIEBrowser(){
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    // If Internet Explorer, return version number
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
    	return true;
    return fals;
}

// function for formatting a String with arguments
// reference from https://coderwall.com/p/flonoa/simple-string-format-in-javascript
String.prototype.format = function() {
    a = this;
    for (k in arguments) {
      a = a.replace("{" + k + "}", arguments[k]);
    }
    return a;
};