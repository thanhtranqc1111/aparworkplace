/**
 * @author ThoaiNH 
 * create Oct 05, 2017
 */
(function () {
    projectMaster = {
        dataTable: null,
        approvalCode: null,
        init: function () {
            projectMaster.dataTable = $('#table_data').removeAttr('width').DataTable({
                "ajax": {
                    url: "/admin/projectMaster/api/getAllPagingProject",
                    type: "post",
                    error: function() { // error handling
                        $(".table_data-error").html("");
                        $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
                        $("#table_data_processing").css("display", "none");
                    },
                    complete: function(data){
                        hideAjaxStatus();
                        $("#table_data tbody td:not(:last-child)").attr('title',GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
                        $("#table_data tbody td:not(:last-child)").css('cursor','pointer');
                    }    
                },
                columns: [
                    {
                    "data" : null,
                    "visible": false,
                    },
                    {
                        "data" : "code",
                        "orderable": true
                    },
                    {
                        "data" : "name",
                        "orderable": true
                    },
                    {
                        "data" : "businessTypeName",
                        "orderable": true
                    },
                    {
                        "data" : "approvalCode",
                        "orderable": true
                    },
                    {
                        "data" : "customerName",
                        "orderable": true
                    },
                    {
                        "data": "code",
                        "orderable": false,
                        "render": function(data, type,full, meta) {
                            if(GLOBAL_PERMISSION["PMS-004"])
                                return iconAction(data, full);
                            else
                                return "";
                     }
                }
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
                "lengthChange" : true
            });

            var col1 = title('cashflow.projectMaster.approvalNo');
            var col2 = title('cashflow.projectMaster.approvalType');
            var col3 = title('cashflow.projectMaster.approvedDate');
            var col4 = title('cashflow.projectMaster.applicant');
            var col5 = title('cashflow.projectMaster.purpose');

            projectMaster.approvalCode = $('#approvalCode').tautocomplete({
                highlight: "",
                autobottom: true,
                columns: [col1, col2, col3, col4, col5],
                hide: [true, true, true, true, true],
                dataIndex: 0,
                data: function(){
                    var x = { keyword: approvalVal.searchdata()}; 
                    return x;
                },
                ajax: {
                    url: "/approvalSalesOrderRequest/api/getApprovalSalesOrderRequestMaps",
                    type: "GET",
                    data: {
                        keyword: function(){
                            var keyword = projectMaster.approvalCode.searchdata();
                            return keyword;
                        }
                    },
                    success: function (ret) {
                        return ret;
                    }
                },
                delay: 1000,
                onchange: function () {
                    var selectedData = projectMaster.approvalCode.all();
                    projectMaster.approvalCode.settext(selectedData[col1]);

                    $('#approvalCode').val($('#approvalCode').next().next().val());
                    $('#approvalCode-error').text('');
                }
            });

            $('#approvalCode').next().next().attr("readonly", true);
        },
        eventListener: function () {
            $('#businessTypeName').attr('disabled','disabled');
            $("#businessTypeName option:first").attr('disabled','disabled');
            $('#customerName').attr('disabled','disabled');
            $("#customerName option:first").attr('disabled','disabled');
            $('#approvalCode').attr("readonly", true);

            if(GLOBAL_PERMISSION["PMS-003"])
            {
                $( document ).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function(){
                    $('#mode').val("Edit");
                    $('button[name="saveButton"]').removeClass('disabled');
                    $('#code').val($(this).closest('tr').children('td:nth-child(1)').text()).attr("readonly", true);
                    $('#name').val($(this).closest('tr').children('td:nth-child(2)').text()).attr("readonly", false);
                    $('#businessTypeName').removeAttr('disabled');
                    $('#businessTypeName').val($(this).closest('tr').children('td:nth-child(3)').text()).attr("readonly", false);
                    $('#approvalCode').next().next().attr("readonly", false);
                    projectMaster.approvalCode.settext($(this).closest('tr').children('td:nth-child(4)').text());
                    $('#approvalCode').val($('#approvalCode').next().next().val());
                    $('#customerName').removeAttr('disabled');
                    $('#customerName').val($(this).closest('tr').children('td:nth-child(5)').text()).attr("readonly", false);
                });
            }
            $('body').on('click', '.edit-ajax', function() {
                var id = $(this).attr('id');
            });
            $('body').on('click', '.delete-ajax', function() {
                var code = $(this).attr('id');
                data = {
                    id: code
                };
                url = CONTEXT_PATH + "/admin/projectMaster/delete";
                $('#popConfirm').modal('toggle');
                $('button[name="btnConfirm"]').on('click', function() {
                    actionAjax(data, url, projectMaster.dataTable);
                });
                
            });
        },
        save: function(){
            var isValid = true;

            // === validate input fields ===
            if(/^(.{1,20})$/.test($('#code').val())) {
                $('#code-error').text('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.projectMaster.code'], '1', '20'));
                isValid = false;
            }

            if(/^(.{1,100})$/.test($('#name').val())) {
                $('#name-error').text('');
            } else {
                $('#name-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.projectMaster.projectName'], '1', '100'));
                isValid = false;
            }

            /*if(/.+/.test($('#approvalCode').next().next().val())) {
                $('#approvalCode-error').text('');
            } else {
                $('#approvalCode-error').text(GLOBAL_MESSAGES['cashflow.common.notEmpty'].format(GLOBAL_MESSAGES['cashflow.projectMaster.approvalCode']));
                isValid = false;
            }*/
            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled'))
            {
                var form = document.getElementById('projectMasterForm');
                form.action = CONTEXT_PATH + "/admin/projectMaster/save";
                form.submit();
                $('button[name="saveButton"]').addClass('disabled');
            }
        },
        newProjectMaster: function(){
            $('#code').val('').attr("readonly", false);
            $('#name').val('').attr("readonly", false);
            $('#businessTypeName').removeAttr('disabled');
            $('#businessTypeName').val($("#businessTypeName option:eq(1)").val()).attr("readonly", false);
            $('#approvalCode').next().next().attr("readonly", false);
            $('#customerName').removeAttr('disabled');
            $('#customerName').val($("#customerName option:eq(1)").val()).attr("readonly", false);
            $('#mode').val("New");
            $('button[name="saveButton"]').removeClass('disabled');
            
            // focus form
            $('#projectMasterForm').goTo();
            $('#code').focus();
        },
    };
}());