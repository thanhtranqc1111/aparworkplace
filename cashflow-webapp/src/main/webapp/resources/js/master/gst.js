/**
 * @author ThoaiNH 
 * create Oct 06, 2017
 */
(function () {
	gstMaster = {
		dataTable: null,
		init: function () {
			gstMaster.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/admin/gstMaster/api/getAllPagingGst",
		            type: "post",
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	hideAjaxStatus();
		            	$("#table_data tbody td:not(:last-child)").attr('title',GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
		            	$("#table_data tbody td:not(:last-child)").css('cursor','pointer');
		            }	
		        },
		        columns: [
		        	{
		            "data" : null,
		            "visible": false,
			        }, 
			        {
			            "data" : "code",
			            "orderable": true
			        }, 
			        {
			            "data" : "type",
			            "orderable": true
			        },
			        {
			            "data" : "rate",
			            "orderable": true
			        },
			        {
			            "data": "code",
			            "orderable": false,
			            "render": function(data, type, full, meta) {
			            	if(GLOBAL_PERMISSION["PMS-004"])
			            		return iconAction(data, full);
			            	else
			            		return "";
		                 }
			                        
			        }
		        ],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true
			});
			
			$('#rate').inputmask("decimal", {
				alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false, allowMinus: false
			});
		},
		eventListener: function () {
			if(GLOBAL_PERMISSION["PMS-003"])
			{
				$( document ).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function(){
			        $('#code').val($(this).closest('tr').children('td:nth-child(1)').text()).attr("readonly", true);
			        $('#type').val($(this).closest('tr').children('td:nth-child(2)').text()).attr("readonly", false);
			        $('#rate').val($(this).closest('tr').children('td:nth-child(3)').text()).attr("readonly", false);
			        $('#mode').val("Edit");
			        $('button[name="saveButton"]').removeClass('disabled');
			    });
			}
		    $('body').on('click', '.edit-ajax', function() {
		        var id = $(this).attr('id');
		    });
			 $('body').on('click', '.delete-ajax', function() {
		        var code = $(this).attr('id');
		        data = {
		            id: code
		        };
		        url = CONTEXT_PATH + "/admin/gstMaster/delete";
		        $('#popConfirm').modal('toggle');
		        $('button[name="btnConfirm"]').on('click', function() {
		            actionAjax(data, url, gstMaster.dataTable);
		        });
		        
		    });
		},
		save: function(){
		    var isValid = true;

            // === validate input fields ===
            if(/^(.{1,20})$/.test($('#code').val())) {
                $('#code-error').text('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.gstMaster.code'], '1', '20'));
                isValid = false;
            }

            if(/^(.{1,100})$/.test($('#type').val())) {
                $('#type-error').text('');
            } else {
                $('#type-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.gstMaster.type'], '1', '100'));
                isValid = false;
            }

            /*if(/^(.{0,10})$/.test($('#rate').val())) {
                $('#rate-error').text('');
            } else {
                $('#rate-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.gstMaster.rate'], '10'));
                isValid = false;
            }*/
            if(/.+/.test($('#rate').val())) {
                $('#rate-error').text('');
            } else {
                $('#rate-error').text(GLOBAL_MESSAGES['cashflow.common.notEmpty'].format(GLOBAL_MESSAGES['cashflow.gstMaster.rate']));
                isValid = false;
            }
            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled'))
			{
				var form = document.getElementById('gstMasterForm');
			    form.action = CONTEXT_PATH + "/admin/gstMaster/save";
			    form.submit();
			}
		},
		newGstMaster: function(){
			$('#code').val('').attr("readonly", false);
		    $('#type').val('').attr("readonly", false);
		    $('#rate').val('').attr("readonly", false);
		    $('#mode').val("New");
		    $('button[name="saveButton"]').removeClass('disabled');

            // focus form
            $('#gstMasterForm').goTo();
            $('#code').focus();
		}
	};
}());