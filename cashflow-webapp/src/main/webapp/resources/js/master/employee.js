(function() {
    employeeMaster = {
        dataTable : null,
        init : function() {
			$('.datepicker input').datepicker({
		    	format: 'dd/mm/yyyy',
		        todayHighlight: true
			});

            if($('.box-body').find('span').hasClass('error')) {
                $('#code').val($(this).children('td:nth-child(1)').children('span').text()).attr("readonly", false);
                $('#name').val($(this).children('td:nth-child(3)').text()).attr("readonly", false);
                $('#roleTitle').val($(this).children('td:nth-child(4)').text()).attr("readonly", false);
                $('#team').val($(this).children('td:nth-child(5)').text()).attr("readonly", false);
                $('#joinDate').val($(this).children('td:nth-child(6)').text()).attr("readonly", false);
                $('#resignDate').val($(this).children('td:nth-child(7)').text()).attr("readonly", false);
                $('button[name="saveButton"]').removeClass('disabled');
            }

            employeeMaster.dataTable = $('#table_data').removeAttr('width').DataTable(
                    {
                        "ajax" : {
                            url : "/admin/employeeMaster/api/getAllPagingEmployee",
                            type : "get",
                            error : function() { // error handling
                                $(".table_data-error").html("");
                                $("#table_data").append(
                                        '<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData']
                                                + '</td></tr></tbody>');
                                $("#table_data_processing").css("display", "none");
                            },
                            complete : function(data) {
                                hideAjaxStatus();
                                $("#table_data tbody td:not(:last-child)").attr('title', GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
                                $("#table_data tbody td:not(:last-child)").css('cursor', 'pointer');
                            }
                        },
                        columns : [ {
                            "data" : "code",
                            "orderable" : true,
                            "render" : function(data, type, full, meta) {
                                if (data != null && data != undefined) {
                                    return "<span employeCode='"+ data +"'>" + data + "</span>";
                                }
                            }
                        }, {
                            "data" : "divisionName",
                            "orderable" : true,
                            "render" : function(data, type, full, meta) {
                                return "<span divisionCode='"+ full.divisionCode +"'>" + data + "</span>";
                            }
                        }, {
                            "data" : "name",
                            "orderable" : true
                        }, {
                            "data" : "roleTitle",
                            "orderable" : true
                        }, {
                            "data" : "team",
                            "orderable" : true
                        }, {
                            "data" : "joinDate",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	if(full.joinDate){
				            		var dayOfMonth = full.joinDate.dayOfMonth;
				            		var month = full.joinDate.month + 1;
				            		var year = full.joinDate.year;
				            		return moment(new Date(month + "/" + dayOfMonth + "/" + year)).format('DD/MM/YYYY');
				            	}
				            	else
				            		return "";
				             }	
                        }, {
                            "data" : "resignDate",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	if(full.resignDate) {
				            		var dayOfMonth = full.resignDate.dayOfMonth;
				            		var month = full.resignDate.month + 1;
				            		var year = full.resignDate.year;
				            		return moment(new Date(month + "/" + dayOfMonth + "/" + year)).format('DD/MM/YYYY');
			            	    }
			            	    else
			            		    return "";
				                }	
                        }, {
                            "data" : "code",
                            "orderable" : false,
                            "render" : function(data, type, full, meta) {
                                if (GLOBAL_PERMISSION["PMS-004"])
                                    return iconAction(data, full);
                                else
                                    return "";
                            }
                        } ],
                        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all'] ] ],
                        "lengthChange" : true,
                        'order' : [ [ 1, 'asc' ] ]
                    });
        },
        eventListener : function() {
            $('#divisionMaster').attr('disabled','disabled');
            $("#divisionMaster option:first").attr('disabled','disabled');

            if (GLOBAL_PERMISSION["PMS-003"]) {
                $(document).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function() {
                    $('#code').val($(this).closest('tr').children('td:nth-child(1)').children('span').text()).attr("readonly", true);
                    $('#divisionMaster').removeAttr('disabled');
                    $('#divisionMaster').val($(this).closest('tr').children('td:nth-child(2)').children('span').attr('divisioncode')).attr("readonly", false);
                    $('#name').val($(this).closest('tr').children('td:nth-child(3)').text()).attr("readonly", false);
                    $('#roleTitle').val($(this).closest('tr').children('td:nth-child(4)').text()).attr("readonly", false);
                    $('#team').val($(this).closest('tr').children('td:nth-child(5)').text()).attr("readonly", false);
                    var joinDate = $(this).closest('tr').children('td:nth-child(6)').text();
					if(joinDate) {
                        joinDate = joinDate.split("/");
                        $('#joinDate').datepicker("setDate", new Date(joinDate[2], joinDate[1] - 1, joinDate[0]));
                    } else {
                     $('#joinDate').val(joinDate);
                    }
                    $('#joinDate').attr("readonly", false);
                    var resignDate = $(this).closest('tr').children('td:nth-child(7)').text();
                    if(resignDate) {
                     resignDate = resignDate.split("/");
                        $('#resignDate').datepicker("setDate", new Date(resignDate[2], resignDate[1] - 1, resignDate[0]));
                    } else {
                     $('#resignDate').val(resignDate);
                    }
                    $('#resignDate').attr("readonly", false);
                    $('#mode').val("Edit");
                    $('button[name="saveButton"]').removeClass('disabled');
                });
            }
            $('body').on('click', '.edit-ajax', function() {
                var id = $(this).attr('id');
            });
            $('body').on('click', '.delete-ajax', function() {
                var code = $(this).attr('id');
                data = {
                    id : code
                };
                url = CONTEXT_PATH + "/admin/employeeMaster/delete";
                $('#popConfirm').modal('toggle');
                $('button[name="btnConfirm"]').on('click', function() {
                    actionAjax(data, url, employeeMaster.dataTable);
                });

            });
        },
        save : function() {
            var isValid = true;

            // === validate input fields ===
            if(/^(.{1,40})$/.test($('#code').val())) {
                $('#code-error').text('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.code'], '1', '40'));
                isValid = false;
            }

            if(/^(.{1,100})$/.test($('#name').val())) {
                $('#name-error').text('');
            } else {
                $('#name-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.name'], '1', '100'));
                isValid = false;
            }

            if(/^(.{0,100})$/.test($('#roleTitle').val())) {
                $('#roleTitle-error').text('');
            } else {
                $('#roleTitle-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.roleTitle'], '100'));
                isValid = false;
            }

            if(/^(.{0,100})$/.test($('#team').val())) {
                $('#team-error').text('');
            } else {
                $('#team-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.team'], '100'));
                isValid = false;
            }
            
            // validate joinDate - resignDate
            var joinDate = $("#joinDate").datepicker('getDate');
            var resignDate = $("#resignDate").datepicker('getDate');
            if($("#joinDate").val() == '') {
           	 	$('#joinDate-error').text(GLOBAL_MESSAGES['cashflow.common.requireInput'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.joinDate']));
           	 	isValid = false;
            } else if ( $("#resignDate").val() == '' || resignDate>joinDate) { 
                $('#joinDate-error').text('');
            } else {
            	 $('#joinDate-error').text(GLOBAL_MESSAGES['cashflow.common.laterThan'].format(GLOBAL_MESSAGES['cashflow.employeeMaster.resignDate'], GLOBAL_MESSAGES['cashflow.employeeMaster.joinDate']));
                isValid = false;
            }

            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled')) {
                var form = document.getElementById('employeeMasterForm');
                form.action = CONTEXT_PATH + "/admin/employeeMaster/save";
                form.submit();
            }
        },
        newEmployeeMaster : function() {
            $('#code').val('').attr("readonly", false);
            $('#divisionMaster').removeAttr('disabled');
            $('#divisionMaster').val($("#divisionMaster option:eq(1)").val()).attr("readonly", false);
            $('#name').val('').attr("readonly", false);
            $('#roleTitle').val('').attr("readonly", false);
            $('#team').val('').attr("readonly", false);
            $('#joinDate').val('').attr("readonly", false);
            $('#resignDate').val('').attr("readonly", false);
            $('#mode').val("New");
            $('button[name="saveButton"]').removeClass('disabled');

            // focus form
            $('#employeeMasterForm').goTo();
            $('#code').focus();
        }
    };
}());