/**
 * @author ThoaiNH 
 * create Oct 05, 2017
 */
(function () {
	payeeMaster = {
		dataTable: null,
		init: function () {
			payeeMaster.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/admin/payeeMaster/api/getAllPagingPayee",
		            type: "post",
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	hideAjaxStatus();
		            	$("#table_data tbody td:not(:last-child)").attr('title',GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
		            	$("#table_data tbody td:not(:last-child)").css('cursor','pointer');
		            }	
		        },
		        columns: [
		        	{
			            "data" : null,
			            "visible": false
		        	}, 
		        	{
		        		"data" : "code",
		        		"orderable": true
		        	}, 
		        	{
		        		"data" : "name",
		        		"orderable": true
		        	}, 
		        	{
			            "data" : "",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.payeePayerTypeMaster && full.payeePayerTypeMaster.name)
			            		return "<span class='type' code='" + full.payeePayerTypeMaster.code + "'>" + full.payeePayerTypeMaster.name + "</span>";
			            	else
			            		return "";
			             }
		        	}, 
		        	{
			            "data" : "",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.accountMaster && full.accountMaster.name)
			            		return full.accountMaster.name;
			            	else
			            		return "";
			             }
			        }, 
			        {
			            "data" : "",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.accountMaster && full.accountMaster.code)
			            		return full.accountMaster.code;
			            	else
			            		return "";
			             }
			        }, 
			        {
			            "data" : "bankAccountCode",
			            "defaultContent": "",
			            "orderable": false
			        }, 
			        {
			            "data" : "bankName",
			            "defaultContent": "",
			            "orderable": false
			        }, 
			        {
			            "data" : "paymentTerm"
			        }, 
			        {
			            "data" : "phone",
			            "defaultContent": "",
			            "orderable": false
			        }, 
			        {
			            "data" : "address",
			            "defaultContent": "",
			            "orderable": false
			        },
			        {
			            "data" : "defaultCurrency",
			            "defaultContent": "",
			            "orderable": false
			        }, 
			        {
			            "data": "code",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(GLOBAL_PERMISSION["PMS-004"])
			            		return iconAction(data, full);
			            	else
			            		return "";
			             }
			        }
			    ],
			    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true,
			    "initComplete": function(settings, json) {
			        $("#table_data").parent().css({
			        	"width": "99%",
			        	"margin-left": "5px",
			        	"overflow": "auto"
			        });
			     }
		    });
		},
		eventListener: function () {
			  if(GLOBAL_PERMISSION["PMS-003"])
			  {
				 $( document ).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function(){
				    	var code = $(this).closest('tr').children('td:nth-child(1)').text();
				    	var seft = $(this).closest('tr');
				    	//load detail data
				    	$('#code').val(code).attr("readonly", true);
			            $('#name').val(seft.children('td:nth-child(2)').text()).attr("readonly", false);
			            document.getElementById('select-type').value = seft.find('.type').attr('code');
			            document.getElementById('select-payee-account').value = seft.children('td:nth-child(5)').text();
			            $('#bankName').val(seft.children('td:nth-child(7)').text()).attr("readonly", false);
			            $('#bankAccountCode').val(seft.children('td:nth-child(6)').text()).attr("readonly", false);
			            $('#paymentTerm').val(seft.children('td:nth-child(8)').text()).attr("readonly", false);
			            $('#phone').val(seft.children('td:nth-child(9)').text()).attr("readonly", false);
			            $('#address').val(seft.children('td:nth-child(10)').text()).attr("readonly", false);
			            $('#select-payee-default-currency').val(seft.children('td:nth-child(11)').text()).attr("readonly", false);
			            $('button[name="saveButton"]').removeClass('disabled');
			            $('#mode').val("Edit");
			            $('#payeeMasterForm').goTo();
				    });
			    }
			    $('body').on('click', '.edit-ajax', function() {
			        var id = $(this).attr('id');
			    });
			    $('body').on('click', '.delete-ajax', function() {
			        var code = $(this).attr('id');
			        data = {
			            id: code
			        };
			        url = CONTEXT_PATH + "/admin/payeeMaster/delete";
			        $('#popConfirm').modal('toggle');
			        $('button[name="btnConfirm"]').on('click', function() {
			            actionAjax(data, url, payeeMaster.dataTable);
			        });
			        
			    });
		},
		save: function(){
		    var isValid = true;

            // === validate input fields ===
            if(/^(.{1,20})$/.test($('#code').val())) {
                $('#code-error').text('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.code'], '1', '20'));
                isValid = false;
            }

            if(/^(.{1,100})$/.test($('#name').val())) {
                $('#name-error').text('');
            } else {
                $('#name-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.name'], '1', '100'));
                isValid = false;
            }

            if(/^(.{0,20})$/.test($('#bankAccountNo').val())) {
                $('#bankAccountNo-error').text('');
            } else {
                $('#bankAccountNo-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.bankAccountNo'], '20'));
                isValid = false;
            }
            
            if(/^(.{0,20})$/.test($('#bankAccountCode').val())) {
                $('#bankAccountCode-error').text('');
            } else {
                $('#bankAccountCode-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.bankAccountNo'], '20'));
                isValid = false;
            }
            
            if(/^(.{0,20})$/.test($('#type').val())) {
                $('#type-error').text('');
            } else {
                $('#type-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.type'], '20'));
                isValid = false;
            }

            if(/^(.{0,100})$/.test($('#bankName').val())) {
                $('#bankName-error').text('');
            } else {
                $('#bankName-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.bankName'], '100'));
                isValid = false;
            }

            if(/^(.{0,40})$/.test($('#paymentTerm').val())) {
                $('#paymentTerm-error').text('');
            } else {
                $('#paymentTerm-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.term'], '40'));
                isValid = false;
            }

            if(/^(.{0,20})$/.test($('#phone').val())) {
                $('#phone-error').text('');
            } else {
                $('#phone-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.phone'], '20'));
                isValid = false;
            }

            if(/^(.{0,255})$/.test($('#address').val())) {
                $('#address-error').text('');
            } else {
                $('#address-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payeeMaster.addres'], '255'));
                isValid = false;
            }

            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled'))
			{
				var form = document.getElementById('payeeMasterForm');
			    form.action = CONTEXT_PATH + "/admin/payeeMaster/save";
			    form.submit();
			}
		},
		newPayeeMaster: function(){
			$('#code').val('').attr("readonly", false);
		    $('#name').val('').attr("readonly", false);
		    $('#phone').val('').attr("readonly", false);
		    $('#address').val('').attr("readonly", false);
		    $('#paymentTerm').val('').attr("readonly", false);
		    $('#bankName').val('').attr("readonly", false);
		    $('#bankName').val('').attr("readonly", false);
		    $('#bankAccountCode').val('').attr("readonly", false);
		    $('button[name="saveButton"]').removeClass('disabled');
		    
            // focus form
            $('#payeeMasterForm').goTo();
            $("#code").focus();
		}
	};
}());