	var getCheckBox = function() {
    var ids = [];
    $("input[name='selectedCheckBox']:checkbox:checked").each(function() {
        ids.push($(this).attr("value"));
    });
    var listId = {
        id : ids
    };
    return listId;
}

/**
 * Delete, restore, approve by ajax
 */
var actionAjax = function(listId, url, dataTable) {
    $.ajax({
        type : "POST",
        url : url,
        data : listId,
        success : function(result) {
            $('#alertContent').removeClass();
            $('#alertIcon').removeClass();
            if (result.status == "invalid") {
                $('#alertContent').addClass('alert alert-danger alert-dismissible flat ');
                $('#alertIcon').addClass('icon fa fa-ban');
            } else {
                $('#alertContent').addClass('alert alert-info alert-dismissible flat ');
                $('#alertIcon').addClass('icon fa fa-check');
            }
            location.reload();
        },
        traditional : true
    });
    $('#btnDelete').attr("disabled", true);
    $('#btnRestore').attr("disabled", true);
}

function editIcon (data, full){
    var icon = '';
    icon += '<span class="icon-action edit-ajax" id="' + data + '">'
        +'<img title="Edit" width="14px" src="../img/icons/edit.png" /></span>';
    return icon;
}
var iconAction = function(data, full) {
    var icon = '';
    if (full.valid == 0) {
        icon += '<span data-toggle="modal" data-target="#popConfirm" class="icon-action restore-ajax" id="' + data + '">'
        +'<img title="Revert" width="14px" src="../img/icons/restore.png" /></span>';
    } else {
        icon += '<span class="glyphicon glyphicon-trash delete-ajax" id="' + data + '"></span>';
        /*icon += '<span class="icon-action edit-ajax" id="' + data + '">'
        +'<img title="Chỉnh sửa" width="14px" src="../img/icons/edit.png" /></span>';*/
    }
    return icon;
}
function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

$(document).ready(function(){
	
    $('#btnDelete').attr("disabled", true);
    $('#btnRestore').attr("disabled", true);
    $.fn.dataTable.render.xss = function ( cutoff ) {
        return function ( data, type, row ) {
            var escaped = escapeHtml(data);
            return escaped;
        };
    };
    
    $.extend( true, $.fn.dataTable.defaults, {
        "processing" : false,
        "serverSide" : true,
        "ordering" : true,
        "info" : true,
        "searching" : true,
        "pagingType" : "simple_numbers",
        "lengthChange" : false,
        "autoWidth" : false,
        'order' : [ [ 2, 'desc' ] ],
        'columnDefs' : [ {
            'targets' : 0,
            'searchable' : false,
            'orderable' : false,
            'className' : 'dt-body-center',
            'render' : function(data, type, full, meta) {
              return '<input type="checkbox" name="selectedCheckBox" class="select" value="'
              + $('<div/>').text(data).html() + '">';
            }
          } ],
        "language" : {
            "zeroRecords" : GLOBAL_MESSAGES['cashflow.common.noData'],
            "emptyTable": GLOBAL_MESSAGES['cashflow.common.noData'],
            "info": GLOBAL_MESSAGES['cashflow.common.dataTableInfo1'],
            "infoEmpty": GLOBAL_MESSAGES['cashflow.common.dataTableInfo2'],
            //"infoFiltered": " (filter from _MAX_ records)",
            "infoFiltered": "",
            "search": "Search",
            "processing" : "<img src='/resources/css/img/ajax-loader.gif'>",
            "paginate" : {
              "first" : GLOBAL_MESSAGES['cashflow.common.dataTablefirst'],
              "last" : GLOBAL_MESSAGES['cashflow.common.dataTableLast'],
              "next" : GLOBAL_MESSAGES['cashflow.common.dataTableNext'],
              "previous" : GLOBAL_MESSAGES['cashflow.common.dataTablePrev']
            },
            "lengthMenu": GLOBAL_MESSAGES['cashflow.common.show'] + ' <select class="form-control">'+
                '<option value="10">10</option>'+
                '<option value="20">20</option>'+
                '<option value="50">50</option>'+
                '<option value="-1">' + GLOBAL_MESSAGES['cashflow.common.all'] + '</option>'+
            '</select> ' + GLOBAL_MESSAGES['cashflow.common.entries']
          }
    });
});