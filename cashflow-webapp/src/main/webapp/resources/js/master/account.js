/**
 * @author ThoaiNH 
 * create Oct 06, 2017
 */
(function () {
	accountMaster = {
		dataTable: null,
		init: function () {
			accountMaster.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/admin/accountMaster/api/getAllPagingAccount",
		            type: "post",
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	hideAjaxStatus();
		            	$("#table_data tbody td:not(:last-child)").attr('title',GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
		            	$("#table_data tbody td:not(:last-child)").css('cursor','pointer');
		            }	
		        },
		        columns: [
		        	{
		            "data" : null,
		            "visible": false,
			        }, 
			        {
			            "data" : "code",
			            "orderable": true
			        }, 
			        {
			            "data" : "parent code",
			            "orderable": true,
			            "defaultContent": ""	
			        },
			        {
			            "data" : "name",
			            "orderable": true
			        },
			        {
			            "data" : "type",
			            "orderable": true,
			            "defaultContent": ""
			        },
			        {
			            "data": "code",
			            "orderable": false,
			            "render": function(data, type, full, meta) {
			            	if(GLOBAL_PERMISSION["PMS-004"])
			            		return iconAction(data, full);
			            	else
			            		return "";
		                 }
			                        
			        }
		        ],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true
			});
		},
		eventListener: function () {
			if(GLOBAL_PERMISSION["PMS-003"])
			{
				$( document ).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function(){
			        $('#code').val($(this).closest('tr').children('td:nth-child(1)').text()).attr("readonly", true);
			        $('#parentCode').val($(this).closest('tr').children('td:nth-child(2)').text()).attr("readonly", false);
			        $('#name').val($(this).closest('tr').children('td:nth-child(3)').text()).attr("readonly", false);
			        $('#type').val($(this).closest('tr').children('td:nth-child(4)').text()).attr("readonly", false);
			        $('#mode').val("Edit");
			        $('button[name="saveButton"]').removeClass('disabled');
			    });
			}
		    $('body').on('click', '.edit-ajax', function() {
		        var id = $(this).attr('id');
		    });
			 $('body').on('click', '.delete-ajax', function() {
		        var code = $(this).attr('id');
		        data = {
		            id: code
		        };
		        url = CONTEXT_PATH + "/admin/accountMaster/delete";
		        $('#popConfirm').modal('toggle');
		        $('button[name="btnConfirm"]').on('click', function() {
		            actionAjax(data, url, accountMaster.dataTable);
		        });
		        
		    });
		},
		save: function(){
		    var isValid = true;

            // === validate input fields ===
            if(/^(.{1,20})$/.test($('#code').val())) {
                $('#code-error').html('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.accountMaster.code'], '1', '20'));
                isValid = false;
            }
            
            if(/^(.{1,100})$/.test($('#name').val())) {
                $('#name-error').html('');
            } else {
                $('#name-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.accountMaster.name'], '1', '100'));
                isValid = false;
            }
            
            if(/^(.{0,20})$/.test($('#parentCode').val())) {
                $('#parentCode-error').html('');
            } else {
                $('#parentCode-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.accountMaster.parentCode'], '20'));
                isValid = false;
            }
            
            if(/^(.{0,40})$/.test($('#type').val())) {
                $('#type-error').text('');
            } else {
                $('#type-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.accountMaster.type'], '40'));
                isValid = false;
            }
            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled'))
			{
				var form = document.getElementById('accountMasterForm');
			    form.action = CONTEXT_PATH + "/admin/accountMaster/save";
			    form.submit();
			}
		},
		newAccountMaster: function(){
			$('#code').val('').attr("readonly", false);
		    $('#parentCode').val('').attr("readonly", false);
		    $('#name').val('').attr("readonly", false);
		    $('#type').val('').attr("readonly", false);
		    $('#mode').val("New");
		    $('button[name="saveButton"]').removeClass('disabled');
		    
            // focus form
            $('#accountMasterForm').goTo();
            $("#code").focus();
		}
	};
}());