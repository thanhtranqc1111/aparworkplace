/**
 * @author ThoaiNH 
 * create Oct 09, 2017
 */
(function () {
	bankAccountMaster = {
		dataTable: null,
		init: function () {
			bankAccountMaster.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/admin/bankAccountMaster/api/getAllPagingBankAccount",
		            type: "post",
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	hideAjaxStatus();
		            	$("#table_data tbody td:not(:last-child)").attr('title',GLOBAL_MESSAGES['cashflow.common.doubleClickToEdit']);
		            	$("#table_data tbody td:not(:last-child)").css('cursor','pointer');
		            }	
		        },
		        columns: [
		        	{
		            "data" : null,
		            "visible": false,
			        }, 
			        {
			            "data" : "code",
			            "orderable": true
			        }, 
			        {
			            "data" : "type",
			            "orderable": true
			        },
			        {
			            "data" : "name",
			            "orderable": true
			        },
			        {
			            "data" : "bankAccountNo",
			            "orderable": true
			        },
			        {
			            "data" : "bankCode",
			            "orderable": true
			        },
			        {
			            "data" : "currencyCode",
			            "orderable": true
			        },
			        {
			            "data" : "beginningBalance",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "endingBalance",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data": "code",
			            "orderable": false,
			            "render": function(data, type, full, meta) {
			            	if(GLOBAL_PERMISSION["PMS-004"])
			            		return iconAction(data, full);
			            	else
			            		return "";
		                 }
			                        
			        }
		        ],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true
			});
			
			$('#beginningBalance').setupInputNumberMask({
				allowMinus: false
			});
		},
		eventListener: function () {
			if(GLOBAL_PERMISSION["PMS-003"])
			{
				$( document ).on("dblclick", "tbody tr[role='row'] td:not(:last-child)", function(){
			        $('#code').val($(this).closest('tr').children('td:nth-child(1)').text()).attr("readonly", true);
			        $('#type').val($(this).closest('tr').children('td:nth-child(2)').text()).attr("readonly", false);
			        $('#name').val($(this).closest('tr').children('td:nth-child(3)').text()).attr("readonly", false);
			        $('#bankAccountNo').val($(this).closest('tr').children('td:nth-child(4)').text()).attr("readonly", false);
			        $('#bankCode').val($(this).closest('tr').children('td:nth-child(5)').text()).attr("readonly", false);
			        $('#currencyCode').val($(this).closest('tr').children('td:nth-child(6)').text()).attr("readonly", false);
			        $('#beginningBalance').val($(this).closest('tr').children('td:nth-child(7)').text()).attr("readonly", false);
			        $('#endingBalance').val($(this).closest('tr').children('td:nth-child(8)').text()).attr("readonly", false);
			        $('#mode').val("Edit");
			        $('button[name="saveButton"]').removeClass('disabled');	
			    });
			}
		    $('body').on('click', '.edit-ajax', function() {
		        var id = $(this).attr('id');
		    });
			 $('body').on('click', '.delete-ajax', function() {
		        var code = $(this).attr('id');
		        data = {
		            id: code
		        };
		        url = CONTEXT_PATH + "/admin/bankAccountMaster/delete";
		        $('#popConfirm').modal('toggle');
		        $('button[name="btnConfirm"]').on('click', function() {
		            actionAjax(data, url, bankAccountMaster.dataTable);
		        });
		        
		    });
		},
		save: function(){
		    var isValid = true;

            // === validate input fields ===
            if(/^(.{1,20})$/.test($('#code').val())) {
                $('#code-error').text('');
            } else {
                $('#code-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.bankAccountMaster.code'], '1', '20'));
                isValid = false;
            }

            if(/^(.{1,100})$/.test($('#name').val())) {
                $('#name-error').text('');
            } else {
                $('#name-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.bankAccountMaster.name'], '1', '100'));
                isValid = false;
            }

            if(/^(.{0,20})$/.test($('#type').val())) {
                $('#type-error').text('');
            } else {
                $('#type-error').text(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.bankAccountMaster.type'], '20'));
                isValid = false;
            }

            if(/^(.{1,40})$/.test($('#bankAccountNo').val())) {
                $('#bankAccountNo-error').text('');
            } else {
                $('#bankAccountNo-error').text(GLOBAL_MESSAGES['cashflow.common.betweenError'].format(GLOBAL_MESSAGES['cashflow.bankAccountMaster.bankAccountNo'], '1', '40'));
                isValid = false;
            }
            // === end: validate input fields ===

            if (isValid == true && !$('button[name="saveButton"]').hasClass('disabled'))
			{
				$('#endingBalance').val( $('#beginningBalance').val());
				var form = document.getElementById('bankAccountMasterForm');
			    form.action = CONTEXT_PATH + "/admin/bankAccountMaster/save";
			    form.submit();
			}
		},
		newBankAccountMaster: function(){
			$('#code').val('').attr("readonly", false);
	        $('#type').val('').attr("readonly", false);
	        $('#name').val('').attr("readonly", false);
	        $('#beginningBalance').val('').attr("readonly", false);
	        $('#endingBalance').val('').attr("readonly", false);
	        $('#bankAccountNo').val('').attr("readonly", false);
		    $('#mode').val("New");
		    $('button[name="saveButton"]').removeClass('disabled');

            // focus form
            $('#bankAccountMasterForm').goTo();
            $("#code").focus();
		}
	};
}());