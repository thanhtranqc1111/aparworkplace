/**
 * @author Tien.tm 
 * create Oct 31, 2017
 */
(function () {
	grantSub = {
		dataList: [],
		init: function () {
			
		},
		eventListener: function () {
			$(document).on('change','input[name="subsidiary-user"]',function(){
				$('.btn-save-all').removeClass('disabled');
				var subsidiaryId = $(this).attr('subsidiaryId');
				var userAccountId = $(this).attr('userAccountId');
				var value = this.checked ? 1 : 0;
				var flag = 0;
				grantSub.dataList.forEach(function(item){
					if(item.userAccountId == userAccountId && item.subsidiaryId == subsidiaryId)
					{
						item.value = value;
						flag = 1;
						return;
					}
				});
				if(flag == 0)
				{
					grantSub.dataList.push({
						userAccountId: userAccountId,
						subsidiaryId: subsidiaryId,
						value: value
					})
				}
			});
			
			$(document).on('click','.btn-save-all',function(){
				$('#isConfirmChanged').val(false);
				grantSub.save();
			});
			
			$(document).on('click','.btn-grant-sub',function(){
				$('#isConfirmChanged').val(false);
				window.location.reload();
			});
		},
		save: function(){
			var formData = new FormData();
			formData.append('data', JSON.stringify(grantSub.dataList));
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/subsidiary/grantSub/save',
	            cache : false,
				contentType : false,
				processData : false,
				data : formData,
	            success : function(ajaxResult) {
	            	 var resultData = JSON.parse(ajaxResult);
	            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
            				 window.location.reload();
	            		 });
	            	 }
	            	 else
	            	 {
	            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
	            	 }
	            }
	        });
		}
	};
}());