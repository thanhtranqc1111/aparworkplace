/**
 * @author Tien.tm 
 * create Oct 31, 2017
 */
(function () {
	subList = {
		dataTable: null,
		rows_selected: [], //id of checked row in data table
		dataList: null,//list data of main table
		txtSearch: "",
		crNewId: 0,//id of new role
		fieldIndex: {1: "code", 2: "name", 3: "address", 4: "status"},
		init: function () {
			jsModel  = subList;
			//set up data table
			subList.dataTable = $('#sub-table').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/subsidiary/api/getAllSubsidiary",
		            type: "GET",
		            data: function ( d ) {
		                d.keyword = subList.txtSearch;
		            },
		            error: function() { // error handling
		                $(".sub-table-error").html("");
		                $("#sub-table").append('<tbody class="table_data-error"><tr><td colspan="10">No data</td></tr></tbody>');
		                $("#sub-table_processing").css("display", "none");
		            },
		            complete: function(data){
		            	
		            	$('#sub-table').tableCheckable(subList.dataTable, subList.rows_selected, subList.renderDeleteButton);
		            	hideAjaxStatus();
		            }
		        },
		        columns: [
			        {
			            "data" : "code",
		            	"render": function(data, type, full, meta) {
		                	return "<a href='detail/"+full.id+"'>"+data+"</a>";
		                 }
			        }, 
			        {
			            "data" : "name"
			        },
			        {
			            "data" : "address"
			        },
			        {
			            "data" : "status",
			            "render": function(data, type, full, meta) {
		                	if (data == true){
		                		return "Active";
		                	} else {
		                		return "Inactive";
		                	}
		                 }
			        }
			        
		        ]
		        ,
		        "columnDefs": [
		        ],
		        searching: false,
		        "ordering" : false,
		        "paging": false,
		        "info" : false
			 });
			
		},
		eventListener: function () {
			$(document).on('click','.btn-search',function(){
				subList.txtSearch = $('#txt-search').val();
				subList.dataTable.ajax.reload();
			});

			
			
			$(document).on("click",".btn-delete",function(){
				if(!$(this).hasClass('disabled'))
		    	{
		    		$('#popConfirm').modal('toggle');
			    	$('button[name="btnConfirm"]').on('click', function() {
			    		subList.deleteSubsidiary();
			        });
		    	}
		    });
			
			$(document).on('click','.btn-reset-sub',function(){
				$('#isConfirmChanged').val(false);
			});
			
		},
		renderDeleteButton: function(){
			// Always disable delete button. Delete subsidiary function is not valid.
			//if(subList.rows_selected.length > 0)
			//	$('.btn-delete').removeClass('disabled');
			//else
			//	$('.btn-delete').addClass('disabled');
		},
		
		deleteSubsidiary : function(){
			var deleteIds = [];
			subList.rows_selected.forEach(function(id){
				console.log(id);
				if(id > 0)
				{
					deleteIds.push(id);
				}
			});
			if(deleteIds.length > 0) {
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/subsidiary/delete',
		            data: "&id=" +  deleteIds,
		            success : function(result) {
		            	showPopupMessage(GLOBAL_MESSAGES['cashflow.common.deletedSuccessfully'], function(){
		            		location.reload();
		            	});	               
		            }
		        });
			}
		},
		
		saveSubsidiary: function() {
			var valid = true;
			var message = [];
			if(isEmpty($('#code').val())){
				message.push(validate.require(GLOBAL_MESSAGES['cashflow.subsidiaryDetail.code']));
				valid = false;
			}
			if(isEmpty($('#currency').val())){
				message.push(validate.require(GLOBAL_MESSAGES['cashflow.subsidiaryDetail.currency']));
				valid = false;
			}
			if(isEmpty($('#name').val())){
				message.push(validate.require(GLOBAL_MESSAGES['cashflow.subsidiaryDetail.name']));
				valid = false;
			}
			
			if(valid) {
				$('#isConfirmChanged').val(false);
				$('#subsidiaryForm').submit();
			} else {
				$('.error-area').showAlertMessage(message)
			}
			
		},
		
	};
}());