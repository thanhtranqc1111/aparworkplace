(function () {
	ReimPaymentOrder = {
		PAYMENT_STATUSES: {},
		init: function () {
			//$('.error-area').showAlertMessage(['1123']);
			
			var ddmmyyyy = 'dd/mm/yyyy';

		    $('input').filter(function(){
				return this.id.match(/^payment-reim-amount-original-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event) {
					ReimPaymentOrder.calculateReimAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
			    	PaymentOrder.calculateTotalOriginalAmount();
				});
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^reim-pay-rate-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
					ReimPaymentOrder.calculateReimAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
					PaymentOrder.calculateTotalOriginalAmount();
					
					// disable variance account 
					ReimPaymentOrder.readonlyVarianceAccount(index);
				})
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^payment-reim-converted-amount-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event) {
					ReimPaymentOrder.calculateReimAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
					PaymentOrder.calculateTotalOriginalAmount();
				});
			});
		    
		    //disable reimbursement is not approval 
		    $('input').filter(function(){
				return this.id.match(/^reim-is-approval-*/);
			}).each(function(){
				if(this.value == 'false'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //disable reimbursement is paid 
		    $('input').filter(function(){
				return this.id.match(/^reim-is-paid-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //disable reimbursement is pay enough 
		    $('input').filter(function(){
				return this.id.match(/^reim-is-pay-enough-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    $('input').filter(function() {
		        return this.id.match(/^reim-variance-account-code-*/);
		    }).each(function(){
		    	var tokens = this.id.split('-');
		    	var rowId = tokens[tokens.length -1];
		    	var accountVal = ReimPaymentOrder.addAccountAutoComplete(rowId);
		    	accountVal.settext(this.value);
		    });
		    $('#payment-reim-table input[autocomplete]').attr("readonly", true);
			$('#payment-reim-table').DataTable( {
		        "scrollY": 200,
		        "scrollX": true,
		        "searching": false,
		        "paging": false,
		        "info": false,
		        "serverSide" : false,
		        "ordering" : false,
		        "columnDefs": [
		            { "width": "2%", "targets": 0 },
		            { "width": "5%", "targets": 1 },
		            { "width": "5%", "targets": 2 },
		            { "width": "8%", "targets": 3 },
		            { "width": "7%", "targets": 4 },
		            { "width": "5%", "targets": 5 },
		            { "width": "8%", "targets": 6 },
		            { "width": "8%", "targets": 7 },
		            { "width": "10%", "targets": 8 },
		            { "width": "8%", "targets": 9 },
		            { "width": "10%", "targets": 10 },
		            { "width": "8%", "targets": 11 },
		            { "width": "8%", "targets": 12 },
		            { "width": "8%", "targets": 13 }
		         ],
		        "drawCallback": function( settings ) {
		            $('.payment-reim-table-wrapper').css('width','100%');
		        }
		    } );
		},
		
		eventListener: function () {
			//Do check all check box in table and enable for some input relate
			$("#payment-reim-table_wrapper .detail-id-all-cb").change(function(){
				var checked = this.checked;
				
				if(checked) {
					$( "#payment-reim-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "checked");
						ReimPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}else{
					$( "#payment-reim-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "");
						ReimPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}
			});
			
			//Trigger action check for each check box
			$("#payment-reim-table_wrapper .detail-id-cb").change(function() {
				var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
			    if(!this.checked) {
			    	$("#payment-reim-table_wrapper .detail-id-all-cb").prop('checked', ""); // uncheck check box all
			    }
			    PaymentOrder.addAccountPayable();
			    PaymentOrder.checkOriginalCurrency();
			    ReimPaymentOrder.enableInputInListDetail(rowId, !this.checked);
			});
		},
		
		calculateReimAmountConverted: function(rowId, event) {
			let targetId = event.target.id;
			var fxRate = getNewDecimal($('#reim-fx-rate-' + rowId).val());
			var payOrginalAmount = getNewDecimal($('#payment-reim-amount-original-' + rowId).autoNumeric('get'));
			var payRate = getNewDecimal($('#reim-pay-rate-' + rowId).autoNumeric('get'));
			var payConvertedAmount = getNewDecimal($('#payment-reim-converted-amount-' + rowId).autoNumeric('get'));
			
			if (targetId.indexOf('payment-reim-amount-original-') >= 0) {
				if(payOrginalAmount.toNumber() != 0 && payRate.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('reim-pay-rate-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('payment-reim-converted-amount-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payRate = payConvertedAmount.dividedBy(payOrginalAmount);
				}
			}
			
			$('#reim-pay-rate-' + rowId).autoNumeric('set', payRate.toNumber());
			$('#payment-reim-converted-amount-' + rowId).autoNumeric('set', payConvertedAmount.toNumber());

			//
			var varianceAmount = getNewDecimal(payOrginalAmount.mul(payRate).toFixed(2, 1)).minus(getNewDecimal(payOrginalAmount.mul(fxRate)).toFixed(2, 1));
			$('#reim-variance-amount-' + rowId).autoNumeric('set', varianceAmount.toNumber());
			
		},
		
		enableInputInListDetail: function(rowId, isEnable){
			$("#payment-reim-amount-original-"+rowId).prop('readonly', isEnable);
			$("#reim-pay-rate-"+rowId).prop('readonly', isEnable);
			$("#payment-reim-converted-amount-"+rowId).prop('readonly', isEnable);
			$("#reim-variance-account-code-"+rowId).prop('readonly', isEnable);
			$("#reim-variance-account-code-"+rowId).parents('.acontainer').find('input[autocomplete]').attr("readonly", isEnable);
			$('#payment-reim-amount-original-' + rowId).autoNumeric('set', 0);
			
			// calculate Payment Order Amount
			if(!isEnable){
				var reimAmountOriginal = getNewDecimal($('#reim-original-amount-' + rowId).autoNumeric('get'));
				var paidAmount = getNewDecimal($('#reim-paid-original-amount-' + rowId).autoNumeric('get'));
				$('#payment-reim-amount-original-' + rowId).autoNumeric('set', (reimAmountOriginal.minus(paidAmount)).valueOf());
			}
		},
		
		readonlyVarianceAccount : function (rowId){
			var fxRate = Number($('#reim-fx-rate-' + rowId).val());
	    	var payRate = Number($('#reim-pay-rate-' + rowId).autoNumeric('get'));
	    	if(fxRate == payRate) {
	    		$('#reim-variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').attr('readonly',true);
	    		$('#reim-variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').val('');
	    		$('#reim-variance-account-code-' +rowId).val('');
	    	} else {
	    		$('#reim-variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').attr('readonly',false);
	    	}
		},
		
		addAccountAutoComplete: function(idCounter){
			var elementId = 'reim-variance-account-code-'+ idCounter;
		    var accountVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns : [
						label('cashflow.paymentOrder.detail.accountCode'),
						label('cashflow.paymentOrder.detail.parentCode'),
						label('cashflow.paymentOrder.detail.accountName'),
						label('cashflow.paymentOrder.detail.type') 
					],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountVal.all();
		        	var accountNameId = 'reim-variance-account-code-'+ idCounter;
		        	accountVal.settext(selectedData[label('cashflow.paymentOrder.detail.accountCode')]);
		        	$('#' + accountNameId).val(selectedData[label('cashflow.paymentOrder.detail.accountCode')]);
		        }
			});
		    return accountVal;
		},

	};
}());