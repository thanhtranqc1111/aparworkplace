(function () {
	PaymentSearch = {
		dataTable: null, //main data table
		fromDate: null,
		toDate: null,
		bankRef: null,
		paymentOrderNoFrom: null,
		paymentOrderNoTo: null,
		status: null,
		init: function () {
			$('#datepicker input').datepicker({
				format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
			});
			
			PaymentSearch.fromDate = $('#paidFromDate').val();
			PaymentSearch.toDate = $('#paidToDate').val();
			PaymentSearch.bankRef = $('#bankRef').val();
			PaymentSearch.paymentOrderNoFrom = $('#paymentOrderNoFrom').val();
			PaymentSearch.paymentOrderNoTo = $('#paymentOrderNoTo').val();
			PaymentSearch.status = $('#status').val();
			PaymentSearch.dataTable = $('#payment-tbl').removeAttr('width').DataTable({
				"ajax": {
		            url: "/payment/api/getAllPagingPaymentOrder",
		            type: "GET",
		            data: function ( d ) {
		            	d.fromDate = PaymentSearch.fromDate;
		                d.toDate = PaymentSearch.toDate;
		                d.bankRef = PaymentSearch.bankRef;
		            	d.paymentOrderNoFrom = PaymentSearch.paymentOrderNoFrom;
		            	d.paymentOrderNoTo = PaymentSearch.paymentOrderNoTo;
		                d.status = PaymentSearch.status;
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="15">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
						hideAjaxStatus();
						PaymentSearch.mergeRows('#payment-tbl', 1, 8);
						PaymentSearch.mergeRows('#payment-tbl', 9, 14);
		            }
				},
				columns: [
			        {
			            "data" : "paymentOrderNo",
			            "width" : "5%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return "<a href='detail/"+full.paymentOrderNo+"'>"+data+"</a>";
			            }
			        },
			        {
			            "data" : "valueDate",
			            "width" : "3%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	if(full.valueDate != undefined){
			            		return "<span class='right' >" + full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year + "</span>";
			            	}
			            }
			        },
			        {
			            "data" : "bankName",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankName = "";
			            	if(full.bankName != undefined){
			            		bankName = full.bankName;
			            	}
			            	return bankName;
			            }
			        },
			        {
			            "data" : "bankAccount",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankAccount = "";
			            	if(full.bankAccount != undefined){
			            		bankAccount = full.bankAccount;
			            	}
			            	return bankAccount;
			            }
			        },
			        {
			        	"data" : "bankStatement",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var trans = "";
			            	var length = data.length;
			            	$.each(data, function(i, obj) {
			            		trans += "<a href='/bankStatement/list/"+obj.id+"' target='_blank'>"+obj.id+"</a>";
			            		if ( i != length - 1){
			            			trans += ", ";
			            		}
			            		
			            	});
			            	return trans;
			            }
			        },
			        {
			            "data" : "bankRefNo",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.bankRefNo != undefined){
			            		return "<span style='white-space:pre-wrap;'>"+data+"</span>";
			            	}
			            }
			        },
			        {
			        	"data" : "includeGstConvertedAmount",
			            "width" : "5%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return "<span class='pull-left'>" +  $('#currentCurrency').val() + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "paymentStatus",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return "<div class='ap-status  pay-"+full.paymentStatusCode+"'>"+data+"</div>";
			            }
			        },
			        {
			            "data" : "accountPayableNo",
			            "width" : "5%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var accountPayableNo = "";
			            	if(full.accountPayableNo != undefined){
			            		switch (full.paymentType) {
			            		  case "0": accountPayableNo = "<a href='/apinvoice/detail/"+full.accountPayableNo+"'>"+data+"</a>"; break;
			            		  case "1":
			            			  if(GLOBAL_PERMISSION["PMS-009"])
			            				  accountPayableNo = "<a href='/payroll/detail/"+full.accountPayableNo+"'>"+data+"</a>";
						              else
						            	  accountPayableNo = data;
			            			  break;
			            		  case "2": accountPayableNo = "<a href='/reimbursement/detail/"+full.accountPayableNo+"'>"+data+"</a>"; break;
			            		  default :  break;
			            		}
			            	}
			            	return accountPayableNo;
			            }
			        },
			        {
			            "data" : "month",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.month != undefined){
			            		return "<span class='right' >" + (full.month.month + 1) + "/" + full.month.year + "</span>";
			            	}
			            }
			        },
			        {
			            "data" : "claimType",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var claimType = "";
			            	if(full.claimType != undefined){
			            		claimType = full.claimType;
			            	}
			            	return claimType;
			            }
			        },
			        {
			        	"data" : "totalAmount",
			            "width" : "5%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "invoiceStatus",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var invoiceStatus = "";
			            	if(full.invoiceStatus != undefined){
			            		invoiceStatus = "<div class='ap-status  api-"+full.invoiceStatusCode+"'>"+data+"</div>";
			            	}
			            	return invoiceStatus;
			            }
			        },
		        ],
		        searching: false,
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true,
				'order' : [ [ 0, 'desc' ] ],
		        'columnDefs' : []
			});
			
			
		},
		
		eventListener: function () {
			$(document).on("click","#btn-search",function(){
				PaymentSearch.fromDate = $('#paidFromDate').val();
				PaymentSearch.toDate = $('#paidToDate').val();
				PaymentSearch.bankRef = $('#bankRef').val();
				PaymentSearch.paymentOrderNoFrom = $('#paymentOrderNoFrom').val();
				PaymentSearch.paymentOrderNoTo = $('#paymentOrderNoTo').val();
				PaymentSearch.status = $('#status').val();
				
				PaymentSearch.dataTable.ajax.reload();
		    });
			
			$('#export').click(function(){
				$('#reset').click();
				var href = $(this).attr("href");
				var param = "";
				
				var fromDate = $('#paidFromDate').val();
				var toDate = $('#paidToDate').val();
				var bankRef = $('#bankRef').val();
				var paymentOrderNoFrom = $('#paymentOrderNoFrom').val();
				var paymentOrderNoTo = $('#paymentOrderNoTo').val();
				var status = $('#status').val();
				if (fromDate.length > 0) {
					param += "&fromDate=" + fromDate;
				}
				if (toDate.length > 0) {
					param += "&toDate=" + toDate;
				}
				if (bankRef.length > 0) {
					param += "&bankRef=" + bankRef;
				}
				if (paymentOrderNoFrom.length) {
					param += "&paymentOrderNoFrom=" + paymentOrderNoFrom;
				}
				if (paymentOrderNoTo.length) {
					param += "&paymentOrderNoTo=" + paymentOrderNoTo;
				}
				if (status.length) {
					param += "&status=" + status;
				}
				if (param.length > 0) {
					href = href + "?" + param.slice(1);
				}
				
				$("#export").attr("href", href);
			});
			
			//clear data search condition form
			$('#btn-reset').click(function(){
				$('#paidFromDate').val('');
				$('#paidToDate').val('');
				$('#bankRef').val('');
				$('#paymentOrderNoFrom').val('');
				$('#paymentOrderNoTo').val('');
				$('#status').val('');
			});
		},
		
		mergeRows : function mergeRows(table, mergeFrom, mergeTo) {
			var rowsList = $(''+table+ ' > tbody').find('tr');
			var previous = null, cellToExtend = null, rowspan = 1;
			
			rowsList.each(function(index, e) {
				var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
				if (previous == content && content !== "" ) {
					rowspan = rowspan + 1;
					
					for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
						if(i >= mergeFrom && i <= mergeTo) {
							$(this).find('td:nth-child(' + i + ')').addClass('hidden');
							padding = rowspan * 16;
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
						} 
					}
				} else {
		             rowspan = 1;
		             previous = content;
		             cellToExtend = elementTab;
				}
			});
			$(table + ' td.hidden').remove();
		}
		
	};
}());