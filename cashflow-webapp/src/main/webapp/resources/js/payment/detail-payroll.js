(function () {
	PayrollPaymentOrder = {
		PAYMENT_STATUSES: {},
		init: function () {
			//$('.error-area').showAlertMessage(['1123']);
			
			var ddmmyyyy = 'dd/mm/yyyy';

		    $('input').filter(function(){
				return this.id.match(/^net-payment-amount-original-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event) {
					PayrollPaymentOrder.calculatePRAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
			    	PaymentOrder.calculateTotalOriginalAmount();
				});
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^pr-pay-rate-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
					PayrollPaymentOrder.calculatePRAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
					PaymentOrder.calculateTotalOriginalAmount();
				})
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^net-payment-amount-converted-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event) {
					PayrollPaymentOrder.calculatePRAmountConverted(index, event);
					PaymentOrder.calculateTotalAmount();
					PaymentOrder.calculateTotalOriginalAmount();
				});
			});
		    
		    //disable payroll is not approval 
		    $('input').filter(function(){
				return this.id.match(/pr-is-approval-*/);
			}).each(function(){
				if(this.value == 'false'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		  //disable payroll is paid 
		    $('input').filter(function(){
				return this.id.match(/pr-is-paid-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //disable payroll is pay enough 
		    $('input').filter(function(){
				return this.id.match(/pr-is-pay-enough-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
			$('#payment-pr-table').DataTable( {
		        "scrollY": 200,
		        "scrollX": true,
		        "searching": false,
		        "paging": false,
		        "info": false,
		        "serverSide" : false,
		        "ordering" : false,
		        "columnDefs": [
		            { "width": "2%", "targets": 0 },
		            { "width": "8%", "targets": 1 },
		            { "width": "8%", "targets": 2 },
		            { "width": "7%", "targets": 3 },
		            { "width": "8%", "targets": 4 },
		            { "width": "10%", "targets": 5 },
		            { "width": "8%", "targets": 6 },
		            { "width": "12%", "targets": 7 },
		            { "width": "8%", "targets": 8 },
		            { "width": "13%", "targets": 9 },
		            { "width": "13%", "targets": 10 }
		         ],
		        "drawCallback": function( settings ) {
		            $('.payment-pr-table-wrapper').css('width','100%');
		        }
		    } );
		},
		
		eventListener: function () {
			//Do check all check box in table and enable for some input relate
			$("#payment-pr-table_wrapper .detail-id-all-cb").change(function(){
				var checked = this.checked;
				
				if(checked) {
					$( "#payment-pr-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "checked");
						PayrollPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}else{
					$( "#payment-pr-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "");
						PayrollPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}
			});
			
			//Trigger action check for each check box
			$("#payment-pr-table_wrapper .detail-id-cb").change(function() {
				var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
			    if(!this.checked) {
			    	$("#payment-pr-table_wrapper .detail-id-all-cb").prop('checked', ""); // uncheck check box all
			    }
			    PaymentOrder.addAccountPayable();
			    PaymentOrder.checkOriginalCurrency();
			    PayrollPaymentOrder.enableInputInListDetail(rowId, !this.checked);
			});
			
		},
		
		calculatePRAmountConverted: function(rowId, event) {
			let targetId = event.target.id;
			var payOrginalAmount = getNewDecimal($('#net-payment-amount-original-' + rowId).autoNumeric('get'));
			var payRate = getNewDecimal($('#pr-pay-rate-' + rowId).autoNumeric('get'));
			var payConvertedAmount = getNewDecimal($('#net-payment-amount-converted-' + rowId).autoNumeric('get'));
			
			if (targetId.indexOf('net-payment-amount-original-') >= 0) {
				if(payOrginalAmount.toNumber() != 0 && payRate.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('pr-pay-rate-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('net-payment-amount-converted-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payRate = payConvertedAmount.dividedBy(payOrginalAmount);
				}
			}
			
			$('#pr-pay-rate-' + rowId).autoNumeric('set', payRate.toNumber());
			$('#net-payment-amount-converted-' + rowId).autoNumeric('set', payConvertedAmount.toNumber());
			
		},
		
		enableInputInListDetail: function(rowId, isEnable){
			$("#net-payment-amount-original-"+rowId).prop('readonly', isEnable);
			$("#pr-pay-rate-"+rowId).prop('readonly', isEnable);
			$("#net-payment-amount-converted-"+rowId).prop('readonly', isEnable);
			$('#net-payment-amount-orginal-' + rowId).autoNumeric('set', 0);
			
			// calculate Payment Order Amount
			if(!isEnable){
				var netPaymentAmount = getNewDecimal($('#total-net-payment-' + rowId).autoNumeric('get'));
				var paidAmount = getNewDecimal($('#paid-net-payment-amount-' + rowId).autoNumeric('get'));
				$('#net-payment-amount-original-' + rowId).autoNumeric('set', (netPaymentAmount.minus(paidAmount)).valueOf());
			}
			
		},


	};
}());