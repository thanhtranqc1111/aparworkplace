(function () {
	PaymentOrder = {
		PAYMENT_STATUSES: {},
		init: function () {
			//$('.error-area').showAlertMessage(['1123']);
			
			var ddmmyyyy = 'dd/mm/yyyy';

		    $('#valueDate').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    $('#searchForm_scheduledFrom, #searchForm_scheduledTo').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    $('#searchForm_monthFrom, #searchForm_monthTo').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom'
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^payment-include-gst-original-amount-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event){
			    	PaymentOrder.calculateAmountConverted(index, event);
			    	PaymentOrder.calculateTotalAmount();
			    	PaymentOrder.calculateTotalOriginalAmount();
				})
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^payment-include-gst-converted-amount-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event){
			    	PaymentOrder.calculateAmountConverted(index, event);
			    	PaymentOrder.calculateTotalAmount();
			    	PaymentOrder.calculateTotalOriginalAmount();
				})
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^pay-rate-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
					var rowId = index;
			    	PaymentOrder.calculateAmountConverted(rowId, event);
			    	PaymentOrder.calculateTotalAmount();
			    	PaymentOrder.calculateTotalOriginalAmount();
			    	
			    	// disable variance account 
			    	PaymentOrder.readonlyVarianceAccount(rowId);
				})
			});
		    
		    //disable ap invoice is not approval 
		    $('input').filter(function(){
				return this.id.match(/^is-approval-*/);
			}).each(function(){
				if(this.value == 'false'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //disable ap invoice is paid 
		    $('input').filter(function(){
				return this.id.match(/is-paid-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //disable ap invoice is pay enough 
		    $('input').filter(function(){
				return this.id.match(/is-pay-enough-*/);
			}).each(function(){
				if(this.value == 'true'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    //search details result (hide/show)
		    PaymentOrder.hideSearchDetailArea(false);
		    
		    //Disable input when edit
		    if(modeEdit){
			    
		    	$("#bankName").attr('readonly', true);
		    	$("#bankAccount").attr('readonly', true);
		    	$("#valueDate").attr('readonly', true);
    			$("#bankRefNo").attr('readonly', true);
				$("#description").attr('readonly', true);
				$("#paymentApprovalNo").attr('readonly', true);
				$("#accountPayableCode").attr('readonly', true);
				$("#accountPayableName").attr('readonly', true);
		    	
				//disable all input in area search
				$("#search-zone input, #search-zone select, #search-zone a").attr('disabled', true);
				
				$(".detail-id-all-cb").prop('checked', "checked").attr("readonly", true);
		    	$(".detail-id-cb").prop('checked', "checked").attr("readonly", true);
		    	
		    	$("#save-btn").addClass('disabled');
		    	$("#reset-btn").addClass('disabled');
		    	
		    }else{
		    	$("#save-btn").removeClass('disabled');
		    	$("#edit-btn").addClass('disabled');
		    	$("#copy-btn").addClass('disabled');
		    }
		    
		    PaymentOrder.setReadonlyInput();
		    ////////ADD AUTO COMPLETE FOR ACCOUNTPAYABLE////////
		    var accountPayableVal = $('#accountPayableCode').tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.paymentOrder.detail.accountCode'), label('cashflow.paymentOrder.detail.parentCode'), label('cashflow.paymentOrder.detail.accountName'), label('cashflow.paymentOrder.detail.type')],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountPayableVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountPayableVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountPayableVal.all();
		        	$('#accountPayableCode').val(selectedData[label('cashflow.paymentOrder.detail.accountCode')]);
		        	$('#accountPayableName').val(selectedData[label('cashflow.paymentOrder.detail.accountName')]);
		        }
			});
		    accountPayableVal.settext($('#accountPayableCode').val());
		   
		    // Add Account Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/variance-account-code-*/);
		    }).each(function(index){
		    	var accountVal = PaymentOrder.addAccountAutoComplete(index);
		    	accountVal.settext(this.value);
		    });
			var thePayee = $('#searchForm_payeeName').tautocomplete({
		        highlight: "",
				columns: [
							label('cashflow.paymentOrder.detail.code'),
							label('cashflow.paymentOrder.detail.payeeAccount'),
							label('cashflow.paymentOrder.detail.payeeName'),
							label('cashflow.paymentOrder.detail.address'),
							label('cashflow.paymentOrder.detail.phone'),
							label('cashflow.paymentOrder.detail.type'),
							label('cashflow.paymentOrder.detail.typeName'),
							label('cashflow.paymentOrder.detail.paymentTermCol'),
							label('cashflow.paymentOrder.detail.accountCode'),
							label('cashflow.paymentOrder.detail.accountName') 
						],
				hide: [false, true, true, true, true, false, false, false, false, false],
				addNewDataLink: '/admin/payeeMaster/list',
		        data: function(){
		        		var x = { keyword: APInvoice.thePayee.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/payeeMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = thePayee.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 100,
		        onchange: function () {
		        	var selectedData = thePayee.all();
		        	$('#searchForm_payeeName').val(selectedData[label('cashflow.paymentOrder.detail.payeeName')]);
		        }
			});
			thePayee.settext($('#searchForm_payeeName').val());
			
			
			$('#payment-table input[autocomplete]').attr("readonly", true);
			$('#accountPayableCode').parents('.acontainer').find('input[autocomplete]').attr("readonly", true);
			//Display read only when page on mode edit.
			if(!modeEdit)
			{
				$('#accountPayableCode').parents('.acontainer').find('input[autocomplete]').attr("readonly", false);
				$('#searchForm_payeeName').parents('.acontainer').find('input[autocomplete]').attr("readonly", false);
			}
			
			$('#payment-table').DataTable( {
		        "scrollY": 200,
		        "scrollX": true,
		        "searching": false,
		        "paging": false,
		        "info": false,
		        "serverSide" : false,
		        "ordering" : false,
		        "columnDefs": [
		            { "width": "2%", "targets": 0 },
		            { "width": "4%", "targets": 1 },
		            { "width": "4%", "targets": 2 },
		            { "width": "9%", "targets": 3 },
		            { "width": "3%", "targets": 4 },
		            { "width": "4%", "targets": 5 },
		            { "width": "7%", "targets": 6 },
		            { "width": "7%", "targets": 7 },
		            { "width": "5%", "targets": 8 },
		            { "width": "7%", "targets": 9 },
		            { "width": "5%", "targets": 10 },
		            { "width": "7%", "targets": 11 },
		            { "width": "7%", "targets": 11 },
		            { "width": "7%", "targets": 12 },
		            { "width": "8%", "targets": 13 },
		            { "width": "8%", "targets": 14 },
		            { "width": "8%", "targets": 15 },
		            { "width": "8%", "targets": 16 }
		            
		         ],
		        "drawCallback": function( settings ) {
		            $('.payment-table-wrapper').css('width','100%');
		        }
		    } );

		}, //end init
		
		eventListener: function () {
			if(!$('#search-btn').is('[disabled=disabled]')){
				$("#search-btn").click(function() {
					$("#action").val("search");
					$('#isConfirmChanged').val(false);
					PaymentOrder.reFormatNumber();
					
					// submit form
					$('#form1').submit();
				});
			}
			
			// disable enter key press
			$('#form1').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					e.preventDefault();
					return false;
				}
			});
			
			//cancel btn
			$('#btn-cancel').click(function(e){
				e.preventDefault();
				//window.location = document.referrer;
				window.location = '/payment/list';
			});
			
			//Do check all check box in table and enable for some input relate
			$("#payment-table_wrapper .detail-id-all-cb").change(function(){
				var checked = this.checked;
				
				if(checked) {
					$( "#payment-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "checked");
						PaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}else{
					$( "#payment-table_wrapper tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "");
						PaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}
			});
			
			//Trigger action check for each check box
			$("#payment-table_wrapper .detail-id-cb").change(function() {
				var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
			    if(!this.checked) {
			    	$("#payment-table_wrapper .detail-id-all-cb").prop('checked', ""); // uncheck check box all
			    }
			    
			    PaymentOrder.addAccountPayable();
			    
			    PaymentOrder.checkOriginalCurrency();
			    
			    PaymentOrder.enableInputInListDetail(rowId, !this.checked);
			});
			
			//
			$("#edit-btn").click(function(){
				let status = $('#statusCode').val();
				let enabled = false;
				if (status == '002') { // active dropdown  when status is processing (002)
					$('.ap-label-status').hide();
					$('.ap-dropdown-status').show();
				} else if(status == '001'){
					enabled = true;
				}
				
				$("#bankName").attr('disabled', false).attr('readonly', false);
		    	$("#bankAccount").attr('disabled', false).attr('readonly', false);
		    	$("#valueDate").attr('readonly', false);
    			$("#bankRefNo").attr('readonly', false);
				$("#description").attr('readonly', false);
				$("#paymentApprovalNo").attr('readonly', false);
				$("#accountPayableCode").attr('readonly', false);
				$("#accountPayableName").attr('readonly', false);
				
		    	$('#form1 table .default').attr('readonly', enabled);
		    	$('#form1 table select').attr('readonly', enabled).attr('disabled', enabled);
		    	
		    	$('.col-sm-8 input[autocomplete]').attr("readonly", false);
		    	$('#searchForm_payeeName').parents('.acontainer').find('input[autocomplete]').attr("readonly", true);
		    	
		    	$("#save-btn").removeClass('disabled');
		    	$("#reset-btn").removeClass('disabled');
				
		    	if(!enabled){
		    		// readonly variance account
		    		$('input').filter(function(){
		    			return this.id.match(/^pay-rate-*/);
		    		}).each(function(){
		    			var tokens = this.id.split('-');
		    			var rowId = tokens[tokens.length -1];
		    			PaymentOrder.readonlyVarianceAccount(rowId);
		    		});
		    		
		    		$('input').filter(function(){
						return this.id.match(/reim-is-paid-*/);
					}).each(function(index){
						ReimPaymentOrder.readonlyVarianceAccount(index);
					});
		    	}
				
			});
			
			//auto set account name	when change account code
			$(document).on('input','#accountPayableCode',function(){
		    	var name = "";
		    	var code = $(this).val();
		        $("#listAccountMaster").find("option").each(function() {
		        	if($(this).attr('value') == code)
		        		$('#accountPayableName').val($(this).attr('name'));
		        });
		    });

			//on change bank in save panel
		    $(document).on('change','#bankName',function(){
		    	var bankCode = this.options[this.selectedIndex].getAttribute('bankcode');
		    	disableAjaxStatus();
		    	PaymentOrder.loadBankAccount(bankCode);
		    });
		    $(document).on('change','.payee-bank-name',function(){
		    	var bankCode = this.options[this.selectedIndex].getAttribute('bankcode');
		    	var bankAccSelector = $(this).attr('bankAcc');
		    	disableAjaxStatus();
		    	//load bank account by bank code	
		        $.ajax({
		            type : "GET",
		            url : CONTEXT_PATH + '/bankStatement/api/getBankAccountMasterByBankCode',
		            data: {
		            	bankCode: bankCode
		            },
		            success : function(ajaxResult) {
	            	   var resultData = JSON.parse(ajaxResult);
		               $("#" + bankAccSelector).html("");
		               if(resultData.STATUS == "SUCCESS") {
		            	   var htmlString = "";
		            	   if(resultData.RESULT.length > 0){
		            		   resultData.RESULT.forEach(function(item){
			            		   htmlString = htmlString + ("<option>"+ item.bankAccountNo +"</option>");
			            	   });
		            	   }
		            	   $("#" + bankAccSelector).html(htmlString);
		               }
		            }
		        });
		    });
		    
		    PaymentOrder.addCommasNumber();
		    PaymentOrder.addCurrencyLabel();
		    
		    $('#reset-btn').click(function(){
		    	if(!$(this).hasClass('disabled')){
		    		if(modeEdit || modeCopy){
				    	window.location.reload();
				    }else {
				    	//reset search conditions area
				    	document.getElementById("form1").reset();
				    	$('#searchForm_scheduledFrom').val('');
				    	$('#searchForm_scheduledTo').val('');
				    	$('#searchForm_monthFrom').val('');
				    	$('#searchForm_monthTo').val('');
				    	$('#searchForm_claimType').val('');
				    	$('#searchForm_isPaid').prop('checked', true);
				    	$('#searchForm_isApproval').prop('checked', true);
				    	$('#includeGstConvertedAmount').val('');
				    	$('#description').val('');
				    	$('#valueDate').val('');
				    	$('#bankRefNo').val('');
				    	$('#paymentApprovalNo').val('');
				    	$('#accountPayableName').val('');
						$('#accountPayableCode').parent().find('input[autocomplete="off"]').val('');
						$('#accountPayableCode').val('');
						$('#warning-message-currency').html('');
						$('#warning-message').html('');
						
				    	PaymentOrder.hideSearchDetailArea(true);
				    	$('.error-area').hide();
				    	//reload bank account
				    	var e = document.getElementById("bankName");
				    	var bankCode = e.options[e.selectedIndex].getAttribute("bankcode");
				    	PaymentOrder.loadBankAccount(bankCode);
				    }
		    	}
		    });
		    
		    //change invoice status
			$(document).on('click','.ap-dropdown-status li',function(){
				const PROCESSING = '002';
				const CANCELLED = '003';
				
				var statusCode = $(this).find('a').attr('status');
				$(this).closest('.ap-dropdown-status').find('button').attr('class','ap-status pay-' + statusCode + ' ap-status-header');
				$(this).closest('.ap-dropdown-status').find('.status-label').html($(this).find('a').html());
				
				// update dropdown
				if(statusCode == CANCELLED){
					$(this).find('a').attr('status', PROCESSING);
					$(this).find('a').html(PaymentOrder.PAYMENT_STATUSES[PROCESSING]);

				} else if(statusCode == PROCESSING){
					$(this).find('a').attr('status', CANCELLED);
					$(this).find('a').html(PaymentOrder.PAYMENT_STATUSES[CANCELLED]);
				}
				$('#statusCode').val(statusCode);
				
			});
		    
		}, //end eventListener
		
		setReadonlyInput: function(){
			$('#form1 table .default').attr('readonly', true);
		},
		
		enableInputInListDetail: function(rowId, isEnable){
			$("#payment-include-gst-original-amount-"+rowId).prop('readonly', isEnable);
			$("#payment-include-gst-converted-amount-"+rowId).prop('readonly', isEnable);
			$("#pay-rate-"+rowId).prop('readonly', isEnable);
			$("#variance-account-code-"+rowId).prop('readonly', isEnable);
			$("#variance-account-code-"+rowId).parents('.acontainer').find('input[autocomplete]').attr("readonly", isEnable);
			
			// calculate Payment Order Amount (original)
			if(!isEnable){
				var apAmountOriginal = getNewDecimal($('#ap-amount-original-' + rowId).autoNumeric('get'));
				var paidAmount = getNewDecimal($('#paid-include-gst-original-amount-' + rowId).autoNumeric('get'));
				$('#payment-include-gst-original-amount-' + rowId).autoNumeric('set', (apAmountOriginal.minus(paidAmount)).valueOf());
			}
		},
		
		calculateAmountConverted: function(rowId, event) {
			
			let targetId = event.target.id;
			
			var fxRate = getNewDecimal($('#fx-rate-' + rowId).val());
			var payOrginalAmount = getNewDecimal($('#payment-include-gst-original-amount-' + rowId).autoNumeric('get'));
			var payRate = getNewDecimal($('#pay-rate-' + rowId).autoNumeric('get'));
			var payConvertedAmount = getNewDecimal($('#payment-include-gst-converted-amount-' + rowId).autoNumeric('get'));
			
			if (targetId.indexOf('payment-include-gst-original-amount-') >= 0) {
				if(payOrginalAmount.toNumber() != 0 && payRate.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('pay-rate-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payConvertedAmount = payOrginalAmount.times(payRate);
				}
				
			} else if (targetId.indexOf('payment-include-gst-converted-amount-') >= 0) {
				if(payOrginalAmount.toNumber() != 0){
					payRate = payConvertedAmount.dividedBy(payOrginalAmount);
				}
			}
			
			$('#pay-rate-' + rowId).autoNumeric('set', payRate.toNumber());
			$('#payment-include-gst-converted-amount-' + rowId).autoNumeric('set', payConvertedAmount.toNumber());
			
			//set varianceAmount
			var varianceAmount = getNewDecimal(payOrginalAmount.mul(payRate).toFixed(2, 1)).minus(getNewDecimal(payOrginalAmount.mul(fxRate)).toFixed(2, 1));
			
			$('#variance-amount-' + rowId).autoNumeric('set', varianceAmount.toNumber());
		},
		
		calculateTotalAmount: function() {
			var total = getNewDecimal(0);
			
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-converted-amount-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-converted-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-converted-amount-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('#includeGstConvertedAmount').autoNumeric('set', total.valueOf());
		},
		
		calculateTotalOriginalAmount: function() {
			var total = getNewDecimal(0);
			
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-original-amount-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-original-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-amount-original-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('#includeGstOriginalAmount').autoNumeric('set', total.valueOf());
		},
		
		savePaymentOrder :  function() {
			var valid = false;
			var message = [];
			
			//add payment type
			PaymentOrder.addPaymentType();
			
			$('input').filter(function(){
				return this.id.match(/^ap-invoice-id-*/);
			}).each(function(){
				if($(this).is(':checked') == true) {
					valid = true;
				}
			});

			$('input').filter(function(){
				return this.id.match(/^payroll-id-*/);
			}).each(function(){
				if($(this).is(':checked') == true) {
					valid = true;
				}
			});

			$('input').filter(function(){
				return this.id.match(/^reim-id-*/);
			}).each(function(){
				if($(this).is(':checked') == true) {
					valid = true;
				}
			});
			
			if(valid === false){
				message.push(label('cashflow.paymentOrder.detail.detailRequired'));
			}

			// check value date is required
			var valueDate = $("#valueDate").val();
			if(!valueDate.trim() || valueDate.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.detail.valueDate')));
				valid = false;
			}
			
			//
			var accountPayableCode = $('#accountPayableCode').val();
			if(!accountPayableCode.trim() || accountPayableCode.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.detail.accountPayableCode')));
				valid = false;
			}
			
			// Account Payable Name
			var accountPayableName = $('#accountPayableName').val();
			if(!accountPayableName.trim() || accountPayableName.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.detail.accountPayableName')));
				valid = false;
			} 
			else if(!/^(.{0,100})$/.test(accountPayableName)){
				message.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.paymentOrder.detail.accountPayableName'], '100'));
				valid = false;
			}
			
			
			// bank ref no
			var bankRefNo = $('#bankRefNo').val();
			if(bankRefNo.trim().length != 0) {
				if(/^[a-zA-Z0-9- ]*$/.test(bankRefNo) == false) {
					message.push(label('cashflow.paymentOrder.detail.error.bankRefNo'));
				    valid = false;
				}
	 			if(!/^(.{0,100})$/.test(bankRefNo)){
	 				message.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.paymentOrder.detail.bankRefNo'], '100'));
					valid = false;
				}
			}
			
			// Payment Order Approval No.
			var paymentApprovalNo = $('#paymentApprovalNo').val();
	 		if(!/^(.{0,40})$/.test(paymentApprovalNo)){
	 			message.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.paymentOrder.detail.paymentOrderApprovalNo'], '40'));
				valid = false;
			}

			//
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-original-amount-*/);
			}).each(function(index, value) {
				var apInvoceNo = $('#ap-invoice-no-'+index).html();
				if($('#ap-invoice-id-'+index).is(':checked') == true) { // ap invoice checked
					
					// check payment include gst original amount is required
					var originalAmount = $('#'+this.id).val();
					if(originalAmount.trim().length == 0 || !originalAmount.trim()){
						var error = label('cashflow.paymentOrder.detail.originalAmountReq').format(apInvoceNo);
						message.push(error);
						valid = false;
					}
					
					// check payment order total amount less or equal ap invoice amount
					var paymentAmountInput = Number($('#'+this.id).autoNumeric('get'));
					var apAmount = Number($('#ap-amount-original-'+index).autoNumeric('get'));
					var paymentAmount = Number($('#sub-payment-include-gst-original-amount-'+index).val()); // amount can input
					$('#total-payment-include-gst-original-amount-'+index).autoNumeric('set', (apAmount - paymentAmount + paymentAmountInput));
					var totalPayment = $('#total-payment-include-gst-original-amount-'+index).autoNumeric('get');
					
					if(totalPayment > apAmount) {
						var totalPayAmount = $('#total-payment-include-gst-original-amount-'+index).val();
						var apInvoiceAmount = $('#ap-amount-original-'+index).html();
						
						//set message error
						var error = label('cashflow.paymentOrder.detail.totalNotBalance').format(totalPayAmount, apInvoceNo, apInvoiceAmount);
						message.push(error);
						valid = false;
					}
				}
				
			});
			
			// payment rate required and greater 0
			$('input').filter(function(){
				return this.id.match(/^pay-rate-*/);
			}).each(function(index, value){
				if($('#ap-invoice-id-'+index).is(':checked') == true) { // ap invoice checked
					var paymentRate = $('#pay-rate-'+index).val();
					if(paymentRate.length ==  0 || paymentRate == '0' ){
						var apInvoceNo = $('#ap-invoice-no-'+index).html();
						var error = label('cashflow.paymentOrder.detail.rateGreater').format(apInvoceNo);
						message.push(error);
						valid = false;
					}
				}
			});
			
			//
			$('input').filter(function(){
				return this.id.match(/^variance-account-code-*/);
			}).each(function(index, value){
				if($('#ap-invoice-id-'+index).is(':checked') == true) { // ap invoice checked
					if($('#variance-amount-'+index).autoNumeric('get') != 0) { // variance account is required when variance amount != 0
						var varianceAmountCode = $('#variance-account-code-'+index).val();
						if(varianceAmountCode.trim().length == 0 || !varianceAmountCode.trim()){
							var apInvoceNo = $('#ap-invoice-no-'+index).html();
							var error = label('cashflow.paymentOrder.detail.varianceReq').format(apInvoceNo);
							message.push(error);
							valid = false;
						}
					}
				}
			});
			
			// PAYROLL
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-original-*/);
			}).each(function(index, value) {
				var payrollNo = $('#payroll-no-'+index).html();
				if($('#payroll-id-'+index).is(':checked') == true) { // ap invoice checked
					
					// check payment include gst original amount is required
					var paymentOrderAmount = $('#'+this.id).val();
					if(paymentOrderAmount.trim().length == 0 || !paymentOrderAmount.trim()){
						var error = label('cashflow.paymentOrder.detail.payroll.originalAmountReq').format(payrollNo);
						message.push(error);
						valid = false;
					}
					
					// check payment order total amount less or equal payroll amount
					var paymentAmountInput = Number($('#'+this.id).autoNumeric('get'));
					var totalPayrollAmount = Number($('#total-net-payment-'+index).autoNumeric('get'));
					var paymentAmount = Number($('#sub-net-payment-amount-'+index).val());
					$('#total-net-payment-amount-original-'+index).autoNumeric('set', (totalPayrollAmount - paymentAmount + paymentAmountInput));
					var totalPayment = $('#total-net-payment-amount-original-'+index).autoNumeric('get');
					
					if(totalPayment > totalPayrollAmount) {
						var totalPayAmount = $('#total-net-payment-amount-original-'+index).val();
						var payrollAmount = $('#total-net-payment-'+index).html();
						
						//set message error
						var error = label('cashflow.paymentOrder.detail.payroll.totalNotBalance').format(totalPayAmount, payrollNo, payrollAmount);
						message.push(error);
						valid = false;
					}
				}
			});
			
			// payment rate required and greater 0
			$('input').filter(function(){
				return this.id.match(/^pr-pay-rate-*/);
			}).each(function(index, value){
				if($('#payroll-id-'+index).is(':checked') == true) { // payroll checked
					var paymentRate = $('#pr-pay-rate-'+index).val();
					if(paymentRate.length ==  0 || paymentRate == '0' ){
						var payrollNo = $('#payroll-no-'+index).html();
						var error = label('cashflow.paymentOrder.detail.payroll.rateGreater').format(payrollNo);
						message.push(error);
						valid = false;
					}
				}
			});
			
			// Reimbursement
			$('input').filter(function(){
				return this.id.match(/^payment-reim-amount-original-*/);
			}).each(function(index, value) {
				var reimNo = $('#reimbursement-no-'+index).html();
				if($('#reim-id-'+index).is(':checked') == true) { // ap invoice checked
					
					// check payment include gst original amount is required
					var paymentOrderAmount = $('#'+this.id).val();
					if(paymentOrderAmount.trim().length == 0 || !paymentOrderAmount.trim()){
						var error = label('cashflow.paymentOrder.detail.reim.originalAmountReq').format(reimNo);
						message.push(error);
						valid = false;
					}
					
					// check payment order total amount less or equal payroll amount
					var paymentAmountInput = Number($('#'+this.id).autoNumeric('get'));
					var totalReimAmount = Number($('#reim-original-amount-'+index).autoNumeric('get'));
					var paymentAmount = Number($('#sub-reim-payment-original-amount-'+index).val());
					$('#total-reim-payment-original-amount-'+index).autoNumeric('set', (totalReimAmount - paymentAmount + paymentAmountInput));
					var totalPayment = $('#total-reim-payment-original-amount-'+index).autoNumeric('get');
					
					if(totalPayment > totalReimAmount) {
						var totalPayAmount = $('#total-reim-payment-original-amount-'+index).val();
						var reimAmount = $('#reim-original-amount-'+index).html();
						
						//set message error
						var error = label('cashflow.paymentOrder.detail.reim.totalNotBalance').format(totalPayAmount, reimNo, reimAmount);
						message.push(error);
						valid = false;
					}
				}
			});
			
			// payment rate required and greater 0
			$('input').filter(function(){
				return this.id.match(/^reim-pay-rate-*/);
			}).each(function(index, value){
				if($('#reim-id-'+index).is(':checked') == true) { // payroll checked
					var paymentRate = $('#reim-pay-rate-'+index).val();
					if(paymentRate.length ==  0 || paymentRate == '0' ){
						var reimNo = $('#reimbursement-no-'+index).html();
						var error = label('cashflow.paymentOrder.detail.reim.rateGreater').format(reimNo);
						message.push(error);
						valid = false;
					}
				}
			});
			
			//
			$('input').filter(function(){
				return this.id.match(/^reim-variance-account-code-*/);
			}).each(function(index, value){
				if($('#reim-id-'+index).is(':checked') == true) { // reim checked
					if($('#reim-variance-amount-'+index).autoNumeric('get') != 0) { // variance account is required when variance amount != 0
						var varianceAmountCode = $('#reim-variance-account-code-'+index).val();
						if(varianceAmountCode.trim().length == 0 || !varianceAmountCode.trim()){
							var apInvoceNo = $('#reimbursement-no-'+index).html();
							var error = label('cashflow.paymentOrder.detail.reim.varianceReq').format(apInvoceNo);
							message.push(error);
							valid = false;
						}
					}
				}
			});
			
			if(valid) {
				$('#isConfirmChanged').val(false);
				PaymentOrder.reFormatNumber();
				$('#form1').submit();
			} else {
				$('.error-area').showAlertMessage(message)
			}
		},
		
		reFormatNumber : function(){
			/*Ap Invoice*/
			$('input').filter(function(){
				return this.id.match(/^paid-include-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^variance-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-include-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^pay-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			/*Payroll*/
			$('input').filter(function(){
				return this.id.match(/^paid-net-payment-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-original*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-net-payment-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});

			$('input').filter(function(){
				return this.id.match(/^pr-pay-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-converted-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('span').filter(function(){
				return this.id.match(/^total-net-payment-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^total-net-payment-amount-original-*/);
			}).each(function(index, value){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			/*Reimbursement*/
			$('span').filter(function(){
				return this.id.match(/^reim-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^total-reim-payment-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-paid-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-amount-original-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-pay-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-unpaid-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-variance-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('#includeGstConvertedAmount').val($('#includeGstConvertedAmount').autoNumeric('get'));
			$('#includeGstOriginalAmount').val($('#includeGstOriginalAmount').autoNumeric('get'));
			$('#originalCurrencyCode').val($('#ap-currency-0').html());
			$('#paymentRate').val($('#pay-rate-0').val());
			
		},
		
		//add currency label Amount Exclude GST 
		addCurrencyLabel : function(){
			$('#payment-table tbody tr td').filter(function(){
				return this.id.match(/^ap-currency-*/);
			}).each(function(key, value){
				$('#paid-unconverted-amount-currency-'+key).html($('#ap-currency-'+key).html());
				$('#payment-unconverted-amount-currency-'+key).html($('#ap-currency-'+key).html());
				$('#unpaid-unconverted-amount-currency-'+key).html($('#ap-currency-'+key).html());
			});
			
			$('#payment-pr-table tbody tr td').filter(function(){
				return this.id.match(/^payroll-currency-*/);
			}).each(function(key, value){
				$('#paid-net-payment-original-currency-'+key).html($('#payroll-currency-'+key).html());
				$('#net-payment-original-currency-'+key).html($('#payroll-currency-'+key).html());
				$('#unpaid-net-payment-original-currency-'+key).html($('#payroll-currency-'+key).html());
			});
			
			$('#payment-reim-table tbody tr td').filter(function(){
				return this.id.match(/^reim-currency-*/);
			}).each(function(key, value){
				$('#reim-paid-original-amount-currency-'+key).html($('#reim-currency-'+key).html());
				$('#payment-reim-original-currency-'+key).html($('#reim-currency-'+key).html());
				$('#reim-unpaid-original-amount-currency-'+key).html($('#reim-currency-'+key).html());
			});
		},
		
		addCommasNumber: function() {
			/*AP Invoice*/
			$('input').filter(function(){
				return this.id.match(/^paid-include-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^total-payment-include-gst-original-amount-*/);
			}).each(function(index, value){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-include-gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^variance-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-include-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^pay-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'S'});
			});
			
			$('span').filter(function(){
				return this.id.match(/^ap-amount-original-*/);
			}).each(function(){
				$(this).autoNumeric('init');
			});
			
			
			/*Payroll */
			$('input').filter(function(){
				return this.id.match(/^paid-net-payment-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-original*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^pr-pay-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'S'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^total-net-payment-amount-original-*/);
			}).each(function(index, value){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-net-payment-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('span').filter(function(){
				return this.id.match(/^total-net-payment-*/);
			}).each(function(){
				$(this).autoNumeric('init');
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-converted-*/);
			}).each(function(){
				$(this).autoNumeric('init');
			});
			
			/*Reimbursement*/
			$('span').filter(function(){
				return this.id.match(/^reim-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^total-reim-payment-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-paid-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0',mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-amount-original-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0',mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-pay-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'S'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^payment-reim-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-unpaid-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^reim-variance-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('#includeGstConvertedAmount').autoNumeric('init',  {mRound: 'D'});
			$('#includeGstOriginalAmount').autoNumeric('init',  {mRound: 'D'});
		},
		
		addAccountAutoComplete: function(idCounter){
			var elementId = 'variance-account-code-'+ idCounter;
		    var accountVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns : [
						label('cashflow.paymentOrder.detail.accountCode'),
						label('cashflow.paymentOrder.detail.parentCode'),
						label('cashflow.paymentOrder.detail.accountName'),
						label('cashflow.paymentOrder.detail.type') 
					],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountVal.all();
		        	var accountNameId = 'variance-account-code-'+ idCounter;
		        	accountVal.settext(selectedData[label('cashflow.paymentOrder.detail.accountCode')]);
		        	$('#' + accountNameId).val(selectedData[label('cashflow.paymentOrder.detail.accountCode')]);
		        }
			});
		    return accountVal;
		},
		loadBankAccount: function(bankCode){
			$.ajax({
	            type : "GET",
	            url : CONTEXT_PATH + '/bankStatement/api/getBankAccountMasterByBankCode',
	            data: {
	            	bankCode: bankCode
	            },
	            success : function(ajaxResult) {
            	   var resultData = JSON.parse(ajaxResult);
	               $("#bankAccs").html("");
	               if(resultData.STATUS == "SUCCESS") {
	            	   var htmlString = "";
	            	   if(resultData.RESULT.length > 0){
	            		   resultData.RESULT.forEach(function(item){
		            		   htmlString = htmlString + ("<option>"+ item.bankAccountNo +"</option>");
		            	   });
	            	   }
	            	   $("#bankAccount").html(htmlString);
	               }
	            }
	        });
		},
		
		readonlyVarianceAccount : function (rowId){
			var fxRate = Number($('#fx-rate-' + rowId).val());
	    	var payRate = Number($('#pay-rate-' + rowId).autoNumeric('get'));
	    	if(fxRate == payRate) {
	    		$('#variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').attr('readonly',true);
	    		$('#variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').val('');
	    		$('#variance-account-code-' +rowId).val('');
	    	} else {
	    		$('#variance-account-code-' +rowId).parent().find('input[autocomplete="off"]').attr('readonly',false);
	    	}
		},
		
		addAccountPayable: function() {
			let accountCode = "";
			let accountName = "";
			$.each($('.account-master-code'), function(index, item) {
				if($(this).parent().find("input[type='checkbox']").is(":checked")){
					if(accountCode != ""){
						if(accountCode == $(this).attr('value')){
							$("#save-btn").removeClass('disabled');
							$('#warning-message').html('');
						} else {
							$("#save-btn").addClass('disabled');
							$('#warning-message').html(label('cashflow.paymentOrder.detail.accountPayableDif'));
						}
						
					} else {
						$("#save-btn").removeClass('disabled');
						$('#warning-message').html('');
						accountCode = $(this).attr('value');
						accountName = $(this).parent().find('.account-master-name').val();
					}
				} 
			});			
			
			$('#accountPayableName').val(accountName);
			$('#accountPayableCode').parent().find('input[autocomplete="off"]').val(accountCode);
			$('#accountPayableCode').val(accountCode);
		},
		
		checkOriginalCurrency(){
			let originalCurrency = "";
			$.each($('.original-currency'), function(index, item) {
				if($(this).parent().find("input[type='checkbox']").is(":checked")){
					if(originalCurrency != ""){
						if(originalCurrency == $(this).html()){
							$('#warning-message-currency').html('');
						} else {
							$("#save-btn").addClass('disabled');
							$('#warning-message-currency').html(label('cashflow.paymentOrder.detail.CurrencyDif'));
						}
					} else {
						$('#warning-message-currency').html('');
						originalCurrency = $(this).html();
					}
				}
			});
		},
		
		addPaymentType : function(){
			let paymentType = "";
			if($("#payment-table .detail-id-cb:checked").length > 0){
				paymentType += "0,";
			} 
			if ($("#payment-pr-table .detail-id-cb:checked").length > 0){
				paymentType += "1,";
			} 
			if ($("#payment-reim-table .detail-id-cb:checked").length > 0){
				paymentType += "2,";
			}
			
			$('#paymentType').val(paymentType.slice(0, -1));
		},
		
		hideSearchDetailArea : function(resetMode){
			$("#payment-apinvoice").hide();
			$("#payment-payroll").hide();
			$("#payment-reimbursement").hide();
			$('#result-nodata').hide();
			let showResult = false;
			
			if(!resetMode){
				if($("#payment-table .detail-id-cb").length > 0){
					$("#payment-apinvoice").show();
					showResult = true;
				}
				if($("#payment-pr-table .detail-id-cb").length > 0){
					$("#payment-payroll").show();
					showResult = true;
				}
				if($("#payment-reim-table .detail-id-cb").length > 0){
					$("#payment-reimbursement").show();
					showResult = true;
				}
			}
		    
		    if(!showResult){
		    	$('#result-nodata').show();
		    }
		}
		
	};
}());