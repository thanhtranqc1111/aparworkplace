/**
 * @author ThoaiNH 
 * create Nov 02, 2017
 */
(function () {
	header = {
		init: function () {
			header.updateFontJP();
		},
		eventListener: function () {
			$(document).on('click','#btn-change-tenancy',function(){
				$('#ap-tanentcy-popup').modal('toggle');
				$('.modal-backdrop').appendTo('#header');

				//remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
		          $('body').removeClass("modal-open")
		          $('body').css("padding-right","");
		          
				/*$.ajax({
		            type : "GET",
		            url : CONTEXT_PATH + '/loadSubsidiaries',
		            success : function(ajaxResult) {
		               var result = JSON.parse(ajaxResult);
		               if(result.STATUS == "SUCCESS") {
		            	   console.log(result.DATA);
		               }
		            }
		        });*/
			});
			
			$(document).on('click','.btn-apply-subsidiary',function(){
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/changeSubsidiary',
		            data: {
	    				id: $('input[name="optionSubsidiaries"]:checked').val()
		            },
		            success : function(ajaxResult) {
		               var result = JSON.parse(ajaxResult);
		               if(result.STATUS == "SUCCESS") {
		            	   window.location="/dashboard/"
		               }
		            }
		        });
			});
			
			header.checkChangeContent();
		},
		
		//check change contents value
		checkChangeContent : function(){
			$('input[type=text], input[type=checkbox], select, textarea').bind("change paste keyup", function() {
				$('#isChangedContent').val(true);
			});
		},
		
		//update font jp
		updateFontJP: function(){
			if($('.nav_menu .navbar-right li .lang-local span').hasClass('jp') == true) {
				$('#header').parent().addClass('wf-notosansjapanese');
			}
		},
	};
}());

function changeLocal(local) {
	let url = window.location.href;
	if (url.indexOf('?') != -1) {
		if(url.indexOf('lang=') != -1){
			if (url.indexOf('lang=en') != -1) {
				url = url.replace("lang=en", "lang=" + local);
			} else {
				url = url.replace("lang=jp", "lang=" + local);
			}
		} else {
			url = url + '&lang=' + local;
		}
	} else {
		url = url + '?lang=' + local;
	}
	window.location.href = url;
}

function showAjaxStatus(){
	NProgress.start();
	$('.ap-indicator').show();
};

function hideAjaxStatus(){
	NProgress.done();
	$('.ap-indicator').hide();
};

window.onbeforeunload = function(){
	var isConfirm = $('#isConfirmChanged').val();
	if(isConfirm === 'true' && isConfirm !== 'undefined'){
		var isChange = $('#isChangedContent').val();
		if(isChange === 'true') {
			return "You may have unsaved Data";
		}
	}
}