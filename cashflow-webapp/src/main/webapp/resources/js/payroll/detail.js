/**
 * @author ThoaiNH 
 * create Mar 15, 2018
 */
(function () {
	payRollDetail = {
			id:0,
			totalPayrollDetails: 0,
			crNewId: 0,
			dataListPayrollDetails: [],
			totalLaborCostOriginal: 0,
			totalDeductionOriginal: 0,
			totalNetPaymentOriginal: 0,
			totalNetPaymentConverted: 0,
			PAYROLL_STATUSES: {},
			initData: function(data){
				if(localStorage.getItem("payrollShowConfirm") && localStorage.getItem("payrollShowConfirm") == $("#txt-payrollNo").val()){
					showPopupSave($('#txt-payrollNo').val() + " " + GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'].toLowerCase() + ".", function() {
	    				$('#isChangedContent').val('false');
	 		 		   	window.location = "/payroll/list";
	 				});
					localStorage.removeItem("payrollShowConfirm");
				}
				if(data){
					payRollDetail.id = data.id;
					payRollDetail.totalLaborCostOriginal = data.totalLaborCostOriginal;
					payRollDetail.totalDeductionOriginal = data.totalDeductionOriginal;
					payRollDetail.totalNetPaymentOriginal = data.totalNetPaymentOriginal;
					payRollDetail.totalNetPaymentConverted = data.totalNetPaymentConverted;
					payRollDetail.dataListPayrollDetails = data.payrollDetailses;
					payRollDetail.totalPayrollDetails = payRollDetail.dataListPayrollDetails.length;
					//show leave confirm POPUP when add new
					if(data.id == 0)
					{
						//setup for copy case
						$('#payroll-table-data tr').each(function(){
							payRollDetailDataTable.setUpTableRow($(this));
						});
						//setup new data id when copy because all id will be = 0 when copy
						for(i = 0; i < payRollDetail.dataListPayrollDetails.length; i++){
							payRollDetail.dataListPayrollDetails[i].id = "new-" + i;
						}
						payRollDetail.crNewId = payRollDetail.dataListPayrollDetails.length;
						payRollDetail.renderCopyButton();
					}
					else {
						//disable delete when edit
						$('#payroll-table-data .btn-delete-payment-detail-row').hide();
					}
				}
			},
			init: function(){
				$('#form-payroll-header').on('keyup change', 'input, select, textarea', function(){
					$('#isChangedContent').val('true');
				});
				
				$('input.ap-big-decimal').setupInputNumberMask({allowMinus:false});
				$('input.ap-big-decimal-allow-minus').setupInputNumberMask({allowMinus:true});
				$('#txt-fx-rate').inputmask("decimal", {
					alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false, allowMinus: false
				});
				$('.ap-date-picker').datepicker({
			    	format: 'dd/mm/yyyy',
			        orientation: 'auto bottom',
			        todayHighlight: true
			    });
				//$('#txt-booking-date').datepicker('setDate', new Date()); 
				$('.ap-date-picker-month-format').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				 
				var accountTautocomplete = $('#txt-account-payable-code').tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [label('cashflow.payroll.detail.accountCode'), label('cashflow.payroll.detail.parentCode'), label('cashflow.payroll.detail.accountName'), label('cashflow.payroll.detail.type')],
					hide: [true, false, true, true],
					dataIndex: 0,
					addNewDataLink: '/admin/accountMaster/list',
			        data: function(){
		        		var x = { keyword: accountTautocomplete.searchdata()}; 
		        		return x;
		        	},
			        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountTautocomplete.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = accountTautocomplete.all();
			        	$('#txt-account-payable-code').val(selectedData[label('cashflow.payroll.detail.accountCode')]);
			        	$('#txt-account-payable-name').val(selectedData[label('cashflow.payroll.detail.accountName')]);
			        }
				});
				accountTautocomplete.settext($('#txt-account-payable-code').val());
				if(payRollDetail.id > 0){
					$('input[autocomplete]').attr('readonly','');
				}
				
				/*$('#payroll-table-data').floatThead({
				    scrollContainer: function($table){
				        return $table.closest('.table-scroll');
				    }
				});*/
			},
			eventListener: function(){
				$(document).on('click','#btn-add-row',function(){
					var data = payRollDetail.createPayrollDetailsJSON();
					payRollDetailDataTable.addNewPayrollDetailRow(data);
				});
				
				$(document).on('click','#btn-save',function(){
					payRollDetail.savePayroll();
				});
				
				$(document).on('click','#edit-btn',function(){
					$(this).attr('disabled','');
					//only allow to update payroll detail when status is not paid
					if($('#status').val() == '002'){
						$('.ap-label-status').hide();
						$('.ap-dropdown-status').show();
						$('#payroll-table-data .btn-delete-payment-detail-row').show();
						$('.panel-bottom-buttons button').removeAttr('disabled');
						$('#select-currency-type').removeAttr('disabled');
						$('input:not(.alway-readonly), select:not(.alway-readonly), textarea:not(.alway-readonly)').attr('readonly',false);
						payRollDetail.renderCopyButton();
						$('#payroll-table-data tr').each(function(){
							payRollDetailDataTable.setUpTableRow($(this));
						});
					}
					//partial paid status will be allowed to edit payroll header
					else {
						$('.panel-bottom-buttons .pull-right button').removeAttr('disabled');
						$('#form-payroll-header #txt-month, #form-payroll-header #txt-booking-date,' + 
						  '#form-payroll-header #txt-invoiceNo, ' + 
						  '#form-payroll-header #select-claim-type, #form-payroll-header #txt-description').attr('readonly',false);
						$('#form-payroll-header #txt-account-payable-code').next().next().attr('readonly',false);
					}
				});
				
				// change invoice status
				$(document).on('click','.ap-dropdown-status li',function(){
					const NOTPAID = '002';
					const CANCELLED = '004';
					
					var statusCode = $(this).find('a').attr('status');
					$(this).closest('.ap-dropdown-status').find('button').attr('class','ap-status api-' + statusCode + ' ap-status-header');
					$(this).closest('.ap-dropdown-status').find('.status-label').html($(this).find('a').html());
					
					// update dropdown
					if(statusCode == CANCELLED){
						$(this).find('a').attr('status', NOTPAID);
						$(this).find('a').html(payRollDetail.PAYROLL_STATUSES[NOTPAID]);

					} else if(statusCode == NOTPAID){
						$(this).find('a').attr('status', CANCELLED);
						$(this).find('a').html(payRollDetail.PAYROLL_STATUSES[CANCELLED]);
					}
					
					$('#status').val(statusCode);
				});
				
				// reset btn
				$('#btn-reset-all').click(function(){
					if(!$(this).hasClass('disabled')){
						if(payRollDetail.id > 0 || window.location.href.indexOf('payroll/copy') > 0){
							window.location.reload();
						}
						else {
							document.getElementById("form-payroll-header").reset();
							$('#payroll-table-data tbody').html('');
							payRollDetail.dataListPayrollDetails = [];
							payRollDetail.totalDeduction = 0;
							payRollDetail.totalLaborCost = 0;
							payRollDetail.totalNetPayment = 0;
							payRollDetail.totalPayrollDetails = 0;
							$('#alertApprovalStatus').html('');
						}
					}
				});
				
				$('#btn-cancel').click(function(e){
					e.preventDefault();
					window.location = "/payroll/list";
				});
				
				$(document).on('click','#btn-copy',function(){
					if(!$(this).hasClass('disabled')){
						payRollDetail.copyPayrollDetails();
					}
				});
				
				$(document).on('click','#btn-load-all-emp',function(){
					if(!$(this).hasClass('disabled')){
						if(payRollDetail.totalPayrollDetails > 0){
							showPopupMessage2(GLOBAL_MESSAGES['cashflow.payroll.detail.mess2'], function(){
								payRollDetail.loadAllEmp()
							});
						}
						else {
							payRollDetail.loadAllEmp();
						}
					}
				});
				
				$(document).on('click','#btn-clone',function(){
					if(!$(this).hasClass('disabled')){
						 window.location.href = "/payroll/copy/" + $('#txt-payrollNo').val();
					}
				});
				
				$(document).on('change','#txt-fx-rate',function(){
					payRollDetail.updateDataByFxRate();
				});
				
				$(document).on('change','#select-currency-type',function(){
					$(".original-currency-code").html($(this).val());
				});
				
			},
			getPayrollDetailItemById: function(id){
				var data = null;
				payRollDetail.dataListPayrollDetails.forEach(function(item){
					if(item.id == id)
					{
						data = item;
						return;
					}
				});
				return data;
			},
			updateSummaryValueUI: function(){
				$('#txt-total-labor-cost').val(payRollDetail.totalLaborCostOriginal);
				$('#txt-deduction').val(payRollDetail.totalDeductionOriginal);
				$('#txt-net-payment').val(payRollDetail.totalNetPaymentOriginal);
				$('#txt-net-payment-converted').val(payRollDetail.totalNetPaymentConverted);
			},
			savePayroll: function(){
				if(GLOBAL_PERMISSION["PMS-003"]){
					var dataList = jQuery.extend(true, [], payRollDetail.dataListPayrollDetails);
					for(var j = 0; j < dataList.length; j++)
					{
						if(typeof dataList[j].id == 'string'){
							dataList[j].id = 0;
						}
					}
					var data = {
						id: payRollDetail.id,
						payrollNo: $("#txt-payrollNo").val(),
						month: $('#txt-month').val(),
						bookingDate: $('#txt-booking-date').val(),
						claimType: $('#select-claim-type').val(),
						accountPayableCode: $('#txt-account-payable-code').val(),
						accountPayableName: $('#txt-account-payable-name').val(),
						paymentScheduleDate: $('#txt-scheduled-payment-date').val(),
						totalLaborCostOriginal: $('#txt-total-labor-cost').val(),
						totalDeductionOriginal: $('#txt-deduction').val(),
						totalNetPaymentOriginal: $('#txt-net-payment').val(),
						totalNetPaymentConverted: $('#txt-net-payment-converted').val(),
						description: $('#txt-description').val(),
						status: $('#status').val(),
						fxRate: $('#txt-fx-rate').val(),
						originalCurrencyCode: $('#select-currency-type').val(),
						invoiceNo: $('#txt-invoiceNo').val(),
						payrollDetailses: dataList
					};
					var arrMess = payRollDetail.validateDataBeforeSave(data);
					if(arrMess.length == 0)
					{
						var formData = new FormData();
						formData.append('data', JSON.stringify(data));
						$.ajax({
				            type : "POST",
				            url : CONTEXT_PATH + '/payroll/save',
				            cache : false,
							contentType : false,
							processData : false,
							data : formData,
				            success : function(ajaxResult) {
				            	 var resultData = JSON.parse(ajaxResult);
				            	 console.log(resultData);
				            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0) {
				            		 $(".alert-warning").hide();
				            		 $('#isChangedContent').val('false');
			         		 		 window.location = "/payroll/detail/" +  $("#txt-payrollNo").val();
			         		 		 localStorage.setItem("payrollShowConfirm", $("#txt-payrollNo").val());
					             }
				            	 else {
				            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
				            	 }
				            }
				        });
					}
					else 
					{
						payRollDetail.showWarning(arrMess);
					}
				}
			},
			createPayrollDetailsJSON: function(){
				/*
				 * insert data list JSON
				 */
				var data = {
					id: "new-" + payRollDetail.crNewId++,
					employeeCode: "",
					roleTitle: "",
					division: "",
					team: "",
					laborBaseSalaryPayment: 0,
					laborAdditionalPayment1: 0,
					laborAdditionalPayment2: 0,
					laborAdditionalPayment3: 0,
					laborSocialInsuranceEmployer1: 0,
					laborSocialInsuranceEmployer2: 0,
					laborSocialInsuranceEmployer3: 0,
					laborOtherPayment1: 0,
					laborOtherPayment2: 0,
					laborOtherPayment3: 0,
					totalLaborCostOriginal: 0,
					deductionSocialInsuranceEmployer1: 0,
					deductionSocialInsuranceEmployer2: 0,
					deductionSocialInsuranceEmployer3: 0,
					deductionSocialInsuranceEmployee1: 0,
					deductionSocialInsuranceEmployee2: 0,
					deductionSocialInsuranceEmployee3: 0,
					deductionWht: 0,
					deductionOther1: 0,
					deductionOther2: 0,
					deductionOther3: 0,
					totalDeductionOriginal: 0,
					netPaymentOriginal: 0,
					netPaymentConverted: 0,
					approvalCode: "",
					name: ""
				};
				payRollDetail.dataListPayrollDetails.push(data);
				return data;
			},
			renderCopyButton: function(){
				var showingList = payRollDetail.dataListPayrollDetails.filter(function(item){
    				return item['removed'] != true;
    			});
				if(showingList.length > 0)
				{
					$('#btn-copy').removeClass('disabled');
				}
				else {
					$('#btn-copy').addClass('disabled');
				}
			},
			copyPayrollDetails: function(){
				var lastItem = null;
				for(i = 0; i < payRollDetail.dataListPayrollDetails.length; i++){
					var item = payRollDetail.dataListPayrollDetails[i];
					if(item['removed'] != true)
					{
						lastItem = item;
					}
				}
				var newItem = jQuery.extend(true, {}, lastItem);
				if(newItem != null)
				{
					newItem.id = "new-" + payRollDetail.crNewId ++;
					payRollDetail.dataListPayrollDetails.push(newItem);
					payRollDetailDataTable.addNewPayrollDetailRow(newItem);
					payRollDetail.totalLaborCostOriginal = getNewDecimal(payRollDetail.totalLaborCostOriginal).plus(newItem.totalLaborCostOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
					payRollDetail.totalDeductionOriginal = getNewDecimal(payRollDetail.totalDeductionOriginal).plus(newItem.totalDeductionOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
					payRollDetail.totalNetPaymentOriginal = getNewDecimal(payRollDetail.totalNetPaymentOriginal).plus(newItem.netPaymentOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
					payRollDetail.totalNetPaymentConverted = getNewDecimal(payRollDetail.totalNetPaymentOriginal).mul(getNewDecimal($('#txt-fx-rate').val())).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
					payRollDetail.updateSummaryValueUI();
					payRollDetailDataTable.showApprovalInfo();
				}
			},
			loadAllEmp: function(){
				$.ajax({
		            type : "GET",
		            url : CONTEXT_PATH + '/manage/getEmployeeMasterForPayroll',
		            data: {
		            	keyword: "",
		            	paymentScheduleDate: $('#txt-scheduled-payment-date').val()
		            	
		            },
		            success : function(ajaxResult) {
		            	console.log(ajaxResult);
		            	//remove all detail data
		            	if(payRollDetail.totalPayrollDetails > 0){
			            	$('#payroll-table-data tbody tr').remove();
							payRollDetail.totalPayrollDetails = 0;
							payRollDetail.totalLaborCostOriginal = '0';
							payRollDetail.totalDeductionOriginal = '0';
							payRollDetail.totalNetPaymentOriginal = '0';
							payRollDetail.totalNetPaymentConverted = '0';
							payRollDetail.updateSummaryValueUI();
							for(var i = payRollDetail.dataListPayrollDetails.length - 1; i >= 0; i--)
							{
								var item = payRollDetail.dataListPayrollDetails[i];
								item['removed'] = true;
								if(typeof item.id == 'string' && item.id.indexOf('new-') == 0)
								{
									payRollDetail.dataListPayrollDetails.splice(i, 1);
								}
							}
		            	}
		            	ajaxResult.forEach(function(item){
		            		var added = false;
		            		for(i = 0; i < payRollDetail.dataListPayrollDetails.length; i++){
		            			if(payRollDetail.dataListPayrollDetails[i].employeeCode == item.code && payRollDetail.dataListPayrollDetails[i]['removed'] != true)
		            			{
		            				added = true;
		            				break;
		            			}
		            		}
		            		if(added == false){
		            			var data = payRollDetail.createPayrollDetailsJSON();
			            	    data.employeeCode = item.code;
			            	    data.roleTitle = item.roleTitle;
			            	    data.division = item.divisionName;
			            	    data.team = item.team;
			            	    data.name = item.name;
								payRollDetailDataTable.addNewPayrollDetailRow(data);
		            		}
		            	});
		            	
		            }
		        });
			},
			showWarning:function(message){
				$('.alert-warning').showAlertMessage(message);
				$('#alert-warning-payroll').goTo();
				
			},
			hideWarning: function(){
				$('.alert-warning').hide();
			},
			validateDataBeforeSave: function(data){
				var arrMess = [];
				var showingList = data.payrollDetailses.filter(function(item){
    				return item['removed'] != true;
    			});
				if(data.month.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.month']));
				}
				if(data.bookingDate.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.bookingdate']));
				}
				if(data.accountPayableCode.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.accountPayableCode']));
				}
				if(data.accountPayableName.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.accountPayableName']));	
				}
				if(showingList.length == 0)
				{
					arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.payrollDetails']));	
				}
				if(/^(.{0,40})$/.test($('#txt-invoiceNo').val()) == false) {
					arrMess.push(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.payroll.detail.invoiceNo'], '40'));
	            } 
				else {
					for(i = 0; i < showingList.length; i++){
						var item = showingList[i];
						if(!item.employeeCode || item.employeeCode.trim().length == 0)
						{
							arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.payroll.detail.payrollDetails'] + " : " + GLOBAL_MESSAGES['cashflow.payroll.detail.employeeID']));
							break;
						}
					}
				}
				return arrMess;
			},
			updateDataByFxRate: function(){
				var fxRate = $('#txt-fx-rate').val();
				$('.txt-re-fx-rate').val(fxRate);
				for(i = 0; i < payRollDetail.dataListPayrollDetails.length; i++){
					var data = payRollDetail.dataListPayrollDetails[i];
					data.netPaymentConverted = getNewDecimal(data.netPaymentOriginal).mul(getNewDecimal(fxRate)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
					$('#payroll-table-data').find('tr[dataId="' + data.id +'"]').find('.txt-net-payment-converted').val(data.netPaymentConverted);
					data['edited'] = true;
				}
				payRollDetail.totalNetPaymentConverted = getNewDecimal(payRollDetail.totalNetPaymentOriginal).mul(getNewDecimal(fxRate)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				$('#txt-net-payment-converted').val(payRollDetail.totalNetPaymentConverted);
			}
	};
}());