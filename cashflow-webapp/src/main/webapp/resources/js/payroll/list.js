/**
 * @author ThoaiNH 
 * create Mar 15, 2018
 */
(function () {
	payRollList = {
			dataTable: null,
			init: function(){
				$('.datepicker input').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				payRollList.dataTable = $('#payroll-data-table').DataTable({
			        "ajax": {
			            url: "/payroll/api/getAllPagingPayroll",
			            type: "GET",
			            data: function ( d ) {
			            	d.fromMonth = $('#txt-fromMonth').val();
			                d.toMonth = $('#txt-toMonth').val();
			                d.status = $('#select-status').val();
			                d.fromPayrollNo = $('#txt-fromApNo').val();
			                d.toPayrollNo = $('#txt-toApNo').val();
			            },
			            error: function() {
			                $("#payroll-data-table").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
			            },
			            complete: function(data){
							hideAjaxStatus();
							payRollList.mergeRows('#payroll-data-table', 1, 6);
				        	payRollList.mergeRows('#payroll-data-table', 7, 14);
			            }
			        },
			        columns: [
			        	{
				            "data" : "payrollNo",
				            "width" : "8%",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	if(GLOBAL_PERMISSION["PMS-009"])
				            		return "<a href='/payroll/detail/" + data +"' >" + data + "</a>";
				            	else
				            		return data;
				             }
				        },
				        {
				            "data" : "month",
				            "orderable": true,
				            "width" : "6%",
				            "render": function(data, type,full, meta) {
				            	return (full.month.month + 1) + "/" + full.month.year;
				             }
				        },
				        {
				            "data" : "claimTypeVal",
				            "orderable": false,
				            "width" : "7%",
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "invoiceNo",
				            "orderable": false,
				            "width" : "5%",
				        },
				        {
				            "data" : "totalNetPayment",
				            "orderable": false,
				            "width" : "8%",
				            "render": function(data, type,full, meta) {
				            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
		            				   + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				             }
				        },
				        {
				            "data" : "payrollStatus",
				            "width" : "7%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(full.payrollStatus != undefined){
				            		return "<div class='ap-status  api-" + full.payrollStatusCode + "'>" + full.payrollStatus + "</div>";
				            	}
				             }
				        },
				        {
				            "data" : "paymentOrderNo",
				            "width" : "7%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
				            		return "<a href='/payment/detail/" + data +"' >" + data + "</a>";
				            	else
				            		return "";
				             }
				        },
				        {
				            "data" : "valueDate",
				            "orderable": false,
				            "width" : "6%",
				            "render": function(data, type,full, meta) {
				            	if(full.valueDate)
				            		return full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year;
				            	else
				            		return "";
				             }	
				        },
				        {
				            "data" : "bankName",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankAccount",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankStatementIds",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            	{
				            		var trans = "";
					            	var length = data.length;
					            	$.each(data, function(i, obj) {
					            		trans += "<a href='/bankStatement/list/" + obj +"' target='_blank'>" + obj + "</a>";
					            		if ( i != length - 1){
					            			trans += ", ";
					            		}
					            	});
					            	return trans;
				            	}
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "includeGstConvertedAmount",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
					            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
		            				        + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "bankRefNo",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data != undefined && data != null)
				            		return data;
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "paymentOrderStatus",
				            "width" : "8%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var paymentStatus = "";
				            	if(full.paymentOrderStatusCode != undefined){
				            		paymentStatus = "<div class='ap-status  pay-" + full.paymentOrderStatusCode + "'>" + full.paymentOrderStatus + "</div>";
				            	}
				            	return paymentStatus;
				             }
				        }
			        ],
			        searching: false,
			        "order": [[ 0, "desc" ]],
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 'All']],
					"lengthChange" : true,
			        "columnDefs": [
			        	{
			                "targets": [0, 4, 6, 13],
			                "className": "text-center",
			            },
			            {
			                "targets": [1, 3, 7, 11],
			                "className": "text-right",
			            }
			        ],
				 });
			},
			eventListener: function(){
				$(document).on('click','#btn-search',function(){
					payRollList.dataTable.ajax.reload();
				});
				
				$('#export').click(function(){
					$('#reset').click();
					var href = $(this).attr("href");
					var param = "";
					
					var fromMonth = $('#txt-fromMonth').val();
					var toMonth = $('#txt-toMonth').val();
					var status = $('#select-status').val();
					var fromPayrollNo = $('#txt-fromApNo').val();
					var toPayrollNo = $('#txt-toApNo').val();
					
					if (fromMonth.length > 0) {
						param += "&fromMonth=" + fromMonth;
					}
					if (toMonth.length > 0) {
						param += "&toMonth=" + toMonth;
					}
					if (status.length) {
						param += "&status=" + status;
					}
					if (fromPayrollNo.length) {
						param += "&fromPayrollNo=" + fromPayrollNo;
					}
					if (toPayrollNo.length) {
						param += "&toPayrollNo=" + toPayrollNo;
					}
					
					if (param.length > 0) {
						href = href + "?" + param.slice(1);
					}
					
					$("#export").attr("href", href);
				});
				
				$(document).on('click','#btn-reset',function(){
					$('.ap-search input').val('');	
					$('#select-status').val('');
				});
			},
			mergeRows: function (table, mergeFrom, mergeTo) {
				var rowsList = $(''+table+ ' > tbody').find('tr');
				var previous = null, cellToExtend = null, rowspan = 1;
				
				rowsList.each(function(index, e) {
					var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
					if (previous == content && content !== "" ) {
						rowspan = rowspan + 1;
						
						for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
							if(i >= mergeFrom && i <= mergeTo) {
								$(this).find('td:nth-child(' + i + ')').addClass('hidden');
								padding = rowspan * 16;
								cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
								cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
							} 
						}
					} else {
			             rowspan = 1;
			             previous = content;
			             cellToExtend = elementTab;
					}
				});
				$(table + ' td.hidden').remove();
			}
	};
}());