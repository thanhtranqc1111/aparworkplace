/**
 * @author ThoaiNH 
 * create Mar 15, 2018
 */
(function () {
	payRollDetailDataTable = {
			init: function(){
			},
			eventListener: function(){
				$(document).on('click','#payroll-table-data .btn-delete-payment-detail-row',function(){
					$('#popConfirm').modal('toggle');
		        	$('#popConfirm button[name="btnConfirm"]').off("click");
		        	var id = $(this).parents('tr').attr('dataId');
		        	$('#popConfirm button[name="btnConfirm"]').click(function() {
		        		payRollDetailDataTable.deletePayrollDetailRow(id);
			        });
				});
				
				$(document).on('change', '#payroll-table-data .ap-big-decimal', function() {
					var updatedDataItem = payRollDetailDataTable.caculateValue($(this).closest('tr'));
					$(this).closest('tr').find('.txt-la-total-cost-labor').val(updatedDataItem.totalLaborCostOriginal);
					$(this).closest('tr').find('.txt-deduction-total').val(updatedDataItem.totalDeductionOriginal);
					$(this).closest('tr').find('.txt-net-payment').val(updatedDataItem.netPaymentOriginal);
					$(this).closest('tr').find('.txt-net-payment-converted').val(updatedDataItem.netPaymentConverted);
					payRollDetail.updateSummaryValueUI();
					payRollDetailDataTable.showApprovalInfo();
			    });
			},
			addNewPayrollDetailRow: function(dataItem){
				/*
				 * append row to data table
				 */
				var tablePayroll = $('#payroll-table-data');
				var tableRef = document.getElementById(tablePayroll.attr('id')).getElementsByTagName('tbody')[0];
				var newRow   = tableRef.insertRow(payRollDetail.totalPayrollDetails++);
				newRow.setAttribute("dataId", dataItem.id);
				newRow.insertCell(0).appendChild(payRollDetailDataTable.createInputCell('form-control txt-employee-id', null, dataItem.employeeCode));
				newRow.insertCell(1).appendChild(payRollDetailDataTable.createInputCell('form-control','readonly', dataItem.roleTitle));
				newRow.insertCell(2).appendChild(payRollDetailDataTable.createInputCell('form-control','readonly', dataItem.division));
				newRow.insertCell(3).appendChild(payRollDetailDataTable.createInputCell('form-control','readonly', dataItem.team));
				newRow.insertCell(4).appendChild(payRollDetailDataTable.createInputCell('form-control','readonly', dataItem.name));
				//labor
				newRow.insertCell(5).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-base-salary-payment', null, dataItem.laborBaseSalaryPayment));
				newRow.insertCell(6).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-additional-payment-1', null, dataItem.laborAdditionalPayment1));
				newRow.insertCell(7).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-additional-payment-2', null, dataItem.laborAdditionalPayment2));
				newRow.insertCell(8).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-additional-payment-3', null, dataItem.laborAdditionalPayment3));
				newRow.insertCell(9).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-social-insurance-on-employer-1', null, dataItem.laborSocialInsuranceEmployer1));
				newRow.insertCell(10).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-social-insurance-on-employer-2', null, dataItem.laborSocialInsuranceEmployer2));
				newRow.insertCell(11).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-social-insurance-on-employer-3', null, dataItem.laborSocialInsuranceEmployer3));
				newRow.insertCell(12).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-other-payment-1', null, dataItem.laborOtherPayment1));
				newRow.insertCell(13).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-other-payment-2', null, dataItem.laborOtherPayment2));
				newRow.insertCell(14).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-other-payment-3', null, dataItem.laborOtherPayment3));
				newRow.insertCell(15).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-la-total-cost-labor', 'readonly', dataItem.totalLaborCostOriginal));
				//deduction
				newRow.insertCell(16).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employer-1', null, dataItem.deductionSocialInsuranceEmployer1));
				newRow.insertCell(17).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employer-2', null, dataItem.deductionSocialInsuranceEmployer2));
				newRow.insertCell(18).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employer-3', null, dataItem.deductionSocialInsuranceEmployer3));
				newRow.insertCell(19).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employee-1', null, dataItem.deductionSocialInsuranceEmployee2));
				newRow.insertCell(20).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employee-2', null, dataItem.deductionSocialInsuranceEmployee2));
				newRow.insertCell(21).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-social-insurance-on-employee-3', null, dataItem.deductionSocialInsuranceEmployee3));
				newRow.insertCell(22).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-wht', null, dataItem.deductionWht));
				newRow.insertCell(23).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-other-deduction-1', null, dataItem.deductionOther1));
				newRow.insertCell(24).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-other-deduction-2', null, dataItem.deductionOther2));
				newRow.insertCell(25).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-de-other-deduction-3', null, dataItem.deductionOther3));
				newRow.insertCell(26).appendChild(payRollDetailDataTable.createInputCell('form-control ap-big-decimal txt-deduction-total','readonly', dataItem.totalDeductionOriginal));
				
				var newCell27  = newRow.insertCell(27);
				var newText27 = document.createElement('div');
				newText27.className  = "input-group";
				newText27.innerHTML = '<span class="input-group-addon original-currency-code">' + $("#select-currency-type").val() + '</span><input type="text" class="form-control ap-big-decimal-allow-minus txt-net-payment alway-readonly" readonly value="' + dataItem.netPaymentOriginal + '">';
				newCell27.appendChild(newText27);
				
				newRow.insertCell(28).appendChild(payRollDetailDataTable.createInputCell('form-control txt-re-fx-rate', 'readonly', $('#txt-fx-rate').val()));
				
				var newCell29  = newRow.insertCell(29);
				var newText29 = document.createElement('div');
				newText29.className  = "input-group";
				newText29.innerHTML = '<span class="input-group-addon">' + $("#currentCurrency").val() + '</span><input type="text" class="form-control ap-big-decimal-allow-minus txt-net-payment-converted alway-readonly" readonly value="' + dataItem.netPaymentConverted +'">';
				newCell29.appendChild(newText29);
				
				newRow.insertCell(30).appendChild(payRollDetailDataTable.createInputCell('form-control txt-approval-code', null, dataItem.approvalCode));
				
				var newCell31  = newRow.insertCell(31);
				var newText31  = document.createElement('span');
				newText31.innerHTML = '<div class="ap-status apr-002 ap-detail">' + $('#apr002Value').val() + '</div>';
				newCell31.appendChild(newText31);
				
				var newCell32  = newRow.insertCell(32);
				var newText32  = document.createElement('span');
				newText32.innerHTML = '<span class="btn-delete-payment-detail-row glyphicon glyphicon-trash"></span>';
				newCell32.appendChild(newText32);
				
				/*
				 * setup for some component
				 */
				payRollDetailDataTable.setUpTableRow($('#payroll-table-data tbody tr:last'));
				payRollDetail.renderCopyButton();
			},
			createInputCell: function(className, readonly, value){
				var newInput  = document.createElement("input");
				newInput.setAttribute('type', 'text');
				if(className){
					newInput.setAttribute('class', className);
				}
				if(readonly){
					newInput.setAttribute('readonly', readonly);
				}
				if(value){
					newInput.setAttribute('value', value);
				}
				return newInput;
			},
			deletePayrollDetailRow: function(id){
				$('#payroll-table-data').find('tr[dataId="' + id +'"]').remove();
				payRollDetail.totalPayrollDetails--;
				for(i = 0; i < payRollDetail.dataListPayrollDetails.length; i++)
				{
					var item = payRollDetail.dataListPayrollDetails[i];
					if(item.id == id)
					{
						item['removed'] = true;
						payRollDetail.totalLaborCostOriginal = getNewDecimal(payRollDetail.totalLaborCostOriginal).minus(item.totalLaborCostOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						payRollDetail.totalDeductionOriginal = getNewDecimal(payRollDetail.totalDeductionOriginal).minus(item.totalDeductionOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						payRollDetail.totalNetPaymentOriginal = getNewDecimal(payRollDetail.totalNetPaymentOriginal).minus(item.netPaymentOriginal).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						payRollDetail.totalNetPaymentConverted = getNewDecimal(payRollDetail.totalNetPaymentConverted).minus(item.netPaymentConverted).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						payRollDetail.updateSummaryValueUI();
						if(typeof id == 'string' && id.indexOf('new-') == 0)
						{
							payRollDetail.dataListPayrollDetails.splice(i, 1);
						}
						break;
					}
				}
				payRollDetailDataTable.showApprovalInfo();
				payRollDetail.renderCopyButton();
			},
			caculateValue: function(tableRow){
				var id = tableRow.attr('dataId');
				var dataItem = payRollDetail.getPayrollDetailItemById(id);
				var laborBaseSalaryPayment = getNewDecimal(tableRow.find('.txt-la-base-salary-payment').val());
				var laborAdditionalPayment1 = getNewDecimal(tableRow.find('.txt-la-additional-payment-1').val());
				var laborAdditionalPayment2 = getNewDecimal(tableRow.find('.txt-la-additional-payment-2').val());
				var laborAdditionalPayment3 = getNewDecimal(tableRow.find('.txt-la-additional-payment-3').val());
				var laborSocialInsuranceEmployer1 = getNewDecimal(tableRow.find('.txt-la-social-insurance-on-employer-1').val());
				var laborSocialInsuranceEmployer2 = getNewDecimal(tableRow.find('.txt-la-social-insurance-on-employer-2').val());
				var laborSocialInsuranceEmployer3 = getNewDecimal(tableRow.find('.txt-la-social-insurance-on-employer-3').val());
				var laborOtherPayment1 = getNewDecimal(tableRow.find('.txt-la-other-payment-1').val());
				var laborOtherPayment2 = getNewDecimal(tableRow.find('.txt-la-other-payment-2').val());
				var laborOtherPayment3 = getNewDecimal(tableRow.find('.txt-la-other-payment-3').val());
				var totalLaborCost = laborBaseSalaryPayment.plus(laborAdditionalPayment1).plus(laborAdditionalPayment2).plus(laborAdditionalPayment3).plus(laborSocialInsuranceEmployer1)
									.plus(laborSocialInsuranceEmployer2).plus(laborSocialInsuranceEmployer3).plus(laborOtherPayment1).plus(laborOtherPayment2).plus(laborOtherPayment3);
				
				var deductionSocialInsuranceEmployer1 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employer-1').val());
				var deductionSocialInsuranceEmployer2 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employer-2').val());
				var deductionSocialInsuranceEmployer3 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employer-3').val());
				var deductionSocialInsuranceEmployee1 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employee-1').val());
				var deductionSocialInsuranceEmployee2 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employee-2').val());
				var deductionSocialInsuranceEmployee3 = getNewDecimal(tableRow.find('.txt-de-social-insurance-on-employee-3').val());
				var deductionWht = getNewDecimal(tableRow.find('.txt-de-wht').val());
				var deductionOther1 = getNewDecimal(tableRow.find('.txt-de-other-deduction-1').val());
				var deductionOther2 = getNewDecimal(tableRow.find('.txt-de-other-deduction-2').val());
				var deductionOther3 = getNewDecimal(tableRow.find('.txt-de-other-deduction-3').val());
				var totalDeduction =  deductionSocialInsuranceEmployer1.plus(deductionSocialInsuranceEmployer2).plus(deductionSocialInsuranceEmployer3).plus(deductionSocialInsuranceEmployee1)
									 .plus(deductionSocialInsuranceEmployee2).plus(deductionSocialInsuranceEmployee3).plus(deductionWht).plus(deductionOther1).plus(deductionOther2).plus(deductionOther3);
				var netPayment = totalLaborCost.minus(totalDeduction);
				var netPaymentConverted = netPayment.mul(getNewDecimal($('#txt-fx-rate').val()));
				console.log(netPaymentConverted.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf());
				/*
				 * update data item
				 */
				dataItem.laborBaseSalaryPayment = laborBaseSalaryPayment.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborAdditionalPayment1 = laborAdditionalPayment1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborAdditionalPayment2 = laborAdditionalPayment2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborAdditionalPayment3 = laborAdditionalPayment3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborSocialInsuranceEmployer1 = laborSocialInsuranceEmployer1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborSocialInsuranceEmployer2 = laborSocialInsuranceEmployer2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborSocialInsuranceEmployer3 = laborSocialInsuranceEmployer3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborOtherPayment1 = laborOtherPayment1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborOtherPayment2 = laborOtherPayment2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.laborOtherPayment3 = laborOtherPayment3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				var oldtotalLaborCost =  getNewDecimal(dataItem.totalLaborCostOriginal);
				dataItem.totalLaborCostOriginal = totalLaborCost.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				
				dataItem.deductionSocialInsuranceEmployer1 = deductionSocialInsuranceEmployer1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionSocialInsuranceEmployer2 = deductionSocialInsuranceEmployer2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionSocialInsuranceEmployer3 = deductionSocialInsuranceEmployer3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionSocialInsuranceEmployee1 = deductionSocialInsuranceEmployee1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionSocialInsuranceEmployee2 = deductionSocialInsuranceEmployee2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionSocialInsuranceEmployee3 = deductionSocialInsuranceEmployee3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionWht = deductionWht.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionOther1 = deductionOther1.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionOther2 = deductionOther2.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem.deductionOther3 = deductionOther3.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				
				var oldTotalDeduction = getNewDecimal(dataItem.totalDeductionOriginal);
				dataItem.totalDeductionOriginal = totalDeduction.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				
				var oldNetPayment =  getNewDecimal(dataItem.netPaymentOriginal);
				dataItem.netPaymentOriginal = netPayment.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				
				var oldNetPaymentConverted =  getNewDecimal(dataItem.netPaymentConverted);
				dataItem.netPaymentConverted = netPaymentConverted.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				
				payRollDetail.totalLaborCostOriginal = getNewDecimal(payRollDetail.totalLaborCostOriginal).minus(oldtotalLaborCost).plus(getNewDecimal(dataItem.totalLaborCostOriginal)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				payRollDetail.totalDeductionOriginal = getNewDecimal(payRollDetail.totalDeductionOriginal).minus(oldTotalDeduction).plus(getNewDecimal(dataItem.totalDeductionOriginal)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				payRollDetail.totalNetPaymentOriginal = getNewDecimal(payRollDetail.totalNetPaymentOriginal).minus(oldNetPayment).plus(getNewDecimal(dataItem.netPaymentOriginal)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				payRollDetail.totalNetPaymentConverted = getNewDecimal(payRollDetail.totalNetPaymentConverted).minus(oldNetPaymentConverted).plus(getNewDecimal(dataItem.netPaymentConverted)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				dataItem['edited'] = true;
				return dataItem;
			},
			setUpTableRow: function(tableRow){
				tableRow.find('input.ap-big-decimal').setupInputNumberMask({allowMinus:false});
				tableRow.find('input.ap-big-decimal-allow-minus').setupInputNumberMask({allowMinus:true});
				var newTxtEmployee = tableRow.find('.txt-employee-id');
				var accountTautocomplete = newTxtEmployee.tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: ["ID", GLOBAL_MESSAGES['cashflow.payroll.detail.employeeID'], GLOBAL_MESSAGES['cashflow.payroll.detail.name'],
							 GLOBAL_MESSAGES['cashflow.payroll.detail.roleTitle'], GLOBAL_MESSAGES['cashflow.payroll.detail.team'],
							 GLOBAL_MESSAGES['cashflow.payroll.detail.division'], "divisionCode", "joinDate", "resignDate"],
					hide: [false, true, true, true, true, true, false, false, false],
					dataIndex: 0,
					addNewDataLink: '/admin/employeeMaster/list',
			        data: function(){
		        		var x = { keyword: accountTautocomplete.searchdata()}; 
		        		return x;
		        	},
			        ajax: {
		                url: "/manage/getEmployeeMasterForPayroll",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountTautocomplete.searchdata();
		                		return keyword;
		                	},
		                	paymentScheduleDate: $('#txt-scheduled-payment-date').val()
		                },
		                success: function (data) {
		                	$('.table-scroll').addClass('table-prevent-overlap');
		                    return data;
		                }
		            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = accountTautocomplete.all();
			        	newTxtEmployee.val(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.employeeID']]);
			        	accountTautocomplete.settext(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.employeeID']]);
			        	newTxtEmployee.parents('tr').find('td:nth-child(2) input').val(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.roleTitle']]);
			        	newTxtEmployee.parents('tr').find('td:nth-child(3) input').val(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.division']]);
			        	newTxtEmployee.parents('tr').find('td:nth-child(4) input').val(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.team']]);
			        	newTxtEmployee.parents('tr').find('td:nth-child(5) input').val(selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.name']]);
			        	/*
			        	 * update data
			        	 */
			        	var id = newTxtEmployee.parents('tr').attr('dataId');
			        	var dataItem = payRollDetail.getPayrollDetailItemById(id);
			        	dataItem.employeeCode = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.employeeID']];
			        	dataItem.roleTitle = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.roleTitle']];
			        	dataItem.division = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.division']];
			        	dataItem.team = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.team']];
			        	dataItem.name = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.name']];
			        	$('.table-scroll').removeClass('table-prevent-overlap');
			        }
				});
				accountTautocomplete.settext(newTxtEmployee.val());
				
				var newTxtApproval = tableRow.find('.txt-approval-code');
				var approvalTautocomplete = newTxtApproval.tautocomplete({
			        highlight: "",
			        autobottom: true,
					columns: [GLOBAL_MESSAGES['cashflow.payroll.detail.approvalNo'], GLOBAL_MESSAGES['cashflow.payroll.detail.amount'], 
							  GLOBAL_MESSAGES['cashflow.payroll.detail.approvalDate'], GLOBAL_MESSAGES['cashflow.payroll.detail.applicant'], GLOBAL_MESSAGES['cashflow.payroll.detail.purpose']],
					hide: [true, true, true, true, true],
					dataIndex: 0,
			        data: function(){
			        		var x = { keyword: approvalTautocomplete.searchdata(), bookingDate: $("#txt-booking-date").val()}; 
			        		return x;
			        	},
			        ajax: {
			                url: "/apinvoice/getListApprovalAvailable",
			                type: "GET",
			                data: {
			                	keyword: function(){
			                		var keyword = approvalTautocomplete.searchdata();
			                		return keyword;
			                	},
			                	bookingDate : function(){
			                		return $("#txt-booking-date").val();
			                	}
			                },
			                success: function (ret) {
			                    return ret;
			                }
			            },
			        delay: 1000,
			        onchange: function () {
			        	var selectedData = approvalTautocomplete.all();
			        	var id = newTxtEmployee.parents('tr').attr('dataId');
			        	var dataItem = payRollDetail.getPayrollDetailItemById(id);
			        	//when set an approval code
			        	if(newTxtApproval.next().next().val().length > 0)
			        	{
			        		dataItem.approvalCode = selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.approvalNo']];
				        	newTxtApproval.attr('approvalcode', selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.approvalNo']]);
				        	newTxtApproval.attr('amount', selectedData[GLOBAL_MESSAGES['cashflow.payroll.detail.amount']].split(" ")[1]);
			        	}
			        	//when remove approval code
			        	else {
			        		dataItem.approvalCode = "";
				        	newTxtApproval.attr('approvalcode',"");
				        	newTxtApproval.attr('amount',"");
			        	}
			        	payRollDetailDataTable.showApprovalInfo();
			        	dataItem['edited'] = true;
			        }
				});
				approvalTautocomplete.settext(newTxtApproval.val());
			},
			showApprovalInfo: function(){
				var showingList = payRollDetail.dataListPayrollDetails.filter(function(item){
    				return item['removed'] != true;
    			});
				var approvalInfo = {};
				for(i = 0; i < showingList.length; i++){
					var item = showingList[i];
					if(item.approvalCode){
						if(approvalInfo[item.approvalCode]){
							approvalInfo[item.approvalCode] = getNewDecimal(approvalInfo[item.approvalCode]).plus(getNewDecimal(item.netPaymentOriginal)).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
						}
						else {
							approvalInfo[item.approvalCode] = item.netPaymentOriginal;
						}
					}
				}
				console.log(approvalInfo);
				var htmlString = "";
				for (var key in approvalInfo) {
				    if (approvalInfo.hasOwnProperty(key)) {
				        console.log(key + " -> " + approvalInfo[key]);
				        var approvalAmount = $('.txt-approval-code[approvalcode="' + key + '"]').attr('amount');
				        htmlString += '<li>' + GLOBAL_MESSAGES['cashflow.payroll.detail.mess1'].replace("%1", key).replace("%2", $('#select-currency-type').val())
				        																	   .replace("%3", formatStringAsNumber(approvalInfo[key])).replace("%4", formatStringAsNumber(approvalAmount)) + '</li>';
				    }
				}
				$('#alertApprovalStatus').html(htmlString);
			}
	};
}());