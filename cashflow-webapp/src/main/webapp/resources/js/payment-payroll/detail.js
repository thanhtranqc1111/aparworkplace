(function () {
	PayrollPaymentOrder = {
		PAYMENT_STATUSES: {},
		init: function () {
			//$('.error-area').showAlertMessage(['1123']);
			
			var ddmmyyyy = 'dd/mm/yyyy';

		    $('#valueDate').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    $('#searchForm_scheduledPaymentDateFrom').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    $('#searchForm_scheduledPaymentDateTo').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    $('#searchForm_monthFrom, #searchForm_monthTo').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom'
			});
		    
		    $('input').filter(function(){
				return this.id.match(/^net-payment-amount-*/);
			}).each(function(index, value){
				$('#' + this.id).change(function (event) {
					PayrollPaymentOrder.calculateTotalNetPayment(index, event);
				});
			});
		    
		    //disable ap invoice is not approval 
		    $('input').filter(function(){
				return this.id.match(/is-approval-*/);
			}).each(function(){
				if(this.value == 'false'){
		    		$('#' + this.id).parent().parent().addClass('disable-row');
		    	}
			});
		    
		    ////////ADD AUTO COMPLETE FOR ACCOUNTPAYABLE////////
		    var accountPayableVal = $('#accountPayableCode').tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.paymentOrder.payRoll.detail.accountCode'), label('cashflow.paymentOrder.payRoll.detail.parentCode'), label('cashflow.paymentOrder.payRoll.detail.accountName'), label('cashflow.paymentOrder.payRoll.detail.type')],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountPayableVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountPayableVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountPayableVal.all();
		        	$('#accountPayableCode').val(selectedData[label('cashflow.paymentOrder.payRoll.detail.accountCode')]);
		        	$('#accountPayableName').val(selectedData[label('cashflow.paymentOrder.payRoll.detail.accountName')]);
		        }
			});
		    accountPayableVal.settext($('#accountPayableCode').val());
		    
		    $('input[autocomplete]').attr("readonly", true);
			//Display read only when page on mode edit.
			if(!modeEdit)
			{
				$('#accountPayableCode').parents('.acontainer').find('input[autocomplete]').attr("readonly", false);
			}
		    
			//Disable input when edit
		    if(modeEdit){
		    	$("#bankName").attr('readonly', true);
		    	$("#bankAccount").attr('readonly', true);
		    	$("#valueDate").attr('readonly', true);
    			$("#bankRefNo").attr('readonly', true);
				$("#description").attr('readonly', true);
				$("#paymentApprovalNo").attr('readonly', true);
				$("#accountPayableCode").attr('readonly', true);
				$("#accountPayableName").attr('readonly', true);
		    	
				//disable all input in area search
				$("#search-zone input, #search-zone select, #search-zone a").attr('disabled', true);
				
				$(".detail-id-all-cb").prop('checked', "checked").attr("readonly", true);
		    	$(".detail-id-cb").prop('checked', "checked").attr("readonly", true);
		    	
		    	$("#save-btn").addClass('disabled');
		    	$("#reset-btn").addClass('disabled');
		    	
		    }else{
		    	$("#save-btn").removeClass('disabled');
		    	$("#edit-btn").addClass('disabled');
		    	$("#copy-btn").addClass('disabled');
		    }
		    
		    PayrollPaymentOrder.setReadonlyInput();
		    
		    $('#payment-table').DataTable( {
		        "scrollY": 200,
		        "scrollX": true,
		        "searching": false,
		        "paging": false,
		        "info": false,
		        "serverSide" : false,
		        "ordering" : false,
		        "columnDefs": [
		            { "width": "5%", "targets": 0 },
		            { "width": "10%", "targets": 1 },
		            { "width": "10%", "targets": 2 },
		            { "width": "10%", "targets": 3 },
		            { "width": "20%", "targets": 4 },
		            { "width": "20%", "targets": 5 },
		            { "width": "20%", "targets": 6 }
		         ],
		        "drawCallback": function( settings ) {
		            $('.payment-table-wrapper').css('width','100%');
		        }
		    } );
		},
		
		eventListener: function () {
			if(!$('#search-btn').is('[disabled=disabled]')){
				$("#search-btn").click(function() {
					$("#action").val("search");
					$('#isConfirmChanged').val(false);
					PayrollPaymentOrder.reFormatNumber();
					
					// submit form
					$('#form1').submit();
				});
			}
			
			// disable enter key press
			$('#form1').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					e.preventDefault();
					return false;
				}
			});
			
			//
			$("#edit-btn").click(function(){
				let status = $('#statusCode').val();
				let enabled = false;
				if (status == '002') { // active dropdown  when status is processing (002)
					$('.pr-label-status').hide();
					$('.pr-dropdown-status').show();
				} else if(status == '001'){
					enabled = true;
				}
				
				$("#bankName").attr('disabled', false).attr('readonly', false);
		    	$("#bankAccount").attr('disabled', false).attr('readonly', false);
		    	$("#valueDate").attr('readonly', false);
    			$("#bankRefNo").attr('readonly', false);
				$("#description").attr('readonly', false);
				$("#paymentApprovalNo").attr('readonly', false);
				$("#accountPayableCode").attr('readonly', false);
				$("#accountPayableName").attr('readonly', false);
				
		    	$('#form1 table .default').attr('readonly', enabled);
		    	$('#form1 table select').attr('readonly', enabled).attr('disabled', enabled);
		    	
		    	$('.col-sm-8 input[autocomplete]').attr("readonly", false);
		    	$('#searchForm_payeeName').parents('.acontainer').find('input[autocomplete]').attr("readonly", true);
		    	
		    	$("#save-btn").removeClass('disabled');
		    	$("#reset-btn").removeClass('disabled');
				
			});
			
			//Do check all check box in table and enable for some input relate
			$(".detail-id-all-cb").change(function(){
				var checked = this.checked;
				
				if(checked) {
					$( "tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "checked");
						PayrollPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}else{
					$( "tr:not(.disable-row) .detail-id-cb" ).each(function() {
						var tokens = this.id.split('-');
						var rowId = tokens[tokens.length -1];
						
						$(this).prop('checked', "");
						PayrollPaymentOrder.enableInputInListDetail(rowId, !checked);
					});
				}
			});
			
			//Trigger action check for each check box
			$(".detail-id-cb").change(function() {
				var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
			    if(!this.checked) {
			    	$(".detail-id-all-cb").prop('checked', ""); // uncheck check box all
			    }
			    
			    PayrollPaymentOrder.addAccountPayable(rowId, this.checked);
			    
			    PayrollPaymentOrder.enableInputInListDetail(rowId, !this.checked);
			});
			
			PayrollPaymentOrder.addCommasNumber();
			
			//change invoice status
			$(document).on('click','.pr-dropdown-status li',function(){
				const PROCESSING = '002';
				const CANCELLED = '003';
				
				var statusCode = $(this).find('a').attr('status');
				$(this).closest('.pr-dropdown-status').find('button').attr('class','pr-status pay-' + statusCode + ' pr-status-header');
				$(this).closest('.pr-dropdown-status').find('.status-label').html($(this).find('a').html());
				
				// update dropdown
				if(statusCode == CANCELLED){
					$(this).find('a').attr('status', PROCESSING);
					$(this).find('a').html(PayrollPaymentOrder.PAYMENT_STATUSES[PROCESSING]);

				} else if(statusCode == PROCESSING){
					$(this).find('a').attr('status', CANCELLED);
					$(this).find('a').html(PayrollPaymentOrder.PAYMENT_STATUSES[CANCELLED]);
				}
				$('#statusCode').val(statusCode);
				
			});
			
		},
		
		savePaymentOrder :  function() {
			var valid = false;
			var message = [];
			
			$('input').filter(function(){
				return this.id.match(/^payroll-id-*/);
			}).each(function(){
				if($(this).is(':checked') == true) {
					valid = true;
				}
			});
			if(valid === false){
				message.push(label('cashflow.paymentOrder.payRoll.detail.payrollRequired'));
			}

			// check value date is required
			var valueDate = $("#valueDate").val();
			if(!valueDate.trim() || valueDate.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.payRoll.detail.valueDate')));
				valid = false;
			}
			
			//
			var accountPayableCode = $('#accountPayableCode').val();
			if(!accountPayableCode.trim() || accountPayableCode.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.payRoll.detail.accountPayableCode')));
				valid = false;
			}
			
			//
			var accountPayableName = $('#accountPayableName').val();
			if(!accountPayableName.trim() || accountPayableName.trim().length == 0){
				message.push(validate.require(label('cashflow.paymentOrder.payRoll.detail.accountPayableName')));
				valid = false;
			}
			
			// bank ref no
			var bankRefNo = $('#bankRefNo').val();
			if(bankRefNo.trim().length != 0) {
				if(/^[a-zA-Z0-9- ]*$/.test(bankRefNo) == false) {
					message.push(label('cashflow.paymentOrder.payRoll.detail.error.bankRefNo'));
				    valid = false;
				}
			}

			//
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-*/);
			}).each(function(index, value) {
				var payrollNo = $('#payroll-no-'+index).html();
				if($('#payroll-id-'+index).is(':checked') == true) { // ap invoice checked
					
					// check payment include gst original amount is required
					var paymentOrderAmount = $('#'+this.id).val();
					if(paymentOrderAmount.trim().length == 0 || !paymentOrderAmount.trim()){
						var error = label('cashflow.paymentOrder.payRoll.detail.originalAmountReq').format(payrollNo);
//						var error = 'Payment Order Amount of Payroll ('+payrollNo+') is required.';
						message.push(error);
						valid = false;
					}
					
					// check payment order total amount less or equal payroll amount
					var paymentAmountInput = Number($('#'+this.id).autoNumeric('get'));
					var totalPayrollAmount = Number($('#total-net-payment-'+index).autoNumeric('get'));
					var paymentAmount = Number($('#sub-net-payment-amount-'+index).val());
					$('#all-total-net-payment-amount-'+index).autoNumeric('set', (totalPayrollAmount - paymentAmount + paymentAmountInput));
					var totalPayment = $('#all-total-net-payment-amount-'+index).autoNumeric('get');
					
					if(totalPayment > totalPayrollAmount) {
						var totalPayAmount = $('#all-total-net-payment-amount-'+index).val();
						var payrollAmount = $('#total-net-payment-'+index).html();
						
						//set message error
						var error = label('cashflow.paymentOrder.payRoll.detail.totalNotBalance').format(totalPayAmount, payrollNo, payrollAmount);
//						var error = 'Total net payment order(s) amount ('+ totalPayAmount +') is more than Payroll No ('+ payrollNo +') amount ('+ payrollAmount +').';
						message.push(error);
						valid = false;
					}
				}
				
			});

			//
			if(valid) {
				$('#isConfirmChanged').val(false);
				PayrollPaymentOrder.reFormatNumber();
				$('#form1').submit();
			} else {
				$('.error-area').showAlertMessage(message)
			}
		},
		
		setReadonlyInput: function(){
			$('#form1 table .default').attr('readonly', true);
		},
		
		reFormatNumber : function(){
			$('input').filter(function(){
				return this.id.match(/^paid-net-payment-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-net-payment-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('#includeGstConvertedAmount').val($('#includeGstConvertedAmount').autoNumeric('get'));
			$('#includeGstOriginalAmount').val($('#includeGstOriginalAmount').autoNumeric('get'));
		},
		
		addCommasNumber: function() {
			$('input').filter(function(){
				return this.id.match(/^paid-net-payment-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^all-total-net-payment-amount-*/);
			}).each(function(index, value){
				$(this).autoNumeric('init', {vMin : '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^unpaid-net-payment-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('span').filter(function(){
				return this.id.match(/^total-net-payment-*/);
			}).each(function(){
				$(this).autoNumeric('init');
			});
			
			$('#includeGstConvertedAmount').autoNumeric('init',  {mRound: 'D'});
			$('#includeGstOriginalAmount').autoNumeric('init',  {mRound: 'D'});
		},
		
		calculateTotalNetPayment : function() {
			var total = getNewDecimal(0);
			
			$('input').filter(function(){
				return this.id.match(/^net-payment-amount-*/);
			}).each(function(){
				total = total.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('#includeGstConvertedAmount').autoNumeric('set', total.valueOf());
			
		},
		
		enableInputInListDetail: function(rowId, isEnable){
			$("#net-payment-amount-"+rowId).prop('readonly', isEnable);
			$('#net-payment-amount-' + rowId).autoNumeric('set', 0);
			
			// calculate Payment Order Amount
			if(!isEnable){
				var netPaymentAmount = getNewDecimal($('#total-net-payment-' + rowId).autoNumeric('get'));
				var paidAmount = getNewDecimal($('#paid-net-payment-amount-' + rowId).autoNumeric('get'));
				$('#net-payment-amount-' + rowId).autoNumeric('set', (netPaymentAmount.minus(paidAmount)).valueOf());
			}
			
			PayrollPaymentOrder.calculateTotalNetPayment();
		},

		addAccountPayable: function(rowId, isChecked) {
			var classList = $('.detail-id-cb');
			let accountCode = "";
			let accountName = "";
			$.each(classList, function(index, item) {
				if(this.checked) {
					if(accountCode != ""){
						if(accountCode == $('#account-payable-code-'+index).val()){
							$("#save-btn").removeClass('disabled');
							$('#warning-message').html('');
						} else {
							$("#save-btn").addClass('disabled');
							$('#warning-message').html(label('cashflow.paymentOrder.payRoll.detail.accountPayableDif'));
						}
						
					} else {
						$("#save-btn").removeClass('disabled');
						$('#warning-message').html('');
						accountCode = $('#account-payable-code-'+index).val();
						accountName = $('#account-payable-name-'+index).val();
					}
				}
			});
			
			$('#accountPayableName').val(accountName);
			$('#accountPayableCode').parent().find('input[autocomplete="off"]').val(accountCode);
			$('#accountPayableCode').val(accountCode);
		}
	};
}());