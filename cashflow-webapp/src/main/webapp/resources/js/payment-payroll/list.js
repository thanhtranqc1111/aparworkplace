(function () {
	PaymentPayrollSearch = {
		dataTable: null, //main data table
		fromDate: null,
		toDate: null,
		bankRef: null,
		fromPayrollNo: null,
		toPayrollNo: null,
		status: null,
		init: function () {
			$('#datepicker input').datepicker({
				format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
			});
			
			PaymentPayrollSearch.fromDate = $('#paidFromDate').val();
			PaymentPayrollSearch.toDate = $('#paidToDate').val();
			PaymentPayrollSearch.bankRef = $('#bankRef').val();
			PaymentPayrollSearch.status = $('#status').val();
			PaymentPayrollSearch.fromPayrollNo = $('#payrollNoFrom').val();
			PaymentPayrollSearch.toPayrollNo = $('#payrollNoTo').val();
			
			PaymentPayrollSearch.dataTable = $('#payment-tbl').removeAttr('width').DataTable({
				"ajax": {
		            url: "/paymentPayroll/api/getAllPagingPaymentOrder",
		            type: "GET",
		            data: function ( d ) {
		            	d.fromDate = PaymentPayrollSearch.fromDate;
		                d.toDate = PaymentPayrollSearch.toDate;
		                d.bankRef = PaymentPayrollSearch.bankRef;
		                d.status = PaymentPayrollSearch.status;
		                d.fromPayrollNo = PaymentPayrollSearch.fromPayrollNo;
		                d.toPayrollNo = PaymentPayrollSearch.toPayrollNo;
		                
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="15">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
						hideAjaxStatus();
						PaymentPayrollSearch.mergeRows('#payment-tbl', 1, 8);
						PaymentPayrollSearch.mergeRows('#payment-tbl', 9, 12);
		            }
				},
				columns: [
			        {
			            "data" : "paymentOrderNo",
			            "width" : "5%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	var paymentOrderNo = "";
			            	if(full.paymentOrderNo != undefined){
			            		paymentOrderNo = "<a href='detail/"+full.paymentOrderNo+"'>"+data+"</a>";
			            	}
			            	return paymentOrderNo;
			            }
			        },
			        {
			            "data" : "valueDate",
			            "width" : "3%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	var valueDate = "";
			            	if(full.valueDate != undefined){
			            		valueDate = "<span class='right' >" + full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year + "</span>";
			            	}
			            	return valueDate;
			            }
			        },
			        {
			            "data" : "bankName",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankName = "";
			            	if(full.bankName != undefined){
			            		bankName = full.bankName;
			            	}
			            	return bankName;
			            }
			        },
			        {
			            "data" : "bankAccount",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankAccount = "";
			            	if(full.bankAccount != undefined){
			            		bankAccount = full.bankAccount;
			            	}
			            	return bankAccount;
			            }
			        },
			        {
			        	"data" : "bankStatement",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var trans = "";
			            	var length = data.length;
			            	$.each(data, function(i, obj) {
			            		trans += "<a href='/bankStatement/list/"+obj.id+"' target='_blank'>"+obj.id+"</a>";
			            		if ( i != length - 1){
			            			trans += ", ";
			            		}
			            		
			            	});
			            	return trans;
			            }
			        },
			        {
			            "data" : "bankRefNo",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankRefNo = "";
			            	if(full.bankRefNo != undefined){
			            		bankRefNo = "<span style='white-space:pre-wrap;'>"+data+"</span>";
			            	}
			            	return bankRefNo;
			            }
			        },
			        {
			        	"data" : "includeGstOriginalAmount",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.paymentCurrency == undefined){
			            		return "";
			            	}
			            	return "<span class='pull-left'>" + full.paymentCurrency + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "paymentStatus",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var paymentStatus = "";
			            	if(full.paymentStatus != undefined){
			            		paymentStatus = "<div class='ap-status  pay-"+full.paymentStatusCode+"'>"+data+"</div>"; 
			            	}
			            	return paymentStatus;
			            }
			        },
			        {
			            "data" : "payrollNo",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var payrollNo = "";
			            	if(full.payrollNo != undefined){
			            		payrollNo = "<a href='/payroll/detail/"+full.payrollNo+"'>"+data+"</a>";
			            	}
			            	return payrollNo;
			            }
			        },
			        {
			            "data" : "month",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var month = "";
			            	if(full.month != undefined){
			            		month = "<span class='right' >" + (full.month.month + 1) + "/" + full.month.year + "</span>";
			            	}
			            	return month;
			            }
			        },
			        {
			        	"data" : "totalNetPaymentAmountConverted",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.totalNetPaymentAmountConverted == undefined || full.totalNetPaymentAmountConverted == ""){
			            		return "";
			            	}
			            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "payrollStatus",
			            "width" : "5%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var payrollStatus = "";
			            	if(full.payrollStatus != undefined){
			            		payrollStatus = "<div class='pr-status ppr-"+full.payrollStatusCode+"'>"+data+"</div>";
			            	}
			            	return payrollStatus;
			            }
			        },
		        ],
		        searching: false,
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true,
				'order' : [ [ 0, 'desc' ] ],
		        'columnDefs' : []
			});
			
			
		},
		
		eventListener: function () {
			$(document).on("click","#btn-search",function(){
				PaymentPayrollSearch.fromDate = $('#paidFromDate').val();
				PaymentPayrollSearch.toDate = $('#paidToDate').val();
				PaymentPayrollSearch.bankRef = $('#bankRef').val();
				PaymentPayrollSearch.status = $('#status').val();
				PaymentPayrollSearch.fromPayrollNo = $('#payrollNoFrom').val();
				PaymentPayrollSearch.toPayrollNo = $('#payrollNoTo').val();
				
				PaymentPayrollSearch.dataTable.ajax.reload();
		    });
			
			$('#export').click(function(){
				$('#reset').click();
				var href = $(this).attr("href");
				var param = "";
				
				var dateFrom = $('#paidFromDate').val();
				var dateTo = $('#paidToDate').val();
				var bankRef = $('#bankRef').val();
				var status = $('#status').val();
				var fromPayrollNo = $('#payrollNoFrom').val();
				var toPayrollNo = $('#payrollNoTo').val();
				
				if (dateFrom.length > 0) {
					param += "&dateFrom=" + dateFrom;
				}
				if (dateTo.length > 0) {
					param += "&dateTo=" + dateTo;
				}
				if (bankRef.length > 0) {
					param += "&bankRef=" + bankRef;
				}
				if (status.length) {
					param += "&status=" + status;
				}
				if (fromPayrollNo.length) {
					param += "&fromPayrollNo=" + fromPayrollNo;
				}
				if (toPayrollNo.length) {
					param += "&toPayrollNo=" + toPayrollNo;
				}
				
				if (param.length > 0) {
					href = href + "?" + param.slice(1);
				}
				
				$("#export").attr("href", href);
			});
			
			//clear data search condition form
			$('#btn-reset').click(function(){
				$('#paidFromDate').val('');
				$('#paidToDate').val('');
				$('#bankRef').val('');
				$('#payrollNoFrom').val('');
				$('#payrollNoTo').val('');
				$('#status').val('');
			});
		},
		
		mergeRows : function mergeRows(table, mergeFrom, mergeTo) {
			var rowsList = $(''+table+ ' > tbody').find('tr');
			var previous = null, cellToExtend = null, rowspan = 1;
			
			rowsList.each(function(index, e) {
				var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
				if (previous == content && content !== "" ) {
					rowspan = rowspan + 1;
					
					for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
						if(i >= mergeFrom && i <= mergeTo) {
							$(this).find('td:nth-child(' + i + ')').addClass('hidden');
							padding = rowspan * 16;
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
						} 
					}
				} else {
		             rowspan = 1;
		             previous = content;
		             cellToExtend = elementTab;
				}
			});
			$(table + ' td.hidden').remove();
		}
		
	};
}());
