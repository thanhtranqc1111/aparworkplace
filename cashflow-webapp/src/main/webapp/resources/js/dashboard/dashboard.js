/**
 * @author ThoaiNH 
 * create May 29, 2018
 */
(function () {
	dashboard = {
		mapMonths: {
			0: "Jan",
			1: "Feb",
			2: "Mar",
			3: "Apr",
			4: "May",
			5: "Jun",
			6: "Jul",
			7: "Aug",
			8: "Sep",
			9: "Oct",
			10: "Nov",
			11: "Dec",
		},	
		init: function () {
			//load default last 6 months
			var toDate = new Date();
			var fromDate = new Date();
			fromDate.setMonth(fromDate.getMonth() - 6);
			$('#txt-fromMonth').val((fromDate.getMonth() + 1) + '/' +  fromDate.getFullYear());
			$('#txt-toMonth').val((toDate.getMonth() + 1) + '/' +  toDate.getFullYear());
			$('.ap-date-picker-month-format').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom'
			}).change(function(){
				dashboard.renderChart($('#txt-fromMonth').val(), $('#txt-toMonth').val());
		    });
			dashboard.renderChart($('#txt-fromMonth').val(), $('#txt-toMonth').val());
		},
		eventListener: function () {
			
		},
		showWarning:function(message){
			$('.alert-warning').showAlertMessage([message]);
			$('#alert-warning').goTo();
		},
		hideWarning: function(){
			$('.alert-warning').hide();
		},
		renderChart: function(fromMonthString, toMonthString){
			//render X title
        	var fromMonth = getDateFromString("01/" + fromMonthString);
			var toMonth = getDateFromString("01/" + toMonthString);
			var months;
		    months = (toMonth.getFullYear() - fromMonth.getFullYear()) * 12;
		    months -= fromMonth.getMonth();
		    months += toMonth.getMonth()  + 1;
		    var crMonth = new Date(fromMonth.getTime());
		    var monthTitle = [];
		    for(i = 0; i < months; i++){
		    	crMonth.setMonth(crMonth.getMonth() + (i > 0 ? 1 : 0));
		    	monthTitle.push(dashboard.mapMonths[crMonth.getMonth()] + "-" + crMonth.getFullYear());
		    }
			if(months <= 0){
				dashboard.showWarning(GLOBAL_MESSAGES['cashflow.dashboard.mess1']);
		    	return;
		    }
			dashboard.hideWarning();
			$.ajax({
	            type : "GET",
	            url : CONTEXT_PATH + '/dashboard/getListChartOne',
	            data: {
	            	fromMonth: fromMonthString,
	            	toMonth: toMonthString
	            	
	            },
	            success : function(ajaxResult) {
	            	if(ajaxResult.length > 0){
	            		//calculate two line chart of AP and AR
		            	/*var aPseri = [];
		            	var aRseri = [];
		            	for(i = 0; i < months; i++){
		            		var totalAP = getNewDecimal('0');
		            		var totalAR = getNewDecimal('0');
		            		for(j = 0; j < ajaxResult.length; j++){
			            		var item = ajaxResult[j];
			            		if(item.stack == "AR"){
			            			totalAR = totalAR.plus(getNewDecimal(item.data[i]));
			            		}
			            		else {
			            			totalAP = totalAP.plus(getNewDecimal(item.data[i]));
			            		}
			            	}
		            		aPseri.push(parseFloat(totalAP.valueOf()));
		            		aRseri.push(parseFloat(totalAR.valueOf()));
		            	}*/
		            	//push line data to chart data
		            	/*ajaxResult.push({
		            		name: GLOBAL_MESSAGES['cashflow.dashboard.totalAPamount'],
	                    	type:'spline',
	                        data: aPseri
	                    });
		            	ajaxResult.push({
		            		name: GLOBAL_MESSAGES['cashflow.dashboard.totalARamount'],
	                    	type:'spline',
	                        data: aRseri
	                    });*/
		            	//init chart
		            	$('#myChartThreeD').highcharts({
		                    chart: {
		                        type: 'column',
		                        height: (9 / 16 * 100) + '%', // 16:9 ratio
		                    },
		                    colors: ['#1abc9c', '#e67e22',  '#9b59b6', '#3498db', '#f39c12'],
		                    title: {
		                        text: GLOBAL_MESSAGES['cashflow.dashboard.mess2']
		                    },
		                    xAxis: {
		                        categories: monthTitle,
		                        labels: {
		    			            skew3d: true,
		    			            style: {
		    			                fontSize: '14px'
		    			            }
		    			        }
		                    },
		                    yAxis: {
		                        allowDecimals: false,
		                        min: 0,
		                        title: {
		                            text: GLOBAL_MESSAGES['cashflow.dashboard.amount']
		                        },
		                        stackLabels: {
		                            enabled: true
		                         }
		                    },
		                    tooltip: {
		                    	 formatter: function() {
		    			            return this.series.name + ":" + formatStringAsNumber(this.y + "");
		    			         }
		                    },
		                    plotOptions: {
		                    	column: {
		    			            stacking: 'normal',
		    			            depth: 40
		    			        }
		                    },
		                    series: ajaxResult
		                });
		            	
		            }
	            }	
	        });
		}
	};
}());