(function () {
	ARInvoice = {
			dataTable: null, //main data table
			fromMonth: null,
			toMonth: null,
			fromArNo: null,
			toArNo: null,
			invoiceNo: null,
			payerName: null,
			status: null,
			init: function () {
				$('.datepicker input').datepicker({
					format: "mm/yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
			        orientation: 'auto bottom'
				});
				
				ARInvoice.fromMonth = $('#fromMonth').val();
				ARInvoice.toMonth = $('#toMonth').val();
				ARInvoice.fromArNo = $('#fromArNo').val();
				ARInvoice.toArNo = $('#toArNo').val();
				ARInvoice.payerName = $('#payerName').val();
				ARInvoice.invoiceNo = $('#invoiceNo').val();
				ARInvoice.status = $('#status').val();
				ARInvoice.dataTable = $('#table_data_ar').removeAttr('width').DataTable({
			        "ajax": {
			            url: "/arinvoice/api/getAllPagingARInvoice",
			            type: "GET",
			            data: function ( d ) {
			            	d.fromMonth = ARInvoice.fromMonth;
			                d.toMonth = ARInvoice.toMonth;
			                d.fromArNo = ARInvoice.fromArNo;
			                d.toArNo = ARInvoice.toArNo;
			                d.payerName = ARInvoice.payerName;
			                d.invoiceNo = ARInvoice.invoiceNo;
			                d.status = ARInvoice.status;
			            },
			            error: function() { // error handling
			                $(".table_data-error").html("");
			                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="14">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
			                $("#table_data_processing").css("display", "none");
			            },
			            complete: function(data){
							hideAjaxStatus();
							ARInvoice.mergeRows('#table_data_ar', 1, 9);
							ARInvoice.mergeRows('#table_data_ar', 10, 15);
							ARInvoice.displayRightText();
			            }
			        },
			        columns: [
				        {
				            "data" : "arInvoiceNo",
				            "width" : "4%",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	return "<a href='detail/"+full.arInvoiceNo+"'>"+data+"</a>";
				            }
				        }, 
				        {
				            "data" : "month",
				            "width" : "3%",
				            "orderable": true,
				            "render": function(data, type,full, meta) {
				            	if(full.month != undefined){
				            		return "<span class='right' >" + (full.month.month + 1) + "/" + full.month.year + "</span>";
				            	}
				             }
				        }, 
				        {
				            "data" : "payerAccount",
				            "orderable": false,
				            "width" : "8%",
				        },
				        {
				            "data" : "claimTypeVal",
				            "orderable": false,
				            "width" : "4%",
				        },
				        {
				            "data" : "payerName",
				            "orderable": true,
				            "width" : "10%"
				        },
				        {
				            "data" : "invoiceNo",
				            "orderable": false,
				            "width" : "4%"
				        },
				        {
				            "data" : "totalAmount",
				            "orderable": false,
				            "width" : "5%",
				            "render": function(data, type,full, meta) {
				            	return "<span class='pull-left'>" +full.originalCurrencyCode+ "</span>" 
				            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            }
				        },
				        {
				            "data" : "remainAmount",
				            "orderable": false,
				            "width" : "5%",
				            "render": function(data, type,full, meta) {
				            	return "<span class='pull-left'>" +full.originalCurrencyCode+ "</span>" 
		            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            }
				        },
				        {
				            "data" : "status",
				            "width" : "6%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return  "<div class='status ari-"+full.statusCode+"' >" +data+ "</div>";
				            }
				        }, 
				        {
				            "data" : "bankTrans",
				            "width" : "2%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var bankTrans = "";
				            	if(full.bankTrans != undefined){
				            		bankTrans =  "<a href='/bankStatement/list/"+full.bankTrans+"'>"+data+"</a>";
				            	}
				            	return bankTrans;
				            }
				        }, 
				        {
				            "data" : "bankName",
				            "width" : "4%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var bankName = "";
				            	if(full.bankName != undefined){
				            		bankName = full.bankName;
				            	}
				            	return bankName;
				            }
				        }, 
				        {
				            "data" : "bankAccount",
				            "width" : "3%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var bankAccount = "";
				            	if(full.bankAccount != undefined){
				            		bankAccount = full.bankAccount;
				            	}
				            	return bankAccount;
				            }
				        }, 
				        {
				            "data" : "transDate",
				            "width" : "2%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var transDate = "";
				            	if(full.transDate != undefined){
				            		transDate = "<span class='right' >" + full.transDate.dayOfMonth + "/" + (full.transDate.month + 1) + "/" + full.transDate.year + "</span>";
				            	}
				            	return transDate;
				             }
				        }, 
				        {
				            "data" : "refNo",
				            "width" : "5%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	var refNo = "";
				            	if(full.refNo != undefined){
				            		refNo = full.refNo;
				            	}
				            	return refNo;
				            }
				        }, 
				        {
				            "data" : "creditAmount",
				            "width" : "5%",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(full.bankCurrencyCode == undefined){
				            		return "";
				            	}
				            	return "<span class='pull-left'>" + full.bankCurrencyCode + "</span>" 
		            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            }
				        }, 
				        
			        ],
			        searching: false,
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.arinvoice.list.all']]],
					"lengthChange" : true,
					'order' : [ [ 0, 'desc' ] ],
			        'columnDefs' : []
				 });
				
		}, //end init
		
		eventListener: function () {
			$(document).on("click","#btn-search",function(){
		    	ARInvoice.fromMonth = $('#fromMonth').val();
				ARInvoice.toMonth = $('#toMonth').val();
				ARInvoice.fromArNo = $('#fromArNo').val();
				ARInvoice.toArNo = $('#toArNo').val();
				ARInvoice.payerName = $('#payerName').val();
				ARInvoice.invoiceNo = $('#invoiceNo').val();
				ARInvoice.status = $('#status').val();
		    	ARInvoice.dataTable.ajax.reload();
		    });
			
			$('#export').click(function(){
				var href = $(this).attr("href");
				var param = "";
				
				var fromMonth = $('#fromMonth').val();
				var toMonth = $('#toMonth').val();
				var invoiceNo = $('#invoiceNo').val();
				var payerName = $('#payerName').val();
				var status = $('#status').val();
				var fromArNo = $('#fromArNo').val();
				var toArNo = $('#toArNo').val();
				
				if (fromMonth.length > 0) {
					param += "&fromMonth=" + fromMonth;
				}
				if (toMonth.length > 0) {
					param += "&toMonth=" + toMonth;
				}
				if (invoiceNo.length) {
					param += "&invoiceNo=" + invoiceNo;
				}
				if (payerName.length > 0) {
					param += "&payerName=" + payerName;
				}
				if (status.length) {
					param += "&status=" + status;
				}
				if (fromArNo.length > 0) {
					param += "&fromArNo=" + fromArNo;
				}
				if (toArNo.length > 0) {
					param += "&toArNo=" + toArNo;
				}
				if (param.length > 0) {
					href = href + "?" + param.slice(1);
				}
				$("#export").attr("href", href);
			});
			
			//clear data search condition form
			$('#btn-reset').click(function(){
				$('#fromMonth').val('');
				$('#toMonth').val('');
				$('#payerName').val('');
				$('#invoiceNo').val('');
				$('#status').val('');
				$('#fromArNo').val('');
				$('#toArNo').val('');
			});
		},
		
		mergeRows : function mergeRows(table, mergeFrom, mergeTo) {
			var rowsList = $(''+table+ ' > tbody').find('tr');
			var previous = null, cellToExtend = null, rowspan = 1;
			
			rowsList.each(function(index, e) {
				var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
				if (previous == content && content !== "" ) {
					rowspan = rowspan + 1;
					
					for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
						if(i >= mergeFrom && i <= mergeTo) {
							$(this).find('td:nth-child(' + i + ')').addClass('hidden');
							padding = rowspan * 16;
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
						} 
					}
				} else {
		             rowspan = 1;
		             previous = content;
		             cellToExtend = elementTab;
				}
			});
			$(table + ' td.hidden').remove();
		},
		
		displayRightText : function(){
			$('#table_data_ar').find('.right').parent().addClass('text-right');
		},
	};
}());