(function () {
	ARInvoiceDetail = {
		dtDetail: null,
		thePayee: null,
		approvalJson: [], //save approval code - amount as json.
		init: function () {
			var ddmmyyyy = 'dd/mm/yyyy';
		    $('#invoiceIssuedDate').datepicker({
		        format: ddmmyyyy,
		        orientation: 'auto bottom',
		    });
		    
		    // Add Account Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/invoice-account-row-detail-*/);
		    }).each(function(index, element){
		    	var accountVal = ARInvoiceDetail.addAccountAutoComplete(index);
		    	accountVal.settext(this.value);
	        });
			
			// Add Project Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/invoice-project-row-detail-*/);
		    }).each(function(index, element){
		    	var projectVal = ARInvoiceDetail.addProjectAutoComplete(index);
		    	projectVal.settext(this.value);
		    });
			
			//
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(index, element){
				$('#' + this.id).change(function (event){
					var rowId = index;
					ARInvoiceDetail.calculateAmountDetail(index, event);
					
					//UPDATE ALERT APPROVAL STATUS
			    	var approvalNo = $('#approval-code-' + rowId).val();
			    	
			    	if(!isEmpty(approvalNo)){
			    		var approvalAmount = ARInvoiceDetail.approvalJson[approvalNo];
			    		ARInvoiceDetail.addAlertApprovalStatus(approvalNo, approvalAmount);
			    		ARInvoiceDetail.updateAlertApprovalStatus();
			    	}
				});
			});
			
			// Add Approval No Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/approval-code-*/);
		    }).each(function(index){
		    	var rowId = index;
		    	var approvalVal = ARInvoiceDetail.addApprovalAutoComplete(rowId);
		    	approvalVal.settext(this.value);
		    	
		    });
			
			//
			$('input').filter(function(){
				return this.id.match(/^exclude-gst-converted-amount-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
			    	var rowId = index;
			    	//$('#fx-rate-' + rowId).autoNumeric('set', 0);
			    	ARInvoiceDetail.calculateAmountDetail(rowId, event);
				});
			});
			
			//
			$('input').filter(function(){
				return this.id.match(/^fx-rate-*/);
			}).each(function(index){
				$('#' + this.id).change(function(event){
					var rowId = index;
					ARInvoiceDetail.calculateAmountDetail(rowId, event);
				})
			});
		    
	    	////////ADD AUTO COMPLETE FOR PAYEE////////
		    ARInvoiceDetail.thePayee = $('#payerName').tautocomplete({
		        highlight: "",
				columns : [
						label('cashflow.arInvoice.detail.code'),
						label('cashflow.arInvoice.detail.payeeAccount'),
						label('cashflow.arInvoice.detail.payerName'),
						label('cashflow.arInvoice.detail.address'),
						label('cashflow.arInvoice.detail.phone'),
						label('cashflow.arInvoice.detail.type'),
						label('cashflow.arInvoice.detail.typeName'),
						label('cashflow.arInvoice.detail.paymentTermCol'),
						label('cashflow.arInvoice.detail.accountCode'),
						label('cashflow.arInvoice.detail.accountName'), 
						label('cashflow.arInvoice.detail.defaultCurrency') 
					],
				hide: [false, true, true, true, true, false, false, false, false, false, false],
				addNewDataLink: '/admin/payeeMaster/list',
		        data: function(){
		        		var x = { keyword: ARInvoiceDetail.thePayee.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/payeeMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = ARInvoiceDetail.thePayee.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (result) {
		                	result.forEach(function(item){
		                		if(!item.address)
		                		{
		                			item.address = '';
		                		}
		                		if(!item.phone)
		                		{
		                			item.phone = '';
		                		}
		                	});
		                    return result;
		                }
		            },
		        delay: 100,
		        onchange: function () {
		        	var selectedData = ARInvoiceDetail.thePayee.all();
		        	$('#payerTypeName').val(selectedData[label('cashflow.arInvoice.detail.typeName')]);
		        	$('#payerTypeCode').val(selectedData[label('cashflow.arInvoice.detail.type')]);
		        	$('#payerCode').val(selectedData[label('cashflow.arInvoice.detail.code')]);
		        	$('#payerName').val(selectedData[label('cashflow.arInvoice.detail.payerName')]);
		        	
		        	// auto fill AR code, AR name and currency
		        	$('#accountReceivableName').val(selectedData[label('cashflow.arInvoice.detail.accountName')]);
					$('#accountReceivableCode').parent().find('input[autocomplete="off"]').val(selectedData[label('cashflow.arInvoice.detail.accountCode')]);
					$('#accountReceivableCode').val(selectedData[label('cashflow.arInvoice.detail.accountCode')]);
					$('#originalCurrencyCode').val(selectedData[label('cashflow.arInvoice.detail.defaultCurrency')]);
					accountPayableVal.settext($('#accountReceivableCode').val());

		        }
			});
		    ////////END ADD AUTO COMPLETE FOR PAYEE////////
		    ARInvoiceDetail.thePayee.settext($('#payerName').val());
		    
	    	////////ADD AUTO COMPLETE FOR ACCOUNTPAYABLE////////
			var accountPayableVal = $('#accountReceivableCode').tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns : [
						label('cashflow.arInvoice.detail.accountCode'),
						label('cashflow.arInvoice.detail.parentCode'),
						label('cashflow.arInvoice.detail.accountName'),
						label('cashflow.arInvoice.detail.type') 
						],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountPayableVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountPayableVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountPayableVal.all();
		        	$('#accountReceivableCode').val(selectedData[label('cashflow.arInvoice.detail.accountCode')]);
		        	$('#accountReceivableName').val(selectedData[label('cashflow.arInvoice.detail.accountName')]);
		        }
			});
			////////END ADD AUTO COMPLETE FOR ACCOUNTPAYABLE////////
			accountPayableVal.settext($('#accountReceivableCode').val());
			
			////////CREATE TABLE LIST INVOICE DETAIL////////
			ARInvoiceDetail.dtDetail = $('#detail-invoice-table').DataTable({
		         "paging":   false,
		         "ordering": false,
		         "info":     false,
		         "filter":     false,
		         "autoWidth": false,
		         "scrollY": 200,
			     "scrollX": true,
			     "drawCallback": function( settings ) {
			    	 $('.detail-invoice-table-wrapper').css('width','100%');
			      }
		    });
			
			//ADD NEW ROW FOR TABLE WHEN ADD BUTTON IS CLICK
		    $('#add-row-detail-invoice-btn').click(function () {
		    	var dataListHTML = "<datalist id='listGstType" + counter + "'>" + $('#gst-master-select-hidden').html() + "</datalist>"
		    	ARInvoiceDetail.dtDetail.row.add( [
		            '<input type="text" name="detailArForms['+counter+'].accountCode" id="invoice-account-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].accountName" id="invoice-account-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].classCode" id="invoice-project-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].className" id="invoice-project-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].approvalCode" id="approval-code-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].description" id="description-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon inv-currency"></span><input type="text" name="detailArForms['+counter+'].excludeGstOriginalAmount" id="exclude-gst-original-amount-'+ counter +'\" class="form-control"></div>',
		            '<input type="text" name="detailArForms['+counter+'].fxRate" id="fx-rate-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].excludeGstConvertedAmount" id="exclude-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
					'<select name="detailArForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type-input" list="listGstType' + counter +'">' + $('#gst-master-select-hidden').html() + '</select>',
		            '<input type="text" name="detailArForms['+counter+'].gstRate" id="gst-rate-'+ counter +'\" class="form-control">',
		            
		            '<input type="hidden" name="detailArForms['+counter+'].gstOriginalAmount" id="gst-original-amount-'+ counter +'\">' +
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].gstConvertedAmount" id="gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            
		            '<input type="hidden" name="detailArForms['+counter+'].includeGstOriginalAmount" id="include-gst-original-amount-'+ counter +'\">'+
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].includeGstConvertedAmount" id="include-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="ap-status apr-' + $('#apr002Value').attr('code') + ' ap-detail">' + $('#apr002Value').val() + '</div>',
		            '<span id="action-del-'+counter+'" class="action-del glyphicon glyphicon-trash"></span>',
		        ] ).draw( false );
		    	
		    	//auto add value to row
		        $('#fx-rate-' + counter).val($('#fxRate').val());
		        $('#gst-type-' + counter).val($('#gstType').find(":selected").text());
		        $('#gst-rate-' + counter).val($('#gstRate').val());
		        $('#description-' + counter).val($('#description').val());
		        
		        //Enable button copy
		        $('#copy-row-detail-invoice-btn').attr('disabled', false);
		 
		        ARInvoiceDetail.addProjectAutoComplete(counter);
		        ARInvoiceDetail.addAccountAutoComplete(counter);
		        ARInvoiceDetail.addApprovalAutoComplete(counter);
		        ARInvoiceDetail.addCalculationAction(counter);
		        ARInvoiceDetail.addCommasNumber();
		        ARInvoiceDetail.setDefaultGstType(counter);
		        counter++;

		    } );//---------------------------------------------------------
		    
		    //COPY TO NEW ROW FOR TABLE
		    $('#copy-row-detail-invoice-btn').click(function () {
		    	
		    	counter = ARInvoiceDetail.dtDetail.data().length;
		    	
		    	ARInvoiceDetail.dtDetail.row.add( [
		            '<input type="text" name="detailArForms['+counter+'].accountCode" id="invoice-account-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].accountName" id="invoice-account-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].classCode" id="invoice-project-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].className" id="invoice-project-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].approvalCode" id="approval-code-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailArForms['+counter+'].description" id="description-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon inv-currency"></span><input type="text" name="detailArForms['+counter+'].excludeGstOriginalAmount" id="exclude-gst-original-amount-'+ counter +'\" class="form-control"></div>',
		            '<input type="text" name="detailArForms['+counter+'].fxRate" id="fx-rate-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].excludeGstConvertedAmount" id="exclude-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
					'<select name="detailArForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type-input" list="listGstType' + counter +'">' + $('#gst-master-select-hidden').html() + '</select>',
		            '<input type="text" name="detailArForms['+counter+'].gstRate" id="gst-rate-'+ counter +'\" class="form-control">',
		            
		            '<input type="hidden" name="detailArForms['+counter+'].gstOriginalAmount" id="gst-original-amount-'+ counter +'\" >' +
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].gstConvertedAmount" id="gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            
		            '<input type="hidden" name="detailArForms['+counter+'].includeGstOriginalAmount" id="include-gst-original-amount-'+ counter +'\">' + 
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailArForms['+counter+'].includeGstConvertedAmount" id="include-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="ap-status apr-' + $('#apr002Value').attr('code') + ' ap-detail">' + $('#apr002Value').val() + '</div>',
		            '<span id="action-del-'+counter+'" class="action-del glyphicon glyphicon-trash"></span>',
		        ] ).draw( false );
		    	
		    	//auto add value to row
		        $('#fx-rate-' + counter).val($('#fxRate').val());
		        $('#description-' + counter).val($('#description').val());
		        ARInvoiceDetail.addCommasNumber();
		        
		        //Copy value
		        $('#invoice-account-row-detail-' + (counter)).val($('#invoice-account-row-detail-' + (counter - 1)).val());
		        $('#invoice-account-name-row-detail-' + (counter)).val($('#invoice-account-name-row-detail-' + (counter - 1)).val());
		        $('#invoice-project-row-detail-' + (counter)).val($('#invoice-project-row-detail-' + (counter - 1)).val());
		        $('#invoice-project-name-row-detail-' + (counter)).val($('#invoice-project-name-row-detail-' + (counter - 1)).val());
		        $('#po-no-' + (counter)).val($('#po-no-' + (counter - 1)).val());
		        $('#approval-code-' + (counter)).val($('#approval-code-' + (counter - 1)).val());
		        $('#exclude-gst-original-amount-' + (counter)).val($('#exclude-gst-original-amount-' + (counter - 1)).val());
		        $('#fx-rate-' + (counter)).val($('#fx-rate-' + (counter - 1)).val());
		        $('#exclude-gst-converted-amount-' + (counter)).val($('#exclude-gst-converted-amount-' + (counter - 1)).val());
		        $('#gst-type-' + (counter)).val($('#gst-type-' + (counter - 1)).val());
		        $('#gst-rate-' + (counter)).val($('#gst-rate-' + (counter - 1)).val());
		        $('#gst-original-amount-' + (counter)).val($('#gst-original-amount-' + (counter - 1)).val());
		        $('#gst-converted-amount-' + (counter)).val($('#gst-converted-amount-' + (counter - 1)).val());
		        $('#include-gst-original-amount-' + (counter)).val($('#include-gst-original-amount-' + (counter - 1)).val());
		        $('#include-gst-converted-amount-' + (counter)).val($('#include-gst-converted-amount-' + (counter - 1)).val());
		 
		        var accountVal = ARInvoiceDetail.addAccountAutoComplete(counter);
		        accountVal.settext($('#invoice-account-row-detail-' + (counter - 1)).val());
		        
		        var projectVal = ARInvoiceDetail.addProjectAutoComplete(counter);
		        projectVal.settext($('#invoice-project-row-detail-' + (counter - 1)).val());
		        
		        var approvalVal = ARInvoiceDetail.addApprovalAutoComplete(counter);
		        approvalVal.settext($('#approval-code-' + (counter - 1)).val());
		        
		        //
		        ARInvoiceDetail.addCalculationAction(counter);
		        
		        $('#exclude-gst-original-amount-' + (counter - 1)).blur();
		        
		        counter++;

		    });//------------------------------------------------
			
		    //ACTION WHEN CLICK ON DELETE
		    $('body').on('click', '.action-del', function() {
		    	
		    	var theId = '#'+$(this).attr('id');
		    	
		    	var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
				var theArInvoiceId = $('#id').val();
				var theArInvoiceAccountDetailId = $('#arInvoiceAccountDetailId-' + rowId).val();
				var theArInvoiceClassDetailId = $('#arInvoiceClassDetailId-' + rowId).val();
		    	
				// Just row move row is not exist on database.
		    	if(theArInvoiceClassDetailId == undefined || theArInvoiceClassDetailId == ""){
		    		ARInvoiceDetail.calculateSubAmountSummarize(rowId);
		    		ARInvoiceDetail.dtDetail.row($(theId).parents('tr')).remove().draw(false);
	    			
	    			//disable copy button when have no row.
	    			var lengthTbl = ARInvoiceDetail.dtDetail.data().length;
	    			if(lengthTbl == 0){
	    				$('#copy-row-detail-invoice-btn').attr('disabled', true);
	    			}
	    			
	    			return;
	    		}
		    	
		    	//Check AR Invoice at least one detail. Prevent delete when have only one detail.
		    	var nDetail = 0;
		    	$('input').filter(function(){
					return this.id.match(/invoice-account-row-detail-*/);
				}).each(function(index, element){
					
			    	var theApiAccDetailId = $('#arInvoiceAccountDetailId-' + index).val();
			    	
			    	if(theApiAccDetailId != undefined && theApiAccDetailId.length > 0){
			    		nDetail++;
			    	}
				});
		    	
		    	if(nDetail == 1){
		    		$('.error-area').showAlertMessage([label('cashflow.arInvoice.detail.required.classDetail')]);
		    		return;
		    	}
		    	
		    	// show modal when delete row is exist on database.
		    	var confirmModal = $("#confirm-delete");
		    	confirmModal.modal('show');
		    	
		    	$(".btn-deleted").off('click').click(function () {
		    		confirmModal.modal("hide");
		    		
		    		if(theArInvoiceClassDetailId.length > 0) {
		    			
		    			$.ajax({
				            type : "POST",
				            url : CONTEXT_PATH + '/arinvoice/delete',
				            data: {
				            	arInvoiceId : theArInvoiceId,
				            	arInvoiceAccountDetailId : theArInvoiceAccountDetailId,
				            	arInvoiceClassDetailId : theArInvoiceClassDetailId
				            },
				            success : function(ajaxResult) {
				               
				               if(ajaxResult.STATUS == "SUCCESS") {
				            	   ARInvoiceDetail.dtDetail.row($(theId).parents('tr')).remove().draw(false);
				            	   
				            	   window.location.reload();
				            	   sessionStorage.setItem("editFlg", true);
				            	   
				               } else {
				            	   
				               }
				            }
				        }); // end ajax
		    		}
		    		
		        });
		    }); // end action delete
		    
		    ///--------------------------------------------------------
		    
		    //Display read only when page on mode edit.
			if(modeEdit){
				ARInvoiceDetail.setReadonlyInput(true);
			}
		    ARInvoiceDetail.defaultDisplay();
		    ARInvoiceDetail.setButtonWhenInit(modeEdit);
		    ARInvoiceDetail.updateCurrency();
	        ARInvoiceDetail.updateConvertedCurrency();
		},
		
		eventListener: function () {
			// update original currency
			$("#originalCurrencyCode").change(function () {
				ARInvoiceDetail.updateCurrency();
		    });
			
			// disable enter key press
			$('#form1').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					e.preventDefault();
					return false;
				}
			});
			
			$("#edit-btn").click(function(){
				ARInvoiceDetail.setReadonlyInput(false);
				ARInvoiceDetail.defaultDisplay();
				ARInvoiceDetail.setButtonWhenEdit();
			});
			//cancel btn
			$('#btn-cancel').click(function(e){
				e.preventDefault();
				//window.location = document.referrer;
				window.location = '/arinvoice/list';
			});
			
			$('#btn-reset').click(function(){
				if(!$(this).hasClass('disabled')){
					if(modeEdit || modeCopy){
						window.location.reload();
					}
					else {
						document.getElementById("form1").reset();
						$('.action-del').trigger('click');
					}
				}
			});
			
			//auto set account name	when change account code
			$(document).on('change','.gst-type-input', function(event){
		    	var rate = this.options[this.selectedIndex].getAttribute('rate');
		    	var rateField = $(this).closest('td').next().find('input');
		    	rateField.val(rate);
		    	
		    	//update input gst, amout relate.
		    	var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				ARInvoiceDetail.calculateAmountDetail(rowId, event);
				
				//UPDATE ALERT APPROVAL STATUS
		    	var approvalNo = $('#approval-code-' + rowId).val();
		    	
		    	if(!isEmpty(approvalNo)){
		    		var approvalAmount = ARInvoiceDetail.approvalJson[approvalNo];
		    		ARInvoiceDetail.addAlertApprovalStatus(approvalNo, approvalAmount);
		    		ARInvoiceDetail.updateAlertApprovalStatus();
		    	}
		    });
		},
		
		//update currency label Amount Exclude GST (original)
		updateCurrency : function(){
			$('#detail-invoice-table span.inv-currency, .exclude-gst-original-currency').html($('#originalCurrencyCode').val());
		},
		
		//update converted currency label
		updateConvertedCurrency : function(){
			$('#detail-invoice-table span.sub-currency').html($('#convertedCurrencyCode').val());
		},
		
		addAccountAutoComplete: function(idCounter){
			var elementId = 'invoice-account-row-detail-'+ idCounter;
		    var accountVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.arInvoice.detail.accountCode'),label('cashflow.arInvoice.detail.parentCode'), label('cashflow.arInvoice.detail.accountName'), label('cashflow.arInvoice.detail.type')],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountVal.all();
		        	var accountNameId = 'invoice-account-name-row-detail-'+ idCounter;
		        	accountVal.settext(selectedData[label('cashflow.arInvoice.detail.accountCode')]);
		        	$('#' + accountNameId).val(selectedData[label('cashflow.arInvoice.detail.accountName')]);
		        	$('#' + elementId).val(selectedData[label('cashflow.arInvoice.detail.accountCode')])
		        }
			});
		    return accountVal;
		},
		
		addProjectAutoComplete: function(idCounter){
			var elementId = 'invoice-project-row-detail-'+ idCounter;
		    var projectVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.arInvoice.detail.prjCode'), label('cashflow.arInvoice.detail.prjName')],
				hide: [true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/projectMaster/list',
		        data: function(){
		        		var x = { keyword: projectVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/projectMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = projectVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = projectVal.all();
		        	var projectNameId = 'invoice-project-name-row-detail-'+ idCounter;
		        	projectVal.settext(selectedData[label('cashflow.arInvoice.detail.prjCode')]);
		        	$('#'+ elementId).val(selectedData[label('cashflow.arInvoice.detail.prjCode')]);
		        	$('#'+ projectNameId).val(selectedData[label('cashflow.arInvoice.detail.prjName')]);
		        }
			});
		    return projectVal;
		},
		
		addApprovalAutoComplete: function(rowId){
			var elementId = 'approval-code-'+ rowId;

			var col1 = title('cashflow.arInvoice.detail.approvalNo');
			var col2 = title('cashflow.arInvoice.detail.amount');
			var col3 = title('cashflow.arInvoice.detail.approvalDate');
			var col4 = title('cashflow.arInvoice.detail.applicant');
			var col5 = title('cashflow.arInvoice.detail.purpose');
			
		    var approvalVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [col1, col2, col3, col4, col5],
				hide: [true, true, true, true, true],
				dataIndex: 0,
		        data: function(){
		        		var x = { keyword: approvalVal.searchdata(), issueDate: $("#invoiceIssuedDate").val()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/arinvoice/getListApprovalAvailable",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = approvalVal.searchdata();
		                		return keyword;
		                	},
		                	issueDate : function(){
		                		return $("#invoiceIssuedDate").val();
		                	}
		                },
		                success: function (ret) {
		                    return ret;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = approvalVal.all();
		        	approvalVal.settext(selectedData[col1]);
		        	$('#'+ elementId).val(selectedData[col1]);
		        	
		        	
		        	//UPDATE ALERT FOR APPROVAL STATUS
		        	var approvalNo = selectedData[col1];
		        	var approvalAmount = selectedData[col2];
		        	
		        	// add new alert approval status.
		        	ARInvoiceDetail.addAlertApprovalStatus(approvalNo, approvalAmount);
		        	
		        	//update alert approval status.
		        	ARInvoiceDetail.updateAlertApprovalStatus();

		        }
			});
		    return approvalVal;
		},
		
		addAlertApprovalStatus: function(approvalNo, approvalAmount){
			if($('#'+ approvalNo).val() == undefined){
        		var includeGstOriginalAmount = ARInvoiceDetail.getIncludeGstOriginalAmountBy(approvalNo);
        		
        		var text = ARInvoiceDetail.buildTextForApprovalStatus(approvalNo, $('#originalCurrencyCode').val(), 
																		includeGstOriginalAmount, approvalAmount);
        		
        		var html = '<li id="'+approvalNo+'" amount="'+approvalAmount+'">'+text+'</li>';
        		
        		$('#alertApprovalStatus').append(html);
        		
        		if(ARInvoiceDetail.approvalJson[approvalNo] == undefined){
        			ARInvoiceDetail.approvalJson[approvalNo] = approvalAmount;
        		}
        	}
		},
		
		updateAlertApprovalStatus: function(){
			$('#alertApprovalStatus').find('li').each(function(){
        		
    			var apNo = $(this).attr('id');
    			
    			//remove element alert not use.
    			if(ARInvoiceDetail.getListApprovalNoApplied().indexOf(apNo) < 0){
    				$(this).remove();
    				return;
    			}
    			
        		//
    			var includeGstOriginalAmount = ARInvoiceDetail.getIncludeGstOriginalAmountBy(apNo);
    			
				var text = ARInvoiceDetail.buildTextForApprovalStatus(apNo, $('#originalCurrencyCode').val(), 
																		includeGstOriginalAmount, $(this).attr('amount'));
	
				$('#'+ apNo).html(text);
    			
    		});
		},
		
		getListApprovalNoApplied: function(){
			var listApprovalNoApplied = [];
        	
        	$('input').filter(function(){
				return this.id.match(/approval-code-*/);
			}).each(function(){
				var apNo = $(this).val();
				
				//push apNo to list if not exist.
				if(listApprovalNoApplied.indexOf(apNo) < 0){
					listApprovalNoApplied.push(apNo);
				}
				
			});
        	
        	return listApprovalNoApplied;
		},
		
		getIncludeGstOriginalAmountBy: function(approvalNo){
			var includeGstOriginalAmount = getNewDecimal(0);
    		
    		$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(index){
				var apNo = $('#approval-code-'+ index).val();
				
				if(approvalNo == apNo){
					includeGstOriginalAmount = includeGstOriginalAmount.plus(getNewDecimal(Number($(this).autoNumeric('get'))));
				}
			});
    		
    		return includeGstOriginalAmount.valueOf();
		},
		
		buildTextForApprovalStatus: function(approvalNo, currency, applyAmount, amount){
			var span1 = '<span id="title">' + 'Total amount applied for Approval No. "' + approvalNo + '" is ' + '</span>';
			var span2 = '<span>' + currency + ' ' + applyAmount + ' / ' + amount  + '</span>';
			
			return span1 + span2;
		},
		
		addCommasNumber: function() {
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', { vMin: '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^fx-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			});
			
			
			$('#fxRate').autoNumeric('init', {vMin : '0', mDec: '10', aPad: false});
			$('#excludeGstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#excludeGstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			$('#gstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#gstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			$('#includeGstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#includeGstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			
			ARInvoiceDetail.updateCurrency();
			ARInvoiceDetail.updateConvertedCurrency();
			ARInvoiceDetail.disableInputClassDetail();
		},
		
		validationForm : function(){
			
			var messages = [];
			var valid = true;
			
			//payeeName
			if(isEmpty($('#payerName').parent().find('input[autocomplete="off"]').val())){
				valid = false;
				messages.push(validate.require(label('cashflow.arInvoice.detail.payerName')));
			}
			
			//issue date
			if(isEmpty($('#invoiceIssuedDate').val())){
				valid = false;
				messages.push(validate.require(label('cashflow.arInvoice.detail.invoiceIssuedDate')));
			}
			
			//fxRate
			if(isEmpty($('#fxRate').val())){
				valid = false;
				messages.push(validate.require(label('cashflow.arInvoice.detail.fxRate')));
			}

 			// invoiceNo
			var invoiceNo = $('#invoiceNo').val();
	 		if(!/^(.{0,40})$/.test(invoiceNo)){
	 			messages.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.arInvoice.detail.invoiceNo'], '40'));
				valid = false;
			}
			
			$('input').filter(function(){
				return this.id.match(/invoice-account-row-detail-*/);
			}).each(function(){
				if($(this).val().trim().length == 0)
				{
					valid = false;
					messages.push(validate.require(label('cashflow.arInvoice.detail.accCode')));
				}
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-project-row-detail-*/);
			}).each(function(){
				if($(this).val().trim().length == 0)
				{
					valid = false;
					messages.push(validate.require(label('cashflow.arInvoice.detail.prjCode')));
				}
			});
			
			//check a invoice should have at least 1 detail.
			if(ARInvoiceDetail.dtDetail.data().length == 0){
				valid = false;
				messages.push(label('cashflow.arInvoice.detail.arInvoiceDetail'));
			}
			
			// When data is valid, then submit form
			if (valid) {

				$('#isConfirmChanged').val(false);

				ARInvoiceDetail.reFormatNumberic();

				$('#form1').submit();

			} else {
				$('.error-area').showAlertMessage(messages);
			}
			
		},
		
		reFormatNumberic : function(){
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^fx-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('#fxRate').val(Number($('#fxRate').autoNumeric('get')));
			$('#excludeGstOriginalAmount').val(Number($('#excludeGstOriginalAmount').autoNumeric('get')));
			$('#excludeGstConvertedAmount').val(Number($('#excludeGstConvertedAmount').autoNumeric('get')));
			$('#gstOriginalAmount').val(Number($('#gstOriginalAmount').autoNumeric('get')));
			$('#gstConvertedAmount').val(Number($('#gstConvertedAmount').autoNumeric('get')));
			$('#includeGstOriginalAmount').val(Number($('#includeGstOriginalAmount').autoNumeric('get')));
			$('#includeGstConvertedAmount').val(Number($('#includeGstConvertedAmount').autoNumeric('get')));
		},
		
		setButtonWhenInit: function(modeEdit){
			if(modeEdit){
				$('#save-btn').addClass('disabled');
				$('#btn-cancel').addClass('disabled');
				$('#btn-reset').addClass('disabled');
			}else{
				$('#copy-btn').addClass('disabled');
				$('#save-btn').removeClass('disabled');
				$('#edit-btn').addClass('disabled');
			}
			
			$('#add-row-detail-invoice-btn').attr('disabled', modeEdit);
			
			//Disable copy button when request not from copy. 
			if(window.location.href.indexOf('copy') == -1){
				$('#copy-row-detail-invoice-btn').attr('disabled', true);
			}
			
			$('.action-del').css("display", (modeEdit ? 'none' : 'inline'));
			
		}, // end setButtonWhenInit
		
		setButtonWhenEdit: function(){
			$('#edit-btn').attr('disabled', true);
			$('#save-btn').removeClass('disabled');
			
			$('#btn-cancel').removeClass('disabled');
			$('#btn-reset').removeClass('disabled');
		}, // end setButtonWhenEdit
		
		setReadonlyInput: function(isReadonly){
			$('#form1 input[type=text]').attr('readonly', isReadonly);
			$('#form1 select').attr('readonly', isReadonly);
			$('#form1 textarea').attr('readonly', isReadonly);
			
			if(isReadonly){
				$('#form1 input[readonly=readonly]').addClass('disabled-input');
				$('#form1 select').addClass('disabled-input');
			}else{
				$('#form1 input[type=text]').removeClass('disabled-input');
				$('#form1 select').removeClass('disabled-input');
			}
			
			// Allow update when AR Invoice have status Received (001), PARTIAL Received(002)
			let enabled = false;
			if(isReadonly == false){
				let statusCode = $('#status').val();
				if(statusCode == '001' || statusCode == '003'){
					enabled = true;
					$('#originalCurrencyCode').attr('readonly', true);
					$('#originalCurrencyCode').addClass('disabled-input');
					
					$('#fxRate').attr('readonly', true);
					$('#fxRate').addClass('disabled-input');
					
					$('.detail-area input[type=text]').attr('readonly', true);
					$('.detail-area select').attr('readonly', true);
					
					$('.detail-area input[readonly=readonly]').addClass('disabled-input');
					$('.detail-area select').addClass('disabled-input');
				}
			}
			$('.action-del').css("display", (enabled ? 'none' : 'inline'));
			$('#add-row-detail-invoice-btn').attr('disabled', enabled);
			$('#copy-row-detail-invoice-btn').attr('disabled', enabled);
			
			$('input').filter(function(){
				return this.id.match(/invoice-account-row-detail-*/);
			}).each(function(){
				$(this).next().next().attr('readonly', true);
			});
		}, // end setReadonlyInput
		
		defaultDisplay: function(){
			$('#arInvoiceNo').attr('readonly',true);
			$('#excludeGstOriginalAmount').attr('readonly',true);
			$('#excludeGstConvertedAmount').attr('readonly',true);
			$('#gstOriginalAmount').attr('readonly',true);
			$('#gstConvertedAmount').attr('readonly',true);
			$('#includeGstOriginalAmount').attr('readonly',true);
			$('#includeGstConvertedAmount').attr('readonly',true);
			$('#accountReceivableName').attr('readonly',true);
			
			ARInvoiceDetail.disableInputClassDetail();
		}, // end defaultDisplay
		
		disableInputClassDetail : function(){
			$('input').filter(function(){
				return this.id.match(/^gst-rate-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-account-name-row-detail-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-project-name-row-detail-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
		},
		
		addCalculationAction: function(rowId){
			$('#exclude-gst-original-amount-' + rowId + ', #gst-rate-' + rowId + ', #fx-rate-' + rowId +', #exclude-gst-converted-amount-' + rowId).change(function(event){
				ARInvoiceDetail.calculateAmountDetail(rowId, event);
				
				//UPDATE ALERT APPROVAL STATUS
		    	var approvalNo = $('#approval-code-' + rowId).val();
		    	
		    	if(!isEmpty(approvalNo)){
		    		var approvalAmount = ARInvoiceDetail.approvalJson[approvalNo];
		    		ARInvoiceDetail.addAlertApprovalStatus(approvalNo, approvalAmount);
		    		ARInvoiceDetail.updateAlertApprovalStatus();
		    	}
			});
		},
		
		calculateAmountDetail: function(rowId, event){
			
			let targetId = event.target.id;
			
			var gstRate =  getNewDecimal(Number($('#gst-rate-' + rowId).autoNumeric('get')));
			var fxRate = getNewDecimal(Number($('#fx-rate-' + rowId).autoNumeric('get')));
			var exclCvrtAmount = getNewDecimal(Number($('#exclude-gst-converted-amount-' + rowId).autoNumeric('get')));
			var exclGstOriginalAmount =  getNewDecimal(Number($('#exclude-gst-original-amount-' + rowId).autoNumeric('get')));
				
			if (targetId.indexOf('exclude-gst-original-amount-') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0 && fxRate.toNumber() != 0){
					exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
				}
				
			} else if (targetId.indexOf('fx-rate-') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0){
					exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
				}
				
			} else if (targetId.indexOf('exclude-gst-converted-amount-') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0){
					fxRate = exclCvrtAmount.dividedBy(exclGstOriginalAmount);
				}
			}
			
			
			var gstOriginalAmount = exclGstOriginalAmount.mul(gstRate.div(getNewDecimal(100)));
			var gstCvrtAmount = gstOriginalAmount.mul(fxRate);
			var inclOriginalAmount = exclGstOriginalAmount.plus(gstOriginalAmount);
			var inclCvrtAmount = exclCvrtAmount.plus(gstCvrtAmount);
			
			$('#fx-rate-' + rowId).autoNumeric('set', fxRate.toNumber());
			$('#exclude-gst-converted-amount-' + rowId).autoNumeric('set', exclCvrtAmount.valueOf());
			$('#gst-original-amount-' + rowId).autoNumeric('set', gstOriginalAmount.valueOf());
			$('#gst-converted-amount-' + rowId).autoNumeric('set', gstCvrtAmount.valueOf());
			$('#include-gst-original-amount-' + rowId).autoNumeric('set', inclOriginalAmount.valueOf());
			$('#include-gst-converted-amount-' + rowId).autoNumeric('set', inclCvrtAmount.valueOf());

			// Summarize
			var exclGstOrginAmountSum = getNewDecimal(0);
			var exclGstCvrtAmountSum = getNewDecimal(0);
			var gstOriginAmountSum = getNewDecimal(0);
			var gstCvrtAmountSum = getNewDecimal(0);
			var inclGstOriginAmountSum =getNewDecimal(0);
			var inclGstCvrtAmountSum = getNewDecimal(0);
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount*/);
			}).each(function(){
				exclGstOrginAmountSum = exclGstOrginAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				exclGstCvrtAmountSum = exclGstCvrtAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				gstCvrtAmountSum = gstCvrtAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				gstOriginAmountSum = gstOriginAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				inclGstCvrtAmountSum = inclGstCvrtAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				inclGstOriginAmountSum = inclGstOriginAmountSum.plus(getNewDecimal(Number($('#'+this.id).autoNumeric('get'))));
			});
			
			// Fill Summarized Values
			$('#excludeGstOriginalAmount').autoNumeric('set', exclGstOrginAmountSum.valueOf());
			$('#excludeGstConvertedAmount').autoNumeric('set', exclGstCvrtAmountSum.valueOf());
			$('#includeGstOriginalAmount').autoNumeric('set', inclGstOriginAmountSum.valueOf());
			$('#gstConvertedAmount').autoNumeric('set', gstCvrtAmountSum.valueOf());
			$('#gstOriginalAmount').autoNumeric('set', gstOriginAmountSum.valueOf());
			$('#includeGstConvertedAmount').autoNumeric('set', inclGstCvrtAmountSum.valueOf());
		},
		
		calculateSubAmountSummarize : function(rowId){
			var fxRate = getNewDecimal(Number($('#fx-rate-' + rowId).autoNumeric('get')));
			// Summarize
			var exclGstOrginAmountSum = getNewDecimal(Number($('#excludeGstOriginalAmount').autoNumeric('get')));
			var exclGstCvrtAmountSum = getNewDecimal(Number($('#excludeGstConvertedAmount').autoNumeric('get')));
			var gstOriginAmountSum = getNewDecimal(Number($('#gstOriginalAmount').autoNumeric('get')));
			var gstCvrtAmountSum = getNewDecimal(Number($('#gstConvertedAmount').autoNumeric('get')));
			var inclGstOriginAmountSum = getNewDecimal(Number($('#includeGstOriginalAmount').autoNumeric('get')));
			var inclGstCvrtAmountSum = getNewDecimal(Number($('#includeGstConvertedAmount').autoNumeric('get')));
			
			exclGstOrginAmountSum = exclGstOrginAmountSum.minus(getNewDecimal(Number($('#exclude-gst-original-amount-'+rowId).autoNumeric('get'))));
			exclGstCvrtAmountSum = exclGstCvrtAmountSum.minus(getNewDecimal(Number($('#exclude-gst-converted-amount-'+rowId).autoNumeric('get'))));
			
			inclGstCvrtAmountSum = inclGstCvrtAmountSum.minus(getNewDecimal(Number($('#include-gst-converted-amount-'+rowId).autoNumeric('get'))));
			inclGstOriginAmountSum = inclGstOriginAmountSum.minus(getNewDecimal(Number($('#include-gst-original-amount-'+rowId).autoNumeric('get'))));

			gstOriginAmountSum = gstOriginAmountSum.minus(getNewDecimal(Number($('#gst-original-amount-'+rowId).autoNumeric('get'))));
			gstCvrtAmountSum = gstCvrtAmountSum.minus(getNewDecimal(Number($('#gst-converted-amount-'+rowId).autoNumeric('get'))));
			
			// Fill Summarized Values
			$('#excludeGstOriginalAmount').autoNumeric('set', exclGstOrginAmountSum.valueOf());
			$('#excludeGstConvertedAmount').autoNumeric('set', exclGstCvrtAmountSum.valueOf());
			
			$('#gstOriginalAmount').autoNumeric('set', gstOriginAmountSum.valueOf());
			$('#gstConvertedAmount').autoNumeric('set', gstCvrtAmountSum.valueOf());
			
			$('#includeGstOriginalAmount').autoNumeric('set', inclGstOriginAmountSum.valueOf());
			$('#includeGstConvertedAmount').autoNumeric('set', inclGstCvrtAmountSum.valueOf());
		},
		
		setDefaultGstType : function(rowId){
			$('select#gst-type-'+rowId+' option[value="NIL"]').prop("selected",true);
			$('#gst-rate-'+rowId).autoNumeric('set', 0);
		}
		
		
	};
}());