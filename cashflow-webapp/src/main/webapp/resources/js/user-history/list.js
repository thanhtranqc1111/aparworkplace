(function() {
    UserHistory = {

        init : function() {
            $('.datepicker input').datepicker({
                format : "dd/mm/yyyy",
                orientation : 'auto bottom'
            });

            UserHistory.firstName = $('#firstName').val();
            UserHistory.lastName = $('#lastName').val();
            UserHistory.email = $('#email').val();
            UserHistory.module = $('#module').val();
            UserHistory.tenantCode = $('#tenantCode').val().trim();
            UserHistory.fromMonth = $('#fromMonth').val();
            UserHistory.toMonth = $('#toMonth').val();

            UserHistory.dataTable = $('#table_data_user_history').removeAttr('width').DataTable(
                    {
                        "ajax" : {
                            url : "/userHistory/getAllPagingUserHistory",
                            type : "GET",
                            data : function(d) {
                                d.firstName = UserHistory.firstName;
                                d.lastName = UserHistory.lastName;
                                d.email = UserHistory.email;
                                d.module = UserHistory.module;
                                d.action = UserHistory.action;
                                d.actionDate = UserHistory.actionDate;
                                d.tenantCode = UserHistory.tenantCode;
                                d.fromMonth = UserHistory.fromMonth;
                                d.toMonth = UserHistory.toMonth;
                            },
                            error : function() { // error handling
                                $(".table_data-error").html("");
                                $("#table_data_user_history").append(
                                        '<tbody class="table_data-error align-center"><tr><td colspan="9">' + GLOBAL_MESSAGES['cashflow.common.noData']
                                                + '</td></tr></tbody>');
                                $("#table_data_processing").css("display", "none");
                            },
                            complete : function(data) {
                                hideAjaxStatus();

                                // insert data for Index(#) column
                                UserHistory.dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                    cell.innerHTML = UserHistory.dataTable.page.info().start + i + 1;
                                } );
                            }
                        },
                        columns : [ {
                            "data" : "id",
                            "orderable" : false,
                            "searchable": false,
                            "render" : function(data, type, full, meta) {
                                if (full.id != null && full.id != undefined) {
                                    return "<span id='" + full.id + "' ></span>";
                                }
                            }
                        },
                        {
                            "data" : "firstName",
                            "orderable" : true,
                            "searchable": false
                        },
                        {
                            "data" : "lastName",
                            "orderable" : true,
                            "searchable": false
                        },
                        {
                            "data" : "email",
                            "orderable" : true,
                            "width" : "15%",
                            "searchable": false
                        },
                        {
                            "data" : "module",
                            "orderable" : true,
                            "width" : "10%",
                            "searchable": false
                        },
                        {
                            "data" : "action",
                            "orderable" : true,
                            "width" : "30%",
                            "searchable": false
                        },
                        {
                            "data" : "tenantCode",
                            "orderable" : true,
                            "width" : "8%",
                            "searchable": false
                        }, 
                        {
                            "data" : "tenantName",
                            "orderable" : true,
                            "searchable": false
                        }, 
                        {
                            "data" : "actionDate",
                            "orderable" : true,
                            "searchable": false,
                            "render" : function(data, type, full, meta) {
                                if (full.actionDate != null && full.actionDate != undefined) {
                                    return "<span class='right' >" + UserHistory.formatDateTime(full.actionDate) + "</span>";
                                }
                            }
                        } ],
                        searching : false,
                        "lengthChange" : true,
                        'order' : [ [ 8, 'desc' ] ],
                        "columnDefs" : [ {
                            "searchable" : false,
                            "orderable" : false,
                            "targets" : 0
                        } ]
                    });

        }, // end init

        // dd/MM/yyyy HH:mm:ss format
        formatDateTime: function(date) {
            var day = UserHistory.addPrefix0IfStringHasOnlyCharacter(date.dayOfMonth);
            var month = UserHistory.addPrefix0IfStringHasOnlyCharacter(date.month + 1);
            var hour = UserHistory.addPrefix0IfStringHasOnlyCharacter(date.hourOfDay);
            var minute = UserHistory.addPrefix0IfStringHasOnlyCharacter(date.minute);
            var second = UserHistory.addPrefix0IfStringHasOnlyCharacter(date.second);

            return day + "/"+ month + "/" + date.year + " " + hour + ":" + minute + ":" + second;
        },

        // if string is one character then add '0' prefix 
        addPrefix0IfStringHasOnlyCharacter: function(param) {
            return (param.toString().length === 1) ? ("0" + param) : param;
        },

        eventListener : function() {
             $(document).on("click","#btn-search-user-history",function(){
                 UserHistory.firstName = $('#firstName').val();
                 UserHistory.lastName = $('#lastName').val();
                 UserHistory.email = $('#email').val();
                 UserHistory.module = $('#module').val();
                 UserHistory.tenantCode = $('#tenantCode').val().trim();
                 UserHistory.fromMonth = $('#fromMonth').val();
                 UserHistory.toMonth = $('#toMonth').val();

                 UserHistory.dataTable.ajax.reload();
             });
        }, // end eventListener
    };
}());