/**
 * @author ThoaiNH 
 * create Dec 05, 2017
 */
(function () {
	receiptOrder = {
		dataTable: null, //main data table
		dataTableARInvoice: null,
		transactionNumber: 0,
		receiptOrderNo: null,
		listArInvoiceData: [],
		bankStatementId: 0,
		init: function () {
			bankStatement.receiptOrderNo = $('#receiptOrderNo').val();
			$('.ap-big-decimal').val(0);
			
			setupInputNumberMask();
			
			receiptOrder.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/receiptOrder/api/getAllPagingReceiptOrder",
		            type: "GET",
		            data: function ( d ) {
		            	d.bankName = $('#search-bank-statementType').val();
		                d.bankAccNo = $('#search-bank-statementAccount').val();
		                d.transactionFrom = $('#search-transactionFrom').val();
		                d.transactionTo = $('#search-transactionTo').val();
		                d.transactionNumber = receiptOrder.transactionNumber;
		                d.creditOption = $('#searh-credit-option').val();
		                d.status = $('#search-bank-status').val();
		                d.credit = $('#searh-credit').val();
		                d.payerName = $('#ap-receipt-order-matching #payername').val();
		                d.receiptOrderNo = bankStatement.receiptOrderNo;
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	console.log(receiptOrder.listArInvoiceData);
						hideAjaxStatus();
						receiptOrder.mergeRows('#table_data', 1, 6);
						receiptOrder.mergeRows('#table_data', 7, 15);
		            }
		        },
		        columns: [
			        {
			            "data" : "bankStatementId",
			            "width" : "6%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return "<a href='/bankStatement/list/" + data +"' class='bank-statment-id'>" + data + "</a>";
			             }
			        }, 
			        {
			            "data" : "transactionDate",
			            "orderable": true,
			            "width" : "8%",
			            "render": function(data, type,full, meta) {
			            	return full.transactionDate.dayOfMonth + "/" + (full.transactionDate.month + 1) + "/" + full.transactionDate.year;
			             }
			        }, 
			        {
			            "data" : "valueDate",
			            "orderable": true,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(full.valueDate)
			            		return full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year;
			            	else
			            		return "";
			             }	
			        },
			        {
			            "data" : "currencyCode",
			            "orderable": false,
			            "width" : "4%"
			        },
			        {
			            "data" : "credit",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(full.arInvoiceNo)
			            	{
			            		return '<div class="status-matched">' + GLOBAL_MESSAGES['cashflow.receiptOrder.matched'] + '</div>';
			            	}
			            	else
			            	{
			            		if(GLOBAL_PERMISSION["PMS-003"])
				            	{
			            			return '<button credit=' + full.credit + ' type="button" class="btn btn-link btn-matching">' + GLOBAL_MESSAGES['cashflow.receiptOrder.matching'] + '</button>';
				            	}
			            		else {
			            			return '<button credit=' + full.credit + ' type="button" class="btn btn-link">' + GLOBAL_MESSAGES['cashflow.receiptOrder.matching'] + '</button>';
			            		}
			            	}
			            }
			        },
			        {
			            "data" : "arInvoiceNo",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return "<a href='/arinvoice/detail/" + data +"' >" + data + "</a>";
			            	else
			            		return "";
			             }
			        },
			        {
			            "data" : "month",
			            "orderable": false,
			            "width" : "4%",
			            "render": function(data, type,full, meta) {
			            	if(full.month)
			            		return full.month.dayOfMonth + "/" + (full.month.month + 1) + "/" + full.month.year;
			            	else
			            		return "";
			             }	
			        },
			        {
			            "data" : "payerName",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data != undefined && data != null)
			            		return data;
			            	else
			            		return "";
			            }
			        },
			        {
			            "data" : "invoiceNo",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data != undefined && data != null)
			            		return data;
			            	else
			            		return "";
			            }
			        },
			        {
			            "data" : "totalAmount",
			            "orderable": false,
			            "width" : "6%",
			             "render": function(data, type,full, meta) {
			            	 if(data)
			            	 	return "<span class='pull-left'>" + full.originalCurrencyCode + "</span>" + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            	 else
			            		return "";
			             }
			        },
			        {
			            "data" : "totalAmountConverted",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return "<span class='pull-left'>" + $('#convertedCurrencyCode').val() + "</span>" + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            	else
			            		return "";
			            }
			        },
			        {
			            "data" : "remainAmount",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data === undefined)
			            		return "";
			            	else
			            		return "<span class='pull-left'>" + full.originalCurrencyCode + "</span>" + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "varianceAmount",
			            "orderable": false,
			            "width" : "6%",
			            "render": function(data, type,full, meta) {
			            	if(data)
			            		return formatStringAsNumber(data);
			            	else
			            		return "";
			            }
			        },
			        {
			            "data" : "arStatus",
			            "width" : "8%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.arStatus != undefined && full.arStatus != null){
			            		return "<div class='ap-status  ari-" + full.arStatus + "'>" + full.arStatusValue + "</div>";
			            	}
			            	else
			            		return "";
			            }
			        },
		        ],
		        searching: false,
		        "order": [[ 0, "desc" ]],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.receiptOrder.list.all']]],
				"lengthChange" : true,
		        "columnDefs": [
		        	{
		                "targets": [0],
		                "className": "text-center",
		            },
		            {
		                "targets": [1,2,4,7,10,11,12,13],
		                "className": "text-right",
		            }
		        ],
			 });
			
			 //setup date picker
			 $('.ap-date-picker').datepicker({
		    	format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
		        todayHighlight: true
		     });
			 
			 $('.ap-date-picker-month-format').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom'
			});
			
			//setup auto complete
			var payerNameAutocomplete = $('#ap-receipt-order-matching #payername').tautocomplete({
			        highlight: "",
					columns : [
							label('cashflow.receiptOrder.code'),
							label('cashflow.receiptOrder.payeeAccount'),
							label('cashflow.receiptOrder.payeeName'),
							label('cashflow.receiptOrder.address'),
							label('cashflow.receiptOrder.phone'),
							label('cashflow.receiptOrder.type'),
							label('cashflow.receiptOrder.typeName'),
							label('cashflow.receiptOrder.paymentTermCol'),
							label('cashflow.receiptOrder.accountCode'),
							label('cashflow.receiptOrder.accountName') 
						],
					hide: [false, true, true, true, true, false, false, false, false, false],
					addNewDataLink: '/admin/payeeMaster/list',
			        data: function(){
		        		var x = { keyword: payerNameAutocomplete.searchdata()}; 
		        		return x;
			        },
			        ajax: {
			                url: "/manage/payeeMasters",
			                type: "GET",
			                data: {
			                	keyword: function(){
			                		var keyword = payerNameAutocomplete.searchdata();
			                		return keyword;
			                	}
			                },
			                success: function (result) {
			                	result.forEach(function(item){
			                		if(!item.address)
			                		{
			                			item.address = '';
			                		}
			                		if(!item.phone)
			                		{
			                			item.phone = '';
			                		}
			                	});
			                    return result;
			                }
			            },
			        delay: 100,
			        onchange: function () {
			        	var selectedData = payerNameAutocomplete.all();
			        	$('#ap-receipt-order-matching #payername').val(selectedData[label('cashflow.receiptOrder.payeeName')]);
			        }
				});
		},
		eventListener: function () {
			 //on click search
			 $(document).on("click",".btn-search",function(){
				receiptOrder.receiptOrderNo = null;
		    	receiptOrder.dataTable.ajax.reload();
		     });
			 
			//on change bank name
		    $("#search-bank-statementType").on('change', function() {
		    	var bankCode = this.options[this.selectedIndex].getAttribute('bankCode');
		    	//bankStatement.bankName = this.options[this.selectedIndex].getAttribute('value');
		    	bankStatement.getBankAccountMasterByBankCode(bankCode, function(ajaxResult){
		    		   var resultData = JSON.parse(ajaxResult);
		    		   $("#search-currency").val("");
				       $("#search-beginning-balance").val("");
				       $("#search-ending-balance").val("");
		               $("#search-bank-statementAccount").html("");
		               bankStatement.endingBalance = null;
		               bankStatement.bankAccountNo = "";
		               if(resultData.STATUS == "SUCCESS") {
		            	   //console.log(resultData);
		            	   if(resultData.RESULT.length > 0){
		            		   var htmlString = "<option value=''>" + label('cashflow.receiptOrder.all') + "</option>";
		            		   resultData.RESULT.forEach(function(item){
			            		   htmlString = htmlString + ("<option currencyCode='" + item.currencyCode + "' beginningBalance='" + (item.beginningBalance?item.beginningBalance:'') + 
			            				   				     "' endingBalance='" + (item.endingBalance?item.endingBalance:'') + "' value='" + item.bankAccountNo + "'>"+ item.bankAccountNo +"</option>");
			            	   });
			            	   $("#search-bank-statementAccount").html(htmlString);
		            	   }
		               }
		    	});
		    });
		    
		    if(GLOBAL_PERMISSION["PMS-003"]){
			    $(document).on('click','#table_data .btn-matching',function(){
			    	$('.alert-warning').hide();
			    	receiptOrder.resetArInvoiceSearch();
					receiptOrder.loadARInvoice($(this).attr('credit'));
					receiptOrder.bankStatementId = $(this).closest('tr').find('.bank-statment-id').html();
					$('#txt-total-setted-amount-original, #txt-total-setted-amount-converted').val(0);
					$('#txt-total-setted-amount-original, #txt-total-setted-amount-original').val(0);
				});
		    }
		    
		    $(document).on('click','#ap-receipt-order-matching .btn-search-popup',function(){
		    	$('#txt-total-setted-amount-original, #txt-total-setted-amount-converted').val(0);
				$('#txt-total-setted-amount-original, #txt-total-setted-amount-original').val(0);
		    	receiptOrder.dataTableARInvoice.ajax.reload();
			});
		    
		    $(document).on('click','#ap-receipt-order-matching .btn-reset-popup',function(){
		    	receiptOrder.resetArInvoiceSearch();
		    	//$('.alert-warning').showAlertMessage(["adad"]);
			});
		    
		    $(document).on('click','#ap-receipt-order-matching .btn-apply-matching',function(){
		    	//only allow save when 'Total Setted Amount (Original)' is equal with 'Bank Credit Amount'
		    	if(formatNumber($('#ap-receipt-order-matching #txt-total-setted-amount-original').val()) == formatNumber($('#ap-receipt-order-matching #txt-bank-credit-amount').val()))
		    	{
		    		$('#ap-receipt-order-matching .alert-warning').hide();
		    		receiptOrder.applyMatching();
		    	}
		    	else {
		    		$('#ap-receipt-order-matching .alert-warning').showAlertMessage([label('cashflow.receiptOrder.message1')]);
		    	}
			});
		    
		    /*
		     * AR invoice popup event
		     */
		    $(document).on('change', '#ap-receipt-order-matching .ar-checkbox', function() {
		    	if($(this).is(":checked"))
		    	{
		    		receiptOrder.toggleRowEditable($(this).closest('tr'), false);
		    	}
		    	else
		    	{
		    		receiptOrder.toggleRowEditable($(this).closest('tr'), true);
		    	}
		    	receiptOrder.caculateValue($(this).closest('tr'));
		    	$('#txt-total-setted-amount-original').val(receiptOrder.getTotalSettedAmountOriginal());
		    	$('#txt-total-setted-amount-converted').val(receiptOrder.getTotalSettedAmountConverted());
		    });
		    
		    //on change AR invoice data list values
		    $(document).on('change', '#ap-receipt-order-matching .txt-receipt-rate', function() {
		    	receiptOrder.caculateValue($(this).closest('tr'));
		    	$('#txt-total-setted-amount-original').val(receiptOrder.getTotalSettedAmountOriginal());
		    	$('#txt-total-setted-amount-converted').val(receiptOrder.getTotalSettedAmountConverted());
		    });
		    
		    $(document).on('change', '#ap-receipt-order-matching .txt-setted-amount-original', function() {
		    	var settedAmount = formatNumber($(this).val());
		    	var remainAmount = formatNumber($(this).closest('tr').find('.remain-amount').html());
		    	if(settedAmount > remainAmount){
		    		$('#ap-receipt-order-matching .alert-warning').showAlertMessage([label('cashflow.receiptOrder.message2')]);
		    	}
		    	else {
			    	receiptOrder.caculateValue($(this).closest('tr'));
			    	$('#txt-total-setted-amount-original').val(receiptOrder.getTotalSettedAmountOriginal());
			    	$('#txt-total-setted-amount-converted').val(receiptOrder.getTotalSettedAmountConverted());
		    	}
		    });
		    
		    $(document).on('change', '#ap-receipt-order-matching .txt-setted-amount-converted', function() {
		    	var receiptRate = 0;
		    	if($(this).closest('tr').find('.txt-setted-amount-original').val() != '0')
		    	{
		    		receiptRate = Decimal.div(getNewDecimal($(this).val()), getNewDecimal($(this).closest('tr').find('.txt-setted-amount-original').val())).toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
		    	}
		    	$(this).closest('tr').find('.txt-receipt-rate').val(receiptRate);
		    	
		    	receiptOrder.caculateValue($(this).closest('tr'));
		    	$('#txt-total-setted-amount-original').val(receiptOrder.getTotalSettedAmountOriginal());
		    	$('#txt-total-setted-amount-converted').val(receiptOrder.getTotalSettedAmountConverted());
		    });
		},
		loadARInvoice: function(bankCredit){
			$('#txt-bank-credit-amount').val(bankCredit);
			$('#ap-receipt-order-matching').modal('toggle');
			if(receiptOrder.dataTableARInvoice && receiptOrder.dataTableARInvoice != null){
				receiptOrder.dataTableARInvoice.ajax.reload();
			}
			else
			{
				receiptOrder.dataTableARInvoice = $('#ar_table_data').removeAttr('width').DataTable({
			        "ajax": {
			            url: "/receiptOrder/api/getArInvoice",
			            type: "GET",
			            data: function (d) {
			            	d.fromApNo = $('#ap-receipt-order-matching .arno-from').val();
			            	d.toApNo = $('#ap-receipt-order-matching .arno-to').val();
			            	d.payerName = $('#ap-receipt-order-matching #payername').val();
			            	d.fromMonth = $('#ap-receipt-order-matching .txt-month-from').val();
			            	d.toMonth = $('#ap-receipt-order-matching .txt-month-to').val();
			            	d.isApproved = document.getElementById('isApproved').checked
			            	d.isNotReceived = document.getElementById('isNotReceived').checked
			                d.creditOption = $('#ap-receipt-order-matching .searh-credit-option').val();
			                d.credit = $('#ap-receipt-order-matching .searh-credit').val();
			            },
			            error: function() { // error handling
			                $(".table_data-error").html("");
			                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
			                $("#table_data_processing").css("display", "none");
			            },
			            complete: function(data){
			            	$('#ar_table_data .ap-big-decimal').setupInputNumberMask({
			            		allowMinus: false
			            	});
				            $('#ar_table_data .txt-receipt-rate').inputmask("decimal", {
				        	    placeholder: "0",
				        	    digits: 10,
				        	    digitsOptional: true,
				        	    radixPoint: ".",
				        	    groupSeparator: ".",
				        	    autoGroup: true,
				        	    allowPlus: false,
				        	    allowMinus: true,
				        	    clearMaskOnLostFocus: false,
				        	    removeMaskOnSubmit: true,
				        	    autoUnmask: true,
				        	    rightAlign: false
				        	});
			            	//inputmask has a bug when format negative number, so must setup it's own input mask as below
			            	$('#ar_table_data .txt-variance-account').inputmask("decimal", {
			            	    placeholder: "0",
			            	    digits: 2,
			            	    digitsOptional: false,
			            	    radixPoint: ".",
			            	    groupSeparator: ".",
			            	    autoGroup: true,
			            	    allowPlus: false,
			            	    allowMinus: true,
			            	    clearMaskOnLostFocus: false,
			            	    removeMaskOnSubmit: true,
			            	    autoUnmask: true,
			            	    rightAlign: false,
			            	    onUnMask: function(maskedValue, unmaskedValue) {
			            	        var x = maskedValue.split(',');
			            	        return x[0].replace(/\./g, '') + '.' + x[1];
			            	    }
			            	});
			            	$('.variance-account').each(function(){
			            		var self = $(this);
			            		var accountVal = $(this).tautocomplete({
			        		        highlight: "",
			        		        autobottom: true,
			        				columns : [
				        					GLOBAL_MESSAGES['cashflow.receiptOrder.accountCode'],
				        					GLOBAL_MESSAGES['cashflow.receiptOrder.parentCode'],
				        					GLOBAL_MESSAGES['cashflow.receiptOrder.accountName'],
				        					GLOBAL_MESSAGES['cashflow.receiptOrder.type'] 
			        					],
			        				hide: [true, false, true, true],
			        				dataIndex: 0,
			        				addNewDataLink: '/admin/accountMaster/list',
			        		        data: function(){
			        		        		var x = { keyword: accountVal.searchdata()}; 
			        		        		return x;
			        		        	},
			        		        ajax: {
			        		                url: "/manage/accountMasters",
			        		                type: "GET",
			        		                data: {
			        		                	keyword: function(){
			        		                		var keyword = accountVal.searchdata();
			        		                		return keyword;
			        		                	}
			        		                },
			        		                success: function (data) {
			        		                    return data;
			        		                }
			        		            },
			        		        delay: 1000,
			        		        onchange: function () {
			        		        	var selectedData = accountVal.all();
			        		        	console.log(selectedData);
			        		        	self.closest('tr').find('input[autocomplete]').val(selectedData['Account Name']);
			        		        	self.closest('tr').find('.variance-account').val(selectedData['Account Name']);
			        		        	receiptOrder.caculateValue(self.closest('tr'));
			        		        }
			        			});
			            	});
							hideAjaxStatus();
							$('#ar_table_data tbody tr').each(function(){
								receiptOrder.listArInvoiceData = data.responseJSON.data;
								receiptOrder.toggleRowEditable($(this), true);
								if($(this).find('.remain-amount').html() == '0.00' || $(this).find('.ar-checkbox').attr('isapproved') == 'false')
								{
									$(this).addClass('disable-row');
								}
							});
							var $table = $('#ar_table_data');
							$table.floatThead({
							    scrollContainer: function($table){
							        return $table.closest('#ar_table_data_wrapper');
							    }
							});
							
							//setup data table scroll with header fixed
							setTimeout(function(){
								$('#ar_table_data_wrapper').scroll();
								$('.floatThead-table').css('margin-top','0px');
								$('.floatThead-container').css('z-index','500');
							}, 200);
			            }
			        },
			        columns: [
				        {
				            "data" : "id",
				            "orderable": false
				        },
				        {
				            "data" : "arInvoiceNo",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
				            		return "<a href='/arinvoice/detail/" + data +"' >" + data + "</a>";
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "month",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(full.month)
				            		return (full.month.month + 1) + "/" + full.month.year;
				            	else
				            		return "";
				             }
				        },
				        {
				            "data" : "payerName",
				            "orderable": false
				        },
				        {
				            "data" : "invoiceNo",
				            "orderable": false
				        },
				        {
				            "data" : "description",
				            "width" : "10%",
				            "orderable": false
				        },
				        {
				            "data" : "includeGstOriginalAmount",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data)
				            		return "<span class='pull-left'>" + full.originalCurrencyCode + "</span>" + "<span class='right' >" + formatStringAsNumber(data) + "</span>";
				            	else
				            		return "";
				            }
				        },
				        {
				            "data" : "remainIncludeGstOriginalAmount",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	if(data === undefined)
				            		return  '<span class="pull-left">' + full.originalCurrencyCode + '</span>' + '<span class="right remain-amount">' + formatStringAsNumber(full.includeGstOriginalAmount) + '</span>';
				            	else
				            		return  '<span class="pull-left">' + full.originalCurrencyCode + '</span>' + '<span class="right remain-amount">' + formatStringAsNumber(data) + '</span>';
				            }
				        },
				        {
				            "data" : "",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return "<input class='ap-big-decimal txt-setted-amount-original' value='0' />";
				            }
				        },
				        {
				            "data" : "",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return "<input class='txt-receipt-rate' value='1.0' />";
				            }
				        },
				        {
				            "data" : "",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return "<input class='ap-big-decimal txt-setted-amount-converted' value='0' disabled/>";
				            }
				        },
				        {
				            "data" : "",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return "<input fx-rate=" + full.fxRate + " class='txt-variance-account disabled' value='0' disabled/>";
				            }
				        },
				        {
				            "data" : "",
				            "orderable": false,
				            "render": function(data, type,full, meta) {
				            	return "<input class='variance-account' />";
				            }
				        }
			        ],
			        "searching": false,
			        "ordering" : false,
			        "paging": false,
			        "info" : false,
			        
			        "columnDefs": [
			        	 {
			                'targets' : 0,
			                'searchable' : false,
			                'className' : 'dt-body-center',
			                'render' : function(data, type, full, meta) {
				                  return '<input isApproved=' + full.isApproved +' arinvoice-id=' + full.id +' type="checkbox" name="selectedCheckBox" class="ar-checkbox" >';
			                 }
			            },
			            {
			                "targets": [2,7,8,11,12],
			                "className": "text-right",
			            }
			        ],
				 });
			}
		},
		/*
		 * Popup Matching AR Invoice event
		 */
		resetArInvoiceSearch: function(){
			$('#ap-receipt-order-matching .arno-from').val('');
        	$('#ap-receipt-order-matching .arno-to').val('');
        	$('#ap-receipt-order-matching .txt-payer-name').val('');
        	$('#ap-receipt-order-matching .txt-month-from').val('');
        	$('#ap-receipt-order-matching .txt-month-to').val('');
        	document.getElementById('isApproved').checked = true;
        	document.getElementById('isNotReceived').checked = true;
            $('#ap-receipt-order-matching .searh-credit-option').val(1);
            $('#ap-receipt-order-matching .searh-credit').val(0);
            $('#ap-receipt-order-matching #payername').val('');
            $('#ap-receipt-order-matching #payername').val('');
            $('#ap-receipt-order-matching #payername').parents().find("input[autocomplete]").val('');
		},
		toggleRowEditable: function(tableRow, disabled){
			tableRow.find("input:not(.disabled,[type='checkbox']), select, textarea").prop('disabled', disabled);
			if(!disabled)
			{
				tableRow.addClass('selected');
			}
			else
			{
				tableRow.removeClass('selected');
			}
		},
		getTotalSettedAmountOriginal: function(){
			var total = getNewDecimal(0);
			$('#ap-receipt-order-matching tr.selected .txt-setted-amount-original').each(function(){
				total = total.plus(getNewDecimal($(this).val()));
			});
			return total.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
		},
		getTotalSettedAmountConverted: function(){
			var total = getNewDecimal(0);
			$('#ap-receipt-order-matching tr.selected .txt-setted-amount-converted').each(function(){
				total = total.plus(getNewDecimal($(this).val()));
			});
			return total.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
		},
		caculateValue: function(tableRow){
			var settedAmountOriginal = getNewDecimal(tableRow.find('.txt-setted-amount-original').val());
			var receiptRate = getNewDecimal(tableRow.find('.txt-receipt-rate').val());
			var settedAmountConverted = Decimal.mul(settedAmountOriginal, receiptRate);//settedAmountOriginal * receiptRate;
			var fxRate = getNewDecimal(tableRow.find('.txt-variance-account').attr('fx-rate'));//formatNumber(tableRow.find('.txt-variance-account').attr('fx-rate'));
			var varianceAmount	= settedAmountConverted.minus(settedAmountOriginal.mul(fxRate));//settedAmountConverted - settedAmountOriginal * fxRate;
			var varianceAccountName = tableRow.find('.variance-account').val();
			var id = tableRow.find('input[type="checkbox"]').attr('arinvoice-id');
			console.log(varianceAmount);
			tableRow.find('.txt-variance-account').val(varianceAmount);
			tableRow.find('.txt-setted-amount-converted').val(settedAmountConverted);
			var checked =  tableRow.find('input[type="checkbox"]').is(":checked");
			var data = {
				settedAmountOriginal: settedAmountOriginal.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf(),
				receiptRate: receiptRate.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf(),
				settedAmountConverted: settedAmountConverted.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf(),
				varianceAmount: varianceAmount,
				varianceAccountName: varianceAccountName,
				checked: checked
			};
			receiptOrder.updateListArInvoiceData(id, data, checked);
			//variance account can edit only if varianceAmount > 0
			if(varianceAmount != 0 && tableRow.hasClass('selected')){
				tableRow.find('.variance-account').next().next().prop('disabled',false);
			}
			else {
				tableRow.find('.variance-account').next().next().prop('disabled',true).val('');
				tableRow.find('.variance-account').val('');
			}
		},
		updateListArInvoiceData: function(id, data, checked){
			receiptOrder.listArInvoiceData.forEach(function(item){
				if(item.id == id)
				{
					jQuery.extend(item, data);
					item.checked = checked
					console.log(item);
					return;
				}
			});
		},
		applyMatching: function(){
			if(GLOBAL_PERMISSION["PMS-003"]){
				var listArMatching = [];
				receiptOrder.listArInvoiceData.forEach(function(item){
					if(item.checked)
					{
						listArMatching.push(item);
					}
				});
				console.log(receiptOrder.bankStatementId);
				console.log(listArMatching);
				var formData = new FormData();
				formData.append('data', JSON.stringify(listArMatching));
				formData.append('bankStatementId', receiptOrder.bankStatementId);
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/receiptOrder/applyMatching',
		            cache : false,
					contentType : false,
					processData : false,
					data : formData,
		            success : function(ajaxResult) {
		            	 $('#ap-receipt-order-matching').modal('toggle');
		            	 var resultData = JSON.parse(ajaxResult);
		            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
		            	 {
		            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
	            				 window.location = "/receiptOrder/list";
		            		 });
		            	 }
		            	 else
		            	 {
		            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
		            	 }
		            }
		        });
			}
		},
		mergeRows: function (table, mergeFrom, mergeTo) {
			var rowsList = $(''+table+ ' > tbody').find('tr');
			var previous = null, cellToExtend = null, rowspan = 1;
			
			rowsList.each(function(index, e) {
				var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
				if (previous == content && content !== "" ) {
					rowspan = rowspan + 1;
					
					for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
						if(i >= mergeFrom && i <= mergeTo) {
							$(this).find('td:nth-child(' + i + ')').addClass('hidden');
							padding = rowspan * 16;
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
						} 
					}
				} else {
		             rowspan = 1;
		             previous = content;
		             cellToExtend = elementTab;
				}
			});
			$(table + ' td.hidden').remove();
		}
	};
}());