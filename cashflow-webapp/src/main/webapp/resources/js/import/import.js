(function () {
	ImportExcel = {
		init: function () {
			$(document).on('change', ':file', function() {
			    var input = $(this),
			        numFiles = input.get(0).files ? input.get(0).files.length : 1,
			        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			    input.trigger('fileselect', [numFiles, label]);
			  });

			  // We can watch for our custom `fileselect` event like this
			  $(document).ready( function() {
			      $(':file').on('fileselect', function(event, numFiles, label) {

			          var input = $(this).parents('.input-group').find(':text'),
			              log = numFiles > 1 ? numFiles + ' files selected' : label;

			          if( input.length ) {
			              input.val(log);
			          } else {
			              if( log ) alert(log);
			          }

			      });
			  });
		},
		
		uploadAjaxForm: function() {
			$("#uploadForm").ajaxForm({
			    success:function(data) { 
			          console.log(data);
			     },
			     dataType:"text"
			   }).submit();
		},

				
		displayData : function() {
			$('#import-page, #import-log, #export-page').DataTable({
				"ordering": false,
				"language" : {
		            "zeroRecords" : GLOBAL_MESSAGES['cashflow.common.noData'],
		            "emptyTable": GLOBAL_MESSAGES['cashflow.common.noData'],
		            "info": GLOBAL_MESSAGES['cashflow.common.dataTableInfo1'],
		            "infoEmpty": GLOBAL_MESSAGES['cashflow.common.dataTableInfo2'],
		            //"infoFiltered": " (filter from _MAX_ records)",
		            "infoFiltered": "",
		            "search": "Search",
		            "processing" : "<img src='/resources/css/img/ajax-loader.gif'>",
		            "paginate" : {
		              "first" : GLOBAL_MESSAGES['cashflow.common.dataTablefirst'],
		              "last" : GLOBAL_MESSAGES['cashflow.common.dataTableLast'],
		              "next" : GLOBAL_MESSAGES['cashflow.common.dataTableNext'],
		              "previous" : GLOBAL_MESSAGES['cashflow.common.dataTablePrev']
		            },
		            "lengthMenu": GLOBAL_MESSAGES['cashflow.common.show'] + ' <select class="form-control">'+
		                '<option value="10">10</option>'+
		                '<option value="20">20</option>'+
		                '<option value="50">50</option>'+
		                '<option value="-1">' + GLOBAL_MESSAGES['cashflow.common.all'] + '</option>'+
		            '</select> ' + GLOBAL_MESSAGES['cashflow.common.entries']
		          }
			});
		},
		
		eventListener: function(){
            $('.btn-cancel').click(function(){
            	let url = window.location.href;
            	
            	//
            	if (url.indexOf('/projectPayment/') != -1) {
            		url = $('ol.breadcrumb  > li:nth-child(3) > a').attr('href');
            		$('.btn-cancel').attr('href', url);
            		
            	}
            });
        }, // END eventListener
		
		handleBackBtn: function(callback){
			if(callback == undefined){
				return;
			}
			return callback.handleBackBtn();
		}
	};
}());