(function () {
	APInvoiceSearch = {
		dataTable: null, //main data table
		fromMonth: null,
		toMonth: null,
		fromApNo: null,
		toApNo: null,
		invoiceNo: null,
		payeeName: null,
		status: null,
		init: function () {
			$('.datepicker input').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom'
			});
			
			APInvoiceSearch.fromMonth = $('#fromMonth').val();
			APInvoiceSearch.toMonth = $('#toMonth').val();
			APInvoiceSearch.fromApNo = $('#fromApNo').val();
			APInvoiceSearch.toApNo = $('#toApNo').val();
			APInvoiceSearch.payeeName = $('#payeeName').val();
			APInvoiceSearch.invoiceNo = $('#invoiceNo').val();
			APInvoiceSearch.status = $('#status').val();
			
			APInvoiceSearch.dataTable = $('#apinvoice-tbl').removeAttr('width').DataTable({
				"ajax": {
		            url: "/apinvoice/api/getAllPagingAPInvoice",
		            type: "GET",
		            data: function ( d ) {
		            	d.fromMonth = APInvoiceSearch.fromMonth;
		                d.toMonth = APInvoiceSearch.toMonth;
		                d.fromApNo = APInvoiceSearch.fromApNo;
		                d.toApNo = APInvoiceSearch.toApNo;
		                d.payeeName = APInvoiceSearch.payeeName;
		                d.invoiceNo = APInvoiceSearch.invoiceNo;
		                d.status = APInvoiceSearch.status;
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="15">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
						hideAjaxStatus();
						APInvoiceSearch.mergeRows('#apinvoice-tbl', 1, 8);
						APInvoiceSearch.mergeRows('#apinvoice-tbl', 9, 16);
						APInvoiceSearch.displayRightText();
		            }
				},
				columns: [
			        {
			            "data" : "apInvoiceNo",
			            "width" : "4%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	return "<a href='detail/"+full.apInvoiceNo+"'>"+data+"</a>";
			            }
			        },
			        {
			            "data" : "month",
			            "width" : "3%",
			            "orderable": true,
			            "render": function(data, type,full, meta) {
			            	if(full.month != undefined){
			            		return "<span class='right' >" + (full.month.month + 1) + "/" + full.month.year + "</span>";
			            	}
			            }
			        },
			        {
			            "data" : "payeeAccount",
			            "width" : "7%",
			            "orderable": false,
			        },
			        {
			            "data" : "claimTypeVal",
			            "width" : "5%",
			            "orderable": false,
			        },
			        {
			            "data" : "payeeName",
			            "width" : "8%",
			            "orderable": false,
			        },
			        {
			            "data" : "invoiceNo",
			            "width" : "2%",
			            "orderable": false,
			        },
			        {
			            "data" : "invIncludeGstConvertedAmount",
			            "width" : "5%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.apInvoiceCurrency == undefined){
			            		return "";
			            	}
			            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "invoiceStatus",
			            "width" : "6%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.invoiceStatus != undefined){
			            		return "<div class='ap-status  api-"+full.invoiceStatusCode+"'>"+data+"</div>";
			            	}
			            }
			        },
			        {
			            "data" : "paymentOrderNo",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var paymentOrderNo = "";
			            	if(full.paymentOrderNo != undefined){
			            		paymentOrderNo = "<a href='/payment/detail/"+full.paymentOrderNo+"'>"+data+"</a>";
			            	}
			            	return paymentOrderNo;
			            }
			        },
			        {
			            "data" : "valueDate",
			            "width" : "2%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var valueDate = "";
			            	if(full.valueDate != undefined){
			            		valueDate = "<span class='right' >" + full.valueDate.dayOfMonth + "/" + (full.valueDate.month + 1) + "/" + full.valueDate.year + "</span>";
			            	}
			            	return valueDate;
			             }
			        },
			        {
			            "data" : "bankName",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankName = "";
			            	if(full.bankName != undefined){
			            		bankName = full.bankName;
			            	}
			            	return bankName;
			            }
			        },
			        {
			            "data" : "bankAccount",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankAccount = "";
			            	if(full.bankAccount != undefined){
			            		bankAccount = full.bankAccount;
			            	}
			            	return bankAccount;
			            }
			        },
			        {
			            "data" : "bankStatement",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var trans = "";
			            	var length = data.length;
			            	$.each(data, function(i, obj) {
			            		trans += "<a href='/bankStatement/list/"+obj.id+"' target='_blank'>"+obj.id+"</a>";
			            		if ( i != length - 1){
			            			trans += ", ";
			            		}
			            	});
			            	return trans;
			            }
			        },
			        {
			            "data" : "includeGstConvertedAmount",
			            "width" : "6%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if (full.paymentOrderNo == undefined) {
			            		return "";
			            	}
			            	return "<span class='pull-left'>" + $('#currentCurrency').val() + "</span>" 
	            			+ "<span class='right' >" + formatStringAsNumber(data) + "</span>";
			            }
			        },
			        {
			            "data" : "bankRefNo",
			            "width" : "3%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var bankRefNo = "";
			            	if(full.bankRefNo != undefined){
			            		bankRefNo = full.bankRefNo;
			            	}
			            	return bankRefNo;
			            }
			        },
			        {
			            "data" : "paymentStatus",
			            "width" : "4%",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	var paymentStatus = "";
			            	if(full.paymentStatus != undefined){
			            		paymentStatus = "<div class='ap-status  pay-"+full.paymentStatusCode+"'>"+data+"</div>";
			            	}
			            	return paymentStatus;
			            }
			        },
		        ],
		        searching: false,
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, GLOBAL_MESSAGES['cashflow.common.all']]],
				"lengthChange" : true,
				'order' : [ [ 0, 'desc' ] ],
		        'columnDefs' : []
			});
			
			
			
		}, //end init
		
		eventListener: function () {
			$(document).on("click","#btn-search",function(){
				APInvoiceSearch.fromMonth = $('#fromMonth').val();
				APInvoiceSearch.toMonth = $('#toMonth').val();
				APInvoiceSearch.fromApNo = $('#fromApNo').val();
				APInvoiceSearch.toApNo = $('#toApNo').val();
				APInvoiceSearch.payeeName = $('#payeeName').val();
				APInvoiceSearch.invoiceNo = $('#invoiceNo').val();
				APInvoiceSearch.status = $('#status').val();
				APInvoiceSearch.dataTable.ajax.reload();
		    });
			
			$('#export').click(function(){
				var href = $(this).attr("href");
				var param = "";
				
				var fromMonth = $('#fromMonth').val();
				var toMonth = $('#toMonth').val();
				var payeeName = $('#payeeName').val();
				var invoiceNo = $('#invoiceNo').val();
				var status = $('#status').val();
				var payeeType = $('#payeeType').val();
				var fromApNo = $('#fromApNo').val();
				var toApNo = $('#toApNo').val();
				
				if (fromMonth.length > 0) {
					param += "&fromMonth=" + fromMonth;
				}
				if (toMonth.length > 0) {
					param += "&toMonth=" + toMonth;
				}
				if (payeeName.length > 0) {
					param += "&payeeName=" + payeeName;
				}
				if (invoiceNo.length) {
					param += "&invoiceNo=" + invoiceNo;
				}
				if (status.length) {
					param += "&status=" + status;
				}
				if (fromApNo.length > 0) {
					param += "&fromApNo=" + fromApNo;
				}
				if (toApNo.length > 0) {
					param += "&toApNo=" + toApNo;
				}
				if (param.length > 0) {
					href = href + "?" + param.slice(1);
				}
				$("#export").attr("href", href);
			});
			
			//clear data search condition form
			$('#btn-reset').click(function(){
				$('#fromMonth').val('');
				$('#toMonth').val('');
				$('#payeeName').val('');
				$('#invoiceNo').val('');
				$('#status').val('');
				$('#payeeType').val('');
				$('#fromApNo').val('');
				$('#toApNo').val('');
			});
		},
		
		mergeRows : function mergeRows(table, mergeFrom, mergeTo) {
			var rowsList = $(''+table+ ' > tbody').find('tr');
			var previous = null, cellToExtend = null, rowspan = 1;
			
			rowsList.each(function(index, e) {
				var elementTab = $(this).find('td:nth-child('+mergeFrom+')'), content = elementTab.text();
				if (previous == content && content !== "" ) {
					rowspan = rowspan + 1;
					
					for(var i=1; i<= $(table).find('tr:nth-child(2) th').length; i++) {
						if(i >= mergeFrom && i <= mergeTo) {
							$(this).find('td:nth-child(' + i + ')').addClass('hidden');
							padding = rowspan * 16;
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr('style', "padding:" + padding + "px 8px");
							cellToExtend.parent().find('td:nth-child(' + i + ')').attr("rowspan", rowspan);
						} 
					}
				} else {
		             rowspan = 1;
		             previous = content;
		             cellToExtend = elementTab;
				}
			});
			$(table + ' td.hidden').remove();
		}, 
		
		displayRightText : function(){
			$('#apinvoice-tbl').find('.right').parent().addClass('text-right');
		},
	};
}());