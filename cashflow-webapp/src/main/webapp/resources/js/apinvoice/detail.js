(function () {
	APInvoice = {
		dtDetail: null,
		approvalJson: [], // save approval code - amount as json.
		thePayee: null,
		AP_STATUSES: {},
		init: function () {
			
			var ddmmyyyy = 'dd/mm/yyyy';

		    $('#bookingDate').datepicker({
		    	format: ddmmyyyy,
		        orientation: 'auto bottom',
		        // todayHighlight: true
		    });
		    
		    $('#month').datepicker({
				format: "mm/yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
		        orientation: 'auto bottom',
		        // todayHighlight: true
		    });
		    
		    $('#invoiceIssuedDate').datepicker({
		        format: ddmmyyyy,
		        orientation: 'auto bottom',
		        // todayHighlight: true
		    });
		    
		    $('#scheduledPaymentDate').datepicker({
		        format: ddmmyyyy,
		        orientation: 'auto bottom',
		        // todayHighlight: true
		    });
		    
		    // Add Account Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/invoice-account-row-detail-*/);
		    }).each(function(){
		    	var tokens = this.id.split('-');
		    	var rowId = tokens[tokens.length -1];
		    	var accountVal = APInvoice.addAccountAutoComplete(rowId);
		    	accountVal.settext(this.value);
		        });
			
			// Add Project Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/invoice-project-row-detail-*/);
		    }).each(function(){
		    	
		    	var tokens = this.id.split('-');
		    	var rowId = tokens[tokens.length -1];
		    	var projectVal = APInvoice.addProjectAutoComplete(rowId);
		    	projectVal.settext(this.value);
		    	
		    });
			
			// Add Approval No Autocomplete table for the list of existed data
			$('input').filter(function() {
		        return this.id.match(/approval-code-*/);
		    }).each(function(index){
		    	var tokens = this.id.split('-');
		    	var rowId = tokens[tokens.length -1];
		    	var approvalVal = APInvoice.addApprovalAutoComplete(rowId);
		    	approvalVal.settext(this.value);
		    	
		    });
			
			//
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
					var rowId = index;
			    	APInvoice.calculateAmountDetail(rowId, event);
			    	
			    	// UPDATE ALERT APPROVAL STATUS
			    	var approvalNo = $('#approval-code-' + rowId).val();
			    	
			    	if(!isEmpty(approvalNo)){
			    		var approvalAmount = APInvoice.approvalJson[approvalNo];
			    		APInvoice.addAlertApprovalStatus(approvalNo, approvalAmount);
			    		APInvoice.updateAlertApprovalStatus();
			    	}
				});
			});
			
			// binh.lv
//			$('input').filter(function(){
//				return this.id.match(/fx-rate-*/);
//			}).each(function(index){
//				$('#' + this.id).change(function (event){
//			    	var rowId = index;
//			    	APInvoice.calculateAmountDetail(rowId, event);
//				});
//			});

			//
			$('input').filter(function(){
				return this.id.match(/^exclude-gst-converted-amount-*/);
			}).each(function(index){
				$('#' + this.id).change(function (event){
			    	var rowId = index;
			    	APInvoice.checkAndReCulcalateFxRate(rowId, event);
				});
			});

			
			//
//			$('input').filter(function(){
//				return this.id.match(/^gst-rate-*/);
//			}).each(function(index){
//				$('#' + this.id).change(function(event){
//					var rowId = index;
//					APInvoice.calculateAmountDetail(rowId, event);
//				})
//			});
			
			// auto cal schedule payment date
			$('#invoiceIssuedDate').change(function() {
				APInvoice.updateScheduledPaymentDate();
			});
					
			// //////ADD AUTO COMPLETE FOR PAYEE////////
			APInvoice.thePayee = $('#payeeName').tautocomplete({
		        highlight: "",
				columns : [
						label('cashflow.apInvoice.detail.code'),
						label('cashflow.apInvoice.detail.payeeAccount'),
						label('cashflow.apInvoice.detail.payeeName'),
						label('cashflow.apInvoice.detail.address'),
						label('cashflow.apInvoice.detail.phone'),
						label('cashflow.apInvoice.detail.type'),
						label('cashflow.apInvoice.detail.typeName'),
						label('cashflow.apInvoice.detail.paymentTerm'),
						label('cashflow.apInvoice.detail.accountCode'),
						label('cashflow.apInvoice.detail.accountName'),
						label('cashflow.apInvoice.detail.currency')
					],
				hide: [false, true, true, true, true, false, false, false, false, false, false],
				addNewDataLink: '/admin/payeeMaster/list',
		        data: function(){
		        		var x = { keyword: APInvoice.thePayee.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/payeeMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = APInvoice.thePayee.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (result) {
		                	result.forEach(function(item){
		                		if(!item.address)
		                		{
		                			item.address = '';
		                		}
		                		if(!item.phone)
		                		{
		                			item.phone = '';
		                		}
		                	});
		                    return result;
		                }
		            },
		        delay: 100,
		        onchange: function () {
		        	var selectedData = APInvoice.thePayee.all();
		        	$('#payeeTypeName').val(selectedData[label('cashflow.apInvoice.detail.typeName')]);
		        	$('#payeeTypeCode').val(selectedData[label('cashflow.apInvoice.detail.type')]);
		        	$('#payeeCode').val(selectedData[label('cashflow.apInvoice.detail.code')]);
		        	$('#payeeName').val(selectedData[label('cashflow.apInvoice.detail.payeeName')]);
		        	$('#paymentTerm').val(selectedData[label('cashflow.apInvoice.detail.paymentTerm')]);
		        	APInvoice.updateScheduledPaymentDate();
		        	
		        	//
		        	$('#accountPayableCode').val(selectedData[label('cashflow.apInvoice.detail.accountCode')]); 
        			$('#accountPayableName').val(selectedData[label('cashflow.apInvoice.detail.accountName')]);
        			accountPayableVal.settext($('#accountPayableCode').val());
		        	
        			$('#originalCurrencyCode').val(selectedData[label('cashflow.apInvoice.detail.currency')]);
		        }
			});
		    
			APInvoice.thePayee.settext($('#payeeName').val());
		    // /----------------------------------------------------------
			
			// //////CREATE TABLE LIST INVOICE DETAIL////////
			APInvoice.dtDetail = $('#detail-invoice-table').DataTable({
		         "paging":   false,
		         "ordering": false,
		         "info":     false,
		         "filter":     false,
		         "autoWidth": false,
		         "scrollY": 200,
			     "scrollX": true,
			     "drawCallback": function( settings ) {
			    	 $('.detail-invoice-table-wrapper').css('width','100%');
			      }
		    });
		    
		    
		    // ADD NEW ROW FOR TABLE WHEN ADD BUTTON IS CLICK
		    $('#add-row-detail-invoice-btn').click(function () {
		    	var dataListHTML = "<datalist id='listGstType" + counter + "'>" + $('#gst-master-select-hidden').html() + "</datalist>"
		    	APInvoice.dtDetail.row.add( [
		            '<input type="text" index="'+ counter +'\" name="detailForms['+counter+'].accountCode" id="invoice-account-row-detail-'+ counter +'\" class="form-control invoice-account-row-detail">',
		            '<input type="text" name="detailForms['+counter+'].accountName" id="invoice-account-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].classCode" id="invoice-project-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].className" id="invoice-project-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].description" id="description-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].poNo" id="po-no-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].approvalCode" id="approval-code-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon inv-currency"></span><input type="text" name="detailForms['+counter+'].excludeGstOriginalAmount" id="exclude-gst-original-amount-'+ counter +'\" class="form-control"></div>',
		            '<input type="text" name="detailForms['+counter+'].fxRate" id="fx-rate-'+ counter +'\" class="form-control fx-rate" readonly="readonly">',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].excludeGstConvertedAmount" id="exclude-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
					// binh.lv CF-630
		            //'<select name="detailForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type-input" list="listGstType' + counter +'">' + $('#gst-master-select-hidden').html() + '</select>',
					'<input type="text" name="detailForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type" readonly="readonly">',
					'<input type="text" name="detailForms['+counter+'].gstRate" id="gst-rate-'+ counter +'\" class="form-control gst-rate" readonly="readonly">',
		            
		            '<input type="hidden" name="detailForms['+counter+'].gstOriginalAmount" id="gst-original-amount-'+ counter +'\">' +
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].gstConvertedAmount" id="gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].includeGstOriginalAmount" id="include-gst-original-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].includeGstConvertedAmount" id="include-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="ap-status apr-' + $('#apr002Value').attr('code') + ' ap-detail">' + $('#apr002Value').val() + '</div>',
		            '<span id="action-del-'+counter+'" class="action-del glyphicon glyphicon-trash"></span>',
		        ] ).draw( false );
		    	
		    	// auto add value to row
		        $('#fx-rate-' + counter).val($('#fxRate').val());
		        $('#gst-type-' + counter).val($('#gstType').find(":selected").text());
		        $('#gst-rate-' + counter).val($('#gstRate').val());
		        $('#description-' + counter).val($('#description').val());
		        
		        // Enable button copy
		        $('#copy-row-detail-invoice-btn').attr('disabled', false);
		 
		        APInvoice.addProjectAutoComplete(counter);
		        APInvoice.addAccountAutoComplete(counter);
		        APInvoice.addApprovalAutoComplete(counter);
		        APInvoice.addCalculationAction(counter);
		        APInvoice.addCommasNumber();
		        counter++;

		    } );// End ADD NEW ROW----------------------------------------------
		    
		    // COPY TO NEW ROW FOR TABLE
		    $('#copy-row-detail-invoice-btn').click(function () {
		    	
		    	lastIndex = $('#detail-invoice-table').find('tbody tr:last-child').find('.invoice-account-row-detail').attr('index');
		    	
		    	APInvoice.dtDetail.row.add( [
		            '<input type="text" index="'+ counter +'\" name="detailForms['+counter+'].accountCode" id="invoice-account-row-detail-'+ counter +'\" class="form-control invoice-account-row-detail" index="'+ counter +'\">',
		            '<input type="text" name="detailForms['+counter+'].accountName" id="invoice-account-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].classCode" id="invoice-project-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].className" id="invoice-project-name-row-detail-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].description" id="description-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].poNo" id="po-no-'+ counter +'\" class="form-control">',
		            '<input type="text" name="detailForms['+counter+'].approvalCode" id="approval-code-'+ counter +'\" class="form-control">',
		            '<div class="input-group"><span class="input-group-addon inv-currency"></span><input type="text" name="detailForms['+counter+'].excludeGstOriginalAmount" id="exclude-gst-original-amount-'+ counter +'\" class="form-control"></div>',
		            '<input type="text" name="detailForms['+counter+'].fxRate" id="fx-rate-'+ counter +'\" class="form-control fx-rate" readonly="readonly">',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].excludeGstConvertedAmount" id="exclude-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
					// binh.lv CF-630
		            //'<select name="detailForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type-input" list="listGstType' + counter +'">' + $('#gst-master-select-hidden').html() + '</select>',
		            '<input type="text" name="detailForms['+counter+'].gstType" id="gst-type-'+ counter +'\" class="form-control gst-type" readonly="readonly">',
		            '<input type="text" name="detailForms['+counter+'].gstRate" id="gst-rate-'+ counter +'\" class="form-control gst-rate" readonly="readonly">',
		            
		            '<input type="hidden" name="detailForms['+counter+'].gstOriginalAmount" id="gst-original-amount-'+ counter +'\" >' +
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].gstConvertedAmount" id="gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].includeGstOriginalAmount" id="include-gst-original-amount-'+ counter +'\" class="form-control"></div>', 
		            '<div class="input-group"><span class="input-group-addon sub-currency"></span><input type="text" name="detailForms['+counter+'].includeGstConvertedAmount" id="include-gst-converted-amount-'+ counter +'\" class="form-control"></div>',
		            '<div class="ap-status apr-' + $('#apr002Value').attr('code') + ' ap-detail">' + $('#apr002Value').val() + '</div>',
		            '<span id="action-del-'+counter+'" class="action-del glyphicon glyphicon-trash"></span>',
		        ] ).draw( false );
		    	
		        APInvoice.addCommasNumber();
		        
		        // Copy value
		        $('#invoice-account-row-detail-' + (counter)).val($('#invoice-account-row-detail-' + (lastIndex)).val());
		        $('#invoice-account-name-row-detail-' + (counter)).val($('#invoice-account-name-row-detail-' + (lastIndex)).val());
		        $('#invoice-project-row-detail-' + (counter)).val($('#invoice-project-row-detail-' + (lastIndex)).val());
		        $('#invoice-project-name-row-detail-' + (counter)).val($('#invoice-project-name-row-detail-' + (lastIndex)).val());
		        $('#description-' + (counter)).val($('#description-' + (lastIndex)).val());
		        $('#po-no-' + (counter)).val($('#po-no-' + (lastIndex)).val());
		        $('#approval-code-' + (counter)).val($('#approval-code-' + (lastIndex)).val());
		        $('#exclude-gst-original-amount-' + (counter)).val($('#exclude-gst-original-amount-' + (lastIndex)).val());
		        $('#fx-rate-' + (counter)).val($('#fx-rate-' + (lastIndex)).val());
		        $('#exclude-gst-converted-amount-' + (counter)).val($('#exclude-gst-converted-amount-' + (lastIndex)).val());
		        $('#gst-type-' + (counter)).val($('#gst-type-' + (lastIndex)).val());
		        $('#gst-rate-' + (counter)).val($('#gst-rate-' + (lastIndex)).val());
		        $('#gst-original-amount-' + (counter)).val($('#gst-original-amount-' + (lastIndex)).val());
		        $('#gst-converted-amount-' + (counter)).val($('#gst-converted-amount-' + (lastIndex)).val());
		        $('#include-gst-original-amount-' + (counter)).val($('#include-gst-original-amount-' + (lastIndex)).val());
		        $('#include-gst-converted-amount-' + (counter)).val($('#include-gst-converted-amount-' + (lastIndex)).val());
		 
		        var accountVal = APInvoice.addAccountAutoComplete(counter);
		        accountVal.settext($('#invoice-account-row-detail-' + (lastIndex)).val());
		        
		        var projectVal = APInvoice.addProjectAutoComplete(counter);
		        projectVal.settext($('#invoice-project-row-detail-' + (lastIndex)).val());
		        
		        var approvalVal = APInvoice.addApprovalAutoComplete(counter);
		        approvalVal.settext($('#approval-code-' + (lastIndex)).val());
		        
		        //
		        APInvoice.addCalculationAction(counter);
		        
		        $('#exclude-gst-original-amount-' + (lastIndex)).blur();
		        
		        counter++;

		    });//End COPY TO NEW ROW------------------------------------------------
		    
		    // ACTION WHEN CLICK ON DELETE
		    $('body').on('click', '.action-del', function() {
		    	
		    	var theId = '#'+$(this).attr('id');
		    	
		    	var tokens = this.id.split('-');
				var rowId = tokens[tokens.length -1];
				
				var theApInvoiceId = $('#id').val();
				var theApInvoiceAccountDetailId = $('#apInvoiceAccountDetailId-' + rowId).val();
				var theApInvoiceClassDetailId = $('#apInvoiceClassDetailId-' + rowId).val();
		    	
				// Just row move row is not exist on database.
		    	if(theApInvoiceClassDetailId == undefined || theApInvoiceClassDetailId == ""){
	    			
		    		// re-calculate amount summarize
		    		APInvoice.calculateSubAmountSummarize(rowId);
	    			
		    		// Delete row in table
	    			APInvoice.dtDetail.row($(theId).parents('tr')).remove().draw(false);
	    			
	    			// disable copy button when have no row.
	    			var lengthTbl = APInvoice.dtDetail.data().length;
	    			if(lengthTbl == 0){
	    				$('#copy-row-detail-invoice-btn').attr('disabled', true);
	    			}
	    			
	    			return;
	    		}
		    	
		    	//Check AP Invoice at least one detail. Prevent delete when have only one detail.
		    	var nDetail = 0;
		    	$('input').filter(function(){
					return this.id.match(/invoice-account-row-detail-*/);
				}).each(function(){
					var tokens = this.id.split('-');
			    	var rowId = tokens[tokens.length -1];
			    	var theApiAccDetailId = $('#apInvoiceAccountDetailId-' + rowId).val();
			    	
			    	if(theApiAccDetailId != undefined && theApiAccDetailId.length > 0){
			    		nDetail++;
			    	}
				});
		    	
		    	if(nDetail == 1){
		    		$('.error-area').showAlertMessage([label('cashflow.apInvoice.detail.required.classDetail')]);
		    		return;
		    	}
		    	
		    	// show modal when delete row is exist on database.
		    	var confirmModal = $("#confirm-delete");
		    	confirmModal.modal('show');
		    	
		    	$(".btn-deleted").off('click').click(function () {
		    		confirmModal.modal("hide");
		    		
		    		if(theApInvoiceClassDetailId.length > 0){
		    			$.ajax({
				            type : "POST",
				            url : CONTEXT_PATH + '/apinvoice/delete',
				            data: {
				            	apInvoiceId : theApInvoiceId,
				            	apInvoiceAccountDetailId : theApInvoiceAccountDetailId,
				            	apInvoiceClassDetailId : theApInvoiceClassDetailId
				            },
				            success : function(ajaxResult) {
				               
				               if(ajaxResult.STATUS == "SUCCESS") {
				            	   APInvoice.dtDetail.row($(theId).parents('tr')).remove().draw(false);
				            	   
				            	   window.location.reload();
				            	   sessionStorage.setItem("editFlg", true);
				               } else {
				            	   
				               }
				            }
				        }); // end ajax
		    		}
		    		
		        });
		    }); // end action delete
		    
		    ///--------------------------------------------------------
			
			
			////////ADD AUTO COMPLETE FOR ACCOUNTPAYABLE////////
			var accountPayableVal = $('#accountPayableCode').tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.apInvoice.detail.accountCode'), label('cashflow.apInvoice.detail.parentCode'), label('cashflow.apInvoice.detail.accountName'), label('cashflow.apInvoice.detail.type')],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountPayableVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountPayableVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountPayableVal.all();
		        	$('#accountPayableCode').val(selectedData[label('cashflow.apInvoice.detail.accountCode')]);
		        	$('#accountPayableName').val(selectedData[label('cashflow.apInvoice.detail.accountName')]);
		        }
			});// -----------------------------------------------------------
			
			accountPayableVal.settext($('#accountPayableCode').val());
			
			// Display read only when page on mode edit.
			if(modeEdit){
				APInvoice.setReadonlyInput(true);
			}
			
			APInvoice.defaultDisplay();
			APInvoice.setButtonWhenInit(modeEdit);
		    
			// update GST when initial.
			if(!(modeEdit || modeCopy)){
				APInvoice.updateGstRate();
			}
			
		}, // END INIT
		
		eventListener: function () {
			$("#edit-btn").click(function(){
				APInvoice.setReadonlyInput(false);
				APInvoice.defaultDisplay();
				APInvoice.setButtonWhenEdit();
				
				if($('#status').val() == '002'){ // active dropdown when status is Not Paid (002)
					$('.ap-label-status').hide();
					$('.ap-dropdown-status').show();
				}
				
			});
			
			// disable enter key press
			$('#form1').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
					e.preventDefault();
					return false;
				}
			});
			
			// cancel btn
			$('#btn-cancel').click(function(e){
				e.preventDefault();
				// window.location = document.referrer;
				window.location = "/apinvoice/list";
			});
			
			// update GST when change vat code.
			$( "#gstType" ).change(function() {
				$('.gst-type').val($('#gstType').val());
				
				APInvoice.updateGstRate();
				
				$('.gst-rate').val($('#gstRate').val());
				$('input').filter(function(event){
					return this.id.match(/gst-rate-*/);
				}).each(function(index){
					var rowId = index;
			    	APInvoice.calculateAmountDetail(rowId, event);
				});
			});
			
			// auto set account name when change account code
			$(document).on('change','#accountPayableCode',function(){
		    	var name = this.options[this.selectedIndex].getAttribute('name');
		    	$('#accountPayableName').val(name);
		    });
			
			// binh.lv CF-630
			// auto set account name when change account code
//			$(document).on('change','.gst-type-input', function(event){
//		    	var rate = this.options[this.selectedIndex].getAttribute('rate');
//		    	var rateField = $(this).closest('td').next().find('input');
//		    	rateField.val(rate);
//		    	
//		    	// update input gst, amout relate.
//		    	var tokens = this.id.split('-');
//				var rowId = tokens[tokens.length -1];
//				APInvoice.calculateAmountDetail(rowId, event);
//				
//				// UPDATE ALERT APPROVAL STATUS
//				var approvalNo = $('#approval-code-' + rowId).val();
//
//				if(!isEmpty(approvalNo)){
//					var approvalAmount = APInvoice.approvalJson[approvalNo];
//					APInvoice.addAlertApprovalStatus(approvalNo, approvalAmount);
//					APInvoice.updateAlertApprovalStatus();
//				}
//		    });
			
			//
			$("#originalCurrencyCode").change(function () {
				APInvoice.updateCurrency();
		    });

			// binh.lv CF-630
			$("#gstRate").change(function () {
				$('.gst-rate').val($('#gstRate').val());

				$('input').filter(function(){
					return this.id.match(/gst-rate-*/);
				}).each(function(index){
					var rowId = index;
			    	APInvoice.calculateAmountDetail(rowId, event);
				});
		    });
			
			$("#fxRate").change(function (event) {
				$('.fx-rate').val($('#fxRate').val());
							
				$('input').filter(function(){
					return this.id.match(/fx-rate-*/);
				}).each(function(index){
					var rowId = index;
			    	APInvoice.calculateAmountDetail(rowId, event);
				});
		    });
		
			// reset btn
			$('#btn-reset').click(function(){
				if(!$(this).hasClass('disabled')){
					if(modeEdit || modeCopy){
						window.location.reload();
					}
					else {
						$('#alertApprovalStatus').html('');
						document.getElementById("form1").reset();
						$('.action-del').trigger('click');
					}
				}
			});
			
			// change invoice status
			$(document).on('click','.ap-dropdown-status li',function(){
				const NOTPAID = '002';
				const CANCELLED = '004';
				
				var statusCode = $(this).find('a').attr('status');
				$(this).closest('.ap-dropdown-status').find('button').attr('class','ap-status api-' + statusCode + ' ap-status-header');
				$(this).closest('.ap-dropdown-status').find('.status-label').html($(this).find('a').html());
				
				// update dropdown
				if(statusCode == CANCELLED){
					$(this).find('a').attr('status', NOTPAID);
					$(this).find('a').html(APInvoice.AP_STATUSES[NOTPAID]);

				} else if(statusCode == NOTPAID){
					$(this).find('a').attr('status', CANCELLED);
					$(this).find('a').html(APInvoice.AP_STATUSES[CANCELLED]);
				}
				
				$('#status').val(statusCode);
			});
		    
		}, // end eventListener
		
		defaultDisplay: function(){
			
			$('#payeeTypeName').attr('readonly',true);
			$('#apInvoiceNo').attr('readonly',true);
			$('#excludeGstOriginalAmount').attr('readonly',true);
			$('#excludeGstConvertedAmount').attr('readonly',true);
			$('#gstConvertedAmount').attr('readonly',true);
			$('#includeGstConvertedAmount').attr('readonly',true);
			$('#paymentTerm').attr('readonly',true);
			$('#scheduledPaymentDate').attr('readonly',true);
			// binh.lv CF-630
			// $('#gstRate').attr('readonly',true);
			$('#accountPayableName').attr('readonly',true);
			
			APInvoice.disableInputClassDetail();
		}, // end defaultDisplay
		
		setReadonlyInput: function(isReadonly){
			
			$('#form1 input[type=text]').attr('readonly', isReadonly);
			$('#form1 select').attr('readonly', isReadonly);
			$('#form1 textarea').attr('readonly', isReadonly);
			
			if(isReadonly){
				$('#form1 input[readonly=readonly]').addClass('disabled-input');
				$('#form1 select').addClass('disabled-input');
			}else{
				$('#form1 input[type=text]').removeClass('disabled-input');
				$('#form1 select').removeClass('disabled-input');
			}
			
			// Allow update when AP Invoice have status PAID (001), PARTIAL PAID(003)
			if(isReadonly == false){ // when edit
				let statusCode = $('#status').val();
				if(statusCode == '001' || statusCode == '003'){
					$('#originalCurrencyCode').attr('readonly', true);
					$('#originalCurrencyCode').addClass('disabled-input');
					
					$('#fxRate').attr('readonly', true);
					$('#fxRate').addClass('disabled-input');
					
					$('#gstRate').attr('readonly', true);
					$('#gstRate').addClass('disabled-input');
					
					$('#gstType').attr('readonly', true);
					$('#gstType').addClass('disabled-input');
					
					$('.detail-area input[type=text]').attr('readonly', true);
					$('.detail-area select').attr('readonly', true);
					
					$('.detail-area input[readonly=readonly]').addClass('disabled-input');
					$('.detail-area select').addClass('disabled-input');
					
				}
			}
			
			$('input').filter(function(){
				return this.id.match(/invoice-account-row-detail-*/);
			}).each(function(){
				$(this).next().next().attr('readonly', true);
			});
			
		}, // end setReadonlyInput
		
		setButtonWhenInit: function(modeEdit){
			
			if(modeEdit){
				$('#save-btn').addClass('disabled');
				$('#btn-cancel').addClass('disabled');
				$('#btn-reset').addClass('disabled');
			}else{
				$('#save-btn').removeClass('disabled');
				$('#copy-btn').addClass('disabled');
				$('#edit-btn').addClass('disabled');
			}

			$('#add-row-detail-invoice-btn').attr('disabled', modeEdit);
			
			//Disable copy button when request not from copy. 
			if(window.location.href.indexOf('copy') == -1){
				$('#copy-row-detail-invoice-btn').attr('disabled', true);
			}
			
			//Disable reset button when copy.
			//if(window.location.href.indexOf('copy') != -1){
			//	$('#btn-reset').addClass('disabled');
			//}
			
			$('.action-del').css("display", (modeEdit ? 'none' : 'inline'));
			
		}, // end setButtonWhenInit
		
		setButtonWhenEdit: function(){
			$('#edit-btn').attr('disabled', true);
			$('#save-btn').removeClass('disabled');
			
			// $('#new-btn').addClass('hidden');
			
			//Enable when AP Invoice have status NOT PAID (002), otherwise always disable.
			if( $('#status').val() == '002'){
				$('#add-row-detail-invoice-btn').attr('disabled', false);
				$('#copy-row-detail-invoice-btn').attr('disabled', false);
			}

			// Show icon delete when AP Invoice have status NOT PAID (002)
			if($('#status').val() == '002'){
				$('.action-del').css("display", 'inline');
			}
			
			$('#btn-cancel').removeClass('disabled');
			$('#btn-reset').removeClass('disabled');
		}, // end setButtonWhenEdit
		
		updateGstRate: function(){
			var gstType = $('#gstType').find(":selected");
			var rate = gstType.attr('rate');
			
			$('#gstRate').val(rate);
			
		}, // end updateGstRate
		
		addProjectAutoComplete: function(idCounter){
			var elementId = 'invoice-project-row-detail-'+ idCounter;
		    var projectVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.apInvoice.detail.prjCode'), label('cashflow.apInvoice.detail.prjName')],
				hide: [true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/projectMaster/list',
		        data: function(){
		        		var x = { keyword: projectVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/projectMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = projectVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = projectVal.all();
		        	var projectNameId = 'invoice-project-name-row-detail-'+ idCounter;
		        	projectVal.settext(selectedData[label('cashflow.apInvoice.detail.prjCode')]);
		        	$('#'+ elementId).val(selectedData[label('cashflow.apInvoice.detail.prjCode')]);
		        	$('#'+ projectNameId).val(selectedData[label('cashflow.apInvoice.detail.prjName')]);
		        }
			});
		    return projectVal;
		},
		
		addAccountAutoComplete: function(idCounter){
			var elementId = 'invoice-account-row-detail-'+ idCounter;
		    var accountVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [label('cashflow.apInvoice.detail.accountCode'),label('cashflow.apInvoice.detail.parentCode'), label('cashflow.apInvoice.detail.accountName'), label('cashflow.apInvoice.detail.type')],
				hide: [true, false, true, true],
				dataIndex: 0,
				addNewDataLink: '/admin/accountMaster/list',
		        data: function(){
		        		var x = { keyword: accountVal.searchdata()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/manage/accountMasters",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = accountVal.searchdata();
		                		return keyword;
		                	}
		                },
		                success: function (data) {
		                    return data;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = accountVal.all();
		        	var accountNameId = 'invoice-account-name-row-detail-'+ idCounter;
		        	accountVal.settext(selectedData[label('cashflow.apInvoice.detail.accountCode')]);
		        	$('#' + accountNameId).val(selectedData[label('cashflow.apInvoice.detail.accountName')]);
		        	$('#' + elementId).val(selectedData[label('cashflow.apInvoice.detail.accountCode')])
		        }
			});
		    return accountVal;
		},
		
		addApprovalAutoComplete: function(rowId){
			var elementId = 'approval-code-'+ rowId;

			var col1 = title('cashflow.apInvoice.detail.approvalNo');
			var col2 = title('cashflow.apInvoice.detail.amount');
			var col3 = title('cashflow.apInvoice.detail.approvalDate');
			var col4 = title('cashflow.apInvoice.detail.applicant');
			var col5 = title('cashflow.apInvoice.detail.purpose');
			
		    var approvalVal = $('#'+ elementId).tautocomplete({
		        highlight: "",
		        autobottom: true,
				columns: [col1, col2, col3, col4, col5],
				hide: [true, true, true, true, true],
				dataIndex: 0,
		        data: function(){
		        		var x = { keyword: approvalVal.searchdata(), bookingDate: $("#bookingDate").val()}; 
		        		return x;
		        	},
		        ajax: {
		                url: "/apinvoice/getListApprovalAvailable",
		                type: "GET",
		                data: {
		                	keyword: function(){
		                		var keyword = approvalVal.searchdata();
		                		return keyword;
		                	},
		                	bookingDate : function(){
		                		return $("#bookingDate").val();
		                	}
		                },
		                success: function (ret) {
		                    return ret;
		                }
		            },
		        delay: 1000,
		        onchange: function () {
		        	var selectedData = approvalVal.all();
		        	approvalVal.settext(selectedData[col1]);
		        	$('#'+ elementId).val(selectedData[col1]);
		        	
		        	
		        	// UPDATE ALERT FOR APPROVAL STATUS
		        	var approvalNo = selectedData[col1];
		        	var approvalAmount = selectedData[col2];
		        	
		        	// add new alert approval status.
		        	APInvoice.addAlertApprovalStatus(approvalNo, approvalAmount);
		        	
		        	// update alert approval status.
		        	APInvoice.updateAlertApprovalStatus();

		        }
			});
		    return approvalVal;
		},
		
		addAlertApprovalStatus: function(approvalNo, approvalAmount){
			if($('#'+ approvalNo).val() == undefined){
	        	
				// get actualy amount of approval
	        	var arrApprovalAmount = approvalAmount.split(" ");
	        	approvalAmount = arrApprovalAmount[arrApprovalAmount.length - 1];
				
        		var includeGstOriginalAmount = APInvoice.getIncludeGstOriginalAmountBy(approvalNo);
        		includeGstOriginalAmount = accounting.formatMoney(includeGstOriginalAmount, '', 2);
        		
        		var text = APInvoice.buildTextForApprovalStatus(
        									approvalNo, $('#originalCurrencyCode').val(), 
											includeGstOriginalAmount, approvalAmount);
        		
        		var html = '<li id="'+approvalNo+'" amount="'+approvalAmount+'">'+text+'</li>';
        		
        		$('#alertApprovalStatus').append(html);
        		
        		if(APInvoice.approvalJson[approvalNo] == undefined){
        			APInvoice.approvalJson[approvalNo] = approvalAmount;
        		}
        	}
		},
		
		updateAlertApprovalStatus: function(){
			$('#alertApprovalStatus').find('li').each(function(){
        		
    			var apNo = $(this).attr('id');
    			
    			// remove element alert not use.
    			if(APInvoice.getListApprovalNoApplied().indexOf(apNo) < 0){
    				$(this).remove();
    				return;
    			}
    			
        		//
    			var includeGstOriginalAmount = APInvoice.getIncludeGstOriginalAmountBy(apNo);
    			includeGstOriginalAmount = accounting.formatMoney(includeGstOriginalAmount, '', 2);
    			
				var text = APInvoice.buildTextForApprovalStatus(apNo, $('#originalCurrencyCode').val(), 
																		includeGstOriginalAmount, $(this).attr('amount'));
	
				$('#'+ apNo).html(text);
    			
    		});
		},
		
		getListApprovalNoApplied: function(){
			var listApprovalNoApplied = [];
        	
        	$('input').filter(function(){
				return this.id.match(/approval-code-*/);
			}).each(function(){
				var apNo = $(this).val();
				
				// push apNo to list if not exist.
				if(listApprovalNoApplied.indexOf(apNo) < 0){
					listApprovalNoApplied.push(apNo);
				}
				
			});
        	
        	return listApprovalNoApplied;
		},
		
		getIncludeGstOriginalAmountBy: function(approvalNo){
			var includeGstOriginalAmount = 0;
    		
    		$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(index){
				var apNo = $('#approval-code-'+ index).val();
				
				if(approvalNo == apNo){
					includeGstOriginalAmount += Number($(this).autoNumeric('get'));
				}
			});
    		
    		return includeGstOriginalAmount;
		},
		
		buildTextForApprovalStatus: function(approvalNo, currency, applyAmount, amount) {
			var span1 = '<span id="title">' + 'Total amount applied for Approval No. "' + approvalNo + '" is ' + '</span>';
			var span2 = '<span>' + currency + ' ' + applyAmount + ' / ' + amount  + '</span>';
			
			return span1 + span2;
		},
		
		addCalculationAction: function(rowId){
			$('#exclude-gst-original-amount-' + rowId + ', #gst-rate-' + rowId + ', #fx-rate-' + rowId).change(function(event){
				APInvoice.calculateAmountDetail(rowId, event);
				
				// UPDATE ALERT APPROVAL STATUS
				var approvalNo = $('#approval-code-' + rowId).val();

				if(!isEmpty(approvalNo)){
					var approvalAmount = APInvoice.approvalJson[approvalNo];
					APInvoice.addAlertApprovalStatus(approvalNo, approvalAmount);
					APInvoice.updateAlertApprovalStatus();
				}
			});
			
			$('#exclude-gst-converted-amount-' + rowId).change(function(event){
		    	APInvoice.checkAndReCulcalateFxRate(rowId, event);
				
			});
		},
		
		calculateAmountDetail: function(rowId, event){

			let targetId = event.target.id;
			var gstRate = getNewDecimal($('#gst-rate-' + rowId).autoNumeric('get'));
			var exclGstOriginalAmount =  getNewDecimal(Number($('#exclude-gst-original-amount-' + rowId).autoNumeric('get')));
			var fxRate = getNewDecimal($('#fx-rate-' + rowId).autoNumeric('get'));
			var exclCvrtAmount = getNewDecimal(Number($('#exclude-gst-converted-amount-' + rowId).autoNumeric('get')));
			
			if (targetId.indexOf('exclude-gst-original-amount-') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0 && fxRate.toNumber() != 0){
					exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
				}
				
			//} else if (targetId.indexOf('fx-rate-') >= 0) {
			//	if(exclGstOriginalAmount.toNumber() != 0){
			//		exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
			//	}
				
			} else if (targetId.indexOf('exclude-gst-converted-amount-') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0){
					exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
				}
			} else if (targetId.indexOf('fxRate') >= 0) {
				if(exclGstOriginalAmount.toNumber() != 0){
					exclCvrtAmount = exclGstOriginalAmount.times(fxRate);
				}
				
			}
			
			var gstOriginalAmount = exclGstOriginalAmount.times(gstRate).dividedBy( getNewDecimal(100) );
			var gstCvrtAmount = gstOriginalAmount.times(fxRate);
			var inclOriginalAmount = exclGstOriginalAmount.plus(gstOriginalAmount);
			var inclCvrtAmount = exclCvrtAmount.plus(gstCvrtAmount);
			
			$('#fx-rate-' + rowId).autoNumeric('set', fxRate.toNumber());
			$('#exclude-gst-converted-amount-' + rowId).autoNumeric('set', exclCvrtAmount.toNumber());
			$('#gst-original-amount-' + rowId).autoNumeric('set', gstOriginalAmount.toNumber());
			$('#gst-converted-amount-' + rowId).autoNumeric('set', gstCvrtAmount.toNumber());
			$('#include-gst-original-amount-' + rowId).autoNumeric('set', inclOriginalAmount.toNumber());
			$('#include-gst-converted-amount-' + rowId).autoNumeric('set', inclCvrtAmount.toNumber());

			// Summarize
			var exclGstOrginAmountSum = getNewDecimal(0);
			var exclGstCvrtAmountSum = getNewDecimal(0);
			var gstOriginAmountSum = getNewDecimal(0);
			var gstCvrtAmountSum = getNewDecimal(0);
			var inclGstOriginAmountSum = getNewDecimal(0);
			var inclGstCvrtAmountSum = getNewDecimal(0);
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount*/);
			}).each(function(){
				exclGstOrginAmountSum = exclGstOrginAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				exclGstCvrtAmountSum = exclGstCvrtAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				gstCvrtAmountSum = gstCvrtAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				gstOriginAmountSum = gstOriginAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				inclGstCvrtAmountSum = inclGstCvrtAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				inclGstOriginAmountSum = inclGstOriginAmountSum.plus( getNewDecimal($('#'+this.id).autoNumeric('get')) );
			});
			
			// Fill Summarized Values
			$('#excludeGstOriginalAmount').autoNumeric('set', exclGstOrginAmountSum.toNumber());
			$('#excludeGstConvertedAmount').autoNumeric('set', exclGstCvrtAmountSum.toNumber());
			$('#gstConvertedAmount').autoNumeric('set', gstCvrtAmountSum.toNumber());
			$('#gstOriginalAmount').autoNumeric('set', gstOriginAmountSum.toNumber());
			$('#includeGstConvertedAmount').autoNumeric('set', inclGstCvrtAmountSum.toNumber());
			$('#includeGstOriginalAmount').autoNumeric('set', inclGstOriginAmountSum.toNumber());
		},
		
		calculateSubAmountSummarize : function(rowId){
			var fxRate = getNewDecimal($('#fx-rate-' + rowId).autoNumeric('get'));
			// Summarize
			var exclGstOrginAmountSum = getNewDecimal($('#excludeGstOriginalAmount').autoNumeric('get'));
			var exclGstCvrtAmountSum = getNewDecimal($('#excludeGstConvertedAmount').autoNumeric('get'));
			var gstOriginAmountSum = getNewDecimal($('#gstOriginalAmount').autoNumeric('get'));
			var gstCvrtAmountSum = getNewDecimal($('#gstConvertedAmount').autoNumeric('get'));
			var inclGstOriginAmountSum = getNewDecimal($('#includeGstOriginalAmount').autoNumeric('get'));
			var inclGstCvrtAmountSum = getNewDecimal($('#includeGstConvertedAmount').autoNumeric('get'));
			
			exclGstOrginAmountSum = exclGstOrginAmountSum.minus( getNewDecimal($('#exclude-gst-original-amount-'+rowId).autoNumeric('get')) );
			exclGstCvrtAmountSum = exclGstCvrtAmountSum.minus( getNewDecimal($('#exclude-gst-converted-amount-'+rowId).autoNumeric('get')));
			
			inclGstCvrtAmountSum = inclGstCvrtAmountSum.minus( getNewDecimal($('#include-gst-converted-amount-'+rowId).autoNumeric('get')) );
			inclGstOriginAmountSum = inclGstOriginAmountSum.minus( getNewDecimal($('#include-gst-original-amount-'+rowId).autoNumeric('get')));

			gstOriginAmountSum = gstOriginAmountSum.minus( getNewDecimal($('#gst-original-amount-'+rowId).autoNumeric('get')));
			gstCvrtAmountSum = gstCvrtAmountSum.minus( getNewDecimal($('#gst-converted-amount-'+rowId).autoNumeric('get')) );
			
			// Fill Summarized Values
			$('#excludeGstOriginalAmount').autoNumeric('set', exclGstOrginAmountSum.toNumber());
			$('#excludeGstConvertedAmount').autoNumeric('set', exclGstCvrtAmountSum.toNumber());
			
			$('#gstOriginalAmount').autoNumeric('set', gstOriginAmountSum.toNumber());
			$('#gstConvertedAmount').autoNumeric('set', gstCvrtAmountSum.toNumber());
			
			$('#includeGstOriginalAmount').autoNumeric('set', inclGstOriginAmountSum.toNumber());
			$('#includeGstConvertedAmount').autoNumeric('set', inclGstCvrtAmountSum.toNumber());
		},
		
		updateScheduledPaymentDate : function() {
			var issueDate = $('#invoiceIssuedDate').val();
			if(issueDate !== "" && issueDate != undefined){
				var paymentDate = moment(issueDate, "DD/MM/YYYY").add($('#paymentTerm').val(), 'days').format('DD/MM/YYYY');
				$('#scheduledPaymentDate').val(paymentDate);
			}
		},
		
		addCommasNumber: function() {
			APInvoice.disableInputClassDetail();
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', { vMin: '0', mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).autoNumeric('init', {mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^fx-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-rate-*/);
			}).each(function(){
				$(this).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			});
			
			$('#gstRate').autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			$('#fxRate').autoNumeric('init', {vMin : '0', mDec: '10', aPad: false});
			$('#excludeGstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#excludeGstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			$('#gstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#gstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			$('#includeGstOriginalAmount').autoNumeric('init', {mRound: 'D'});
			$('#includeGstConvertedAmount').autoNumeric('init', {mRound: 'D'});
			
			APInvoice.updateCurrency();
			APInvoice.updateConvertedCurrency();
		},
		
		initAutoNumeric: function(rowId) {
			$('#exclude-gst-original-amount-' + rowId).autoNumeric('init', { vMin: '0', mRound: 'D'});
			$('#exclude-gst-converted-amount-' + rowId).autoNumeric('init', {mRound: 'D'});
			$('#gst-original-amount-' + rowId).autoNumeric('init', {mRound: 'D'});
			$('#gst-converted-amount-' + rowId).autoNumeric('init', {mRound: 'D'});
			$('#include-gst-original-amount-' + rowId).autoNumeric('init', {mRound: 'D'});
			$('#include-gst-converted-amount-' + rowId).autoNumeric('init', {mRound: 'D'});
			$('#fx-rate-' + rowId).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
			$('#gst-rate-' + rowId).autoNumeric('init', {vMin : '0', mDec: '10', aPad: false, mRound: 'D'});
		},
		
		validationForm : function(){
			
			var messages = [];
			var valid = true;
			
			// payeeName
			if(isEmpty($('#payeeName').parent().find('input[autocomplete="off"]').val())){
				valid = false;
				messages.push(validate.require(label('cashflow.apInvoice.detail.payeeName')));
			}
			
			// fxRate
			if(isEmpty($('#fxRate').val())){
				valid = false;
				messages.push(validate.require(label('cashflow.apInvoice.detail.fxRate')));
			}

 			// invoiceNo
			var invoiceNo = $('#invoiceNo').val();
	 		if(!/^(.{0,40})$/.test(invoiceNo)){
	 			messages.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.apInvoice.detail.invoiceNo'], '40'));
				valid = false;
			}

			// poNo
			$('input').filter(function(){
				return this.id.match(/po-no-*/);
			}).each(function(){
				var poNo = $(this).val();
		 		if(!/^(.{0,40})$/.test(poNo)){
		 			messages.push(label('cashflow.common.lessThanOrEqualError').format(GLOBAL_MESSAGES['cashflow.apInvoice.detail.poNo'], '40'));
					valid = false;
				}
			});

			$('input').filter(function(){
				return this.id.match(/invoice-account-row-detail-*/);
			}).each(function(){
				if($(this).val().trim().length == 0)
				{
					valid = false;
					messages.push(validate.require(label('cashflow.apInvoice.detail.accCode')));
				}
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-project-row-detail-*/);
			}).each(function(){
				if($(this).val().trim().length == 0)
				{
					valid = false;
					messages.push(validate.require(label('cashflow.apInvoice.detail.prjCode')));
				}
			});
			
			// check a invoice should have at least 1 detail.
			if(APInvoice.dtDetail.data().length == 0){
				valid = false;
				messages.push(label('cashflow.apInvoice.detail.apInvoiceDetail'));
			}
			
			// When data is valid, then submit form
			if(valid) {
				
				$('#isConfirmChanged').val(false);
				
				APInvoice.reFormatNumberic();
				
				$('#form1').submit();
				
			} else {
				$('.error-area').showAlertMessage(messages);
			}
			
		},
		
		disableInputClassDetail : function(){

			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-account-name-row-detail-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/invoice-project-name-row-detail-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			// binh.lv CF-630
			$('input').filter(function(){
				return this.id.match(/fx-rate-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/gst-rate-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
			
			$('input').filter(function(){
				return this.id.match(/gst-type-*/);
			}).each(function(){
				$(this).attr('readonly',true);
			});
		},
		
		// update currency label Amount Exclude GST (original)
		updateCurrency : function(){
			$('#detail-invoice-table span.inv-currency, .exclude-gst-original-currency').html($('#originalCurrencyCode').val());
		},
		
		// update converted currency label
		updateConvertedCurrency : function(){
			$('#detail-invoice-table span.sub-currency').html($('#convertedCurrencyCode').val());
		},
		
		reFormatNumberic : function(){
			$('input').filter(function(){
				return this.id.match(/exclude-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/exclude-gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-original-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/include-gst-converted-amount-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^fx-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('input').filter(function(){
				return this.id.match(/^gst-rate-*/);
			}).each(function(){
				$(this).val(Number($(this).autoNumeric('get')));
			});
			
			$('#fxRate').val(Number($('#fxRate').autoNumeric('get')));
			$('#excludeGstOriginalAmount').val(Number($('#excludeGstOriginalAmount').autoNumeric('get')));
			$('#excludeGstConvertedAmount').val(Number($('#excludeGstConvertedAmount').autoNumeric('get')));
			$('#gstOriginalAmount').val(Number($('#gstOriginalAmount').autoNumeric('get')));
			$('#gstConvertedAmount').val(Number($('#gstConvertedAmount').autoNumeric('get')));
			$('#includeGstOriginalAmount').val(Number($('#includeGstOriginalAmount').autoNumeric('get')));
			$('#includeGstConvertedAmount').val(Number($('#includeGstConvertedAmount').autoNumeric('get')));
		},
		
		checkAndReCulcalateFxRate : function(rowId, event){
			var fxRate = getNewDecimal($('#fx-rate-' + rowId).autoNumeric('get'));
	    	var exclGstOriginalAmount = getNewDecimal(Number($('#exclude-gst-original-amount-' + rowId).autoNumeric('get')));
	    	var exclCvrtAmount = getNewDecimal(Number($('#exclude-gst-converted-amount-' + rowId).autoNumeric('get')));
	    	
	    	if(exclGstOriginalAmount.toNumber() > 0){
				var fxRateNew = exclCvrtAmount.dividedBy(exclGstOriginalAmount);
				
				if(fxRateNew.toNumber() != fxRate.toNumber()){
				    
					showPopupMessage2('Your update makes the value of "Fx rate" change to <b>' + fxRateNew + '</b>, do you want to apply the new "Fx rate"?', 
	        			//OK
				    	function(){
					    	$('#fxRate').autoNumeric('set', fxRateNew.toNumber());
					    	$('.fx-rate').val($('#fxRate').val());
							
					    	$('input').filter(function(){
								return this.id.match(/fx-rate-*/);
							}).each(function(index){
								var rowId = index;
						    	APInvoice.calculateAmountDetail(rowId, event);
							});
	        			},
	        			
	        			//Cancel
	        			function(){
						       $('#exclude-gst-converted-amount-' + rowId).val('');
						       $('#exclude-gst-original-amount-' + rowId).val('');
	        			}
	        		);
		        			
					
				}		
			}
		}
		
	};
}());