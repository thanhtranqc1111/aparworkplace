/* HIGH CHARTS*/

Highcharts.chart('myChart', {
    chart: {
        zoomType: 'x',
        backgroundColor: null,
        height: (9 / 16 * 100) + '%' // 16:9 ratio
    },
    title: {
        text: false
    },
    subtitle: {
        text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
    },
    xAxis: {
        type: 'datetime'
    },
    yAxis: {
        title: {
            text: 'Exchange rate'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        area: {
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
            },
            marker: {
                radius: 2
            },
            lineWidth: 1,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            threshold: null
        }
    },

    series: [{
        type: 'area',
        name: 'USD to EUR',
        data: [
	        	[
	        		1167609600000,
	        		0.7537
	        	],
	        	[
	        		1167696000000,
	        		0.7537
	        	],
	        	[
	        		1167782400000,
	        		0.7559
	        	],
	        	[
	        		1167868800000,
	        		0.7631
	        	],
	        	[
	        		1167955200000,
	        		0.7644
	        	],
	        	[
	        		1168214400000,
	        		0.769
	        	],
	        	[
	        		1168300800000,
	        		0.7683
	        	],
	        	[
	        		1168387200000,
	        		0.77
	        	],
	        	[
	        		1168473600000,
	        		0.7703
	        	],
	        	[
	        		1168560000000,
	        		0.7757
	        	],
	        	[
	        		1168819200000,
	        		0.7728
	        	],
	        	[
	        		1168905600000,
	        		0.7721
	        	],
	        	[
	        		1168992000000,
	        		0.7748
	        	],
	        	[
	        		1169078400000,
	        		0.774
	        	],
	        	[
	        		1169164800000,
	        		0.7718
	        	],
	        	[
	        		1169424000000,
	        		0.7731
	        	],
	        	[
	        		1169510400000,
	        		0.767
	        	],
	        	[
	        		1169596800000,
	        		0.769
	        	],
	        	[
	        		1169683200000,
	        		0.7706
	        	]
	        ]
    }]
});
		

Highcharts.chart('container', {
    chart: {
        zoomType: 'x',
        backgroundColor: null,
        height: (9 / 16 * 100) + '%' // 16:9 ratio
    },
    title: {
        text: false
    },
    subtitle: {
        text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
    },
    xAxis: {
        type: 'datetime'
    },
    yAxis: {
        title: {
            text: 'Exchange rate'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        area: {
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
            },
            marker: {
                radius: 2
            },
            lineWidth: 1,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            threshold: null
        }
    },

    series: [{
        type: 'area',
        name: 'USD to EUR',
        data: [
        	[
        		1176249600000,
        		0.7454
        	],
        	[
        		1176336000000,
        		0.7427
        	],
        	[
        		1176422400000,
        		0.7391
        	],
        	[
        		1176681600000,
        		0.7381
        	],
        	[
        		1176768000000,
        		0.7382
        	],
        	[
        		1176854400000,
        		0.7366
        	],
        	[
        		1176940800000,
        		0.7353
        	],
        	[
        		1177027200000,
        		0.7351
        	],
        	[
        		1177286400000,
        		0.7377
        	],
        	[
        		1177372800000,
        		0.7364
        	],
        	[
        		1177459200000,
        		0.7328
        	]
        ]
    }]
});
		
Highcharts.chart('myChartThreeD', {
    chart: {
        type: 'column',
        backgroundColor: null,
        height: (9 / 16 * 100) + '%', // 16:9 ratio
    },

    title: {
        text: 'Total Amount AR Invoice, AP Invoice by month'
    },

    xAxis: {
        categories: ['Oct', 'Now', 'Dec', 'Jan', 'Feb'],
        labels: {
            skew3d: true,
            style: {
                fontSize: '16px'
            }
        }
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Amount',
            skew3d: true
        }
    },

    tooltip: {
        headerFormat: '<b>{point.key}</b><br>',
        pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
    },

    plotOptions: {
        column: {
            stacking: 'normal',
            depth: 40
        }
    },

    series: [{
        name: 'Paid',
        data: [23000, 11000, 14560, 50000, 10980],
        stack: 'AR'
    }, {
        name: 'Not Paid',
        data: [16010, 21000, 17560, 19800, 19888],
        stack: 'AR'
    }, {
        name: 'Received',
        data: [23010, 14000, 11560, 20000, 40980],
        stack: 'AP'
    }, {
        name: 'Not Received',
        data: [13010, 24000, 10560, 29800, 10980],
        stack: 'AP'
    }]
});
