 /**
 * @author ThoaiNH 
 * create Oct 26, 2017
 */
(function () {
	userAccountDetail = {
		role: null,
		settedSubsidiaryIds:[],
		init: function (role) {
			userAccountDetail.role = role;
			$('.checkbox-subsidiaty').each(function(){
				if($(this).is(":checked") == true)
				{
					userAccountDetail.settedSubsidiaryIds.push(Number($(this).attr('subId')));
				}
			});
			//auto set all subsidiary when ADMIN creates new account
			var roleType = $("#select-role option:selected").attr('roleType');
			if(roleType == 'ADM' && $("#user-account-id").val() == 0){
				$('.checkbox-subsidiaty').prop("checked", true);
				 $('.checkbox-subsidiaty').each(function(){
					 userAccountDetail.settedSubsidiaryIds.push(Number($(this).attr('subId')));
				 });
			}
		},
		eventListener: function () {
			$(document).on('click','.btn-change-pass',function(){
				$('.change-pass-fieldset').slideToggle();
			});
			
			$(document).on('click','.btn-apply',function(){
				if(userAccountDetail.role == 'SYS')
					userAccountDetail.saveForAdmin();
				else
					userAccountDetail.save();
			});
			
			$(document).on('click','.btn-cancel',function(){
				//history.go(-1);
				window.location='/userAccount/list';
			});
			
			$(document).on('click','.btn-reset',function(){
				window.location.reload();
			});
			
			$(document).on('click','.btn-gen-pass',function(){
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/userAccount/generatePassword',
		            data: {},
		            success : function(ajaxResult) {
		               var result = JSON.parse(ajaxResult);
		               if(result.STATUS == "SUCCESS") {
		            	  $('#gen-password').val(result.RESULT);
		               }
		            }
		        });
			});
			
			$(document).on('click','.checkbox-subsidiaty',function(){
				 var subId = Number($(this).attr('subId'));
				 if($(this).is(':checked')){
					 userAccountDetail.settedSubsidiaryIds.push(subId);
			     } else {
			    	 userAccountDetail.settedSubsidiaryIds = userAccountDetail.settedSubsidiaryIds.filter(function(id){
			    		 return id != subId;
			    	 });
			     }
			});
			
			$(document).on('change','#select-role',function(){
				 var roleType = this.options[this.selectedIndex].getAttribute('roleType');
				 var crrole = this.options[this.selectedIndex].getAttribute('crrole');
				 userAccountDetail.settedSubsidiaryIds = [];
				 $('.checkbox-subsidiaty').prop("checked", false);
				 if(roleType == 'ADM'){
					 $('.checkbox-subsidiaty').prop("checked", true);
					 $('.checkbox-subsidiaty').each(function(){
						 userAccountDetail.settedSubsidiaryIds.push(Number($(this).attr('subId')));
					 });
				 }
				 //when edit detail of a user, current subsidiary must be re-check when user re-select current role
				 if(crrole == 'crrole'){
					 $('.checkbox-subsidiaty').prop("checked", false);
					 $('.checkbox-subsidiaty[crsub]').each(function(){
						 $(this).prop("checked", true);
						 userAccountDetail.settedSubsidiaryIds.push(Number($(this).attr('subId')));
					 });
				 }
			});
			
			$(document).on('click','.btn-toogle-2fa',function(){
				var userId = $("#user-account-id").val();
				var value = $(this).attr('value');
				userAccountDetail.toogle2FA(userId,value);
			});
		},
		save: function(){
			var updatePasswordStatus = 0;
			if($('.change-pass-fieldset').css('display') == 'block')
				updatePasswordStatus = 1;
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/userAccount/saveMyProfile',
	            data: {
    				id: $("#user-account-id").val(),
    				password: $("#password").val().trim(),
    				newPassword: $("#new-password").val().trim(),
    				reNewPassword: $("#confirm-password").val().trim(),
    				firstName: $("#first-name").val().trim(),
    				lastName: $("#last-name").val().trim(),
    				phone: $("#phone").val().trim(),
    				updatePasswordStatus: updatePasswordStatus,
    				defaultSubsidiaryId: document.getElementById('select-default-sub').value
	            },
	            success : function(ajaxResult) {
	               var result = JSON.parse(ajaxResult);
	               if(result.STATUS == "SUCCESS") {
	            	   $(".alert-warning").hide();
	            	   showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
	            		   window.location.reload();
	            	   });
	               }
	               else
	               {
	            	   var arrMessage = [];
	            	   result.RESULT.forEach(function(item){
	            		   arrMessage.push(item.defaultMessage);
	            	   });
	            	   $(".alert-warning").showAlertMessage(arrMessage);
	               }
	            }
	        });
		},
		saveForAdmin: function(){
			var updatePasswordStatus = 0;
			if($('.change-pass-fieldset').css('display') == 'block')
			{
				updatePasswordStatus = 1;
			}
			var data = {
				id: $("#user-account-id").val(),
				email: $("#email").val().trim(),
				newPassword: $("#new-password").val().trim(),
				reNewPassword: $("#confirm-password").val().trim(),
				firstName: $("#first-name").val().trim(),
				lastName: $("#last-name").val().trim(),
				phone: $("#phone").val().trim(),
				isActive: $('input[name="optionsEnable"]:checked').val(),
				updatePasswordStatus: updatePasswordStatus,
				roleId: document.getElementById('select-role').value,
		        settedSubsidiaries: userAccountDetail.settedSubsidiaryIds
			};
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/userAccount/admin.save',
	            data: JSON.stringify(data),
	            contentType: 'application/json',
	            success : function(ajaxResult) {
	               var result = JSON.parse(ajaxResult);
	               if(result.STATUS == "SUCCESS") {
	            	   $(".alert-warning").hide();
	            	   showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
	            		   window.location = "/userAccount/list"
	            	   });
	               }
	               else
	               {
	            	   var arrMessage = [];
	            	   result.RESULT.forEach(function(item){
	            		   arrMessage.push(item.defaultMessage);
	            	   });
	            	   $(".alert-warning").showAlertMessage(arrMessage);
	               }
	            }
	        });
		},
		toogle2FA: function(userId, value){
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/userAccount/toogle2FA',
	            data: {
	            	id: userId,
	            	value: value
	            },
	            success : function(ajaxResult) {
	               var result = JSON.parse(ajaxResult);
	               if(result.STATUS == "SUCCESS") {
	            	   $(".alert-warning").hide();
	            	   showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
	            		   window.location.reload();
	            	   });
	               }
	               else
	               {
	            	   var arrMessage = [];
	            	   result.RESULT.forEach(function(item){
	            		   arrMessage.push(item.defaultMessage);
	            	   });
	            	   $(".alert-warning").showAlertMessage(arrMessage);
	               }
	            }
	        });
		}
	};
}());