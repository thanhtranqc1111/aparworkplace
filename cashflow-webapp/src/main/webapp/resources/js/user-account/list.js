/**
 * @author ThoaiNH 
 * create Oct 27, 2017
 */
(function () {
	userAccountList = {
		dataTable: null,
		txtSearch: "",
		rows_selected: [], //id of checked row in data table
		init: function () {
			userAccountList.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/userAccount/api/getAllPagingUserAccount",
		            type: "post",
		            data: function (d) {
		                d.txtSearch = userAccountList.txtSearch;
		            },
		            error: function() { // error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">No data</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },

		        },
		        columns: [
		        	{
			            "data" : "id",
			            "width" : "1%"
			        }, 
			        {
			            "data" : "firstName",
			            "orderable": false,
			            "width" : "9%"
			        }, 
			        {
			            "data" : "lastName",
			            "orderable": false,
			            "width" : "9%"		
			        },
			        {
			            "data" : "email",
			            "orderable": false,
				        "width" : "17%",
				        "render": function(data, type,full, meta) {
				        	return "<a href='/userAccount/detail/" + full.id +"'>" + data + "</a>";
			            }
			        },
			        {
			            "data" : "isActive",
			            "orderable": false,
			            "width" : "10%",
			            "render": function(data, type,full, meta) {
			            	if(data == true)
			            		return "Active";
			            	else
			            		return "InActive";
			             }
			        }
		        ],
		        "columnDefs": [
		            {
		                'targets' : 0,
		                'searchable' : false,
		                'orderable' : false,
		                'className' : 'dt-body-center',
		                'render' : function(data, type, full, meta) {
			                  return '<input type="checkbox" name="selectedCheckBox" class="select" value="' + full.id+ '">';
		                 }
		            }
		        ],
		        searching: false,
		        "ordering" : false
			});
		},
		eventListener: function () {
			$(document).on('click','.btn-search',function(){
				userAccountList.txtSearch = $('#txt-search').val();
				userAccountList.dataTable.ajax.reload();
			});

			$(document).on('keyup','#txt-search',function(e){
				if(e.keyCode == 13){
					userAccountList.txtSearch = $(this).val();
					userAccountList.dataTable.ajax.reload();
				}
			});
			
			$(document).on("click",".btn-delete",function(){
		    	if(!$(this).hasClass('disabled'))
		    	{
		    		$('#popConfirm').modal('toggle');
			    	$('button[name="btnConfirm"]').on('click', function() {
			    		userAccountList.deleteUserAccount();
			        });
		    	}
		    });
			
			// Handle click on checkbox
		    $('#table_data tbody').on('click', 'input[type="checkbox"]', function(e){
		       var $row = $(this).closest('tr');

		       // Get row data
		       var data = userAccountList.dataTable.row($row).data();

		       // Get row ID
		       var rowId = $(this).val();

		       // Determine whether row ID is in the list of selected row IDs
		       var index = $.inArray(rowId, userAccountList.rows_selected);

		       // If checkbox is checked and row ID is not in list of selected row IDs
		       if(this.checked && index === -1){
		    	   userAccountList.rows_selected.push(rowId);

		       // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
		       } else if (!this.checked && index !== -1){
		    	   userAccountList.rows_selected.splice(index, 1);
		       }

		       if(this.checked){
		          $row.addClass('selected');
		       } else {
		          $row.removeClass('selected');
		       }

		       // Update state of "Select all" control
		       userAccountList.updateDataTableSelectAllCtrl(userAccountList.dataTable);

		       // Prevent click event from propagating to parent
		       e.stopPropagation();
		       
		       userAccountList.renderDeleteButton();
		    });
		   
		    // Handle click on "Select all" control
		    $('thead input[name="select_all"]', userAccountList.dataTable.table().container()).on('click', function(e){
		       if(this.checked){
		          $('#table_data tbody input[type="checkbox"]:not(:checked)').trigger('click');
		       } else {
		          $('#table_data tbody input[type="checkbox"]:checked').trigger('click');
		       }

		       // Prevent click event from propagating to parent
		       e.stopPropagation();
		    });
		    
		},
		updateDataTableSelectAllCtrl: function(table){
			   var $table             = table.table().node();
			   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
			   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
			   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

			   // If none of the checkboxes are checked
			   if($chkbox_checked.length === 0){
			      chkbox_select_all.checked = false;
			      if('indeterminate' in chkbox_select_all){
			      }

			   // If all of the checkboxes are checked
			   } else if ($chkbox_checked.length === $chkbox_all.length){
			      chkbox_select_all.checked = true;
			      if('indeterminate' in chkbox_select_all){
			         chkbox_select_all.indeterminate = false;
			      }

			   // If some of the checkboxes are checked
			   } else {
			      chkbox_select_all.checked = true;
			      if('indeterminate' in chkbox_select_all){
			         chkbox_select_all.indeterminate = true;
			      }
			   }
		},
		renderDeleteButton: function(){
			if(userAccountList.rows_selected.length > 0)
				$('.btn-delete').removeClass('disabled');
			else
				$('.btn-delete').addClass('disabled');
		},
		deleteUserAccount: function(){
			$.ajax({
	            type : "POST",
	            url : CONTEXT_PATH + '/userAccount/delete',
	            data: "&id=" +  userAccountList.rows_selected,
	            success : function(result) {
	            	showPopupMessage(GLOBAL_MESSAGES['cashflow.common.deletedSuccessfully'], function(){
	            		location.reload();
	            	});	               
	            }
	        });
		}
	};
}());