/**
 * @author Binh.lv
 * create May 03, 2018
 */
(function () {
    projectPayment = {

            init: function () {
                // add comma
                projectPayment.addCommasNumber();

                // total project
                $('table').filter(function(){
                    return this.id.match(/payment-payroll-detail-*/);
                }).each(function(){

                    let numCol = $("#"+(this.id)).find("tr th").length;
                    let numRow = $("#"+(this.id)).find("tr").length;
                    
                    var totals=new Array(numCol);
                    for (var i = 0; i < numCol; i++) {
                    	totals[i]=0; // Fills all elements of array with 0
                    }
                    
                    var $dataRows=$("#"+(this.id) +" .rowtable");
                    $dataRows.each(function() {
                            $(this).find('.projectAmount').each(function(i){        
                                var number=getNewDecimal($(this).autoNumeric('get'));
                                totals[i]+=number.toNumber();
                            });
                    });

                    $("#"+(this.id) +" .totalCol").each(function(i){
                        $(this).html(totals[i]);
                    });
                    
                      
                    // set per for Column
                    var widthPerForColumn= 100/numCol + "%";

                    // set scrollY 
                    var scrollY = false; 
                    if(numRow > 20) {
                        scrollY = 700 + "px";
                    }
                    // set width for table
                    var widthTable = document.getElementById(this.id).offsetWidth;
                    var widthTableCaculate = (numCol*168.0+1);
                    
                    if(widthTable > widthTableCaculate) {
                        $("#"+(this.id)).css('width', '100%');
                        if(numRow > 20) {
                            $("#"+(this.id)).DataTable({
                                paging: false,
                                ordering: false,
                                info: false,
                                filter: false,
                                autoWidth: false,
                                scrollX: true,
                                scrollY: scrollY,
                                scrollCollapse: false,
                                drawCallback: function( settings ) {
                                    $(".employeeColumn").css('width','150px');
                                    $("#"+(this.id)).css('width','100%');
                                }
                            });
                        }
                    } else {
                        widthTable = widthTableCaculate + 'px';
                        $("#"+(this.id)).css('width', widthTable);

                        $("#"+(this.id)).DataTable({
                            paging: false,
                            ordering: false,
                            info: false,
                            filter: false,
                            autoWidth: false,
                            scrollX: true,
                            scrollY: scrollY,
                            scrollCollapse: false,
                            fixedColumns:{
                                leftColumns: 3
                            },
                            columnDefs: [
                                {
                                    "targets": '_all',
                                    "width" : widthPerForColumn
                                }
                            ],
                            drawCallback: function( settings ) {
                                $(".employeeColumn").css('width','150px');
                                $(".detail-payment-table-wrapper" + " #"+(this.id)).css('width','100%');
                            }
                        });

                    }

                });

                $('.totalCol').autoNumeric('init', {mRound: 'D'});
            }, // END INIT
            eventListener: function(){
                $('#export').click(function(){
                    
                    $("#export").attr("href", "export" + window.location.search);
                });
            }, // END eventListener

            addCommasNumber : function(){
                $(".projectAmount").autoNumeric('init', {mRound: 'D'});
            }
    };
}());
