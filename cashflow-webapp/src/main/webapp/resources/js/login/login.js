/**
 * @author ThoaiNH 
 * create Mar 12, 2018
 */
(function () {
	login = {
		init: function () {
			
		},
		eventListener: function () {
			$(document).on('click','.btn-pre-login',function(){
				login.preAuthenticateUserAccount($('#email').val(), $('#password').val());
			});
			$(document).on('click','.btn-verify',function(){
				$('.btn-login').trigger('click');
			});
		},
		preAuthenticateUserAccount: function(email, password){
			if(email && email.length > 0 && password && password.length > 0){
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/publicAuthenticate/preAuthenticateUserAccount',
		            data: {
		            	email : email,
		            	password : password
		            	
		            },
		            success : function(ajaxResult) {
		            	var resultData = JSON.parse(ajaxResult);
		            	if(resultData.STATUS == "SUCCESS"){
		            		//user doesn't use 2fa
		            		if(resultData.RESULT == false)
		            		{	
		            			var url = "/camunda/api/admin/auth/user/login/welcome?username=" + email + "&password=" + password;
		        				$.get(url, function( data ) {
		        					$('.btn-login').trigger('click');
		        				});
		            		}
		            		else 
		            		{
		            			$('.div-normal-login').hide();
		            			$('.div-otp-confirmation').show();
		            		}
		            	}
		            	else {
		            		 window.location = '/login?error=INVALID_USERNAME_CREDENTIALS'
		            	}
		            }
		        });
			}
		}
	};
}());