/**
 * @author ThoaiNH 
 * create Feb, 09 2018
 */
(function () {
	bankStatementWebSocket = {
		stompClient: null,
		flag: true,//set false to prevent user subscribes himself
		editorSelector: ".btn-add-new-row, .btn-clean-payment-order, .btn-add-inline, .btn-edit-inline, .btn-remove-inline, " + 
						".btn-save-all, .auto-match-function, .temp-payment-order-no, .btn-clean-payment-order, .btn-clean-receipt-order, .btn-auto-matching-payment",
		init: function(){
			bankStatementWebSocket.connect();
		},
		setConnected: function(connected){
			
		},
		connect: function(){
			var socket = new SockJS('/ws');
            stompClient = Stomp.over(socket);  
            stompClient.connect({}, function(frame) {
            	bankStatementWebSocket.setConnected(true);
                //console.log('Connected: ' + frame);
                stompClient.subscribe('/bankStatementStompController/messages', function(messageOutput) {
                	if(messageOutput.body != null)
            		{
                		var bankInfo = JSON.parse(messageOutput.body);
                		if(bankStatementWebSocket.flag == true && bankInfo.bankName == bankStatement.bankName && bankInfo.bankAccNo == bankStatement.bankAccountNo && bankInfo.subsidiaryId == $('#currentSubsidiaryId').val())
                			bankStatementWebSocket.disableEdit(bankInfo.userName);
            		}
            		else {
            			bankStatementWebSocket.enableEdit();
            		}
                });
            });
		},
		disconnect: function(){
			if(stompClient != null) {
                stompClient.disconnect();
            }
            setConnected(false);
            console.log("Disconnected");
		},
		sendMessage: function(subsidiaryId, bankName, bankAccNo, userName){
			bankStatementWebSocket.flag  = false;
            stompClient.send("/app/bankStatementStompController/edit", {}, JSON.stringify({subsidiaryId: subsidiaryId, 'bankName':bankName, 'bankAccNo':bankAccNo, userName: userName}));
		},
		disableEdit: function(userNameEditing){
			$(bankStatementWebSocket.editorSelector).hide();
			bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message15'].replace("%", userNameEditing));
		},
		enableEdit: function(){
			$(bankStatementWebSocket.editorSelector).show();
			bankStatement.hideWarning();
		}
	};
}());