/**
 * @author ThoaiNH 
 * create Oct 10, 2017
 */
(function () {
	bankStatement = {
		dataTable: null, //main data table
		dataList: null,//list data of main table
		crNewId: 0,//id of new bank statement record
		//search panel value
		endingBalance: null,
		bankAccountNo: "",
		bankName: "",
		transactionFrom: null,
		transactionTo: null,
		transactionNumber: 0,
		maxTransactionDate: null, //max value of transaction date field in bank_statement table.
		intitedDataTable: false,
		//name of column in data table
		fieldIndex: {2: "transactionDate", 3: "valueDate", 6: "description1", 7: "description2", 8: "reference1", 9: "reference2", 10: "debit", 11: "credit", 12:"balance", 13: "paymentOrderNo"},
		columnIndexEdit: {
			TRANS_NO: 1,
			TRANSACTION_DATE: 2,
			VALUE_DATE: 3,
			CURRENCY: 4,
			BEGINNING_BALANCE: 5,
			DESCRIPTION1: 6,
			DESCRIPTION2: 7,
			REFERENCE1: 8,
			REFERENCE2: 9,
			DEBIT: 10,
			CREDIT: 11,
			BALANCE: 12,
			PAYMENT_ORDER_NO: 13
		},
		columnIndexDOM: {
			TRANSACTION_NO: 1,
			TRANS_NO: 2,
			TRANSACTION_DATE: 3,
			VALUE_DATE: 4,
			CURRENCY: 5,
			BEGINNING_BALANCE: 6,
			DESCRIPTION1: 7,
			DESCRIPTION2: 8,
			REFERENCE1: 9,
			REFERENCE2: 10,
			DEBIT: 11,
			CREDIT: 12,
			BALANCE: 13,
			PAYMENT_ORDER_NO: 14,
			ACTION: 16
		},
		//handler function of row user edit/add. Call after reload datatable
		functionModify: null,
		init: function () {
			 //get default search criteria
			 bankStatement.bankName = $('#search-bank-statementType').val();
             bankStatement.bankAccountNo = $('#search-bank-statementAccount').val();
             bankStatement.transactionFrom = $('#search-transactionFrom').val();
             bankStatement.transactionTo = $('#search-transactionTo').val();
			 bankStatement.transactionNumber = $('#transactionNumber').val();
			 bankStatement.getBankAccInfo();
			 $("#search-bank-trans-date").val($("#transDateFilter").val());
			 if($("#transDateFilter").val() == 0){
				 $('.panel-from-to-date').css("visibility", "visible")
			 }
			 //set up number mask for input
			 setupInputNumberMask();
			 
			 //set up data table
			 bankStatement.dataTable = $('#table_data').removeAttr('width').DataTable({
		        "ajax": {
		            url: "/bankStatement/api/getAllPagingBankStatement",
		            type: "GET",
		            data: function ( d ) {
		                d.bankName = bankStatement.bankName;
		                d.bankAccNo = bankStatement.bankAccountNo;
		                d.transactionFrom = bankStatement.transactionFrom;
		                d.transactionTo = bankStatement.transactionTo;
		                d.transactionNumber = bankStatement.transactionNumber;
		                d.bankCode = $( "#search-bank-statementType option:selected" ).attr('bankcode');
		                d.transDateFilter = $("#search-bank-trans-date" ).val();
		                d.subsidiaryId = $('#currentSubsidiaryId').val();
		                //auto sort by transaction date ASC if user modifying data
		                /*if(bankStatement.functionModify != null) {
			                d.order[0]['column'] = 1;
			                d.order[0]['dir'] = 'asc';
		                }*/
		            },
		            error: function() { 
		            	// error handling
		                $(".table_data-error").html("");
		                $("#table_data").append('<tbody class="table_data-error"><tr><td colspan="10">' + GLOBAL_MESSAGES['cashflow.common.noData'] + '</td></tr></tbody>');
		                $("#table_data_processing").css("display", "none");
		            },
		            complete: function(data){
		            	//update max transaction date
		            	if(data.responseJSON.maxTransactionDate)
		    			{
		            		bankStatement.setMaxTransactionDate(data.responseJSON.maxTransactionDate);
		    			}
		            	else
		            	{
		            		bankStatement.setMaxTransactionDate(null);
		            	}
		            	
		            	if(data.responseJSON.error){
		            		bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message10']);
		            	}
		            	else {
			            	bankStatement.dataList = data.responseJSON.data;
			            	bankStatement.dataList.forEach(function(item){
			            		item.createdDate = item.createdDate.dayOfMonth + "/" + (item.createdDate.month + 1) + "/" + item.createdDate.year;
			            		if(item.transactionDate)
			            		{
			            			item.transactionDate = item.transactionDate.dayOfMonth + "/" + (item.transactionDate.month + 1) + "/" + item.transactionDate.year;
			            		}
			            		if(item.valueDate)
			            		{
			            			item.valueDate = item.valueDate.dayOfMonth + "/" + (item.valueDate.month + 1) + "/" + item.valueDate.year;
			            		}
			            	});
			            	//INIT table editable
			            	if((GLOBAL_PERMISSION["PMS-003"] || GLOBAL_PERMISSION["PMS-002"]) && bankStatement.dataList.length > 0)
			            	{
			            		$('#table_data').enableEditor(false);
		            			bankStatement.intitedDataTable = true;
			            	}
		            	}
		            	//disable save button
						hideAjaxStatus();
						//edit event
						if(bankStatement.functionModify != null)
						{
							bankStatement.functionModify();
							bankStatement.functionModify = null;
						}
						if(bankStatement.bankName.trim().length > 0 && bankStatement.bankAccountNo.trim().length > 0)
						{
							$('.btn-add-new-row').removeClass('disabled');
						}
						else {
							$('.btn-add-new-row').addClass('disabled');
						}
						//render editor based on role
						if(data.responseJSON.roleEdit == 1 || bankStatementWebSocket.flag == false)
		            	{
		            		bankStatementWebSocket.enableEdit();
		            	}
		            	else {
		            		//disable when other user is editing the same bank account
		            		bankStatementWebSocket.disableEdit(data.responseJSON.userEditing);
		            		$(".btn-clean-payment-order, .btn-add-inline, .btn-edit-inline, .btn-remove-inline").remove();
		            	}
						/*
						 * update 16/05/2018
						 * hide add button for transactions have the same transaction date
						 */
						try {
							var transactionDate = null;
				        	for(i = 0; i < bankStatement.dataList.length; i++){
				        		var item = bankStatement.dataList[i];
				        		if(transactionDate != null){
									if(transactionDate == item.transactionDate)
									{
										$("#table_data input[type='checkbox'][value='" + bankStatement.dataList[i-1].id + "']").parents('tr').find('.btn-add-inline').hide();
									}
									else {
										$("#table_data input[type='checkbox'][value='" + bankStatement.dataList[i-1].id + "']").parents('tr').find('.btn-add-inline').show();
									}
								}
				        		transactionDate = item.transactionDate;
				        	}
						} catch (e) {
							console.log(e);
						}
		            }
		        },
		        columns: [
		        	{
			            "data" : "id",
			            "width" : "0%"
			        },
			        {
			            "data" : "id",
			            "orderable": false
			        },
			        {
			            "data" : "transactionDate",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatCalendar(data);
			             }
			        }, 
			        {
			            "data" : "valueDate",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	if(full.valueDate)
			            		return formatCalendar(full.valueDate);
			            	else
			            		return "";
			             }
			        },
			        {
			            "data" : "currencyCode",
			            "orderable": false,
			        },
			        {
			            "data" : "beginningBalance",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "description1",
			            "orderable": false,
			        },
			        {
			            "data" : "description2",
			            "orderable": false,
			        },
			        {
			            "data" : "reference1",
			            "orderable": false,
			        },	
			        {
			            "data" : "reference2",
			            "orderable": false,
			        },
			        {
			            "data" : "debit",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "credit",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "balance",
			            "orderable": false,
			            "render": function(data, type,full, meta) {
			            	return formatStringAsNumber(data);
			            }
			        },
			        {
			            "data" : "paymentOrderNo",
			            "orderable": false,
				        "render": function(data, type,full, meta) {
				        	if(full.paymentOrderId && full.paymentOrderId > 0)
				        		return "<a target='blank' href='/payment/detail/" + data + "'>" + data + "</a>"
				        	else if(data != null)
				        		return data;
				        	else
				        		return "";
			             }	
			        },
			        {
			            "data" : "receiptOrderNo",
			            "orderable": false,
				        "render": function(data, type,full, meta) {
				        	if(data){
					        	var res = data.split(",");
					        	var html = "";
					        	$.each(res, function(i, obj) {
					        		html += "<a href='/receiptOrder/list/" + obj + "' target='_blank' >" + obj +"</a> "
				            		if ( i != res.length - 1){
				            			html += ", ";
				            		}
				            		else
				            		{
				            			if(GLOBAL_PERMISSION["PMS-003"]){
				            				html += "<i title='Delete Collection Management No. from this statement' class='btn-clean-receipt-order fa fa-trash'></i>";
				            			}
				            		}
				            	});
					        	return html;
				        	}
				        	else {
				        		return "";
				        	}
			             }	
			        },
			        {
			            "data" : "",
			            "orderable": false,
				        "render": function(data, type, full, meta) {
				        	if(bankStatement.transactionNumber == 0){
					        	var htmlString = "<div class='row'>";
					        	if(GLOBAL_PERMISSION["PMS-002"])
					        	{
					        		htmlString += '<div class="col-md-4 col-xs-3 btn-add-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.addNewRowBelow'] + '" style="cursor:pointer;" class="fa fa-plus-square-o "></i></div>';
					        	}
					        	
					        	if(!full['paymentOrderId'] && !full['receiptOrderNo']){
					        		if(GLOBAL_PERMISSION["PMS-003"])
					        		{
					        			htmlString += '<div class="col-md-4 col-xs-3 btn-edit-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.edit'] + '" style="cursor:pointer;" class="fa fa-pencil-square-o "></i></div>';
					        		}
					        		if(GLOBAL_PERMISSION["PMS-004"]){
					        			htmlString += '<div class="col-md-4 col-xs-3 btn-remove-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.remove'] + '" style="cursor:pointer;" class="fa fa-trash-o "></i></div>';
					        		}
					        	}
					        	else {
					        		if(GLOBAL_PERMISSION["PMS-003"])
					        		{
					        			htmlString += '<div class="col-md-4 col-xs-3 btn-edit-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.edit'] + '" style="cursor:pointer;" class="fa fa-pencil-square-o  assigned-payment"></i></div>';
					        		}
					        	}
					        	htmlString += '</div><div class="row"><button style="display:none;" class="btn btn-info btn-success-inline">' + GLOBAL_MESSAGES['cashflow.bankStatement.submit'] + '</button></div>';
					        	return htmlString;
				        	}
				        	else 
				        	{
				        		var htmlString = "<div class='row'>";
				        		if(GLOBAL_PERMISSION["PMS-003"])
				        		{
				        			htmlString += '<div class="col-md-4 col-xs-3 btn-edit-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.edit'] + '" style="cursor:pointer;" class="fa fa-pencil-square-o  assigned-payment"></i></div>';
				        		}
					        	htmlString += '</div><div class="row"><button style="display:none;" class="btn btn-info btn-success-inline">' + GLOBAL_MESSAGES['cashflow.bankStatement.submit'] + '</button></div>';
					        	return htmlString;
				        	}
			             }	
			        }
		        ],
		        "columnDefs": [
		            {
		                'targets' : 0,
		                'searchable' : false,
		                'className' : 'dt-body-center',
		                'render' : function(data, type, full, meta) {
			                  return '<input bankAccountNo="' + full.bankAccountNo + '" bankName="' + full.bankName + '" type="checkbox" name="selectedCheckBox" class="select" ref2="' + full.reference2 +'" value="' + full.id+ '">';
		                 }
		            },
		            {
		                "targets": [
		                	bankStatement.columnIndexEdit.TRANSACTION_DATE,
		                	bankStatement.columnIndexEdit.VALUE_DATE,
		                	bankStatement.columnIndexEdit.DEBIT,
		                	bankStatement.columnIndexEdit.CREDIT,
		                	bankStatement.columnIndexEdit.BEGINNING_BALANCE,
		                	bankStatement.columnIndexEdit.BALANCE
		                ],
		                "className": "text-right",
		            },
		            {
		                "targets": [1],
		                "className": "text-center",
		            }
		        ],
		        "searching": false,
		        "paging": false,
		        "info": true,
		        "processing" : false,
		        ordering: false,
		        "language" : {
		            "info": "Showing _END_ record(s) in _TOTAL_ record(s)"
		          }
			 });
			 
			 //setup datepicker
			 $('.ap-date-picker.form-group-transection-preiod').datepicker({
		    	format: 'dd/mm/yyyy',
		        orientation: 'auto bottom',
		        todayHighlight: true
		     });
			 
		},
		eventListener: function () {
			//on click save button
			$(document).on("click",".btn-save-all",function(){
				if(!$(this).hasClass('disabled'))
					bankStatement.save();
			});
			
		    //on change bank name
		    $("#search-bank-statementType").on('change', function() {
		    	bankStatement.resetDatatable();
		    	if($('#search-bank-statementType').val().trim().length > 0){
		    		var bankCode = this.options[this.selectedIndex].getAttribute('bankCode');
			    	//load bank account belong to this bank name
			    	bankStatement.getBankAccountMasterByBankCode(bankCode, function(ajaxResult){
			    		   var resultData = JSON.parse(ajaxResult);
			    		   $("#search-currency").val("");
					       $("#search-beginning-balance").val("");
					       $("#search-ending-balance").val("");
			               $("#search-bank-statementAccount").html("");
			               bankStatement.endingBalance = null;
			               bankStatement.bankAccountNo = "";
			               if(resultData.STATUS == "SUCCESS") {
			            	   if(resultData.RESULT.length > 0){
			            		   var htmlString = "<option value=''>" + GLOBAL_MESSAGES['cashflow.bankStatement.all'] + "</option>";
			            		   resultData.RESULT.forEach(function(item){
				            		   htmlString = htmlString + ("<option currencyCode='" + item.currencyCode + "' beginningBalance='" + (item.beginningBalance  != null ? item.beginningBalance : '0') + 
				            				   				     "' endingBalance='" + (item.endingBalance != null ? item.endingBalance : '0') + "' value='" + item.bankAccountNo + "'>"+ item.bankAccountNo +"</option>");
				            	   });
				            	   $("#search-bank-statementAccount").html(htmlString);
			            	   }
			               }
			    	});
		    	}
		    	else {
		    		 var htmlString = "<option value=''>" + GLOBAL_MESSAGES['cashflow.bankStatement.all'] + "</option>";
		    		 $("#search-bank-statementAccount").html(htmlString);
		    	}
		    });
		    
		    //on click search
		    $(document).on("click",".btn-search",function(){
		    	var bankName = $('#search-bank-statementType').val();
		    	var bankAccount = $('#search-bank-statementAccount').val();
				if(bankName.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message2']);
					resetTable = true;
				}
				else if(bankAccount.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message1']);
					resetTable = true;
				}
				else if(bankStatement.endingBalance == null)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message12']);
					resetTable = true;
				}
				else {
				    //clear data list
				    bankStatement.dataList = [];
				    bankStatement.hideWarning();
			    	bankStatement.transactionNumber = 0;
			    	bankStatement.bankName = $('#search-bank-statementType').val();
	                bankStatement.bankAccountNo = $('#search-bank-statementAccount').val();
	                bankStatement.transactionFrom = $('#search-transactionFrom').val();
	                bankStatement.transactionTo = $('#search-transactionTo').val();
			    	bankStatement.dataTable.ajax.reload();
				}
		    });
		    
		    //update list bank account when change bank name
		    $("#search-bank-statementAccount").on('change', function() {
		    	bankStatement.resetDatatable();
		    	if($("#search-bank-statementAccount").val() == '0')
		    	{
		    		bankStatement.endingBalance = null;
		    	}
		    	else {
		    		bankStatement.updateMaxTransactionDate($("#search-bank-statementType").val(), $("#search-bank-statementAccount").val());
			    	bankStatement.getBankAccInfo();
		    	}
		    });
		    
		    //on click button add new row
			$(document).on("click",".btn-add-new-row",function(){
				var bankName = bankStatement.bankName;
		    	var bankAccount = bankStatement.bankAccountNo;
				if(bankName.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message2']);
				}
				else if(bankAccount.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message1']);
				}
				else if(bankStatement.endingBalance == null)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message12']);
				}
				 else {
					 if(bankStatement.validateDataList(bankStatement.dataList, true)){
						$('#table_data tbody .dataTables_empty').parents('tr').remove();
				    	var numberCol = document.getElementById('table_data').rows[0].cells.length;
				    	var rowIndex = bankStatement.dataList.length - 1;
				    	var endingBalance =  bankStatement.endingBalance;
				    	
			    		bankStatement.onBeforeModifyData(function(){
			    			var showingList = bankStatement.dataList.filter(function(item){
			    				return item['removed'] != true;
			    			});
			    			if(showingList.length > 0)
			    			{
			    				$('#table_data').find('.btn-add-inline:last').trigger('click');
			    			}
			    			else {
			    				var transactionDate = formatDate(new Date());
			    				$('#table_data').addNewBankStatementRow(endingBalance, -1, transactionDate);
					    		$("#table_data input[type='checkbox'][value='" + 'new-' + bankStatement.crNewId + "']").parents('tr').find('.btn-edit-inline').trigger('click');
			    			}
					    	
				    	});
			    	}
				 }
				 bankStatement.onAfterAddToDataList();
			});
			   
		    $(document).on("click",".btn-import",function(){
		    	 var bankName = $('#search-bank-statementType').val();
				 if(!bankStatement.endingBalance && bankStatement.endingBalance == null)
				 {
					 bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message1']);
				 }
				 else if(bankName.trim().length <= 0)
				 {
					 bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message2']);
				 }
				 else {
					 var bankName = $('#search-bank-statementType').val();
	                 var bankAccountNo = $('#search-bank-statementAccount').val();
	                 var currencyCode = $('#search-currency').val();
			    	 $.ajax({
			            type : "POST",
			            url : CONTEXT_PATH + '/bankStatement/selectImportTemplate',
			            data: "bankName=" + bankName + "&bankAccCode=" + bankAccountNo + "&currencyCode=" + currencyCode,
			            success : function(result) {
			            	 window.location = "/bankStatement/import"              
			            }
			         });
				 }
		    });
		    
		    $(document).on("click",".btn-export",function(){
		    	var bankName = $('#search-bank-statementType').val();
		    	var bankAccountNo = $('#search-bank-statementAccount').val();
		    	var transactionFrom = $('#search-transactionFrom').val();
		    	var transactionTo = $('#search-transactionTo').val();
		    	var transDateFilter = $("#search-bank-trans-date").val();
				if(bankName.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message2']);
				}
				else if(bankAccountNo.trim().length == 0)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message1']);
				}
				else if(bankStatement.endingBalance == null)
				{
					bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message12']);
				}
				else {
	                var url = "/bankStatement/export?bankName=" + bankName + 
	                		  "&bankAccNo=" + bankAccountNo +
	                		  "&transactionFrom=" + transactionFrom +
	                		  "&transactionTo=" + transactionTo +
	                		  "&transDateFilter=" + transDateFilter +
	                		  "&transactionNumber=" + bankStatement.transactionNumber;
	                window.location = url;
				}
		    });
		    
		    //button edit in table row
		    $(document).on("click","#table_data .btn-edit-inline",function(){
		    	if($(this).parents('tr').find('.btn-success-inline').css('display') == 'none'){
		    		var crEdittingId = $(this).parents('tr').find("input[type='checkbox']").val();
		    		var self = $(this);
		    		bankStatement.validateTransactionDateBeforeEdit(crEdittingId, function(){
		    			$('#table_data .awhite table').hide();
				    	//turn off on editing row
				    	$("#table_data .btn-success-inline").hide();
						var hasAssignedPayment = self.find('i').hasClass('assigned-payment');
				    	bankStatement.onBeforeModifyData(function(){
				    		bankStatement.initEditor(crEdittingId);
				    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('.btn-edit-hidden:first').trigger('click');
				    		//do not allow edit transaction date, value date, credit, debit when assign statement for a payment
				    		if(hasAssignedPayment == true){
				    			$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + ') input').hide();
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + ') span').show();
					    		
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.VALUE_DATE + ') input').hide();
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.VALUE_DATE + ') span').show();
					    		
				    			$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.DEBIT + ') input').hide();
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.DEBIT + ') span').show();
					    		
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.CREDIT + ') input').hide();
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.CREDIT + ') span').show();
					    		
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ') input').hide();
					    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ') span').show();
				    		}
				    		else {
				    			
				    			setTimeout(function(){
				    				$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + ') input').datepicker('hide');
					    			$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.VALUE_DATE + ') input').datepicker('hide');
					    			$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.DESCRIPTION1 + ') input').focus();
								}, 100);
				    		}
				    		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').find('.btn-success-inline').show();
				    	});
		    		});
		    	}
		    });
		    
		    //button add in table row
		    $(document).on("click","#table_data .btn-add-inline",function(){
		    	$('#table_data .awhite table').hide();
		    	if(bankStatement.validateDataList(bankStatement.dataList, true)){
			    	var numberCol = document.getElementById('table_data').rows[0].cells.length;
			    	var crEdittingId = $(this).parents('tr').find("input[type='checkbox']").val();
			    	var rowIndex = $(this).parents('tr').index();
			    	var endingBalance =  formatNumber($(this).parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.BALANCE + '):first').text());
			    	var transactionDate = $(this).parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + '):first').text();
		    		bankStatement.onBeforeModifyData(function(){
		    			$('#table_data tr').find('th:eq(' + (numberCol - 1) + '),td:eq(' + (numberCol - 1) + ')' ).remove();
				    	$('#table_data').addNewBankStatementRow(endingBalance, rowIndex, transactionDate);
			    		$("#table_data input[type='checkbox'][value='" + 'new-' + bankStatement.crNewId + "']").parents('tr').find('.btn-edit-inline').trigger('click');
			    		$('#table_data tr td:nth-child(' + bankStatement.columnIndexDOM.ACTION + ') .row').show();
			    	});
		    		bankStatement.onAfterAddToDataList();
		    	}
		    });
		    
		    //button remove in table row
		    $(document).on("click","#table_data .btn-remove-inline",function(){
		    	var crEdittingId = $(this).parents('tr').find("input[type='checkbox']").val();
		        bankStatement.onBeforeModifyData(function(){
		        	$('#popConfirm').modal('toggle');
		        	$('#popConfirm button[name="btnConfirm"]').off("click");
		        	$('#popConfirm button[name="btnConfirm"]').click(function() {
		        		$("#table_data input[type='checkbox'][value='" + crEdittingId + "']").parents('tr').remove();
		        		for(i = 0; i < bankStatement.dataList.length; i++)
						{
							var item = bankStatement.dataList[i];
							if(item.id == crEdittingId)
							{
								item['removed'] = true;
								bankStatement.updateBalance(crEdittingId);
								if(crEdittingId.indexOf('new-') == 0)
								{
									bankStatement.dataList.splice(bankStatement.dataList.indexOf(item), 1);
								}
								bankStatement.endingBalance = Number($('#search-ending-balance').getNumberValue());
								bankStatement.onDataListChange();
								break;
							}
						}
						bankStatement.renderSaveButton();
			        });
		    	});
		    });
		    
		    //button submit in table row
		    $(document).on("click","#table_data .btn-success-inline",function(){
		    	$(this).hide();
		    	$(this).parents('tr').find('.awhite table').hide();
				$(this).parents('tr').find('.btn-success-hidden').trigger('click');
		    });
		    
		    //short key event
		    $(window).keydown(function(event) {
		    	//shiftKey + a
	    	  if(event.shiftKey && event.keyCode == 65) { 
	    		  $('.btn-add-new-row').trigger('click');	
	    	    event.preventDefault(); 
	    	  }
	    	  //shiftKey + s
	    	  if(event.shiftKey && event.keyCode == 83) { 
	    		  $('.btn-save-all').trigger('click');
	    	    event.preventDefault(); 
	    	  }
	    	});
		    
		    //delete payment order no
		    $(document).on("click","#table_data .btn-clean-payment-order",function(){
		    	bankStatement.sendMessageWhenEdit();
		    	$('#popConfirm').modal('toggle');
	        	$('#popConfirm button[name="btnConfirm"]').off("click");
	        	var crEdittingId = $(this).parents('tr').find("input[type='checkbox']").val();
	        	$('#popConfirm button[name="btnConfirm"]').click(function() {
	        		$.ajax({
	    	            type : "POST",
	    	            url : CONTEXT_PATH + '/bankStatement/api/resetPaymentOrderNo',
	    	            data: {
	    	            	id: crEdittingId
	    	            },
	    	            success : function(ajaxResult) {
	    	            	window.location.reload();
	    	            }
	    	        });
		        });
		    });
		    
		    //delete receipt order no
		    $(document).on("click","#table_data .btn-clean-receipt-order",function(){
		    	bankStatement.sendMessageWhenEdit();
		    	$('#popConfirm').modal('toggle');
	        	$('#popConfirm button[name="btnConfirm"]').off("click");
	        	var crEdittingId = $(this).parents('tr').find("input[type='checkbox']").val();
	        	$('#popConfirm button[name="btnConfirm"]').click(function() {
	        		$.ajax({
	    	            type : "POST",
	    	            url : CONTEXT_PATH + '/bankStatement/api/resetReceiptOrderNo',
	    	            data: {
	    	            	id: crEdittingId
	    	            },
	    	            success : function(ajaxResult) {
	    	            	window.location.reload();
	    	            }
	    	        });
		        });
		    });
		    
		    $(document).on("click","#table_data .btn-success-hidden",function(){
				 bankStatement.updateDataList($(this));
		    });
		    
		    //search-bank-trans-date
		    $("#search-bank-trans-date").on('change', function() {
		    	if($(this).val() == '0')
		    	{
		    		$('.panel-from-to-date').css("visibility", "visible")
		    	}
		    	else {
		    		$('.panel-from-to-date').css("visibility", "hidden")
		    	}
		    });
		    
		    //onclick auto match payment order No
		    $(document).on("click",".btn-auto-matching-payment",function(){
		    	//reset editting row
		    	$('#table_data .btn-success-inline').each(function(){
		    		if($(this).css('display') != 'none'){
		    			$(this).parents('tr').find('.btn-edit-hidden:first').trigger('click');
		    			$(this).hide();
		    		}
		    	});
		    	bankStatement.autoMatchPaymentOrder();
		    	bankStatement.sendMessageWhenEdit();
		    });
		    
		    $(document).on("click","#table_data .btn-cancel-match",function(){
		    	var row = $(this).closest('tr');
		    	row.find('.temp-payment-order-no').remove();
		    	row.find('.auto-match-function').remove();
		    	row.find('td:nth-child(' + bankStatement.columnIndexDOM.ACTION + ') .row').show();
		    	var id = $(this).closest('tr').find("input[name='selectedCheckBox']").attr('value');
		    	bankStatement.dataList.forEach(function(item){
					if(item.id == id)
					{
						delete item['paymentOrderNoTemp'];
						return;
					}
				});
		    });
		    
		    $(document).on("click","#table_data .btn-apply-match",function(){
		    	$(this).closest('tr').addClass('warning');
		    	var id = $(this).closest('tr').find("input[name='selectedCheckBox']").attr('value');
		    	var paymentOrderNo = $(this).closest('td').find('.temp-payment-order-no').html();
		    	$(this).closest('td').find('.tabledit-span').html(paymentOrderNo);
		    	$(this).closest('td').find('.btn-cancel-match').trigger('click');
		    	$('.btn-save-all').removeClass('disabled');
		    	bankStatement.dataList.forEach(function(item){
					if(item.id == id)
					{
						item['paymentOrderNo'] = paymentOrderNo;
						item['edited'] = true;
						return;
					}
				});
		    	
		    });	
		},
		/*
		 * dynamic load bank account based on bank name
		 */
		getBankAccountMasterByBankCode: function(bankCode, callBack){
			$.ajax({
	            type : "GET",
	            url : CONTEXT_PATH + '/bankStatement/api/getBankAccountMasterByBankCode',
	            data: {
	            	bankCode: bankCode
	            },
	            success : function(ajaxResult) {
	            	callBack(ajaxResult);
	            }
	        });
		},
		/*
		 * update data table from UI to JS model
		 */
		updateDataList: function(self){
			//console.log("updateDataList");
			$('#table_data .btn-edit-inline').removeClass('editing');
			$('#table_data .btn-success-inline').hide();
			var id = self.closest('tr').find("input[name='selectedCheckBox']").attr('value');
			bankStatement.updateDataItem(self.parents('tr'));
			bankStatement.onDataListChange();
		},
		/*
		 * update data table row (self = <tr>)
		 */
		updateDataItem: function(self){
			//update data model
			var id = self.find("input[name='selectedCheckBox']").attr('value');
			var needToUpdateBalance = false;
			bankStatement.dataList.forEach(function(item, index){
				if(item.id == id)
				{
					self.find('input[type="text"]').each(function(){
						var index = $(this).closest('td')[0].cellIndex;
						var value = $(this).val();
						if(index == bankStatement.columnIndexEdit.PAYMENT_ORDER_NO)
						{
							value = $(this).closest('td').find('.tabledit-span').text();
						}
						//update balance when edit debit or credit
						if(index == bankStatement.columnIndexEdit.DEBIT)
						{
							//don't allow set payment order if debit != paymentTotalAmount
							if(item['Payment Total Amount'] && item['Payment Total Amount'] != formatNumber(value))
							{
								self.closest('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('input[autocomplete]').val('');
								self.closest('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('.tabledit-span').text('');
								bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message4']);
								item['paymentOrderNo'] = "";
							}
							else
				        	{
								bankStatement.hideWarning();
				        	}
							var balanceD = getNewDecimal(item["balance"]);
							var debitD = getNewDecimal(item["debit"]);
							var valueD = getNewDecimal(value);
							balanceD = balanceD.plus(debitD).minus(valueD);
							item["balance"] = balanceD.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
							if(formatNumber(item["debit"]) != formatNumber(value)){
								needToUpdateBalance = true;
								$(this).closest('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.BALANCE + ')').text(formatStringAsNumber(item["balance"]));
								item[bankStatement.fieldIndex[index]] = formatNumber(value);
							}
						}
						else if(index == bankStatement.columnIndexEdit.CREDIT)
						{
							var balanceD = getNewDecimal(item["balance"]);
							var creditD = getNewDecimal(item["credit"]);
							var valueD = getNewDecimal(value);
							balanceD = balanceD.minus(creditD).plus(valueD);
							item["balance"] = balanceD.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
							if(formatNumber(item["credit"]) != formatNumber(value)){
								needToUpdateBalance = true;
								$(this).closest('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.BALANCE + ')').text(formatStringAsNumber(item["balance"]));
								item[bankStatement.fieldIndex[index]] = formatNumber(value);
							}
						}
						else {
							item[bankStatement.fieldIndex[index]] = value;
						}
					});
					item['edited'] = true;
					bankStatement.renderSaveButton();
					if(needToUpdateBalance == true){
						bankStatement.updateBalance(id);
					}
					return;
				}
			});
		},
		showWarning:function(message){
			$('.alert-warning').showAlertMessage([message]);
			$('#alert-warning-bank').goTo();
			
		},
		hideWarning: function(){
			$('.alert-warning').hide();
		},
		getDataListItemById: function(id){
			var data = null;
			bankStatement.dataList.forEach(function(item){
				if(item.id == id)
				{
					data = item;
					return;
				}
			});
			return data;
		},
		setMaxTransactionDate: function(strDate){
		     if(!strDate || strDate.trim() == 0)
		     {
		    	 bankStatement.maxTransactionDate = null;
		     }
		     else {
		    	 var dateParts = strDate.split("/");
				 bankStatement.maxTransactionDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
		     }
		},
		/*
		 * validate all item in data list statement
		 */
		validateDataList: function(dataList, onClickAddNew){
			var validTranDate = true;
			var validDebitCredit = true;
			var validValueDate = true;
			var validRef1 = true;
			var validRef2 = true;
			for(i = 0; i < dataList.length; i++){
				var dateItem = dataList[i];
				if(!dateItem['transactionDate'])
				{
					validTranDate = false;
				}
				if(!dateItem['valueDate'])
				{
					validValueDate = false;
				}
				if(dateItem['debit'] == 0 && dateItem['credit'] == 0)
				{
					validDebitCredit = false;
				}
				if(/^(.{0,100})$/.test(dateItem['reference1']) == false) {
					validRef1 = false
	            } 
				if(/^(.{0,100})$/.test(dateItem['reference2']) == false) {
					validRef2 = false
	            } 
			}
			if(!validTranDate || !validDebitCredit || !validValueDate || !validRef1 || !validRef2){
				var arrMess = [];
				if($('.btn-success-inline:visible').length > 0)
				{
					arrMess.push(GLOBAL_MESSAGES['cashflow.bankStatement.message5']);
				}
				else {
					if(!validTranDate)
						arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.bankStatement.transactionDate']));
					if(!validDebitCredit)
						arrMess.push(GLOBAL_MESSAGES['cashflow.bankStatement.debitOrCredit']);
					if(!validValueDate)
						arrMess.push(validate.require(GLOBAL_MESSAGES['cashflow.bankStatement.valueDate']));
					if(!validRef1)
						arrMess.push(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.bankStatement.reference1'], '100'));
					if(!validRef2)
						arrMess.push(GLOBAL_MESSAGES['cashflow.common.lessThanOrEqualError'].format(GLOBAL_MESSAGES['cashflow.bankStatement.reference2'], '100'));
				}
				$('.alert-warning').showAlertMessage(arrMess);
				$('#alert-warning-bank').goTo();
				return false;
			}
			return true;
		},
		save: function(){
			//console.log(bankStatement.dataList);
			if(bankStatement.validateDataList(bankStatement.dataList)){
				var formData = new FormData();
				var dataListChanged = [];
				for(j = 0; j < bankStatement.dataList.length; j++)
				{
					if(typeof bankStatement.dataList[j].id == 'string'){
						bankStatement.dataList[j].id = 0;
					}
					if(bankStatement.dataList[j]['edited'] == true || bankStatement.dataList[j]['removed'] == true)
					{
						dataListChanged.push(bankStatement.dataList[j]);
					}
				}
				formData.append('dataList', JSON.stringify(dataListChanged));
				formData.append('endingBalance', bankStatement.endingBalance);
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/bankStatement/save',
		            cache : false,
					contentType : false,
					processData : false,
					data : formData,
		            success : function(ajaxResult) {
		            	 var resultData = JSON.parse(ajaxResult);
		            	 if(resultData.STATUS == "SUCCESS" && resultData.RESULT > 0)
		            	 {
		            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedSuccessfully'], function(){
	            				 window.location.reload();
		            		 });
		            	 }
		            	 else
		            	 {
		            		 showPopupMessage(GLOBAL_MESSAGES['cashflow.common.savedError']);
		            	 }
		            }
		        });
			}
		},
		updateMaxTransactionDate: function(bankCode, bankAccNo){
			$.ajax({
	            type : "GET",
	            url : CONTEXT_PATH + '/bankStatement/api/getMaxTransactionDate',
	            data: {
	            	bankCode: bankCode,
	            	bankAccNo: bankAccNo,
	            },
	            success : function(ajaxResult) {
	            	//console.log(ajaxResult);
	            	if(ajaxResult.STATUS == 'SUCCESS' && ajaxResult.RESULT)
	    			{
	            		bankStatement.setMaxTransactionDate(ajaxResult.RESULT);
	    			}
	            	else
	            	{
	            		bankStatement.setMaxTransactionDate(null);
	            	}
	            }
	        });
		},
		/*
		 * update bank account info user selected into some input hidden fields
		 */
		getBankAccInfo: function(){
			var self = document.getElementById("search-bank-statementAccount");
			$("#search-currency").val(self.options[self.selectedIndex].getAttribute('currencyCode'));
	    	$("#search-beginning-balance").val(self.options[self.selectedIndex].getAttribute('beginningBalance'));
	    	$("#search-ending-balance").val(self.options[self.selectedIndex].getAttribute('endingBalance'));
	    	//update ending balance
	    	bankStatement.endingBalance = Number($('#search-ending-balance').getNumberValue());
	    	//bankStatement.bankAccountNo = this.options[self.selectedIndex].getAttribute('value');
		},
		updateBalance: function(crId){
			//console.log('---updateBalance');
			var crBalance = 0;
			bankStatement.sortDataList();
			var needToUpdate = false;
			var totalRecordUpdated = 0;
			for(i = 0; i < bankStatement.dataList.length; i++)
			{
				var item = bankStatement.dataList[i];
				if(item.id == crId )
				{
					needToUpdate = true;
				}
				
				if(item['removed'] == true)
				{
					if(item.id == crId )
						crBalance = item.beginningBalance;
					continue;
				}
				
				if(needToUpdate == true && Math.round(item.beginningBalance * 100) / 100 != Math.round(crBalance * 100) / 100){
					//console.log(item.id + ", " +item.transactionDate + " item.beginningBalance:" + item.beginningBalance + "," + Math.round(crBalance * 100) / 100)
					$("#table_data input[type='checkbox'][value='" + item.id + "']").parents('tr').addClass('warning');
					totalRecordUpdated ++;
					item['edited'] = true;
				}
				if(crBalance == 0){
					crBalance = item.beginningBalance;
				}
				else
				{
					item.beginningBalance = crBalance;
				}
				//
				var beginningBalanceD = getNewDecimal(item.beginningBalance);
				var creditD = getNewDecimal(item.credit);
				var debitD = getNewDecimal(item.debit);
				var balanceD = beginningBalanceD.minus(debitD).plus(creditD);
				item.balance = balanceD.toDecimalPlaces(2, Decimal.ROUND_DOWN).valueOf();
				crBalance = item.balance;
				
				$("#table_data input[type='checkbox'][value='" + item.id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.BEGINNING_BALANCE + ')').text(formatStringAsNumber(item.beginningBalance));
				$("#table_data input[type='checkbox'][value='" + item.id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.BALANCE + ')').text(formatStringAsNumber(item.balance));
					
			}
			if(totalRecordUpdated > 0)
			{
				bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message11'].replace("%", totalRecordUpdated));
			}
			bankStatement.endingBalance = crBalance;
			$('#search-ending-balance').val(bankStatement.endingBalance);
		},
		/*
		 * call when user modifies any data in the table
		 * if user filtered a part of all BANK ACC. TRANS (transactionTo < maxTransactionDate), need to load all data before editing
		 */
		onBeforeModifyData: function(functionModify){
			bankStatement.sendMessageWhenEdit();
			if(bankStatement.dataTable.page.info().recordsDisplay == 0){
				functionModify();
			}
			else {
				if(bankStatement.dataTable.page.info().end == 0){
					bankStatement.transactionFrom = formatDate(bankStatement.maxTransactionDate);
					bankStatement.transactionTo = formatDate(bankStatement.maxTransactionDate);
					$('#search-transactionFrom').val(formatDate(bankStatement.maxTransactionDate));
					$('#search-transactionTo').val(formatDate(bankStatement.maxTransactionDate));
					$("#search-bank-trans-date").val('0');
					$('.panel-from-to-date').css("visibility", "visible")
					bankStatement.functionModify = functionModify;
					bankStatement.dataTable.ajax.reload();
				}
				else {
					var dataTableLastTransDate = bankStatement.getLastTransDateFromDataTable();
					//console.log(dataTableLastTransDate);
					if(dataTableLastTransDate == null || dataTableLastTransDate.getTime() >= bankStatement.maxTransactionDate.getTime()){
						functionModify();
					}
					else {
						bankStatement.transactionTo = formatDate(bankStatement.maxTransactionDate);
						$('#search-transactionTo').val(formatDate(bankStatement.maxTransactionDate));
						$("#search-bank-trans-date").val('0');
						$('.panel-from-to-date').css("visibility", "visible")
						bankStatement.functionModify = functionModify;
						bankStatement.dataTable.ajax.reload();
					}
				}
			}
		},
		/*
		 * sort data list by transactionDate and id ASC before saving
		 */
		sortDataList: function(){
			bankStatement.dataList.sort(function(a,b){
				if(getDateFromString(a.transactionDate).getTime() - getDateFromString(b.transactionDate).getTime() == 0)
				{
					if(typeof a.id == 'string' && typeof b.id != 'string')
					{
						return 1;
					}
					else if(typeof b.id == 'string' && typeof a.id != 'string')
					{
						return -1;
					}
					else if(typeof b.id != 'string' && typeof a.id != 'string')
					{
						return a.id - b.id;
					}
					else {
						var ida = formatNumber(a.id.substring(4,a.id.length));
						var idb = formatNumber(b.id.substring(4,a.id.length));
						return ida - idb;
					}
				}
				else
				{
					return getDateFromString(a.transactionDate).getTime() - getDateFromString(b.transactionDate).getTime();
				}
			});
		},
		renderSaveButton: function(){
			var disable = true;
			for(i = 0; i < bankStatement.dataList.length; i++)
			{
				var item = bankStatement.dataList[i];
				if(typeof item.id == 'string' || item['edited'] == true || item['removed'] == true)
				{
					disable = false;
					break;
				}
			}
			if(disable == true)
			{
				$('.btn-save-all').addClass('disabled');
			}
			else {
				$('.btn-save-all').removeClass('disabled');
			}
		},
		/*
		 * call after any item in data list changed to update UI
		 */
		onDataListChange: function(){
			bankStatement.sortDataList();
			//show add button for previous row in the same transaction date
			var showingList = bankStatement.dataList.filter(function(item){
				return item['removed'] != true;
			});
			var transactionDate = null;
			for(i = 0; i < showingList.length; i++){
				var item = showingList[i];
				if(transactionDate != null){
					if(getDateFromString(transactionDate).getTime() - getDateFromString(item.transactionDate).getTime() == 0)
					{
						$("#table_data input[type='checkbox'][value='" + showingList[i-1].id + "']").parents('tr').find('.btn-add-inline').hide();
					}
					else {
						$("#table_data input[type='checkbox'][value='" + showingList[i-1].id + "']").parents('tr').find('.btn-add-inline').show();
					}
				}
				if(i == showingList.length - 1){
					$("#table_data input[type='checkbox'][value='" + showingList[i].id + "']").parents('tr').find('.btn-add-inline').show();
				}
				transactionDate = item.transactionDate;
			}
		},
		/*
		 * prevent bug TABLEDIT-TOOLBAR is added MULTI times
		 */
		onAfterAddToDataList: function(){
			setTimeout(function(){
				 $('#table_data .tabledit-toolbar').closest('td').hide();
			}, 10);
		},
		/*
		 * setup editor component for specific table row by its ID when user click button edit
		 */
		initEditor: function(id){
			bankStatement.sortDataList();
			var startDate = null;
			var endDate = null;
			var transactionDate = null;
			for(i = 0; i < bankStatement.dataList.length; i++){
				var item = bankStatement.dataList[i];
				if(item.id == id){
					startDate = i > 0 ? getDateFromString(bankStatement.dataList[i - 1].transactionDate) : null;
					endDate = i < bankStatement.dataList.length - 1 ? getDateFromString(bankStatement.dataList[i + 1].transactionDate) : null;
					if(endDate && startDate && endDate.getTime() != startDate.getTime())
					{
						if(endDate.getTime() == getDateFromString(item.transactionDate).getTime())
						{
							endDate.setDate(endDate.getDate());
						}
						else {
							endDate.setDate(endDate.getDate() - 1);
						}
					}
					//don't setup for statement has assigned payment
					if((item.paymentOrderNo == null || item.paymentOrderNo.length == 0) && (item.receiptOrderNo == null || item.receiptOrderNo.length == 0)){
						//console.log('startDate:' + startDate);
						//console.log('endDate:' + endDate);
						$("#table_data input[type='checkbox'][value='" + id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + ') input').datepicker({
					    	format: 'dd/mm/yyyy',
					        orientation: 'auto bottom',
					        startDate: startDate,
					        endDate: endDate
					    }).change(function(){
					    	 $(this).datepicker('hide');
					    });
						
						$("#table_data input[type='checkbox'][value='" + id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.VALUE_DATE + ') input').datepicker({
							format: 'dd/mm/yyyy',
					        orientation: 'auto bottom',
					        todayHighlight: true
					    }).change(function(){
					    	 $(this).datepicker('hide');
					    });
					}
					
					if(transactionDate != null){
						if(transactionDate == item.transactionDate)
						{
							$("#table_data input[type='checkbox'][value='" + bankStatement.dataList[i-1].id + "']").parents('tr').find('.btn-add-inline').hide();
						}
						else {
							$("#table_data input[type='checkbox'][value='" + bankStatement.dataList[i-1].id + "']").parents('tr').find('.btn-add-inline').show();
						}
					}
					
					if(item['removed'] != true)
					{
						transactionDate = item.transactionDate;
					}
					
					$("#table_data input[type='checkbox'][value='" + id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.DEBIT + ')').find('input').setupInputNumberMask();
					$("#table_data input[type='checkbox'][value='" + id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.CREDIT + ')').find('input').setupInputNumberMask();
					$("#table_data input[type='checkbox'][value='" + id + "']").parents('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('input').each(function(){
						var self = $(this).closest('td');
						//don't setup for statement has assigned payment
						if((item.paymentOrderNo == null || item.paymentOrderNo.length == 0) && (item.receiptOrderNo == null || item.receiptOrderNo.length == 0)){
							var autocompleteObj = $(this).tautocomplete({
						        highlight: "",
						        columns: ['Payment Order No',GLOBAL_MESSAGES['cashflow.bankStatement.paymentOrderNo'],GLOBAL_MESSAGES['cashflow.bankStatement.paymentTotalAmount'], 
						        	      GLOBAL_MESSAGES['cashflow.bankStatement.description'], GLOBAL_MESSAGES['cashflow.bankStatement.valueDate']
						        		 ],
						        hide: [false, true, true, true],
						        data: function(){
					        		var x = {}; 
					        		return x;
					        	},
					        	onchange: function () {
						        	var selectedData = autocompleteObj.all();
						        	$('input[autocomplete]').val(selectedData[GLOBAL_MESSAGES['cashflow.bankStatement.paymentOrderNo']]);
						        	//warning if debit != paymentTotalAmount
						        	var paymentTotalAmount = formatNumber(selectedData[GLOBAL_MESSAGES['cashflow.bankStatement.paymentTotalAmount']]);
						        	var id = self.closest('tr').find("input[name='selectedCheckBox']").attr('value');
						        	var dataListItem = bankStatement.getDataListItemById(id);
						        	var debit = formatNumber(self.closest('tr').find('td:nth-child(' + bankStatement.columnIndexDOM.DEBIT + ') input').val());
									dataListItem['Payment Total Amount'] = paymentTotalAmount;//for compare when edit debit
									//don't allow debit != payment total amount
						        	if(debit != paymentTotalAmount)
						        	{
										bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message4']);
										self.closest('tr').find('input[autocomplete]').val('');
									}
									else
						        	{
										bankStatement.hideWarning();
						        	}
						        },
						        ajax: {
					                url: "/bankStatement/api/findPaymentOrderByPaymentOrderNoContaining",
					                type: "GET", 
					                data: {
					                	paymentOrderNo : function(){
					                		return self.find('input[autocomplete]').val();
					                	}
					                },
					                success: function (result) {
					                	var filterData = [];
					                	result.data.forEach(function(item){
					                		filterData.push({
					                			id: item.id,
					                			paymentOrderNo: item.paymentOrderNo,
					                			paymentTotalAmount: formatStringAsNumber(item.paymentTotalAmount),
					                			description: item.description?item.description:'',
					                			valueDate: item.valueDate? item.valueDate.dayOfMonth + "/" + (item.valueDate.month + 1) + "/" + item.valueDate.year: ''
					                		})
					                	});
					                    return filterData;
					                }
						        },
						        delay: 100
							},function(self){
								self.next().next().addClass('tabledit-input').hide();
								self.remove();
							});
						}
					});
					break;
				}
			}
		},
		validateTransactionDateBeforeEdit: function(id, initFunction){
			if(typeof id == 'string' && id.indexOf('new-') == 0){
				initFunction();
			}
			else {
				$.ajax({
		            type : "POST",
		            url : CONTEXT_PATH + '/bankStatement/api/validateTransactionDateBeforeEdit',
		            data: {
		            	id: id
		            },
		            success : function(ajaxResult) {
		            	var resultData = JSON.parse(ajaxResult);
		            	//console.log(resultData);
		            	if(resultData.RESULT == true)
		            	{
		            		initFunction();
		            	}
		            	else
		            	{ 
		            		bankStatement.showWarning(GLOBAL_MESSAGES['cashflow.bankStatement.message13'].replace("%", resultData.MESSAGE));
		            	}
		            }
		        });
			}
		},
		getLastTransDateFromDataTable: function(){
			if(bankStatement.dataList.length == 0){
				return null;
			}
			else {
				var strDate = bankStatement.dataList[bankStatement.dataList.length - 1].transactionDate;
				var dateParts = strDate.split("/");
				return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
			}
		},
		resetDatatable: function() {
			//clear data list
		    bankStatement.dataList = [];
	    	bankStatement.transactionNumber = 0;
	    	bankStatement.bankName = "";
            bankStatement.bankAccountNo = "";
            bankStatement.transactionFrom = "";
            bankStatement.transactionTo = "";
	    	bankStatement.dataTable.ajax.reload();
	    	$('.btn-save-all, .btn-add-new-row').addClass('disabled');
		},
		autoMatchPaymentOrder: function(){
			var bankRefString = [];
			bankStatement.dataList.forEach(function(item){
				if((!item.paymentOrderNo || item.paymentOrderNo.length == 0) && item.reference2 && item.reference2.length > 0)
				{
					bankRefString.push("'" + item.reference2 + "'");
				}
			});
			if(bankRefString.length > 0){
				$.ajax({
		            type : "GET",
		            url : CONTEXT_PATH + '/bankStatement/api/getPaymentOrderNoBasedOnBankRef',
		            data: {
		            	bankName : bankStatement.bankName,
		            	bankAccNo : bankStatement.bankAccountNo,
		            	transactionFrom : bankStatement.transactionFrom,
		            	transactionTo : bankStatement.transactionTo,
		            	bankCode : $( "#search-bank-statementType option:selected" ).attr('bankcode'),
		            	transDateFilter : $("#search-bank-trans-date" ).val()
		            	
		            },
		            success : function(ajaxResult) {
		            	var arrInvaildMess = [];
		            	ajaxResult.RESULT.forEach(function(item){
		            	    $("#table_data input[type='checkbox'][ref2='" + item.bankRefNo + "'").parents('tr').each(function(){
		            	    	if($(this).find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('a').length == 0 && 
		            	    			$(this).find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('.temp-payment-order-no').length == 0 && 
		            	    			$(this).find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').find('.tabledit-span').html().length == 0
		            	    	)
			            	    {
		            	    		var id = $(this).find("input[name='selectedCheckBox']").attr('value');
						        	var dataListItem = bankStatement.getDataListItemById(id);
		            	    		var includeGstConvertedAmount = item.includeGstConvertedAmount;
		            	    		var debit = dataListItem['debit'];
		            	    		
		            	    		if(debit == includeGstConvertedAmount)
		            	    	    {
		            	    			bankStatement.appendHtmlToRowWhenAutoMatchingSuccess($(this), item.paymentOrderNo);
		            	    			dataListItem['paymentOrderNoTemp'] = item.paymentOrderNo;
		            	    	    }
		            	    		else {
		            	    			var mess = GLOBAL_MESSAGES['cashflow.bankStatement.message14'].replace("%1", item.paymentOrderNo).replace("%2", dataListItem.id)
		            	    																		  .replace("%3", formatStringAsNumber(includeGstConvertedAmount)).replace("%4", formatStringAsNumber(debit));
		            	    			arrInvaildMess.push(mess);
		            	    		}
			            	    }
		            	    });
		            	});
		            	if(arrInvaildMess.length > 0){
		            		$('.alert-warning').showAlertMessage(arrInvaildMess);
			    			$('#alert-warning-bank').goTo();
		            	}
		            }
		        });
			}
			
		},
		appendHtmlToRowWhenAutoMatchingSuccess: function(crRow, paymentOrderNo){
			crRow.find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').append('<a target="_blank" href="/payment/detail/' + paymentOrderNo +'" class="temp-payment-order-no">' + paymentOrderNo + '</a>');
			crRow.find('td:nth-child(' + bankStatement.columnIndexDOM.PAYMENT_ORDER_NO + ')').append('<div class="row auto-match-function"> ' + 
    				 																					   '<button title="Apply" class="btn-apply-match "><i class="fa fa-check button-margin"></i></button>' + 
    																								   	   '<button title="Cancel" class="btn-cancel-match"><i class="fa fa-close button-margin"></i></button>' + 
    																								   '</div>');
			crRow.find('td:nth-child(' + bankStatement.columnIndexDOM.ACTION + ') .row').hide();
		},
		sendMessageWhenEdit: function(){
			bankStatementWebSocket.sendMessage($('#currentSubsidiaryId').val(), bankStatement.bankName, bankStatement.bankAccountNo, $('.user-logged').html());
		}
	};
}());