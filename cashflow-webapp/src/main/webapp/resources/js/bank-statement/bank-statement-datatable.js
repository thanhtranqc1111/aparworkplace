(function ($) {
	$.fn.enableEditor = function(initedTableEditor){
		var id = $(this).attr('id');
		var colEdit = [];
		if(bankStatement.transactionNumber == 0){
			colEdit = [
				[bankStatement.columnIndexEdit.TRANSACTION_DATE],
	        	[bankStatement.columnIndexEdit.VALUE_DATE], 
	        	[bankStatement.columnIndexEdit.DESCRIPTION1], 
	        	[bankStatement.columnIndexEdit.DESCRIPTION2], 
	        	[bankStatement.columnIndexEdit.REFERENCE1], 
	        	[bankStatement.columnIndexEdit.REFERENCE2],
	        	[bankStatement.columnIndexEdit.DEBIT], 
	        	[bankStatement.columnIndexEdit.CREDIT], 
	        	[bankStatement.columnIndexEdit.PAYMENT_ORDER_NO]
			];
		}
		else {
			colEdit = [
	        	[bankStatement.columnIndexEdit.DESCRIPTION1], 
	        	[bankStatement.columnIndexEdit.DESCRIPTION2], 
	        	[bankStatement.columnIndexEdit.REFERENCE1], 
	        	[bankStatement.columnIndexEdit.REFERENCE2],
	        	[bankStatement.columnIndexEdit.PAYMENT_ORDER_NO]
			];
		}
		
		$(this).Tabledit({
			submitFunction: bankStatement.updateDataList,
			initedTableEditor: initedTableEditor,
			saveButton: true,
			restoreButton: true,
			deleteButton: false,
		    columns: {
		    	identifier: [20, 'id'],
		        editable: colEdit
		    },
		    buttons: {
		    	edit: {
		    		 class: 'btn btn-sm btn-primary btn-edit-hidden',
		             html: '<span class="glyphicon glyphicon-pencil"></span> &nbsp EDIT',
		             action: 'edit'
                },
                save: {
                    "class": "btn btn-sm btn-success-hidden",
                    html: "Save"
                },
                restore: {
                    "class": "btn btn-sm btn-warning",
                    html: "Restore",
                    action: "restore"
                }
            },
		});
		 
		//not allow to edit payment order No
		$('#table_data .tabledit-view-mode').each(function(){
			if($(this).find('a').length > 0)
			{
				$(this).removeClass('tabledit-view-mode');
				$(this).css('cursor','inherit');
				$(this).attr('title','');
			}
		});
		
		$('#table_data td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_NO + '), #table_data td:nth-child(' + bankStatement.columnIndexDOM.TRANSACTION_DATE + '), #table_data td:nth-child(' + bankStatement.columnIndexDOM.CURRENCY + '), ' + 
		  '#table_data td:nth-child(' + bankStatement.columnIndexDOM.DEBIT + '), #table_data td:nth-child(' + bankStatement.columnIndexDOM.CREDIT + '), #table_data td:nth-child(' + bankStatement.columnIndexDOM.BALANCE + ')').attr('title','');
		
		bankStatement.onDataListChange();
	};
	$.fn.addNewBankStatementRow = function(balance, rowIndex, transactionDate){
		//prepare data
		bankStatement.crNewId ++;
	
		//append row to data table
		var tableRef = document.getElementById($(this).attr('id')).getElementsByTagName('tbody')[0];
		
		//var newRow   = tableRef.insertRow(tableRef.rows.length);
		var newRow   = tableRef.insertRow(rowIndex + 1);
		var newCell0  = newRow.insertCell(0);
		var checkbox = document.createElement('input');
		checkbox.type = "checkbox";
		checkbox.name = "selectedCheckBox";
		checkbox.value = "new-" + bankStatement.crNewId;
		checkbox.id = "id";  
		checkbox.class = "tabledit-input form-control input-sm";
		newCell0.style = 'display:none';
		newCell0.appendChild(checkbox);
		
		var newCell1  = newRow.insertCell(1);
		var newText1  = document.createTextNode("");
		newCell1.appendChild(newText1);
		
		var newCell2  = newRow.insertCell(2);
		var newText2  = document.createTextNode(transactionDate);
		newCell2.appendChild(newText2);
		
		var newCell3  = newRow.insertCell(3);
		var newText3  = document.createTextNode(transactionDate);
		newCell3.appendChild(newText3);	
		
		var newCell4  = newRow.insertCell(4);
		var newText4  = document.createTextNode($('#search-currency').val());
		newCell4.appendChild(newText4);
		
		var newCell5  = newRow.insertCell(5);
		var newText5  = document.createTextNode(formatStringAsNumber(balance));
		newCell5.appendChild(newText5);
		
		var newCell6  = newRow.insertCell(6);
		var newText6  = document.createTextNode('');
		newCell6.appendChild(newText6);
		
		var newCell7  = newRow.insertCell(7);
		var newText7  = document.createTextNode('');
		newCell7.appendChild(newText7);
		
		var newCell8  = newRow.insertCell(8);
		var newText8  = document.createTextNode('');
		newCell8.appendChild(newText8);
		
		var newCell9  = newRow.insertCell(9);
		var newText9  = document.createTextNode('');
		newCell9.appendChild(newText9);
		
		var newCell10  = newRow.insertCell(10);
		var newText10  = document.createTextNode('0');
		newCell10.appendChild(newText10);
		
		var newCell11  = newRow.insertCell(11);
		var newText11 = document.createTextNode('0');
		newCell11.appendChild(newText11);
		
		var newCell12  = newRow.insertCell(12);
		var newText12  = document.createTextNode(formatStringAsNumber(balance));
		newCell12.appendChild(newText12);
			
		var newCell13  = newRow.insertCell(13);
		var newText13  = document.createTextNode('');
		newCell13.appendChild(newText13);
		
		var newCell14  = newRow.insertCell(14);
		var newText14  = document.createTextNode('');
		newCell14.appendChild(newText14);
		
		var newCell15  = newRow.insertCell(15);
		var newText15  = document.createElement('span');

		var htmlString = '<div class="row">';
		if(GLOBAL_PERMISSION["PMS-002"])
		{
			htmlString += '<div class="col-md-4 col-xs-3 btn-add-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.addNewRowBelow'] + '" style="cursor:pointer;" class="fa fa-plus-square-o "></i></div>';
		}
		
		if(GLOBAL_PERMISSION["PMS-003"]){
			htmlString += '<div class="col-md-4 col-xs-3 btn-edit-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.edit'] + '" style="cursor:pointer;" class="fa fa-pencil-square-o "></i></div>';
		}
		
		if(GLOBAL_PERMISSION["PMS-004"]){
			htmlString += '<div class="col-md-4 col-xs-3 btn-remove-inline"><i title="' + GLOBAL_MESSAGES['cashflow.bankStatement.remove'] + '" style="cursor:pointer;" class="fa fa-trash-o "></i></div>';
		}
		htmlString += '</div>';
		htmlString +=	'<div class="row"><button style="display:none;" class="btn btn-info btn-success-inline">' + GLOBAL_MESSAGES['cashflow.bankStatement.submit'] + '</button></div>';
		newText15.innerHTML = htmlString;
		newCell15.appendChild(newText15);
		
		bankStatement.dataList.push({
			id: "new-" + bankStatement.crNewId,
			bankAccountNo: $('#search-bank-statementAccount').val(),
			bankName: $('#search-bank-statementType').val(),
			debit: 0,
			credit: 0,
			balance: balance,
			beginningBalance: balance,
			description1: "",
			description2: "",
			reference1: "",
			reference2: "",
			paymentOrderNo: "",
			valueDate: transactionDate,
			currencyCode: $('#search-currency').val(),
			edited: true,
			transactionDate: transactionDate
		});
		
		if(bankStatement.crNewId == 1){
			$(this).enableEditor(false);
		}
		else
		{
			$(this).enableEditor(true);
		}
		//reload auto matching HTML when user click auto matching then click add new row.
		setTimeout(function(){
			bankStatement.dataList.forEach(function(item){
				if(item['paymentOrderNoTemp'] && item['paymentOrderNoTemp'].length > 0 && (!item['paymentOrderNo'] || item['paymentOrderNo'].length == 0)){
					var row = $("#table_data input[type='checkbox'][value='" + item.id + "']").parents('tr');
					bankStatement.appendHtmlToRowWhenAutoMatchingSuccess(row, item['paymentOrderNoTemp']);
				}
			})
		}, 100);
		
	};
}(jQuery));
