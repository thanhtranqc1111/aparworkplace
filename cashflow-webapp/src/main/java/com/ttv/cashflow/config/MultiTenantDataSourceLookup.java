package com.ttv.cashflow.config;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.lookup.MapDataSourceLookup;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.domain.Tenant;
import com.ttv.cashflow.util.Constant;

@Component(value = "dataSourceLookup")
public class MultiTenantDataSourceLookup extends MapDataSourceLookup {

    @Autowired
    public MultiTenantDataSourceLookup(DataSource defaultDataSource) {
        super();
        try {
            initializeDataSources(defaultDataSource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * It initialize all the datasources.
     * 
     */
    private void initializeDataSources(DataSource defaultDataSource) throws IOException {
        // Add the default tenant
        addDataSource(Constant.DEFAULT_TENANT_ID, defaultDataSource);
        //add all tanents
        JdbcTemplate jdbcTemplate = new JdbcTemplate(defaultDataSource);
        List<Tenant> tanents = jdbcTemplate.query("Select code, db_url, db_username, db_password, db_driver_class_name from tenant", new RowMapper<Tenant>() {
									@Override
									public Tenant mapRow(ResultSet rs, int rowNum) throws SQLException {
										Tenant t = new Tenant();
						                t.setCode(rs.getString(1));
						                t.setDbUrl(rs.getString(2));
						                t.setDbUsername(rs.getString(3));
						                t.setDbPassword(rs.getString(4));
						                t.setDbDriverClassName(rs.getString(5));
						                return t;
									}
						        });      
        addAllDataSource(tanents);
    }
    
	private void addAllDataSource(List<Tenant> tanents) {
		org.apache.tomcat.jdbc.pool.DataSource ds;
		for(Tenant tenant : tanents) {
			ds = new org.apache.tomcat.jdbc.pool.DataSource();
            ds.setUrl(tenant.getDbUrl().trim());
            ds.setUsername(tenant.getDbUsername().trim());
            ds.setPassword(tenant.getDbPassword().trim());
            ds.setDriverClassName(tenant.getDbDriverClassName().trim());
            addDataSource(tenant.getCode(), ds);
		}
		
	}

}