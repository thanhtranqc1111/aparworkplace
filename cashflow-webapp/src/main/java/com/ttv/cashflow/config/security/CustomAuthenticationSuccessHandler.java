package com.ttv.cashflow.config.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.ttv.cashflow.dao.SubsidiaryUserDetailDAO;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.helper.MapDataSourceLookupHelper;
import com.ttv.cashflow.session.LoginVisited;
import com.ttv.cashflow.util.Constant;

/**
 * @author thoai.nh
 * created date Mar 12, 2018
 */
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
	public LoginVisited loginedInfo;
	@Autowired
	private SubsidiaryUserDetailDAO subsidiaryUserDetailDAO;
	@Autowired
    private MapDataSourceLookupHelper mapDataSourceHelper;
	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Authentication authentication) throws IOException, ServletException {
		try {
			HttpSession session = httpServletRequest.getSession();
			UserAccount authUser = (UserAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			session.setAttribute(Constant.SESSSION_NAME_USER_NAME, authUser.getEmail());
			Map<String, Boolean> authorities = new HashMap<>();
			for(GrantedAuthority grantedAuthority: authentication.getAuthorities())
			{
				authorities.put(grantedAuthority.getAuthority(), true);
			}
			session.setAttribute(Constant.SESSSION_NAME_AUTHORITIES, authorities);
			// set our response to OK status
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			
			//Store user info to session.
			loginedInfo.setUserName(authUser.getEmail());
			loginedInfo.setFirstName(authUser.getFirstName());
			loginedInfo.setLastName(authUser.getLastName());
			loginedInfo.setEmail(authUser.getEmail());
			loginedInfo.setUserAccountId(authUser.getId());
			session.setAttribute(Constant.SESSSION_NAME_USER_FIRST_NAME, authUser.getFirstName());
			session.setAttribute(Constant.SESSSION_NAME_USER_LAST_NAME, authUser.getLastName());
			/**
			 * load subsidiary info
			 */
			Set<SubsidiaryUserDetail> setSubsidiaryUserDetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByUserId(authUser.getId());
			List<Subsidiary> listSubsidiary = new ArrayList<Subsidiary>();
			for(SubsidiaryUserDetail subsidiaryUserDetail: setSubsidiaryUserDetail) {
				listSubsidiary.add(subsidiaryUserDetail.getSubsidiary());
			}
			session.setAttribute(Constant.SESSSION_NAME_SUBSIDIARIES, listSubsidiary);
			//current subsidiary
			Iterator<SubsidiaryUserDetail> iterator = setSubsidiaryUserDetail.iterator();
			Subsidiary subsidiary = iterator.next().getSubsidiary();
			for(SubsidiaryUserDetail subsidiaryUserDetail: setSubsidiaryUserDetail) {
				if(subsidiaryUserDetail.getIsDefault() != null && subsidiaryUserDetail.getIsDefault() == true)
				{
					subsidiary = subsidiaryUserDetail.getSubsidiary();
					break;
				}
			}
			loginedInfo.setSubsidiaryName(subsidiary.getName());
			loginedInfo.setSubsidiaryId(subsidiary.getId());
			session.setAttribute(Constant.SESSSION_NAME_CURRENT_SUBSIDIARY, subsidiary);
			
			//add datasource belong user login.
			mapDataSourceHelper.addDataSourceIfNotExist(subsidiary.getCode());
			
			//set tenant code to session for decide get right datasource.
			session.setAttribute(Constant.SESSSION_NAME_TENANT_ID, subsidiary.getCode());
			
			// we will redirect the user after successfully login
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/");
		} catch (Exception e) {
			e.printStackTrace();
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
		}
	}
	
	
}
