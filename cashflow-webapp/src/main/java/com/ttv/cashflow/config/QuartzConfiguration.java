package com.ttv.cashflow.config;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import com.ttv.cashflow.util.Constant;

@Configuration 
@ComponentScan("com.ttv.cashflow.scheduler")
public class QuartzConfiguration {
    
    //PROPERTIES FOR CONNECT TO DATABASE.
    @Value( "${postgresql.default.url}" )
    private String url;
    
    @Value( "${postgresql.default.username}" )
    private String username;
    
    @Value( "${postgresql.default.password}" )
    private String password;
    
    @Value( "${postgresql.default.driver-class-name}" )
    private String driverClassName;
    
    @Bean
    public MethodInvokingJobDetailFactoryBean methodInvokingJobDetailFactoryBean() {
        MethodInvokingJobDetailFactoryBean job = new MethodInvokingJobDetailFactoryBean();
        job.setTargetBeanName("RefeshView");
        job.setTargetMethod("execute");
        
        return job;
    }

    @Bean
    public SimpleTriggerFactoryBean simpleTriggerFactoryBean(){
        SimpleTriggerFactoryBean factory = new SimpleTriggerFactoryBean();
        factory.setJobDetail(methodInvokingJobDetailFactoryBean().getObject());
        factory.setStartDelay(3000);
        factory.setRepeatInterval(getRepeatInterval());
        
        return factory;
    }
    
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
        scheduler.setTriggers(simpleTriggerFactoryBean().getObject());
        
        return scheduler;
    }
    
    /**
     * Get repeat interval have configuration on database.
     */
    private long getRepeatInterval() {
    
        long ret = 15 * 60 * 1000; // default 15 minutes.

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnectionOfDBGeneral();

            String sql = "SELECT value FROM scheduler WHERE code = ?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, Constant.SCHEDULER_REFESH_VIEW);
            
            //execute
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                ret = rs.getLong("value");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // close rs
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            
            // close stmt
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            // close conn
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
        return ret;
    
    }
    
    /**
     * Get connection to general database.
     */
    private Connection getConnectionOfDBGeneral() throws IOException, ClassNotFoundException, SQLException {

        Class.forName(driverClassName);

        Connection conn = DriverManager.getConnection(url, username, password);

        return conn;

    }
} 
