package com.ttv.cashflow.config.security;

import java.util.List;
import java.util.stream.Collectors;

import org.jboss.aerogear.security.otp.Totp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;

import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.UserAccount;

/**
 * @author thoai.nh
 * created date Mar 12, 2018
 */
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {
	 
    @Autowired
    private UserAccountDAO userAccountDAO;
 
    @Override
    public Authentication authenticate(Authentication auth) {
        String verificationCode = ((CustomWebAuthenticationDetails) auth.getDetails()).getVerificationCode();
        List<UserAccount> list = userAccountDAO.findUserAccountByEmail(auth.getName()).stream().collect(Collectors.toList());
        if ((list.isEmpty())) {
            throw new BadCredentialsException(CustomAuthenticationFailureHandler.ERROR_CODE_INVALID_USERNAME_CREDENTIALS);
        }
        UserAccount user = list.get(0);
        if(!user.getIsActive()) {
        	throw new BadCredentialsException(CustomAuthenticationFailureHandler.ERROR_CODE_USER_IS_DEACTIVE);
        }
        else {
        	if(user.getSubsidiaryUserDetails().stream().noneMatch(s -> s.getSubsidiary().getIsActive()))
        	{
        		throw new BadCredentialsException(CustomAuthenticationFailureHandler.ERROR_CODE_DOES_NOT_HAVE_ACTIVE_TANENT);
        	}
        }
        if (user.getIsUsing2fa()) {
            Totp totp = new Totp(user.getSecretToken());
            if (!isValidLong(verificationCode) || !totp.verify(verificationCode)) {
                throw new BadCredentialsException(CustomAuthenticationFailureHandler.ERROR_CODE_INVALID_VERFICATION_CODE);
            }
        }
        Authentication result = super.authenticate(auth);
        return new UsernamePasswordAuthenticationToken(user, result.getCredentials(), result.getAuthorities());
    }
 
    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
