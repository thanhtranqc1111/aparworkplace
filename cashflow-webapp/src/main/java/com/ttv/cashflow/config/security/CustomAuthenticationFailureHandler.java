package com.ttv.cashflow.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.util.Constant;
/**
 * @author thoai.nh
 * created date Mar 27, 2018
 */
@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler{
	public static final String ERROR_CODE_INVALID_USERNAME_CREDENTIALS = "INVALID_USERNAME_CREDENTIALS";
	public static final String ERROR_CODE_USER_IS_DEACTIVE = "USER_IS_DEACTIVE";
	public static final String ERROR_CODE_DOES_NOT_HAVE_ACTIVE_TANENT = "DOES_NOT_HAVE_ACTIVE_TANENT";
	public static final String ERROR_CODE_INVALID_VERFICATION_CODE = "INVALID_VERFICATION_CODE";
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		setDefaultFailureUrl("/login?error=true");
        super.onAuthenticationFailure(request, response, exception);
        request.getSession().setAttribute(Constant.SESSSION_LOGIN_ERROR_CODE, exception.getMessage());
	}

}
