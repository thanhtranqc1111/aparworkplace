package com.ttv.cashflow.config;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.util.Constant;

@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {
    @Override
    public String resolveCurrentTenantIdentifier() {
        return resolveTenantByHttpSession();
    }

    /**
     * Get tenant id.
     */
    public String resolveTenantByHttpSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attr != null) {
            HttpServletRequest request = attr.getRequest();
            String tanentCodeFromRequest = getTanentCodeFromRequest(request);
            if(tanentCodeFromRequest != null) {
            	return tanentCodeFromRequest.replace("\"", "");
            }
            String requestURI = request.getRequestURI();
            // Set default tenant id when client request for general database.
            if (isRequestForGeneral(requestURI)) {
                return Constant.DEFAULT_TENANT_ID;
            }
            
            // Otherwise, get tenant id from session if exist.
            HttpSession session = request.getSession(false); 
            if (session != null) {
                String tenantId = (String) session.getAttribute(Constant.SESSSION_NAME_TENANT_ID);
                
                if (StringUtils.isNotEmpty(tenantId)) {
                    return tenantId;
                }
            }
        }
        
        // Return default tenant id when first start.
        return Constant.DEFAULT_TENANT_ID;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
    private String getTanentCodeFromRequest(HttpServletRequest request) {
    	if(request.getRequestURI().contains("restApi/")) {
    		return request.getParameter("tanentCode");
    	}
    	else {
    		return null;
    	}
    }
    private boolean isRequestForGeneral(String requestURI) {
        return requestURI.contains("/changeSubsidiary")
                || requestURI.contains("/subsidiary/")
                || requestURI.contains("/userAccount/")
                || requestURI.contains("/role/")
                || requestURI.contains("/permission/")
                || requestURI.contains("/grantPermission/")
                || requestURI.contains("/userHistory");
       
    }
}
