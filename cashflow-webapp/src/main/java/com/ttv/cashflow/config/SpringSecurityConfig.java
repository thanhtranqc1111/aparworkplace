package com.ttv.cashflow.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;

import com.ttv.cashflow.config.security.CustomAuthenticationProvider;
import com.ttv.cashflow.config.security.CustomRememberMeServices;
import com.ttv.cashflow.config.security.CustomWebAuthenticationDetailsSource;
import com.ttv.cashflow.util.PermissionCode;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private CustomWebAuthenticationDetailsSource authenticationDetailsSource;
	@Autowired
	private AccessDeniedHandler accessDeniedHandler;
	@Autowired
	private SimpleUrlAuthenticationFailureHandler simpleUrlAuthenticationFailureHandler;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private LogoutSuccessHandler logoutSuccessHandler;
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				/**
				 * public request for all user
				 */
				.antMatchers("/resources/**", "/publicAuthenticate/*").permitAll()
				.antMatchers("/favicon.ico").permitAll()
				.antMatchers("/login").permitAll()
				.antMatchers("/restApi/**").permitAll()
				/**
				 * Manage user accounts permission
				 */
				.antMatchers("/userAccount/list")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				.antMatchers("/userAccount/detail/*")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				.antMatchers("/userAccount/addNew")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				.antMatchers("/userAccount/api/getAllPagingUserAccount")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				.antMatchers("/userAccount/admin.save")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				.antMatchers("/userAccount/delete")
				.hasAnyAuthority(PermissionCode.PMS_007.toString(), PermissionCode.PMS_008.toString())
				/**
				 * Grant permissions
				 */
				.antMatchers("/role/**").hasAnyAuthority(PermissionCode.PMS_008.toString())
				.antMatchers("/permission/**").hasAnyAuthority(PermissionCode.PMS_008.toString())
				.antMatchers("/grantPermission/**").hasAnyAuthority(PermissionCode.PMS_008.toString())
				/**
				 * View payroll details and create payroll
				 */
				.antMatchers("/payroll/import", "/payroll/detail", "/payroll/downTemplate", "/payroll/detail/*", "/projectPayment/detail").hasAuthority(PermissionCode.PMS_009.toString())
				/**
				 * Import data
				 */
				.antMatchers("/*/import").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.upload").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.do").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/downTemplate").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.download").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.downLog").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.delete").hasAnyAuthority(PermissionCode.PMS_005.toString())
				.antMatchers("/*/import.deleteLog").hasAnyAuthority(PermissionCode.PMS_005.toString())
				/**
				 * Export data
				 */
				.antMatchers("/*/export").hasAnyAuthority(PermissionCode.PMS_006.toString())
				.antMatchers("/*/export.do").hasAnyAuthority(PermissionCode.PMS_006.toString())
				.antMatchers("/*/export.download").hasAnyAuthority(PermissionCode.PMS_006.toString())
				.antMatchers("/*/export.delete").hasAnyAuthority(PermissionCode.PMS_006.toString())
				/**
				 * Delete data
				 */
				.antMatchers("/*/delete").hasAnyAuthority(PermissionCode.PMS_004.toString())
				/**
				 * Edit data Create data
				 */
				.antMatchers("/*/save").hasAnyAuthority(PermissionCode.PMS_002.toString(), PermissionCode.PMS_003.toString())
				.antMatchers("/*/detail").hasAnyAuthority(PermissionCode.PMS_002.toString())
				.antMatchers("/receiptOrder/applyMatching").hasAnyAuthority(PermissionCode.PMS_003.toString())
				.antMatchers("/bankStatement/api/resetPaymentOrderNo").hasAnyAuthority(PermissionCode.PMS_003.toString())
				.antMatchers("/bankStatement/api/resetReceiptOrderNo").hasAnyAuthority(PermissionCode.PMS_003.toString())
				/**
				 * Configuration form login
				 */
				.anyRequest().authenticated().and().headers().frameOptions().sameOrigin().and().csrf().disable()
				.formLogin().authenticationDetailsSource(authenticationDetailsSource).loginPage("/login")
				.successHandler(authenticationSuccessHandler).failureHandler(simpleUrlAuthenticationFailureHandler)
				.usernameParameter("email").passwordParameter("password").and().logout().logoutUrl("/logout")
				.logoutSuccessHandler(logoutSuccessHandler).and().exceptionHandling()
				.accessDeniedHandler(accessDeniedHandler).and().rememberMe().rememberMeServices(rememberMeServices()).key("theKey")
				.authenticationSuccessHandler(authenticationSuccessHandler).tokenValiditySeconds(1209600);
	}
	
	@Bean
    public RememberMeServices rememberMeServices() {
        return new CustomRememberMeServices("theKey", userDetailsService, new InMemoryTokenRepositoryImpl());
    }
	
	@Bean
	public DaoAuthenticationProvider authProvider() {
		final CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder);
		return authProvider;
	}

	/*@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(
				"SELECT TRIM(user_name) as username, TRIM(password) as password,is_active as enabled "
						+ "FROM user_account u " + "WHERE user_name = ? "
						+ "and (SELECT count(1) FROM subsidiary_user_detail su, subsidiary s WHERE su.user_account_id = u.id and su.subsidiary_id = s.id and s.is_active = true) > 0")
				.authoritiesByUsernameQuery("SELECT user_name, p.code "
						+ "FROM user_account u, role r, permission p, role_permission rp "
						+ "WHERE trim(u.user_name) = ? and u.role_id = r.id and u.role_id = rp.role_id and rp.permission_id = p.id "
						+ "and (SELECT count(1) FROM subsidiary_user_detail su, subsidiary s WHERE su.user_account_id = u.id and su.subsidiary_id = s.id and s.is_active = true) > 0")
				.passwordEncoder(bCryptPasswordEncoder);
	}*/
}
