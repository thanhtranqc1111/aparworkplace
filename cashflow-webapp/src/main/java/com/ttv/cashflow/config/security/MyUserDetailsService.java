package com.ttv.cashflow.config.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.UserAccount;

/**
 * @author thoai.nh
 * created date Mar 12, 2018
 * load permisson by user name
 */
@Service("userDetailsService")
@Transactional
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserAccountDAO userAccountDAO;

    public MyUserDetailsService() {
        super();
    }
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        try {
            Set<UserAccount> set = userAccountDAO.findUserAccountByEmail(email);
            if (set.isEmpty()) {
                throw new UsernameNotFoundException("No user found with email: " + email);
            }
            UserAccount userAccount = set.stream().collect(Collectors.toList()).get(0);
            List<String> privileges = userAccount.getRole().getRolePermissions().stream().map(rp -> rp.getPermission().getCode()).collect(Collectors.toList());
            return new org.springframework.security.core.userdetails.User(userAccount.getEmail(), userAccount.getPassword(), userAccount.getIsActive(), true, true, true, getGrantedAuthorities(privileges));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private final List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
