package com.ttv.cashflow.config.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

/**
 * @author thoai.nh
 * created date Mar 12, 2018
 */
public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {
	 
    private String verificationCode;
 
    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        verificationCode = request.getParameter("code");
    }
 
    public String getVerificationCode() {
        return verificationCode;
    }
}