/*package com.ttv.cashflow.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.ttv.cashflow.session.LoginVisited;
@WebFilter
public class FilterServlet implements Filter {

    @Autowired
    private LoginVisited visit;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String loginURI  = request.getContextPath() + "/";
		
		boolean loginRequest = request.getRequestURI().equals(loginURI);
		boolean loggedIn = visit != null && visit.isLogin();
		boolean isStaticResource = request.getRequestURI().startsWith("/resources/")
				|| request.getRequestURI().startsWith("/login");

		if (loggedIn || loginRequest || isStaticResource) {
			chain.doFilter(request, response);
		} else {
			response.sendRedirect(loginURI);
		}
		 
    }

    @Override
    public void init(FilterConfig paramFilterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
*/