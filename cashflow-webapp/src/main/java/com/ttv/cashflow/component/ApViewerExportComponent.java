package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ApViewerExportComponent.Column.*;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.ApViewerDAO;
import com.ttv.cashflow.domain.ApViewer;
import com.ttv.cashflow.util.Constant;

@Component
@PrototypeScope
public class ApViewerExportComponent {
    
    private static final int START_ROW_DATA_IN_TEMPLATE = 2;
    
    @Autowired
    private ApViewerDAO apViewerDAO;
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;
    
    public void doExport(Workbook workbook, List<String> listApInvoiceNo) throws Exception{
        DataFormat format = workbook.createDataFormat();
        CreationHelper createHelper = workbook.getCreationHelper();
        XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
        
        apViewerDAO.refresh();
        int rowNum = START_ROW_DATA_IN_TEMPLATE;
        
        for (String apInvoiceNo : listApInvoiceNo) {
            int countApiDetail = 0;
            BigInteger numDetail = BigInteger.ZERO;
            boolean hasWritePayment = false;
            
            Set<ApViewer> apViewerSet = apViewerDAO.findApViewerByApInvoiceNo(apInvoiceNo);
            Iterator<ApViewer> apViewerIter = apViewerSet.iterator();
            
            while (apViewerIter.hasNext()) {
                
                ApViewer apViewer = apViewerIter.next();
                Row row = sheet.createRow(rowNum);
                
                //////////PUT DATA//////////
                
                row.createCell(AP_NO).setCellValue(apViewer.getApInvoiceNo());
                
                //Set month
                CellStyle monthStyle = workbook.createCellStyle();
                formatDate(monthStyle, Constant.MMMYYYY, createHelper);
                
                Cell monthCell = row.createCell(MONTH);
                monthCell.setCellValue(apViewer.getMonth());
                monthCell.setCellStyle(monthStyle);
                
                //
                row.createCell(PAYEE).setCellValue(apViewer.getPayeeName());
                row.createCell(INVOICE_NO).setCellValue(apViewer.getInvoiceNo());
                row.createCell(APPROVAL_CODE).setCellValue(apViewer.getApprovalCode());
                row.createCell(ORIGINAL_CURRENCY).setCellValue(apViewer.getOriginalCurrencyCode());
                
                //ExcludeGstOriginalAmount
                CellStyle excludeGstOriginalAmountStyle = workbook.createCellStyle();
                formatNumeric(excludeGstOriginalAmountStyle, format);
                
                Cell excludeGstOriginalAmountCell = row.createCell(EXCLUDE_GST_ORIGINAL_AMOUNT);
                excludeGstOriginalAmountCell.setCellValue(doubleValue(apViewer.getExcludeGstOriginalAmount()));
                excludeGstOriginalAmountCell.setCellStyle(excludeGstOriginalAmountStyle);
                
                //
                row.createCell(GST_TYPE).setCellValue(apViewer.getGstType());
                row.createCell(GST_RATE).setCellValue(doubleValue(apViewer.getGstRate()));
                
                //IncludeGstOriginalAmount
                CellStyle includeGstOriginalAmountStyle = workbook.createCellStyle();
                formatNumeric(includeGstOriginalAmountStyle, format);
                
                Cell includeGstOriginalAmountCell = row.createCell(INCLUDE_GST_ORIGINAL_AMOUNT);
                includeGstOriginalAmountCell.setCellValue(doubleValue(apViewer.getIncludeGstOriginalAmount()));
                includeGstOriginalAmountCell.setCellStyle(includeGstOriginalAmountStyle);
                
                //FxRate
                CellStyle fxRateStyle = workbook.createCellStyle();
                formatNumeric(fxRateStyle, format);
                
                Cell fxRateCell = row.createCell(FX_RATE);
                fxRateCell.setCellValue(doubleValue(apViewer.getFxRate()));
                fxRateCell.setCellStyle(fxRateStyle);
                
                //IncludeGstConvertedAmount
                CellStyle includeGstConvertedAmountStyle = workbook.createCellStyle();
                formatNumeric(includeGstConvertedAmountStyle, format);
                
                Cell includeGstConvertedAmountCell = row.createCell(INCLUDE_GST_CONVERTED_AMOUNT);
                includeGstConvertedAmountCell.setCellValue(doubleValue(apViewer.getIncludeGstConvertedAmount()));
                includeGstConvertedAmountCell.setCellStyle(includeGstConvertedAmountStyle);
                
                //
                row.createCell(ACCOUNT_NAME).setCellValue(apViewer.getAccountName());
                row.createCell(CLASS_NAME).setCellValue(apViewer.getClassName());
                row.createCell(DESCRIPTION).setCellValue(apViewer.getDescription());
                
                // reset count detail of AP invoice (countApiDetail) and flag(hasWritePayment)
                // for write PAYMENT section when multiple PAYMNET paid for 1 AP invoice. 
                if(countApiDetail == numDetail.intValue()){
                    countApiDetail = 0;
                    hasWritePayment = false;
                }
                
                //Write PAYMENT section.
                if(!hasWritePayment && hasText(apViewer.getPaymentOrderNo())){
                    
                    numDetail = apInvoiceDAO.countDetail(apInvoiceNo);
                    int fistRow = rowNum;
                    int lastRow = rowNum + numDetail.intValue() - 1;
                    
                    //Merge row
                    if (fistRow != lastRow) {
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORDER_NO, PAYMENT_ORDER_NO));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VALUE_DATE, VALUE_DATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_INCLUDE_GST_ORIGINAL_AMOUNT, PAYMENT_INCLUDE_GST_ORIGINAL_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_RATE, PAYMENT_RATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_INCLUDE_GST_CONVERTED_AMOUNT, PAYMENT_INCLUDE_GST_CONVERTED_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VARIANCE_AMOUNT, VARIANCE_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, UNPAID_INCLUDE_GST_ORIGINAL_AMOUNT, UNPAID_INCLUDE_GST_ORIGINAL_AMOUNT));
                    }
                    
                    //PaymentOrderNo
                    CellStyle paymentOrderNoStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(paymentOrderNoStyle);
                    
                    Cell paymentOrderNoCell = row.createCell(PAYMENT_ORDER_NO);
                    paymentOrderNoCell.setCellValue(apViewer.getPaymentOrderNo());
                    paymentOrderNoCell.setCellStyle(paymentOrderNoStyle);

                    //ValueDate
                    CellStyle valueDateStyle = workbook.createCellStyle();
                    formatDate(valueDateStyle, Constant.DDMMYYYY, createHelper);
                    setVerticalAlignCenter(valueDateStyle);
                    
                    Cell valueDateCell = row.createCell(VALUE_DATE);
                    valueDateCell.setCellValue(apViewer.getValueDate());
                    valueDateCell.setCellStyle(valueDateStyle);

                    //PaymentIncludeGstOriginalAmount
                    CellStyle paymentIncludeGstOriginalAmountStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(paymentIncludeGstOriginalAmountStyle);
                    formatNumeric(paymentIncludeGstOriginalAmountStyle, format);
                    
                    Cell paymentIncludeGstOriginalAmountCell = row.createCell(PAYMENT_INCLUDE_GST_ORIGINAL_AMOUNT);
                    paymentIncludeGstOriginalAmountCell.setCellValue(doubleValue(apViewer.getPaymentIncludeGstOriginalAmount()));
                    paymentIncludeGstOriginalAmountCell.setCellStyle(paymentIncludeGstOriginalAmountStyle);
                    
                    //PaymentRate
                    CellStyle paymentRateStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(paymentRateStyle);
                    formatNumeric(paymentRateStyle, format);
                    
                    Cell paymentRateCell = row.createCell(PAYMENT_RATE);
                    paymentRateCell.setCellValue(doubleValue(apViewer.getPaymentRate()));
                    paymentRateCell.setCellStyle(paymentRateStyle);
                    
                    //PaymentIncludeGstConvertedAmount
                    CellStyle paymentIncludeGstConvertedAmountStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(paymentIncludeGstConvertedAmountStyle);
                    formatNumeric(paymentIncludeGstConvertedAmountStyle, format);
                    
                    Cell paymentIncludeGstConvertedAmountCell = row.createCell(PAYMENT_INCLUDE_GST_CONVERTED_AMOUNT);
                    paymentIncludeGstConvertedAmountCell.setCellValue(doubleValue(apViewer.getPaymentIncludeGstConvertedAmount()));
                    paymentIncludeGstConvertedAmountCell.setCellStyle(paymentIncludeGstConvertedAmountStyle);
                    
                    //VarianceAmount
                    CellStyle varianceAmountStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(varianceAmountStyle);
                    formatNumeric(varianceAmountStyle, format);
                    
                    Cell varianceAmountCell = row.createCell(VARIANCE_AMOUNT);
                    varianceAmountCell.setCellValue(doubleValue(apViewer.getVarianceAmount()));
                    varianceAmountCell.setCellStyle(varianceAmountStyle);
                    
                    //UnpaidIncludeGstOriginalAmount
                    CellStyle unpaidIncludeGstOriginalAmountStyle = workbook.createCellStyle();
                    setVerticalAlignCenter(unpaidIncludeGstOriginalAmountStyle);
                    formatNumeric(unpaidIncludeGstOriginalAmountStyle, format);
                    
                    Cell unpaidIncludeGstOriginalAmountCell = row.createCell(UNPAID_INCLUDE_GST_ORIGINAL_AMOUNT);
                    unpaidIncludeGstOriginalAmountCell.setCellValue(doubleValue(apViewer.getUnpaidIncludeGstOriginalAmount()));
                    unpaidIncludeGstOriginalAmountCell.setCellStyle(unpaidIncludeGstOriginalAmountStyle);
                    
                    //
                    hasWritePayment = true;
                }
                
                countApiDetail++;
                rowNum++;
                
            } // end while
        }// end for
    }
    
    
    private Double doubleValue(BigDecimal val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    /**
     * Remove old contents
     * @param sheet
     * @param hRow -- Header row
     */
    public void  removeContents(XSSFSheet sheet, int hRow) {
        int nRow = sheet.getPhysicalNumberOfRows();
        for (int i = hRow; i < nRow; i++) {
            Row row = sheet.getRow(i); 
            if (null != row) {
                try {
                    sheet.removeRow(row);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void setVerticalAlignCenter(CellStyle cellStyle){
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    }
    
    private void formatDate(CellStyle cellStyle,  String format, CreationHelper createHelper){
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
    }
    
    /**
     * Set format number.
     * <p>Example:
     * <ul>
     * <li>3456.70  -> 3,456.70
     * <li>3456.789 -> 3,456.78
     * </ul>
     * <p>
     * 
     */
    private void formatNumeric(CellStyle cellStyle, DataFormat format){
        cellStyle.setDataFormat(format.getFormat("#,##0.00"));
    }
	
	class Column {
	    static final int AP_NO = 0; //AP No
	    static final int MONTH = 1; //Month
	    static final int PAYEE = 2; //Payee
	    static final int INVOICE_NO = 3; //Invoice No
	    static final int APPROVAL_CODE = 4; //Approval Code
	    static final int ORIGINAL_CURRENCY = 5; //Original Currency
	    static final int EXCLUDE_GST_ORIGINAL_AMOUNT = 6; //Amount Excluded GST(Original)
	    static final int GST_TYPE = 7; //GST Type
	    static final int GST_RATE = 8; //GST (%) Rate
	    static final int INCLUDE_GST_ORIGINAL_AMOUNT = 9; //Amount Included GST(Original)
	    static final int FX_RATE = 10; //Fx Rate
	    static final int INCLUDE_GST_CONVERTED_AMOUNT = 11; //Amount Included GST(Converted)
	    static final int ACCOUNT_NAME = 12; //Corresponding Accounts
	    static final int CLASS_NAME = 13; //Project
	    static final int DESCRIPTION = 14; //Remarks
	    static final int PAYMENT_ORDER_NO = 15; //Payment Order No
	    static final int VALUE_DATE = 16; //Paid Date(dd/mm/yyyy)
	    static final int PAYMENT_INCLUDE_GST_ORIGINAL_AMOUNT = 17; //Amount (Original)
	    static final int PAYMENT_RATE = 18; //Payment Rate
	    static final int PAYMENT_INCLUDE_GST_CONVERTED_AMOUNT = 19; //Amount (Converted)
	    static final int VARIANCE_AMOUNT = 20; //Variance Amount
	    static final int UNPAID_INCLUDE_GST_ORIGINAL_AMOUNT = 21; //Remain Amount(Original)
	}

}

