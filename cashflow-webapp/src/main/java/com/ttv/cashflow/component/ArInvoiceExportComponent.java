package com.ttv.cashflow.component;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.service.ArInvoiceService;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class ArInvoiceExportComponent extends ExportComponent {
    
    private static final Logger logger = Logger.getLogger("AR INVOICE");
    
    DataFormatter df = new DataFormatter();
    
    @Autowired
    private ArInvoiceService arInvoiceService;
    
    @Autowired
    private ArViewerExportComponent arViewerExportComponent;
	
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<String> listArInvoiceNo  = getListArInvoiceNo(request);
        listArInvoiceNo.sort((o1, o2) -> o1.compareTo(o2));

        try {
            
            arViewerExportComponent.doExport(workbook, listArInvoiceNo);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private List<String> getListArInvoiceNo(HttpServletRequest request) {
        String fromMonth = StringUtils.stripToEmpty(request.getParameter("fromMonth"));
        String toMonth = StringUtils.stripToEmpty(request.getParameter("toMonth"));
        String invoiceNo = StringUtils.stripToEmpty(request.getParameter("invoiceNo"));
        String payerName = StringUtils.stripToEmpty(request.getParameter("payerName"));
        String status = StringUtils.stripToEmpty(request.getParameter("status"));
		String fromArNo = StringUtils.stripToEmpty(request.getParameter("fromArNo"));
		String toArNo = StringUtils.stripToEmpty(request.getParameter("toArNo"));
		
        //
		List<String> listArInoviceNo = arInvoiceService.searchArInvoice(null, null, fromMonth, toMonth,
		                                                                    invoiceNo, payerName, status,  fromArNo, toArNo, -1, null);
        
        return listArInoviceNo;
    }
    
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.arInvoice", new Object[]{}, locale);
	}

}

