package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.DivisionImportComponent.Message.REQUIRED;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.service.DivisionMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class DivisionImportComponent extends ImportComponent {

    private static final Logger LOGGER = Logger.getLogger("DIVISION");

    private DataFormatter dataFormatter = new DataFormatter();

    @Autowired
    private DivisionMasterService divisionMasterService;

    @Override
    public ImportReturn doMerge(Workbook workbook) {
        // create log file
        logHelper.init(LOGGER, this.getLogPath());

	    int nRowSuccess = 0, nRowFail = 0;
        Sheet sheet = workbook.getSheetAt(0);
        
        for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (row != null) {
                try {
                    String code = dataFormatter.formatCellValue(row.getCell(Column.CODE));

                    //Ignore row invalid
                    if (StringUtils.isEmpty(code)) {
						logHelper.writeWarningLog(i, "Code", REQUIRED);
						nRowFail++;
						continue;
                    }

                    DivisionMaster divisionMaster = new DivisionMaster();
                    divisionMaster.setCode(code);
                    divisionMaster.setName(dataFormatter.formatCellValue(row.getCell(Column.NAME)));
                    divisionMasterService.saveDivisionMaster(divisionMaster);

                    nRowSuccess++;
                } catch (Exception e) {
                    LOGGER.log(Level.INFO, e.getMessage());

                    nRowFail++;
                }
            }
        }
        
		// write summary log and close log file.
		logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
		logHelper.close();

		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public ImportReturn doUpdate(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doReplace(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doDelete(Workbook workbook) {
        // create log file
        logHelper.init(LOGGER, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;

        Sheet sheet = workbook.getSheetAt(0);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);

            if (row != null) {
                try {
                    String code = dataFormatter.formatCellValue(row.getCell(Column.CODE));

                    // Ignore row invalid
                    if (StringUtils.isEmpty(code))
                        continue;

                    DivisionMaster divisionMaster = divisionMasterService.findDivisionMasterByCode(code);

                    if (divisionMaster != null) {
                        divisionMasterService.deleteDivisionMaster(divisionMaster);
                        nRowSuccess++;
                    } else {
                        logHelper.writeWarningLog(i, "Code", "not exist DB");
                        nRowFail++;
                    }
                } catch (Exception e) {
                    LOGGER.log(Level.INFO, e.getMessage());
                    nRowFail++;
                }
            }
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public String getName() {
    	Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.divisionMaster", new Object[]{}, locale);
    }

    class Column {
        static final int CODE = 0;
        static final int NAME = 1;

        private Column() {
        }
    }

	class Message {
		 static final String REQUIRED = "must be required";
	}
}
