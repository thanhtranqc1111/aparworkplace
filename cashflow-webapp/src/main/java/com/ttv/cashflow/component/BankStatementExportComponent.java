package com.ttv.cashflow.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.util.ApplicationUtil;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ExportReturn;

/**
 * @author thoai.nh
 * created date Nov 1, 2017
 */
@Component
@PrototypeScope
public class BankStatementExportComponent extends ExportComponent{
	@Autowired
	private BankStatementDAO bankStatementDAO;
	@Override
	public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
		// TODO Auto-generated method stub
		int success = 0, unSuccess = 0;
		String bankName = StringUtils.stripToEmpty(request.getParameter("bankName"));
		String bankAccNo = StringUtils.stripToEmpty(request.getParameter("bankAccNo"));
		String transactionFromString = StringUtils.stripToEmpty(request.getParameter("transactionFrom"));
		String transactionToString = StringUtils.stripToEmpty(request.getParameter("transactionTo"));
		Integer transDateFilter = NumberUtils.toInt(request.getParameter("transDateFilter"));
		BigInteger transactionNumber = BigInteger.ZERO;
		Calendar transactionFrom = CalendarUtil.getCalendar(transactionFromString, "dd/MM/yyyy");
		Calendar transactionTo = CalendarUtil.getCalendar(transactionToString, "dd/MM/yyyy");
		try {
			transactionNumber = new BigInteger(request.getParameter("transactionNumber"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			Set<String> setBankAccountNo = new HashSet<String>();
			Set<BankStatement> setBankStatement = new HashSet<BankStatement>();
			if(transactionNumber.compareTo(BigInteger.ZERO) > 0) {
				setBankStatement.add(bankStatementDAO.findBankStatementById(transactionNumber));
			}
			else {
				setBankStatement = bankStatementDAO.findBankStatementPaging(bankName, bankAccNo, transactionFrom, transactionTo, transDateFilter);
			}
			workbook = new XSSFWorkbook();
			workbook.createSheet();
			XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
			int index = 0;
			//bank account info style
			CellStyle styleBankAccInfo = workbook.createCellStyle();
		    styleBankAccInfo.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		    styleBankAccInfo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		    //header of bank statement style
		    CellStyle styleBankStaHeader = workbook.createCellStyle();
		    styleBankStaHeader.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		    styleBankStaHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		    Font headerFont= workbook.createFont();
		    headerFont.setColor(IndexedColors.BLACK.getIndex());
		    headerFont.setBold(true);
		    styleBankStaHeader.setFont(headerFont);
		    styleBankStaHeader.setBorderBottom(BorderStyle.HAIR);
		    styleBankStaHeader.setBorderTop(BorderStyle.HAIR);
		    styleBankStaHeader.setBorderRight(BorderStyle.HAIR);
		    styleBankStaHeader.setBorderLeft(BorderStyle.HAIR);
		    
			for (BankStatement bankStatement : setBankStatement) {
				try {
					if(setBankAccountNo.contains(bankStatement.getBankAccountNo()))
					{	
						//draw bank statement row
						drawBankStaRow(workbook, sheet, index, bankStatement);
						index++;
						success++;
						continue;
					}
					else {
						setBankAccountNo.add(bankStatement.getBankAccountNo());
						//draw bank account info
						drawBankAccountInfo(sheet, bankStatement, index, styleBankAccInfo);
						index += 3;
						//draw header for bank statement
						drawBankStaHeader(sheet, index, styleBankStaHeader);
						index ++;
						//draw bank statement row
						drawBankStaRow(workbook, sheet, index, bankStatement);
						index ++;
						
						success++;
					}
				} catch (Exception e) {
					e.printStackTrace();
					unSuccess++;
				}
			}
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.setColumnWidth(4, 255 * 40);
			sheet.setColumnWidth(5, 255 * 40);
			sheet.setColumnWidth(6, 255 * 40);
			sheet.setColumnWidth(7, 255 * 40);
			sheet.autoSizeColumn(8);
			sheet.autoSizeColumn(9);
			sheet.autoSizeColumn(10);
			sheet.autoSizeColumn(11);
			sheet.autoSizeColumn(12);
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
			workbook.write(outputStream);
			workbook.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return getExportReturn(success, unSuccess);
	}
	
	private void drawBankAccountInfo(XSSFSheet sheet, BankStatement bankStatement, int index, CellStyle style) {
		//draw bank account header
		sheet.addMergedRegion(new CellRangeAddress(index, index, 0, 12));
		Row row1 = sheet.createRow(index);
		Cell cell = row1.createCell(0);
		cell.setCellStyle(style);
		cell.setCellValue("Bank Name:" + bankStatement.getBankName());
		index++;
		
		sheet.addMergedRegion(new CellRangeAddress(index, index, 0, 12));
		Row row2 = sheet.createRow(index);
		Cell cell2 = row2.createCell(0);
		cell2.setCellStyle(style);
		cell2.setCellValue("Bank Account No:" + bankStatement.getBankAccountNo());
		index++;
		
		sheet.addMergedRegion(new CellRangeAddress(index, index, 0, 12));
		Row row3 = sheet.createRow(index);
		Cell cell3 = row3.createCell(0);
		cell3.setCellStyle(style);
		cell3.setCellValue("Currency:" + bankStatement.getCurrencyCode());
		index++;
	}
	
	private void drawBankStaHeader(XSSFSheet sheet, int index, CellStyle style) {
		//draw bank account header
		Row rowHeader = sheet.createRow(index);
		Cell cell0 = rowHeader.createCell(0);
		cell0.setCellValue("Transaction Date");
		cell0.setCellStyle(style);
		
		Cell cell1 = rowHeader.createCell(1);
		cell1.setCellValue("Value Date");
		cell1.setCellStyle(style);
		
		Cell cell2 = rowHeader.createCell(2);
		cell2.setCellValue("Currency");
		cell2.setCellStyle(style);
		
		Cell cell3 = rowHeader.createCell(3);
		cell3.setCellValue("Beginning Balance");
		cell3.setCellStyle(style);
		
		Cell cell4 = rowHeader.createCell(4);
		cell4.setCellValue("Description 1");
		cell4.setCellStyle(style);
		
		Cell cell5 = rowHeader.createCell(5);
		cell5.setCellValue("Description 2");
		cell5.setCellStyle(style);
		
		Cell cell6 = rowHeader.createCell(6);
		cell6.setCellValue("Reference 1");
		cell6.setCellStyle(style);
		
		Cell cell7 = rowHeader.createCell(7);
		cell7.setCellValue("Reference 2");
		cell7.setCellStyle(style);
		
		Cell cell8 = rowHeader.createCell(8);
		cell8.setCellValue("Debit");
		cell8.setCellStyle(style);
		
		Cell cell9 = rowHeader.createCell(9);
		cell9.setCellValue("Credit");
		cell9.setCellStyle(style);
		
		Cell cell10 = rowHeader.createCell(10);
		cell10.setCellValue("Ending Balance");
		cell10.setCellStyle(style);
		
		Cell cell11 = rowHeader.createCell(11);
		cell11.setCellValue("Payment Order No.");
		cell11.setCellStyle(style);
		
		Cell cell12 = rowHeader.createCell(12);
		cell12.setCellValue("Receipt processing No.");
		cell12.setCellStyle(style);
	}
	
	private void drawBankStaRow(Workbook workbook, XSSFSheet sheet, int index, BankStatement bankStatement) {
		//cell date style
		CellStyle dateStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(Constant.DDMMYYYY));
		dateStyle.setBorderBottom(BorderStyle.HAIR);
		dateStyle.setBorderTop(BorderStyle.HAIR);
		dateStyle.setBorderRight(BorderStyle.HAIR);
		dateStyle.setBorderLeft(BorderStyle.HAIR);
		//orther cell style
		CellStyle rowStyle = workbook.createCellStyle();
	    Font headerFont= workbook.createFont();
	    headerFont.setColor(IndexedColors.BLACK.getIndex());
	    rowStyle.setFont(headerFont);
	    rowStyle.setBorderBottom(BorderStyle.HAIR);
	    rowStyle.setBorderTop(BorderStyle.HAIR);
	    rowStyle.setBorderRight(BorderStyle.HAIR);
	    rowStyle.setBorderLeft(BorderStyle.HAIR);
	    
		//draw bank account header
		Row row = sheet.createRow(index);
		Cell cell0 = row.createCell(0);
		cell0.setCellValue(bankStatement.getTransactionDate());
		cell0.setCellStyle(dateStyle);
		
		Cell cell1 = row.createCell(1);
		cell1.setCellValue(bankStatement.getValueDate());
		cell1.setCellStyle(dateStyle);
		
		Cell cell2 = row.createCell(2);
		cell2.setCellValue(bankStatement.getCurrencyCode());
		cell2.setCellStyle(rowStyle);
		
		Cell cell3 = row.createCell(3);
		cell3.setCellValue(bankStatement.getBeginningBalance() != null? bankStatement.getBeginningBalance().doubleValue() : BigDecimal.ZERO.doubleValue());
		cell3.setCellStyle(rowStyle);
		
		Cell cell4 = row.createCell(4);
		cell4.setCellValue(bankStatement.getDescription1());
		cell4.setCellStyle(rowStyle);
		
		Cell cell5 = row.createCell(5);
		cell5.setCellValue(bankStatement.getDescription2());
		cell5.setCellStyle(rowStyle);
		
		Cell cell6 = row.createCell(6);
		cell6.setCellValue(bankStatement.getReference1());
		cell6.setCellStyle(rowStyle);
		
		Cell cell7 = row.createCell(7);
		cell7.setCellValue(bankStatement.getReference2());
		cell7.setCellStyle(rowStyle);
		
		Cell cell8 = row.createCell(8);
		cell8.setCellValue(bankStatement.getDebit() != null? bankStatement.getDebit().doubleValue() : BigDecimal.ZERO.doubleValue());
		cell8.setCellStyle(rowStyle);
		
		Cell cell9 = row.createCell(9);
		cell9.setCellValue(bankStatement.getCredit() != null? bankStatement.getCredit().doubleValue() : BigDecimal.ZERO.doubleValue());
		cell9.setCellStyle(rowStyle);
		
		Cell cell10 = row.createCell(10);
		cell10.setCellValue(bankStatement.getBalance() != null? bankStatement.getBalance().doubleValue() : BigDecimal.ZERO.doubleValue());
		cell10.setCellStyle(rowStyle);
		
		Cell cell11 = row.createCell(11);
		if(bankStatement.getPaymentOrderNo() != null) {
			cell11.setCellValue(bankStatement.getPaymentOrderNo());
		} else {
			cell11.setCellValue("");
		}
		cell11.setCellStyle(rowStyle);
		
		Cell cell12 = row.createCell(12);
		cell12.setCellValue(bankStatement.getReceiptOrderNo());
		cell12.setCellStyle(rowStyle);
		
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.bankStatement", new Object[]{}, locale);
	}
}
