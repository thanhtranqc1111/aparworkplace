package com.ttv.cashflow.component;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.service.ApprovalPurchaseRequestService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;


/**
 * @author thoai.nh
 * created date Jan 12, 2018
 */
@Component
@PrototypeScope
public class ApprovalPurchaseRequestImportComponent extends ImportComponent {
	private static final Logger logger = Logger.getLogger("APPROVAL");
	private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(ApprovalPurchaseRequestImportComponent.class);
	@Autowired
	private ApprovalPurchaseRequestService approvalService;
	@Autowired
	private ApInvoiceClassDetailsDAO apInvoiceClassDetailsDAO;
	private DataFormatter dataFormatter = new DataFormatter();
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
		int i = 0;
		int nRecordSuccess = 0;
		int nRowSuccess = 0;
		int nRecordFail = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 2) {
				try {
					nRowSuccess++;
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						ApprovalPurchaseRequest approval = new ApprovalPurchaseRequest();
						if(currentRow.getCell(Column.APPROVAL_TYPE) == null && currentRow.getCell(Column.APPROVAL_NO) == null) {
							continue;
						}
						
						if (currentRow.getCell(Column.INCLUDE_GST_ORIGINAL_AMOUNT) != null)
						{
							BigDecimal includeGstOriginalAmount = NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.INCLUDE_GST_ORIGINAL_AMOUNT).getNumericCellValue()));
							if(includeGstOriginalAmount.compareTo(BigDecimal.ZERO) < 0)
							{
								logHelper.writeWarningLog(i, "Include gst original amount", "negative (it must be zero or positive)");
								nRecordFail ++;
								continue;
							}
							else 
							{
								approval.setIncludeGstOriginalAmount(includeGstOriginalAmount);
							}
						}
						
						if (currentRow.getCell(Column.APPROVAL_TYPE) != null)
						{
							approval.setApprovalType(currentRow.getCell(Column.APPROVAL_TYPE).getStringCellValue());
						} else {
							logHelper.writeWarningLog(i, "Approval type", "required");
							nRecordFail ++;
							continue;
						}
						
						if (currentRow.getCell(Column.APPROVAL_NO) != null)
						{
							String approvalCode = currentRow.getCell(Column.APPROVAL_NO).getStringCellValue().trim();
							approval.setApprovalCode(approvalCode);
						}
						
						if(approval.getApprovalCode() == null){
							logHelper.writeWarningLog(i, "Approval no", "required");
							nRecordFail ++;
							continue;
						} else {
							//check data not exists before save
							if(approvalService.checkExists(approval) > 0) {
							    logHelper.writeErrorLog("Columns [Approval No, Total Amount Included GST(Original)] in row 8 ["
										      + approval.getApprovalCode() + ", " + approval.getIncludeGstOriginalAmount() + "] are already existed in approval data");
								nRecordFail ++;
								continue;
							}
						}
						
						if (currentRow.getCell(Column.APPLICATION_DATE) != null && currentRow.getCell(Column.APPLICATION_DATE).getCellTypeEnum() == CellType.NUMERIC)
						{
							approval.setApplicationDate(CalendarUtil.getCalendar(currentRow.getCell(Column.APPLICATION_DATE).getDateCellValue()));
						}

						if (currentRow.getCell(Column.VALIDITY_FROM) != null && currentRow.getCell(Column.VALIDITY_FROM).getCellTypeEnum() == CellType.NUMERIC)
						{
							approval.setValidityFrom(CalendarUtil.getCalendar(currentRow.getCell(Column.VALIDITY_FROM).getDateCellValue()));
						}

						if (currentRow.getCell(Column.VALIDITY_TO) != null && currentRow.getCell(Column.VALIDITY_TO).getCellTypeEnum() == CellType.NUMERIC)
						{
							approval.setValidityTo(CalendarUtil.getCalendar(currentRow.getCell(Column.VALIDITY_TO).getDateCellValue()));
						}

						if (currentRow.getCell(Column.CURRENCY) != null)
						{
							approval.setCurrencyCode(dataFormatter.formatCellValue(currentRow.getCell(Column.CURRENCY)));
						}

						if (currentRow.getCell(Column.PURPOSE) != null)
						{
							approval.setPurpose(currentRow.getCell(Column.PURPOSE).getStringCellValue());
						}

						if (currentRow.getCell(Column.APPLICANT) != null)
						{
							approval.setApplicant(currentRow.getCell(Column.APPLICANT).getStringCellValue());
						}

						if (currentRow.getCell(Column.APPROVED_DATE) != null && currentRow.getCell(Column.APPROVED_DATE).getCellTypeEnum() == CellType.NUMERIC)
						{
							approval.setApprovedDate(CalendarUtil.getCalendar(currentRow.getCell(Column.APPROVED_DATE).getDateCellValue()));
						}
						approvalService.saveApprovalPurchaseRequest(approval);
						nRecordSuccess++;
					}
				} catch (Exception e) {
					APP_LOGGER.error("Import Approval Purchase Request Error", e);
					nRecordFail++;
				}
			}
			i++;
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();		
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess, nRowSuccess, nRecordFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
		int i = 0;
		int nRowSuccess = 0;
		int nRecordFail = 0;
		int nRecordSuccess = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 2) {
				try {
					nRowSuccess ++;
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						if(currentRow.getCell(Column.APPROVAL_TYPE) == null || currentRow.getCell(Column.APPROVAL_NO) == null) {
							logHelper.writeWarningLog(i, "Approval no", "required");
							nRecordFail ++;
							continue;
						}
						else
						{
							String approvalNo = dataFormatter.formatCellValue(currentRow.getCell(Column.APPROVAL_NO)).trim();
							Set<ApInvoiceClassDetails> setApInvoiceClassDetails = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByApprovalCode(approvalNo);
							//don't remove approval assigned for invoice
							if(setApInvoiceClassDetails.size() == 0)
							{
								Set<ApprovalPurchaseRequest> set = approvalService.findApprovalByApprovalCode(approvalNo);
								if(set.size() > 0)
								{
									//delete
									for(ApprovalPurchaseRequest approval: set){
										approvalService.deleteApprovalPurchaseRequest(approval);
										nRecordSuccess ++;
									}
								}
							}
							else {
							    logHelper.writeErrorLog("Can't delete approval Code:" + approvalNo + ", because it has been assigned for an invoice.");
								nRecordFail++;
							}
						}
					}
				} catch (Exception e) {
					APP_LOGGER.error("Import Approval Purchase Request Error", e);
					nRecordFail++;
				}
			}
			i++;
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.approval", new Object[]{}, locale);
	}
	
	class Column {
		static final int APPROVAL_TYPE = 0;
		static final int APPROVAL_NO = 1;
		static final int APPLICATION_DATE = 2;
		static final int VALIDITY_FROM = 3;
		static final int VALIDITY_TO = 4;
		static final int CURRENCY = 5;
		static final int INCLUDE_GST_ORIGINAL_AMOUNT = 6;
		static final int PURPOSE = 7;
		static final int APPLICANT = 8;
		static final int APPROVED_DATE = 9;
		static final int APPLIED_FOR = 10;
	}
}
