package com.ttv.cashflow.component;


import static com.ttv.cashflow.component.ProjectAllocationExportComponent.Column.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dto.ProjectAllocationDTO;
import com.ttv.cashflow.dto.ProjectAllocationPrecentageDTO;
import com.ttv.cashflow.service.ProjectAllocationService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class ProjectAllocationExportComponent extends ExportComponent {
    
    private static final String MMYYYY = "MM/yyyy";
    
    private Calendar fromMonthAsCalendar;
    private Calendar toMonthAsCalendar;
    
    @Autowired
    private ProjectAllocationService projectAllocationService;
    
    DataFormatter df = new DataFormatter();
    
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<ProjectAllocationDTO> projectAllocationList = getListProjectAllocation(request);
        
        try {
            
            doExport(workbook, projectAllocationList);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private void doExport(Workbook workbook, List<ProjectAllocationDTO> projectAllocationList){
        
        // check parameter is valid, if not will be return.
        if(getFromMonthAsCalendar() == null || getToMonthAsCalendar() == null){
            return;
        }
        
        Sheet sheet = workbook.getSheetAt(0);
        
        // write title for list of month.
        setTitleListOfMonth(sheet);
        
        //write data.
        Row row;
        List<BigInteger> employeVisited = new ArrayList<>();
        List<Integer> rowEmployeHaveWrote = new ArrayList<>();
        
        CellStyle style1 = workbook.createCellStyle();
        style1.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        int rownum = 1;
        for(ProjectAllocationDTO projectAllocation : projectAllocationList){
            
            if(!employeVisited.contains(projectAllocation.getEmployeeId())){
                
                row = sheet.createRow(rownum);
                
                // writing employee.
                Cell empNameCell = row.createCell(EMP_NAME);
                empNameCell.setCellValue(projectAllocation.getEmployeeName());
                empNameCell.setCellStyle(style1);
                
                row.createCell(EMP_CODE).setCellValue(projectAllocation.getEmployeeCode());
                row.createCell(TEAM).setCellValue(projectAllocation.getTeam());
                
                rowEmployeHaveWrote.add(rownum);
                
                rownum++;
                
                // writing project.
                writeProjectInfo(sheet, rownum, projectAllocation);
                
                employeVisited.add(projectAllocation.getEmployeeId());
                
            } else {
                // writing project.
                writeProjectInfo(sheet, rownum, projectAllocation);
            }
            
            rownum++;

        }
        
        //
        updateStyleForSheet(sheet, rowEmployeHaveWrote);
    }

    private void setTitleListOfMonth(Sheet sheet) {
        
        int fromMonthAsInt = getFromMonthAsCalendar().get(Calendar.MONTH) + 1;
        int toMonthAsInt = getToMonthAsCalendar().get(Calendar.MONTH) + 1;
        
        int theYear = getFromMonthAsCalendar().get(Calendar.YEAR);
        
        Row firstRow = sheet.getRow(0);
        
        for (; fromMonthAsInt <= toMonthAsInt; fromMonthAsInt++) {
            Cell cell = firstRow.getCell(fromMonthAsInt + 3);
            cell.setCellValue(CalendarUtil.getCalendar(fromMonthAsInt + "/" + theYear, MMYYYY));
        }
    }
    
    private void writeProjectInfo(Sheet sheet, int rownum, ProjectAllocationDTO projectAllocation) {
        Row row = sheet.createRow(rownum);
        row.createCell(PROJECT).setCellValue(projectAllocation.getProjectName());

        // write list month
        Map<String, ProjectAllocationPrecentageDTO> mapMonthPercent = projectAllocation.getMapMonthPercent();

        for (Map.Entry<String, ProjectAllocationPrecentageDTO> entry : mapMonthPercent.entrySet()) {

            Cell cell = row.createCell(getColumn(entry.getKey()));
            cell.setCellValue(entry.getValue().getPercentAllocation().doubleValue());

        }
    }
    
    private int getColumn(String month){
        Calendar monthAsCal = CalendarUtil.getCalendar(month);
        int monthAsInt = monthAsCal.get(Calendar.MONTH) + 1;
        
        return mapMonthAndColumn.get(monthAsInt);
    }
    
    private void updateStyleForSheet(Sheet sheet, List<Integer> rowEmployeHaveWrote) {
        int numOfRow = sheet.getPhysicalNumberOfRows();
        int numOfColumn = 4 + getToMonthAsCalendar().get(Calendar.MONTH) - getFromMonthAsCalendar().get(Calendar.MONTH);
        

        CellStyle styleForProjectAllocation = sheet.getWorkbook().createCellStyle();
        setBorder(styleForProjectAllocation);
        
        CellStyle styleForEmployeeInfo = sheet.getWorkbook().createCellStyle();
        setBorder(styleForEmployeeInfo);
        styleForEmployeeInfo.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        styleForEmployeeInfo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        // Remove cell not use at header (row 1).
        for (int counterCol = numOfColumn + 1; counterCol < 16; counterCol++) {
            Row headerRow = sheet.getRow(0);
            headerRow.removeCell(headerRow.getCell(counterCol));
        }
        
        // Set style
        for (int rownum = 1; rownum < numOfRow; rownum++) {
            Row row = sheet.getRow(rownum);
            
            
            //Set style for rows are wrote employee information.
            if (rowEmployeHaveWrote.contains(rownum)) {
                for (int counterCol = 0; counterCol <= numOfColumn; counterCol++) {
                    Cell cell = row.getCell(counterCol);
                    if (cell == null) {
                        cell = row.createCell(counterCol);
                    }

                    cell.setCellStyle(styleForEmployeeInfo);
                }

            } else {
                //Set style for rows are wrote projects, which are allocation.
                for (int counterCol = 0; counterCol <= numOfColumn; counterCol++) {
                    Cell cell = row.getCell(counterCol);
                    if (cell == null) {
                        cell = row.createCell(counterCol);
                    }

                    cell.setCellStyle(styleForProjectAllocation);
                }
            }
        }
        
    }
    
    private List<ProjectAllocationDTO> getListProjectAllocation(HttpServletRequest request) {
        
        String employeeCode = StringUtils.stripToEmpty(request.getParameter("employeeCode"));
        
        String fromMonth = StringUtils.stripToEmpty(request.getParameter("fromMonth"));
        String toMonth = StringUtils.stripToEmpty(request.getParameter("toMonth"));
        Calendar fromMonthAsCalendar = CalendarUtil.getCalendar(fromMonth, MMYYYY);
        Calendar toMonthAsCalendar = CalendarUtil.getCalendar(toMonth, MMYYYY);
        
        setFromMonthAsCalendar(fromMonthAsCalendar);
        setToMonthAsCalendar(toMonthAsCalendar);
        
        Set<ProjectAllocationDTO> projectAllocationSet = null;
        
        try {
            projectAllocationSet = projectAllocationService.findProjectAllocation(employeeCode, fromMonthAsCalendar, toMonthAsCalendar);
        } catch (Exception e) {
            return new ArrayList<>();
        }
        
        List<ProjectAllocationDTO> projectAllocationList = projectAllocationSet.stream().filter(pa -> !StringUtils.isEmpty(pa.getProjectCode())).collect(Collectors.toList());
        
        return projectAllocationList;
    }
    
    /**
     * Set border for cell.
     */
    private void setBorder(CellStyle style){
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
    }
    
    public String getName(){
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.projectAllocation", new Object[]{}, locale);
    }
    
    public void setFromMonthAsCalendar(Calendar fromMonthAsCalendar) {
        this.fromMonthAsCalendar = fromMonthAsCalendar;
    }
    
    public Calendar getFromMonthAsCalendar() {
        return fromMonthAsCalendar;
    }
    
    public void setToMonthAsCalendar(Calendar toMonthAsCalendar) {
        this.toMonthAsCalendar = toMonthAsCalendar;
    }
    
    public Calendar getToMonthAsCalendar() {
        return toMonthAsCalendar;
    }
    
    class Column {
        static final int EMP_NAME = 0;
        static final int EMP_CODE = 1;
        static final int TEAM = 2;
        static final int PROJECT = 3;
        
        static final int MONTH_1 = 4;
        static final int MONTH_2 = 5;
        static final int MONTH_3 = 6;
        static final int MONTH_4 = 7;
        static final int MONTH_5 = 8;
        static final int MONTH_6 = 9;
        static final int MONTH_7 = 10;
        static final int MONTH_8 = 11;
        static final int MONTH_9 = 12;
        static final int MONTH_10 = 13;
        static final int MONTH_11 = 14;
        static final int MONTH_12 = 15;
    }
    
            
    @SuppressWarnings("serial")
    static final Map<Integer, Integer> mapMonthAndColumn = new HashMap<Integer, Integer>() {
        {
            put(1, MONTH_1);
            put(2, MONTH_2);
            put(3, MONTH_3);
            put(4, MONTH_4);
            put(5, MONTH_5);
            put(6, MONTH_6);
            put(7, MONTH_7);
            put(8, MONTH_8);
            put(9, MONTH_9);
            put(10, MONTH_10);
            put(11, MONTH_11);
            put(12, MONTH_12);
            
        }
    };

}

