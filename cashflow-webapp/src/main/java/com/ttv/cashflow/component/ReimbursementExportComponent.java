package com.ttv.cashflow.component;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dto.ReimbursementPaymentDTO;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class ReimbursementExportComponent extends ExportComponent {
    
    DataFormatter df = new DataFormatter();
    
    @Autowired
    private ReimbursementDAO reimbursementDAO;
    
    @Autowired
    private AccountPayableViewerExportComponent accountPayableViewerExportComponent;
	
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<String> reimbursementNoList  = getReimbursementNoList(request);
        //listPayrollNo.sort((o1, o2) -> o1.compareTo(o2));

        try {
            
            accountPayableViewerExportComponent.doExportForReimbursement(workbook, reimbursementNoList);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private List<String> getReimbursementNoList(HttpServletRequest request) {
        
        String fromReimbursementNo = StringUtils.stripToEmpty(request.getParameter("fromReimbursementNo"));
        String toReimbursementNo = StringUtils.stripToEmpty(request.getParameter("toReimbursementNo"));
        Calendar fromMonth = CalendarUtil.getCalendar(request.getParameter("fromMonth"), "MM/yyyy");
        Calendar toMonth = CalendarUtil.getCalendar(request.getParameter("toMonth"), "MM/yyyy");
        String employeeCode = StringUtils.stripToEmpty(request.getParameter("employeeCode"));
        Integer status = NumberUtils.toInt(request.getParameter("status"));
		
        List<ReimbursementPaymentDTO> listData = reimbursementDAO.findReimbursementPaging(-1, -1, fromMonth, toMonth, fromReimbursementNo, toReimbursementNo, employeeCode, -1, null, status);
		
        //
        List<String> reimbursementNoList = new ArrayList<>();
        for (ReimbursementPaymentDTO data : listData) {
        	if(!reimbursementNoList.contains(data.getReimbursementNo())){
        		reimbursementNoList.add(data.getReimbursementNo());
        	}
        }
        
        return reimbursementNoList;
    }
    
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.reimbursement", new Object[]{}, locale);
	}

}

