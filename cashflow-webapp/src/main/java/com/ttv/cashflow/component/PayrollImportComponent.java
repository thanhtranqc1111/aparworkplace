package com.ttv.cashflow.component;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_FOUND_DB;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_NULL_DATA;
import static com.ttv.cashflow.component.ImportComponent.ErrorMsg.WRONG_FORMAT_DATE;
import static java.util.Objects.nonNull;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ReimbursementImportComponent.Column;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.dto.PayrollDTO;
import com.ttv.cashflow.dto.PayrollDetailsDTO;
import com.ttv.cashflow.service.PayrollService;
import com.ttv.cashflow.service.impl.PayrollServiceImpl;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;


/**
 * @author thoai.nh
 * created date Jan 12, 2018
 */
@Component
@PrototypeScope
public class PayrollImportComponent extends ImportComponent {
	private static final Logger LOGGER = Logger.getLogger("PAYROLL");
	private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(PayrollImportComponent.class);
	private DataFormatter dataFormatter = new DataFormatter();
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private PayrollService payrollService;
	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
	@Autowired
	private ApprovalPurchaseRequestDAO approvalPurchaseRequestDAO;
	@Autowired
	private AccountMasterDAO accountMasterDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(LOGGER, this.getLogPath());
		int i = 0;
		int nRecordSuccess = 0;
		int nRowSuccess = 0;
		int nRecordFail = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		Set<PayrollDTO> setPayrollDTO = new HashSet<>();
		PayrollDTO lastPayrollDTO = null; //use to insert
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 3) {
				try {
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						final PayrollDTO payrollDTO = new PayrollDTO();
						if(!parsePayrollHeader(currentRow, payrollDTO, i)) {
							nRecordFail++;
							continue;
						}
						/*
						 * parse details
						 * do not continue when Employee ID isn't exists to prevent do not save all payroll
						 */
						PayrollDetailsDTO payrollDetailsDTO = new PayrollDetailsDTO();
						payrollDetailsDTO.setId(BigInteger.ZERO);
						if (currentRow.getCell(Column.EMPLOYEE_ID) != null)
						{
							String employeeID = dataFormatter.formatCellValue(currentRow.getCell(Column.EMPLOYEE_ID));
							Set<EmployeeMaster> setEmployeeMaster = employeeMasterDAO.findEmployeeMasterByCode(employeeID);
							if(setEmployeeMaster.isEmpty())
							{
								logHelper.writeWarningLog( i, "Employee ID", "required");
								nRecordFail ++;
							}
							else {
								Iterator<EmployeeMaster> iteratorEmployeeMaster = setEmployeeMaster.iterator();
								EmployeeMaster emp = iteratorEmployeeMaster.next();
								if(emp.getResignDate()!= null && emp.getResignDate().compareTo(payrollDTO.getPaymentScheduleDate()) < 0)
								{
									logHelper.writeWarningLog( i, "Resign Date", "after current date (it must be NULL or before payment schedule date)");
									nRecordFail ++;
								}
								else {
									payrollDetailsDTO.setEmployeeCode(employeeID);
								}
							}
						}
						else {
							nRecordFail ++;
						}
						
						if (currentRow.getCell(Column.ROLE_TITLE) != null)
						{
							payrollDetailsDTO.setRoleTitle(dataFormatter.formatCellValue(currentRow.getCell(Column.ROLE_TITLE)));
						}
						
						if (currentRow.getCell(Column.DIVISION) != null)
						{
							payrollDetailsDTO.setDivision(dataFormatter.formatCellValue(currentRow.getCell(Column.DIVISION)));
						}
						
						if (currentRow.getCell(Column.TEAM) != null)
						{
							payrollDetailsDTO.setTeam(dataFormatter.formatCellValue(currentRow.getCell(Column.TEAM)));
						}
						
						if (currentRow.getCell(Column.APPROVAL_CODE) != null)
						{
							String approvalCode = dataFormatter.formatCellValue(currentRow.getCell(Column.APPROVAL_CODE));
							if(CollectionUtils.isEmpty(approvalPurchaseRequestDAO.findApprovalAvailable(approvalCode, CalendarUtil.toString(payrollDTO.getBookingDate()))))
							{
								logHelper.writeErrorLog(String.format("Row [%s], %s",i, "Approval code does not exist or invalid"));
								nRecordFail ++;
								continue;	
							}
							else {
								payrollDetailsDTO.setApprovalCode(approvalCode);
								payrollDetailsDTO.setIsApproval(true);
							}
						}
						
						if (currentRow.getCell(Column.LABOR_BASE_SALARY_PAYMENT) != null)
						{
							payrollDetailsDTO.setLaborBaseSalaryPayment(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_BASE_SALARY_PAYMENT).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_1) != null)
						{
							payrollDetailsDTO.setLaborAdditionalPayment1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_2) != null)
						{
							payrollDetailsDTO.setLaborAdditionalPayment2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_3) != null)
						{
							payrollDetailsDTO.setLaborAdditionalPayment3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_ADDITIONAL_PAYMENT_3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_1) != null)
						{
							payrollDetailsDTO.setLaborSocialInsuranceEmployer1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_2) != null)
						{
							payrollDetailsDTO.setLaborSocialInsuranceEmployer2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_3) != null)
						{
							payrollDetailsDTO.setLaborSocialInsuranceEmployer3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_SOCIAL_INSURAN_CEEMPLOYER_3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_OTHER_PAYMENT_1) != null)
						{
							payrollDetailsDTO.setLaborOtherPayment1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_OTHER_PAYMENT_1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_OTHER_PAYMENT_2) != null)
						{
							payrollDetailsDTO.setLaborOtherPayment2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_OTHER_PAYMENT_2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.LABOR_OTHER_PAYMENT_3) != null)
						{
							payrollDetailsDTO.setLaborOtherPayment3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.LABOR_OTHER_PAYMENT_3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.TOTAL_LABOR_COST) != null)
						{
							payrollDetailsDTO.setTotalLaborCostOriginal(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.TOTAL_LABOR_COST).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_1) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployer1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_2) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployer2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_3) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployer3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_1) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployee1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_2) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployee2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_3) != null)
						{
							payrollDetailsDTO.setDeductionSocialInsuranceEmployee3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_WHT) != null)
						{
							payrollDetailsDTO.setDeductionWht(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_WHT).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_OTHER1) != null)
						{
							payrollDetailsDTO.setDeductionOther1(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_OTHER1).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_OTHER2) != null)
						{
							payrollDetailsDTO.setDeductionOther2(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_OTHER2).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.DEDUCTION_OTHER3) != null)
						{
							payrollDetailsDTO.setDeductionOther3(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.DEDUCTION_OTHER3).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.TOTAL_DEDUCTION) != null)
						{
							payrollDetailsDTO.setTotalDeductionOriginal(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.TOTAL_DEDUCTION).getNumericCellValue())));
						}

						if (currentRow.getCell(Column.NET_TPAYMENT_ORIGINAL) != null)
						{
							payrollDetailsDTO.setNetPaymentOriginal(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.NET_TPAYMENT_ORIGINAL).getNumericCellValue())));
						}
						
						if (currentRow.getCell(Column.NET_TPAYMENT_CONVERTED) != null)
						{
							payrollDetailsDTO.setNetPaymentConverted(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.NET_TPAYMENT_CONVERTED).getNumericCellValue())));
						}
						
						if(!setPayrollDTO.contains(payrollDTO)) {
							//init payroll
							Set<PayrollDetailsDTO> setPayrollDetailsDTO = new HashSet<>();
							payrollDTO.setPayrollDetailses(setPayrollDetailsDTO);
							payrollDTO.setPayrollNo(payrollService.getPayrollNo(payrollDTO.getBookingDate()));
							payrollDTO.setStatus(Constant.API_NOT_PAID);
							payrollDTO.setImportKey(getImportKey(payrollDTO));
							payrollDTO.setId(BigInteger.ZERO);
							
							setPayrollDTO.add(payrollDTO);
							if(lastPayrollDTO != null) {
								if(savePayroll(lastPayrollDTO, i)) {
									nRecordSuccess++;
									nRowSuccess += lastPayrollDTO.getPayrollDetailses().size();
								}
								else {
									nRecordFail += lastPayrollDTO.getPayrollDetailses().size();
								}
							}
							lastPayrollDTO = payrollDTO;
						}
						else {
							Optional<PayrollDTO> optional = setPayrollDTO.stream().filter(p -> p.equals(payrollDTO)).findFirst();
							if(optional.isPresent())
							{
								payrollDTO.setPayrollDetailses(optional.get().getPayrollDetailses());
							}
						}
						payrollDTO.getPayrollDetailses().add(payrollDetailsDTO);
					}
				} catch (Exception e) {
					nRecordFail++;
					APP_LOGGER.error("Import Payroll error", e);
				}
			
			}
		}
		if(lastPayrollDTO != null) {
			if(savePayroll(lastPayrollDTO, i)) {
				nRecordSuccess++;
				nRowSuccess += lastPayrollDTO.getPayrollDetailses().size();
			}
			else {
				nRecordFail += lastPayrollDTO.getPayrollDetailses().size();
			}
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	
	private boolean savePayroll(PayrollDTO payrollDTO, int i) {
		if(!payrollDTO.getPayrollDetailses().isEmpty() && !payrollDTO.getPayrollDetailses().stream().anyMatch(p -> p.getEmployeeCode() == null)) {
			BigInteger resultSave = payrollService.savePayroll(payrollDTO, true);
			if(resultSave.compareTo(BigInteger.ZERO) > 0)
			{
				return true;
			}
			else if(resultSave.equals(PayrollServiceImpl.SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_ORIGINAL)){
				logHelper.writeErrorLog("Inovice No : " + payrollDTO.getInvoiceNo() 
                						+ ", \"Total Net Payment Amount\" column: must be equal sum of \"Net Payment (Original)\" of all details");
				return false;
			}
			else if(resultSave.equals(PayrollServiceImpl.SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_CONVERTED)) {
				logHelper.writeErrorLog("Inovice No : " + payrollDTO.getInvoiceNo() 
										+ ", \"Total Net Payment (Converted)\" column: must be equal sum of \"Net Payment (Converted)\" of all details");
				return false;
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(LOGGER, this.getLogPath());
		int i = 0;
		int nRecordSuccess = 0;
		int nRecordFail = 0;
		int nRowSuccess = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		Set<PayrollDTO> setPayrollDTO = new HashSet<>();
		PayrollDTO lastPayrollDTO = null;
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 3) {
				try {	
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						final PayrollDTO payrollDTO = new PayrollDTO();
						if(!parsePayrollHeader(currentRow, payrollDTO, i)) {
							nRecordFail++;
							continue;
						}
						nRowSuccess ++;
						if(!setPayrollDTO.contains(payrollDTO)) {
							payrollDTO.setImportKey(getImportKey(payrollDTO));
							setPayrollDTO.add(payrollDTO);
							if(lastPayrollDTO != null) {
								if(payrollService.deletePayrollByImportKey(lastPayrollDTO.getImportKey()) == 1)
								{
									nRecordSuccess++;
								}
								else
								{
									nRecordFail++;
								}
							}
							lastPayrollDTO = payrollDTO;
						}
					}
				} catch (Exception e) {
					APP_LOGGER.error(e.getMessage());
					nRecordFail++;
				}
			
			}
		}
		if(lastPayrollDTO != null) {
			if(payrollService.deletePayrollByImportKey(lastPayrollDTO.getImportKey()) == 1)
			{
				nRecordSuccess++;
			}
			else
			{
				nRecordFail++;
			}
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess, nRowSuccess, nRecordFail);
	
	}
	
	private boolean parsePayrollHeader(Row currentRow, PayrollDTO payrollDTO, int i) {
		if (currentRow.getCell(Column.MONTH) != null && currentRow.getCell(Column.MONTH).getCellTypeEnum() == CellType.NUMERIC)
		{
			payrollDTO.setMonth(CalendarUtil.getCalendar(currentRow.getCell(Column.MONTH).getDateCellValue()));
		}
		else {
			logHelper.writeWarningLog(i, "Month", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.CLAIM_TYPE).getCellTypeEnum() != CellType.BLANK)
		{
			String claimType = dataFormatter.formatCellValue(currentRow.getCell(Column.CLAIM_TYPE));
			Iterator<ClaimTypeMaster> claimTypeIter = claimTypeMasterDAO.findClaimTypeMasterByName(claimType).iterator();
			if (claimTypeIter.hasNext()) {
	            ClaimTypeMaster claimTypeMaster = claimTypeIter.next();
	            //Claim Type column: must be "Salary".
	            if(claimTypeMaster.getCode().equals(Constant.CLAIM_TYPE_PAYROLL))
	            {
	            	 payrollDTO.setClaimType(claimTypeMaster.getCode());
	            }
	            else {
	            	logHelper.writeErrorLog("Row [" + i + "]" + ", Claim Type must be \"Salary\"");
	            } 
	        } else {
	        	logHelper.writeWarningLog(i, "Claim Type", claimType, NOT_FOUND_DB);
	        	return false;
	        }
		}
		else {
			logHelper.writeWarningLog( i, "Claim type", "required");
			return false;
		}
		
		//Set get receivable account /code
        String payableName = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_PAYABLE_NAME)).trim();
        String payableCode = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_PAYABLECODE)).trim();

    	if(StringUtils.isEmpty(payableCode)){
    		if(StringUtils.isEmpty(payableName)){
    			logHelper.writeWarningLog(i, "Account Payable Name or Account Payable Code", NOT_NULL_DATA);
    			return false;
    		} else {
    			Set<AccountMaster> payableSet = accountMasterDAO.findAccountMasterByName(payableName);
            	Iterator<AccountMaster> payableAccountMasterIter = payableSet.iterator();
            	
            	if(payableAccountMasterIter.hasNext()){
            		AccountMaster account = payableAccountMasterIter.next();
            		payrollDTO.setAccountPayableCode(account.getCode());
            		payrollDTO.setAccountPayableName(account.getName());
            	} else {
            		logHelper.writeWarningLog(i, "Account Payable Name", payableName, NOT_FOUND_DB);
            		return false;
            	}
    		}
    	} else {
    		AccountMaster payableAccountMaster = accountMasterDAO.findAccountMasterByCode(payableCode);
    		if(payableAccountMaster != null) {
    			if(StringUtils.isEmpty(payableName)){
    				payrollDTO.setAccountPayableCode(payableAccountMaster.getCode());
    				payrollDTO.setAccountPayableName(payableAccountMaster.getName());
        		} else {
        			if(payableAccountMaster.getName().equals(payableName)){
        				payrollDTO.setAccountPayableCode(payableAccountMaster.getCode());
        				payrollDTO.setAccountPayableName(payableAccountMaster.getName());
        			} else {
        				logHelper.writeErrorLog(i, "Account Payable Code", payableCode, "Account Payable Name", payableName);
            			return false;
        			}
        		}
    		} else {
    			logHelper.writeWarningLog(i, "Account Payable Code", payableCode, NOT_FOUND_DB);
                return false;
    		}
    	}
		
		String scheduledPaymentDateAsStr = dataFormatter.formatCellValue(currentRow.getCell(Column.SCHEDULED_PAYMENT_DATE));
        if (hasText(scheduledPaymentDateAsStr)) {
            Calendar scheduledPaymentDate = CalendarUtil.getCalendar(scheduledPaymentDateAsStr);
            if (nonNull(scheduledPaymentDate)) {
            	payrollDTO.setPaymentScheduleDate(scheduledPaymentDate);
            } else {
                logHelper.writeWarningLog(i, "Scheduled Payment Date", dataFormatter.formatCellValue(currentRow.getCell(Column.SCHEDULED_PAYMENT_DATE)), WRONG_FORMAT_DATE);
                return false;
            }
        }
        
		if (currentRow.getCell(Column.BOOKING_DATE) != null && currentRow.getCell(Column.BOOKING_DATE).getCellTypeEnum() == CellType.NUMERIC)
		{
			payrollDTO.setBookingDate(CalendarUtil.getCalendar(currentRow.getCell(Column.BOOKING_DATE).getDateCellValue()));
		}
		else {
			logHelper.writeWarningLog( i, "Booking Date", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.INVOICE_NO) != null)
		{
			payrollDTO.setInvoiceNo(currentRow.getCell(Column.INVOICE_NO).getStringCellValue());
		}
		
		if (currentRow.getCell(Column.ORIGINAL_CURRENCY).getCellTypeEnum() != CellType.BLANK)
		{
			payrollDTO.setOriginalCurrencyCode(currentRow.getCell(Column.ORIGINAL_CURRENCY).getStringCellValue());
		}
		else {
			logHelper.writeWarningLog( i, "Original Currency ", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.TOTAL_NET_PAYMENT_ORIGINAL).getCellTypeEnum() != CellType.BLANK)
		{
			payrollDTO.setTotalNetPaymentOriginal(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.TOTAL_NET_PAYMENT_ORIGINAL).getNumericCellValue())));
		}
		else {
			logHelper.writeWarningLog( i, "Total Net Payment Amount (Original)", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.FX_RATE).getCellTypeEnum() != CellType.BLANK)
		{
			payrollDTO.setFxRate(getBigDecimal(currentRow.getCell(Column.FX_RATE)));
		}
		else {
			logHelper.writeWarningLog( i, "Fx rate ", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.TOTAL_NET_PAYMENT_CONVERTED).getCellTypeEnum() != CellType.BLANK)
		{
			payrollDTO.setTotalNetPaymentConverted(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.TOTAL_NET_PAYMENT_CONVERTED).getNumericCellValue())));
		}
		else {
			logHelper.writeWarningLog( i, "Total Net Payment Amount (Converted)", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.DESCRIPTION) != null)
		{
			payrollDTO.setDescription(dataFormatter.formatCellValue(currentRow.getCell(Column.DESCRIPTION)));
		}
		return true;
	}
	private String getImportKey(PayrollDTO p) {
		return String.join(" ", CalendarUtil.toString(p.getMonth()), p.getClaimType(), p.getAccountPayableCode(), p.getAccountPayableName(),
								 CalendarUtil.toString(p.getPaymentScheduleDate()), CalendarUtil.toString(p.getBookingDate()));
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.payroll", new Object[]{}, locale);
	}
	
	class Column {
		static final int MONTH = 0;
		static final int CLAIM_TYPE = 1;
		static final int ACCOUNT_PAYABLECODE = 2;
		static final int ACCOUNT_PAYABLE_NAME = 3;
		static final int SCHEDULED_PAYMENT_DATE = 4;
		static final int BOOKING_DATE = 5;
		static final int INVOICE_NO = 6;
		static final int ORIGINAL_CURRENCY  = 7;
		static final int TOTAL_NET_PAYMENT_ORIGINAL = 8;
		static final int FX_RATE = 9;
		static final int TOTAL_NET_PAYMENT_CONVERTED = 10;
		static final int DESCRIPTION = 11;
		static final int EMPLOYEE_ID = 12;
		static final int ROLE_TITLE = 13;
		static final int DIVISION = 14;
		static final int TEAM = 15;
		static final int LABOR_BASE_SALARY_PAYMENT = 16;
		static final int LABOR_ADDITIONAL_PAYMENT_1 = 17;
		static final int LABOR_ADDITIONAL_PAYMENT_2 = 18;
		static final int LABOR_ADDITIONAL_PAYMENT_3 = 19;
		static final int LABOR_SOCIAL_INSURAN_CEEMPLOYER_1 = 20;
		static final int LABOR_SOCIAL_INSURAN_CEEMPLOYER_2 = 21;
		static final int LABOR_SOCIAL_INSURAN_CEEMPLOYER_3 = 22;
		static final int LABOR_OTHER_PAYMENT_1 = 23;
		static final int LABOR_OTHER_PAYMENT_2 = 24;
		static final int LABOR_OTHER_PAYMENT_3 = 25;
		static final int TOTAL_LABOR_COST = 26;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_1 = 27;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_2 = 28;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYER_3 = 29;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_1 = 30;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_2 = 31;
		static final int DEDUCTION_SOCIAL_INSURANCE_EMPLOYEE_3 = 32;
		static final int DEDUCTION_WHT = 33;
		static final int DEDUCTION_OTHER1 = 34;
		static final int DEDUCTION_OTHER2 = 35;
		static final int DEDUCTION_OTHER3 = 36;
		static final int TOTAL_DEDUCTION = 37;
		static final int APPROVAL_CODE = 38;
		static final int NET_TPAYMENT_ORIGINAL = 39;
		static final int NET_TPAYMENT_CONVERTED = 40;
	}
}
