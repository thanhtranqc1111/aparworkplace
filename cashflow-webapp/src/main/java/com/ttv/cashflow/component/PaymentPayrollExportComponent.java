package com.ttv.cashflow.component;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class PaymentPayrollExportComponent extends ExportComponent {
    
    DataFormatter df = new DataFormatter();
    
    @Autowired
    private PaymentOrderService paymentOrderService;
    
    @Autowired
    private PayrollViewerExportComponent payrollViewerExportComponent;
    
    @Autowired
    private PaymentOrderDAO paymentOrderDAO;
	
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<String> listPayrollNo  = getListPayrollNo(request);
        listPayrollNo.sort((o1, o2) -> o1.compareTo(o2));

        try {
            
            payrollViewerExportComponent.doExport(workbook, listPayrollNo);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private List<String> getListPayrollNo(HttpServletRequest request) {
        
        String dateFrom = StringUtils.stripToEmpty(request.getParameter("dateFrom"));
        String dateTo = StringUtils.stripToEmpty(request.getParameter("dateTo"));
        String bankRef = StringUtils.stripToEmpty(request.getParameter("bankRef"));
        String status = StringUtils.stripToEmpty(request.getParameter("status"));
		String fromPayrollNo = StringUtils.stripToEmpty(request.getParameter("fromPayrollNo"));
		String toPayrollNo = StringUtils.stripToEmpty(request.getParameter("toPayrollNo"));
		
		// 
		List<String> listPaymentOrderNo = paymentOrderService.searchPayementOrderPayroll(-1, -1, dateFrom, dateTo, bankRef, fromPayrollNo, toPayrollNo, status, -1, null);
		
        return getListPayrollNoHelper(listPaymentOrderNo);
    }
    
    private List<String> getListPayrollNoHelper(List<String> listPaymentOrderNo) {
        List<String> retList = new ArrayList<String>();
        
        List<PaymentOrder> paymentOrderList = paymentOrderDAO.searchPaymentDetailByOrderNo(listPaymentOrderNo);
        for (PaymentOrder payment : paymentOrderList) {
            Iterator<PayrollPaymentDetails> iter1 = payment.getPayrollPaymentDetailses().iterator();
            while (iter1.hasNext()) {
                PayrollPaymentDetails payrollPaymentDetails = iter1.next();
                
                String payrollNo = payrollPaymentDetails.getPayroll().getPayrollNo();
                if (!retList.contains(payrollNo)) {
                    retList.add(payrollNo);
                }
            }
            
        }
        return retList;
    }
    
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.paymentOrder.payroll.payment", new Object[]{}, locale);
	}

}

