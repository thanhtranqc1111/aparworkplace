package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.AccountImportComponent.Column.CODE;
import static com.ttv.cashflow.component.ImportComponent.ErrorMsg.WRONG_FORMAT_DATE;
import static com.ttv.cashflow.component.ImportComponent.ErrorMsg.NOT_FOUND_DB;

import static com.ttv.cashflow.component.EmployeeImportComponent.Message.REQUIRED;
import static com.ttv.cashflow.component.EmployeeImportComponent.Message.INVALID;

import static org.springframework.util.StringUtils.hasText;
import static java.util.Objects.isNull;

import java.util.Calendar;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.DivisionMasterDAO;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.service.DivisionMasterService;
import com.ttv.cashflow.service.EmployeeMasterService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class EmployeeImportComponent extends ImportComponent {

    private static final Logger LOGGER = Logger.getLogger("EMPLOYEE");

    private DataFormatter df = new DataFormatter();

    @Autowired
    private EmployeeMasterService employeeMasterService;

    @Autowired
    private DivisionMasterService divisionMasterService;

    @Autowired
    private DivisionMasterDAO divisionMasterDAO;

    @Override
    public ImportReturn doMerge(Workbook workbook) {
        // create log file
        logHelper.init(LOGGER, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
        boolean hasErr;
        Sheet sheet = workbook.getSheetAt(0);
        Row row;
        EmployeeMaster employeeMaster;
        DivisionMaster divisionMaster;
        Calendar joinDate;
        Calendar resignDate;
        
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            row = sheet.getRow(i);
            hasErr = false;
            divisionMaster = new DivisionMaster();
            if (null != row) {
                try {
 
                    // Ignore row invalid
					if (StringUtils.isEmpty(df.formatCellValue(row.getCell(Column.EMPLOYEE_CODE)))) {
						logHelper.writeWarningLog(i, "Code", REQUIRED);
						hasErr = true;
					}

					// validate Division Code and Division Name
					String divisionCode = df.formatCellValue(row.getCell(Column.DIVISION_CODE));
					String divisionName = df.formatCellValue(row.getCell(Column.DIVISION_NAME));

					if (StringUtils.isEmpty(divisionCode)) {
						// check Division Name
						if (StringUtils.isEmpty(divisionName)) {
							logHelper.writeWarningLog(i, "Division Code or Division Name", REQUIRED);
							hasErr = true;
						} else {
							// check Exist Division Name
							Set<DivisionMaster> divisionMasterSet = divisionMasterDAO.findDivisionMasterByName(divisionName);
							divisionMaster = !CollectionUtils.isEmpty(divisionMasterSet) ? divisionMasterSet.iterator().next() : null;
							// Division Name no exist on DivisionMaster
							if (divisionMaster == null) {
								logHelper.writeWarningLog(i, "Division Name",
										df.formatCellValue(row.getCell(Column.DIVISION_NAME)), NOT_FOUND_DB);
								hasErr = true;
							}
						}
					} else {
						// check Exist Division Code
						divisionMaster = divisionMasterService.findDivisionMasterByCode(df.formatCellValue(row.getCell(Column.DIVISION_CODE)));
						// Division Code no exist on DivisionMaster
						if (divisionMaster == null) {
							logHelper.writeWarningLog(i, "Division Code",
									df.formatCellValue(row.getCell(Column.DIVISION_CODE)), NOT_FOUND_DB);
							hasErr = true;
						} else if (!StringUtils.isEmpty(divisionName) && !divisionName.equals(divisionMaster.getName())) {
								logHelper.writeWarningLog(i, "Division Name", df.formatCellValue(row.getCell(Column.DIVISION_NAME)), NOT_FOUND_DB);
								hasErr = true;
						}
					}

        	        //joinDate
					joinDate = null;
					if (hasText(df.formatCellValue(row.getCell(Column.JOIN_DATE)))) {
						joinDate = CalendarUtil.getCalendar(df.formatCellValue(row.getCell(Column.JOIN_DATE)));
						// if joinDate is have different format "dd/MM/yyyy", return NULL.
						if (isNull(joinDate)) {
							logHelper.writeWarningLog(i, "Join Date", df.formatCellValue(row.getCell(Column.JOIN_DATE)), WRONG_FORMAT_DATE);
							hasErr = true;
						}
					} else {
						logHelper.writeWarningLog(i, "Join Date", REQUIRED);
						hasErr = true;
					}

        	        //resignDate
        	        resignDate = null;
        	        if(hasText(df.formatCellValue(row.getCell(Column.RESIGN_DATE)))){
        	        	resignDate = CalendarUtil.getCalendar(df.formatCellValue(row.getCell(Column.RESIGN_DATE)));
        	            //if resignDate is have different format "dd/MM/yyyy", return NULL.
        	            if (isNull(resignDate)) {
        	                logHelper.writeWarningLog(i, "Resign Date", df.formatCellValue(row.getCell(Column.RESIGN_DATE)), WRONG_FORMAT_DATE);
        	                hasErr = true;
        	            } else if (joinDate != null && resignDate.compareTo(joinDate) <= 0){
        	            	logHelper.writeWarningLog(i, "Resign Date", df.formatCellValue(row.getCell(Column.RESIGN_DATE)), INVALID + ". It must be later than Join Date "  + df.formatCellValue(row.getCell(Column.JOIN_DATE)));
        	                hasErr = true;
        	            }	
        	        }

        	        if (hasErr) {
        	        	nRowFail++;
        	        	continue ;
        	        }

                    employeeMaster = new EmployeeMaster();
                    employeeMaster.setCode(df.formatCellValue(row.getCell(Column.EMPLOYEE_CODE)));
                    employeeMaster.setDivisionMaster(divisionMaster);
                    employeeMaster.setName(df.formatCellValue(row.getCell(Column.EMPLOYEE_NAME)));
                    employeeMaster.setRoleTitle(df.formatCellValue(row.getCell(Column.ROLE_TITLE)));
                    employeeMaster.setTeam(df.formatCellValue(row.getCell(Column.TEAM)));
                    employeeMaster.setJoinDate(joinDate);
                    employeeMaster.setResignDate(resignDate);
                    
                    employeeMasterService.saveEmployeeMaster(employeeMaster);

                    nRowSuccess++;
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                    nRowFail++;
                }
            }
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public ImportReturn doUpdate(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doReplace(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doDelete(Workbook workbook) {
        // create log file
        logHelper.init(LOGGER, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;

        Sheet sheet = workbook.getSheetAt(0);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);

            if (row != null) {
                try {
                    String code = df.formatCellValue(row.getCell(CODE));

                    // Ignore row invalid
                    if (StringUtils.isEmpty(code)) {
                        continue;
                    }

                    EmployeeMaster employeeMaster = employeeMasterService.findEmployeeMasterByCode(code);

                    if (employeeMaster != null) {
                        employeeMasterService.deleteEmployeeMaster(employeeMaster);
                        nRowSuccess++;
                    } else {
                        logHelper.writeWarningLog(i, "Code", "not exist DB");
                        nRowFail++;
                    }
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                    nRowFail++;
                }
            }
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public String getName() {
    	Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.employeeMaster", new Object[]{}, locale);
    }

    class Column {
        static final int EMPLOYEE_CODE = 0;
        static final int DIVISION_CODE = 1;
        static final int DIVISION_NAME = 2;
        static final int EMPLOYEE_NAME = 3;
        static final int ROLE_TITLE = 4;
        static final int TEAM = 5;
        static final int JOIN_DATE = 6;
        static final int RESIGN_DATE = 7;
        private Column() {
        }
    }
    
	class Message {
		 static final String REQUIRED = "must be required";
		 static final String INVALID = "invalid";
	}
}
