package com.ttv.cashflow.component;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.ttv.cashflow.helper.LogHelper;
import com.ttv.cashflow.vo.ExportReturn;


public abstract class ExportComponent {
    
    @Autowired
    protected LogHelper logHelper;
    
    @Autowired
    protected MessageSource messageSource;

    private String logPath;

    private String filePath;
    

    protected ExportReturn getExportReturn() {
        return new ExportReturn();
    }
    
    protected ExportReturn getExportReturn(int nRecordSuccess, int nRecordFail){
	    return new ExportReturn(getName(), nRecordSuccess, nRecordFail);
	}
    
    public abstract ExportReturn doExport(Workbook workbook, HttpServletRequest request);
    
    public String getName() {
        return "EXPORT";
    }
    
    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
    /**
     * Remove old contents
     * @param sheet
     * @param hRow -- Header row
     */
    public void  removeContents(XSSFSheet sheet, int hRow) {
    	int nRow = sheet.getPhysicalNumberOfRows();
		for (int i = hRow; i < nRow; i++) {
			Row row = sheet.getRow(i); 
			if (null != row) {
				try {
					sheet.removeRow(row);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
    
}
