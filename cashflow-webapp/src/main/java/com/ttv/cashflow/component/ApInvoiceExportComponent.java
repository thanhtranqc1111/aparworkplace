package com.ttv.cashflow.component;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class ApInvoiceExportComponent extends ExportComponent {
    
    private static final Logger logger = Logger.getLogger("AP INVOICE");
    
    DataFormatter df = new DataFormatter();
    
    @Autowired
    private ApInvoiceService apInvoiceService;
    
    @Autowired
    private AccountPayableViewerExportComponent accountPayableViewerExportComponent;
	
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<String> listApInvoiceNo  = getListApInvoiceNo(request);
        //listApInvoiceNo.sort((o1, o2) -> o1.compareTo(o2));

        try {
            
            accountPayableViewerExportComponent.doExportForAPInvoice(workbook, listApInvoiceNo);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private List<String> getListApInvoiceNo(HttpServletRequest request) {
        
        String fromMonth = StringUtils.stripToEmpty(request.getParameter("fromMonth"));
        String toMonth = StringUtils.stripToEmpty(request.getParameter("toMonth"));
        String payeeName = StringUtils.stripToEmpty(request.getParameter("payeeName"));
        String invoiceNo = StringUtils.stripToEmpty(request.getParameter("invoiceNo"));
        String status = StringUtils.stripToEmpty(request.getParameter("status"));
		String fromApNo = StringUtils.stripToEmpty(request.getParameter("fromApNo"));
		String toApNo = StringUtils.stripToEmpty(request.getParameter("toApNo"));
        //
        List<PaymentInvoice> listData = apInvoiceService.searchInvoice(null, null, fromMonth, toMonth, invoiceNo, payeeName, status, fromApNo, toApNo);
        
        //
        List<String> listApInvoiceNo = new ArrayList<>();
        for (Object data : listData) {
            listApInvoiceNo.add(data.toString());
        }
        
        return listApInvoiceNo;
    }
    
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.apInvoice", new Object[]{}, locale);
	}

}

