package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ArViewerExportComponent.Column.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.dao.ArViewerDAO;
import com.ttv.cashflow.domain.ArViewer;
import com.ttv.cashflow.util.Constant;

@Component
@PrototypeScope
public class ArViewerExportComponent {
    
    private static final int START_ROW_DATA_IN_TEMPLATE = 2;
    
    @Autowired
    private ArViewerDAO arViewerDAO;
    
    @Autowired
    private ArInvoiceDAO arInvoiceDAO;
    
    public void doExport(Workbook workbook, List<String> listArInvoiceNo) throws Exception{
        DataFormat format = workbook.createDataFormat();
        CreationHelper createHelper = workbook.getCreationHelper();
        XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
        CellStyle style = null;
        Cell cell = null;
        
        int rowNum = START_ROW_DATA_IN_TEMPLATE;
        
        for (String arInvoiceNo : listArInvoiceNo) {
            int countAriDetail = 0;
            BigInteger numDetail = BigInteger.ZERO;
            boolean hasWriteReceipt = false;
            
            arViewerDAO.refresh();
            Set<ArViewer> arViewerSet = arViewerDAO.findArViewerByArInvoiceNo(arInvoiceNo);
            Iterator<ArViewer> apViewerIter = arViewerSet.iterator();
            
            while (apViewerIter.hasNext()) {
                
                ArViewer arViewer = apViewerIter.next();
                Row row = sheet.createRow(rowNum);
                
                //////////PUT DATA//////////
                
                row.createCell(AR_NO).setCellValue(arViewer.getArInvoiceNo());
                
                //Set month
                style = workbook.createCellStyle();
                formatDate(style, Constant.MMMYYYY, createHelper);
                
                cell = row.createCell(MONTH);
                cell.setCellValue(arViewer.getMonth());
                cell.setCellStyle(style);
                
                //
                row.createCell(PAYER).setCellValue(arViewer.getPayerName());
                row.createCell(INVOICE_NO).setCellValue(arViewer.getInvoiceNo());
                row.createCell(APPROVAL_CODE).setCellValue(arViewer.getApprovalCode());
                row.createCell(ORIGINAL_CURRENCY).setCellValue(arViewer.getOriginalCurrencyCode());
                
                //ExcludeGstOriginalAmount
                style = workbook.createCellStyle();
                formatNumeric(style, format);
                
                cell = row.createCell(EXCLUDE_GST_ORIGINAL_AMOUNT);
                cell.setCellValue(doubleValue(arViewer.getExcludeGstOriginalAmount()));
                cell.setCellStyle(style);
                
                //
                row.createCell(GST_TYPE).setCellValue(arViewer.getGstType());
                row.createCell(GST_RATE).setCellValue(doubleValue(arViewer.getGstRate()));
                
                //IncludeGstOriginalAmount
                style = workbook.createCellStyle();
                formatNumeric(style, format);
                
                cell = row.createCell(INCLUDE_GST_ORIGINAL_AMOUNT);
                cell.setCellValue(doubleValue(arViewer.getIncludeGstOriginalAmount()));
                cell.setCellStyle(style);
                
                //FxRate
                style = workbook.createCellStyle();
                formatNumeric(style, format);
                
                cell = row.createCell(FX_RATE);
                cell.setCellValue(doubleValue(arViewer.getFxRate()));
                cell.setCellStyle(style);
                
                //IncludeGstConvertedAmount
                style = workbook.createCellStyle();
                formatNumeric(style, format);
                
                cell = row.createCell(INCLUDE_GST_CONVERTED_AMOUNT);
                cell.setCellValue(doubleValue(arViewer.getIncludeGstConvertedAmount()));
                cell.setCellStyle(style);
                
                //
                row.createCell(ACCOUNT_NAME).setCellValue(arViewer.getAccountName());
                row.createCell(CLASS_NAME).setCellValue(arViewer.getClassName());
                row.createCell(DESCRIPTION).setCellValue(arViewer.getDescription());
                
                // reset count detail of AR invoice (countAriDetail) and flag(hasWriteReceipt)
                // for write RECEIPT section when multiple RECEIPT is paid for 1 AR invoice. 
                if(countAriDetail == numDetail.intValue()){
                    countAriDetail = 0;
                    hasWriteReceipt = false;
                }
                
                //Write RECEIPT section.
                if(!hasWriteReceipt && Objects.nonNull((arViewer.getReceiptOrderId()))){
                    
                    numDetail = arInvoiceDAO.countDetail(arInvoiceNo);
                    int fistRow = rowNum;
                    int lastRow = rowNum + numDetail.intValue() - 1;
                    
                    //Merge row
                    if (fistRow != lastRow) {
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, RECEIPT_ORDER_ID, RECEIPT_ORDER_ID));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, TRANSATION_ID, TRANSATION_ID));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, TRANSATION_DATE, TRANSATION_DATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, SETTED_INCLUDE_GST_ORIGINAL_AMOUNT, SETTED_INCLUDE_GST_ORIGINAL_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, RECEIPT_RATE, RECEIPT_RATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, SETTED_INCLUDE_GST_CONVERTED_AMOUNT, SETTED_INCLUDE_GST_CONVERTED_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VARIANCE_AMOUNT, VARIANCE_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, REMAIN_INCLUDE_GST_ORIGINAL_AMOUNT, REMAIN_INCLUDE_GST_ORIGINAL_AMOUNT));
                    }
                    
                    //RECEIPT_ORDER_ID
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    
                    cell = row.createCell(RECEIPT_ORDER_ID);
                    cell.setCellValue(doubleValue(arViewer.getReceiptOrderId()));
                    cell.setCellStyle(style);
                    
                    //TransationId
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    
                    cell = row.createCell(TRANSATION_ID);
                    cell.setCellValue(doubleValue(arViewer.getTransactionId()));
                    cell.setCellStyle(style);

                    //ValueDate
                    style = workbook.createCellStyle();
                    formatDate(style, Constant.DDMMYYYY, createHelper);
                    setVerticalAlignCenter(style);
                    
                    cell = row.createCell(TRANSATION_DATE);
                    cell.setCellValue(arViewer.getTransactionDate());
                    cell.setCellStyle(style);

                    //SETTED_INCLUDE_GST_ORIGINAL_AMOUNT
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    formatNumeric(style, format);
                    
                    cell = row.createCell(SETTED_INCLUDE_GST_ORIGINAL_AMOUNT);
                    cell.setCellValue(doubleValue(arViewer.getSettedIncludeGstOriginalAmount()));
                    cell.setCellStyle(style);
                    
                    //RECEIPT_RATE
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    formatNumeric(style, format);
                    
                    cell = row.createCell(RECEIPT_RATE);
                    cell.setCellValue(doubleValue(arViewer.getReceiptRate()));
                    cell.setCellStyle(style);
                    
                    //SETTED_INCLUDE_GST_CONVERTED_AMOUNT
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    formatNumeric(style, format);
                    
                    cell = row.createCell(SETTED_INCLUDE_GST_CONVERTED_AMOUNT);
                    cell.setCellValue(doubleValue(arViewer.getSettedIncludeGstConvertedAmount()));
                    cell.setCellStyle(style);
                    
                    //VarianceAmount
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    formatNumeric(style, format);
                    
                    cell = row.createCell(VARIANCE_AMOUNT);
                    cell.setCellValue(doubleValue(arViewer.getVarianceAmount()));
                    cell.setCellStyle(style);
                    
                    //REMAIN_INCLUDE_GST_ORIGINAL_AMOUNT
                    style = workbook.createCellStyle();
                    setVerticalAlignCenter(style);
                    formatNumeric(style, format);
                    
                    cell = row.createCell(REMAIN_INCLUDE_GST_ORIGINAL_AMOUNT);
                    cell.setCellValue(doubleValue(arViewer.getRemainIncludeGstOriginalAmount()));
                    cell.setCellStyle(style);
                    
                    //
                    hasWriteReceipt = true;
                }
                
                countAriDetail++;
                rowNum++;
                
            } // end while
        }// end for
    }
    
    private Double doubleValue(BigInteger val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    private Double doubleValue(BigDecimal val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    private void setVerticalAlignCenter(CellStyle cellStyle){
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    }
    
    private void formatDate(CellStyle cellStyle,  String format, CreationHelper createHelper){
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
    }
    
    /**
     * Set format number.
     * <p>Example:
     * <ul>
     * <li>3456.70  -> 3,456.70
     * <li>3456.789 -> 3,456.78
     * </ul>
     * <p>
     * 
     */
    private void formatNumeric(CellStyle cellStyle, DataFormat format){
        cellStyle.setDataFormat(format.getFormat("#,##0.00"));
    }
	
	class Column {
	    static final int AR_NO = 0;
	    static final int MONTH = 1;
	    static final int PAYER = 2;
	    static final int INVOICE_NO = 3;
	    static final int APPROVAL_CODE = 4;
	    static final int ORIGINAL_CURRENCY = 5;
	    /**
	     *  Amount Excluded GST(Original)
	     */
	    static final int EXCLUDE_GST_ORIGINAL_AMOUNT = 6;
	    static final int GST_TYPE = 7;
	    /**
	     *  GST (%) Rate
	     */
	    static final int GST_RATE = 8;
	    /**
	     *  Amount Included GST(Original)
	     */
	    static final int INCLUDE_GST_ORIGINAL_AMOUNT = 9;
	    static final int FX_RATE = 10;
	    /**
	     *  Amount Included GST(Converted)
	     */
	    static final int INCLUDE_GST_CONVERTED_AMOUNT = 11;
	    /**
	     *  Corresponding Accounts
	     */
	    static final int ACCOUNT_NAME = 12;
	    /**
	     *  Project
	     */
	    static final int CLASS_NAME = 13;
	    /**
	     *  Remarks
	     */
	    static final int DESCRIPTION = 14;
	    static final int RECEIPT_ORDER_ID = 15;
	    static final int TRANSATION_ID = 16;
	    /**
	     *  Transation Date(dd/mm/yyyy)
	     */
	    static final int TRANSATION_DATE = 17;
	    /**
	     *  Amount (Original)
	     */
	    static final int SETTED_INCLUDE_GST_ORIGINAL_AMOUNT = 18;
	    static final int RECEIPT_RATE = 19;
	    /**
	     *  Amount (Converted)
	     */
	    static final int SETTED_INCLUDE_GST_CONVERTED_AMOUNT = 20;
	    static final int VARIANCE_AMOUNT = 21;
	    /**
	     *  Remain Amount(Original)
	     */
	    static final int REMAIN_INCLUDE_GST_ORIGINAL_AMOUNT = 22;
	}

}

