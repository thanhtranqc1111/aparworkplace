	package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_FOUND_DB;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_NULL_DATA;
import static com.ttv.cashflow.component.ImportComponent.ErrorMsg.WRONG_FORMAT_DATE;
import static java.util.Objects.nonNull;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.domain.GstMaster;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.dto.ReimbursementDTO;
import com.ttv.cashflow.dto.ReimbursementDetailsDTO;
import com.ttv.cashflow.service.ReimbursementService;
import com.ttv.cashflow.service.impl.ReimbursementServiceImpl;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;


/**
 * @author thoai.nh
 * created date Apr 13, 2018
 */
@Component
@PrototypeScope
public class ReimbursementImportComponent extends ImportComponent {
	private static final Logger LOGGER = Logger.getLogger("REIMBURSEMENT");
	private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(ReimbursementImportComponent.class);
	private DataFormatter dataFormatter = new DataFormatter();
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private ReimbursementService reimbursementService;
	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
	@Autowired
	private ApprovalPurchaseRequestDAO approvalDAO;
	@Autowired
	private GstMasterDAO gstMasterDAO;
	@Autowired
	private AccountMasterDAO accountMasterDAO;
	@Autowired
	private ProjectMasterDAO projectMasterDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(LOGGER, this.getLogPath());
		int i = 0;
		int nRecordSuccess = 0;
		int nRowSuccess = 0;
		int nRecordFail = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		Set<ReimbursementDTO> setReimbursementDTO = new HashSet<>();//set used to check multiple rows are the same on reimbursement
		ReimbursementDTO lastReimbursementDTO = null; //use to insert
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 3) {
				try {	
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						final ReimbursementDTO reimbursementDTO = new ReimbursementDTO();
						if(!parseReimbursementHeader(currentRow, reimbursementDTO, i)) {
							nRecordFail++;
							continue;
						}
						/*
						 * parse details
						 */
						ReimbursementDetailsDTO reimbursementDetailsDTO = new ReimbursementDetailsDTO();
						 // set account
				        String accountName = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_NAME)).trim();
				        String accountCode = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_CODE)).trim();
				        
				        if(StringUtils.isEmpty(accountCode)){
				    		if(StringUtils.isEmpty(accountName)){
				    			logHelper.writeWarningLog(i, "Account Name or Account Code", NOT_NULL_DATA);
				    			nRecordFail ++;
								continue;
				    		} else {
				    			Set<AccountMaster> accountSet = accountMasterDAO.findAccountMasterByName(accountName);
						        Iterator<AccountMaster> accountIter = accountSet.iterator();
						        if(accountIter.hasNext()){
						        	AccountMaster account = accountIter.next();
						        	reimbursementDetailsDTO.setAccountCode(account.getCode());
						        	reimbursementDetailsDTO.setAccountName(account.getName());
						        } else {
						        	logHelper.writeWarningLog(i, "Account Name", accountCode, NOT_FOUND_DB);
						        	nRecordFail ++;
									continue;
						        }
				    		}
				    	} else {
				    		AccountMaster accountMaster = accountMasterDAO.findAccountMasterByCode(accountCode);
				    		if(accountMaster != null) {
				    			if(StringUtils.isEmpty(accountName)){
				    				reimbursementDetailsDTO.setAccountCode(accountMaster.getCode());
				    				reimbursementDetailsDTO.setAccountName(accountMaster.getName());
				    			} else {
				    				if(accountMaster.getName().equals(accountName)){
				    					reimbursementDetailsDTO.setAccountCode(accountMaster.getCode());
				    					reimbursementDetailsDTO.setAccountName(accountMaster.getName());
				    				} else {
				    					logHelper.writeErrorLog(i, "Account Code", accountCode, "Account Name", accountName);
				    					nRecordFail ++;
										continue;
				    				}
				    			}
				    		} else {
				    			logHelper.writeWarningLog(i, "Account Code", accountCode, NOT_FOUND_DB);
				    			nRecordFail ++;
								continue;
				    		}
				    	}
				        
				        // set project name
				        String projectCode = dataFormatter.formatCellValue(currentRow.getCell(Column.PROJECT_CODE)).trim();
				        String projectName = dataFormatter.formatCellValue(currentRow.getCell(Column.PROJECT_NAME)).trim();
				        
				        if(StringUtils.isEmpty(projectCode)){
				    		if(StringUtils.isEmpty(projectName)){
				    			logHelper.writeWarningLog(i, "Project Name or Project Code", NOT_NULL_DATA);
				    			nRecordFail ++;
								continue;
				    		} else {
				    			Set<ProjectMaster> projectSet = projectMasterDAO.findProjectMasterByName(projectName);
				    	        Iterator<ProjectMaster> projectIter = projectSet.iterator();
				    	        if(projectIter.hasNext()){
				    	        	ProjectMaster project = projectIter.next();
				    	        	reimbursementDetailsDTO.setProjectCode(project.getCode());
				    	        	reimbursementDetailsDTO.setProjectName(project.getName());
				    	        } else {
				    	        	logHelper.writeWarningLog(i, "Project Name", projectName, NOT_FOUND_DB);
				    	        	nRecordFail ++;
									continue;
				    	        }
				    		}
				    	} else {
				    		ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(projectCode);
				    		if(projectMaster != null) {
				    			if(StringUtils.isEmpty(projectName)){
				    				reimbursementDetailsDTO.setProjectCode(projectMaster.getCode());
				    				reimbursementDetailsDTO.setProjectName(projectMaster.getName());
				    			} else {
				    				if(projectMaster.getName().equals(projectName)){
				    					reimbursementDetailsDTO.setProjectCode(projectMaster.getCode());
				    					reimbursementDetailsDTO.setProjectName(projectMaster.getName());
				    				} else {
				    					logHelper.writeErrorLog(i, "Project Code", projectCode, "Project Name", projectName);
				    					nRecordFail ++;
										continue;
				    				}
				    			}
				    		} else {
				    			logHelper.writeWarningLog(i, "Project Code", projectCode, NOT_FOUND_DB);
				    			nRecordFail ++;
								continue;
				    		}
				    	}
				        
						if (currentRow.getCell(Column.APPROVAL_NO) != null)
						{
							String approvalCode = dataFormatter.formatCellValue(currentRow.getCell(Column.APPROVAL_NO));
							if(CollectionUtils.isEmpty(approvalDAO.findApprovalAvailable(approvalCode, CalendarUtil.toString(reimbursementDTO.getBookingDate()))))
							{
								logHelper.writeErrorLog(String.format("Row [%s], %s",i, "Approval code does not exist or invalid"));
								nRecordFail ++;
								continue;	
							}
							else {
								reimbursementDetailsDTO.setApprovalCode(approvalCode);
								reimbursementDetailsDTO.setIsApproval(true);
							}
						}
						
						if (currentRow.getCell(Column.DESCRIPTION_DETAIL) != null)
						{
							reimbursementDetailsDTO.setDescription(dataFormatter.formatCellValue(currentRow.getCell(Column.DESCRIPTION_DETAIL)));
						}
						
						if (currentRow.getCell(Column.EXCLUDE_GST_ORIGINAL_AMOUNT) != null)
						{
							reimbursementDetailsDTO.setExcludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.EXCLUDE_GST_ORIGINAL_AMOUNT).getNumericCellValue())));
						}
						
						if (currentRow.getCell(Column.INCLUDE_GST_CONVERTED_AMOUNT) != null)
						{
							reimbursementDetailsDTO.setIncludeGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.INCLUDE_GST_CONVERTED_AMOUNT).getNumericCellValue())));
						}
						
						if(!setReimbursementDTO.contains(reimbursementDTO)) {
							//init reimbursement
							Set<ReimbursementDetailsDTO> setReimbursementDetailsDTO = new HashSet<>();
							reimbursementDTO.setReimbursementDetailses(setReimbursementDetailsDTO);
							reimbursementDTO.setReimbursementNo(reimbursementService.getReimbursementNo(reimbursementDTO.getBookingDate()));
							reimbursementDTO.setStatus(Constant.API_NOT_PAID);
							reimbursementDTO.setImportKey(getImportKey(reimbursementDTO));
							reimbursementDTO.setId(BigInteger.ZERO);
							
							setReimbursementDTO.add(reimbursementDTO);
							if(lastReimbursementDTO != null) {
								if(saveReimbursement(lastReimbursementDTO, i)) {
									nRecordSuccess++;
									nRowSuccess += lastReimbursementDTO.getReimbursementDetailses().size();
								}
								else {
									nRecordFail += lastReimbursementDTO.getReimbursementDetailses().size();
								}
							}
							lastReimbursementDTO = reimbursementDTO;
						}
						else {
							Optional<ReimbursementDTO> optional = setReimbursementDTO.stream().filter(p -> p.equals(reimbursementDTO)).findFirst();
							if(optional.isPresent())
							{
								reimbursementDTO.setReimbursementDetailses(optional.get().getReimbursementDetailses());
							}
						}
						reimbursementDTO.getReimbursementDetailses().add(reimbursementDetailsDTO);
					}
				} catch (Exception e) {
					nRecordFail++;
					APP_LOGGER.error("Import Reimbursement Error", e);
				}
			
			}
		}
		if(lastReimbursementDTO != null) {
			if(saveReimbursement(lastReimbursementDTO, i)) {
				nRecordSuccess++;
				nRowSuccess += lastReimbursementDTO.getReimbursementDetailses().size();
			}
			else {
				nRecordFail += lastReimbursementDTO.getReimbursementDetailses().size();
			}
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess ,nRowSuccess, nRecordFail);
	}
	
	private boolean saveReimbursement(ReimbursementDTO reimbursementDTO, int i) {
		//automatic caculate some value
		reimbursementDTO.setExcludeGstTotalConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDTO.getExcludeGstTotalOriginalAmount()
																		   .multiply(reimbursementDTO.getFxRate())));
		reimbursementDTO.setGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDTO.getExcludeGstTotalOriginalAmount()
																		  			  .multiply(reimbursementDTO.getGstRate())
																		  			  .divide(BigDecimal.valueOf(100))));
		reimbursementDTO.setGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDTO.getExcludeGstTotalConvertedAmount()
																		  			  .multiply(reimbursementDTO.getGstRate())
																		  			  .divide(BigDecimal.valueOf(100))));
		reimbursementDTO.setIncludeGstTotalOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDTO.getExcludeGstTotalOriginalAmount()
				   																				.add(reimbursementDTO.getGstOriginalAmount())));
		for(ReimbursementDetailsDTO reimbursementDetailsDTO: reimbursementDTO.getReimbursementDetailses()) {
			reimbursementDetailsDTO.setExcludeGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDetailsDTO.getExcludeGstOriginalAmount()
					   																					.multiply(reimbursementDTO.getFxRate())));
			reimbursementDetailsDTO.setGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDetailsDTO.getExcludeGstOriginalAmount()
																							  .multiply(reimbursementDTO.getGstRate())
																							  .divide(BigDecimal.valueOf(100))));
			reimbursementDetailsDTO.setGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDetailsDTO.getExcludeGstConvertedAmount()
																								  .multiply(reimbursementDTO.getGstRate())
																								  .divide(BigDecimal.valueOf(100))));
			reimbursementDetailsDTO.setIncludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, reimbursementDetailsDTO.getExcludeGstOriginalAmount()
																										.add(reimbursementDetailsDTO.getGstOriginalAmount())));
		}
		BigInteger resultSave = reimbursementService.saveReimbursement(reimbursementDTO, true);
		if(resultSave.compareTo(BigInteger.ZERO) > 0)
		{
			return true;
		}
		else if(resultSave.equals(ReimbursementServiceImpl.SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_ORIGINAL)){
			LOGGER.severe("Inovice No : " + reimbursementDTO.getInvoiceNo()  + ", \"Total Reimbursement Amount exclude GST (Original)\" column: must be equal sum of \"Reimbursement Amount exclude GST (Original) \" of parent");
			return false;
		}
		else if(resultSave.equals(ReimbursementServiceImpl.SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_CONVERTED)) {
			LOGGER.severe("Inovice No : " + reimbursementDTO.getInvoiceNo()  + ", \"Total Reimbursement Amount include GST (Converted)\" column: must be equal sum of \"Reimbursement Amount include GST (Converted)\" column parent");
			return false;
		}
		else {
			return false;
		}
	}
	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		
		//create log file
		logHelper.init(LOGGER, this.getLogPath());
		int i = 0;
		int nRecordSuccess = 0;
		int nRecordFail = 0;
		int nRowSuccess = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		Set<ReimbursementDTO> setReimbursementDTO = new HashSet<>();
		ReimbursementDTO lastReimbursementDTO = null; //use to insert
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if (i >= 3) {
				try {	
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						final ReimbursementDTO reimbursementDTO = new ReimbursementDTO();
						if(!parseReimbursementHeader(currentRow, reimbursementDTO, i)) {
							nRecordFail++;
							continue;
						}
						nRowSuccess ++;
						if(!setReimbursementDTO.contains(reimbursementDTO)) {
							reimbursementDTO.setImportKey(getImportKey(reimbursementDTO));
							setReimbursementDTO.add(reimbursementDTO);
							if(lastReimbursementDTO != null) {
								if(reimbursementService.deleteReimbursementByImportKey(lastReimbursementDTO.getImportKey()) == 1)
								{
									nRecordSuccess++;
								}
								else
								{
									nRecordFail++;
								}
							}
							lastReimbursementDTO = reimbursementDTO;
						}
					}
				} catch (Exception e) {
					APP_LOGGER.error("Import Reimbursement Error", e);
					nRecordFail++;
				}
			
			}
		}
		if(lastReimbursementDTO != null) {
			if(reimbursementService.deleteReimbursementByImportKey(lastReimbursementDTO.getImportKey()) == 1)
			{
				nRecordSuccess++;
			}
			else
			{
				nRecordFail++;
			}
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	private boolean parseReimbursementHeader(Row currentRow, ReimbursementDTO reimbursementDTO, int i) {
		if (currentRow.getCell(Column.EMPLOYEE_ID).getCellTypeEnum() != CellType.BLANK)
		{
			String employeeID = dataFormatter.formatCellValue(currentRow.getCell(Column.EMPLOYEE_ID));
			Set<EmployeeMaster> setEmployeeMaster = employeeMasterDAO.findEmployeeMasterByCode(employeeID);
			if(setEmployeeMaster.isEmpty())
			{
				logHelper.writeWarningLog( i, "Employee", "not exists in the system");
				return false;
			}
			else {
				Iterator<EmployeeMaster> iteratorEmployeeMaster = setEmployeeMaster.iterator();
				EmployeeMaster emp = iteratorEmployeeMaster.next();
				if(emp.getResignDate()!= null && emp.getResignDate().compareTo(Calendar.getInstance()) < 0)
				{
					logHelper.writeWarningLog( i, "Resign Date", "after current date (it must be NULL or before current date)");
					return false;
				}
				else {
					reimbursementDTO.setEmployeeCode(employeeID);
				}
			}
		}
		else {
			logHelper.writeWarningLog( i, "Employee ID", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.EMPLOYEE_NAME).getCellTypeEnum() != CellType.BLANK)
		{
			reimbursementDTO.setEmployeeName(dataFormatter.formatCellValue(currentRow.getCell(Column.EMPLOYEE_NAME)));
		}
		else {
			logHelper.writeWarningLog( i, "Employee name", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.BOOKING_DATE) != null && currentRow.getCell(Column.BOOKING_DATE).getCellTypeEnum() == CellType.NUMERIC)
		{
			reimbursementDTO.setBookingDate(CalendarUtil.getCalendar(currentRow.getCell(Column.BOOKING_DATE).getDateCellValue()));
		}
		else {
			logHelper.writeWarningLog( i, "Booking Date", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.MONTH) != null && currentRow.getCell(Column.MONTH).getCellTypeEnum() == CellType.NUMERIC)
		{
			reimbursementDTO.setMonth(CalendarUtil.getCalendar(currentRow.getCell(Column.MONTH).getDateCellValue()));
		}
		else {
			logHelper.writeWarningLog( i, "Month", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.CLAIM_TYPE).getCellTypeEnum() != CellType.BLANK)
		{
			String claimType = currentRow.getCell(Column.CLAIM_TYPE).getStringCellValue();
			Iterator<ClaimTypeMaster> claimTypeIter = claimTypeMasterDAO.findClaimTypeMasterByName(claimType).iterator();
			if (claimTypeIter.hasNext()) {
	            ClaimTypeMaster claimTypeMaster = claimTypeIter.next();
	            //Claim Type column: must be "Reimbursement".
	            if(claimTypeMaster.getCode().equals(Constant.CLAIM_TYPE_REIMBURSEMENT))
	            {
	            	reimbursementDTO.setClaimType(claimTypeMaster.getCode());
	            }
	            else {
	            	LOGGER.severe("Row [" + i + "]" + ", Claim Type must be \"Reimbursement\"");
	            } 
	        } else {
	        	logHelper.writeWarningLog(i, "Claim Type", claimType, NOT_FOUND_DB);
	        	return false;
	        }
		}
		else {
			logHelper.writeWarningLog( i, "Claim type", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.INVOICE_NO) != null)
		{
			reimbursementDTO.setInvoiceNo(dataFormatter.formatCellValue(currentRow.getCell(Column.INVOICE_NO)));
		}
		
		if (currentRow.getCell(Column.ORIGINAL_CURRENCY).getCellTypeEnum() != CellType.BLANK)
		{
			reimbursementDTO.setOriginalCurrencyCode(dataFormatter.formatCellValue(currentRow.getCell(Column.ORIGINAL_CURRENCY)));
		}
		else {
			logHelper.writeWarningLog( i, "Original Currency ", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.EXCLUDE_GST_TOTAL_ORIGINAL_AMOUNT).getCellTypeEnum() != CellType.BLANK)
		{
			reimbursementDTO.setExcludeGstTotalOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.EXCLUDE_GST_TOTAL_ORIGINAL_AMOUNT).getNumericCellValue())));
		}
		else {
			logHelper.writeWarningLog( i, "Total Amount Excluded GST (Orignal)", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.FX_RATE).getCellTypeEnum() != CellType.BLANK)
		{
			reimbursementDTO.setFxRate(getBigDecimal(currentRow.getCell(Column.FX_RATE)));
		}
		else {
			logHelper.writeWarningLog( i, "Fx rate ", "required");
			return false;
		}
		
		if (currentRow.getCell(Column.GST_TYPE).getCellTypeEnum() != CellType.BLANK)
		{
			String gstType = dataFormatter.formatCellValue(currentRow.getCell(Column.GST_TYPE));
			Set<GstMaster> setGstMaster = gstMasterDAO.findGstMasterByName(gstType);
			if(!setGstMaster.isEmpty()) {
				reimbursementDTO.setGstType(gstType);
			}
			else {
				logHelper.writeWarningLog(i, "GST Type", gstType, NOT_FOUND_DB);
				return false;
			}
		}
		else {
			logHelper.writeWarningLog( i, "GST type", "required");
			return false;
		}
		
		reimbursementDTO.setGstRate(getBigDecimal(currentRow.getCell(Column.GST_RATE)));
		
		if (currentRow.getCell(Column.INCLUDE_GST_TOTAL_CONVERTED_AMOUNT).getCellTypeEnum() != CellType.BLANK)
		{
			reimbursementDTO.setIncludeGstTotalConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column.INCLUDE_GST_TOTAL_CONVERTED_AMOUNT).getNumericCellValue())));
		}
		else {
			logHelper.writeWarningLog( i, "Total Amount Included GST (Converted)", "required");
			return false;
		}
		
		//Set get receivable account /code
        String payableName = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_PAYABLE_NAME)).trim();
        String payableCode = dataFormatter.formatCellValue(currentRow.getCell(Column.ACCOUNT_PAYABLE_CODE)).trim();

    	if(StringUtils.isEmpty(payableCode)){
    		if(StringUtils.isEmpty(payableName)){
    			logHelper.writeWarningLog(i, "Account Payable Name or Account Payable Code", NOT_NULL_DATA);
    			return false;
    		} else {
    			Set<AccountMaster> payableSet = accountMasterDAO.findAccountMasterByName(payableName);
            	Iterator<AccountMaster> payableAccountMasterIter = payableSet.iterator();
            	
            	if(payableAccountMasterIter.hasNext()){
            		AccountMaster account = payableAccountMasterIter.next();
            		reimbursementDTO.setAccountPayableCode(account.getCode());
            		reimbursementDTO.setAccountPayableName(account.getName());
            	} else {
            		logHelper.writeWarningLog(i, "Account Payable Name", payableName, NOT_FOUND_DB);
            		return false;
            	}
    		}
    	} else {
    		AccountMaster payableAccountMaster = accountMasterDAO.findAccountMasterByCode(payableCode);
    		if(payableAccountMaster != null) {
    			if(StringUtils.isEmpty(payableName)){
    				reimbursementDTO.setAccountPayableCode(payableAccountMaster.getCode());
    				reimbursementDTO.setAccountPayableName(payableAccountMaster.getName());
        		} else {
        			if(payableAccountMaster.getName().equals(payableName)){
        				reimbursementDTO.setAccountPayableCode(payableAccountMaster.getCode());
        				reimbursementDTO.setAccountPayableName(payableAccountMaster.getName());
        			} else {
        				logHelper.writeErrorLog(i, "Account Payable Code", payableCode, "Account Payable Name", payableName);
            			return false;
        			}
        		}
    		} else {
    			logHelper.writeWarningLog(i, "Account Payable Code", payableCode, NOT_FOUND_DB);
                return false;
    		}
    	}
    	
		String scheduledPaymentDateAsStr = dataFormatter.formatCellValue(currentRow.getCell(Column.SCHEDULED_PAYMENT_DATE));
        if (hasText(scheduledPaymentDateAsStr)) {
            Calendar scheduledPaymentDate = CalendarUtil.getCalendar(scheduledPaymentDateAsStr);
            if (nonNull(scheduledPaymentDate)) {
            	reimbursementDTO.setScheduledPaymentDate(scheduledPaymentDate);
            } else {
                logHelper.writeWarningLog(i, "Scheduled Payment Date", dataFormatter.formatCellValue(currentRow.getCell(Column.SCHEDULED_PAYMENT_DATE)), WRONG_FORMAT_DATE);
                return false;
            }
        }
		
		if (currentRow.getCell(Column.DESCRIPTION_HEADER) != null)
		{
			reimbursementDTO.setDescription(currentRow.getCell(Column.DESCRIPTION_HEADER).getStringCellValue());
		}
		return true;
	}
	private String getImportKey(ReimbursementDTO p) {
		return String.join(" ", p.getEmployeeCode(), p.getEmployeeName(), CalendarUtil.toString(p.getBookingDate()), CalendarUtil.toString(p.getMonth()),
							    p.getClaimType(), p.getOriginalCurrencyCode(), p.getExcludeGstTotalOriginalAmount().toPlainString(), p.getFxRate().toPlainString(), p.getIncludeGstTotalConvertedAmount().toPlainString(),
							    p.getAccountPayableCode(), p.getAccountPayableName(), CalendarUtil.toString(p.getScheduledPaymentDate()), p.getDescription());
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.reimbursement", new Object[]{}, locale);
	}
	
	class Column {
		static final int EMPLOYEE_ID = 0;
		static final int EMPLOYEE_NAME = 1;
		static final int BOOKING_DATE = 2;
		static final int MONTH = 3;
		static final int CLAIM_TYPE = 4;
		static final int INVOICE_NO = 5;
		static final int ORIGINAL_CURRENCY = 6;
		static final int EXCLUDE_GST_TOTAL_ORIGINAL_AMOUNT = 7;
		static final int FX_RATE = 8;
		static final int GST_TYPE = 9;
		static final int GST_RATE = 10;
		static final int INCLUDE_GST_TOTAL_CONVERTED_AMOUNT = 11;
		static final int ACCOUNT_PAYABLE_CODE = 12;
		static final int ACCOUNT_PAYABLE_NAME = 13;
		static final int SCHEDULED_PAYMENT_DATE = 14;
		static final int DESCRIPTION_HEADER = 15;
		static final int ACCOUNT_CODE = 16;
		static final int ACCOUNT_NAME = 17;
		static final int PROJECT_CODE = 18;
		static final int PROJECT_NAME = 19;
		static final int APPROVAL_NO = 20;
		static final int DESCRIPTION_DETAIL = 21;
		static final int EXCLUDE_GST_ORIGINAL_AMOUNT  = 22;
		static final int INCLUDE_GST_CONVERTED_AMOUNT  = 23;
	}
}
