package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.AccountImportComponent.Column.CODE;
import static com.ttv.cashflow.component.PayeeImportComponent.Column.*;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.service.PayeePayerMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class PayeeImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("PAYEE");
    
    private DataFormatter df = new DataFormatter();
    
    @Autowired
    private PayeePayerMasterService payeePayerMasterService;
    
    @Autowired
    private PayeePayerMasterDAO payeePayerMasterDAO;
    
    @Autowired
	private PayeePayerTypeMasterDAO payeePayerTypeMasterDAO;
    
	@Autowired
	private AccountMasterDAO accountMasterDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
	    Sheet sheet = workbook.getSheetAt(0);
	    
	    for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {
                    PayeePayerMaster master = new PayeePayerMaster();
                    
                    String code = df.formatCellValue(row.getCell(CODE));
                    
                    //Ignore row invalid
                    if(StringUtils.isEmpty(code)) 
                    {
                        logHelper.writeWarningLog(i, "Code", "required");
                    	continue;
                    }
                    master.setCode(code);
                    
                    String typeName = df.formatCellValue(row.getCell(TYPE_NAME));
                    if(!StringUtils.isEmpty(typeName)) 
                    {
                    	Set<PayeePayerTypeMaster> setPayeePayerTypeMaster = payeePayerTypeMasterDAO.findPayeePayerTypeMasterByName(typeName);
                    	if(!setPayeePayerTypeMaster.isEmpty()) {
                    		Iterator<PayeePayerTypeMaster> iterator = setPayeePayerTypeMaster.iterator();
                    		master.setTypeCode(iterator.next().getCode());
                    	}
                    	else {
                    	    logHelper.writeWarningLog(i, "Type name", "not exists in master data");
                    	    nRowFail ++;
							continue;	
                    	}
                    }
                    
                    String accountCode = df.formatCellValue(row.getCell(ACCOUNT_CODE));
                    if(!StringUtils.isEmpty(accountCode)) 
                    {
                    	AccountMaster accountMaster = accountMasterDAO.findAccountMasterByPrimaryKey(accountCode);
                    	if(accountMaster != null) {
                    		master.setAccountCode(accountCode);
                    	}
                    	else {
                    	    logHelper.writeWarningLog(i, "Account code", "not exists in master data");
                    	    nRowFail ++;
							continue;	
                    	}
                    }
                    
                    master.setName(df.formatCellValue(row.getCell(NAME)));
                    master.setPaymentTerm(df.formatCellValue(row.getCell(PAYMENT_TERM)));
                    master.setBankName(df.formatCellValue(row.getCell(BANK_NAME)));
                    master.setBankAccountCode(df.formatCellValue(row.getCell(BANK_ACCOUNT_CODE)));
                    master.setAddress(df.formatCellValue(row.getCell(ADDRESS)));
                    master.setPhone(df.formatCellValue(row.getCell(PHONE)));
                    master.setDefaultCurrency(df.formatCellValue(row.getCell(DEFAULT_CURRENCY)));
                    payeePayerMasterService.savePayeePayerMaster(master);
                    nRowSuccess++;
                } catch (Exception e) {
                    e.printStackTrace();
                    nRowFail++;
                }
            }
	    }
	    
	    // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
        
        Sheet sheet = workbook.getSheetAt(0);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);

            try {
                
                String code = df.formatCellValue(row.getCell(CODE));
                
                //Ignore row invalid
                if(StringUtils.isEmpty(code)) continue;
                
                PayeePayerMaster master =  payeePayerMasterDAO.findPayeePayerMasterByPrimaryKey(code);

                if (null != master) {
                    payeePayerMasterService.deletePayeePayerMaster(master);
                    nRowSuccess++;
                } else {
                    logHelper.writeWarningLog(i, "Code", "not exist DB");
                    nRowFail++;
                }

            } catch (Exception e) {
                e.printStackTrace();
                nRowFail++;
            }
        }
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.payeeMaster", new Object[]{}, locale);
    }
	
	class Column {
	    static final int CODE = 0;
        static final int NAME = 1;
        static final int TYPE_NAME = 2;
        static final int PAYMENT_TERM = 3;
        static final int ACCOUNT_CODE = 4;
        static final int BANK_NAME = 5;
        static final int BANK_ACCOUNT_CODE = 6;
        static final int BANK_SWIFT_CODE = 7;
        static final int ADDRESS = 8;
        static final int PHONE = 9;
        static final int DEFAULT_CURRENCY = 10;
	}
	

}
