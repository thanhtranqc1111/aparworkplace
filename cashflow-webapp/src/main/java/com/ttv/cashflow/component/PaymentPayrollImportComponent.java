package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.ACCOUNT_PAYABLE_CODE;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.ACCOUNT_PAYABLE_NAME;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.BANK_ACCOUNT;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.BANK_NAME;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.BANK_REF_NO;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.CURRENCY;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.DESCRIPTION;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.PAYMENT_ORDER_APPROVAL_NO;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.PAYROLL_NO;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.TOTAL_NET_PAYMENT_AMOUNT;
import static com.ttv.cashflow.component.PaymentPayrollImportComponent.Column.VALUE_DATE;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.service.PayrollPaymentDetailsService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class PaymentPayrollImportComponent extends ImportComponent {
	
	private final static Logger logger = Logger.getLogger("PAYMENT PAYROLL");
	
	@Autowired
	private PaymentOrderService paymentService;
	
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;

	@Autowired
	private PayrollPaymentDetailsService payrollPaymentDetailsService;

	@Autowired
	private PaymentOrderDAO paymentDAO;
	
	@Autowired
	private PayrollDAO payrollDAO;

	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());

		int success = 0, unSuccess = 0;
		boolean error = false;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();
		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					// ap invoice no is required
					if (row.getCell(PAYROLL_NO).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Payroll No", "required");
						unSuccess++;
						nRow++;
						continue;
					}

					// check value date is not empty
					Calendar valueDate = null;
					if (row.getCell(VALUE_DATE).getCellTypeEnum() != CellType.BLANK) {
						if (row.getCell(VALUE_DATE).getCellTypeEnum() == CellType.NUMERIC) {
							// parse date to calendar
							valueDate = CalendarUtil.getCalendar(row.getCell(VALUE_DATE).getDateCellValue());
						} else {
						    logHelper.writeWarningLog(nRow, "Value Date", formatter.formatCellValue(row.getCell(VALUE_DATE)), "wrong format(ex: dd/mm/yyyy)");
							unSuccess++;
							nRow++; 
							continue;
						}
					} else {
					    logHelper.writeWarningLog(nRow, "Value Date", "required");
						unSuccess++;
						nRow++;
						continue;
					}

					// parse string to BigDecimal
					BigDecimal totalNetPaymentAmount = getNumberData(row.getCell(TOTAL_NET_PAYMENT_AMOUNT), nRow, "Total Net Payment Amount");
					if (null == totalNetPaymentAmount) {
						unSuccess++;
						nRow++;
						continue;
					}

					String bankName = formatter.formatCellValue(row.getCell(BANK_NAME));
					String bankAccount = formatter.formatCellValue(row.getCell(BANK_ACCOUNT));

					// check duplicate payment order
                    boolean hasExistingPayment = paymentService.checkDuplicatePaymentOrder(bankName, bankAccount, totalNetPaymentAmount, valueDate);
                    if (hasExistingPayment) {
						String payrollNoCell = formatter.formatCellValue(row.getCell(PAYROLL_NO));
						String[] payrollNoArr = payrollNoCell.split(",");
						for (String payrollNo : payrollNoArr) {
							Payroll payroll = payrollDAO.findPayrollObjByPayrollNo(payrollNo.trim());
							if(null == payroll){
								error = true;
								logHelper.writeWarningLog(nRow, "Payroll No", payrollNo, "not exist in DB");
								unSuccess++;
								nRow++;
								break;
							}
						}
						
						if(error){
							continue;
						}
						PaymentOrder payment = new PaymentOrder();
						// get payment order no by value date
						String paymentOrderNo = paymentDAO.getPaymentOrderNo(valueDate);
						payment.setPaymentOrderNo(paymentOrderNo);
						payment.setBankName(bankName);
						payment.setBankAccount(bankAccount);
						payment.setValueDate(valueDate);
						payment.setIncludeGstOriginalAmount(totalNetPaymentAmount);
						payment.setIncludeGstConvertedAmount(totalNetPaymentAmount);
						payment.setPaymentRate(BigDecimal.ONE);
						// Value date is null => payment was cancelled.
						payment.setStatus(valueDate == null ? Constant.PAY_CANCELLED : Constant.PAY_PROCESSING);
						payment.setOriginalCurrencyCode( formatter.formatCellValue(row.getCell(CURRENCY)));
						
						// remove first # character
						String bankRefNoCell = formatter.formatCellValue(row.getCell(BANK_REF_NO));
						payment.setBankRefNo(bankRefNoCell.replaceFirst("^#", ""));
						
						payment.setDescription(formatter.formatCellValue(row.getCell(DESCRIPTION)));
						payment.setPaymentApprovalNo(formatter.formatCellValue(row.getCell(PAYMENT_ORDER_APPROVAL_NO)));
						payment.setAccountPayableCode(formatter.formatCellValue(row.getCell(ACCOUNT_PAYABLE_CODE)));
						payment.setAccountPayableName(formatter.formatCellValue(row.getCell(ACCOUNT_PAYABLE_NAME)));
						payment.setCreatedDate(Calendar.getInstance());
						payment.setModifiedDate(Calendar.getInstance());
						payment.setPaymentType(Constant.PAYMENT_ORDER_PAYROLL);
						
						// save to database
						paymentService.savePaymentOrder(payment);
						success++;
						
						for (String payrollNo : payrollNoArr) {
							// find payroll by payroll no
							Payroll payroll = payrollDAO.findPayrollObjByPayrollNo(payrollNo.trim());
							
							// save (update) payroll payment detail
							savePayrollPaymentOrderDetail(payment.getId(), payroll.getId(), payrollNoArr.length);
						}
						
					} else {
					    logHelper.writeWarningExistLog(nRow, "Payment Order");
						unSuccess++;
						nRow++;
						continue;
					}
				} catch (Exception e) {
					unSuccess++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(success, unSuccess);
		logHelper.close();
		
		return getImportReturn(Constant.IMPORT_MODE.MERGE, success, unSuccess);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());

		int success = 0, unSuccess = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(PAYROLL_NO).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Payroll No", "required");
						unSuccess++;
						nRow++;
						continue;
					}
					
					String payrollNoCell = formatter.formatCellValue(row.getCell(PAYROLL_NO));
					String[] payrollNoArr = payrollNoCell.split(",");
					for (String payrollNo : payrollNoArr) {
						Payroll payroll = payrollDAO.findPayrollObjByPayrollNo(payrollNo.trim());
						if(null == payroll){
						    logHelper.writeWarningLog(nRow, "Payroll No", payrollNo, "not map Payment Order");
							unSuccess++;
							nRow++;
							break;
						} else {
							for (PayrollPaymentDetails payrollPaymentDetails : payroll.getPayrollPaymentDetailses()) {
								paymentService.deletePaymentOrder(payrollPaymentDetails.getPaymentOrder());
								success++;
							}
							
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			nRow++;
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(success, unSuccess);
		logHelper.close();
		
		return getImportReturn(Constant.IMPORT_MODE.DELETE, success, unSuccess);
	}
	
	/**
	 * save (update) payroll payment order detail
	 * @param paymentId
	 * @param payrollId
	 * @param payrollCounter
	 */
	private void savePayrollPaymentOrderDetail(BigInteger paymentId, BigInteger payrollId, int payrollCounter){
		// find payment order
		PaymentOrder paymentOrder = paymentOrderDAO.findPaymentOrderById(paymentId);
		// find payroll
		Payroll payroll = payrollDAO.findPayrollById(payrollId);
		
		//
		BigDecimal netPaymentAmount = BigDecimal.ZERO;
		
		if(payrollCounter > 1) { //payment order - ap invoice (1-n)
			netPaymentAmount = payroll.getTotalNetPaymentOriginal();
		} else {
			netPaymentAmount = paymentOrder.getIncludeGstConvertedAmount();
		}
		
		// set data payroll payment detail
		PayrollPaymentDetails payrollPaymentDetails = new PayrollPaymentDetails();
		payrollPaymentDetails.setPaymentOrder(paymentOrder);
		payrollPaymentDetails.setPayroll(payroll);
		payrollPaymentDetails.setNetPaymentAmountOriginal(netPaymentAmount);
		payrollPaymentDetails.setCreatedDate(payroll.getCreatedDate());
		payrollPaymentDetails.setModifiedDate(Calendar.getInstance());
		
		//save payroll payment detail
		payrollPaymentDetailsService.savePayrollPaymentDetails(payrollPaymentDetails);
		
	}
	
	/**
	 * get total amount
	 * @param cell
	 * @param nRow
	 * @return
	 */
	private BigDecimal getNumberData(Cell cell, int nRow, String column) {
		if (cell.getCellTypeEnum() != CellType.BLANK) {
			if(cell.getCellTypeEnum() == CellType.NUMERIC) {
				return BigDecimal.valueOf(cell.getNumericCellValue());
			} else {
			    logHelper.writeWarningLog(nRow, column, cell.getStringCellValue(), "number");
				return null;
			}
		}
		return BigDecimal.ZERO;
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.paymentOrder.import.payroll.payment", new Object[]{}, locale);
	}
	
	class Column {
		static final int PAYROLL_NO = 0;
		static final int BANK_NAME = 1;
		static final int BANK_ACCOUNT = 2;
		static final int CURRENCY = 3;
		static final int TOTAL_NET_PAYMENT_AMOUNT = 4;
		static final int VALUE_DATE = 5;
		static final int BANK_REF_NO = 6;
		static final int DESCRIPTION = 7;
		static final int PAYMENT_ORDER_APPROVAL_NO = 8;
		static final int ACCOUNT_PAYABLE_CODE = 9;
		static final int ACCOUNT_PAYABLE_NAME = 10;
	}

}
