package com.ttv.cashflow.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.dao.ProjectAllocationDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.dto.ProjectAllocationCollectorDTO;
import com.ttv.cashflow.service.ProjectAllocationService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;

/**
 * @author thoai.nh 
 * created date May 4, 2018
 */
@Component
@PrototypeScope
public class ProjectAllocationImportComponent extends ImportComponent {
	private static final Logger LOGGER = Logger.getLogger("PROJECT ALLOCATION");
	private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(ProjectAllocationImportComponent.class);
	private static final Double TOTAL_PERCENT = Double.valueOf(100);
	private DataFormatter dataFormatter = new DataFormatter();
	@Autowired
	private ProjectMasterDAO projectMasterDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private ProjectAllocationService projectAllocationService;
	@Autowired
	private ProjectAllocationDAO projectAllocationDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		// create log file
		logHelper.init(LOGGER, this.getLogPath());
		int nRowSuccess = 0;
		int nRecordFail = 0;
		int nRecordSuccess = 0;
		List<ProjectAllocation> listProjectAllocation = new ArrayList<>();
		nRecordFail = parseData(workbook, listProjectAllocation);
		/*
		 * validate and insert to DB
		 * create a map group total percent per employee and month
		 */
		try {
			Map<ProjectAllocationCollectorDTO, Double> mapGroupBy = listProjectAllocation.stream().collect(Collectors.groupingBy(p -> new ProjectAllocationCollectorDTO(p.getEmployeeMaster(), p.getMonth()), 
					  Collectors.summingDouble(ProjectAllocation::getPercentageDouble)));
			for (Map.Entry<ProjectAllocationCollectorDTO, Double> entry : mapGroupBy.entrySet())
			{
				//write log total percent must be 100%
				int compareRs = entry.getValue().compareTo(TOTAL_PERCENT);
				if(compareRs < 0) {
				logHelper.writeErrorLog("Total allocated of " + entry.getKey().getEmployeeMaster().getName() +" at " + CalendarUtil.toString(entry.getKey().getMonth(), "MM/yyyy") +" must be 100%");
				nRecordFail ++;
				}
				else if(compareRs > 0) {
					logHelper.writeErrorLog("Total allocated of " + entry.getKey().getEmployeeMaster().getName() +" at " + CalendarUtil.toString(entry.getKey().getMonth(), "MM/yyyy") +" exceeds 100%");
					nRecordFail ++;
				}
				else {
					Set<ProjectAllocation> existedData = projectAllocationDAO.findProjectAllocationByMonthAndEmployeeId(entry.getKey().getMonth(), entry.getKey().getEmployeeMaster().getId());
					for(ProjectAllocation projectAllocation: existedData) {
						//delete old date before insert
						projectAllocationService.deleteProjectAllocation(projectAllocation);
					}
				}
			}
			for(ProjectAllocation projectAllocation: listProjectAllocation) {
				ProjectAllocationCollectorDTO projectAllocationCollectorDTO = new ProjectAllocationCollectorDTO(projectAllocation.getEmployeeMaster(), projectAllocation.getMonth());
				//validate total percent must be 100%
				if(mapGroupBy.get(projectAllocationCollectorDTO).compareTo(TOTAL_PERCENT) == 0) {	
					projectAllocationService.saveProjectAllocation(projectAllocation);
					nRecordSuccess++;
					nRowSuccess++;
				}
			}
		} catch (Exception e) {
			APP_LOGGER.error("Import Project Management Error",e);
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	
	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		// create log file
		logHelper.init(LOGGER, this.getLogPath());
		int nRowSuccess = 0;
		int nRecordFail = 0;
		int nRecordSuccess = 0;
		List<ProjectAllocation> listProjectAllocation = new ArrayList<>();
		nRecordFail = parseData(workbook, listProjectAllocation);
		/*
		 * map group total percent per employee and month
		 */
		Map<ProjectAllocationCollectorDTO, Double> mapGroupBy = listProjectAllocation.stream().collect(Collectors.groupingBy(p -> new ProjectAllocationCollectorDTO(p.getEmployeeMaster(), p.getMonth()), 
																																  Collectors.summingDouble(ProjectAllocation::getPercentageDouble)));
		for (Map.Entry<ProjectAllocationCollectorDTO, Double> entry : mapGroupBy.entrySet())
		{
			//write log total percent must be 100%
			int compareRs = entry.getValue().compareTo(TOTAL_PERCENT);
		    if(compareRs < 0) {
		    	logHelper.writeErrorLog("Total allocated of " + entry.getKey().getEmployeeMaster().getName() +" at " + CalendarUtil.toString(entry.getKey().getMonth(), "MM/yyyy") +" must be 100%");
		    	nRecordFail ++;
		    }
		    else if(compareRs > 0) {
		    	logHelper.writeErrorLog("Total allocated of " + entry.getKey().getEmployeeMaster().getName() +" at " + CalendarUtil.toString(entry.getKey().getMonth(), "MM/yyyy") +" exceeds 100%");
		    	nRecordFail ++;
		    }
		    else {
		    	Set<ProjectAllocation> existedData = projectAllocationDAO.findProjectAllocationByMonthAndEmployeeId(entry.getKey().getMonth(), entry.getKey().getEmployeeMaster().getId());
		    	for(ProjectAllocation projectAllocation: existedData) {
		    		projectAllocationService.deleteProjectAllocation(projectAllocation);
		    		nRowSuccess++;
		    		nRecordSuccess++;
		    	}
		    }
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	

	/**
	 * @param workbook
	 * @param listProjectAllocation
	 * @return total record fail
	 */
	private int parseData(Workbook workbook, List<ProjectAllocation> listProjectAllocation) {
		int i = 0;
		int nRecordFail = 0;
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		ProjectAllocation projectAllocationHeader = null;
		Map<Integer, Calendar> mapMonthIndex = new HashMap<>();
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			if(i == 1) {
				Iterator<Cell> cellIterator = currentRow.iterator();
				int cellCount = 0;
				while (cellIterator.hasNext()) {
					cellCount++;
					Cell cell = cellIterator.next();
					if(cellCount > 4) {
						mapMonthIndex.put(cellCount, CalendarUtil.getCalendar(cell.getDateCellValue()));
					}
				}
			}
			else {
				try {
					Iterator<Cell> cellIterator = currentRow.iterator();
					if (cellIterator.hasNext()) {
						if (currentRow.getCell(Column.EMPLOYEE_CODE).getCellTypeEnum() == CellType.BLANK &&
							currentRow.getCell(Column.EMPLOYEE_NAME).getCellTypeEnum() == CellType.BLANK &&
							currentRow.getCell(Column.TEAM).getCellTypeEnum() == CellType.BLANK) {
							if(projectAllocationHeader != null) {
								/*
								 * detail row
								 */
								if (currentRow.getCell(Column.PROJECT).getCellTypeEnum() == CellType.BLANK)
								{
									logHelper.writeWarningLog( i, "Project name", "required");
									nRecordFail ++;
									continue;
								}
								else {
									String projectName = dataFormatter.formatCellValue(currentRow.getCell(Column.PROJECT)).trim();
									Set<ProjectMaster> setProjectMaster = projectMasterDAO.findProjectMasterByName(projectName);
									if(setProjectMaster.isEmpty())
									{
										logHelper.writeErrorLog(String.format("Row [%s], %s",i, "Project does not exist or invalid"));
										nRecordFail ++;
										continue;
									}
									else {
										Iterator<ProjectMaster> iteratorProjectMaster = setProjectMaster.iterator();
										projectAllocationHeader.setProjectMaster(iteratorProjectMaster.next());
									}
									//loop month percentage data
									int cellCount = 0;
									while (cellIterator.hasNext()) {
										cellCount++;
										Cell cell = cellIterator.next();
										if(cellCount > 4) {
											if(cell.getCellTypeEnum() != CellType.BLANK ) {
												if(cell.getCellTypeEnum() == CellType.NUMERIC) {
													BigDecimal percentage = NumberUtil.getBigDecimalScaleDown(2, String.valueOf(cell.getNumericCellValue()));
													projectAllocationHeader.setMonth(mapMonthIndex.get(cellCount));
													projectAllocationHeader.setPercentAllocation(percentage);
													ProjectAllocation projectAllocation = new ProjectAllocation();
													projectAllocation.copy(projectAllocationHeader);
													listProjectAllocation.add(projectAllocation);
												}
												else {
													logHelper.writeErrorLog(String.format("Row [%s], %s",i, "Project allocated must be numeric"));
													nRecordFail ++;
													continue;
												}
											}
										}
									}
								}
							}
						}
						else {
							/*
							 * header row 
							 */
							projectAllocationHeader = null;
							if (currentRow.getCell(Column.EMPLOYEE_NAME).getCellTypeEnum() == CellType.BLANK)
							{
								logHelper.writeWarningLog( i, "Employee name", "required");
								nRecordFail ++;
								continue;
							}
							
							if (currentRow.getCell(Column.TEAM).getCellTypeEnum() == CellType.BLANK)
							{
								logHelper.writeWarningLog( i, "Team", "required");
								nRecordFail ++;
								continue;
							}
							
							if (currentRow.getCell(Column.EMPLOYEE_CODE).getCellTypeEnum() != CellType.BLANK)
							{
								String employeeCode = dataFormatter.formatCellValue(currentRow.getCell(Column.EMPLOYEE_CODE));
								Set<EmployeeMaster> setEmployeeMaster = employeeMasterDAO.findEmployeeMasterByCode(employeeCode);
								if(setEmployeeMaster.isEmpty())
								{
									logHelper.writeErrorLog(String.format("Row [%s], %s",i, "Employee does not exist or invalid"));
									nRecordFail ++;
									continue;
								}
								else {
									projectAllocationHeader = new ProjectAllocation();
									Iterator<EmployeeMaster> iteratorEmployeeMaster = setEmployeeMaster.iterator();
									projectAllocationHeader.setEmployeeMaster(iteratorEmployeeMaster.next());
								}
							}
							else {
								logHelper.writeWarningLog( i, "Employee Code", "required");
								nRecordFail ++;
								continue;
							}
						}
					}
				} catch (Exception e) {
					nRecordFail++;
					throw e;
				}
			}
		}
		return nRecordFail;
	}
	
	@Override
	public String getName(){
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.projectAllocation", new Object[]{}, locale);
    }
	
	class Column {
		static final int EMPLOYEE_NAME = 0;
		static final int EMPLOYEE_CODE = 1;
		static final int TEAM = 2;
		static final int PROJECT = 3;
	}
}
