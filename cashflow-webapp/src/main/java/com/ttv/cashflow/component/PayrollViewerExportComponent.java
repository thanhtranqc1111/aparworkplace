package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.PayrollViewerExportComponent.Column.*;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.PayrollViewerDAO;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.PayrollViewer;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.PermissionCode;

@Component
@PrototypeScope
public class PayrollViewerExportComponent {
    
    @Autowired
    private PayrollViewerDAO payrollViewerDAO;
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
    
    @Autowired
    private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;
    
    public void doExport(Workbook workbook, List<String> payrollNoList) throws Exception{
        
        Sheet sheet1 = workbook.getSheetAt(0);
        Sheet sheet2 = workbook.getSheetAt(1);
        
        payrollViewerDAO.refresh();
        
        exportPayroll(workbook, sheet1, payrollNoList);
        
        if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_009.toString())){
            exportPayrollDetail(workbook, sheet2, payrollNoList);
        }
        
        
    }
    
    private void exportPayroll(Workbook workbook, Sheet sheet, List<String> payrollNoList){
        
        List<String> paymentOrderNoVisitedList = new ArrayList<>();
        
        int rowNum = 2;
        
        for (String payrollNo : payrollNoList) {
            
            Row row = sheet.createRow(rowNum);
            
            List<String> payrollNoRetList = new ArrayList<>();
            List<String> paymentOrderNoList = new ArrayList<>();
            
            // Get list payment order no following payroll no.
            Iterator<PayrollViewer> payrollViewerIter = payrollViewerDAO.findPayrollViewerByPayrollNo(payrollNo).iterator();
            
            while (payrollViewerIter.hasNext()) {
                PayrollViewer payrollViewer = payrollViewerIter.next();

                String payrollNoRet = payrollViewer.getPayrollNo();
                if (hasText(payrollNoRet) && !payrollNoRetList.contains(payrollNoRet)) {
                    payrollNoRetList.add(payrollViewer.getPayrollNo());
                }

                String paymentOrderNoRet = payrollViewer.getPaymentOrderNo();
                if (hasText(paymentOrderNoRet) && !paymentOrderNoList.contains(paymentOrderNoRet)) {
                    paymentOrderNoList.add(payrollViewer.getPaymentOrderNo());
                }
            }
            
            // CASE 1: ONE payroll - ZERO payment
            // Just write only payroll header
            if (paymentOrderNoList.isEmpty()) {

                Iterator<PayrollViewer> payrollViewerIter3 = payrollViewerDAO.findPayrollViewerByPayrollNo(payrollNo).iterator();
                PayrollViewer payrollViewer = payrollViewerIter3.next();

                writePayrollHeader(workbook, row, payrollViewer);

            } else if (paymentOrderNoList.size() == 1) {
                // When ONE payroll find ONE payment, 
                // we will check payroll following payment. 
                
                String paymentOrderNoRet = paymentOrderNoList.get(0);
                
                // Only check on payment not visit.
                if(paymentOrderNoVisitedList.contains(paymentOrderNoRet)){
                   continue; 
                }
                
                // Find payroll following payment.
                // Will be continue add payroll when find one more payroll. 
                Iterator<PayrollViewer> payrollViewerIter2 = payrollViewerDAO.findPayrollViewerByPaymentOrderNo(paymentOrderNoRet).iterator();
                paymentOrderNoVisitedList.add(paymentOrderNoRet);
                
                while (payrollViewerIter2.hasNext()) {
                    PayrollViewer payrollViewer = payrollViewerIter2.next();

                    String payrollNoRet = payrollViewer.getPayrollNo();
                    if (hasText(payrollNoRet) && !payrollNoRetList.contains(payrollNoRet)) {
                        payrollNoRetList.add(payrollViewer.getPayrollNo());
                    }
                }
                
                //CASE 2: ONE payroll - ONE payment
                //write payroll header and payment
                if (payrollNoRetList.size() == 1) {
                    
                    Iterator<PayrollViewer> payrollViewerIter3 = payrollViewerDAO.findPayrollViewerByPayrollNo(payrollNo).iterator();
                    PayrollViewer payrollViewer = payrollViewerIter3.next();

                    writePayrollHeader(workbook, row, payrollViewer);
                    
                    writePaymentOrder(workbook, row, payrollViewer);
                    
                //CASE 3: MANY payroll - ONE payment
                // write payroll as normal, on payment will be merge then write.
                } else if (payrollNoRetList.size() > 1) {
                    int count = 0; // count number payment have wrote.
                    boolean hasMerge = false;

                    for(String payrollNoRet : payrollNoRetList){
                        
                        Iterator<PayrollViewer> payrollViewerIter3 = payrollViewerDAO.findPayrollViewerByPayrollNo(payrollNoRet).iterator();
                        PayrollViewer payrollViewer = payrollViewerIter3.next();
                        
                        writePayrollHeader(workbook, row, payrollViewer);
                        
                        // Merge row on payment section.
                        if (!hasMerge) {
                            
                            int fistRow = rowNum;
                            int lastRow = rowNum + payrollNoRetList.size() - 1;
                                
                            mergeRowOnPayment(sheet, fistRow, lastRow);
                        
                            writePaymentOrder(workbook, row, payrollViewer);
                            
                            hasMerge = true;
                        }
                        
                        count++;
                        
                        // Add more row to write more payroll
                        if(count < payrollNoRetList.size()){
                            rowNum++;
                            row = sheet.createRow(rowNum);
                        }
                    }
                }
            
            //CASE 3: ONE payroll - MANY payment
            // Will be merge payroll section then write to, payment write as normal.
            } else if (paymentOrderNoList.size() > 1) {
                
                int count = 0; // count number payment have wrote.
                boolean hasMerge = false;
                
                for (String paymentOrderNo : paymentOrderNoList) {
                    
                    Iterator<PayrollViewer> payrollViewerIter3 = payrollViewerDAO.findPayrollViewerByPaymentOrderNo(paymentOrderNo).iterator();
                    PayrollViewer payrollViewer = payrollViewerIter3.next();
                    
                    if (!hasMerge) {
                        int fistRow = rowNum;
                        int lastRow = rowNum + paymentOrderNoList.size() - 1;
                            
                        mergeRowOnPayroll(sheet, fistRow, lastRow);
                        
                        writePayrollHeader(workbook, row, payrollViewer);
                        
                        hasMerge = true;
                    }
                    
                    writePaymentOrder(workbook, row, payrollViewer);
                
                    count++;
                    if(count < paymentOrderNoList.size()){
                        rowNum++;
                        row = sheet.createRow(rowNum);
                    }
                }
            }
            
            rowNum++;
        }

    }

    private void writePayrollHeader(Workbook workbook, Row row, PayrollViewer payrollViewer) {
        
        CreationHelper createHelper = workbook.getCreationHelper();
        DataFormat format = workbook.createDataFormat();
        
        //PAYROLL_NO
        CellStyle payrollNoStyle = workbook.createCellStyle();
        setVerticalAlignCenter(payrollNoStyle);
        
        Cell payrollNoCell = row.createCell(PAYROLL_NO);
        payrollNoCell.setCellValue(payrollViewer.getPayrollNo());
        payrollNoCell.setCellStyle(payrollNoStyle);

        //MONTH
        CellStyle monthStyle = workbook.createCellStyle();
        formatDate(monthStyle, Constant.MMYYYY, createHelper);
        setVerticalAlignCenter(monthStyle);

        Cell monthCell = row.createCell(MONTH);
        monthCell.setCellValue(payrollViewer.getMonth());
        monthCell.setCellStyle(monthStyle);

        //CLAIM_TYPE
        CellStyle claimStyle = workbook.createCellStyle();
        setVerticalAlignCenter(claimStyle);
        
        Cell claimCell = row.createCell(CLAIM_TYPE);
        claimCell.setCellValue(getClaimTypeName(payrollViewer.getClaimType()));
        claimCell.setCellStyle(claimStyle);

        // PaymentScheduleDate
        CellStyle paymentDateStyle = workbook.createCellStyle();
        formatDate(paymentDateStyle, Constant.DDMMYYYY, createHelper);
        setVerticalAlignCenter(paymentDateStyle);

        Cell paymentDateCell = row.createCell(SCHEDULED_PAYMENT_DATE);
        paymentDateCell.setCellValue(payrollViewer.getPaymentScheduleDate());
        paymentDateCell.setCellStyle(paymentDateStyle);

        //DESCRIPTION
        CellStyle descriptionStyle = workbook.createCellStyle();
        setVerticalAlignCenter(descriptionStyle);
        
        Cell descriptionCell = row.createCell(DESCRIPTION);
        descriptionCell.setCellValue(payrollViewer.getDescription());
        descriptionCell.setCellStyle(descriptionStyle);
        
        //Invoice No
        CellStyle invoiceNoStyle = workbook.createCellStyle();
        setVerticalAlignCenter(invoiceNoStyle);
        
        Cell invoiceNoCell = row.createCell(INVOICE_NO);
        invoiceNoCell.setCellValue(payrollViewer.getInvoiceNo());
        invoiceNoCell.setCellStyle(invoiceNoStyle);
        
        //Original Currency
        CellStyle originalCurrencyStyle = workbook.createCellStyle();
        setVerticalAlignCenter(originalCurrencyStyle);
        
        Cell originalCurrencyCell = row.createCell(ORIGINAL_CURRENCY);
        originalCurrencyCell.setCellValue(payrollViewer.getOriginalCurrencyCode());
        originalCurrencyCell.setCellStyle(originalCurrencyStyle);

        // Net Payment (Original)
        CellStyle totalNetPaymentOriginalStyle = workbook.createCellStyle();
        setVerticalAlignCenter(totalNetPaymentOriginalStyle);
        formatNumeric(totalNetPaymentOriginalStyle, format);

        Cell totalNetPaymentOriginalCell = row.createCell(TOTAL_NET_PAYMENT_ORIGINAL);
        totalNetPaymentOriginalCell.setCellValue(doubleValue(payrollViewer.getTotalNetPaymentOriginal()));
        totalNetPaymentOriginalCell.setCellStyle(totalNetPaymentOriginalStyle);

        // Fx Rate
        CellStyle fxRateStyle = workbook.createCellStyle();
        setVerticalAlignCenter(fxRateStyle);
        formatNumeric(fxRateStyle, format);

        Cell fxRateCell = row.createCell(PAYROLL_FX_RATE);
        fxRateCell.setCellValue(doubleValue(payrollViewer.getFxRate()));
        fxRateCell.setCellStyle(fxRateStyle);

        // Net Payment (Converted)
        CellStyle totalNetPaymentConvertedStyle = workbook.createCellStyle();
        setVerticalAlignCenter(totalNetPaymentConvertedStyle);
        formatNumeric(totalNetPaymentConvertedStyle, format);

        Cell totalNetPaymentConvertedCell = row.createCell(TOTAL_NET_PAYMENT_CONVERTED);
        totalNetPaymentConvertedCell.setCellValue(doubleValue(payrollViewer.getTotalNetPaymentConverted()));
        totalNetPaymentConvertedCell.setCellStyle(totalNetPaymentConvertedStyle);
    }
    
    private void writePaymentOrder(Workbook workbook, Row row, PayrollViewer payrollViewer) {
        
        CreationHelper createHelper = workbook.getCreationHelper();
        DataFormat format = workbook.createDataFormat();
        
        //PAYMENT_ORDER_NO
        CellStyle paymentOrderNoStyle = workbook.createCellStyle();
        setVerticalAlignCenter(paymentOrderNoStyle);
        
        Cell paymentOrderNoCell = row.createCell(PAYMENT_ORDER_NO);
        paymentOrderNoCell.setCellValue(payrollViewer.getPaymentOrderNo());
        paymentOrderNoCell.setCellStyle(paymentOrderNoStyle);
        
        // PaymentDate
        CellStyle paymentDateStyle = workbook.createCellStyle();
        formatDate(paymentDateStyle, Constant.DDMMYYYY, createHelper);
        setVerticalAlignCenter(paymentDateStyle);

        Cell paymentDateCell = row.createCell(PAYMENT_DATE);
        paymentDateCell.setCellValue(payrollViewer.getValueDate());
        paymentDateCell.setCellStyle(paymentDateStyle);
        
        //PAYMENT_AMOUNT_ORIGINAL
        CellStyle paymentAmountOriginalStyle = workbook.createCellStyle();
        setVerticalAlignCenter(paymentAmountOriginalStyle);
        formatNumeric(paymentAmountOriginalStyle, format);

        Cell paymentAmountOriginalCell = row.createCell(PAYMENT_AMOUNT_ORIGINAL);
        paymentAmountOriginalCell.setCellValue(doubleValue(payrollViewer.getPaymentAmountOriginal()));
        paymentAmountOriginalCell.setCellStyle(paymentAmountOriginalStyle);
        
        //PAYMENT_RATE
        CellStyle paymentRateStyle = workbook.createCellStyle();
        setVerticalAlignCenter(paymentRateStyle);
        formatNumeric(paymentRateStyle, format);

        Cell paymentRateCell = row.createCell(PAYMENT_RATE);
        paymentRateCell.setCellValue(doubleValue(payrollViewer.getPaymentRate()));
        paymentRateCell.setCellStyle(paymentRateStyle);
        
        //PAYMENT_AMOUNT_CONVERTED
        CellStyle paymentAmountConvertedStyle = workbook.createCellStyle();
        setVerticalAlignCenter(paymentAmountConvertedStyle);
        formatNumeric(paymentAmountConvertedStyle, format);

        Cell paymentAmountConvertedCell = row.createCell(PAYMENT_AMOUNT_CONVERTED);
        paymentAmountConvertedCell.setCellValue(doubleValue(payrollViewer.getPaymentAmountConverted()));
        paymentAmountConvertedCell.setCellStyle(paymentAmountConvertedStyle);
        
        //RemainAmount
        CellStyle paymentRemainAmountStyle = workbook.createCellStyle();
        setVerticalAlignCenter(paymentRemainAmountStyle);
        formatNumeric(paymentRemainAmountStyle, format);

        Cell paymentRemainAmountCell = row.createCell(PAYMENT_REMAIN_AMOUNT);
        paymentRemainAmountCell.setCellValue(doubleValue(payrollViewer.getPaymentRemainAmount()));
        paymentRemainAmountCell.setCellStyle(paymentRemainAmountStyle);
    }
    
    private void mergeRowOnPayment(Sheet sheet, int fistRow, int lastRow) {
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORDER_NO, PAYMENT_ORDER_NO));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_DATE, PAYMENT_DATE));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_AMOUNT_ORIGINAL, PAYMENT_AMOUNT_ORIGINAL));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_RATE, PAYMENT_RATE));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_AMOUNT_CONVERTED, PAYMENT_AMOUNT_CONVERTED));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_REMAIN_AMOUNT, PAYMENT_REMAIN_AMOUNT));
        
    }
    
    private void mergeRowOnPayroll(Sheet sheet, int fistRow, int lastRow) {
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYROLL_NO, PAYROLL_NO));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, MONTH, MONTH));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, CLAIM_TYPE, CLAIM_TYPE));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, SCHEDULED_PAYMENT_DATE, SCHEDULED_PAYMENT_DATE));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, DESCRIPTION, DESCRIPTION));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, INVOICE_NO, INVOICE_NO));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, ORIGINAL_CURRENCY, ORIGINAL_CURRENCY));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, TOTAL_NET_PAYMENT_ORIGINAL, TOTAL_NET_PAYMENT_ORIGINAL));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYROLL_FX_RATE, PAYROLL_FX_RATE));
        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, TOTAL_NET_PAYMENT_CONVERTED, TOTAL_NET_PAYMENT_CONVERTED));
    }
    
    private void exportPayrollDetail(Workbook workbook, Sheet sheet, List<String> listPayrollNo){
        
        int rowNum = 0;
        int rowNumSummary = 0;
        
        for (String payrollNo : listPayrollNo) {
            
            rowNumSummary = rowNum;
            
            //1. Set style for summary section.
            setStyleForSumary(workbook, sheet, rowNum);
            
            //2. Set title for summary section.
            rowNum = writeTitleForSummary(workbook, sheet, rowNum);
            
            rowNum++;

            //3. Set style for detail section.
            setStyleForDetail(workbook, sheet, rowNum);
            
            //4. Set title for detail section.
            rowNum = writeTitleForDetail(workbook, sheet, rowNum);
            
            //5. Get data and write it.
            Iterator<PayrollViewer> payrollViewerIter = payrollViewerDAO.findPayrollViewerByPayrollNo(payrollNo).iterator();
            
            boolean hasWriteHeader = false;
            
            while (payrollViewerIter.hasNext()) {
                PayrollViewer payrollViewer = payrollViewerIter.next();
                
                //5.1 Write data for summary only one.
                if(!hasWriteHeader){
                    
                    writeDataForSummary(workbook, sheet, rowNumSummary, payrollViewer);
                    
                    hasWriteHeader = true;
                }
                
                //5.2 Write data for detail
                rowNum = writeDataForDetail(workbook, sheet, rowNum, payrollViewer);
                
            }
            
            rowNum = rowNum + 4;
        }
        
    }

    private void setStyleForSumary(Workbook workbook, Sheet sheet, int rowNum) {
        CellStyle style = createStyleForSummaryTitle(workbook);
        
        for (int rowNo = rowNum; rowNo < rowNum + 5; rowNo++) {
            Row row = sheet.createRow(rowNo);

            for (int col = 0; col < 29; col++) {
                row.createCell(col).setCellStyle(style);
            }
            
        }
    }
    
    private void setStyleForDetail(Workbook workbook, Sheet sheet, int rowNum) {
        CellStyle style1 = createStyleForHeaderTitle(workbook, IndexedColors.GREEN);
        CellStyle style2 = createStyleForHeaderTitle(workbook, IndexedColors.DARK_YELLOW);
        CellStyle style3 = createStyleForHeaderTitle(workbook, IndexedColors.BLUE_GREY);
        
        for (int rowNo = rowNum; rowNo < rowNum + 2; rowNo++) {
            Row row = sheet.createRow(rowNo);
            
            for (int col = 0; col < 15; col++) {
                row.createCell(col).setCellStyle(style1);
            }
            
            for (int col = 15; col < 26; col++) {
                row.createCell(col).setCellStyle(style2);
            }
            
            for (int col = 26; col < 29; col++) {
                row.createCell(col).setCellStyle(style3);
            }
            
        }
    }


    private int writeTitleForSummary(Workbook workbook, Sheet sheet, int rowNum) {
        
        // + create new row
        Row row = sheet.getRow(rowNum);
        
        Cell titlePayrollNoCell = row.getCell(COL_0);
        titlePayrollNoCell.setCellValue(TITLE_PAYROLL_NO);
        
        //
        Cell titleTotalLaborCostCell = row.getCell(COL_3);
        titleTotalLaborCostCell.setCellValue(TITLE_ORIGINAL_CURRENCY);

        // + create new row
        row = sheet.getRow(++rowNum);
        
        Cell titleMonthCell = row.getCell(COL_0);
        titleMonthCell.setCellValue(TITLE_MONTH);
        
        Cell titleTotalDeductionCell = row.getCell(COL_3);
        titleTotalDeductionCell.setCellValue(TITLE_TOTAL_NET_PAYMENT);
        
        // + create new row
        row = sheet.getRow(++rowNum);
        
        Cell titleClaimTypeCell = row.getCell(COL_0);
        titleClaimTypeCell.setCellValue(TITLE_CLAIM_TYPE);

        // + create new row
        row = sheet.getRow(++rowNum);
        
        Cell titleScheduledPaymentDateCell = row.getCell(COL_0);
        titleScheduledPaymentDateCell.setCellValue(TITLE_SCHEDULED_PAYMENT_DATE);
        
        // + create new row
        row = sheet.getRow(++rowNum);
        
        Cell titleDescriptionCell = row.getCell(COL_0);
        titleDescriptionCell.setCellValue(TITLE_DESCRIPTION);
        
        return rowNum;
    }
    
    private int writeTitleForDetail(Workbook workbook, Sheet sheet, int rowNum) {
        
        // + create new row
        Row row = sheet.getRow(rowNum);

        //Labor Cost
        Font fontTitleFristRow = workbook.createFont();
        fontTitleFristRow.setColor(IndexedColors.WHITE.getIndex());
        fontTitleFristRow.setBold(true);
        
        CellStyle titleLaborCostStyle = workbook.createCellStyle();
        setFillForegroundColor(titleLaborCostStyle, IndexedColors.GREEN);
        titleLaborCostStyle.setAlignment(HorizontalAlignment.LEFT);
        setBorder(titleLaborCostStyle);
        titleLaborCostStyle.setFont(fontTitleFristRow);

        Cell titleLaborCostCell = row.getCell(COL_0);
        titleLaborCostCell.setCellValue(TITLE_LABOR_COST);
        titleLaborCostCell.setCellStyle(titleLaborCostStyle);
        
        //Deduction
        CellStyle titleDeductionStyle = workbook.createCellStyle();
        setFillForegroundColor(titleDeductionStyle, IndexedColors.DARK_YELLOW);
        titleDeductionStyle.setAlignment(HorizontalAlignment.LEFT);
        setBorder(titleDeductionStyle);
        titleDeductionStyle.setFont(fontTitleFristRow);
        
        Cell titleDeductionCell = row.getCell(COL_15);
        titleDeductionCell.setCellValue(TITLE_DEDUCTION);
        titleDeductionCell.setCellStyle(titleDeductionStyle);
        
        
        //Payment
        CellStyle titlePaymentStyle = workbook.createCellStyle();
        setFillForegroundColor(titlePaymentStyle, IndexedColors.BLUE_GREY);
        titlePaymentStyle.setAlignment(HorizontalAlignment.LEFT);
        setBorder(titlePaymentStyle);
        titlePaymentStyle.setFont(fontTitleFristRow);
        
        Cell titlePaymentCell = row.getCell(COL_26);
        titlePaymentCell.setCellValue(TITLE_PAYMENT);
        titlePaymentCell.setCellStyle(titlePaymentStyle);
        
        //
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 14));
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 15, 25));
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 26, 28));
        
        // + create new row
        row = sheet.getRow(++rowNum);
        Cell titleEmployeeIDCell = row.getCell(EMPLOYEE_ID);
        titleEmployeeIDCell.setCellValue(TITLE_EMPLOYEE_ID);
        
        Cell titleRoleTitleCell = row.getCell(ROLE_TITLE);
        titleRoleTitleCell.setCellValue(TITLE_ROLE_TITLE);
        
        Cell titleDivisionCell = row.getCell(DIVISION);
        titleDivisionCell.setCellValue(TITLE_DIVISION);
        
        Cell titleTeamCell = row.getCell(TEAM);
        titleTeamCell.setCellValue(TITLE_TEAM);
        
        Cell titleLaborBaseSalaryPaymentCell = row.getCell(LABOR_BASE_SALARY_PAYMENT);
        titleLaborBaseSalaryPaymentCell.setCellValue(TITLE_LABOR_BASE_SALARY_PAYMENT);
        
        Cell titleLaborAdditionalPayment1Cell = row.getCell(LABOR_ADDITIONAL_PAYMENT_1);
        titleLaborAdditionalPayment1Cell.setCellValue(TITLE_LABOR_ADDITIONAL_PAYMENT_1);
        
        Cell titleLaborAdditionalPayment2Cell = row.getCell(LABOR_ADDITIONAL_PAYMENT_2);
        titleLaborAdditionalPayment2Cell.setCellValue(TITLE_LABOR_ADDITIONAL_PAYMENT_2);
        
        Cell titleLaborAdditionalPayment3Cell = row.getCell(LABOR_ADDITIONAL_PAYMENT_3);
        titleLaborAdditionalPayment3Cell.setCellValue(TITLE_LABOR_ADDITIONAL_PAYMENT_3);
        
        Cell titleLaborSocialInsuranceOnEmployer1Cell = row.getCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        titleLaborSocialInsuranceOnEmployer1Cell.setCellValue(TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        
        Cell titleLaborSocialInsuranceOnEmployer2Cell = row.getCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        titleLaborSocialInsuranceOnEmployer2Cell.setCellValue(TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        
        Cell titleLaborSocialInsuranceOnEmployer3Cell = row.getCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        titleLaborSocialInsuranceOnEmployer3Cell.setCellValue(TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        
        Cell titleLaborOtherPayment1Cell = row.getCell(LABOR_OTHER_PAYMENT_1);
        titleLaborOtherPayment1Cell.setCellValue(TITLE_LABOR_OTHER_PAYMENT_1);
        
        Cell titleLaborOtherPayment2Cell = row.getCell(LABOR_OTHER_PAYMENT_2);
        titleLaborOtherPayment2Cell.setCellValue(TITLE_LABOR_OTHER_PAYMENT_2);
        
        Cell titleLaborOtherPayment3Cell = row.getCell(LABOR_OTHER_PAYMENT_3);
        titleLaborOtherPayment3Cell.setCellValue(TITLE_LABOR_OTHER_PAYMENT_3);
        
        Cell titleLaborCostTotalCell = row.getCell(LABOR_COST_TOTAL);
        titleLaborCostTotalCell.setCellValue(TITLE_LABOR_COST_TOTAL);
        
        /////////////
        Cell titleDeductionSocialInsuranceOnEmployer1Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        titleDeductionSocialInsuranceOnEmployer1Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        
        Cell titleDeductionSocialInsuranceOnEmployer2Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        titleDeductionSocialInsuranceOnEmployer2Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        
        Cell titleDeductionSocialInsuranceOnEmployer3Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        titleDeductionSocialInsuranceOnEmployer3Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        
        Cell titleDeductionSocialInsuranceOnEmployee1Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1);
        titleDeductionSocialInsuranceOnEmployee1Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1);
        
        Cell titleDeductionSocialInsuranceOnEmployee2Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2);
        titleDeductionSocialInsuranceOnEmployee2Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2);
        
        Cell titleDeductionSocialInsuranceOnEmployee3Cell = row.getCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3);
        titleDeductionSocialInsuranceOnEmployee3Cell.setCellValue(TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3);
        
        Cell titleDeductionWHTCell = row.getCell(DEDUCTION_WHT);
        titleDeductionWHTCell.setCellValue(TITLE_DEDUCTION_WHT);
        
        Cell titleDeductionOtherDeduction1Cell = row.getCell(DEDUCTION_OTHER_DEDUCTION_1);
        titleDeductionOtherDeduction1Cell.setCellValue(TITLE_DEDUCTION_OTHER_DEDUCTION_1);
        
        Cell titleDeductionOtherDeduction2Cell = row.getCell(DEDUCTION_OTHER_DEDUCTION_2);
        titleDeductionOtherDeduction2Cell.setCellValue(TITLE_DEDUCTION_OTHER_DEDUCTION_2);
        
        Cell titleDeductionOtherDeduction3Cell = row.getCell(DEDUCTION_OTHER_DEDUCTION_3);
        titleDeductionOtherDeduction3Cell.setCellValue(TITLE_DEDUCTION_OTHER_DEDUCTION_3);
        
        Cell titleDeductionTotalCell = row.getCell(DEDUCTION_TOTAL);
        titleDeductionTotalCell.setCellValue(TITLE_DEDUCTION_TOTAL);
        
        Cell titleNetPaymentOriginalCell = row.getCell(NET_PAYMENT_ORIGINAL);
        titleNetPaymentOriginalCell.setCellValue(TITLE_NET_PAYMENT_ORIGINAL);
        
        Cell fxRateCell = row.getCell(FX_RATE);
        fxRateCell.setCellValue(TITLE_FX_RATE);
        
        Cell titleNetPaymentConvertedCell = row.getCell(NET_PAYMENT_CONVERTED);
        titleNetPaymentConvertedCell.setCellValue(TITLE_NET_PAYMENT_CONVERTED);
        
        return rowNum;
    }
    
    private void writeDataForSummary(Workbook workbook, Sheet sheet, int rowNumSummary, PayrollViewer payrollViewer) {
        
        CreationHelper createHelper = workbook.getCreationHelper();
        DataFormat format = workbook.createDataFormat();
        
        // + create new row
        Row row = sheet.getRow(rowNumSummary);
        
        //SUM_PAYROLL_NO
        CellStyle payrollNoStyle = workbook.createCellStyle();
        setFillForegroundColor(payrollNoStyle, IndexedColors.GREY_25_PERCENT);
        
        Cell payrollNoCell = row.getCell(SUM_PAYROLL_NO);
        payrollNoCell.setCellValue(payrollViewer.getPayrollNo());
        payrollNoCell.setCellStyle(payrollNoStyle);
        
        //SUM_ORIGINAL_CURRENCY
        CellStyle originalCurrencyStyle = workbook.createCellStyle();
        setFillForegroundColor(originalCurrencyStyle, IndexedColors.GREY_25_PERCENT);
        setVerticalAlignCenter(originalCurrencyStyle);
        
        Cell originalCurrencyCell = row.getCell(SUM_ORIGINAL_CURRENCY);
        originalCurrencyCell.setCellValue(payrollViewer.getOriginalCurrencyCode());
        originalCurrencyCell.setCellStyle(originalCurrencyStyle);
        
        // + create new row
        row = sheet.getRow(++rowNumSummary);

        //SUM_MONTH
        CellStyle monthStyle = workbook.createCellStyle();
        setFillForegroundColor(monthStyle, IndexedColors.GREY_25_PERCENT);
        formatDate(monthStyle, Constant.MMYYYY, createHelper);
        setVerticalAlignCenter(monthStyle);
        
        Cell monthCell = row.getCell(SUM_MONTH);
        monthCell.setCellValue(payrollViewer.getMonth());
        monthCell.setCellStyle(monthStyle);
        
        //SUM_TOTAL_NET_PAYMENT
        CellStyle TotalNetPaymentStyle = workbook.createCellStyle();
        setFillForegroundColor(TotalNetPaymentStyle, IndexedColors.GREY_25_PERCENT);
        setVerticalAlignCenter(TotalNetPaymentStyle);
        formatNumeric(TotalNetPaymentStyle, format);

        Cell TotalNetPaymentCell = row.getCell(SUM_TOTAL_NET_PAYMENT);
        TotalNetPaymentCell.setCellValue(doubleValue(payrollViewer.getTotalNetPaymentConverted()));
        TotalNetPaymentCell.setCellStyle(TotalNetPaymentStyle);
        
        // + create new row
        row = sheet.getRow(++rowNumSummary);
        
        //SUM_CLAIM_TYPE
        CellStyle claimTypeStyle = workbook.createCellStyle();
        setFillForegroundColor(claimTypeStyle, IndexedColors.GREY_25_PERCENT);
        
        Cell claimTypeCell = row.getCell(SUM_CLAIM_TYPE);
        claimTypeCell.setCellValue(getClaimTypeName(payrollViewer.getClaimType()));
        claimTypeCell.setCellStyle(claimTypeStyle);
        

        // + create new row
        row = sheet.getRow(++rowNumSummary);
        
        //SUM_PAYMENT_SCHEDULE_DATE
        CellStyle paymentScheduleDateStyle = workbook.createCellStyle();
        setFillForegroundColor(paymentScheduleDateStyle, IndexedColors.GREY_25_PERCENT);
        formatDate(paymentScheduleDateStyle, Constant.DDMMYYYY, createHelper);
        setVerticalAlignCenter(paymentScheduleDateStyle);

        Cell paymentScheduleDateCell = row.getCell(SUM_PAYMENT_SCHEDULE_DATE);
        paymentScheduleDateCell.setCellValue(payrollViewer.getPaymentScheduleDate());
        paymentScheduleDateCell.setCellStyle(paymentScheduleDateStyle);
        
        // + create new row
        row = sheet.getRow(++rowNumSummary);
        
        //SUM_DESCRIPTION
        CellStyle descriptionStyle = workbook.createCellStyle();
        setFillForegroundColor(descriptionStyle, IndexedColors.GREY_25_PERCENT);
        
        Cell descriptionCell = row.getCell(SUM_DESCRIPTION);
        descriptionCell.setCellValue(payrollViewer.getDescription());
        descriptionCell.setCellStyle(descriptionStyle);
    }

    private int writeDataForDetail(Workbook workbook, Sheet sheet, int rowNum, PayrollViewer payrollViewer) {
        DataFormat format = workbook.createDataFormat();

        CellStyle borderStyle = workbook.createCellStyle();
        borderStyle.setBorderTop(BorderStyle.THIN);
        borderStyle.setBorderBottom(BorderStyle.THIN);
        borderStyle.setBorderLeft(BorderStyle.THIN);
        borderStyle.setBorderRight(BorderStyle.THIN);
        
        // + create new row
        Row row = sheet.createRow(++rowNum);

        //EMPLOYEE_ID
        Cell employeeIDCell = row.createCell(EMPLOYEE_ID);
        employeeIDCell.setCellValue(payrollViewer.getEmployeeCode());
        employeeIDCell.setCellStyle(borderStyle);
        
        //ROLE_TITLE
        Cell roleTitleCell = row.createCell(ROLE_TITLE);
        roleTitleCell.setCellValue(payrollViewer.getRoleTitle());
        roleTitleCell.setCellStyle(borderStyle);
        
        //DIVISION
        Cell divisionCell = row.createCell(DIVISION);
        divisionCell.setCellValue(payrollViewer.getDivision());
        divisionCell.setCellStyle(borderStyle);
        
        //TEAM
        Cell teamCell = row.createCell(TEAM);
        teamCell.setCellValue(payrollViewer.getTeam());
        teamCell.setCellStyle(borderStyle);
        
        //LABOR_BASE_SALARY_PAYMENT
        CellStyle laborBaseSalaryPaymentStyle = workbook.createCellStyle();
        setBorder(laborBaseSalaryPaymentStyle);
        setVerticalAlignCenter(laborBaseSalaryPaymentStyle);
        formatNumeric(laborBaseSalaryPaymentStyle, format);

        Cell laborBaseSalaryPaymentCell = row.createCell(LABOR_BASE_SALARY_PAYMENT);
        laborBaseSalaryPaymentCell.setCellValue(doubleValue(payrollViewer.getLaborBaseSalaryPayment()));
        laborBaseSalaryPaymentCell.setCellStyle(laborBaseSalaryPaymentStyle);
        
        //LABOR_ADDITIONAL_PAYMENT_1
        CellStyle laborAdditionalPayment1Style = workbook.createCellStyle();
        setBorder(laborAdditionalPayment1Style);
        setVerticalAlignCenter(laborAdditionalPayment1Style);
        formatNumeric(laborAdditionalPayment1Style, format);

        Cell laborAdditionalPayment1Cell = row.createCell(LABOR_ADDITIONAL_PAYMENT_1);
        laborAdditionalPayment1Cell.setCellValue(doubleValue(payrollViewer.getLaborAdditionalPayment1()));
        laborAdditionalPayment1Cell.setCellStyle(laborAdditionalPayment1Style);
        
        //LABOR_ADDITIONAL_PAYMENT_2
        CellStyle laborAdditionalPayment2Style = workbook.createCellStyle();
        setBorder(laborAdditionalPayment2Style);
        setVerticalAlignCenter(laborAdditionalPayment2Style);
        formatNumeric(laborAdditionalPayment2Style, format);

        Cell laborAdditionalPayment2Cell = row.createCell(LABOR_ADDITIONAL_PAYMENT_2);
        laborAdditionalPayment2Cell.setCellValue(doubleValue(payrollViewer.getLaborAdditionalPayment2()));
        laborAdditionalPayment2Cell.setCellStyle(laborAdditionalPayment2Style);
        
        //LABOR_ADDITIONAL_PAYMENT_3
        CellStyle laborAdditionalPayment3Style = workbook.createCellStyle();
        setBorder(laborAdditionalPayment3Style);
        setVerticalAlignCenter(laborAdditionalPayment3Style);
        formatNumeric(laborAdditionalPayment3Style, format);

        Cell laborAdditionalPayment3Cell = row.createCell(LABOR_ADDITIONAL_PAYMENT_3);
        laborAdditionalPayment3Cell.setCellValue(doubleValue(payrollViewer.getLaborAdditionalPayment3()));
        laborAdditionalPayment3Cell.setCellStyle(laborAdditionalPayment3Style);
        
        //LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1
        CellStyle laborSocialInsuranceEmployer1Style = workbook.createCellStyle();
        setBorder(laborSocialInsuranceEmployer1Style);
        setVerticalAlignCenter(laborSocialInsuranceEmployer1Style);
        formatNumeric(laborSocialInsuranceEmployer1Style, format);

        Cell laborSocialInsuranceEmployer1Cell = row.createCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        laborSocialInsuranceEmployer1Cell.setCellValue(doubleValue(payrollViewer.getLaborSocialInsuranceEmployer1()));
        laborSocialInsuranceEmployer1Cell.setCellStyle(laborSocialInsuranceEmployer1Style);
        
        //LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2
        CellStyle laborSocialInsuranceEmployer2Style = workbook.createCellStyle();
        setBorder(laborSocialInsuranceEmployer2Style);
        setVerticalAlignCenter(laborSocialInsuranceEmployer2Style);
        formatNumeric(laborSocialInsuranceEmployer2Style, format);

        Cell laborSocialInsuranceEmployer2Cell = row.createCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        laborSocialInsuranceEmployer2Cell.setCellValue(doubleValue(payrollViewer.getLaborSocialInsuranceEmployer1()));
        laborSocialInsuranceEmployer2Cell.setCellStyle(laborSocialInsuranceEmployer2Style);
        
        //LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3
        CellStyle laborSocialInsuranceEmployer3Style = workbook.createCellStyle();
        setBorder(laborSocialInsuranceEmployer3Style);
        setVerticalAlignCenter(laborSocialInsuranceEmployer3Style);
        formatNumeric(laborSocialInsuranceEmployer3Style, format);

        Cell laborSocialInsuranceEmployer3Cell = row.createCell(LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        laborSocialInsuranceEmployer3Cell.setCellValue(doubleValue(payrollViewer.getLaborSocialInsuranceEmployer1()));
        laborSocialInsuranceEmployer3Cell.setCellStyle(laborSocialInsuranceEmployer3Style);
        
        //LABOR_OTHER_PAYMENT_1
        CellStyle laborOtherPayment1Style = workbook.createCellStyle();
        setBorder(laborOtherPayment1Style);
        setVerticalAlignCenter(laborOtherPayment1Style);
        formatNumeric(laborOtherPayment1Style, format);

        Cell laborOtherPayment1Cell = row.createCell(LABOR_OTHER_PAYMENT_1);
        laborOtherPayment1Cell.setCellValue(doubleValue(payrollViewer.getLaborOtherPayment1()));
        laborOtherPayment1Cell.setCellStyle(laborOtherPayment1Style);
        
        //LABOR_OTHER_PAYMENT_2
        CellStyle laborOtherPayment2Style = workbook.createCellStyle();
        setBorder(laborOtherPayment2Style);
        setVerticalAlignCenter(laborOtherPayment2Style);
        formatNumeric(laborOtherPayment2Style, format);

        Cell laborOtherPayment2Cell = row.createCell(LABOR_OTHER_PAYMENT_2);
        laborOtherPayment2Cell.setCellValue(doubleValue(payrollViewer.getLaborOtherPayment2()));
        laborOtherPayment2Cell.setCellStyle(laborOtherPayment2Style);
        
        //LABOR_OTHER_PAYMENT_3
        CellStyle laborOtherPayment3Style = workbook.createCellStyle();
        setBorder(laborOtherPayment3Style);
        setVerticalAlignCenter(laborOtherPayment3Style);
        formatNumeric(laborOtherPayment3Style, format);

        Cell laborOtherPayment3Cell = row.createCell(LABOR_OTHER_PAYMENT_3);
        laborOtherPayment3Cell.setCellValue(doubleValue(payrollViewer.getLaborOtherPayment1()));
        laborOtherPayment3Cell.setCellStyle(laborOtherPayment3Style);
        
        //TOTAL_LABOR_COST
        CellStyle laborCostTotalStyle = workbook.createCellStyle();
        setBorder(laborCostTotalStyle);
        setVerticalAlignCenter(laborCostTotalStyle);
        formatNumeric(laborCostTotalStyle, format);

        Cell laborCostTotalCell = row.createCell(LABOR_COST_TOTAL);
        laborCostTotalCell.setCellValue(doubleValue(payrollViewer.getTotalLaborCost()));
        laborCostTotalCell.setCellStyle(laborCostTotalStyle);

        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1
        CellStyle deductionSocialInsuranceOnEmployer1Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployer1Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployer1Style);
        formatNumeric(deductionSocialInsuranceOnEmployer1Style, format);

        Cell deductionSocialInsuranceOnEmployer1Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1);
        deductionSocialInsuranceOnEmployer1Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployer1()));
        deductionSocialInsuranceOnEmployer1Cell.setCellStyle(deductionSocialInsuranceOnEmployer1Style);
        
        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2
        CellStyle deductionSocialInsuranceOnEmployer2Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployer2Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployer2Style);
        formatNumeric(deductionSocialInsuranceOnEmployer2Style, format);

        Cell deductionSocialInsuranceOnEmployer2Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2);
        deductionSocialInsuranceOnEmployer2Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployer2()));
        deductionSocialInsuranceOnEmployer2Cell.setCellStyle(deductionSocialInsuranceOnEmployer2Style);
        
        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3
        CellStyle deductionSocialInsuranceOnEmployer3Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployer3Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployer3Style);
        formatNumeric(deductionSocialInsuranceOnEmployer3Style, format);

        Cell deductionSocialInsuranceOnEmployer3Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3);
        deductionSocialInsuranceOnEmployer3Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployer3()));
        deductionSocialInsuranceOnEmployer3Cell.setCellStyle(deductionSocialInsuranceOnEmployer3Style);
        
        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1
        CellStyle deductionSocialInsuranceOnEmployee1Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployee1Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployee1Style);
        formatNumeric(deductionSocialInsuranceOnEmployee1Style, format);

        Cell deductionSocialInsuranceOnEmployee1Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1);
        deductionSocialInsuranceOnEmployee1Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployee1()));
        deductionSocialInsuranceOnEmployee1Cell.setCellStyle(deductionSocialInsuranceOnEmployee1Style);
        
        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2
        CellStyle deductionSocialInsuranceOnEmployee2Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployee2Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployee2Style);
        formatNumeric(deductionSocialInsuranceOnEmployee2Style, format);

        Cell deductionSocialInsuranceOnEmployee2Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2);
        deductionSocialInsuranceOnEmployee2Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployee2()));
        deductionSocialInsuranceOnEmployee2Cell.setCellStyle(deductionSocialInsuranceOnEmployee2Style);
        
        //DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3
        CellStyle deductionSocialInsuranceOnEmployee3Style = workbook.createCellStyle();
        setBorder(deductionSocialInsuranceOnEmployee3Style);
        setVerticalAlignCenter(deductionSocialInsuranceOnEmployee3Style);
        formatNumeric(deductionSocialInsuranceOnEmployee3Style, format);

        Cell deductionSocialInsuranceOnEmployee3Cell = row.createCell(DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3);
        deductionSocialInsuranceOnEmployee3Cell.setCellValue(doubleValue(payrollViewer.getDeductionSocialInsuranceEmployee3()));
        deductionSocialInsuranceOnEmployee3Cell.setCellStyle(deductionSocialInsuranceOnEmployee3Style);
        
        //DEDUCTION_WHT
        CellStyle deductionWHTStyle = workbook.createCellStyle();
        setBorder(deductionWHTStyle);
        setVerticalAlignCenter(deductionWHTStyle);
        formatNumeric(deductionWHTStyle, format);

        Cell deductionWHTSCell = row.createCell(DEDUCTION_WHT);
        deductionWHTSCell.setCellValue(doubleValue(payrollViewer.getDeductionWht()));
        deductionWHTSCell.setCellStyle(deductionWHTStyle);
        
        //DEDUCTION_OTHER_DEDUCTION_1
        CellStyle deductionOtherDeduction1Style = workbook.createCellStyle();
        setBorder(deductionOtherDeduction1Style);
        setVerticalAlignCenter(deductionOtherDeduction1Style);
        formatNumeric(deductionOtherDeduction1Style, format);

        Cell deductionOtherDeduction1Cell = row.createCell(DEDUCTION_OTHER_DEDUCTION_1);
        deductionOtherDeduction1Cell.setCellValue(doubleValue(payrollViewer.getDeductionOther1()));
        deductionOtherDeduction1Cell.setCellStyle(deductionOtherDeduction1Style);
        
        //DEDUCTION_OTHER_DEDUCTION_2
        CellStyle deductionOtherDeduction2Style = workbook.createCellStyle();
        setBorder(deductionOtherDeduction2Style);
        setVerticalAlignCenter(deductionOtherDeduction2Style);
        formatNumeric(deductionOtherDeduction2Style, format);

        Cell deductionOtherDeduction2Cell = row.createCell(DEDUCTION_OTHER_DEDUCTION_2);
        deductionOtherDeduction2Cell.setCellValue(doubleValue(payrollViewer.getDeductionOther2()));
        deductionOtherDeduction2Cell.setCellStyle(deductionOtherDeduction2Style);
        
        //DEDUCTION_OTHER_DEDUCTION_3
        CellStyle deductionOtherDeduction3Style = workbook.createCellStyle();
        setBorder(deductionOtherDeduction3Style);
        setVerticalAlignCenter(deductionOtherDeduction3Style);
        formatNumeric(deductionOtherDeduction3Style, format);

        Cell deductionOtherDeduction3Cell = row.createCell(DEDUCTION_OTHER_DEDUCTION_3);
        deductionOtherDeduction3Cell.setCellValue(doubleValue(payrollViewer.getDeductionOther3()));
        deductionOtherDeduction3Cell.setCellStyle(deductionOtherDeduction3Style);

        //DEDUCTION_TOTAL
        CellStyle deductionTotalStyle = workbook.createCellStyle();
        setBorder(deductionTotalStyle);
        setVerticalAlignCenter(deductionTotalStyle);
        formatNumeric(deductionTotalStyle, format);

        Cell deductionTotalCell = row.createCell(DEDUCTION_TOTAL);
        deductionTotalCell.setCellValue(doubleValue(payrollViewer.getTotalDeduction()));
        deductionTotalCell.setCellStyle(deductionTotalStyle);
        
        //NET_PAYMENT (Original)
        CellStyle netPaymentOriginalStyle = workbook.createCellStyle();
        setBorder(netPaymentOriginalStyle);
        setVerticalAlignCenter(netPaymentOriginalStyle);
        formatNumeric(netPaymentOriginalStyle, format);

        Cell netPaymentOriginalCell = row.createCell(NET_PAYMENT_ORIGINAL);
        netPaymentOriginalCell.setCellValue(doubleValue(payrollViewer.getNetPaymentOriginal()));
        netPaymentOriginalCell.setCellStyle(netPaymentOriginalStyle);
        
        //Fx Rate detail = Fx Rate
        CellStyle fxRateStyle = workbook.createCellStyle();
        setBorder(fxRateStyle);
        setVerticalAlignCenter(fxRateStyle);
        formatNumeric(fxRateStyle, format);

        Cell fxRateCell = row.createCell(FX_RATE);
        fxRateCell.setCellValue(doubleValue(payrollViewer.getFxRate()));
        fxRateCell.setCellStyle(fxRateStyle);
        
        //Net payment (Converted)
        CellStyle netPaymentConvertedStyle = workbook.createCellStyle();
        setBorder(netPaymentConvertedStyle);
        setVerticalAlignCenter(netPaymentConvertedStyle);
        formatNumeric(netPaymentConvertedStyle, format);

        Cell netPaymentConvertedCell = row.createCell(NET_PAYMENT_CONVERTED);
        netPaymentConvertedCell.setCellValue(doubleValue(payrollViewer.getNetPaymentConverted()));
        netPaymentConvertedCell.setCellStyle(netPaymentConvertedStyle);
        
        return rowNum;
    }
    
    private Double doubleValue(BigDecimal val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    private void setVerticalAlignCenter(CellStyle cellStyle){
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    }
    
    private CellStyle createStyleForSummaryTitle(Workbook workbook) {
        Font font = workbook.createFont();
        font.setBold(true);
        
        CellStyle style = workbook.createCellStyle();
        setFillForegroundColor(style, IndexedColors.GREY_25_PERCENT);
        style.setFont(font);
        
        return style;
    }
    
    private CellStyle createStyleForHeaderTitle(Workbook workbook, IndexedColors indexColor) {
        CellStyle stytle = workbook.createCellStyle();
        
        Font font = workbook.createFont();
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setBold(true);
        
        setFillForegroundColor(stytle, indexColor);
        stytle.setAlignment(HorizontalAlignment.CENTER);
        stytle.setVerticalAlignment(VerticalAlignment.CENTER);
        setBorder(stytle);
        stytle.setWrapText(true);
        stytle.setFont(font);
        
        return stytle;
    }
    
    /**
     * Set border for cell.
     */
    private void setBorder(CellStyle style){
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
    }
    
    private void formatDate(CellStyle cellStyle,  String format, CreationHelper createHelper){
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
    }
    
    /**
     * Set format number.
     * <p>Example:
     * <ul>
     * <li>3456.70  -> 3,456.70
     * <li>3456.789 -> 3,456.78
     * </ul>
     * <p>
     * 
     */
    private void formatNumeric(CellStyle cellStyle, DataFormat format){
        cellStyle.setDataFormat(format.getFormat("#,##0.00"));
    }
    
    private void setFillForegroundColor(CellStyle payrollNoStyle, IndexedColors index) {
        payrollNoStyle.setFillForegroundColor(index.getIndex());
        payrollNoStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }
    
    private String getClaimTypeName(String code){
        ClaimTypeMaster cliamType = claimTypeMasterDAO.findClaimTypeMasterByCode(code);
        return cliamType.getName();
        
    }
	
    class Column {

        // there are column for sheet 1 (payroll)
        static final int PAYROLL_NO = 0;
        static final int MONTH = 1;
        static final int CLAIM_TYPE = 2;
        static final int SCHEDULED_PAYMENT_DATE = 3;
        static final int DESCRIPTION = 4;
        static final int INVOICE_NO = 5;
        static final int ORIGINAL_CURRENCY = 6;
        static final int TOTAL_NET_PAYMENT_ORIGINAL = 7;
        static final int PAYROLL_FX_RATE = 8;
        static final int TOTAL_NET_PAYMENT_CONVERTED = 9;
        
        static final int PAYMENT_ORDER_NO = 10;
        static final int PAYMENT_DATE = 11;
        static final int PAYMENT_AMOUNT_ORIGINAL = 12;
        static final int PAYMENT_RATE = 13;
        static final int PAYMENT_AMOUNT_CONVERTED = 14;
        static final int PAYMENT_REMAIN_AMOUNT = 15;

        
        // there are column for sheet 2 (payroll detail)
        static final int EMPLOYEE_ID = 0;
        static final int ROLE_TITLE = 1;
        static final int DIVISION = 2;
        static final int TEAM = 3;
        static final int LABOR_BASE_SALARY_PAYMENT = 4;
        static final int LABOR_ADDITIONAL_PAYMENT_1 = 5;
        static final int LABOR_ADDITIONAL_PAYMENT_2 = 6;
        static final int LABOR_ADDITIONAL_PAYMENT_3 = 7;
        static final int LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1 = 8;
        static final int LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2 = 9;
        static final int LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3 = 10;
        static final int LABOR_OTHER_PAYMENT_1 = 11;
        static final int LABOR_OTHER_PAYMENT_2 = 12;
        static final int LABOR_OTHER_PAYMENT_3 = 13;
        static final int LABOR_COST_TOTAL = 14;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1 = 15;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2 = 16;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3 = 17;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1 = 18;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2 = 19;
        static final int DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3 = 20;
        static final int DEDUCTION_WHT = 21;
        static final int DEDUCTION_OTHER_DEDUCTION_1 = 22;
        static final int DEDUCTION_OTHER_DEDUCTION_2 = 23;
        static final int DEDUCTION_OTHER_DEDUCTION_3 = 24;
        static final int DEDUCTION_TOTAL = 25;
        static final int NET_PAYMENT_ORIGINAL = 26;
        static final int FX_RATE = 27;
        static final int NET_PAYMENT_CONVERTED = 28;
        
        //Summary column
        static final int COL_0 = 0;
        static final int SUM_PAYROLL_NO = 1;
        static final int SUM_MONTH = 1;
        static final int SUM_CLAIM_TYPE = 1;
        static final int SUM_PAYMENT_SCHEDULE_DATE = 1;
        static final int SUM_DESCRIPTION = 1;
        static final int COL_3 = 3;
        static final int SUM_ORIGINAL_CURRENCY = 4;
        static final int SUM_TOTAL_NET_PAYMENT = 4;
        static final int COL_15 = 15;
        static final int COL_26 = 26;

    }
    
    private static final String TITLE_PAYROLL_NO = "Payroll No";
    private static final String TITLE_MONTH = "Month";
    private static final String TITLE_CLAIM_TYPE = "Claim Type";
    private static final String TITLE_SCHEDULED_PAYMENT_DATE = "Scheduled Payment Date";
    private static final String TITLE_DESCRIPTION = "Description";
    private static final String TITLE_ORIGINAL_CURRENCY = "Original Currency";
    private static final String TITLE_TOTAL_NET_PAYMENT = "Total Net Payment";
    
    private static final String TITLE_LABOR_COST = "Labor Cost";
    private static final String TITLE_DEDUCTION = "Deduction";
    private static final String TITLE_PAYMENT = "Payment";
    
    private static final String TITLE_EMPLOYEE_ID = "Employee ID";
    private static final String TITLE_ROLE_TITLE = "Role Title";
    private static final String TITLE_DIVISION = "Division";
    private static final String TITLE_TEAM = "Team";
    private static final String TITLE_LABOR_BASE_SALARY_PAYMENT = "Base Salary Payment";
    private static final String TITLE_LABOR_ADDITIONAL_PAYMENT_1 = "Additional Payment 1";
    private static final String TITLE_LABOR_ADDITIONAL_PAYMENT_2 = "Additional Payment 2";
    private static final String TITLE_LABOR_ADDITIONAL_PAYMENT_3 = "Additional Payment 3";
    private static final String TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_1 = "Social insurance \n on Employer 1";
    private static final String TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_2 = "Social insurance \n on Employer 2";
    private static final String TITLE_LABOR_SOCIAL_INSURANCE_ON_EMPLOYER_3 = "Social insurance \n on Employer 3";
    private static final String TITLE_LABOR_OTHER_PAYMENT_1 = "Other Payment 1";
    private static final String TITLE_LABOR_OTHER_PAYMENT_2 = "Other Payment 2";
    private static final String TITLE_LABOR_OTHER_PAYMENT_3 = "Other Payment 3";
    private static final String TITLE_LABOR_COST_TOTAL = "Labor cost Total";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_1 = "Social insurance \n on Employer 1";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_2 = "Social insurance \n on Employer 2";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYER_3 = "Social insurance \n on Employer 3";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_1 = "Social insurance \n on Employee 1";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_2 = "Social insurance \n on Employee 2";
    private static final String TITLE_DEDUCTION_SOCIAL_INSURANCE_ON_EMPLOYEE_3 = "Social insurance \n on Employee 3";
    private static final String TITLE_DEDUCTION_WHT = "WHT";
    private static final String TITLE_DEDUCTION_OTHER_DEDUCTION_1 = "Other Deduction 1";
    private static final String TITLE_DEDUCTION_OTHER_DEDUCTION_2 = "Other Deduction 2";
    private static final String TITLE_DEDUCTION_OTHER_DEDUCTION_3 = "Other Deduction 3";
    private static final String TITLE_DEDUCTION_TOTAL = "Deduction Total";
    private static final String TITLE_NET_PAYMENT_ORIGINAL = "Net payment (Original)";
    private static final String TITLE_FX_RATE = "Fx Rate";
    private static final String TITLE_NET_PAYMENT_CONVERTED = "Net payment (Converted)";


}

