package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.BANK_ACCOUNT_NO;
import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.BANK_CODE;
import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.CODE;
import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.CURRENCY_CODE;
import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.NAME;
import static com.ttv.cashflow.component.BankAccountMasterImportComponent.Column.TYPE;

import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.service.BankAccountMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class BankAccountMasterImportComponent extends ImportComponent {
	private static final Logger logger = Logger.getLogger("BANK ACCOUNT MASTER");
	
	@Autowired
	private BankAccountMasterDAO bankAccountMasterDAO;
	
	@Autowired
	private BankAccountMasterService bankAccountMasterService;

	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					
					BankAccountMaster bankMaster = new BankAccountMaster();
					bankMaster.setCode(formatter.formatCellValue(row.getCell(CODE)));
					bankMaster.setType(formatter.formatCellValue(row.getCell(TYPE)));
					bankMaster.setName(formatter.formatCellValue(row.getCell(NAME)));
					bankMaster.setBankAccountNo(formatter.formatCellValue(row.getCell(BANK_ACCOUNT_NO)));
					bankMaster.setBankCode(formatter.formatCellValue(row.getCell(BANK_CODE)));
					bankMaster.setCurrencyCode(formatter.formatCellValue(row.getCell(CURRENCY_CODE)));
					
					//save /update bank account master
					bankAccountMasterService.saveBankAccountMaster(bankMaster);
					nRowSuccess++;
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					String code = formatter.formatCellValue(row.getCell(CODE));
					BankAccountMaster bankMaster = bankAccountMasterDAO.findBankAccountMasterByCode(code);
					
					if(bankMaster != null){
						bankAccountMasterService.deleteBankAccountMaster(bankMaster);
						nRowSuccess++;
					} else {
					    logHelper.writeWarningLog(nRow, "Code", "not exist DB");
					    nRowFail++;
					}
					
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.bankAccountMaster", new Object[]{}, locale);
	}
	
	class Column {
		static final int CODE = 0;
		static final int TYPE = 1;
		static final int NAME = 2;
		static final int BANK_ACCOUNT_NO = 3;
		static final int BANK_CODE = 4;
		static final int CURRENCY_CODE = 5;
	}

}
