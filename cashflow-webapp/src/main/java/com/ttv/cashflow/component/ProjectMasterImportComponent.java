package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ImportComponent.ErrorMsg.NOT_FOUND_DB;
import static com.ttv.cashflow.component.ProjectMasterImportComponent.Column.APPROVAL_CODE;
import static com.ttv.cashflow.component.ProjectMasterImportComponent.Column.BUSINESS_TYPE_NAME;
import static com.ttv.cashflow.component.ProjectMasterImportComponent.Column.CODE;
import static com.ttv.cashflow.component.ProjectMasterImportComponent.Column.CUSTOMER_NAME;
import static com.ttv.cashflow.component.ProjectMasterImportComponent.Column.NAME;

import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.service.ApprovalSalesOrderRequestService;
import com.ttv.cashflow.service.BusinessTypeMasterService;
import com.ttv.cashflow.service.PayeePayerMasterService;
import com.ttv.cashflow.service.ProjectMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class ProjectMasterImportComponent extends ImportComponent {
    private static final Logger logger = Logger.getLogger("PROJECT MASTER");
    
    public static final String COMMON_VALIDATION_PATTERN = "Row [%s], column %s: %s.";
    public static final String MUST_BE_NOT_BLANK = "must be not blank";

    @Autowired
    private ProjectMasterDAO projectMasterDAO;

    @Autowired
    private ProjectMasterService projectMasterService;

    @Autowired
    private BusinessTypeMasterService businessTypeMasterService;

    @Autowired
    private ApprovalSalesOrderRequestService approvalSalesOrderRequestService;

    @Autowired
    private PayeePayerMasterService transactionPartyMasterService;

    @Override
    public ImportReturn doMerge(Workbook workbook) {
        // create log file
        logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
        Row row = null;
        Sheet paymentSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = paymentSheet.iterator();
        int rowNumber = 1;

        while (iterator.hasNext()) {
            row = iterator.next();

            if (rowNumber > 1) {
                try {

                    // ===== Validate data ===== //
                    String code = validateNotBlank(row.getCell(CODE.getColumnNumber()), CODE.getColumnName());

                    String name = validateNotBlank(row.getCell(NAME.getColumnNumber()), NAME.getColumnName());
                    String businessTypeName = validateNotBlank(row.getCell(BUSINESS_TYPE_NAME.getColumnNumber()), BUSINESS_TYPE_NAME.getColumnName());
                    String approvalCode = validateNotBlank(row.getCell(APPROVAL_CODE.getColumnNumber()), APPROVAL_CODE.getColumnName());
                    String customerName = validateNotBlank(row.getCell(CUSTOMER_NAME.getColumnNumber()), CUSTOMER_NAME.getColumnName());

                    boolean isExistingBusinessTypeCode = businessTypeMasterService.checkExistingBusinessTypeCode(businessTypeName);

                    if (!isExistingBusinessTypeCode) {
                        throw new Exception(String.format(COMMON_VALIDATION_PATTERN, rowNumber, BUSINESS_TYPE_NAME.getColumnName(), NOT_FOUND_DB));
                    }

                    boolean isExistingExistingApprovalCode = approvalSalesOrderRequestService.checkExistingApprovalCode(approvalCode);

                    if (!isExistingExistingApprovalCode) {
                        throw new Exception(String.format(COMMON_VALIDATION_PATTERN, rowNumber, APPROVAL_CODE.getColumnName(), NOT_FOUND_DB));
                    }

                    boolean isExistingCustomerName = transactionPartyMasterService.checkExistingCustomerName(customerName);

                    if (!isExistingCustomerName) {
                        throw new Exception(String.format(COMMON_VALIDATION_PATTERN, rowNumber, CUSTOMER_NAME.getColumnName(), NOT_FOUND_DB));
                    }

                    // ===== End: Validate data ===== //

                    ProjectMaster project = new ProjectMaster();
                    project.setCode(code);
                    project.setName(name);
                    project.setBusinessTypeName(businessTypeName);
                    project.setApprovalCode(approvalCode);
                    project.setCustomerName(customerName);

                    projectMasterService.saveProjectMaster(project);
                    nRowSuccess++;
                } catch (Exception e) {
                	nRowFail++;
                    logger.info(e.getMessage());
                }
            }
            rowNumber++;
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public ImportReturn doUpdate(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doReplace(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doDelete(Workbook workbook) {

        // create log file
        logHelper.init(logger, this.getLogPath());

        int nRowSuccess = 0, nRowFail = 0;
        Row row = null;
        Sheet paymentSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = paymentSheet.iterator();
        int rowNumber = 1;

        while (iterator.hasNext()) {
            row = iterator.next();

            if (rowNumber > 1) {
                try {
                    if (row.getCell(CODE.getColumnNumber()).getCellTypeEnum() == CellType.BLANK) {
                        logHelper.writeWarningLog(rowNumber, "Code", "required");
                        nRowFail++;
                        rowNumber++;
                        continue;
                    }

                    String code = ImportComponent.DATA_FORMATTER.formatCellValue(row.getCell(CODE.getColumnNumber()));
                    ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(code);

                    if (projectMaster != null) {
                        projectMasterService.deleteProjectMaster(projectMaster);
                        nRowSuccess++;
                    } else {
                        logHelper.writeWarningLog(rowNumber, "Code", "not exist DB");
                        nRowFail++;
                    }

                } catch (Exception e) {
                	nRowFail++;
                    logger.log(Level.INFO, e.getMessage());
                }
            }
            rowNumber++;
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
    }

    @Override
    public String getName() {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.projectMaster", new Object[] {}, locale);
    }
    
    private String validateNotBlank(Cell cell, String columnName) throws Exception {
        if (cell.getCellTypeEnum() == CellType.BLANK) {
            throw new Exception(String.format(COMMON_VALIDATION_PATTERN, cell.getAddress().getRow() + 1, columnName, MUST_BE_NOT_BLANK));
        }

        return DATA_FORMATTER.formatCellValue(cell);
    }

    enum Column {
        CODE(0, "Code"), 
        NAME(1, "Name"), 
        BUSINESS_TYPE_NAME(2, "Business Type Name"), 
        APPROVAL_CODE(3, "Approval Code"), 
        CUSTOMER_NAME(4, "Customer Name");

        private int columnNumber;
        private String columnName;

        Column(int columnNumber, String columnName) {
            this.columnNumber = columnNumber;
            this.columnName = columnName;
        }

        public int getColumnNumber() {
            return columnNumber;
        }

        public String getColumnName() {
            return columnName;
        }
    }
}
