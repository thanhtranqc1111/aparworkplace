package com.ttv.cashflow.component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.service.BankStatementService;
import com.ttv.cashflow.util.ApplicationUtil;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;


/**
 * @author thoai.nh
 * created date Nov 2, 2017
 */
@Component
@PrototypeScope
public class BankStatementImportComponent extends ImportComponent {
	private static final Logger logger = Logger.getLogger("BANK STATEMENT");
	private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(BankStatementImportComponent.class);
	private String bankName;
	private String bankAccountCode;
	private String currencyCode = null;
	private DataFormatter dataFormatter = new DataFormatter();
	@Autowired
	private BankStatementService bankStatementService;
	@Autowired
	private BankAccountMasterDAO bankAccountMasterDao;
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attr.getRequest();
		bankName = request.getSession().getAttribute(Constant.BSM_BANK_NAME).toString();
		bankAccountCode = request.getSession().getAttribute(Constant.BSM_BANK_ACC_CODE).toString();
		currencyCode = request.getSession().getAttribute(Constant.BSM_CURRENCY_CODE).toString();
		return doMergeTemplate2(workbook);
	}
	
	public ImportReturn doMergeTemplate2(Workbook workbook) {
		logHelper.init(logger, this.getLogPath());
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		int i = 0;
		int nRowSuccess = 0;
		int nRecordFail = 0;
		int nRecordSuccess = 0;
		int ignoreRow = 0;//ignore 5 rows when meet new bank statement entry
		boolean isSummaryRow = false;
		BigDecimal beginningBalance = new BigDecimal(0);
		BankStatement bankStatement = null;
		Calendar maxTransactionDate = bankStatementService.getMaxTransactionDate(bankName, bankAccountCode);
		
		BigDecimal currentEndingBalance = BigDecimal.ZERO;
		Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterDao.findBankAccountMasterByBankNameAndBankAccountNo(bankName, bankAccountCode);
		if(!setBankAccountMaster.isEmpty()) {
			Iterator<BankAccountMaster> iteratorBankAccountMaster = setBankAccountMaster.iterator();
			currentEndingBalance = iteratorBankAccountMaster.next().getEndingBalance();
		}
		while (iterator.hasNext()) {
			i++;
			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			if (cellIterator.hasNext()) {
				try {
					String firstRowContent = dataFormatter.formatCellValue(currentRow.getCell(0));
					//ignore 5 rows when meet "Bank statement N" content
					if(firstRowContent.indexOf("Bank statement N") == 0)
					{
						ignoreRow = 1;
					}
					else if(ignoreRow == 5)
					{
						isSummaryRow = true;
						ignoreRow = 0;
					}
					else if(ignoreRow > 0)
					{
						ignoreRow ++;
					}
					//start parsing
					if(ignoreRow == 0) {
						if(isSummaryRow) {
							beginningBalance = NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(7).getNumericCellValue()));
							isSummaryRow = false;
							bankStatement = null;
						}
						else {
							//is first bank statement row 
							if(bankStatement == null)
							{
								//continue when meet closing balance row
								if((currentRow.getCell(0) == null && currentRow.getCell(1) == null) || StringUtils.isEmpty(firstRowContent))
								{
									continue;
								}
								bankStatement = new BankStatement();
								bankStatement.setCurrencyCode(currencyCode);
								bankStatement.setBankAccountNo(bankAccountCode);
								bankStatement.setBankName(bankName);
								bankStatement.setTransactionDate(CalendarUtil.getCalendar(currentRow.getCell(Column2.TRANSACTION_DATE).getDateCellValue()));
								if(maxTransactionDate != null && !bankStatement.getTransactionDate().after(maxTransactionDate)) {
								    logHelper.writeErrorLog("[Transaction Date] must be after the last transaction in bank statement data.");
									nRecordFail = datatypeSheet.getPhysicalNumberOfRows();
									break;
								}
								else
								{
									maxTransactionDate = null;
								}
								if(currentEndingBalance.compareTo(BigDecimal.ZERO) > 0 && beginningBalance.compareTo(currentEndingBalance) != 0) {
								    logHelper.writeErrorLog("[Begining balance] must be equal with ending balance of bank account No:" + bankAccountCode + " " + bankName);
									nRecordFail = datatypeSheet.getPhysicalNumberOfRows();
									break;
								}
								else {
									currentEndingBalance = BigDecimal.ZERO;
								}
								bankStatement.setValueDate(CalendarUtil.getCalendar(currentRow.getCell(Column2.VALUE_DATE).getDateCellValue()));
								bankStatement.setDescription1(dataFormatter.formatCellValue(currentRow.getCell(Column2.DESCRIPTION1)));
								bankStatement.setReference1(preProcessRefString(dataFormatter.formatCellValue(currentRow.getCell(Column2.REFERENCE1))));
								bankStatement.setDebit(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column2.DEBIT).getNumericCellValue())));
								bankStatement.setCredit(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column2.CREDIT).getNumericCellValue())));
								bankStatement.setBeginningBalance(beginningBalance);
								beginningBalance = beginningBalance.subtract(bankStatement.getDebit()).add(bankStatement.getCredit());
								bankStatement.setBalance(beginningBalance);
							}
							//is second bank statement row 
							else
							{
								if(bankStatement.getTransactionDate() == null) {
								    logHelper.writeWarningLog(i, "Transaction Date", "required");
									bankStatement = null;
									nRecordFail ++;
								}
								else {
									//check data not exists before save
									if(bankStatementService.checkExists(bankStatement) > 0) {
										logHelper.writeErrorLog("Columns [Transaction Date, Debit, Credit] in row 8 [" + CalendarUtil.toString(bankStatement.getTransactionDate()) + ", " 
													   + bankStatement.getDebit() + ", " + bankStatement.getCredit() + "] already existed in bank statement data");
										bankStatement = null;
										nRecordFail ++;
									}
									else {
										//save here
										bankStatement.setDescription2(dataFormatter.formatCellValue(currentRow.getCell(Column2.DESCRIPTION2)));
										bankStatement.setReference2(preProcessRefString(dataFormatter.formatCellValue(currentRow.getCell(Column2.REFERENCE2))));
										bankStatementService.saveBankStatement(bankStatement);								
										//update ending balance in bank account master data
										updateBankAccountEndingBalance(bankStatement);
										bankStatement = null;
										nRowSuccess++;
										nRecordSuccess++;
									}
								}
							}
						}
					}
				} catch (Exception e) {
					nRecordFail ++;
					APP_LOGGER.error("Import bank statement error", e);
				}
			}
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess, nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess, nRowSuccess, nRecordFail);
	}
	
	@Deprecated
	public ImportReturn doMergeTemplate1(Workbook workbook) {
		Calendar maxTransactionDate = bankStatementService.getMaxTransactionDate(bankName, bankAccountCode);
		logHelper.init(logger, this.getLogPath());
		int i = 0, nRowSuccess = 0, nRecordFail = 0;
		try {
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			BigDecimal currentEndingBalance = BigDecimal.ZERO;
			Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterDao.findBankAccountMasterByBankNameAndBankAccountNo(bankName, bankAccountCode);
			if(setBankAccountMaster.size() > 0) {
				Iterator<BankAccountMaster> iteratorBankAccountMaster = setBankAccountMaster.iterator();
				currentEndingBalance = iteratorBankAccountMaster.next().getEndingBalance();
			}
			while (iterator.hasNext()) {
				i++;
				Row currentRow = iterator.next();
				if (i >= 2) {
					try {
						Iterator<Cell> cellIterator = currentRow.iterator();
						if (cellIterator.hasNext()) {
							BankStatement bankStatement = new BankStatement();
							if (currentRow.getCell(Column1.TRANSACTION_DATE) != null && currentRow.getCell(Column1.TRANSACTION_DATE).getCellTypeEnum() == CellType.NUMERIC)
							{
								Calendar transactionDate = ApplicationUtil.toCalendar(currentRow.getCell(Column1.TRANSACTION_DATE).getDateCellValue());
								bankStatement.setTransactionDate(transactionDate);
								if(maxTransactionDate != null && !bankStatement.getTransactionDate().after(maxTransactionDate)) {
								    logHelper.writeErrorLog("[Transaction Date] must be after the last transaction in bank statement data.");
									nRecordFail = datatypeSheet.getPhysicalNumberOfRows();
									break;
								}
								else
								{
									maxTransactionDate = null;
								}
							}
							if (currentRow.getCell(Column1.VALUE_DATE) != null && currentRow.getCell(Column1.VALUE_DATE).getCellTypeEnum() == CellType.NUMERIC)
							{
								bankStatement.setValueDate(ApplicationUtil.toCalendar(currentRow.getCell(Column1.VALUE_DATE).getDateCellValue()));
							}
							
							if (currentRow.getCell(Column1.BEGINNING_BALANCE) != null)
							{
								bankStatement.setBeginningBalance(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column1.BEGINNING_BALANCE).getNumericCellValue())));
								if(currentEndingBalance.compareTo(BigDecimal.ZERO) > 0 && bankStatement.getBeginningBalance().compareTo(currentEndingBalance) != 0) {
								    logHelper.writeErrorLog("[Begining balance] must be equal with ending balance of bank account No:" + bankAccountCode + " " + bankName);
									nRecordFail = datatypeSheet.getPhysicalNumberOfRows();
									break;
								}
								else {
									currentEndingBalance = BigDecimal.ZERO;
								}
							}
							
							if (currentRow.getCell(Column1.DESCRIPTION1) != null)
							{
								bankStatement.setDescription1(dataFormatter.formatCellValue(currentRow.getCell(Column1.DESCRIPTION1)));
							}
							
							if (currentRow.getCell(Column1.DESCRIPTION2) != null)
							{
								bankStatement.setDescription2(dataFormatter.formatCellValue(currentRow.getCell(Column1.DESCRIPTION2)));
							}
							
							if (currentRow.getCell(Column1.REFERENCE1) != null)
							{
								bankStatement.setReference1(preProcessRefString(dataFormatter.formatCellValue(currentRow.getCell(Column1.REFERENCE1))));
							}
							
							if (currentRow.getCell(Column1.REFERENCE2) != null)
							{
								bankStatement.setReference2(preProcessRefString(dataFormatter.formatCellValue(currentRow.getCell(Column1.REFERENCE2))));
							}
							
							if (currentRow.getCell(Column1.DEBIT) != null)
							{
								bankStatement.setDebit(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column1.DEBIT).getNumericCellValue())));
							}
							
							if (currentRow.getCell(Column1.CREDIT) != null)
							{
								bankStatement.setCredit(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column1.CREDIT).getNumericCellValue())));
							}
							
							if (currentRow.getCell(Column1.ENDING_BALANCE) != null)
							{
								bankStatement.setBalance(NumberUtil.getBigDecimalScaleDown(2, String.valueOf(currentRow.getCell(Column1.ENDING_BALANCE).getNumericCellValue())));
							}
							bankStatement.setBankAccountNo(bankAccountCode);
							bankStatement.setBankName(bankName);
							bankStatement.setCurrencyCode(currencyCode);
							bankStatementService.saveBankStatement(bankStatement);
							updateBankAccountEndingBalance(bankStatement);
							nRowSuccess++;
						}
					} catch (Exception e) {
						e.printStackTrace();
						nRecordFail++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRecordFail);
		logHelper.close();
		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRecordFail);
	
	}
	
	private void updateBankAccountEndingBalance(BankStatement bankStatement) {
		Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterDao.findBankAccountMasterByBankAccountNo(bankStatement.getBankAccountNo());
		if(setBankAccountMaster.size() > 0) {
			Iterator<BankAccountMaster> bankAccIterator = setBankAccountMaster.iterator();
			while(bankAccIterator.hasNext()) {
				BankAccountMaster bankAccountMaster = bankAccIterator.next();
				bankAccountMaster.setEndingBalance(bankStatement.getBalance());
				bankAccountMasterDao.store(bankAccountMaster);
			}
		}
	}
	
	private String preProcessRefString(String ref) {
		if(ref.indexOf("Code: ") == 0)
		{
			return ref.substring(6, ref.length());
		}
		else if(ref.indexOf("Ref: ") == 0)
		{
			return ref.substring(5, ref.length());
		}
		else {
			return ref;
		}
	}
	public String getBankName() {
		return bankName;
	}
	
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getBankAccountCode() {
		return bankAccountCode;
	}
	
	public void setBankAccountCode(String bankAccountCode) {
		this.bankAccountCode = bankAccountCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.bankStatement", new Object[]{}, locale);
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}
	
	class Column1 {
		static final int TRANSACTION_DATE = 0;
		static final int VALUE_DATE = 1;
		static final int BEGINNING_BALANCE = 2;
		static final int DESCRIPTION1 = 3;
		static final int DESCRIPTION2 = 4;
		static final int REFERENCE1 = 5;
		static final int REFERENCE2 = 6;
		static final int DEBIT = 7;
		static final int CREDIT = 8;
		static final int ENDING_BALANCE = 9;
	}
	
	class Column2 {
		static final int BANK_NAME = 0;
		static final int BANK_ACC_NO = 1;
		static final int TRANSACTION_DATE = 2;
		static final int VALUE_DATE = 3;
		static final int DESCRIPTION1 = 4;
		static final int DESCRIPTION2 = 4;
		static final int REFERENCE1 = 5;
		static final int REFERENCE2 = 5;
		static final int DEBIT = 6;
		static final int CREDIT = 7;
	}
}
