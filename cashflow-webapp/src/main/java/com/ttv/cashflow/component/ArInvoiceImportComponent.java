package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.ACCOUNT_CODE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.ACCOUNT_NAME;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.ACCOUNT_RECEIVABLE_CODE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.ACCOUNT_RECEIVABLE_NAME;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.APPROVAL_CODE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.CLAIM_TYPE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.DESCRIPTION;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.EXCLUDE_GST_ORIGINAL_AMOUNT;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.FX_RATE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.GST_RATE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.GST_TYPE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.INVOICE_ISSUED_DATE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.INVOICE_NO;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.MONTH;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.ORIGINAL_CURRENCY;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.PAYER_NAME;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.PROJECT_CODE;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.PROJECT_DESCRIPTION;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Column.PROJECT_NAME;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Message.LESS_THAN_ZERO;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Message.NOT_FOUND_DB;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Message.NOT_NULL_DATA;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Message.REQUIRED;
import static com.ttv.cashflow.component.ArInvoiceImportComponent.Message.WRONG_FORMAT_DATE;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApprovalSalesOrderRequestDAO;
import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.dao.ArInvoiceReceiptStatusDAO;
import com.ttv.cashflow.dao.ArInvoiceTempDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;
import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;
import com.ttv.cashflow.domain.ArInvoiceTemp;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.service.ArInvoiceAccountDetailsService;
import com.ttv.cashflow.service.ArInvoiceClassDetailsService;
import com.ttv.cashflow.service.ArInvoiceReceiptStatusService;
import com.ttv.cashflow.service.ArInvoiceService;
import com.ttv.cashflow.service.ArInvoiceTempService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class ArInvoiceImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("AR INVOICE");
    private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(ArInvoiceImportComponent.class);
    
    private DataFormatter df = new DataFormatter();
    
    @Autowired
    private ArInvoiceTempDAO arInvoiceTempDAO;
    
    @Autowired
    private ArInvoiceTempService arInvoiceTempService;
    
    @Autowired
    private ArInvoiceService arInvoiceService;
    
    @Autowired
    private ArInvoiceDAO arInvoiceDAO;
    
    @Autowired
    private ArInvoiceAccountDetailsService arInvoiceAccountDetailsService;
    
    @Autowired
    private ArInvoiceClassDetailsService arInvoiceClassDetailsService;
    
    @Autowired
    private PayeePayerMasterDAO payeePayerMasterDAO;
    
    @Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
    
    @Autowired
    private AccountMasterDAO accountMasterDAO;
    
    @Autowired
    private ProjectMasterDAO projectMasterDAO;
    
    @Autowired
    private ArInvoiceReceiptStatusDAO arInvoiceReceiptStatusDAO;
    
    @Autowired
    private ArInvoiceReceiptStatusService arInvoiceReceiptStatusService;
    
    @Autowired
    private ApprovalSalesOrderRequestDAO approvalDAO;
    	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
		
		MutableInt nRecordSuccess = new MutableInt(0);
		MutableInt nRowSuccess = new MutableInt(0);
		MutableInt nRowFail = new MutableInt(0);
		
	    Sheet sheet = workbook.getSheetAt(0);
	    
	    //
	    arInvoiceTempDAO.truncate();
	    
	    //SAVE TO AP INVOICE TEMP
	    for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {

                    doSaveArInvoiceTemp(row, Mode.MERGE, nRowSuccess, nRowFail);

                } catch (Exception e) {
                    APP_LOGGER.error(getName(), e);
                }
            }
	    }
	    
	    //GET DATA FROM TABLE TEMP AND IMPORT
        doImportData(nRecordSuccess);
        
        //
        arInvoiceTempDAO.truncate();
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
        logHelper.close();
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
	}

    private void doSaveArInvoiceTemp(Row row, Mode mode, MutableInt nRecordSuccess, MutableInt nRecordFail) {
    	// check end of row.
        if(isRowEmpty(row)){
        	return;
        }
    	
        ArInvoiceTemp arInvoiceTemp = new ArInvoiceTemp();
        
        int nRowData = row.getRowNum() + 1;//row data from 3.
        
        //set payerName
        String payerName = df.formatCellValue(row.getCell(PAYER_NAME)).trim();
        if(StringUtils.isEmpty(payerName)){
        	logHelper.writeWarningLog(nRowData, "Transaction Party Master", NOT_NULL_DATA);
    		nRecordFail.increment();
    		return;
    		
        } else {
        	Set<PayeePayerMaster> payeeSet = payeePayerMasterDAO.findPayeePayerMasterByName(payerName);
        	Iterator<PayeePayerMaster> payeeIter = payeeSet.iterator();
        	
        	if (payeeIter.hasNext()) {
        		PayeePayerMaster payer = payeeIter.next();
        		arInvoiceTemp.setPayerName(payer.getName());
        		
        	} else {
        		logHelper.writeWarningLog(nRowData, "Transaction Party Master", payerName, NOT_FOUND_DB);
        		nRecordFail.increment();
        		return;
        	}
        }
        
        String invoiceNo = df.formatCellValue(row.getCell(INVOICE_NO));
        arInvoiceTemp.setInvoiceNo(invoiceNo);
        
        // Invoice Issued Date
        Calendar invoiceIssuedDate = null;
        if (row.getCell(INVOICE_ISSUED_DATE).getCellTypeEnum() != CellType.BLANK) {
            if (row.getCell(INVOICE_ISSUED_DATE).getCellTypeEnum() == CellType.NUMERIC) {
                invoiceIssuedDate = CalendarUtil.getCalendar(row.getCell(INVOICE_ISSUED_DATE).getDateCellValue());
            } else {
                logHelper.writeWarningLog(nRowData, "Invoice Issued Date", df.formatCellValue(row.getCell(INVOICE_ISSUED_DATE)), WRONG_FORMAT_DATE);
                nRecordFail.increment();
                return;
            }
        }
        
        arInvoiceTemp.setInvoiceIssuedDate(invoiceIssuedDate);
        // check value date is not empty
        Calendar month = null;
        if (row.getCell(MONTH).getCellTypeEnum() != CellType.BLANK) {
            if (row.getCell(MONTH).getCellTypeEnum() == CellType.NUMERIC) {
                month = CalendarUtil.getCalendar(row.getCell(MONTH).getDateCellValue());
            } else {
                logHelper.writeWarningLog(nRowData, "Month", df.formatCellValue(row.getCell(MONTH)), WRONG_FORMAT_DATE);
                nRecordFail.increment();
                return;
            }
        } else {
        	logHelper.writeWarningLog(nRowData, "Month", REQUIRED);
        	nRecordFail.increment();
            return;
        }
        arInvoiceTemp.setMonth(month);
        
        //claimType
        String claimType = df.formatCellValue(row.getCell(CLAIM_TYPE)).trim();

        Iterator<ClaimTypeMaster> claimTypeIter = claimTypeMasterDAO.findClaimTypeMasterByName(claimType).iterator();
        if (claimTypeIter.hasNext()) {
            ClaimTypeMaster claimTypeMaster = claimTypeIter.next();
            arInvoiceTemp.setClaimType(claimTypeMaster.getCode());
        } else {
            logHelper.writeWarningLog(nRowData, "Claim Type", df.formatCellValue(row.getCell(CLAIM_TYPE)), NOT_FOUND_DB);
            nRecordFail.increment();
            return;
        }
        
        
        arInvoiceTemp.setOriginalCurrency(df.formatCellValue(row.getCell(ORIGINAL_CURRENCY)));
        
        //fxRate
        BigDecimal fxRate = getBigDecimal(row.getCell(FX_RATE));
        
        if(fxRate.compareTo(BigDecimal.ZERO) == - 1){ //fxRate < 0
            logHelper.writeWarningLog(nRowData, "Fx Rate", fxRate.toString(), LESS_THAN_ZERO);
            nRecordFail.increment();
            return;
        } else {
            arInvoiceTemp.setFxRate(fxRate);
        }    
        
        //Set get receivable account /code
        String receivableName = df.formatCellValue(row.getCell(ACCOUNT_RECEIVABLE_NAME)).trim();
        String receivableCode = df.formatCellValue(row.getCell(ACCOUNT_RECEIVABLE_CODE)).trim();

        //check code
    	if(StringUtils.isEmpty(receivableCode)){
    		if(StringUtils.isEmpty(receivableName)){
    			logHelper.writeWarningLog(nRowData, "Account Receivable Name or Account Receivable Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
    			Set<AccountMaster> receivableSet = accountMasterDAO.findAccountMasterByName(receivableName);
            	Iterator<AccountMaster> receivableAccountMasterIter = receivableSet.iterator();

            	if(receivableAccountMasterIter.hasNext()){
            		AccountMaster account = receivableAccountMasterIter.next();
            		arInvoiceTemp.setAccountReceivableCode(account.getCode());
            		arInvoiceTemp.setAccountReceivableName(account.getName());
            	} else {
            		logHelper.writeWarningLog(nRowData, "Account Payable Name", receivableName, NOT_FOUND_DB);
                    nRecordFail.increment();
                    return;
            	}
    		}
    	} else {
    		AccountMaster receivableAccountMaster = accountMasterDAO.findAccountMasterByCode(receivableCode);
    		if(receivableAccountMaster != null){
    			if(StringUtils.isEmpty(receivableName)){
    				arInvoiceTemp.setAccountReceivableCode(receivableAccountMaster.getCode());
    				arInvoiceTemp.setAccountReceivableName(receivableAccountMaster.getName());
    			} else {
    				if(receivableAccountMaster.getName().equals(receivableName)){
    					arInvoiceTemp.setAccountReceivableCode(receivableAccountMaster.getCode());
    					arInvoiceTemp.setAccountReceivableName(receivableAccountMaster.getName());
    				} else {
    					logHelper.writeErrorLog(nRowData, "Account Receivable Code", receivableCode, "Account Receivable Name", receivableName);
    					nRecordFail.increment();
    					return;
    				}
    			}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Account Payable Code", receivableCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
    	
        arInvoiceTemp.setDescription(df.formatCellValue(row.getCell(DESCRIPTION)));
        
        String accountCode = df.formatCellValue(row.getCell(ACCOUNT_CODE)).trim();
        String accountName = df.formatCellValue(row.getCell(ACCOUNT_NAME)).trim();
        if(StringUtils.isEmpty(accountCode)){
    		if(StringUtils.isEmpty(accountName)){
    			logHelper.writeWarningLog(nRowData, "Account Name or Account Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
            	Set<AccountMaster> accountSet = accountMasterDAO.findAccountMasterByName(accountName);
    			Iterator<AccountMaster> accountIter = accountSet.iterator();
    			
    			if(accountIter.hasNext()){
    				AccountMaster account = accountIter.next();
    				arInvoiceTemp.setAccountCode(account.getCode());
    				arInvoiceTemp.setAccountName(account.getName());
    			} else {
    				logHelper.writeWarningLog(nRowData, "Account Name", accountName, NOT_FOUND_DB);
                    nRecordFail.increment();
                    return;
    			}
    		}
    	} else {
    		AccountMaster accountMaster = accountMasterDAO.findAccountMasterByCode(accountCode);
    		if(accountMaster != null){
    			if(StringUtils.isEmpty(accountName)){
    				arInvoiceTemp.setAccountCode(accountMaster.getCode());
    				arInvoiceTemp.setAccountName(accountMaster.getName());
    			} else {
    				if(accountMaster.getName().equals(accountName)){
    					arInvoiceTemp.setAccountCode(accountMaster.getCode());
    					arInvoiceTemp.setAccountName(accountMaster.getName());
    				} else {
    					logHelper.writeErrorLog(nRowData, "Account Code", accountCode, "Account Name", accountName);
    					nRecordFail.increment();
    					return;
    				}
    			}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Account Code", accountCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
        
        // set project name
        String projectCode = df.formatCellValue(row.getCell(PROJECT_CODE)).trim();
        String projectName = df.formatCellValue(row.getCell(PROJECT_NAME)).trim();
        
        if(StringUtils.isEmpty(projectCode)){
    		if(StringUtils.isEmpty(projectName)){
    			logHelper.writeWarningLog(nRowData, "Project Name or Project Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
    			Set<ProjectMaster> projectSet = projectMasterDAO.findProjectMasterByName(projectName);
            	Iterator<ProjectMaster> projectIter = projectSet.iterator();
            	
            	if(projectIter.hasNext()){
            		ProjectMaster project = projectIter.next();
                	arInvoiceTemp.setProjectCode(project.getCode());
        			arInvoiceTemp.setProjectName(project.getName());
            	} else {
            		logHelper.writeWarningLog(nRowData, "Project Name", projectName, NOT_FOUND_DB);
                    nRecordFail.increment();
                    return;
            	}
    		}
    	} else {
    		ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(projectCode);
    		
    		if(projectMaster != null){
    			if(StringUtils.isEmpty(projectName)){
    				arInvoiceTemp.setProjectCode(projectMaster.getCode());
    				arInvoiceTemp.setProjectName(projectMaster.getName());
    			} else {
    				if(projectMaster.getName().equals(projectName)){
    					arInvoiceTemp.setProjectCode(projectMaster.getCode());
    					arInvoiceTemp.setProjectName(projectMaster.getName());
    				} else {
    					logHelper.writeErrorLog(nRowData, "Project Code", projectCode, "Project Name", projectName);
    					nRecordFail.increment();
    					return;
    				}
    			}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Project Code", projectCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
        
        arInvoiceTemp.setProjectDescription(df.formatCellValue(row.getCell(PROJECT_DESCRIPTION)));
        //approvalCode
        String approvalCode = df.formatCellValue(row.getCell(APPROVAL_CODE));
        if(hasText(approvalCode)){
            if(approvalDAO.findApprovalSalesOrderRequestByApprovalCode(approvalCode).size() == 0){
                logHelper.writeWarningLog(nRowData, "Approval Code", approvalCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
            } else {
                arInvoiceTemp.setIsApproval(Boolean.TRUE);
                arInvoiceTemp.setApprovalCode(approvalCode);
            }
        }else{
            arInvoiceTemp.setIsApproval(Boolean.FALSE);
            arInvoiceTemp.setApprovalCode(approvalCode);
        }
        
        //excludeGstOriginalAmount
        BigDecimal excludeGstOriginalAmount = getBigDecimal(row.getCell(EXCLUDE_GST_ORIGINAL_AMOUNT));
        if(excludeGstOriginalAmount.compareTo(BigDecimal.ZERO) == - 1){ //excludeGstOriginalAmount < 0
            logHelper.writeWarningLog(nRowData, "Amount Excluded GST (Original)", excludeGstOriginalAmount.toString(), LESS_THAN_ZERO);
            nRecordFail.increment();
            return;
        } else {
            arInvoiceTemp.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
        }    
        
        arInvoiceTemp.setGstType(df.formatCellValue(row.getCell(GST_TYPE)));
        
        //gstRate
        BigDecimal gstRate = getBigDecimal(row.getCell(GST_RATE));
        
        if(gstRate.compareTo(BigDecimal.ZERO) == - 1){ //gstRate < 0
            logHelper.writeWarningLog(nRowData, "GST(%) Rate", gstRate.toString(), LESS_THAN_ZERO);
            nRecordFail.increment();
            return;
        } else {
            arInvoiceTemp.setGstRate(gstRate);
        }
        
        //Check row is exist on database
        String importKey = getImportKey(arInvoiceTemp);
        if (mode.equals(Mode.MERGE)
                && null != arInvoiceDAO.findArInvoiceByImportKey(importKey)) {
            logHelper.writeWarningExistLog(nRowData, "Invoice No", invoiceNo);
            nRecordFail.increment();
            return;
        }
        
        arInvoiceTempService.saveArInvoiceTemp(arInvoiceTemp);
        
        //
        nRecordSuccess.increment();
    }
    
    private void doImportData(MutableInt nRecordSuccess) {
        
        List<String> listIdtemp = arInvoiceTempDAO.findListIdArInvoiceTempByHavingGroup();
        
        for(int i = 0; i < listIdtemp.size(); i++){
            String[] arrIdtemp = listIdtemp.get(i).toString().split(",");
            
            ArInvoiceTemp arInvoiceTemp = arInvoiceTempService.findArInvoiceTempByPrimaryKey(new BigInteger(arrIdtemp[0]));
            List<ArInvoiceTemp> arInvoiceTempList = arInvoiceTempDAO.findArInvoiceTempListId(getListIdTempInBigInt(arrIdtemp));
            //apInvoiceTempList.sort((o1, o2) -> o1.getPaymentOrderNo().compareTo(o2.getPaymentOrderNo()));
            
            //SAVE AR INVOICE
            ArInvoice arInvoice = new ArInvoice();
            doSaveArInvoice(arInvoice, arInvoiceTemp, arInvoiceTempList);
            
            nRecordSuccess.increment();
            
            ////SAVE AR INVOICE DETAIL
            doSaveArInvoiceDetail(arInvoice, arInvoiceTempList);
        }
    }
    
    private void doSaveArInvoice(ArInvoice arInvoice, ArInvoiceTemp arInvoiceTemp, List<ArInvoiceTemp> arInvoiceTempList) {
        String importKey = getImportKey(arInvoiceTemp);
        
        BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
        BigDecimal excludeGstConvertedAmount = new BigDecimal(0);
        BigDecimal gstOriginalAmount = new BigDecimal(0);
        BigDecimal gstConvertedAmount = new BigDecimal(0);
        BigDecimal includeGstOriginalAmount = new BigDecimal(0);
        BigDecimal includeGstConvertedAmount = new BigDecimal(0);
        
        for(ArInvoiceTemp arInvoiceTemp1 : arInvoiceTempList){
            BigDecimal fxRateTmp = arInvoiceTemp1.getFxRate();
            BigDecimal gstRateTmp = arInvoiceTemp1.getGstRate();
            BigDecimal excludeGstOriginalAmountTmp = arInvoiceTemp1.getExcludeGstOriginalAmount();
            BigDecimal excludeGstConvertedAmountTemp =  excludeGstOriginalAmountTmp.multiply(fxRateTmp);
            BigDecimal gstAmountOriginalTmp = excludeGstOriginalAmountTmp.multiply(gstRateTmp).divide(BigDecimal.valueOf(100));
            BigDecimal gstConvertedAmountTemp = gstAmountOriginalTmp.multiply(fxRateTmp);
            
            //
            excludeGstOriginalAmount = excludeGstOriginalAmount.add(excludeGstOriginalAmountTmp);
            excludeGstConvertedAmount = excludeGstConvertedAmount.add(excludeGstConvertedAmountTemp);
            gstOriginalAmount = gstOriginalAmount.add(gstAmountOriginalTmp);
            gstConvertedAmount = gstConvertedAmount.add(gstConvertedAmountTemp);
            includeGstOriginalAmount = includeGstOriginalAmount.add(excludeGstOriginalAmountTmp.add(gstAmountOriginalTmp));
            includeGstConvertedAmount = includeGstConvertedAmount.add(excludeGstConvertedAmountTemp.add(gstConvertedAmountTemp));
        }
        
        //do scale
        excludeGstOriginalAmount = getScaleBigDecimal(excludeGstOriginalAmount);
        excludeGstConvertedAmount = getScaleBigDecimal(excludeGstConvertedAmount);
        gstOriginalAmount = getScaleBigDecimal(gstOriginalAmount);
        gstConvertedAmount = getScaleBigDecimal(gstConvertedAmount);
        includeGstOriginalAmount = getScaleBigDecimal(includeGstOriginalAmount);
        includeGstConvertedAmount = getScaleBigDecimal(includeGstConvertedAmount);
        
        //
        BeanUtils.copyProperties(arInvoiceTemp, arInvoice, "id");
        arInvoice.setArInvoiceNo(arInvoiceDAO.getArInvoiceNo(arInvoiceTemp.getMonth()));
        arInvoice.setOriginalCurrencyCode(arInvoiceTemp.getOriginalCurrency());
        arInvoice.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
        arInvoice.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
        arInvoice.setGstType(arInvoiceTemp.getGstType());
        arInvoice.setGstRate(arInvoiceTemp.getGstRate());
        arInvoice.setGstOriginalAmount(gstOriginalAmount);
        arInvoice.setGstConvertedAmount(gstConvertedAmount);
        arInvoice.setIncludeGstOriginalAmount(includeGstOriginalAmount);
        arInvoice.setIncludeGstConvertedAmount(includeGstConvertedAmount);
        arInvoice.setStatus(Constant.API_NOT_PAID);
        arInvoice.setImportKey(importKey);
        arInvoice.setCreatedDate(Calendar.getInstance());
        arInvoice.setModifiedDate(Calendar.getInstance());
        
        arInvoiceService.saveArInvoice(arInvoice);
    }
    
    private void doSaveArInvoiceDetail(ArInvoice arInvoice, List<ArInvoiceTemp> arInvoiceTempList) {
        List<String> accountNameVisited = new ArrayList<>();
        
        for(ArInvoiceTemp arInvoiceTemp : arInvoiceTempList){
            
            String accountName = arInvoiceTemp.getAccountName();
            
            if (accountNameVisited.contains(accountName)) {
                continue;
            }
            accountNameVisited.add(accountName);
            
            /////Do save invoice account detail/////
            ArInvoiceAccountDetails arInvoiceAccountDetails = new ArInvoiceAccountDetails();
            
            BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
            BigDecimal excludeGstConvertedAmount = new BigDecimal(0);
            BigDecimal gstOriginalAmount = new BigDecimal(0);
            BigDecimal gstConvertedAmount = new BigDecimal(0);
            BigDecimal includeGstOriginalAmount = new BigDecimal(0);
            BigDecimal includeGstConvertedAmount = new BigDecimal(0);
            
            for(ArInvoiceTemp arInvoiceTemp2 : arInvoiceTempList){
                if(accountName.equals(arInvoiceTemp2.getAccountName())){
                    BigDecimal fxRateTmp = arInvoiceTemp2.getFxRate();
                    BigDecimal gstRateTmp = arInvoiceTemp2.getGstRate();
                    BigDecimal excludeGstOriginalAmountTmp = arInvoiceTemp2.getExcludeGstOriginalAmount();
                    BigDecimal excludeGstConvertedAmountTmp =  excludeGstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal gstOriginalAmountTmp = excludeGstOriginalAmountTmp.multiply(gstRateTmp).divide(BigDecimal.valueOf(100));
                    BigDecimal gstConvertedAmountTmp = gstOriginalAmountTmp.multiply(fxRateTmp);
                    
                    //
                    excludeGstOriginalAmount = excludeGstOriginalAmount.add(excludeGstOriginalAmountTmp);
                    excludeGstConvertedAmount = excludeGstConvertedAmount.add(excludeGstConvertedAmountTmp);
                    gstOriginalAmount = gstOriginalAmount.add(gstOriginalAmountTmp);
                    gstConvertedAmount = gstConvertedAmount.add(gstConvertedAmountTmp);
                    includeGstOriginalAmount = includeGstOriginalAmount.add(excludeGstOriginalAmountTmp.add(gstOriginalAmountTmp));
                    includeGstConvertedAmount = includeGstConvertedAmount.add(excludeGstConvertedAmountTmp.add(gstConvertedAmountTmp));
                }
            }
            
            //do scale
            excludeGstOriginalAmount = getScaleBigDecimal(excludeGstOriginalAmount);
            excludeGstConvertedAmount = getScaleBigDecimal(excludeGstConvertedAmount);
            gstOriginalAmount = getScaleBigDecimal(gstOriginalAmount);
            gstConvertedAmount = getScaleBigDecimal(gstConvertedAmount);
            includeGstOriginalAmount = getScaleBigDecimal(includeGstOriginalAmount);
            includeGstConvertedAmount = getScaleBigDecimal(includeGstConvertedAmount);
            
            //
            BeanUtils.copyProperties(arInvoiceTemp, arInvoiceAccountDetails, "id");
            arInvoiceAccountDetails.setArInvoice(arInvoice);
            arInvoiceAccountDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
            arInvoiceAccountDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
            arInvoiceAccountDetails.setGstOriginalAmount(gstOriginalAmount);
            arInvoiceAccountDetails.setGstConvertedAmount(gstConvertedAmount);
            arInvoiceAccountDetails.setIncludeGstOriginalAmount(includeGstOriginalAmount);
            arInvoiceAccountDetails.setIncludeGstConvertedAmount(includeGstConvertedAmount);
            arInvoiceAccountDetails.setCreatedDate(Calendar.getInstance());
            arInvoiceAccountDetails.setModifiedDate(Calendar.getInstance());
            
            arInvoiceAccountDetailsService.saveArInvoiceAccountDetails(arInvoiceAccountDetails);
            
            /////Do save invoice class detail/////
            for(ArInvoiceTemp arInvoiceTemp2 : arInvoiceTempList){
                if(accountName.equals(arInvoiceTemp2.getAccountName())){
                    ArInvoiceClassDetails arInvoiceClassDetails = new ArInvoiceClassDetails();
                    
                    BigDecimal fxRateTmp = arInvoiceTemp2.getFxRate();
                    BigDecimal gstRateTmp = arInvoiceTemp2.getGstRate();
                    BigDecimal excludeGstOriginalAmountTmp = arInvoiceTemp2.getExcludeGstOriginalAmount();
                    BigDecimal excludeGstConvertedAmountTmp = excludeGstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal gstOriginalAmountTmp = excludeGstOriginalAmountTmp.multiply(gstRateTmp).divide(BigDecimal.valueOf(100));
                    BigDecimal gstConvertedAmountTmp = gstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal includeGstOriginalAmountTmp = excludeGstOriginalAmountTmp.add(gstOriginalAmountTmp);
                    BigDecimal includeGstConvertedAmountTmp = excludeGstConvertedAmountTmp.add(gstConvertedAmountTmp);
                    
                    //do scale
                    excludeGstOriginalAmountTmp = getScaleBigDecimal(excludeGstOriginalAmountTmp);
                    excludeGstConvertedAmountTmp = getScaleBigDecimal(excludeGstConvertedAmountTmp);
                    gstOriginalAmountTmp = getScaleBigDecimal(gstOriginalAmountTmp);
                    gstConvertedAmountTmp = getScaleBigDecimal(gstConvertedAmountTmp);
                    includeGstOriginalAmountTmp = getScaleBigDecimal(includeGstOriginalAmountTmp);
                    includeGstConvertedAmountTmp = getScaleBigDecimal(includeGstConvertedAmountTmp);
                    
                    //
                    BeanUtils.copyProperties(arInvoiceTemp2, arInvoiceClassDetails);
                    
                    arInvoiceClassDetails.setClassCode(arInvoiceTemp2.getProjectCode());
                    arInvoiceClassDetails.setClassName(arInvoiceTemp2.getProjectName());
                    arInvoiceClassDetails.setDescription(arInvoiceTemp2.getProjectDescription());
                    arInvoiceClassDetails.setArInvoiceAccountDetails(arInvoiceAccountDetails);
                    arInvoiceClassDetails.setFxRate(fxRateTmp);
                    arInvoiceClassDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmountTmp);
                    arInvoiceClassDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmountTmp);
                    arInvoiceClassDetails.setGstRate(gstRateTmp);
                    arInvoiceClassDetails.setGstOriginalAmount(gstOriginalAmountTmp);
                    arInvoiceClassDetails.setGstConvertedAmount(gstConvertedAmountTmp);
                    arInvoiceClassDetails.setIncludeGstOriginalAmount(includeGstOriginalAmountTmp);
                    arInvoiceClassDetails.setIncludeGstConvertedAmount(includeGstConvertedAmountTmp);
                    arInvoiceClassDetails.setGstType(arInvoiceTemp2.getGstType());
                    arInvoiceClassDetails.setCreatedDate(Calendar.getInstance());
                    arInvoiceClassDetails.setModifiedDate(Calendar.getInstance());
                    
                    arInvoiceClassDetailsService.saveArInvoiceClassDetails(arInvoiceClassDetails);
                }
            }
            
        }
    }

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
		
		MutableInt nRecordSuccess = new MutableInt(0);
		MutableInt nRowSuccess = new MutableInt(0);
        MutableInt nRowFail = new MutableInt(0);
        
        Sheet sheet = workbook.getSheetAt(0);
	    
        //
        arInvoiceTempDAO.truncate();
        
        //SAVE TO AP INVOICE TEMP
        for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            
            try {
                
                doSaveArInvoiceTemp(row, Mode.DELETE, nRowSuccess, nRowFail);
                
            } catch (Exception e) {
                APP_LOGGER.error(getName(), e);
                
            }
        }
	    
        //DELETE AP INVOICE AND DETAIL RELATE
        List<String> listIdtemp = arInvoiceTempDAO.findListIdArInvoiceTempByHavingGroup();
        
        for(int i = 0; i < listIdtemp.size(); i++){
            String[] arrIdtemp = listIdtemp.get(i).toString().split(",");
            
            ArInvoiceTemp arInvoiceTemp = arInvoiceTempService.findArInvoiceTempByPrimaryKey(new BigInteger(arrIdtemp[0]));
            
            String importKey = getImportKey(arInvoiceTemp);
            ArInvoice arInvoice = arInvoiceDAO.findArInvoiceByImportKey(importKey);
            
            if(arInvoice != null){
                arInvoiceService.deleteArInvoice(arInvoice);
                
                nRecordSuccess.increment();
                
                //delete ApInvoicePaymentStatus
                ArInvoiceReceiptStatus arInvoiceReceiptStatus =  arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByArInvoiceId(arInvoice.getId());
                arInvoiceReceiptStatusService.deleteArInvoiceReceiptStatus(arInvoiceReceiptStatus);
            }
        }
        
        //
        arInvoiceTempDAO.truncate();
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
	}
	
	private List<BigInteger> getListIdTempInBigInt(String[] arrIdtemp){
	    List<BigInteger> listId = new ArrayList<>();
	    
	    for(String id : arrIdtemp){
	        listId.add(new BigInteger(id));
	    }
	    
	    return listId;
	}
	
	private String getImportKey(ArInvoiceTemp apInvoiceTemp) {
        return new StringBuilder()
        .append(apInvoiceTemp.getPayerName())
        .append(apInvoiceTemp.getInvoiceNo())
        .append(CalendarUtil.toString(apInvoiceTemp.getMonth()))
        .append(apInvoiceTemp.getClaimType())
        .toString();
    }
	
	/**
	 * Scale BigDecimal in 2 with down.
	 */
	private BigDecimal getScaleBigDecimal(BigDecimal value){
        return NumberUtil.getBigDecimalScaleDown(2, value);
    }
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.arInvoice", new Object[]{}, locale);
	}
	
	class Column {
	    static final int PAYER_NAME = 0;
        static final int INVOICE_NO = 1;
        static final int INVOICE_ISSUED_DATE = 2;
        static final int MONTH = 3;
        static final int CLAIM_TYPE = 4;
        static final int ORIGINAL_CURRENCY = 5;
        static final int FX_RATE = 6;
        static final int ACCOUNT_RECEIVABLE_CODE = 7;
        static final int ACCOUNT_RECEIVABLE_NAME = 8;
        static final int DESCRIPTION = 9;
        static final int ACCOUNT_CODE = 10;
        static final int ACCOUNT_NAME = 11;
        static final int PROJECT_CODE = 12;
        static final int PROJECT_NAME = 13;
        static final int PROJECT_DESCRIPTION = 14;
        static final int APPROVAL_CODE = 15;
        /**
         *  Amount Excluded GST (Original)
         */
        static final int EXCLUDE_GST_ORIGINAL_AMOUNT = 16; 
        static final int GST_TYPE = 17;
        static final int GST_RATE = 18;
	}
	
	enum Mode {
	    MERGE, DELETE;
	}
	
	class Message {
	    static final String NOT_FOUND_DB = "not found in master";
	    static final String WRONG_FORMAT_DATE = "wrong format(ex: dd/mm/yyyy)";
	    static final String LESS_THAN_ZERO = "less than zero";
	    static final String REQUIRED = "required";
	    static final String NOT_MATCH = "not match";
	    static final String NOT_NULL_DATA = "not null";
	}

}
