package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ClaimMasterImportComponent.Column.CODE;
import static com.ttv.cashflow.component.ClaimMasterImportComponent.Column.NAME;

import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.service.ClaimTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class ClaimMasterImportComponent extends ImportComponent {
	private static final Logger logger = Logger.getLogger("CLAIM TYPE MASTER");
	
	@Autowired
	private ClaimTypeMasterDAO claimTypeMasterDAO;
	
	@Autowired
	private ClaimTypeMasterService claimTypeMasterService;

	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					ClaimTypeMaster claim = new ClaimTypeMaster();
					claim.setCode(formatter.formatCellValue(row.getCell(CODE)));
					claim.setName(formatter.formatCellValue(row.getCell(NAME)));
					
					//save /update claim type master
					claimTypeMasterService.saveClaimTypeMaster(claim);
					nRowSuccess++;
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
		logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
		logHelper.close();

		return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					String code = formatter.formatCellValue(row.getCell(CODE));
					ClaimTypeMaster claim = claimTypeMasterDAO.findClaimTypeMasterByCode(code);
					
					if(claim != null){
						claimTypeMasterService.deleteClaimTypeMaster(claim);
						nRowSuccess++;
					} else {
					    logHelper.writeWarningLog(nRow, "Code", "not exist DB");
					    nRowFail++;
					}
					
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.claimTypeMaster", new Object[]{}, locale);
	}
	
	class Column {
		static final int CODE = 0;
		static final int NAME = 1;
	}

}
