package com.ttv.cashflow.component;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dto.PayrollPaymentDTO;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class PayrollExportComponent extends ExportComponent {
    
    DataFormatter df = new DataFormatter();
    
    @Autowired
    private PayrollDAO payrollDAO;
    
    @Autowired
    private PayrollViewerExportComponent payrollViewerExportComponent;
	
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<String> payrollNoList  = getListPayrollNo(request);
        //listPayrollNo.sort((o1, o2) -> o1.compareTo(o2));

        try {
            
            payrollViewerExportComponent.doExport(workbook, payrollNoList);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private List<String> getListPayrollNo(HttpServletRequest request) {
        
        Integer status = NumberUtils.toInt(request.getParameter("status"));
		String fromPayrollNo = StringUtils.stripToEmpty(request.getParameter("fromPayrollNo"));
		String toPayrollNo = StringUtils.stripToEmpty(request.getParameter("toPayrollNo"));
		
		String fromMonth = StringUtils.stripToEmpty(request.getParameter("fromMonth"));
		String toMonth = StringUtils.stripToEmpty(request.getParameter("toMonth"));
		Calendar calendarMonthFrom = CalendarUtil.getCalendar(fromMonth, "MM/yyyy");
		Calendar calendarMonthTo = CalendarUtil.getCalendar(toMonth, "MM/yyyy");
		
		//
        List<PayrollPaymentDTO> listData = payrollDAO.findPayrollPaging(-1, -1, calendarMonthFrom, calendarMonthTo, fromPayrollNo, toPayrollNo, -1, null, status);
        
        //
        List<String> listPayrollNo = new ArrayList<>();
        for (PayrollPaymentDTO data : listData) {
        	if(!listPayrollNo.contains(data.getPayrollNo())){
        		listPayrollNo.add(data.getPayrollNo());
        	}
        }
        
        return listPayrollNo;
    }
    
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.payroll", new Object[]{}, locale);
	}

}

