package com.ttv.cashflow.component;

import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.ttv.cashflow.helper.LogHelper;
import com.ttv.cashflow.util.Constant.IMPORT_MODE;
import com.ttv.cashflow.vo.ImportReturn;

public abstract class ImportComponent {

    public static final DataFormatter DATA_FORMATTER = new DataFormatter();

	@Autowired
	protected LogHelper logHelper;
	
	@Autowired
    protected MessageSource messageSource;
	
	private String logPath;

	public abstract ImportReturn doMerge(Workbook workbook);

	public abstract ImportReturn doUpdate(Workbook workbook);

	public abstract ImportReturn doReplace(Workbook workbook);

	public abstract ImportReturn doDelete(Workbook workbook);
	
	
	protected ImportReturn getImportReturn(IMPORT_MODE mode, int nRecordSuccess, int nRecordFail){
	    return new ImportReturn(getName(), mode.getValue(), nRecordSuccess, nRecordFail);
	}
	
    protected ImportReturn getImportReturn(IMPORT_MODE mode, int nRecordSuccess, int nRowSuccess, int nRowFail) {
        return new ImportReturn(getName(), mode.getValue(), nRecordSuccess, nRowSuccess, nRowFail);
    }
	
	public String getName(){
	    return "IMPORT";
	}
	
	public String getLogPath() {
		return logPath;
	}
	
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	
	class ErrorMsg {
	    static final String NOT_FOUND_DB = "not found in master";
	    static final String WRONG_FORMAT_DATE = "wrong format(ex: dd/mm/yyyy)";
	}
	
	/**
	 * Get real number.
	 * @throws NumberFormatException throw exception when data is not number.
	 */
    public BigDecimal getBigDecimal(Cell cell) throws NumberFormatException {

        if (null != cell && cell.getCellTypeEnum() != CellType.BLANK) {
            if (cell.getCellTypeEnum() == CellType.NUMERIC || cell.getCellTypeEnum() == CellType.FORMULA) {
                try {
                    return BigDecimal.valueOf(cell.getNumericCellValue());
                } catch (Exception e) {
                    throw new NumberFormatException();
                }
            }
        }
        return BigDecimal.ZERO;
    }
	
	public boolean isRowEmpty(Row row) {
		for (int cellNo = row.getFirstCellNum(); cellNo < row.getLastCellNum(); cellNo++) {
			Cell cell = row.getCell(cellNo);
			if (cell != null && cell.getCellTypeEnum() != CellType.BLANK) {
				return false;
			}
		}
		return true;
	}
	
}
