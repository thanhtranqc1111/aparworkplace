package com.ttv.cashflow.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class PaymentExportComponent extends ExportComponent {

	
	@Autowired
	private PaymentOrderService paymentService;

	@Autowired
	private PaymentOrderDAO paymentOrderDAO;
	
	@Autowired
	private AccountPayableViewerExportComponent accountPayableViewerExportComponent;

	@Override
	public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
		int success = 0, unSuccess = 0;
		
        List<String> paymentOrderNoList = getPaymentOrderNo(request);
        
		try {
			
		    accountPayableViewerExportComponent.doExportForPayment(workbook, paymentOrderNoList);

			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
			workbook.write(outputStream);
			workbook.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return getExportReturn(success, unSuccess);
	}
    
	private List<String> getPaymentOrderNo(HttpServletRequest request) {
	    
	    
	    List<String> paymentOrderNoList = new ArrayList<>();
	   
	    //parameter for search
        String fromDate = StringUtils.stripToEmpty(request.getParameter("fromDate"));
        String toDate = StringUtils.stripToEmpty(request.getParameter("toDate"));
        String bankRef = StringUtils.stripToEmpty(request.getParameter("bankRef"));
        String paymentOrderNoFrom = StringUtils.stripToEmpty(request.getParameter("paymentOrderNoFrom"));
        String paymentOrderNoTo = StringUtils.stripToEmpty(request.getParameter("paymentOrderNoTo"));
        String status = StringUtils.stripToEmpty(request.getParameter("status"));
        
        List<PaymentInvoice> paymentInvoiceList = paymentOrderDAO.searchPaymentOrder(-1, -1, fromDate, toDate, bankRef, paymentOrderNoFrom, paymentOrderNoTo, status, -1, null);
        
        for(PaymentInvoice paymentInvoice : paymentInvoiceList){
            paymentOrderNoList.add(paymentInvoice.getPaymentOrderNo());
        }
        
        return paymentOrderNoList;
	    
	}
	
	public String getName() {
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.paymentOrder", new Object[]{}, locale);
	}
}
