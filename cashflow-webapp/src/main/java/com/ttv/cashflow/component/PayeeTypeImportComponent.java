package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.AccountImportComponent.Column.CODE;
import static com.ttv.cashflow.component.PayeeTypeImportComponent.Column.*;

import java.util.Locale;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.service.PayeePayerTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class PayeeTypeImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("PAYEE TYPE");
    
    private DataFormatter df = new DataFormatter();
    
    @Autowired
    private PayeePayerTypeMasterService payeePayerTypeMasterService;
    
    @Autowired
    private PayeePayerTypeMasterDAO payeePayerTypeMasterDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
	    Sheet sheet = workbook.getSheetAt(0);
	    
	    for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {
                    PayeePayerTypeMaster master = new PayeePayerTypeMaster();
                    
                    String code = df.formatCellValue(row.getCell(CODE));
                    
                    //Ignore row invalid
                    if(StringUtils.isEmpty(code)) continue;
                    
                    master.setCode(code);
                    master.setName(df.formatCellValue(row.getCell(NAME)));
                    
                    payeePayerTypeMasterService.savePayeePayerTypeMaster(master);
                    
                    nRowSuccess++;

                } catch (Exception e) {
                    e.printStackTrace();

                    nRowFail++;
                }
            }
	    }
	    
	    // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
        int nRowSuccess = 0, nRowFail = 0;
        
        Sheet sheet = workbook.getSheetAt(0);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);

            try {
                String code = df.formatCellValue(row.getCell(CODE));
                
                //Ignore row invalid
                if(StringUtils.isEmpty(code)) continue;
                
                PayeePayerTypeMaster master = payeePayerTypeMasterDAO.findPayeePayerTypeMasterByPrimaryKey(code);

                if (null != master) {
                    payeePayerTypeMasterService.deletePayeePayerTypeMaster(master);
                    nRowSuccess++;
                } else {
                    logHelper.writeWarningLog(i, "Code", "not exist DB");
                    nRowFail++;
                }

            } catch (Exception e) {
                e.printStackTrace();
                nRowFail++;
            }
        }

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.payeeTypeMaster", new Object[]{}, locale);
    }
	
	class Column {
	    static final int CODE = 0;
        static final int NAME = 1;
	}
	

}
