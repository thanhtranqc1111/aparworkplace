package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ApprovalTypeSalesMasterImportComponent.Column.CODE;
import static com.ttv.cashflow.component.ApprovalTypeSalesMasterImportComponent.Column.NAME;

import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.ApprovalTypeSalesMasterDAO;
import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;
import com.ttv.cashflow.service.ApprovalTypeSalesMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class ApprovalTypeSalesMasterImportComponent extends ImportComponent {
	private static final Logger logger = Logger.getLogger("APPROVAL TYPE MASTER");
	
	@Autowired
	private ApprovalTypeSalesMasterDAO approvalTypeMasterDAO;
	
	@Autowired
	private ApprovalTypeSalesMasterService approvalTypeMasterService;

	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					ApprovalTypeSalesMaster approval = new ApprovalTypeSalesMaster();
					approval.setCode(formatter.formatCellValue(row.getCell(CODE)));
					approval.setName(formatter.formatCellValue(row.getCell(NAME)));
					
					//save /update Approval Type master
					approvalTypeMasterService.saveApprovalTypeSalesMaster(approval);;
					nRowSuccess++;
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
		Row row = null;
		Sheet paymentSheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();

		Iterator<Row> iterator = paymentSheet.iterator();
		int nRow = 1;
		while (iterator.hasNext()) {
			row = iterator.next();
			if (nRow > 1) {
				try {
					if (row.getCell(CODE).getCellTypeEnum() == CellType.BLANK) {
					    logHelper.writeWarningLog(nRow, "Code", "required");
					    nRowFail++;
						nRow++;
						continue;
					}
					String code = formatter.formatCellValue(row.getCell(CODE));
					ApprovalTypeSalesMaster approval = approvalTypeMasterDAO.findApprovalTypeSalesMasterByCode(code);
					
					if(approval != null){
						approvalTypeMasterService.deleteApprovalTypeSalesMaster(approval);
						nRowSuccess++;
					} else {
					    logHelper.writeWarningLog(nRow, "Code", "not exist DB");
					    nRowFail++;
					}
					
				} catch (Exception e) {
					nRowFail++;
					e.printStackTrace();
				}
			}
			nRow++;
		}
		// write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.approvalTypeMaster", new Object[]{}, locale);
	}
	
	class Column {
		static final int CODE = 0;
		static final int NAME = 1;
	}

}
