package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.ACCOUNT_CODE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.ACCOUNT_NAME;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.ACCOUNT_PAYABLE_CODE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.ACCOUNT_PAYABLE_NAME;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.APPROVAL_CODE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.CLAIM_TYPE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.DESCRIPTION;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.EXCLUDE_GST_ORIGINAL_AMOUNT;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.FX_RATE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.GST_RATE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.GST_TYPE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.INCLUDED_GST_CONVERTED_AMOUNT;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.INVOICE_ISSUED_DATE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.INVOICE_NO;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.MONTH;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.ORIGINAL_CURRENCY;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PAYEE_NAME;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PAYMENT_TERM;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PO_NO;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PROJECT_DESCRIPTION;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.SCHEDULED_PAYMENT_DATE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.TOTAL_EXCLUDE_GST_ORIGINAL_AMOUNT;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.TOTAL_INCLUDED_GST_CONVERTED_AMOUNT;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.WRONG_FORMAT_DATE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PROJECT_NAME;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Column.PROJECT_CODE;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_FOUND_DB;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.REQUIRED;
import static com.ttv.cashflow.component.ApInvoiceImportComponent.Message.NOT_NULL_DATA;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;
import com.ttv.cashflow.dao.ApInvoiceTempDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;
import com.ttv.cashflow.domain.ApInvoiceTemp;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.service.ApInvoiceAccountDetailsService;
import com.ttv.cashflow.service.ApInvoiceClassDetailsService;
import com.ttv.cashflow.service.ApInvoicePaymentStatusService;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.service.ApInvoiceTempService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class ApInvoiceImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("AP INVOICE");
    private static final org.slf4j.Logger APP_LOGGER = LoggerFactory.getLogger(ApInvoiceImportComponent.class);
    
    private DataFormatter df = new DataFormatter();
    
    @Autowired
    private ApInvoiceTempDAO apInvoiceTempDAO;
    
    @Autowired
    private ApInvoiceTempService apInvoiceTempService;
    
    @Autowired
    private ApInvoiceService apInvoiceService;
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private ApInvoiceAccountDetailsService apInvoiceAccountDetailsService;
    
    @Autowired
    private ApInvoiceClassDetailsService apInvoiceClassDetailsService;
    
    @Autowired
    private PayeePayerMasterDAO payeePayerMasterDAO;
    
    @Autowired
    private PayeePayerTypeMasterDAO payeePayerTypeMasterDAO;
    
    @Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
    
    @Autowired
    private AccountMasterDAO accountMasterDAO;
    
    @Autowired
    private ProjectMasterDAO projectMasterDAO;
    
    @Autowired
    private ApInvoicePaymentStatusDAO apInvoicePaymentStatusDAO;
    
    @Autowired
    private ApInvoicePaymentStatusService apInvoicePaymentStatusService;
    
    @Autowired
    private ApprovalPurchaseRequestDAO approvalDAO;
    
    @Override
    public ImportReturn doMerge(Workbook workbook) {
        //create log file
        logHelper.init(logger, this.getLogPath());
        
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRowSuccess = new MutableInt(0);
        MutableInt nRowFail = new MutableInt(0);
        
        Sheet sheet = workbook.getSheetAt(0);
        
        //
        apInvoiceTempDAO.truncate();
        
        //SAVE TO AP INVOICE TEMP
        for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {

                    doSaveApInvoiceTemp(row, Mode.MERGE, nRowSuccess, nRowFail);

                } catch (Exception e) {
                    APP_LOGGER.error(getName(), e);
                }
            }
        }
        
        //GET DATA TABLE TEMP AND IMPORT
        doImportData(nRecordSuccess, nRowSuccess, nRowFail);
        
        //truncate table temporary
        apInvoiceTempDAO.truncate();
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
        logHelper.close();

        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
    }

    private void doSaveApInvoiceTemp(Row row, Mode mode, MutableInt nRecordSuccess, MutableInt nRecordFail) {
    	// check null records
        if(isRowEmpty(row)){
        	return;
        }
    	
        ApInvoiceTemp apInvoiceTemp = new ApInvoiceTemp();
        
        int nRowData = row.getRowNum() + 1;
        
        //set payee
        String payeeName = df.formatCellValue(row.getCell(PAYEE_NAME)).trim();
        String payeePaymentTerm = "";

        if(StringUtils.isEmpty(payeeName)){
        	logHelper.writeWarningLog(nRowData, "Transaction Party Master", NOT_NULL_DATA);
    		nRecordFail.increment();
    		return;
    		
        } else {
        	Set<PayeePayerMaster> payeeSet = payeePayerMasterDAO.findPayeePayerMasterByName(payeeName);
            Iterator<PayeePayerMaster> payeeIter = payeeSet.iterator();
            
            if (payeeIter.hasNext()) {
                PayeePayerMaster payee = payeeIter.next();
                apInvoiceTemp.setPayeeCode(payee.getCode());
                apInvoiceTemp.setPayeeName(payee.getName());
                
                payeePaymentTerm = payee.getPaymentTerm();
                
                //set payeeTypeName
                PayeePayerTypeMaster payeeType = payeePayerTypeMasterDAO.findPayeePayerTypeMasterByPrimaryKey(payee.getTypeCode());
                if (Objects.nonNull(payeeType)) {
                    apInvoiceTemp.setPayeeTypeName(payeeType.getName());
                }
            } else {
                logHelper.writeWarningLog(nRowData, "Transaction Party Master", payeeName, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
            }
        }
        
        String invoiceNo = df.formatCellValue(row.getCell(INVOICE_NO));
        apInvoiceTemp.setInvoiceNo(invoiceNo);
        
        //invoiceIssuedDate
        Calendar invoiceIssuedDate = null;
        String invoiceIssuedDateAsStr = df.formatCellValue(row.getCell(INVOICE_ISSUED_DATE));
        if(hasText(invoiceIssuedDateAsStr)){
            
            invoiceIssuedDate = CalendarUtil.getCalendar(invoiceIssuedDateAsStr);
            
            //if invoiceIssuedDate is have different format "dd/MM/yyyy", return NULL.
            if (isNull(invoiceIssuedDate)) {
                logHelper.writeWarningLog(nRowData, "Invoice Issued Date", df.formatCellValue(row.getCell(INVOICE_ISSUED_DATE)), WRONG_FORMAT_DATE);
                nRecordFail.increment();
                return;
            }
        }
        apInvoiceTemp.setInvoiceIssuedDate(invoiceIssuedDate);
        
        // month
        Calendar month = null;
        if (row.getCell(MONTH).getCellTypeEnum() != CellType.BLANK) {
            if (row.getCell(MONTH).getCellTypeEnum() == CellType.NUMERIC) {
                month = CalendarUtil.getCalendar(row.getCell(MONTH).getDateCellValue());
            } else {
                logHelper.writeWarningLog(nRowData, "Month", df.formatCellValue(row.getCell(MONTH)), WRONG_FORMAT_DATE);
                nRecordFail.increment();
                return;
            }
        } else {
        	logHelper.writeWarningLog(nRowData, "Month", REQUIRED);
        	nRecordFail.increment();
            return;
        }
        apInvoiceTemp.setMonth(month);
        
        //claimType
        String claimType = df.formatCellValue(row.getCell(CLAIM_TYPE)).trim();

        Iterator<ClaimTypeMaster> claimTypeIter = claimTypeMasterDAO.findClaimTypeMasterByName(claimType).iterator();
        if (claimTypeIter.hasNext()) {
            ClaimTypeMaster claimTypeMaster = claimTypeIter.next();
            apInvoiceTemp.setClaimType(claimTypeMaster.getCode());
        } else {
            logHelper.writeWarningLog(nRowData, "Claim Type", df.formatCellValue(row.getCell(CLAIM_TYPE)), NOT_FOUND_DB);
            nRecordFail.increment();
            return;
        }
        
        apInvoiceTemp.setOriginalCurrency(df.formatCellValue(row.getCell(ORIGINAL_CURRENCY)));
        apInvoiceTemp.setTotalExcludeGstOriginalAmount(getBigDecimal(row.getCell(TOTAL_EXCLUDE_GST_ORIGINAL_AMOUNT)));
        apInvoiceTemp.setFxRate(getBigDecimal(row.getCell(FX_RATE)));
        apInvoiceTemp.setGstType(df.formatCellValue(row.getCell(GST_TYPE)));
        apInvoiceTemp.setGstRate(getBigDecimal(row.getCell(GST_RATE)));
        apInvoiceTemp.setTotalIncludedGstConvertedAmount(getBigDecimal(row.getCell(TOTAL_INCLUDED_GST_CONVERTED_AMOUNT)));
        
        //Set get receivable account /code
        String payableName = df.formatCellValue(row.getCell(ACCOUNT_PAYABLE_NAME)).trim();
        String payableCode = df.formatCellValue(row.getCell(ACCOUNT_PAYABLE_CODE)).trim();

    	if(StringUtils.isEmpty(payableCode)){
    		if(StringUtils.isEmpty(payableName)){
    			logHelper.writeWarningLog(nRowData, "Account Payable Name or Account Payable Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
    			Set<AccountMaster> payableSet = accountMasterDAO.findAccountMasterByName(payableName);
            	Iterator<AccountMaster> payableAccountMasterIter = payableSet.iterator();
            	
            	if(payableAccountMasterIter.hasNext()){
            		AccountMaster account = payableAccountMasterIter.next();
                	apInvoiceTemp.setAccountPayableCode(account.getCode());
                	apInvoiceTemp.setAccountPayableName(account.getName());
            	} else {
            		logHelper.writeWarningLog(nRowData, "Account Payable Name", payableName, NOT_FOUND_DB);
                    nRecordFail.increment();
                    return;
            	}
    		}
    	} else {
    		AccountMaster payableAccountMaster = accountMasterDAO.findAccountMasterByCode(payableCode);
    		if(payableAccountMaster != null) {
    			if(StringUtils.isEmpty(payableName)){
        			apInvoiceTemp.setAccountPayableCode(payableAccountMaster.getCode());
                	apInvoiceTemp.setAccountPayableName(payableAccountMaster.getName());
        		} else {
        			if(payableAccountMaster.getName().equals(payableName)){
        				apInvoiceTemp.setAccountPayableCode(payableAccountMaster.getCode());
                    	apInvoiceTemp.setAccountPayableName(payableAccountMaster.getName());
        			} else {
        				logHelper.writeErrorLog(nRowData, "Account Payable Code", payableCode, "Account Payable Name", payableName);
            			nRecordFail.increment();
            			return;
        			}
        		}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Account Payable Code", payableCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
        
        //paymentTerm
        String paymentTerm = df.formatCellValue(row.getCell(PAYMENT_TERM));
        
        if (hasText(paymentTerm)) {
            apInvoiceTemp.setPaymentTerm(paymentTerm);
        } else {
            apInvoiceTemp.setPaymentTerm(payeePaymentTerm);
        }
        
        //scheduledPaymentDate
        String scheduledPaymentDateAsStr = df.formatCellValue(row.getCell(SCHEDULED_PAYMENT_DATE));
        
        if (hasText(scheduledPaymentDateAsStr)) {
            Calendar scheduledPaymentDate = CalendarUtil.getCalendar(scheduledPaymentDateAsStr);
            if (nonNull(scheduledPaymentDate)) {
                apInvoiceTemp.setScheduledPaymentDate(scheduledPaymentDate);
            } else {
                logHelper.writeWarningLog(nRowData, "Scheduled Payment Date", df.formatCellValue(row.getCell(SCHEDULED_PAYMENT_DATE)), WRONG_FORMAT_DATE);
                nRecordFail.increment();
                return;
            }
        } else {
            autoCalculateSchedulePaymentDate(apInvoiceTemp);
        }
        
        //
        apInvoiceTemp.setDescription(df.formatCellValue(row.getCell(DESCRIPTION)));
        
        // set account
        String accountName = df.formatCellValue(row.getCell(ACCOUNT_NAME)).trim();
        String accountCode = df.formatCellValue(row.getCell(ACCOUNT_CODE)).trim();
        
        if(StringUtils.isEmpty(accountCode)){
    		if(StringUtils.isEmpty(accountName)){
    			logHelper.writeWarningLog(nRowData, "Account Name or Account Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
    			Set<AccountMaster> accountSet = accountMasterDAO.findAccountMasterByName(accountName);
		        Iterator<AccountMaster> accountIter = accountSet.iterator();
		        if(accountIter.hasNext()){
		        	AccountMaster account = accountIter.next();
		        	apInvoiceTemp.setAccountCode(account.getCode());
		        	apInvoiceTemp.setAccountName(account.getName());
		        	apInvoiceTemp.setAccountType(account.getType());
		        } else {
		        	logHelper.writeWarningLog(nRowData, "Account Name", accountCode, NOT_FOUND_DB);
	                nRecordFail.increment();
	                return;
		        }
    		}
    	} else {
    		AccountMaster accountMaster = accountMasterDAO.findAccountMasterByCode(accountCode);
    		if(accountMaster != null) {
    			if(StringUtils.isEmpty(accountName)){
    				apInvoiceTemp.setAccountCode(accountMaster.getCode());
    				apInvoiceTemp.setAccountName(accountMaster.getName());
    				apInvoiceTemp.setAccountType(accountMaster.getType());
    			} else {
    				if(accountMaster.getName().equals(accountName)){
    					apInvoiceTemp.setAccountCode(accountMaster.getCode());
    					apInvoiceTemp.setAccountName(accountMaster.getName());
    					apInvoiceTemp.setAccountType(accountMaster.getType());
    				} else {
    					logHelper.writeErrorLog(nRowData, "Account Code", accountCode, "Account Name", accountName);
    					nRecordFail.increment();
    					return;
    				}
    			}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Account Code", accountCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
        
        // set project name
        String projectCode = df.formatCellValue(row.getCell(PROJECT_CODE)).trim();
        String projectName = df.formatCellValue(row.getCell(PROJECT_NAME)).trim();
        
        if(StringUtils.isEmpty(projectCode)){
    		if(StringUtils.isEmpty(projectName)){
    			logHelper.writeWarningLog(nRowData, "Project Name or Project Code", NOT_NULL_DATA);
                nRecordFail.increment();
                return;
    		} else {
    			Set<ProjectMaster> projectSet = projectMasterDAO.findProjectMasterByName(projectName);
    	        Iterator<ProjectMaster> projectIter = projectSet.iterator();
    	        if(projectIter.hasNext()){
    	        	ProjectMaster project = projectIter.next();
    	        	apInvoiceTemp.setProjectCode(project.getCode());
    	        	apInvoiceTemp.setProjectName(project.getName());
    	        } else {
    	        	logHelper.writeWarningLog(nRowData, "Project Name", projectName, NOT_FOUND_DB);
                    nRecordFail.increment();
                    return;
    	        }
    		}
    	} else {
    		ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(projectCode);
    		if(projectMaster != null) {
    			if(StringUtils.isEmpty(projectName)){
    				apInvoiceTemp.setProjectCode(projectMaster.getCode());
    				apInvoiceTemp.setProjectName(projectMaster.getName());
    			} else {
    				if(projectMaster.getName().equals(projectName)){
    					apInvoiceTemp.setProjectCode(projectMaster.getCode());
    					apInvoiceTemp.setProjectName(projectMaster.getName());
    				} else {
    					logHelper.writeErrorLog(nRowData, "Project Code", projectCode, "Project Name", projectName);
    					nRecordFail.increment();
    					return;
    				}
    			}
    		} else {
    			logHelper.writeWarningLog(nRowData, "Project Code", projectCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
    		}
    	}
        
        apInvoiceTemp.setProjectDescription(df.formatCellValue(row.getCell(PROJECT_DESCRIPTION)));
        apInvoiceTemp.setPoNo(df.formatCellValue(row.getCell(PO_NO)));
        
        String approvalCode = df.formatCellValue(row.getCell(APPROVAL_CODE));
        if(hasText(approvalCode)){
            if(approvalDAO.findApprovalPurchaseRequestByApprovalCode(approvalCode).size() == 0){
                logHelper.writeWarningLog(nRowData, "Approval Code", approvalCode, NOT_FOUND_DB);
                nRecordFail.increment();
                return;
            } else {
                apInvoiceTemp.setIsApproval(Boolean.TRUE);
                apInvoiceTemp.setApprovalCode(approvalCode);
            }
        } else {
            apInvoiceTemp.setIsApproval(Boolean.FALSE);
            apInvoiceTemp.setApprovalCode(approvalCode);
        }
        
        apInvoiceTemp.setExcludeGstOriginalAmount(getBigDecimal(row.getCell(EXCLUDE_GST_ORIGINAL_AMOUNT)));
        apInvoiceTemp.setIncludedGstConvertedAmount(getBigDecimal(row.getCell(INCLUDED_GST_CONVERTED_AMOUNT)));
        
        //Check row is exist on database
        String importKey = getImportKey(apInvoiceTemp);
        if (mode.equals(Mode.MERGE)
                && null != apInvoiceDAO.findApInvoiceByImportKey(importKey)) {
            logHelper.writeWarningExistLog(nRowData, "Invoice No", invoiceNo);
            nRecordFail.increment();
            return;
        }
        
        apInvoiceTempService.saveApInvoiceTemp(apInvoiceTemp);
        
        //
        nRecordSuccess.increment();
    }
    
    private void doImportData(MutableInt nRecordSuccess, MutableInt nRowSuccess, MutableInt nRowFail) {
        
        List<String> listIdtemp = apInvoiceTempDAO.findListIdApInvoiceTempByHavingGroup();
        
        for(int i = 0; i < listIdtemp.size(); i++){
            String[] arrIdtemp = listIdtemp.get(i).toString().split(",");
            
            ApInvoiceTemp apInvoiceTemp = apInvoiceTempService.findApInvoiceTempByPrimaryKey(new BigInteger(arrIdtemp[0]));
            List<ApInvoiceTemp> apInvoiceTempList = apInvoiceTempDAO.findApInvoiceTempByListId(getListIdTempInBigInt(arrIdtemp));
            //apInvoiceTempList.sort((o1, o2) -> o1.getPaymentOrderNo().compareTo(o2.getPaymentOrderNo()));
            
            // check total amount of AP invoice with detail
            if(checkTotalAmoutOfAPInvoice(apInvoiceTempList, nRowSuccess, nRowFail)){
                ////SAVE APINVOICE
                ApInvoice apInvoice = new ApInvoice();
                doSaveApInvoice(apInvoice, apInvoiceTemp);
                
                nRecordSuccess.increment();
                
                ////SAVE APINVOICE DETAIL
                doSaveApInvoiceDetail(apInvoice, apInvoiceTempList);
            }
            
        }
    }

    private void doSaveApInvoice(ApInvoice apInvoice, ApInvoiceTemp apInvoiceTemp) {
        String importKey = getImportKey(apInvoiceTemp);
        
        BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
        BigDecimal excludeGstConvertedAmount = new BigDecimal(0);
        BigDecimal gstOriginalAmount = new BigDecimal(0);
        BigDecimal gstConvertedAmount = new BigDecimal(0);
        BigDecimal includeGstOriginalAmount = new BigDecimal(0);
        BigDecimal includeGstConvertedAmount = new BigDecimal(0);
        
        //calculate
        excludeGstOriginalAmount = apInvoiceTemp.getTotalExcludeGstOriginalAmount();
        excludeGstConvertedAmount = excludeGstOriginalAmount.multiply(apInvoiceTemp.getFxRate());
        gstOriginalAmount = excludeGstOriginalAmount.multiply(apInvoiceTemp.getGstRate()).divide(BigDecimal.valueOf(100));
        gstConvertedAmount = gstOriginalAmount.multiply(apInvoiceTemp.getFxRate());
        includeGstOriginalAmount = excludeGstOriginalAmount.add(gstOriginalAmount);
        includeGstConvertedAmount = apInvoiceTemp.getTotalIncludedGstConvertedAmount();
        
        //do scale
        excludeGstOriginalAmount = getScaleBigDecimal(excludeGstOriginalAmount);
        excludeGstConvertedAmount = getScaleBigDecimal(excludeGstConvertedAmount);
        gstOriginalAmount = getScaleBigDecimal(gstOriginalAmount);
        gstConvertedAmount = getScaleBigDecimal(gstConvertedAmount);
        includeGstOriginalAmount = getScaleBigDecimal(includeGstOriginalAmount);
        includeGstConvertedAmount = getScaleBigDecimal(includeGstConvertedAmount);
        
        //
        BeanUtils.copyProperties(apInvoiceTemp, apInvoice, "id");
        apInvoice.setApInvoiceNo(apInvoiceDAO.getApInvoiceNo(apInvoiceTemp.getMonth()));
        apInvoice.setOriginalCurrencyCode(apInvoiceTemp.getOriginalCurrency());
        apInvoice.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
        apInvoice.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
        apInvoice.setGstType(apInvoiceTemp.getGstType());
        apInvoice.setGstRate(apInvoiceTemp.getGstRate());
        apInvoice.setGstOriginalAmount(gstOriginalAmount);
        apInvoice.setGstConvertedAmount(gstConvertedAmount);
        apInvoice.setIncludeGstOriginalAmount(includeGstOriginalAmount);
        apInvoice.setIncludeGstConvertedAmount(includeGstConvertedAmount);
        apInvoice.setStatus(Constant.API_NOT_PAID);
        apInvoice.setImportKey(importKey);
        apInvoice.setCreatedDate(Calendar.getInstance());
        apInvoice.setModifiedDate(Calendar.getInstance());
        
        apInvoiceService.saveApInvoice(apInvoice);
    }
    
    private void doSaveApInvoiceDetail(ApInvoice apInvoice, List<ApInvoiceTemp> apInvoiceTempList) {
        List<String> accountNameVisited = new ArrayList<>();
        
        for(ApInvoiceTemp apInvoiceTemp : apInvoiceTempList){
            
            String accountName = apInvoiceTemp.getAccountName();
            
            if (accountNameVisited.contains(accountName)) {
                continue;
            }
            accountNameVisited.add(accountName);
            
            /////Do save invoice account detail/////
            ApInvoiceAccountDetails apInvoiceAccountDetails = new ApInvoiceAccountDetails();
            
            BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
            BigDecimal excludeGstConvertedAmount = new BigDecimal(0);
            BigDecimal gstOriginalAmount = new BigDecimal(0);
            BigDecimal gstConvertedAmount = new BigDecimal(0);
            BigDecimal includeGstOriginalAmount = new BigDecimal(0);
            BigDecimal includeGstConvertedAmount = new BigDecimal(0);
            
            for(ApInvoiceTemp apInvoiceTemp2 : apInvoiceTempList){
                if(accountName.equals(apInvoiceTemp2.getAccountName())){
                    BigDecimal fxRateTmp = apInvoiceTemp2.getFxRate();
                    BigDecimal gstRateTmp = apInvoiceTemp2.getGstRate();
                    BigDecimal excludeGstOriginalAmountTmp = apInvoiceTemp2.getExcludeGstOriginalAmount();
                    BigDecimal excludeGstConvertedAmountTmp =  excludeGstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal gstOriginalAmountTmp = excludeGstOriginalAmountTmp.multiply(gstRateTmp).divide(BigDecimal.valueOf(100));
                    BigDecimal gstConvertedAmountTmp = gstOriginalAmountTmp.multiply(fxRateTmp);
                    
                    //
                    excludeGstOriginalAmount = excludeGstOriginalAmount.add(excludeGstOriginalAmountTmp);
                    excludeGstConvertedAmount = excludeGstConvertedAmount.add(excludeGstConvertedAmountTmp);
                    gstOriginalAmount = gstOriginalAmount.add(gstOriginalAmountTmp);
                    gstConvertedAmount = gstConvertedAmount.add(gstConvertedAmountTmp);
                    includeGstOriginalAmount = includeGstOriginalAmount.add(excludeGstOriginalAmountTmp.add(gstOriginalAmountTmp));
                    includeGstConvertedAmount = includeGstConvertedAmount.add(apInvoiceTemp2.getIncludedGstConvertedAmount());
                }
            }
            
            //do scale
            excludeGstOriginalAmount = getScaleBigDecimal(excludeGstOriginalAmount);
            excludeGstConvertedAmount = getScaleBigDecimal(excludeGstConvertedAmount);
            gstOriginalAmount = getScaleBigDecimal(gstOriginalAmount);
            gstConvertedAmount = getScaleBigDecimal(gstConvertedAmount);
            includeGstOriginalAmount = getScaleBigDecimal(includeGstOriginalAmount);
            includeGstConvertedAmount = getScaleBigDecimal(includeGstConvertedAmount);
            
            //
            BeanUtils.copyProperties(apInvoiceTemp, apInvoiceAccountDetails);
            apInvoiceAccountDetails.setApInvoice(apInvoice);
            apInvoiceAccountDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
            apInvoiceAccountDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
            apInvoiceAccountDetails.setGstOriginalAmount(gstOriginalAmount);
            apInvoiceAccountDetails.setGstConvertedAmount(gstConvertedAmount);
            apInvoiceAccountDetails.setIncludeGstOriginalAmount(includeGstOriginalAmount);
            apInvoiceAccountDetails.setIncludeGstConvertedAmount(includeGstConvertedAmount);
            apInvoiceAccountDetails.setCreatedDate(Calendar.getInstance());
            apInvoiceAccountDetails.setModifiedDate(Calendar.getInstance());
            
            apInvoiceAccountDetailsService.saveApInvoiceAccountDetails(apInvoiceAccountDetails);
            
            /////Do save invoice class detail/////
            for(ApInvoiceTemp apInvoiceTemp2 : apInvoiceTempList){
                if(accountName.equals(apInvoiceTemp2.getAccountName())){
                    ApInvoiceClassDetails apInvoiceClassDetails = new ApInvoiceClassDetails();
                    
                    BigDecimal fxRateTmp = apInvoiceTemp2.getFxRate();
                    BigDecimal gstRateTmp = apInvoiceTemp2.getGstRate();
                    BigDecimal excludeGstOriginalAmountTmp = apInvoiceTemp2.getExcludeGstOriginalAmount();
                    BigDecimal excludeGstConvertedAmountTmp = excludeGstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal gstOriginalAmountTmp = excludeGstOriginalAmountTmp.multiply(gstRateTmp).divide(BigDecimal.valueOf(100));
                    BigDecimal gstConvertedAmountTmp = gstOriginalAmountTmp.multiply(fxRateTmp);
                    BigDecimal includeGstOriginalAmountTmp = excludeGstOriginalAmountTmp.add(gstOriginalAmountTmp);
                    BigDecimal includeGstConvertedAmountTmp = apInvoiceTemp2.getIncludedGstConvertedAmount();
                    
                    //do scale
                    excludeGstOriginalAmountTmp = getScaleBigDecimal(excludeGstOriginalAmountTmp);
                    excludeGstConvertedAmountTmp = getScaleBigDecimal(excludeGstConvertedAmountTmp);
                    gstOriginalAmountTmp = getScaleBigDecimal(gstOriginalAmountTmp);
                    gstConvertedAmountTmp = getScaleBigDecimal(gstConvertedAmountTmp);
                    includeGstOriginalAmountTmp = getScaleBigDecimal(includeGstOriginalAmountTmp);
                    includeGstConvertedAmountTmp = getScaleBigDecimal(includeGstConvertedAmountTmp);
                    
                    //
                    BeanUtils.copyProperties(apInvoiceTemp2, apInvoiceClassDetails);
                    
                    apInvoiceClassDetails.setClassCode(apInvoiceTemp2.getProjectCode());
                    apInvoiceClassDetails.setClassName(apInvoiceTemp2.getProjectName());
                    apInvoiceClassDetails.setDescription(apInvoiceTemp2.getProjectDescription());
                    apInvoiceClassDetails.setApInvoiceAccountDetails(apInvoiceAccountDetails);
                    apInvoiceClassDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmountTmp);
                    apInvoiceClassDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmountTmp);
                    apInvoiceClassDetails.setGstOriginalAmount(gstOriginalAmountTmp);
                    apInvoiceClassDetails.setGstConvertedAmount(gstConvertedAmountTmp);
                    apInvoiceClassDetails.setIncludeGstOriginalAmount(includeGstOriginalAmountTmp);
                    apInvoiceClassDetails.setIncludeGstConvertedAmount(includeGstConvertedAmountTmp);
                    apInvoiceClassDetails.setCreatedDate(Calendar.getInstance());
                    apInvoiceClassDetails.setModifiedDate(Calendar.getInstance());
                    
                    apInvoiceClassDetailsService.saveApInvoiceClassDetails(apInvoiceClassDetails);
                }
            }
            
        }
    }
    
    /**
     * Check total amount should be sum of amount of detail
     */
    private boolean checkTotalAmoutOfAPInvoice(List<ApInvoiceTemp> apInvoiceTempList, MutableInt nRecordSuccess, MutableInt nRecordFail){
        int count = 0;
        boolean hasError = false;
        
        ApInvoiceTemp apInvoiceTemp = apInvoiceTempList.get(0);
        
        BigDecimal totalExcludeGstOriginalAmount = apInvoiceTemp.getTotalExcludeGstOriginalAmount();
        BigDecimal totalIncludedGstConvertedAmount = apInvoiceTemp.getTotalIncludedGstConvertedAmount();
        
        BigDecimal totalExcludeGstOriginalAmountInDetail = BigDecimal.ZERO;
        BigDecimal totalIncludedGstConvertedAmountInDetail = BigDecimal.ZERO;
        
        for(ApInvoiceTemp apInvoiceTemp2 : apInvoiceTempList){
            totalExcludeGstOriginalAmountInDetail = totalExcludeGstOriginalAmountInDetail.add(apInvoiceTemp2.getExcludeGstOriginalAmount());
            totalIncludedGstConvertedAmountInDetail = totalIncludedGstConvertedAmountInDetail.add(apInvoiceTemp2.getIncludedGstConvertedAmount());
            
            count++;
        }
        
        if (!getScaleBigDecimal(totalExcludeGstOriginalAmount).equals(getScaleBigDecimal(totalExcludeGstOriginalAmountInDetail))) {
            logHelper.writeErrorLog("Inovice No : " + apInvoiceTemp.getInvoiceNo() 
                                        + ", Total Amount Excluded GST (Orignal) should be equal sum of Amount Excluded GST (Original)");
            hasError = true;
        }
        
        if (!getScaleBigDecimal(totalIncludedGstConvertedAmount).equals(getScaleBigDecimal(totalIncludedGstConvertedAmountInDetail))) {
            logHelper.writeErrorLog("Inovice No : " + apInvoiceTemp.getInvoiceNo() 
                                        + ", Total Amount Included GST (Converted) should be equal sum of Amount Included GST (Converted)");
            hasError = true;
        }
        
        if (hasError) {
            nRecordFail.add(count);
            nRecordSuccess.subtract(count);
        }
        
        return !hasError;
    }
    
    /**
     * Set scheduledPaymentDate for ApInvoiceTemp flowing InvoiceIssuedDate + paymentTerm
     */
    private void autoCalculateSchedulePaymentDate(ApInvoiceTemp apInvoiceTemp) {
        Calendar scheduledPaymentDate = null;
        String paymentTerm = apInvoiceTemp.getPaymentTerm();
        Integer paymentTermAsInt = Integer.valueOf(0);
        
        try {
            paymentTermAsInt = Integer.valueOf(paymentTerm);
        } catch (NumberFormatException nfe) {
            APP_LOGGER.error(getName(), nfe);
        }

        if (hasText(paymentTerm)) {
            // isue + term
            Calendar invoiceIssuedDate = apInvoiceTemp.getInvoiceIssuedDate();

            if (nonNull(invoiceIssuedDate)) {
                scheduledPaymentDate = (Calendar) invoiceIssuedDate.clone();
                scheduledPaymentDate.add(Calendar.DAY_OF_MONTH, paymentTermAsInt);
                
            }
        }
        
        apInvoiceTemp.setScheduledPaymentDate(scheduledPaymentDate);
    }
    
    @Override
    public ImportReturn doUpdate(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doReplace(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doDelete(Workbook workbook) {
        //create log file
        logHelper.init(logger, this.getLogPath());
        
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRowSuccess = new MutableInt(0);
        MutableInt nRowFail = new MutableInt(0);
        
        Sheet sheet = workbook.getSheetAt(0);
        
        //
        apInvoiceTempDAO.truncate();
        
        //SAVE TO AP INVOICE TEMP
        for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            
            try {
                
                doSaveApInvoiceTemp(row, Mode.DELETE, nRowSuccess, nRowFail);
                
            } catch (Exception e) {
                APP_LOGGER.error(getName(), e);
            }
        }
        
        //DELETE AP INVOICE AND DETAIL RELATE
        List<String> listIdtemp = apInvoiceTempDAO.findListIdApInvoiceTempByHavingGroup();
        
        for(int i = 0; i < listIdtemp.size(); i++){
            String[] arrIdtemp = listIdtemp.get(i).toString().split(",");
            
            ApInvoiceTemp apInvoiceTemp = apInvoiceTempService.findApInvoiceTempByPrimaryKey(new BigInteger(arrIdtemp[0]));
            
            String importKey = getImportKey(apInvoiceTemp);
            ApInvoice apInvoice = apInvoiceDAO.findApInvoiceByImportKey(importKey);
            
            if(apInvoice != null){
                apInvoiceService.deleteApInvoice(apInvoice);
                
                nRecordSuccess.increment();
                
                //delete ApInvoicePaymentStatus
                ApInvoicePaymentStatus apPaymentStatus =  apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByApInvoiceId(apInvoice.getId());
                apInvoicePaymentStatusService.deleteApInvoicePaymentStatus(apPaymentStatus);
                
            }
        }
        
        //
        apInvoiceTempDAO.truncate();
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess.intValue(), nRowSuccess.intValue(), nRowFail.intValue());
    }
    
    private List<BigInteger> getListIdTempInBigInt(String[] arrIdtemp){
        List<BigInteger> listId = new ArrayList<>();
        
        for(String id : arrIdtemp){
            listId.add(new BigInteger(id));
        }
        
        return listId;
    }
        
    private String getImportKey(ApInvoiceTemp apInvoiceTemp) {
        return new StringBuilder()
        .append(apInvoiceTemp.getPayeeName())
        .append(apInvoiceTemp.getInvoiceNo())
        .append(CalendarUtil.toString(apInvoiceTemp.getMonth()))
        .append(apInvoiceTemp.getClaimType())
        .toString();
    }
    
    /**
     * Scale BigDecimal in 2 with down.
     */
    private BigDecimal getScaleBigDecimal(BigDecimal value){
        return NumberUtil.getBigDecimalScaleDown(2, value);
    }
    
    public String getName(){
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.apInvoice", new Object[]{}, locale);
    }
    
    class Column {
        static final int PAYEE_NAME = 0;
        static final int INVOICE_NO = 1;
        static final int INVOICE_ISSUED_DATE = 2;
        static final int MONTH = 3;
        static final int CLAIM_TYPE = 4;
        static final int ORIGINAL_CURRENCY = 5;
        static final int TOTAL_EXCLUDE_GST_ORIGINAL_AMOUNT = 6;
        static final int FX_RATE = 7;
        static final int GST_TYPE = 8;
        static final int GST_RATE = 9;
        static final int TOTAL_INCLUDED_GST_CONVERTED_AMOUNT = 10;
        static final int ACCOUNT_PAYABLE_CODE = 11;
        static final int ACCOUNT_PAYABLE_NAME = 12;
        static final int PAYMENT_TERM = 13;
        static final int SCHEDULED_PAYMENT_DATE = 14;
        static final int DESCRIPTION = 15;
        static final int ACCOUNT_CODE = 16;
        static final int ACCOUNT_NAME = 17;
        static final int PROJECT_CODE = 18;
        static final int PROJECT_NAME = 19;
        static final int PROJECT_DESCRIPTION = 20;
        static final int PO_NO = 21;
        static final int APPROVAL_CODE = 22;
        static final int EXCLUDE_GST_ORIGINAL_AMOUNT = 23;
        static final int INCLUDED_GST_CONVERTED_AMOUNT = 24;

    }
    
    enum Mode {
        MERGE, DELETE;
    }
    
    class Message {
        static final String NOT_FOUND_DB = "not found in master";
        static final String REQUIRED = "required";
        static final String WRONG_FORMAT_DATE = "wrong format(ex: dd/mm/yyyy)";
        static final String NOT_NULL_DATA = "not null";
    }

}

