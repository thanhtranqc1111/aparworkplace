package com.ttv.cashflow.component;


import static com.ttv.cashflow.component.ProjectPaymentExportComponent.Column.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dto.PaymentEmployeeDTO;
import com.ttv.cashflow.dto.ProjectPaymentDTO;
import com.ttv.cashflow.service.ProjectPayrollDetailsService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.vo.ExportReturn;

@Component
@PrototypeScope
public class ProjectPaymentExportComponent extends ExportComponent {
    
    private static final String MMYYYY = "MM/yyyy";
    
    @Autowired
    private ProjectPayrollDetailsService projectPayrollDetailsService;
    
    DataFormatter df = new DataFormatter();
    
    @Override
    public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt nRecordFail = new MutableInt(0);
        
        List<ProjectPaymentDTO> projectPaymentList = getListProjectPayment(request);
        
        try {
            
            doExport(workbook, projectPaymentList);
            
            //
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
            
        } catch (Exception e) {
            nRecordFail.increment();
            e.printStackTrace();
        }

        return getExportReturn(nRecordSuccess.intValue(), nRecordFail.intValue());
    }
    
    private void doExport(Workbook workbook, List<ProjectPaymentDTO> projectPaymentList){

        DataFormat format = workbook.createDataFormat();
        
        Sheet sheet = workbook.getSheetAt(0);
        
        Font titleFont = workbook.createFont();
        titleFont.setColor(IndexedColors.WHITE.getIndex());
        
        //write data.
        Row row;
        
        int rownum = 0;
        for(ProjectPaymentDTO projectPayment : projectPaymentList){
            
            //+ new row
            row = sheet.createRow(rownum++);
            
            //1. write month
            Font fontMonth = workbook.createFont();
            fontMonth.setBold(true);
            
            CellStyle monthStyle = workbook.createCellStyle();
            setBorder(monthStyle);
            monthStyle.setFont(fontMonth);
            setFillForegroundColor(monthStyle, IndexedColors.GREY_25_PERCENT);
            
            Cell monthCell = row.createCell(MONTH);
            monthCell.setCellValue(projectPayment.getMonth());
            monthCell.setCellStyle(monthStyle);
            
            //+ new row
            //2. write title of employee and list project name
            row = sheet.createRow(rownum++);
            
            // 2.1 write title of employee
            //title EMPLOYEE_NAME
            CellStyle empNameTitleStyle = workbook.createCellStyle();
            setBorder(empNameTitleStyle);
            setFillForegroundColor(empNameTitleStyle, IndexedColors.PALE_BLUE);
            empNameTitleStyle.setFont(titleFont);
            
            Cell empNameTitleCell = row.createCell(EMPLOYEE_NAME);
            empNameTitleCell.setCellValue(Title.EMPLOYEE_NAME);
            empNameTitleCell.setCellStyle(empNameTitleStyle);
            
            //title EMP_CODE
            CellStyle empCodeTitleStyle = workbook.createCellStyle();
            setBorder(empCodeTitleStyle);
            setFillForegroundColor(empCodeTitleStyle, IndexedColors.PALE_BLUE);
            empCodeTitleStyle.setFont(titleFont);
            
            Cell empNameCodeTitleCell = row.createCell(EMPLOYEE_CODE);
            empNameCodeTitleCell.setCellValue(Title.EMPLOYEE_CODE);
            empNameCodeTitleCell.setCellStyle(empCodeTitleStyle);
            
            //title SALARY
            CellStyle salaryTitleStyle = workbook.createCellStyle();
            setBorder(salaryTitleStyle);
            setFillForegroundColor(salaryTitleStyle, IndexedColors.PALE_BLUE);
            salaryTitleStyle.setFont(titleFont);
            
            Cell salaryTitleCell = row.createCell(TOTAL_SALARY);
            salaryTitleCell.setCellValue(Title.TOTAL_SALARY);
            salaryTitleCell.setCellStyle(salaryTitleStyle);
            
            // 2.2. write list project name
            Map<String, String> mapProjectCodeAndProjectName = projectPayment.getMapProjectCodeAndProjectName();
            
            int startColumnOfProject = 3;
            
            Font projectFont = workbook.createFont();
            projectFont.setColor(IndexedColors.WHITE.getIndex());
            
            CellStyle projectStyle = workbook.createCellStyle();
            setBorder(projectStyle);
            setFillForegroundColor(projectStyle, IndexedColors.LIME);
            projectStyle.setFont(projectFont);
            
            for (Map.Entry<String, String> projectCodeAndProjectName : mapProjectCodeAndProjectName.entrySet()) {
                
                Cell projectCell = row.createCell(startColumnOfProject++);
                projectCell.setCellValue(projectCodeAndProjectName.getValue());
                projectCell.setCellStyle(projectStyle);
            }
            
            //+ new row
            row = sheet.createRow(rownum++);
            
            //3. write list employee and amount projects are assigned.
            List<PaymentEmployeeDTO> paymentEmployeeList = projectPayment.getListPaymentEmployeeDTO();
            
            for(PaymentEmployeeDTO paymentEmployee : paymentEmployeeList){
                
                // 3.1. write employee information 
                
                //EMPLOYEE_NAME
                CellStyle employeeNameStyle = workbook.createCellStyle();
                setBorder(employeeNameStyle);
                
                Cell employeeNameCell = row.createCell(EMPLOYEE_NAME);
                employeeNameCell.setCellValue(paymentEmployee.getEmployeeName());
                employeeNameCell.setCellStyle(employeeNameStyle);
                
                
                //EMPLOYEE_CODE
                CellStyle employeeCodeStyle = workbook.createCellStyle();
                setBorder(employeeCodeStyle);
                
                Cell employeeCodeCell = row.createCell(EMPLOYEE_CODE);
                employeeCodeCell.setCellValue(paymentEmployee.getEmployeeCode());
                employeeCodeCell.setCellStyle(employeeCodeStyle);
                
                //
                CellStyle salaryStyle = workbook.createCellStyle();
                setBorder(salaryStyle);
                formatNumeric(salaryStyle, format);

                Cell salaryCell = row.createCell(TOTAL_SALARY);
                salaryCell.setCellValue(doubleValue(paymentEmployee.getEmployeeSalary()));
                salaryCell.setCellStyle(salaryStyle);
                
                
                // 3.2 write list amount of projects are assigned
                Map<String, BigDecimal> mapProjectAssignedSalary = paymentEmployee.getMapProjectAssignedSalary();
                
                int startColumnOfAmountProjectAssigned = 3;
                
                for (Map.Entry<String, String> projectCodeAndProjectName : mapProjectCodeAndProjectName.entrySet()) {
                    
                    CellStyle projectAssignedSalaryStyle = workbook.createCellStyle();
                    formatNumeric(projectAssignedSalaryStyle, format);
                    setBorder(projectAssignedSalaryStyle);
                    
                    Cell projectAssignedSalaryCell = row.createCell(startColumnOfAmountProjectAssigned++);
                    projectAssignedSalaryCell.setCellValue(doubleValue(mapProjectAssignedSalary.get(projectCodeAndProjectName.getKey())));
                    projectAssignedSalaryCell.setCellStyle(projectAssignedSalaryStyle);
                }
                
                //+ new row for loop
                row = sheet.createRow(rownum++);
            
            }
            
            //4. For [Total] section
            //   - total for all project
            //   - total per project
            
            // title - totalSalary 
            sheet.addMergedRegion(new CellRangeAddress(rownum - 1, rownum - 1, 0, 1));
            
            CellStyle titleTotalSalaryStyle = workbook.createCellStyle();
            setFillForegroundColor(titleTotalSalaryStyle, IndexedColors.ORANGE);
            titleTotalSalaryStyle.setAlignment(HorizontalAlignment.RIGHT);
            setBorder(titleTotalSalaryStyle);
            
            Cell titleTotalSalaryCell = row.createCell(0);
            titleTotalSalaryCell.setCellStyle(titleTotalSalaryStyle);
            titleTotalSalaryCell.setCellValue(Title.TOTAL);
            
            //Set border for [Total] cell after merged.
            CellStyle titleTotalSalaryStyle_ = workbook.createCellStyle();
            Cell titleTotalSalaryCell_ = row.createCell(1);
            titleTotalSalaryCell_.setCellStyle(titleTotalSalaryStyle_);
            setBorder(titleTotalSalaryStyle_);
            
            // totalSalary all project.
            CellStyle totalSalaryStyle = workbook.createCellStyle();
            formatNumeric(totalSalaryStyle, format);
            setBorder(totalSalaryStyle);
            setFillForegroundColor(totalSalaryStyle, IndexedColors.ORANGE);
            
            Cell totalSalaryCell = row.createCell(TOTAL_SALARY_ALL_PROJECT);
            totalSalaryCell.setCellValue(doubleValue(projectPayment.getTotalSalaryAllEmplpoyee()));
            totalSalaryCell.setCellStyle(totalSalaryStyle);
            
            //totalSalary per project
            int startColumnOfTotalAmountProjectAssigned = 3;
            for (Map.Entry<String, String> projectCodeAndProjectName : mapProjectCodeAndProjectName.entrySet()) {
                String projectCode = projectCodeAndProjectName.getKey();

                CellStyle totalAmountOfProjectStyle = workbook.createCellStyle();
                formatNumeric(totalAmountOfProjectStyle, format);
                setBorder(totalAmountOfProjectStyle);
                setFillForegroundColor(totalAmountOfProjectStyle, IndexedColors.ORANGE);

                Cell totalSalaryOnProject = row.createCell(startColumnOfTotalAmountProjectAssigned++);
                totalSalaryOnProject.setCellValue(doubleValue(projectPayment.getTotalAmountOfProject(projectCode)));
                totalSalaryOnProject.setCellStyle(totalAmountOfProjectStyle);

            }
            
            //
            rownum++;

        }
        
    }
    
    private List<ProjectPaymentDTO> getListProjectPayment(HttpServletRequest request) {
        
        String monthFrom = StringUtils.stripToEmpty(request.getParameter("monthFrom"));
        String monthTo = StringUtils.stripToEmpty(request.getParameter("monthTo"));
        
        Calendar calMonthFrom = CalendarUtil.getCalendar(monthFrom, MMYYYY);
        Calendar calMonthTo = CalendarUtil.getCalendar(monthTo, MMYYYY);
        
        // check parameter is valid, if not will be return.
        if(calMonthFrom == null || calMonthTo == null){
            return new ArrayList<ProjectPaymentDTO>();
        }
        
        return projectPayrollDetailsService.getProjectPaymentDTOLst(calMonthFrom, calMonthTo);
    }
    
    /**
     * Set border for cell.
     */
    private void setBorder(CellStyle style){
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
    }
    
    private void setFillForegroundColor(CellStyle style, IndexedColors index) {
        style.setFillForegroundColor(index.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }
    
    public String getName(){
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.projectPayment", new Object[]{}, locale);
    }
    
    private Double doubleValue(BigDecimal val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    /**
     * Set format number.
     * <p>Example:
     * <ul>
     * <li>3456.70  -> 3,456.70
     * <li>3456.789 -> 3,456.78
     * </ul>
     * <p>
     * 
     */
    private void formatNumeric(CellStyle cellStyle, DataFormat format){
        cellStyle.setDataFormat(format.getFormat("#,##0.00"));
    }
    
    class Column {
        static final int MONTH = 0;
        
        //
        static final int EMPLOYEE_NAME = 0;
        static final int EMPLOYEE_CODE = 1;
        static final int TOTAL_SALARY = 2;
        
        static final int TOTAL_SALARY_ALL_PROJECT = 2;
        
    }
    
    class Title {
        static final String EMPLOYEE_NAME = "Employee Name";
        static final String EMPLOYEE_CODE = "Employee Code";
        static final String TOTAL_SALARY = "Total Salary";
        static final String TOTAL = "Total";
    }

}

