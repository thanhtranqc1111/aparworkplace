package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.AccountPayableViewerExportComponent.Column.*;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.AccountPayableViewerDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.domain.AccountPayableViewer;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.service.AccountPayableViewerService;
import com.ttv.cashflow.util.Constant;

@Component
@PrototypeScope
public class AccountPayableViewerExportComponent {
    
    
    @Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
    
    @Autowired
    private AccountPayableViewerService accountPayableViewerService;
    
    @Autowired
    private AccountPayableViewerDAO accountPayableViewerDAO;
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private ReimbursementDAO reimbursementDAO;
    
    public void doExportForAPInvoice(Workbook workbook, List<String> apInvoiceNoList) throws Exception{
        List<AccountPayableViewer> accountPayableViewerList = accountPayableViewerDAO.findAccountPayableViewerByAccountPayableNoList(apInvoiceNoList);
        doExport(workbook, accountPayableViewerList);
    }
    
    public void doExportForPayroll(Workbook workbook, List<String> payrollNoList) throws Exception {
        List<AccountPayableViewer> accountPayableViewerList = accountPayableViewerDAO.findAccountPayableViewerByAccountPayableNoList(payrollNoList);
        doExport(workbook, accountPayableViewerList);
    }
    
    public void doExportForReimbursement(Workbook workbook, List<String> reimbursementNoList) throws Exception{
        List<AccountPayableViewer> accountPayableViewerList = accountPayableViewerDAO.findAccountPayableViewerByAccountPayableNoList(reimbursementNoList);
        doExport(workbook, accountPayableViewerList);
    }
    
    public void doExportForPayment(Workbook workbook, List<String> paymentOrderNoList) throws Exception{
        List<AccountPayableViewer> accountPayableViewerList = accountPayableViewerDAO.findAccountPayableViewerByPaymentOrderNoList(paymentOrderNoList);
        doExport(workbook, accountPayableViewerList);
    }
    
    private void doExport(Workbook workbook, List<AccountPayableViewer> accountPayableViewerList) throws Exception {

        CreationHelper createHelper = workbook.getCreationHelper();
        DataFormat format = workbook.createDataFormat();
        Sheet sheet = workbook.getSheetAt(0);
        int rowNum = 2;

        accountPayableViewerDAO.refresh();
        
        //////////////////////////////////PAYABLE INFO//////////////////////////////
        for (AccountPayableViewer accountPayableViewer : accountPayableViewerList) {

            Row row = sheet.createRow(rowNum);
            row.createCell(NO_).setCellValue(rowNum - 1);
            row.createCell(ACCOUNT_PAYABLE_NO).setCellValue(accountPayableViewer.getAccountPayableNo());

            // Month
            CellStyle monthStyle = workbook.createCellStyle();
            formatDate(monthStyle, Constant.MMYYYY, createHelper);
            setVerticalAlignCenter(monthStyle);

            Cell monthCell = row.createCell(MONTH);
            monthCell.setCellValue(accountPayableViewer.getMonth());
            monthCell.setCellStyle(monthStyle);

            // Claim
            Cell claimCell = row.createCell(CLAIM_TYPE);
            claimCell.setCellValue(getClaimTypeName(accountPayableViewer.getClaimType()));

            // Vendor/Employee
            Cell vendorCell = row.createCell(VENDOR);
            vendorCell.setCellValue(accountPayableViewer.getVendor());

            // Invoice No.
            row.createCell(INVOICE_NO).setCellValue(accountPayableViewer.getInvoiceNo());

            // Approval Code
            row.createCell(APPROVAL_CODE).setCellValue(accountPayableViewer.getApprovalCode());

            // Currency
            row.createCell(CURRENCY).setCellValue(accountPayableViewer.getCurrency());

            // Amount(Original)
            CellStyle originalAmountStyle = workbook.createCellStyle();
            setVerticalAlignCenter(originalAmountStyle);
            formatNumeric(originalAmountStyle, format);

            Cell originalAmountCell = row.createCell(ORIGINAL_AMOUNT);
            originalAmountCell.setCellValue(doubleValue(accountPayableViewer.getOriginalAmount()));
            originalAmountCell.setCellStyle(originalAmountStyle);

            // Rate
            CellStyle fxRateStyle = workbook.createCellStyle();
            setVerticalAlignCenter(fxRateStyle);
            formatNumeric(fxRateStyle, format);

            Cell fxRateCell = row.createCell(FX_RATE);
            fxRateCell.setCellValue(doubleValue(accountPayableViewer.getFxRate()));
            fxRateCell.setCellStyle(fxRateStyle);

            // Amount(Converted)
            CellStyle convertedAmountStyle = workbook.createCellStyle();
            setVerticalAlignCenter(convertedAmountStyle);
            formatNumeric(convertedAmountStyle, format);

            Cell convertedAmountCell = row.createCell(CONVERTED_AMOUNT);
            convertedAmountCell.setCellValue(doubleValue(accountPayableViewer.getConvertedAmount()));
            convertedAmountCell.setCellStyle(convertedAmountStyle);

            // Corresponding Accounts
            row.createCell(ACCOUNT_NAME).setCellValue(accountPayableViewer.getAccountName());

            // Project
            row.createCell(PROJECT_NAME).setCellValue(accountPayableViewer.getProjectName());

            // Remarks
            row.createCell(DESCRIPTION).setCellValue(accountPayableViewer.getDescription());

            // -> next row
            rowNum++;
        }

        //////////////////////////////////PAYMENT ORDER///////////////////////////////////
        rowNum = 2;
        Integer numDetail = Integer.valueOf(0);
        
        List<String> accountPayableCodeNoAndPaymentOrderNoList = new ArrayList<String>();
        
        for (AccountPayableViewer accountPayableViewer : accountPayableViewerList) {

            Row row = sheet.getRow(rowNum);

            if (hasText(accountPayableViewer.getPaymentOrderNo())) {
                
                //Check pair account payable no - payment order no
                // If it was visited, will be ignore.
                // Otherwise will be write data to excel.
                String accountPayableCodeNoAndPaymentOrderNo = accountPayableViewer.getPaymentOrderNo() + accountPayableViewer.getAccountPayableNo();
                if(accountPayableCodeNoAndPaymentOrderNoList.contains(accountPayableCodeNoAndPaymentOrderNo)){
                    continue;
                }
                accountPayableCodeNoAndPaymentOrderNoList.add(accountPayableCodeNoAndPaymentOrderNo);

                // NOT Merge when payable payroll
                if (accountPayableViewer.getAccountPayableType().equals(Constant.PAYMENT_ORDER_PAYROLL)) {
                    numDetail = 1;
                }

                // Merge when payable AP invoice
                if (accountPayableViewer.getAccountPayableType().equals(Constant.PAYMENT_ORDER_APINVOICE)) {

                    // AccountPayableNo -> APInvoiceNo
                    String apInvoiceNo = accountPayableViewer.getAccountPayableNo();
                    numDetail = apInvoiceDAO.countDetail(apInvoiceNo).intValue();

                    int fistRow = rowNum;
                    int lastRow = rowNum + numDetail - 1;

                    if (fistRow != lastRow) {
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORDER_NO, PAYMENT_ORDER_NO));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VALUE_DATE, VALUE_DATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORIGINAL_AMOUNT, PAYMENT_ORIGINAL_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_RATE, PAYMENT_RATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_CONVERTED_AMOUNT, PAYMENT_CONVERTED_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VARIANCE_AMOUNT, VARIANCE_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, REMAIN_AMOUNT, REMAIN_AMOUNT));
                    }
                }
                
                // Merge when payable reimbursement
                if (accountPayableViewer.getAccountPayableType().equals(Constant.PAYMENT_ORDER_REIMBURSEMENT)) {

                    // AccountPayableNo -> ReimbursementNo
                    String reimbursementNo = accountPayableViewer.getAccountPayableNo();
                    numDetail = reimbursementDAO.countDetail(reimbursementNo).intValue();

                    int fistRow = rowNum;
                    int lastRow = rowNum + numDetail - 1;

                    if (fistRow != lastRow) {
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORDER_NO, PAYMENT_ORDER_NO));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VALUE_DATE, VALUE_DATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_ORIGINAL_AMOUNT, PAYMENT_ORIGINAL_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_RATE, PAYMENT_RATE));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, PAYMENT_CONVERTED_AMOUNT, PAYMENT_CONVERTED_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, VARIANCE_AMOUNT, VARIANCE_AMOUNT));
                        sheet.addMergedRegion(new CellRangeAddress(fistRow, lastRow, REMAIN_AMOUNT, REMAIN_AMOUNT));
                    }
                    
                }
                
                ////////Write data///////
                // Payment No
                CellStyle paymentOrderNoStyle = workbook.createCellStyle();
                setVerticalAlignCenter(paymentOrderNoStyle);
                
                Cell paymentOrderNoCell = row.createCell(PAYMENT_ORDER_NO);
                paymentOrderNoCell.setCellValue(accountPayableViewer.getPaymentOrderNo());
                paymentOrderNoCell.setCellStyle(paymentOrderNoStyle);

                // Paid Date(dd/mm/yyyy)
                CellStyle valueDateStyle = workbook.createCellStyle();
                formatDate(valueDateStyle, Constant.DDMMYYYY, createHelper);
                setVerticalAlignCenter(valueDateStyle);

                Cell valueDateCell = row.createCell(VALUE_DATE);
                valueDateCell.setCellValue(accountPayableViewer.getValueDate());
                valueDateCell.setCellStyle(valueDateStyle);

                // paymentOriginalAmount
                CellStyle paymentOriginalAmountStyle = workbook.createCellStyle();
                setVerticalAlignCenter(paymentOriginalAmountStyle);
                formatNumeric(paymentOriginalAmountStyle, format);

                Cell paymentOriginalAmountCell = row.createCell(PAYMENT_ORIGINAL_AMOUNT);
                paymentOriginalAmountCell.setCellValue(doubleValue(accountPayableViewer.getPaymentOriginalAmount()));
                paymentOriginalAmountCell.setCellStyle(paymentOriginalAmountStyle);

                // Payment Rate
                CellStyle paymentRateStyle = workbook.createCellStyle();
                setVerticalAlignCenter(paymentRateStyle);
                formatNumeric(paymentRateStyle, format);

                Cell paymentRateCell = row.createCell(PAYMENT_RATE);
                paymentRateCell.setCellValue(doubleValue(accountPayableViewer.getPaymentRate()));
                paymentRateCell.setCellStyle(paymentRateStyle);

                // PaymentConvertedAmount
                CellStyle paymentConvertedAmountStyle = workbook.createCellStyle();
                setVerticalAlignCenter(paymentConvertedAmountStyle);
                formatNumeric(paymentConvertedAmountStyle, format);

                Cell paymentConvertedAmountCell = row.createCell(PAYMENT_CONVERTED_AMOUNT);
                paymentConvertedAmountCell.setCellValue(doubleValue(accountPayableViewer.getPaymentConvertedAmount()));
                paymentConvertedAmountCell.setCellStyle(paymentConvertedAmountStyle);

                // VarianceAmount
                CellStyle varianceAmountStyle = workbook.createCellStyle();
                setVerticalAlignCenter(varianceAmountStyle);
                formatNumeric(varianceAmountStyle, format);

                Cell varianceAmountCell = row.createCell(VARIANCE_AMOUNT);
                varianceAmountCell.setCellValue(doubleValue(accountPayableViewer.getVarianceAmount()));
                varianceAmountCell.setCellStyle(varianceAmountStyle);

                // Remain Amount (Original)
                CellStyle remainAmountStyle = workbook.createCellStyle();
                setVerticalAlignCenter(remainAmountStyle);
                formatNumeric(remainAmountStyle, format);

                Cell remainAmountCell = row.createCell(REMAIN_AMOUNT);
                remainAmountCell.setCellValue(doubleValue(accountPayableViewer.getRemainAmount()));
                remainAmountCell.setCellStyle(remainAmountStyle);
            }

            rowNum = rowNum + numDetail;
        }
    }


    private Double doubleValue(BigDecimal val) {
        if (Objects.isNull(val)) {
            return 0d;
        } else {
            return val.doubleValue();
        }
    }
    
    private void formatDate(CellStyle cellStyle,  String format, CreationHelper createHelper){
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
    }
    
    /**
     * Set format number.
     * <p>Example:
     * <ul>
     * <li>3456.70  -> 3,456.70
     * <li>3456.789 -> 3,456.78
     * </ul>
     * <p>
     * 
     */
    private void formatNumeric(CellStyle cellStyle, DataFormat format){
        cellStyle.setDataFormat(format.getFormat("#,##0.00"));
    }
    
    private void setVerticalAlignCenter(CellStyle cellStyle){
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    }
    
    private String getClaimTypeName(String code) {
        ClaimTypeMaster cliamType = claimTypeMasterDAO.findClaimTypeMasterByCode(code);
        if (cliamType == null) {
            return StringUtils.EMPTY;
        }
        return cliamType.getName();

    }
	
    class Column {

        static final int NO_ = 0;
        static final int ACCOUNT_PAYABLE_NO = 1;
        static final int MONTH = 2;
        static final int CLAIM_TYPE = 3;
        static final int VENDOR = 4;
        static final int INVOICE_NO = 5;
        static final int APPROVAL_CODE = 6;
        static final int CURRENCY = 7;
        static final int ORIGINAL_AMOUNT = 8;
        static final int FX_RATE = 9;
        static final int CONVERTED_AMOUNT = 10;
        static final int ACCOUNT_NAME = 11;
        static final int PROJECT_NAME = 12;
        static final int DESCRIPTION = 13;
        static final int PAYMENT_ORDER_NO = 14;
        static final int VALUE_DATE = 15;
        static final int PAYMENT_ORIGINAL_AMOUNT = 16;
        static final int PAYMENT_RATE = 17;
        static final int PAYMENT_CONVERTED_AMOUNT = 18;
        static final int VARIANCE_AMOUNT = 19;
        static final int REMAIN_AMOUNT = 20;

    }
    
    


}

