package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.CurrencyImportComponent.Column.*;

import java.util.Locale;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.CurrencyMasterDAO;
import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class CurrencyImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("CURRENCY");
    
    private DataFormatter df = new DataFormatter();
    
    @Autowired
    private CurrencyMasterService currencyMasterService;
    
    @Autowired
    private CurrencyMasterDAO currencyMasterDAO;
	
	@Override
	public ImportReturn doMerge(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
	    int nRowSuccess = 0, nRowFail = 0;
	    Sheet sheet = workbook.getSheetAt(0);
	    
	    for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {
                    CurrencyMaster master = new CurrencyMaster();
                    
                    String code = df.formatCellValue(row.getCell(CODE));
                    
                    //Ignore row invalid
                    if(StringUtils.isEmpty(code)) continue;
                    
                    master.setCode(code);
                    master.setName(df.formatCellValue(row.getCell(NAME)));
                    
                    currencyMasterService.saveCurrencyMaster(master);
                    
                    nRowSuccess++;

                } catch (Exception e) {
                    e.printStackTrace();

                    nRowFail++;
                }
            }
	    }
	    
	    // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRowSuccess, nRowSuccess, nRowFail);
	}

	@Override
	public ImportReturn doUpdate(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doReplace(Workbook workbook) {
		return null;
	}

	@Override
	public ImportReturn doDelete(Workbook workbook) {
		//create log file
		logHelper.init(logger, this.getLogPath());
        int nRowSuccess = 0, nRowFail = 0;
        
        Sheet sheet = workbook.getSheetAt(0);

        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);

            try {
                String code = df.formatCellValue(row.getCell(CODE));
                
                //Ignore row invalid
                if(StringUtils.isEmpty(code)) continue;
                
                CurrencyMaster master = currencyMasterDAO.findCurrencyMasterByPrimaryKey(code);

                if (null != master) {
                    currencyMasterService.deleteCurrencyMaster(master);
                    nRowSuccess++;
                } else {
                    logHelper.writeWarningLog(i, "Code", "not exist DB");
                    nRowFail++;
                }
                
            } catch (Exception e) {
                e.printStackTrace();
                nRowFail++;
            }
        }
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRowSuccess, nRowSuccess, nRowFail);
        logHelper.close();
        
        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRowSuccess, nRowSuccess, nRowFail);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.currencyMaster", new Object[]{}, locale);
    }
	
	class Column {
        static final int CODE = 0;
        static final int NAME = 1;
    }
	

}
