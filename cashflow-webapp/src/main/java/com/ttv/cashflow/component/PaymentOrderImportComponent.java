package com.ttv.cashflow.component;

import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.ACCOUNT_PAYABLE_CODE;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.ACCOUNT_PAYABLE_NAME;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.AP_INVOICE_NO;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.AP_INVOICE_ORIGINAL_CURRENCY;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.AP_INVOICE_PAYMENT_RATE;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.AP_PAYMENT_ORDER_AMOUNT_CONVERTED;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.AP_PAYMENT_ORDER_AMOUNT_ORIGINAL;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.BANK_ACCOUNT;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.BANK_NAME;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.BANK_REF_NO;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.DESCRIPTION;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYEE_BANK_ACCOUNT;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYEE_BANK_NAME;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYMENT_ORDER_APPROVAL_NO;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYROLL_NO;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYROLL_ORIGINAL_CURRENCY;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.PAYROLL_PAYMENT_RATE;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.REIMBURSEMENT_NO;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.REIMBURSEMENT_ORIGINAL_CURRENCY;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.REIMBURSEMENT_PAYMENT_RATE;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.TOTAL_AMOUNT_CONVERTED;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Column.VALUE_DATE;
import static com.ttv.cashflow.component.PaymentOrderImportComponent.Message.NOT_FOUND_DB;
import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApInvoiceImportComponent.Mode;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.PaymentOrderTempDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.PaymentOrderTemp;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.helper.LogHelper;
import com.ttv.cashflow.service.ApInvoicePaymentDetailsService;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.service.PaymentOrderTempService;
import com.ttv.cashflow.service.PayrollPaymentDetailsService;
import com.ttv.cashflow.service.PayrollService;
import com.ttv.cashflow.service.ReimbursementPaymentDetailsService;
import com.ttv.cashflow.service.ReimbursementService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.vo.ImportReturn;

@Component
@PrototypeScope
public class PaymentOrderImportComponent extends ImportComponent {
    
    private static final Logger logger = Logger.getLogger("PAYMENT ORDER");
    
    public static final String COMMON_VALIDATION_PATTERN_WARNING = "Row [%s], %s: %s is %s";
    public static final String COMMON_VALIDATION_PATTERN_WARNING_CODE_NAME = "Row [%s], Data not found with %s : %s and %s : %s";
    public static final String HAS_ALREADY_IMPORT_VALIDATION_PATTERN = "has already imported";
    public static final String IS_WRONG_FORMAT_PATTERN_DD_MM_YYYY = "is wrong format(pattern: dd/mm/yyyy)";
    public static final String IS_INVALID_NUMERIC_FORMAT = "is invalid numeric format";
    public static final String MUST_BE_NOT_BLANK = "must be not blank";
    public static final String IS_NOT_EXIST_IN_DB = "is not exist in DB";
    public static final String PAYMENT_RATE_NOT_MATCH_SAME_VALUE = "not match same value all of payment detail(s)";
    public static final String ORIGINAL_CURRENCY_NOT_MATCH_SAME_VALUE = "not match same value all of payment detail(s)";
    public static final String COMMON_VALIDATION_PATTERN_PAYMENT ="Row [%s], Not found Payment Order pay for %s : %s";


    @Autowired
    private PaymentOrderService paymentOrderService;

    @Autowired
    private ApInvoiceService apInvoiceService;

    @Autowired
    private PayrollService payrollService;

    @Autowired
    private AccountMasterDAO accountMasterDAO;
    
    @Autowired
    private ReimbursementService reimbursementService;

    @Autowired
    private ApInvoicePaymentDetailsService apInvoicePaymentDetailsService;

    @Autowired
    private PayrollPaymentDetailsService payrollPaymentDetailsService;

    @Autowired
    private ReimbursementPaymentDetailsService reimbursementPaymentDetailsService;
    
    @Autowired
    private PaymentOrderTempService paymentOrderTempService;
    
    @Autowired
    private PaymentOrderTempDAO paymentOrderTempDAO;

    @Autowired
    protected LogHelper logHelper;

    @Override
    public ImportReturn doMerge(Workbook workbook) {
        // create log file
        logHelper.init(logger, this.getLogPath());
        
        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt numOfSuccessfulRecords = new MutableInt(0);
        MutableInt numOfFailedRecords = new MutableInt(0);
        
        Sheet sheet = workbook.getSheetAt(0);
        
        //truncate table temporary
        paymentOrderTempDAO.truncate();
        
        for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {

                	doSavePaymentOrderTemp(row, Mode.MERGE, numOfSuccessfulRecords, numOfFailedRecords);
                	
                } catch (Exception e) {
                	logHelper.writeErrorLog(e.getMessage());
                }
            }
        }
        
        doImportData(nRecordSuccess, numOfSuccessfulRecords, numOfFailedRecords);
        
        //truncate table temporary
        paymentOrderTempDAO.truncate();

        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), numOfSuccessfulRecords.intValue(), numOfFailedRecords.intValue());
        logHelper.close();

        return getImportReturn(Constant.IMPORT_MODE.MERGE, nRecordSuccess.intValue(), numOfSuccessfulRecords.intValue(), numOfFailedRecords.intValue());
    }

    @Override
    public ImportReturn doUpdate(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doReplace(Workbook workbook) {
        return null;
    }

    @Override
    public ImportReturn doDelete(Workbook workbook) {
        // create log file
        logHelper.init(logger, this.getLogPath());

        MutableInt nRecordSuccess = new MutableInt(0);
        MutableInt numOfSuccessfulRecords = new MutableInt(0);
        MutableInt numOfFailedRecords = new MutableInt(0);
        
        Sheet sheet = workbook.getSheetAt(0);

        for(int i = 2; i < sheet.getPhysicalNumberOfRows(); i++){
            Row row = sheet.getRow(i);
            if (null != row) {
                try {

                	doDeletePaymentOrder(row, nRecordSuccess, numOfSuccessfulRecords, numOfFailedRecords);
                	
                } catch (Exception e) {
                	logHelper.writeErrorLog(e.getMessage());
                }
            }
        }
        
        // write summary log and close log file.
        logHelper.writeSummaryLog(nRecordSuccess.intValue(), nRecordSuccess.intValue(), numOfFailedRecords.intValue());
        logHelper.close();

        return getImportReturn(Constant.IMPORT_MODE.DELETE, nRecordSuccess.intValue(), nRecordSuccess.intValue(), numOfFailedRecords.intValue());
    }
    
    /**
     * save row (excel) to payment order temp table
     * @param row
     * @param mode
     * @param nRecordSuccess
     * @param nRecordFail
     */
    private void doSavePaymentOrderTemp(Row row, Mode mode, MutableInt nRecordSuccess, MutableInt nRecordFail) {
    	// check null records
        if(isRowEmpty(row)){
        	return;
        }
        
        PaymentOrderTemp paymentTemp = new PaymentOrderTemp();
        int nRowData = row.getRowNum() + 1;
        
        // check payment detail
        if ((row.getCell(AP_INVOICE_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)
        		&& (row.getCell(PAYROLL_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)
        		&& (row.getCell(REIMBURSEMENT_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)) {
        	logHelper.writeErrorLog(nRowData, "Must be have payment info");
        	nRecordFail.increment();
        	return;
        }
        
        //bank name
        String bankName = DATA_FORMATTER.formatCellValue(row.getCell(BANK_NAME.getColumnNumber()));
        if(!hasText(bankName)){
        	logHelper.writeWarningLog(nRowData, BANK_NAME.getColumnName(), MUST_BE_NOT_BLANK);
        	nRecordFail.increment();
        	return;
        }
        paymentTemp.setBankName(bankName);
        
        //bank account
        String bankAccount = DATA_FORMATTER.formatCellValue(row.getCell(BANK_ACCOUNT.getColumnNumber()));
        if(!hasText(bankAccount)){
        	logHelper.writeWarningLog(nRowData, BANK_ACCOUNT.getColumnName(), MUST_BE_NOT_BLANK);
        	nRecordFail.increment();
        	return;
        }
        paymentTemp.setBankAccount(bankAccount);
        
        //value date
        Calendar valueDate = null;
        if (row.getCell(VALUE_DATE.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
            if (row.getCell(VALUE_DATE.getColumnNumber()).getCellTypeEnum() == CellType.NUMERIC) {
            	valueDate = CalendarUtil.getCalendar(row.getCell(VALUE_DATE.getColumnNumber()).getDateCellValue());
            } else {
                logHelper.writeWarningLog(nRowData, VALUE_DATE.getColumnName(), DATA_FORMATTER.formatCellValue(row.getCell(VALUE_DATE.getColumnNumber())), IS_WRONG_FORMAT_PATTERN_DD_MM_YYYY);
                nRecordFail.increment();
                return;
            }
        } else {
        	logHelper.writeWarningLog(nRowData, VALUE_DATE.getColumnName(), MUST_BE_NOT_BLANK);
        	nRecordFail.increment();
            return;
        }
        paymentTemp.setValueDate(valueDate);

        //Total Amount Converted
        BigDecimal totalAmountConverted = BigDecimal.ZERO;
        if (row.getCell(TOTAL_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
        	try {
            	totalAmountConverted =  getBigDecimal(row.getCell(TOTAL_AMOUNT_CONVERTED.getColumnNumber()));
    		} catch (NumberFormatException num) {
    			logHelper.writeWarningLog(nRowData, TOTAL_AMOUNT_CONVERTED.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
            	nRecordFail.increment();
                return;
    		}
        } else {
        	logHelper.writeWarningLog(nRowData, TOTAL_AMOUNT_CONVERTED.getColumnName(), MUST_BE_NOT_BLANK);
        	nRecordFail.increment();
            return;
        }
        paymentTemp.setTotalAmountConvertedAmount(totalAmountConverted);
        
        // bank ref no
        paymentTemp.setBankRefNo(DATA_FORMATTER.formatCellValue(row.getCell(BANK_REF_NO.getColumnNumber())));
        //description
        paymentTemp.setDescription(DATA_FORMATTER.formatCellValue(row.getCell(DESCRIPTION.getColumnNumber())));
        // approval no
        paymentTemp.setPaymentApprovalNo(DATA_FORMATTER.formatCellValue(row.getCell(PAYMENT_ORDER_APPROVAL_NO.getColumnNumber())));
        
        String accountPayableCode = DATA_FORMATTER.formatCellValue(row.getCell(ACCOUNT_PAYABLE_CODE.getColumnNumber())).trim();
        String accountPayableName = DATA_FORMATTER.formatCellValue(row.getCell(ACCOUNT_PAYABLE_NAME.getColumnNumber())).trim();
        
        if(StringUtils.isEmpty(accountPayableCode)){
        	if(StringUtils.isEmpty(accountPayableName)){
        		logHelper.writeWarningLog(nRowData, "Account Payable Name or Account Payable Code", MUST_BE_NOT_BLANK);
            	nRecordFail.increment();
                return;
        	} else {
        		Set<AccountMaster> payableSet = accountMasterDAO.findAccountMasterByName(accountPayableName);
            	Iterator<AccountMaster> payableAccountMasterIter = payableSet.iterator();
            	if(payableAccountMasterIter.hasNext()){
            		AccountMaster account = payableAccountMasterIter.next();
            		paymentTemp.setAccountPayableCode(account.getCode());
            		paymentTemp.setAccountPayableName(account.getName());
            	} else {
            		logHelper.writeErrorLog(String.format(COMMON_VALIDATION_PATTERN_WARNING, nRowData, ACCOUNT_PAYABLE_NAME.getColumnName(), accountPayableName, NOT_FOUND_DB));
	            	nRecordFail.increment();
	                return;
            	}
        	}
        } else {
        	AccountMaster payableAccountMaster = accountMasterDAO.findAccountMasterByCode(accountPayableCode);
    		if(payableAccountMaster != null) {
    			if(StringUtils.isEmpty(accountPayableName)){
    				paymentTemp.setAccountPayableCode(payableAccountMaster.getCode());
    				paymentTemp.setAccountPayableName(payableAccountMaster.getName());
    			} else {
    				if(payableAccountMaster.getName().equals(accountPayableName)){
    					paymentTemp.setAccountPayableCode(payableAccountMaster.getCode());
    					paymentTemp.setAccountPayableName(payableAccountMaster.getName());
    				} else {
    					logHelper.writeErrorLog(String.format(COMMON_VALIDATION_PATTERN_WARNING_CODE_NAME, nRowData, ACCOUNT_PAYABLE_CODE.getColumnName(), accountPayableCode, ACCOUNT_PAYABLE_NAME.getColumnName(), accountPayableName));
    	            	nRecordFail.increment();
    	                return;
    				}
    			}
    		} else {
    			logHelper.writeErrorLog(String.format(COMMON_VALIDATION_PATTERN_WARNING, nRowData, ACCOUNT_PAYABLE_CODE.getColumnName(), accountPayableCode, NOT_FOUND_DB));
            	nRecordFail.increment();
                return;
    		}
        }
        
        String originalCurrency = "";
        BigDecimal paymentRate = BigDecimal.ZERO;
        
        // AP INVOICE
        if(row.getCell(AP_INVOICE_NO.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
        	// check ap invoice is exist 
        	String apInvoiceNo = DATA_FORMATTER.formatCellValue(row.getCell(AP_INVOICE_NO.getColumnNumber()));
        	ApInvoice apInvoice = apInvoiceService.findApInvoiceByApInvoiceNo(apInvoiceNo);
        	if (apInvoice == null) {
        		logHelper.writeWarningLog(nRowData, AP_INVOICE_NO.getColumnName(), apInvoiceNo, IS_NOT_EXIST_IN_DB);
            	nRecordFail.increment();
                return;
        	}
        	
        	if(row.getCell(AP_INVOICE_ORIGINAL_CURRENCY.getColumnNumber()).getCellTypeEnum() == CellType.BLANK){
        		logHelper.writeWarningLog(nRowData, AP_INVOICE_ORIGINAL_CURRENCY.getColumnName(), MUST_BE_NOT_BLANK);
            	nRecordFail.increment();
                return;
			} else {
				String apCurrency = DATA_FORMATTER.formatCellValue(row.getCell(AP_INVOICE_ORIGINAL_CURRENCY.getColumnNumber()));
				if(("").equals(originalCurrency)){
					originalCurrency = apCurrency;
				} else {
					if(!(apCurrency).equals(originalCurrency)){
						logHelper.writeWarningLog(nRowData, AP_INVOICE_ORIGINAL_CURRENCY.getColumnName(), apCurrency, ORIGINAL_CURRENCY_NOT_MATCH_SAME_VALUE);
		            	nRecordFail.increment();
		                return;
					}
				}
				paymentTemp.setApInvoiceOriginalCurrencyCode(apCurrency);
			}
        	
			// payment rate ap invoice
			if (row.getCell(AP_INVOICE_PAYMENT_RATE.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					BigDecimal apInvoicePaymentRate = getBigDecimal(row.getCell(AP_INVOICE_PAYMENT_RATE.getColumnNumber()));
					if (paymentRate == BigDecimal.ZERO) {
						paymentRate = apInvoicePaymentRate;
					} else {
						if (apInvoicePaymentRate.compareTo(paymentRate) != 0) {
							logHelper.writeWarningLog(nRowData, AP_INVOICE_PAYMENT_RATE.getColumnName(), apInvoicePaymentRate.toString(), PAYMENT_RATE_NOT_MATCH_SAME_VALUE);
							nRecordFail.increment();
							return;
						}
					}
					paymentTemp.setApInvoicePaymentRate(apInvoicePaymentRate);
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, AP_INVOICE_PAYMENT_RATE.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
					nRecordFail.increment();
					return;
				}
			} else {
				logHelper.writeWarningLog(nRowData, AP_INVOICE_PAYMENT_RATE.getColumnName(), MUST_BE_NOT_BLANK);
				nRecordFail.increment();
				return;
			}
			
			// ap invoice payment order original amount
			if (row.getCell(AP_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setApInvoicePaymentOriginalAmount(getBigDecimal(row.getCell(AP_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, AP_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, AP_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			// ap invoice payment order converted amount
			if (row.getCell(AP_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setApInvoicePaymentConvertedAmount(getBigDecimal(row.getCell(AP_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, AP_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, AP_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			paymentTemp.setApInvoiceNo(DATA_FORMATTER.formatCellValue(row.getCell(AP_INVOICE_NO.getColumnNumber())));
			paymentTemp.setApInvoicePayeeBankName(DATA_FORMATTER.formatCellValue(row.getCell(PAYEE_BANK_NAME.getColumnNumber())));
			paymentTemp.setApInvoicePayeeBankAccNo(DATA_FORMATTER.formatCellValue(row.getCell(PAYEE_BANK_ACCOUNT.getColumnNumber())));
        }
        
        // PAYROLL
        if(row.getCell(PAYROLL_NO.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
        	// check payroll is exist 
        	String payrollNo = DATA_FORMATTER.formatCellValue(row.getCell(PAYROLL_NO.getColumnNumber()));
        	Payroll payroll = payrollService.findPayrollByPayrollNo(payrollNo);
        	if (payroll == null) {
        		logHelper.writeWarningLog(nRowData, PAYROLL_NO.getColumnName(), payrollNo, IS_NOT_EXIST_IN_DB);
            	nRecordFail.increment();
                return;
        	}
        	
        	if(row.getCell(PAYROLL_ORIGINAL_CURRENCY.getColumnNumber()).getCellTypeEnum() == CellType.BLANK){
        		logHelper.writeWarningLog(nRowData, PAYROLL_ORIGINAL_CURRENCY.getColumnName(), MUST_BE_NOT_BLANK);
            	nRecordFail.increment();
                return;
			} else {
				String prCurrency = DATA_FORMATTER.formatCellValue(row.getCell(PAYROLL_ORIGINAL_CURRENCY.getColumnNumber()));
				if(("").equals(originalCurrency)){
					originalCurrency = prCurrency;
				} else {
					if(!(prCurrency).equals(originalCurrency)){
						logHelper.writeWarningLog(nRowData, PAYROLL_ORIGINAL_CURRENCY.getColumnName(), prCurrency, ORIGINAL_CURRENCY_NOT_MATCH_SAME_VALUE);
		            	nRecordFail.increment();
		                return;
					}
				}
				paymentTemp.setPayrollOriginalCurrencyCode(prCurrency);
			}
        	
			// payment rate payroll
			if (row.getCell(PAYROLL_PAYMENT_RATE.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					BigDecimal payrollPaymentRate = getBigDecimal(row.getCell(PAYROLL_PAYMENT_RATE.getColumnNumber()));
					if (paymentRate == BigDecimal.ZERO) {
						paymentRate = payrollPaymentRate;
					} else {
						if (payrollPaymentRate.compareTo(paymentRate) != 0) {
							logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_RATE.getColumnName(), payrollPaymentRate.toString(), PAYMENT_RATE_NOT_MATCH_SAME_VALUE);
							nRecordFail.increment();
							return;
						}
					}
					paymentTemp.setPayrollPaymentRate(payrollPaymentRate);
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_RATE.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
					nRecordFail.increment();
					return;
				}
			} else {
				logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_RATE.getColumnName(), MUST_BE_NOT_BLANK);
				nRecordFail.increment();
				return;
			}
			
			// payroll payment order original amount
			if (row.getCell(PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setPayrollPaymentOriginalAmount(getBigDecimal(row.getCell(PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			// payroll payment order converted amount
			if (row.getCell(PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setPayrollPaymentConvertedAmount(getBigDecimal(row.getCell(PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			paymentTemp.setPayrollNo(DATA_FORMATTER.formatCellValue(row.getCell(PAYROLL_NO.getColumnNumber())));
        }
        
        // REIMBURSEMENT
        if(row.getCell(REIMBURSEMENT_NO.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
        	// check reimbursement is exist 
        	String reimbursementNo = DATA_FORMATTER.formatCellValue(row.getCell(REIMBURSEMENT_NO.getColumnNumber()));
        	Reimbursement reim = reimbursementService.findReimbursementByReimbursementNo(reimbursementNo);
        	if (reim == null) {
        		logHelper.writeWarningLog(nRowData, REIMBURSEMENT_NO.getColumnName(), reimbursementNo, IS_NOT_EXIST_IN_DB);
            	nRecordFail.increment();
                return;
        	}
        	
        	if(row.getCell(REIMBURSEMENT_ORIGINAL_CURRENCY.getColumnNumber()).getCellTypeEnum() == CellType.BLANK){
        		logHelper.writeWarningLog(nRowData, REIMBURSEMENT_ORIGINAL_CURRENCY.getColumnName(), MUST_BE_NOT_BLANK);
            	nRecordFail.increment();
                return;
			} else {
				String reimCurrency = DATA_FORMATTER.formatCellValue(row.getCell(REIMBURSEMENT_ORIGINAL_CURRENCY.getColumnNumber()));
				if(("").equals(originalCurrency)){
					originalCurrency = reimCurrency;
				} else {
					if(!(reimCurrency).equals(originalCurrency)){
						logHelper.writeWarningLog(nRowData, REIMBURSEMENT_ORIGINAL_CURRENCY.getColumnName(), reimCurrency, ORIGINAL_CURRENCY_NOT_MATCH_SAME_VALUE);
		            	nRecordFail.increment();
		                return;
					}
				}
				paymentTemp.setReimbursementOriginalCurrencyCode(reimCurrency);
			}
        	
			// payment rate reim
			if (row.getCell(REIMBURSEMENT_PAYMENT_RATE.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					BigDecimal reimPaymentRate = getBigDecimal(row.getCell(REIMBURSEMENT_PAYMENT_RATE.getColumnNumber()));
					if (paymentRate == BigDecimal.ZERO) {
						paymentRate = reimPaymentRate;
					} else {
						if (reimPaymentRate.compareTo(paymentRate) != 0) {
							logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_RATE.getColumnName(), reimPaymentRate.toString(), PAYMENT_RATE_NOT_MATCH_SAME_VALUE);
							nRecordFail.increment();
							return;
						}
					}
					paymentTemp.setReimbursementPaymentRate(reimPaymentRate);
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_RATE.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
					nRecordFail.increment();
					return;
				}
			} else {
				logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_RATE.getColumnName(), MUST_BE_NOT_BLANK);
				nRecordFail.increment();
				return;
			}
			
			// reimbursement payment order original amount
			if (row.getCell(REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setReimbursementPaymentOriginalAmount(getBigDecimal(row.getCell(REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			// reimbursement payment order converted amount
			if (row.getCell(REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
				try {
					paymentTemp.setReimbursementPaymentConvertedAmount(getBigDecimal(row.getCell(REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnNumber())));
				} catch (NumberFormatException num) {
					logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
	            	nRecordFail.increment();
	                return;
				}
	        } else {
	        	logHelper.writeWarningLog(nRowData, REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED.getColumnName(), MUST_BE_NOT_BLANK);
	        	nRecordFail.increment();
	            return;
	        }
			
			paymentTemp.setReimbursementNo(DATA_FORMATTER.formatCellValue(row.getCell(REIMBURSEMENT_NO.getColumnNumber())));
        }
        
        // check duplicate payment order
        boolean hasExistingPayment = paymentOrderService.checkDuplicatePaymentOrder(bankName, bankAccount, totalAmountConverted, valueDate);
    	if(hasExistingPayment){
    		logHelper.writeWarningLog(nRowData, BANK_NAME.getColumnName() + " : " + bankName + "[" + VALUE_DATE.getColumnName() + " : " + CalendarUtil.toString(valueDate) + "]", HAS_ALREADY_IMPORT_VALIDATION_PATTERN);
        	nRecordFail.increment();
            return;
    	}
        
        paymentOrderTempService.savePaymentOrderTemp(paymentTemp);
        
        nRecordSuccess.increment();
    }
    
    /**
     * import payment and payment detail
     * @param nRecordSuccess
     * @param nRowSuccess
     * @param nRowFail
     */
    private void doImportData(MutableInt nRecordSuccess, MutableInt nRowSuccess, MutableInt nRowFail) {
        
        List<String> listIdtemp = paymentOrderTempDAO.findListIdPaymentOrderTempByHavingGroup();
        
        for(int i = 0; i < listIdtemp.size(); i++){
            String[] arrIdtemp = listIdtemp.get(i).toString().split(",");
            
            PaymentOrderTemp paymentOrderTemp = paymentOrderTempService.findPaymentOrderTempByPrimaryKey(new BigInteger(arrIdtemp[0]));
            List<PaymentOrderTemp> paymentTempList = paymentOrderTempDAO.findPaymentOrderTempByListId(getListIdTempInBigInt(arrIdtemp));
            
            // check total amount of AP invoice with detail
            if(checkTotalAmoutOfPaymentOrder(paymentTempList, nRowSuccess, nRowFail)){
                ////save Payment
                PaymentOrder payment = doSavePaymentOrder(paymentOrderTemp, nRowFail);
                nRecordSuccess.increment();
                
                ////save Payment detail
                if(payment != null){
                	doSavePaymentOrderDetail(payment, paymentTempList);
                }
            }
        }
    }
    
    /**
     * save payment order table
     * @param paymentOrderTemp
     * @param nRowFail
     * @param status
     * @return
     */
    private PaymentOrder doSavePaymentOrder(PaymentOrderTemp paymentOrderTemp, MutableInt nRowFail){
		// get payment rate
		BigDecimal paymemtRate = BigDecimal.ZERO;
		String orginalCurrency = "";
		if(hasText(paymentOrderTemp.getApInvoiceNo())){
			paymemtRate = paymentOrderTemp.getApInvoicePaymentRate();
			orginalCurrency = paymentOrderTemp.getApInvoiceOriginalCurrencyCode();
		} else if (hasText(paymentOrderTemp.getPayrollNo())){
			paymemtRate = paymentOrderTemp.getPayrollPaymentRate();
			orginalCurrency = paymentOrderTemp.getPayrollOriginalCurrencyCode();
		} else if (hasText(paymentOrderTemp.getReimbursementNo())){
			paymemtRate = paymentOrderTemp.getReimbursementPaymentRate();
			orginalCurrency = paymentOrderTemp.getReimbursementOriginalCurrencyCode();
		}
		
		BigDecimal totalAmountConverted = paymentOrderTemp.getTotalAmountConvertedAmount();
		// check duplicate payment order
		PaymentOrder hasExistingPayment = paymentOrderService.checkDuplicateByConvertedAmountAndValueDate(paymentOrderTemp.getBankName(), paymentOrderTemp.getBankAccount(), totalAmountConverted, paymentOrderTemp.getValueDate());
		if(hasExistingPayment != null){
			return hasExistingPayment;
		}
		String paymentOrderNo = paymentOrderService.getPaymentOrderNo(paymentOrderTemp.getValueDate());
		
		StringBuilder paymentType = new StringBuilder();
		paymentType.append(hasText(paymentOrderTemp.getApInvoiceNo()) ? Constant.PAYMENT_ORDER_APINVOICE +"," : StringUtils.EMPTY);
		paymentType.append(hasText(paymentOrderTemp.getPayrollNo()) ? Constant.PAYMENT_ORDER_PAYROLL +"," : StringUtils.EMPTY);
		paymentType.append(hasText(paymentOrderTemp.getReimbursementNo()) ? Constant.PAYMENT_ORDER_REIMBURSEMENT +",": StringUtils.EMPTY);
		
		// payment order
		PaymentOrder payment = new PaymentOrder();
		payment.setPaymentOrderNo(paymentOrderNo);
		payment.setBankName(paymentOrderTemp.getBankName());
		payment.setBankAccount(paymentOrderTemp.getBankAccount());
		payment.setOriginalCurrencyCode(orginalCurrency);
		payment.setPaymentRate(paymemtRate);
		payment.setIncludeGstConvertedAmount(totalAmountConverted);
		payment.setIncludeGstOriginalAmount(getScaleBigDecimal(totalAmountConverted).divide(paymemtRate, RoundingMode.HALF_UP));
		payment.setValueDate(paymentOrderTemp.getValueDate());
		payment.setBankRefNo(paymentOrderTemp.getBankRefNo());
		payment.setDescription(paymentOrderTemp.getDescription());
		payment.setPaymentApprovalNo(paymentOrderTemp.getPaymentApprovalNo());
		payment.setAccountPayableCode(paymentOrderTemp.getAccountPayableCode());
		payment.setAccountPayableName(paymentOrderTemp.getAccountPayableName());
		payment.setPaymentType(paymentType.toString());
		payment.setStatus(Constant.PAY_PROCESSING);
		payment.setCreatedDate(Calendar.getInstance());
		payment.setModifiedDate(Calendar.getInstance());
		
		// save PaymentOrder to database
		return paymentOrderService.savePaymentOrder(payment);
		
    }
    
    /**
     * save payment order detail
     * @param payment
     * @param paymentTempList
     */
    private void doSavePaymentOrderDetail(PaymentOrder payment , List<PaymentOrderTemp> paymentTempList){
    	for (PaymentOrderTemp paymentOrderTemp : paymentTempList) {
    		// check  to save ap invoice payment detail
    		if(hasText(paymentOrderTemp.getApInvoiceNo())){
    			saveApInvoicePaymentOrderDetail(payment, paymentOrderTemp);
    		}
    		
    		// check to save payroll payment detail
    		if(hasText(paymentOrderTemp.getPayrollNo())){
    			savePayrollPaymentOrderDetail(payment, paymentOrderTemp);
    		}
    		
    		// check to save reimbursement payment detail
    		if(hasText(paymentOrderTemp.getReimbursementNo())){
    			saveReimbursementPaymentOrderDetail(payment, paymentOrderTemp);
    		}
    	}
    }
    
    /**
     * save ap invoice payment detail
     * @param paymentOrder
     * @param paymentOrderTemp
     * @return
     */
    private ApInvoicePaymentDetails saveApInvoicePaymentOrderDetail(PaymentOrder paymentOrder, PaymentOrderTemp paymentOrderTemp){
    	ApInvoice apInvoice = apInvoiceService.findApInvoiceByApInvoiceNo(paymentOrderTemp.getApInvoiceNo());
    	BigDecimal includeGstOriginalAmount = paymentOrderTemp.getApInvoicePaymentOriginalAmount();
    	BigDecimal includeGstConvertedAmount = paymentOrderTemp.getApInvoicePaymentConvertedAmount();
    	BigDecimal varianceAmount = includeGstConvertedAmount.subtract(includeGstOriginalAmount.multiply(apInvoice.getFxRate()));
    	
    	ApInvoicePaymentDetails apInvoicePaymentDetail = new ApInvoicePaymentDetails();
    	apInvoicePaymentDetail.setPaymentOrder(paymentOrder);
    	apInvoicePaymentDetail.setApInvoice(apInvoice);
    	apInvoicePaymentDetail.setPaymentIncludeGstOriginalAmount(includeGstOriginalAmount);
    	apInvoicePaymentDetail.setPaymentIncludeGstConvertedAmount(includeGstConvertedAmount);
    	apInvoicePaymentDetail.setPaymentRate(paymentOrderTemp.getApInvoicePaymentRate());
    	apInvoicePaymentDetail.setPayeeBankName(paymentOrderTemp.getApInvoicePayeeBankName());
    	apInvoicePaymentDetail.setPayeeBankAccNo(paymentOrderTemp.getApInvoicePayeeBankAccNo());
    	apInvoicePaymentDetail.setVarianceAmount(varianceAmount);
    	apInvoicePaymentDetail.setCreatedDate(Calendar.getInstance());
    	apInvoicePaymentDetail.setModifiedDate(Calendar.getInstance());
    	
    	return apInvoicePaymentDetailsService.saveApInvoicePaymentDetails(apInvoicePaymentDetail);
    }
    
    /**
     * save payroll payment order detail
     * @param paymentOrder
     * @param paymentOrderTemp
     * @return
     */
    private PayrollPaymentDetails savePayrollPaymentOrderDetail(PaymentOrder paymentOrder, PaymentOrderTemp paymentOrderTemp){
    	Payroll payroll = payrollService.findPayrollByPayrollNo(paymentOrderTemp.getPayrollNo());
    	PayrollPaymentDetails payrollPaymentDetails = new PayrollPaymentDetails();
    	payrollPaymentDetails.setPaymentOrder(paymentOrder);
    	payrollPaymentDetails.setPayroll(payroll);
    	payrollPaymentDetails.setPaymentRate(paymentOrderTemp.getPayrollPaymentRate());
    	payrollPaymentDetails.setNetPaymentAmountOriginal(paymentOrderTemp.getPayrollPaymentOriginalAmount());
    	payrollPaymentDetails.setNetPaymentAmountConverted(paymentOrderTemp.getPayrollPaymentConvertedAmount());
    	payrollPaymentDetails.setCreatedDate(Calendar.getInstance());
    	payrollPaymentDetails.setModifiedDate(Calendar.getInstance());
    	
    	return payrollPaymentDetailsService.savePayrollPaymentDetails(payrollPaymentDetails);
    }
    
    /**
     * save reimbursement payment order detail
     * @param paymentOrder
     * @param paymentOrderTemp
     * @return
     */
    private ReimbursementPaymentDetails saveReimbursementPaymentOrderDetail(PaymentOrder paymentOrder, PaymentOrderTemp paymentOrderTemp){
    	Reimbursement reimbursement = reimbursementService.findReimbursementByReimbursementNo(paymentOrderTemp.getReimbursementNo());
    	BigDecimal paymentOriginalAmount = paymentOrderTemp.getReimbursementPaymentOriginalAmount();
    	BigDecimal paymentConvertedAmount = paymentOrderTemp.getReimbursementPaymentConvertedAmount();
    	BigDecimal varianceAmount = paymentConvertedAmount.subtract(paymentOriginalAmount.multiply(reimbursement.getFxRate()));
    	
    	ReimbursementPaymentDetails reimbursementPaymentDetails = new ReimbursementPaymentDetails();
    	reimbursementPaymentDetails.setPaymentOrder(paymentOrder);
    	reimbursementPaymentDetails.setReimbursement(reimbursement);
    	reimbursementPaymentDetails.setPaymentRate(paymentOrderTemp.getReimbursementPaymentRate());
    	reimbursementPaymentDetails.setPaymentOriginalAmount(paymentOriginalAmount);
    	reimbursementPaymentDetails.setPaymentConvertedAmount(paymentConvertedAmount);
    	reimbursementPaymentDetails.setVarianceAmount(varianceAmount);
    	reimbursementPaymentDetails.setCreatedDate(Calendar.getInstance());
    	reimbursementPaymentDetails.setModifiedDate(Calendar.getInstance());
    	
    	return reimbursementPaymentDetailsService.saveReimbursementPaymentDetails(reimbursementPaymentDetails);
    }
    		
    /**
     * get list id temp detail
     * @param arrIdtemp
     * @return
     */
    private List<BigInteger> getListIdTempInBigInt(String[] arrIdtemp){
        List<BigInteger> listId = new ArrayList<>();
        
        for(String id : arrIdtemp){
            listId.add(new BigInteger(id));
        }
        
        return listId;
    }

    /**
     * Check total amount should be sum of amount of detail
     */
    private boolean checkTotalAmoutOfPaymentOrder(List<PaymentOrderTemp> paymentOrderTempList, MutableInt nRecordSuccess, MutableInt nRecordFail){
        int count = 0;
        boolean hasError = false;
        
        PaymentOrderTemp paymentOrderTemp = paymentOrderTempList.get(0);
        
        BigDecimal totalConvertedAmount = BigDecimal.ZERO;
        BigDecimal totalConvertedAmountInDetail = BigDecimal.ZERO;
        
        if(paymentOrderTempList.size() > 1){
        	for (PaymentOrderTemp paymentOrderTemp2 : paymentOrderTempList) {
        		totalConvertedAmount = paymentOrderTemp2.getTotalAmountConvertedAmount();
        		
        		if(hasText(paymentOrderTemp2.getApInvoiceNo())){
        			totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp2.getApInvoicePaymentConvertedAmount());
        		}
        		if(hasText(paymentOrderTemp2.getPayrollNo())){
        			totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp2.getPayrollPaymentConvertedAmount());
        		}
        		if(hasText(paymentOrderTemp2.getReimbursementNo())){
        			totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp2.getReimbursementPaymentConvertedAmount());
        		}
        		count++;
        	}
        	
        } else {
        	if(hasText(paymentOrderTemp.getApInvoiceNo())){
        		totalConvertedAmount = totalConvertedAmount.add(paymentOrderTempDAO.totalApinvoiceConvertedAmount(paymentOrderTemp.getApInvoiceNo()));
        		totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp.getApInvoicePaymentConvertedAmount());
        	}
        	if(hasText(paymentOrderTemp.getPayrollNo())){
        		totalConvertedAmount = totalConvertedAmount.add(paymentOrderTempDAO.totalPayrollConvertedAmount(paymentOrderTemp.getPayrollNo()));
        		totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp.getPayrollPaymentConvertedAmount());
        	}
        	if(hasText(paymentOrderTemp.getReimbursementNo())){
        		totalConvertedAmount = totalConvertedAmount.add(paymentOrderTempDAO.totalReimbursementConvertedAmount(paymentOrderTemp.getReimbursementNo()));
        		totalConvertedAmountInDetail = totalConvertedAmountInDetail.add(paymentOrderTemp.getReimbursementPaymentConvertedAmount());
        	}
        	count++;
        	
        }
        
        if(getScaleBigDecimal(totalConvertedAmount).compareTo(getScaleBigDecimal(totalConvertedAmountInDetail)) < 0) {
        	logHelper.writeErrorLog(BANK_NAME.getColumnName() + " : " + paymentOrderTemp.getBankName() + "["
        			+ VALUE_DATE.getColumnName() + " : " + CalendarUtil.toString(paymentOrderTemp.getValueDate())
        			+ "], Total Amount (Converted) should greater or equal sum of amount converted detail(s).");
        	hasError = true;
        }
        
        
        if (hasError) {
            nRecordFail.add(count);
            nRecordSuccess.subtract(count);
        }
        
        return !hasError;
    }
    
    private void doDeletePaymentOrder(Row row, MutableInt nRecordSuccess, MutableInt numOfSuccessfulRecords, MutableInt numOfFailRecords){
    	// check null records
        if(isRowEmpty(row)){
        	return;
        }
        
        int nRowData = row.getRowNum() + 1;
        
        // check payment detail
        if ((row.getCell(AP_INVOICE_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)
        		&& (row.getCell(PAYROLL_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)
        		&& (row.getCell(REIMBURSEMENT_NO.getColumnNumber()).getCellTypeEnum() == CellType.BLANK)) {
        	logHelper.writeErrorLog(nRowData, "Must be have payment info");
        	numOfFailRecords.increment();
        	return;
        }
        
        //bank name
        String bankName = DATA_FORMATTER.formatCellValue(row.getCell(BANK_NAME.getColumnNumber()));
        if(!hasText(bankName)){
        	logHelper.writeWarningLog(nRowData, BANK_NAME.getColumnName(), MUST_BE_NOT_BLANK);
        	numOfFailRecords.increment();
        	return;
        }
        
        //bank account
        String bankAccount = DATA_FORMATTER.formatCellValue(row.getCell(BANK_ACCOUNT.getColumnNumber()));
        if(!hasText(bankAccount)){
        	logHelper.writeWarningLog(nRowData, BANK_ACCOUNT.getColumnName(), MUST_BE_NOT_BLANK);
        	numOfFailRecords.increment();
        	return;
        }
        
        //value date
        Calendar valueDate = null;
        if (row.getCell(VALUE_DATE.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
            if (row.getCell(VALUE_DATE.getColumnNumber()).getCellTypeEnum() == CellType.NUMERIC) {
            	valueDate = CalendarUtil.getCalendar(row.getCell(VALUE_DATE.getColumnNumber()).getDateCellValue());
            } else {
                logHelper.writeWarningLog(nRowData, VALUE_DATE.getColumnName(), DATA_FORMATTER.formatCellValue(row.getCell(VALUE_DATE.getColumnNumber())), IS_WRONG_FORMAT_PATTERN_DD_MM_YYYY);
                numOfFailRecords.increment();
                return;
            }
        } else {
        	logHelper.writeWarningLog(nRowData, VALUE_DATE.getColumnName(), MUST_BE_NOT_BLANK);
        	numOfFailRecords.increment();
            return;
        }

        //Total Amount Converted
        BigDecimal totalAmountConverted = BigDecimal.ZERO;
        if (row.getCell(TOTAL_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() != CellType.BLANK) {
        	if (row.getCell(TOTAL_AMOUNT_CONVERTED.getColumnNumber()).getCellTypeEnum() == CellType.NUMERIC) {
        		totalAmountConverted =  BigDecimal.valueOf(row.getCell(TOTAL_AMOUNT_CONVERTED.getColumnNumber()).getNumericCellValue());
            } else {
            	logHelper.writeWarningLog(nRowData, TOTAL_AMOUNT_CONVERTED.getColumnName(), IS_INVALID_NUMERIC_FORMAT);
            	numOfFailRecords.increment();
                return;
            }
        } else {
        	logHelper.writeWarningLog(nRowData, TOTAL_AMOUNT_CONVERTED.getColumnName(), MUST_BE_NOT_BLANK);
        	numOfFailRecords.increment();
            return;
        }
        
        //check payment order 
        PaymentOrder paymentOrder = paymentOrderService.checkDuplicateByConvertedAmountAndValueDate(bankName, bankAccount, totalAmountConverted, valueDate);
        if(paymentOrder != null){
        	paymentOrderService.deletePaymentOrder(paymentOrder);
        	nRecordSuccess.increment();
        } else {
        	logHelper.writeWarningLog(nRowData, BANK_NAME.getColumnName() + " : " + bankName + "[" + VALUE_DATE.getColumnName() + " : " + CalendarUtil.toString(valueDate) + "]", IS_NOT_EXIST_IN_DB);
        	numOfFailRecords.increment();
        }
        numOfSuccessfulRecords.increment();
    }

    /**
     * Scale BigDecimal in 2 with down.
     */
    private BigDecimal getScaleBigDecimal(BigDecimal value){
        return NumberUtil.getBigDecimalScaleDown(2, value);
    }
    
    @Override
    public String getName() {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage("cashflow.common.paymentOrder", new Object[] {}, locale);
    }

    enum Column {
        // Payment Header
        BANK_NAME(0, "Bank name"),
        BANK_ACCOUNT(1, "Bank Account"),
        VALUE_DATE(2, "Value date"),
        TOTAL_AMOUNT_CONVERTED(3, "Total Amount (Converted)"),
        BANK_REF_NO(4, "Bank Ref No"),
        DESCRIPTION(5, "Description"),
        PAYMENT_ORDER_APPROVAL_NO(6, "Payment Order Approval No"),
        ACCOUNT_PAYABLE_CODE(7, "Account Payable Code"),
        ACCOUNT_PAYABLE_NAME(8, "Account Payable Name"),

        // Payment Details
        AP_INVOICE_NO(9, "AP Invoice No"),
        AP_INVOICE_ORIGINAL_CURRENCY(10, "Original Currency"),
        AP_PAYMENT_ORDER_AMOUNT_ORIGINAL(11, "AP Payment Order Amount (Original)"),
        AP_INVOICE_PAYMENT_RATE(12, "Payment Rate"),
        AP_PAYMENT_ORDER_AMOUNT_CONVERTED(13, "AP Payment Order Amount (Converted)"),
        PAYEE_BANK_NAME(14, "Payee Bank Name"),
        PAYEE_BANK_ACCOUNT(15, "Payee Bank Account"),

        PAYROLL_NO(16, "Payroll No"),
        PAYROLL_ORIGINAL_CURRENCY(17, "Original Currency"),
        PAYROLL_PAYMENT_ORDER_AMOUNT_ORIGINAL(18, "Payroll Payment Order Amount (Original)"),
        PAYROLL_PAYMENT_RATE(19, "Payment Rate"),
        PAYROLL_PAYMENT_ORDER_AMOUNT_CONVERTED(20, "Payroll Payment Order Amount (Converted)"),

        REIMBURSEMENT_NO(21, "Reimbursement No"),
        REIMBURSEMENT_ORIGINAL_CURRENCY(22, "Original Currency"),
        REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_ORIGINAL(23, "Reimbursement Payment Order Amount (Original)"),
        REIMBURSEMENT_PAYMENT_RATE(24, "Payment Rate"),
        REIMBURSEMENT_PAYMENT_ORDER_AMOUNT_CONVERTED(25, "Reimbursement Payment Order Amount (Converted)");

        private int columnNumber;
        private String columnName;

        Column(int columnNumber, String columnName) {
            this.columnNumber = columnNumber;
            this.columnName = columnName;
        }

        public int getColumnNumber() {
            return columnNumber;
        }

        public String getColumnName() {
            return columnName;
        }
    }
    
    class Message {
        static final String NOT_FOUND_DB = "not found in master";
        static final String NOT_NULL_DATA = "not null";
        static final String REQUIRED = "required";
    }
}
