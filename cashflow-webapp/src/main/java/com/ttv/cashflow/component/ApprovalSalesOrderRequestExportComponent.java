package com.ttv.cashflow.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApprovalSalesOrderRequestImportComponent.Column;
import com.ttv.cashflow.dao.ApprovalSalesOrderRequestDAO;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;
import com.ttv.cashflow.util.ApplicationUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.vo.ExportReturn;

/**
 * @author thoai.nh
 * created date Nov 1, 2017
 */
@Component
@PrototypeScope
public class ApprovalSalesOrderRequestExportComponent extends ExportComponent{
	@Autowired
	private ApprovalSalesOrderRequestDAO approvalDAO;
	@Override
	public ExportReturn doExport(Workbook workbook, HttpServletRequest request) {
		// TODO Auto-generated method stub
		int success = 0, unSuccess = 0;
		String approvalType = StringUtils.stripToEmpty(request.getParameter("approvalType"));
		String approvalCode = StringUtils.stripToEmpty(request.getParameter("approvalCode"));
		String validityFromString = StringUtils.stripToEmpty(request.getParameter("validityFrom"));
		String validityToString = StringUtils.stripToEmpty(request.getParameter("validityTo"));
		Calendar calendarValidityFrom = null;
		Calendar calendarValidityTo = null;
		try {
			calendarValidityFrom = ApplicationUtil.getCalendar(validityFromString, "dd/MM/yyyy");
			calendarValidityTo = ApplicationUtil.getCalendar(validityToString, "dd/MM/yyyy");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			Set<ApprovalSalesOrderRequest> set = approvalDAO.findApprovalPaging(-1, -1, approvalType, approvalCode, calendarValidityFrom, calendarValidityTo, -1, null);
			XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
			// remove old contents 
			removeContents(sheet, 1);
			int index = 1;
			for (ApprovalSalesOrderRequest approval : set) {
				try {
					//cell date style
					CellStyle dateStyle = workbook.createCellStyle();
					CreationHelper createHelper = workbook.getCreationHelper();
					dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(Constant.DDMMYYYY));
					dateStyle.setBorderBottom(BorderStyle.HAIR);
					dateStyle.setBorderTop(BorderStyle.HAIR);
					dateStyle.setBorderRight(BorderStyle.HAIR);
					dateStyle.setBorderLeft(BorderStyle.HAIR);
					//orther cell style
					CellStyle rowStyle = workbook.createCellStyle();
				    Font headerFont= workbook.createFont();
				    headerFont.setColor(IndexedColors.BLACK.getIndex());
				    rowStyle.setFont(headerFont);
				    rowStyle.setBorderBottom(BorderStyle.HAIR);
				    rowStyle.setBorderTop(BorderStyle.HAIR);
				    rowStyle.setBorderRight(BorderStyle.HAIR);
				    rowStyle.setBorderLeft(BorderStyle.HAIR);
					
					Row row = sheet.createRow(index++);
					Cell cell0 = row.createCell(Column.APPROVAL_TYPE);
					cell0.setCellValue(approval.getApprovalType());
					cell0.setCellStyle(rowStyle);
					
					Cell cell1 = row.createCell(Column.APPROVAL_NO);
					cell1.setCellValue(approval.getApprovalCode());
					cell1.setCellStyle(rowStyle);
					
					Cell cell2 = row.createCell(Column.APPLICATION_DATE);
					cell2.setCellValue(approval.getApplicationDate());
					cell2.setCellStyle(dateStyle);
					
					Cell cell3 = row.createCell(Column.VALIDITY_FROM);
					cell3.setCellValue(approval.getValidityFrom());
					cell3.setCellStyle(dateStyle);
					
					Cell cell4 = row.createCell(Column.VALIDITY_TO);
					cell4.setCellValue(approval.getValidityTo());
					cell4.setCellStyle(dateStyle);
					
					Cell cell5 = row.createCell(Column.CURRENCY);
					cell5.setCellValue(approval.getCurrencyCode());
					cell5.setCellStyle(rowStyle);
					
					Cell cell6 = row.createCell(Column.INCLUDE_GST_ORIGINAL_AMOUNT);
					cell6.setCellValue(approval.getIncludeGstOriginalAmount().doubleValue());
					cell6.setCellStyle(rowStyle);
					
					Cell cell7 = row.createCell(Column.PURPOSE);
					cell7.setCellValue(approval.getPurpose());
					cell7.setCellStyle(rowStyle);
					
					Cell cell8 = row.createCell(Column.APPLICANT);
					cell8.setCellValue(approval.getApplicant());
					cell8.setCellStyle(rowStyle);
					
					Cell cell9 = row.createCell(Column.APPROVED_DATE);
					cell9.setCellValue(approval.getApprovedDate());
					cell9.setCellStyle(dateStyle);
					
	
					Cell cell11 = row.createCell(Column.APPLIED_FOR);
					cell11.setCellValue(String.join(", ", approval.getArInVoicesApplied()));
					cell11.setCellStyle(rowStyle);
					success++;
				} catch (Exception e) {
					e.printStackTrace();
					unSuccess++;
				}
			}
			sheet.autoSizeColumn(Column.APPLIED_FOR);
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getFilePath())));
			workbook.write(outputStream);
			workbook.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return getExportReturn(success, unSuccess);
	}
	
	public String getName(){
		Locale locale = LocaleContextHolder.getLocale();
	    return messageSource.getMessage("cashflow.common.approval", new Object[]{}, locale);
	}

}
