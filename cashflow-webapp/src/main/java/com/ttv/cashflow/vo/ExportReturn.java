package com.ttv.cashflow.vo;

public class ExportReturn {
	private String type;
	private int nRecordSuccess;
	private int nRecordFail;
	private String errorDetail;
	
	public ExportReturn() {
		super();
	}

	public ExportReturn(String type, int nRecordSuccess, int nRecordFail) {
		this.type = type;
		this.nRecordSuccess = nRecordSuccess;
		this.nRecordFail = nRecordFail;
	}
	
	public ExportReturn(String type, String mode, int nRecordSuccess, int nRecordFail, String errorDetail) {
		this.type = type;
		this.nRecordSuccess = nRecordSuccess;
		this.nRecordFail = nRecordFail;
		this.errorDetail = errorDetail;
	}

	public int getnRecordSuccess() {
		return nRecordSuccess;
	}
	
	public void setnRecordSuccess(int nRecordSuccess) {
		this.nRecordSuccess = nRecordSuccess;
	}
	
	public int getnRecordFail() {
		return nRecordFail;
	}
	
	public void setnRecordFail(int nRecordFail) {
		this.nRecordFail = nRecordFail;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}
	
	public String getMessageLog(String builder){
        return String.format(builder, type);
    }
}
