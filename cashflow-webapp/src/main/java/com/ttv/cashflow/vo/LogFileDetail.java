package com.ttv.cashflow.vo;

import java.util.Calendar;

public class LogFileDetail {
	private String LogName;
	private Calendar importedDate;

	public LogFileDetail() {
	}

	public LogFileDetail(String logName, Calendar importedDate) {
		super();
		LogName = logName;
		this.importedDate = importedDate;
	}


	public String getLogName() {
		return LogName;
	}

	public void setLogName(String logName) {
		LogName = logName;
	}

	public Calendar getImportedDate() {
		return importedDate;
	}

	public void setImportedDate(Calendar importedDate) {
		this.importedDate = importedDate;
	}

}
