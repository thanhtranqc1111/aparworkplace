package com.ttv.cashflow.vo;

import java.util.Calendar;

public class FileExportDetail{
	private String name;
	private double size;
	private Calendar date;
	
	public FileExportDetail() {
    }
	
    public FileExportDetail(String name, double size, Calendar date) {
        super();
        this.name = name;
        this.size = size;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
	
	
	
   
}
