package com.ttv.cashflow.vo;

public class ImportReturn {
	
    private String type;
	private String mode;
	private int nRecordeSuccess;
	private int nRowSuccess;
	private int nRowFail;
	
	public ImportReturn() {
		super();
	}

	public ImportReturn(String type, String mode, int nRowSuccess, int nRowFail) {
		this.type = type;
		this.mode = mode;
		this.nRowSuccess = nRowSuccess;
		this.nRowFail = nRowFail;
	}
	
	public ImportReturn(String type, String mode, int nRecordeSuccess, int nRowSuccess, int nRowFail) {
        this.type = type;
        this.mode = mode;
        this.nRecordeSuccess = nRecordeSuccess;
        this.nRowSuccess = nRowSuccess;
        this.nRowFail = nRowFail;
    }
	
	public void setnRecordeSuccess(int nRecordeSuccess) {
        this.nRecordeSuccess = nRecordeSuccess;
    }
	
	public int getnRecordeSuccess() {
        return nRecordeSuccess;
    }

    public int getnRowSuccess() {
		return nRowSuccess;
	}
	
	public void setnRowSuccess(int nRowSuccess) {
		this.nRowSuccess = nRowSuccess;
	}
	
	public int getnRowFail() {
		return nRowFail;
	}
	
	public void setnRowFail(int nRowFail) {
		this.nRowFail = nRowFail;
	}

	public String getMessageLog(String templateMsg, String detailMsg){
        if (nRowFail > 0) {
            return String.format(templateMsg, type, mode, nRecordeSuccess, type, nRowSuccess, nRowFail)
                                                + "<br/> <span style='color: red;'>" + detailMsg + "</span>";
        } else {
            return String.format(templateMsg, type, mode, nRecordeSuccess, type, nRowSuccess, nRowFail);
        }
    }
	
}
