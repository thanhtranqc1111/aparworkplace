package com.ttv.cashflow.vo;

import java.util.Calendar;

public class FileImportDetail {
	private String fileName;
	private double fileSize;
	private Calendar uploadedDate;
	
	
	public FileImportDetail() {
		// TODO Auto-generated constructor stub
	}

	public FileImportDetail(String fileName, double fileSize, Calendar uploadedDate) {
		super();
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.uploadedDate = uploadedDate;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public double getFileSize() {
		return fileSize;
	}


	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}


	public Calendar getUploadedDate() {
		return uploadedDate;
	}


	public void setUploadedDate(Calendar uploadedDate) {
		this.uploadedDate = uploadedDate;
	}
}
