package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class PaymentReimDetailForm {
    
    private BigInteger detailId;
    private BigInteger reimId;
    private String reimbursementNo;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar scheduledPaymentDate;
    private String employeeName;
    @DateTimeFormat(pattern = Constant.MMYYYY)
    private Calendar month;
    private BigDecimal includeGstTotalConvertedAmount;
    private BigDecimal includeGstTotalOriginalAmount;
    private BigDecimal paidOriginalAmount;
    private String originalCurrencyCode;
    private BigDecimal paymentReimOriginalAmount;
    private BigDecimal paymentRate;
    private BigDecimal fxRate;
    private BigDecimal paymentReimConvertedAmount;
    private BigDecimal unpaidOriginalAmount;
    private BigDecimal varianceAmount;
    private String varianceAccountCode;
    private Boolean isApproval;
    private Boolean isPaid;
    private Boolean isPayEnough;
    private String accountPayableCode;
    private String accountPayableName;
    private BigDecimal paidOriginalAmountWaiting;

    public PaymentReimDetailForm() {}

	public BigInteger getDetailId() {
		return detailId;
	}

	public void setDetailId(BigInteger detailId) {
		this.detailId = detailId;
	}

	public BigInteger getReimId() {
		return reimId;
	}

	public void setReimId(BigInteger reimId) {
		this.reimId = reimId;
	}


	public String getReimbursementNo() {
		return reimbursementNo;
	}

	public void setReimbursementNo(String reimbursementNo) {
		this.reimbursementNo = reimbursementNo;
	}

	public Calendar getScheduledPaymentDate() {
		return scheduledPaymentDate;
	}

	public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
		this.scheduledPaymentDate = scheduledPaymentDate;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}

	public BigDecimal getIncludeGstTotalConvertedAmount() {
		return includeGstTotalConvertedAmount;
	}

	public void setIncludeGstTotalConvertedAmount(BigDecimal includeGstTotalConvertedAmount) {
		this.includeGstTotalConvertedAmount = includeGstTotalConvertedAmount;
	}

	public BigDecimal getIncludeGstTotalOriginalAmount() {
		return includeGstTotalOriginalAmount;
	}

	public void setIncludeGstTotalOriginalAmount(BigDecimal includeGstTotalOriginalAmount) {
		this.includeGstTotalOriginalAmount = includeGstTotalOriginalAmount;
	}

	public BigDecimal getPaidOriginalAmount() {
		return paidOriginalAmount;
	}

	public void setPaidOriginalAmount(BigDecimal paidOriginalAmount) {
		this.paidOriginalAmount = paidOriginalAmount;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getPaymentReimOriginalAmount() {
		return paymentReimOriginalAmount;
	}

	public void setPaymentReimOriginalAmount(BigDecimal paymentReimOriginalAmount) {
		this.paymentReimOriginalAmount = paymentReimOriginalAmount;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	public BigDecimal getPaymentReimConvertedAmount() {
		return paymentReimConvertedAmount;
	}

	public void setPaymentReimConvertedAmount(BigDecimal paymentReimConvertedAmount) {
		this.paymentReimConvertedAmount = paymentReimConvertedAmount;
	}

	public BigDecimal getUnpaidOriginalAmount() {
		return unpaidOriginalAmount;
	}

	public void setUnpaidOriginalAmount(BigDecimal unpaidOriginalAmount) {
		this.unpaidOriginalAmount = unpaidOriginalAmount;
	}

	public BigDecimal getVarianceAmount() {
		return varianceAmount;
	}

	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	public String getVarianceAccountCode() {
		return varianceAccountCode;
	}

	public void setVarianceAccountCode(String varianceAccountCode) {
		this.varianceAccountCode = varianceAccountCode;
	}

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public Boolean getIsPayEnough() {
		return isPayEnough;
	}

	public void setIsPayEnough(Boolean isPayEnough) {
		this.isPayEnough = isPayEnough;
	}


	public String getAccountPayableCode() {
		return accountPayableCode;
	}

	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	public String getAccountPayableName() {
		return accountPayableName;
	}

	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	public BigDecimal getPaidOriginalAmountWaiting() {
		return paidOriginalAmountWaiting;
	}

	public void setPaidOriginalAmountWaiting(BigDecimal paidOriginalAmountWaiting) {
		this.paidOriginalAmountWaiting = paidOriginalAmountWaiting;
	}

	public BigDecimal getFxRate() {
		return fxRate;
	}

	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((reimId == null) ? 0 : reimId.hashCode());
        result = prime * result + ((reimbursementNo == null) ? 0 : reimbursementNo.hashCode());
        result = prime * result + ((detailId == null) ? 0 : detailId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaymentReimDetailForm other = (PaymentReimDetailForm) obj;
        if (reimId == null) {
            if (other.reimId != null)
                return false;
        } else if (!reimId.equals(other.reimId))
            return false;
        if (reimbursementNo == null) {
            if (other.reimbursementNo != null)
                return false;
        } else if (!reimbursementNo.equals(other.reimbursementNo))
            return false;
        if (detailId == null) {
            if (other.detailId != null)
                return false;
        } else if (!detailId.equals(other.detailId))
            return false;
        return true;
    }

    

}
