package com.ttv.cashflow.form;


public class PaymentPayrollSearchForm {

    private String payrollNoFrom;
    private String payrollNoTo;
    private String scheduledPaymentDateFrom;
    private String scheduledPaymentDateTo;
    private String monthFrom;
    private String monthTo;
    private Boolean isPaid;
    private Boolean isApproval;

    public PaymentPayrollSearchForm() {
    }

	public String getPayrollNoFrom() {
		return payrollNoFrom;
	}


	public void setPayrollNoFrom(String payrollNoFrom) {
		this.payrollNoFrom = payrollNoFrom;
	}


	public String getPayrollNoTo() {
		return payrollNoTo;
	}


	public void setPayrollNoTo(String payrollNoTo) {
		this.payrollNoTo = payrollNoTo;
	}


	public String getScheduledPaymentDateFrom() {
		return scheduledPaymentDateFrom;
	}

	public void setScheduledPaymentDateFrom(String scheduledPaymentDateFrom) {
		this.scheduledPaymentDateFrom = scheduledPaymentDateFrom;
	}

	public String getScheduledPaymentDateTo() {
		return scheduledPaymentDateTo;
	}

	public void setScheduledPaymentDateTo(String scheduledPaymentDateTo) {
		this.scheduledPaymentDateTo = scheduledPaymentDateTo;
	}

	public String getMonthFrom() {
		return monthFrom;
	}

	public void setMonthFrom(String monthFrom) {
		this.monthFrom = monthFrom;
	}

	public String getMonthTo() {
		return monthTo;
	}

	public void setMonthTo(String monthTo) {
		this.monthTo = monthTo;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

    
}
