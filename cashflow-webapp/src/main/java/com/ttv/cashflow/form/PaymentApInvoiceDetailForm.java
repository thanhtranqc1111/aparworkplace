package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class PaymentApInvoiceDetailForm {
    
    private BigInteger detailId;
    private BigInteger apInvoiceId;
    private String apInvoiceNo;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar scheduledPaymentDate;
    @DateTimeFormat(pattern = Constant.MMYYYY)
    private Calendar month;
    private String payeeName;
    private String invoiceNo;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal fxRate;
    private BigDecimal gst;
    private String originalCurrencyCode;
    private BigDecimal paidIncludeGstOriginalAmount;
    private BigDecimal paymentIncludeGstOriginalAmount;
    private BigDecimal paymentRate;
    private BigDecimal paymentIncludeGstConvertedAmount;
    private String payeeBankName;
    private String payeeBankAccNo;
    private BigDecimal unpaidIncludeGstOriginalAmount;
    private BigDecimal varianceAmount;
    private String varianceAccountCode;
    private Boolean isApproval;
    private BigDecimal paidIncludeGstOriginalAmountWaiting;
    private Boolean isPaid;
    private Boolean isPayEnough;
    private String accountCode;
    private String accountName;
    
    public PaymentApInvoiceDetailForm() {}

    public BigInteger getDetailId() {
        return detailId;
    }
    
    public void setDetailId(BigInteger detailId) {
        this.detailId = detailId;
    }
    
    public BigInteger getApInvoiceId() {
        return apInvoiceId;
    }
    
    public void setApInvoiceId(BigInteger apInvoiceId) {
        this.apInvoiceId = apInvoiceId;
    }

    public String getApInvoiceNo() {
        return apInvoiceNo;
    }

    public void setApInvoiceNo(String apInvoiceNo) {
        this.apInvoiceNo = apInvoiceNo;
    }

    public Calendar getScheduledPaymentDate() {
        return scheduledPaymentDate;
    }

    public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
        this.scheduledPaymentDate = scheduledPaymentDate;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public BigDecimal getExcludeGstOriginalAmount() {
        return excludeGstOriginalAmount;
    }

    public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
        this.excludeGstOriginalAmount = excludeGstOriginalAmount;
    }
    
    public BigDecimal getFxRate() {
        return fxRate;
    }
    
    public void setFxRate(BigDecimal fxRate) {
        this.fxRate = fxRate;
    }
    
    public BigDecimal getGst() {
        return gst;
    }
    
    public void setGst(BigDecimal gst) {
        this.gst = gst;
    }
    
    public String getOriginalCurrencyCode() {
        return originalCurrencyCode;
    }

    public void setOriginalCurrencyCode(String originalCurrencyCode) {
        this.originalCurrencyCode = originalCurrencyCode;
    }

    public BigDecimal getPaidIncludeGstOriginalAmount() {
		return paidIncludeGstOriginalAmount;
	}

	public void setPaidIncludeGstOriginalAmount(BigDecimal paidIncludeGstOriginalAmount) {
		this.paidIncludeGstOriginalAmount = paidIncludeGstOriginalAmount;
	}

	public BigDecimal getPaymentIncludeGstOriginalAmount() {
		return paymentIncludeGstOriginalAmount;
	}

	public void setPaymentIncludeGstOriginalAmount(BigDecimal paymentIncludeGstOriginalAmount) {
		this.paymentIncludeGstOriginalAmount = paymentIncludeGstOriginalAmount;
	}

	public BigDecimal getPaymentRate() {
        return paymentRate;
    }

    public void setPaymentRate(BigDecimal paymentRate) {
        this.paymentRate = paymentRate;
    }

    public BigDecimal getPaymentIncludeGstConvertedAmount() {
		return paymentIncludeGstConvertedAmount;
	}

	public void setPaymentIncludeGstConvertedAmount(BigDecimal paymentIncludeGstConvertedAmount) {
		this.paymentIncludeGstConvertedAmount = paymentIncludeGstConvertedAmount;
	}

	public String getPayeeBankName() {
        return payeeBankName;
    }

    public void setPayeeBankName(String payeeBankName) {
        this.payeeBankName = payeeBankName;
    }

    public String getPayeeBankAccNo() {
        return payeeBankAccNo;
    }

    public void setPayeeBankAccNo(String payeeBankAccNo) {
        this.payeeBankAccNo = payeeBankAccNo;
    }

    public BigDecimal getUnpaidIncludeGstOriginalAmount() {
		return unpaidIncludeGstOriginalAmount;
	}

	public void setUnpaidIncludeGstOriginalAmount(BigDecimal unpaidIncludeGstOriginalAmount) {
		this.unpaidIncludeGstOriginalAmount = unpaidIncludeGstOriginalAmount;
	}

	public BigDecimal getVarianceAmount() {
        return varianceAmount;
    }

    public void setVarianceAmount(BigDecimal varianceAmount) {
        this.varianceAmount = varianceAmount;
    }

    public String getVarianceAccountCode() {
        return varianceAccountCode;
    }

    public void setVarianceAccountCode(String varianceAccountCode) {
        this.varianceAccountCode = varianceAccountCode;
    }
    
    public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public Boolean getIsApproval() {
        return isApproval;
    }
    
    public void setIsApproval(Boolean isApproval) {
        this.isApproval = isApproval;
    }
    
    public BigDecimal getPaidIncludeGstOriginalAmountWaiting() {
		return paidIncludeGstOriginalAmountWaiting;
	}

	public void setPaidIncludeGstOriginalAmountWaiting(BigDecimal paidIncludeGstOriginalAmountWaiting) {
		this.paidIncludeGstOriginalAmountWaiting = paidIncludeGstOriginalAmountWaiting;
	}

	public Boolean getIsPaid() {
        return isPaid;
    }
    
    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }
	
    public Boolean getIsPayEnough() {
        return isPayEnough;
    }
    
    public void setIsPayEnough(Boolean isPayEnough) {
        this.isPayEnough = isPayEnough;
    }
    
	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((apInvoiceId == null) ? 0 : apInvoiceId.hashCode());
        result = prime * result + ((apInvoiceNo == null) ? 0 : apInvoiceNo.hashCode());
        result = prime * result + ((detailId == null) ? 0 : detailId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaymentApInvoiceDetailForm other = (PaymentApInvoiceDetailForm) obj;
        if (apInvoiceId == null) {
            if (other.apInvoiceId != null)
                return false;
        } else if (!apInvoiceId.equals(other.apInvoiceId))
            return false;
        if (apInvoiceNo == null) {
            if (other.apInvoiceNo != null)
                return false;
        } else if (!apInvoiceNo.equals(other.apInvoiceNo))
            return false;
        if (detailId == null) {
            if (other.detailId != null)
                return false;
        } else if (!detailId.equals(other.detailId))
            return false;
        return true;
    }

    

}
