package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

/**
 * @author thoai.nh 
 * created date Oct 11, 2017
 */
public class ApprovalForm {
	private BigInteger id;
	
	@NotEmpty(message = "{cashflow.approval.approvalCodeInvalidMessage}")
	private String approvalCode;
	
	@NotEmpty(message = "{cashflow.approval.approvalTypeInvalidMessage}")
	private String approvalType;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	@NotNull(message = "{cashflow.approval.validityFromInvalidMessage}")
	private Calendar validityFrom;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	@NotNull(message = "{cashflow.approval.validityToInvalidMessage}")
	private Calendar validityTo;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	@NotNull(message = "{cashflow.approval.approvedDateInvalidMessage}")
	private Calendar approvedDate;
	
	private String purpose;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	private Calendar applicationDate;
	
	@NotNull(message = "{cashflow.approval.amountInvalidMessage}")
	@DecimalMin(value = "0", inclusive = false, message = "{cashflow.approval.amountInvalidMessage2}")
	private BigDecimal includeGstOriginalAmount;
	
	private String applicant;

	private String currencyCode;
	
	@NotNull
	private String classification;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getApprovalType() {
		return approvalType;
	}
	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}
	public Calendar getValidityFrom() {
		return validityFrom;
	}
	public void setValidityFrom(Calendar validityFrom) {
		this.validityFrom = validityFrom;
	}
	public Calendar getValidityTo() {
		return validityTo;
	}
	public void setValidityTo(Calendar validityTo) {
		this.validityTo = validityTo;
	}
	public Calendar getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Calendar approvedDate) {
		this.approvedDate = approvedDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public Calendar getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}
	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
}
