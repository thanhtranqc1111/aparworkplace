package com.ttv.cashflow.form;


public class PaymentApInvoiceSearchForm {

    private String APNoFrom;
    private String APNoTo;
    private String scheduledPaymentDateFrom;
    private String scheduledPaymentDateTo;
    private String originalCurrencyCode;
    private String payeeName;
    private Boolean isPaid;
    private Boolean isApproval;

    public PaymentApInvoiceSearchForm() {
    }

    public String getAPNoFrom() {
        return APNoFrom;
    }

    public void setAPNoFrom(String aPNoFrom) {
        APNoFrom = aPNoFrom;
    }

    public String getAPNoTo() {
        return APNoTo;
    }

    public void setAPNoTo(String aPNoTo) {
        APNoTo = aPNoTo;
    }

    public String getScheduledPaymentDateFrom() {
        return scheduledPaymentDateFrom;
    }

    public void setScheduledPaymentDateFrom(String scheduledPaymentDateFrom) {
        this.scheduledPaymentDateFrom = scheduledPaymentDateFrom;
    }

    public String getScheduledPaymentDateTo() {
        return scheduledPaymentDateTo;
    }

    public void setScheduledPaymentDateTo(String scheduledPaymentDateTo) {
        this.scheduledPaymentDateTo = scheduledPaymentDateTo;
    }

    public String getOriginalCurrencyCode() {
        return originalCurrencyCode;
    }

    public void setOriginalCurrencyCode(String originalCurrencyCode) {
        this.originalCurrencyCode = originalCurrencyCode;
    }
    
    public String getPayeeName() {
        return payeeName;
    }
    
    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }
    
    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }
    
    public Boolean getIsApproval() {
        return isApproval;
    }
    
    public void setIsApproval(Boolean isApproval) {
        this.isApproval = isApproval;
    }
}
