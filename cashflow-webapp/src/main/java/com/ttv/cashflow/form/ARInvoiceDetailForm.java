package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ARInvoiceDetailForm {

	private BigInteger arInvoiceAccountDetailId;
    private String accountCode;
    private String accountName;
    private BigInteger arInvoiceClassDetailId;
    private String classCode;
    private String className;
    private String approvalCode;
    private String description;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal fxRate;
    private BigDecimal excludeGstConvertedAmount;
    private String gstType;
    private BigDecimal gstRate;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private Boolean isApproval;
    private String isApprovalCode;
    private String isApprovalStatus;
    
    public ARInvoiceDetailForm() {
    }
	public BigInteger getArInvoiceAccountDetailId() {
		return arInvoiceAccountDetailId;
	}
	public void setArInvoiceAccountDetailId(BigInteger arInvoiceAccountDetailId) {
		this.arInvoiceAccountDetailId = arInvoiceAccountDetailId;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public BigInteger getArInvoiceClassDetailId() {
		return arInvoiceClassDetailId;
	}
	public void setArInvoiceClassDetailId(BigInteger arInvoiceClassDetailId) {
		this.arInvoiceClassDetailId = arInvoiceClassDetailId;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}
	public BigDecimal getFxRate() {
		return fxRate;
	}
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}
	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}
	public String getGstType() {
		return gstType;
	}
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}
	public BigDecimal getGstRate() {
		return gstRate;
	}
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}
	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}
	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public Boolean getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}
	public String getIsApprovalCode() {
		return isApprovalCode;
	}
	public void setIsApprovalCode(String isApprovalCode) {
		this.isApprovalCode = isApprovalCode;
	}
	public String getIsApprovalStatus() {
		return isApprovalStatus;
	}
	public void setIsApprovalStatus(String isApprovalStatus) {
		this.isApprovalStatus = isApprovalStatus;
	}

}
