package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class ApInvoiceForm {

    private BigInteger id;
    private String payeeCode;
    private String payeeName;
    private String payeeTypeCode;
    private String payeeTypeName;
    
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar invoiceIssuedDate;
    
    private String apInvoiceNo;
    
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar createdDate;
    // binh.lv
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar bookingDate;
    
    @DateTimeFormat(pattern = "MM/yyyy")
    private Calendar month;
    
    private String invoiceNo;
    private String poNo;
    private String originalCurrencyCode;
    private String approvalCode;
    private BigDecimal fxRate;
    private String claimType;
    private BigDecimal gstRate;
    private String description;
    private String gstType;
    private String accountPayableCode;
    private String accountPayableName;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal excludeGstConvertedAmount;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private String paymentTerm;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar scheduledPaymentDate;
    private List<InvoiceDetailForm> detailForms = new ArrayList<InvoiceDetailForm>();
    private String status;
    private String statusVal;
    public ApInvoiceForm() {
    }
    
    public void setId(BigInteger id) {
        this.id = id;
    }
    
    public BigInteger getId() {
        return id;
    }
    
    public String getPayeeCode() {
        return payeeCode;
    }
    
    public void setPayeeCode(String payeeCode) {
        this.payeeCode = payeeCode;
    }
    
    public String getPayeeName() {
        return payeeName;
    }
    
    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPayeeTypeCode() {
        return payeeTypeCode;
    }
    
    public void setPayeeTypeCode(String payeeTypeCode) {
        this.payeeTypeCode = payeeTypeCode;
    }
    
    public String getPayeeTypeName() {
        return payeeTypeName;
    }
    
    public void setPayeeTypeName(String payeeTypeName) {
        this.payeeTypeName = payeeTypeName;
    }

    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    public Calendar getInvoiceIssuedDate() {
        return invoiceIssuedDate;
    }

    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
        this.invoiceIssuedDate = invoiceIssuedDate;
    }

    public String getApInvoiceNo() {
        return apInvoiceNo;
    }

    public void setApInvoiceNo(String apInvoiceNo) {
        this.apInvoiceNo = apInvoiceNo;
    }

    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    public Calendar getCreatedDate() {
        return createdDate;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getOriginalCurrencyCode() {
        return originalCurrencyCode;
    }

    public void setOriginalCurrencyCode(String originalCurrencyCode) {
        this.originalCurrencyCode = originalCurrencyCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public BigDecimal getFxRate() {
        return fxRate;
    }
    
    public void setFxRate(BigDecimal fxRate) {
        this.fxRate = fxRate;
    }

    public String getClaimType() {
        return claimType;
    }

    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }
    
    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getGstType() {
        return gstType;
    }
    
    public String getAccountPayableCode() {
        return accountPayableCode;
    }

    public void setAccountPayableCode(String accountPayableCode) {
        this.accountPayableCode = accountPayableCode;
    }

    public String getAccountPayableName() {
        return accountPayableName;
    }

    public void setAccountPayableName(String accountPayableName) {
        this.accountPayableName = accountPayableName;
    }

    public BigDecimal getExcludeGstOriginalAmount() {
        return excludeGstOriginalAmount;
    }

    public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
        this.excludeGstOriginalAmount = excludeGstOriginalAmount;
    }
    
    public BigDecimal getExcludeGstConvertedAmount() {
        return excludeGstConvertedAmount;
    }
    
    public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
        this.excludeGstConvertedAmount = excludeGstConvertedAmount;
    }
    
    public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }
    
    public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }
    
    public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
    
    public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }
    
    public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }
    
    public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
    
    public BigDecimal getIncludeGstConvertedAmount() {
        return includeGstConvertedAmount;
    }
    
    public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
        this.includeGstConvertedAmount = includeGstConvertedAmount;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }
    
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }
    
    public Calendar getScheduledPaymentDate() {
        return scheduledPaymentDate;
    }
    
    public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
        this.scheduledPaymentDate = scheduledPaymentDate;
    }
    
    public List<InvoiceDetailForm> getDetailForms() {
        return detailForms;
    }

    public void setDetailForms(List<InvoiceDetailForm> detailForms) {
        this.detailForms = detailForms;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusVal() {
        return statusVal;
    }
	
	public void setStatusVal(String statusVal) {
        this.statusVal = statusVal;
    }

	public Calendar getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}
	
	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}
}
