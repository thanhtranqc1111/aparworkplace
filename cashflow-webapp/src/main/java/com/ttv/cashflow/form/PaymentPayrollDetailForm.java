package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class PaymentPayrollDetailForm {
    
	private BigInteger detailId;
	private BigInteger payrollId;
	private String payrollNo;
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	private Calendar paymentScheduleDate;
	@DateTimeFormat(pattern = Constant.MMYYYY)
    private Calendar month;
	private BigDecimal totalNetPaymentOriginal;
	private BigDecimal totalNetPaymentConverted;
	private BigDecimal paymentRate;
	private String originalCurrencyCode;
	private BigDecimal paidNetPayment;
	private BigDecimal unpaidNetPayment;
	private BigDecimal netPaymentAmountOriginal;
	private BigDecimal netPaymentAmountConverted;
	private Boolean isApproval;
	private BigDecimal paidNetPaymentAmountWaiting;
    private Boolean isPaid;
    private Boolean isPayEnough;
    private String accountPayableCode;
    private String accountPayableName;
    
    public PaymentPayrollDetailForm() {}
    
	public BigInteger getDetailId() {
		return detailId;
	}

	public void setDetailId(BigInteger detailId) {
		this.detailId = detailId;
	}

	public BigInteger getPayrollId() {
		return payrollId;
	}

	public void setPayrollId(BigInteger payrollId) {
		this.payrollId = payrollId;
	}

	public String getPayrollNo() {
		return payrollNo;
	}

	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}

	public Calendar getPaymentScheduleDate() {
		return paymentScheduleDate;
	}

	public void setPaymentScheduleDate(Calendar paymentScheduleDate) {
		this.paymentScheduleDate = paymentScheduleDate;
	}

	public BigDecimal getTotalNetPaymentOriginal() {
		return totalNetPaymentOriginal;
	}

	public void setTotalNetPaymentOriginal(BigDecimal totalNetPaymentOriginal) {
		this.totalNetPaymentOriginal = totalNetPaymentOriginal;
	}

	public BigDecimal getTotalNetPaymentConverted() {
		return totalNetPaymentConverted;
	}

	public void setTotalNetPaymentConverted(BigDecimal totalNetPaymentConverted) {
		this.totalNetPaymentConverted = totalNetPaymentConverted;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getPaidNetPayment() {
		return paidNetPayment;
	}

	public void setPaidNetPayment(BigDecimal paidNetPayment) {
		this.paidNetPayment = paidNetPayment;
	}

	public BigDecimal getUnpaidNetPayment() {
		return unpaidNetPayment;
	}

	public void setUnpaidNetPayment(BigDecimal unpaidNetPayment) {
		this.unpaidNetPayment = unpaidNetPayment;
	}

	public BigDecimal getNetPaymentAmountOriginal() {
		return netPaymentAmountOriginal;
	}

	public void setNetPaymentAmountOriginal(BigDecimal netPaymentAmountOriginal) {
		this.netPaymentAmountOriginal = netPaymentAmountOriginal;
	}

	public BigDecimal getNetPaymentAmountConverted() {
		return netPaymentAmountConverted;
	}

	public void setNetPaymentAmountConverted(BigDecimal netPaymentAmountConverted) {
		this.netPaymentAmountConverted = netPaymentAmountConverted;
	}

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	public BigDecimal getPaidNetPaymentAmountWaiting() {
		return paidNetPaymentAmountWaiting;
	}

	public void setPaidNetPaymentAmountWaiting(BigDecimal paidNetPaymentAmountWaiting) {
		this.paidNetPaymentAmountWaiting = paidNetPaymentAmountWaiting;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
	
	public String getAccountPayableCode() {
		return accountPayableCode;
	}

	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	public String getAccountPayableName() {
		return accountPayableName;
	}

	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}
	
	public Boolean getIsPayEnough() {
		return isPayEnough;
	}

	public void setIsPayEnough(Boolean isPayEnough) {
		this.isPayEnough = isPayEnough;
	}

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((payrollId == null) ? 0 : payrollId.hashCode());
        result = prime * result + ((payrollNo == null) ? 0 : payrollNo.hashCode());
        result = prime * result + ((detailId == null) ? 0 : detailId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaymentPayrollDetailForm other = (PaymentPayrollDetailForm) obj;
        if (payrollId == null) {
            if (other.payrollId != null)
                return false;
        } else if (!payrollId.equals(other.payrollId))
            return false;
        if (payrollNo == null) {
            if (other.payrollNo != null)
                return false;
        } else if (!payrollNo.equals(other.payrollNo))
            return false;
        if (detailId == null) {
            if (other.detailId != null)
                return false;
        } else if (!detailId.equals(other.detailId))
            return false;
        return true;
    }

    

}
