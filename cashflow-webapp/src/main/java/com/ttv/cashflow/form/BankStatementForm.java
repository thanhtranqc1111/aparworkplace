
package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.ttv.cashflow.util.Constant;

/**
 * @author thoai.nh
 * created date Oct 13, 2017
 */
public class BankStatementForm{
	private BigInteger id;
	
	@NotEmpty(message = "{cashflow.bankStatement.message2}")
	private String bankName;
	
	@NotEmpty(message = "{cashflow.bankStatement.message1}")
	private String bankAccountNo;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	@NotNull(message = "{cashflow.bankStatement.message6}")
	private Calendar transactionDate;
	
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	private Calendar valueDate;
	
	private String description1;
	
	private String description2;
	
	private String reference1;
	
	private String reference2;
	
	@NotNull(message = "{cashflow.bankStatement.message7}")
	@NumberFormat(style=Style.CURRENCY)
	private BigDecimal debit;
	
	@NotNull(message = "{cashflow.bankStatement.message8}")
	private BigDecimal credit;
	
	@NotNull(message = "{cashflow.bankStatement.message9}")
	private BigDecimal balance;
	
	private String paymentOrderNo;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	public Calendar getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getDescription1() {
		return description1;
	}
	public void setDescription1(String description1) {
		this.description1 = description1;
	}
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	public String getReference1() {
		return reference1;
	}
	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}
	public String getReference2() {
		return reference2;
	}
	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}
	public BigDecimal getDebit() {
		return debit;
	}
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}
	public BigDecimal getCredit() {
		return credit;
	}
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}
}
