package com.ttv.cashflow.form;

public class SearchForm extends Form {

	protected int currentPage;
	
	protected int nextPage;

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
}
