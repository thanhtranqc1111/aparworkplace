package com.ttv.cashflow.form;

import com.ttv.cashflow.util.Constant.IMPORT_MODE;

public class ImportForm {

	private String fileName;
	private IMPORT_MODE mode;

	public ImportForm() {
		// TODO Auto-generated constructor stub
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public IMPORT_MODE getMode() {
		return mode;
	}

	public void setMode(IMPORT_MODE mode) {
		this.mode = mode;
	}

}
