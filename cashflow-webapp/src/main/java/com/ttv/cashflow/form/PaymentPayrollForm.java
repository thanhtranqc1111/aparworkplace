package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class PaymentPayrollForm {

    private BigInteger id;
    private String paymentOrderNo;
    private String bankName;
    private String bankAccount;
    private BigDecimal includeGstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar valueDate;
    private String bankRefNo;
    private String description;
    private String paymentApprovalNo;
    private String accountPayableCode;
    private String accountPayableName;
    private String originalCurrencyCode;
    private BigDecimal paymentRate;
    private Integer paymentType;
    
    private PaymentPayrollSearchForm searchForm = new PaymentPayrollSearchForm();
    private List<PaymentPayrollDetailForm> detailForms = new ArrayList<>();
    private String status;
    private String statusCode;
    public PaymentPayrollForm() {
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getPaymentOrderNo() {
        return paymentOrderNo;
    }

    public void setPaymentOrderNo(String paymentOrderNo) {
        this.paymentOrderNo = paymentOrderNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public Calendar getValueDate() {
        return valueDate;
    }

    public void setValueDate(Calendar valueDate) {
        this.valueDate = valueDate;
    }

    public String getBankRefNo() {
        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentApprovalNo() {
        return paymentApprovalNo;
    }

    public void setPaymentApprovalNo(String paymentApprovalNo) {
        this.paymentApprovalNo = paymentApprovalNo;
    }

    public String getAccountPayableCode() {
        return accountPayableCode;
    }

    public void setAccountPayableCode(String accountPayableCode) {
        this.accountPayableCode = accountPayableCode;
    }

    public String getAccountPayableName() {
        return accountPayableName;
    }

    public void setAccountPayableName(String accountPayableName) {
        this.accountPayableName = accountPayableName;
    }

    public PaymentPayrollSearchForm getSearchForm() {
		return searchForm;
	}

	public void setSearchForm(PaymentPayrollSearchForm searchForm) {
		this.searchForm = searchForm;
	}

	public List<PaymentPayrollDetailForm> getDetailForms() {
        return detailForms;
    }
    
    public void setDetailForms(List<PaymentPayrollDetailForm> detailForms) {
        this.detailForms = detailForms;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}


}
