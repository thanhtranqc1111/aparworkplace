package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class PaymentForm {

    private BigInteger id;
    private String paymentOrderNo;
    private String bankName;
    private String bankAccount;
    private BigDecimal includeGstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar valueDate;
    private String bankRefNo;
    private String description;
    private String paymentApprovalNo;
    private String accountPayableCode;
    private String accountPayableName;
    private String originalCurrencyCode;
    private BigDecimal paymentRate;
    private String paymentType;
    
    private PaymentSearchForm searchForm = new PaymentSearchForm();
    private List<PaymentApInvoiceDetailForm> detailApForms = new ArrayList<>();
    private List<PaymentPayrollDetailForm> detailPayrollForms = new ArrayList<>();
    private List<PaymentReimDetailForm> detailReimForms = new ArrayList<>();
    
    private String status;
    private String statusCode;
    public PaymentForm() {
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getPaymentOrderNo() {
        return paymentOrderNo;
    }

    public void setPaymentOrderNo(String paymentOrderNo) {
        this.paymentOrderNo = paymentOrderNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public Calendar getValueDate() {
        return valueDate;
    }

    public void setValueDate(Calendar valueDate) {
        this.valueDate = valueDate;
    }

    public String getBankRefNo() {
        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentApprovalNo() {
        return paymentApprovalNo;
    }

    public void setPaymentApprovalNo(String paymentApprovalNo) {
        this.paymentApprovalNo = paymentApprovalNo;
    }

    public String getAccountPayableCode() {
        return accountPayableCode;
    }

    public void setAccountPayableCode(String accountPayableCode) {
        this.accountPayableCode = accountPayableCode;
    }

    public String getAccountPayableName() {
        return accountPayableName;
    }

    public void setAccountPayableName(String accountPayableName) {
        this.accountPayableName = accountPayableName;
    }

    public PaymentSearchForm getSearchForm() {
		return searchForm;
	}

	public void setSearchForm(PaymentSearchForm searchForm) {
		this.searchForm = searchForm;
	}

	public List<PaymentApInvoiceDetailForm> getDetailApForms() {
		return detailApForms;
	}

	public void setDetailApForms(List<PaymentApInvoiceDetailForm> detailApForms) {
		this.detailApForms = detailApForms;
	}

	public List<PaymentPayrollDetailForm> getDetailPayrollForms() {
		return detailPayrollForms;
	}

	public void setDetailPayrollForms(List<PaymentPayrollDetailForm> detailPayrollForms) {
		this.detailPayrollForms = detailPayrollForms;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public List<PaymentReimDetailForm> getDetailReimForms() {
		return detailReimForms;
	}

	public void setDetailReimForms(List<PaymentReimDetailForm> detailReimForms) {
		this.detailReimForms = detailReimForms;
	}

}
