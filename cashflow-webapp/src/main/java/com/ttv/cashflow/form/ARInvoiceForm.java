package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.util.Constant;

public class ARInvoiceForm {
    private BigInteger id;
    private String payerCode;
    private String payerName;
    private String payerTypeCode;
    private String payerTypeName;
    private String invoiceNo;
    private String arInvoiceNo;
    private String originalCurrencyCode;
    private BigDecimal fxRate;
    private String description;
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private Calendar invoiceIssuedDate;
    private String claimType;
    private String accountReceivableCode;
    private String accountReceivableName;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal excludeGstConvertedAmount;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private String status;
    private String statusName;
    
    @DateTimeFormat(pattern = Constant.DDMMYYYY)
    private List<ARInvoiceDetailForm> detailArForms = new ArrayList<ARInvoiceDetailForm>();
	public ARInvoiceForm() {
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getPayerCode() {
		return payerCode;
	}
	public void setPayerCode(String payerCode) {
		this.payerCode = payerCode;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getPayerTypeCode() {
		return payerTypeCode;
	}
	public void setPayerTypeCode(String payerTypeCode) {
		this.payerTypeCode = payerTypeCode;
	}
	public String getPayerTypeName() {
		return payerTypeName;
	}
	public void setPayerTypeName(String payerTypeName) {
		this.payerTypeName = payerTypeName;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getArInvoiceNo() {
		return arInvoiceNo;
	}
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public BigDecimal getFxRate() {
		return fxRate;
	}
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Calendar getInvoiceIssuedDate() {
		return invoiceIssuedDate;
	}
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getAccountReceivableCode() {
		return accountReceivableCode;
	}
	public void setAccountReceivableCode(String accountReceivableCode) {
		this.accountReceivableCode = accountReceivableCode;
	}
	public String getAccountReceivableName() {
		return accountReceivableName;
	}
	public void setAccountReceivableName(String accountReceivableName) {
		this.accountReceivableName = accountReceivableName;
	}
	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}
	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}
	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}
	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public List<ARInvoiceDetailForm> getDetailArForms() {
		return detailArForms;
	}
	public void setDetailArForms(List<ARInvoiceDetailForm> detailArForms) {
		this.detailArForms = detailArForms;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
}
