package com.ttv.cashflow.form;

import java.math.BigDecimal;
import java.math.BigInteger;

public class InvoiceDetailForm {

    private BigInteger apInvoiceAccountDetailId;
    private String accountCode;
    private String accountName;
    private BigInteger apInvoiceClassDetailId;
    private String classCode;
    private String className;
    private String description;
    private String poNo;
    private String approvalCode;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal fxRate;
    private BigDecimal excludeGstConvertedAmount;
    private String gstType;
    private BigDecimal gstRate;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private String apprStatusCode;
    private String apprStatusVal;
    public InvoiceDetailForm() {
    }

    public BigInteger getApInvoiceAccountDetailId() {
		return apInvoiceAccountDetailId;
	}
    
    public void setApInvoiceAccountDetailId(
			BigInteger apInvoiceAccountDetailId) {
		this.apInvoiceAccountDetailId = apInvoiceAccountDetailId;
	}

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public BigInteger getApInvoiceClassDetailId() {
        return apInvoiceClassDetailId;
    }

    public void setApInvoiceClassDetailId(BigInteger apInvoiceClassDetailId) {
		this.apInvoiceClassDetailId = apInvoiceClassDetailId;
	}

    public String getClassCode() {
        return classCode;
    }
    
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }
    
    public String getClassName() {
        return className;
    }
    
    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getPoNo() {
        return poNo;
    }
    
    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }
    
    public String getApprovalCode() {
        return approvalCode;
    }
    
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public BigDecimal getExcludeGstOriginalAmount() {
        return excludeGstOriginalAmount;
    }

    public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
        this.excludeGstOriginalAmount = excludeGstOriginalAmount;
    }
    
    public BigDecimal getExcludeGstConvertedAmount() {
        return excludeGstConvertedAmount;
    }
    
    public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
        this.excludeGstConvertedAmount = excludeGstConvertedAmount;
    }
    
    public String getGstType() {
        return gstType;
    }
    
    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }
    
    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }
    
    public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }
    
    public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }
    
    public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
    
    public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }

    public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
    
    public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }
    
    public BigDecimal getIncludeGstConvertedAmount() {
        return includeGstConvertedAmount;
    }

    public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
        this.includeGstConvertedAmount = includeGstConvertedAmount;
    }

    public BigDecimal getFxRate() {
        return fxRate;
    }

    public void setFxRate(BigDecimal fxRate) {
        this.fxRate = fxRate;
    }
    
	public String getApprStatusCode() {
        return apprStatusCode;
    }
	
	public void setApprStatusCode(String apprStatusCode) {
        this.apprStatusCode = apprStatusCode;
    }
	
	public void setApprStatusVal(String apprStatusVal) {
        this.apprStatusVal = apprStatusVal;
    }
	
	public String getApprStatusVal() {
        return apprStatusVal;
    }
}
