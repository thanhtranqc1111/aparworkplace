package com.ttv.cashflow.form;


public class PaymentSearchForm {

	private String claimType;
    private String monthFrom;
    private String monthTo;
    private String scheduledPaymentDateFrom;
    private String scheduledPaymentDateTo;
    private Boolean isPaid;
    private Boolean isApproval;

    public PaymentSearchForm() {
    }

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getMonthFrom() {
		return monthFrom;
	}

	public void setMonthFrom(String monthFrom) {
		this.monthFrom = monthFrom;
	}

	public String getMonthTo() {
		return monthTo;
	}

	public void setMonthTo(String monthTo) {
		this.monthTo = monthTo;
	}

	public String getScheduledPaymentDateFrom() {
		return scheduledPaymentDateFrom;
	}

	public void setScheduledPaymentDateFrom(String scheduledPaymentDateFrom) {
		this.scheduledPaymentDateFrom = scheduledPaymentDateFrom;
	}

	public String getScheduledPaymentDateTo() {
		return scheduledPaymentDateTo;
	}

	public void setScheduledPaymentDateTo(String scheduledPaymentDateTo) {
		this.scheduledPaymentDateTo = scheduledPaymentDateTo;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

    
}
