package com.ttv.cashflow.scheduler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ttv.cashflow.util.Constant;

@Service("RefeshView")
public class RefeshView {

    private static final Logger logger = LoggerFactory.getLogger(RefeshView.class);

    //PROPERTIES FOR CONNECT TO DATABASE.
    @Value( "${postgresql.default.pre_url}" )
    private String preUrl;
    
    @Value( "${postgresql.default.url}" )
    private String url;
    
    @Value( "${postgresql.default.username}" )
    private String username;
    
    @Value( "${postgresql.default.password}" )
    private String password;
    
    @Value( "${postgresql.default.driver-class-name}" )
    private String driverClassName;
    
    @Value( "${postgresql.cashflow.schema}" )
    private String schema;

    protected void execute() {

        Connection conn = null;
        Statement stmt = null;
        
        List<String> tenantCodes = getListTenantCode();

        for (String tenantCode : tenantCodes) {

            conn = getConnectionFrom(tenantCode);

            try {
                stmt = conn.createStatement();
                
                stmt.executeUpdate("REFRESH MATERIALIZED VIEW account_payable_viewer");
                logger.info("Execute statement on cashflow_"+tenantCode+" database: REFRESH MATERIALIZED VIEW account_payable_viewer");

                stmt.executeUpdate("REFRESH MATERIALIZED VIEW ar_ledger");
                logger.info("Execute statement on cashflow_"+tenantCode+" database: REFRESH MATERIALIZED VIEW ar_ledger");
                
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                // close stmt
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // close conn
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } // end for
    }

    
    /**
     * Get Connection from properties and tenant code, that have configure for connection to
     * database.
     */
    private Connection getConnectionFrom(String tenantCode){
        String dbName = Constant.PREFIX_DB_NAME + tenantCode;
        
        Connection conn = null;
        
        try {
            
            String toUrl = preUrl + dbName + "?currentSchema=" + schema;
            // get connection
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(toUrl, username, password);
            
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    /**
     * Get all tenant code from general database.
     */
    private List<String> getListTenantCode() {
        List<String> codes = new ArrayList<>();
    

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnectionOfDBGeneral();

            String sql = "SELECT code FROM subsidiary";

            stmt = conn.prepareStatement(sql);
            
            //execute
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                String ret = rs.getString("code");
                codes.add(ret);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // close rs
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            
            // close stmt
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            // close conn
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    
        return codes;
    }

    /**
     * Get connection to general database.
     */
    private Connection getConnectionOfDBGeneral() throws IOException, ClassNotFoundException, SQLException {

        Class.forName(driverClassName);
        Connection conn = DriverManager.getConnection(url, username, password);

        return conn;

    }
}
