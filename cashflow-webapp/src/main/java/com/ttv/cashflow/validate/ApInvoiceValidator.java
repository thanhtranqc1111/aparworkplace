package com.ttv.cashflow.validate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ttv.cashflow.form.ApInvoiceForm;

@Component
public class ApInvoiceValidator implements Validator {
    
    @Autowired
    MessageSource messageSource;
    
    @Override
    public boolean supports(Class<?> clazz) {
        return ApInvoiceForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apNo", "apleger.apno.required", "Need to input");
    }
}
