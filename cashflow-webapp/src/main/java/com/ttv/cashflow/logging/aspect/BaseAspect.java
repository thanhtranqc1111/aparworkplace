package com.ttv.cashflow.logging.aspect;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.session.LoginVisited;
import com.ttv.cashflow.util.Constant;

public class BaseAspect {

    protected static final String SQL_INSERT = "set search_path=cashflow; INSERT INTO user_history(first_name, last_name, email, module, action, tenant_code, tenant_name, action_date)VALUES(?,?,?,?,?,?,?,?)";
    
    protected JdbcTemplate jdbcTemplate = null;

    @Autowired
    protected HttpServletRequest httpServletRequest;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private LoginVisited loginVisited;

    /**
     * Save history to database.
     */
    protected void saveHistory(String module, String action) {
        HttpSession session = httpServletRequest.getSession();
        Subsidiary subsidiary = (Subsidiary) session.getAttribute(Constant.SESSSION_NAME_CURRENT_SUBSIDIARY);

        
        getJdbcTemplate().update(SQL_INSERT, 
                                loginVisited.getFirstName(),
                                loginVisited.getLastName(),
                                loginVisited.getEmail(), 
                                module, 
                                action, 
                                subsidiary.getCode(),
                                subsidiary.getName(), 
                                Calendar.getInstance());
    }

    /**
     * Get JdbcTemplate base datasource.
     */
    protected JdbcTemplate getJdbcTemplate() {
        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(dataSource);
        }

        return jdbcTemplate;
    }

}
