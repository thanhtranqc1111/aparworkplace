package com.ttv.cashflow.logging.aspect;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class LoginAspect extends BaseAspect {

    @AfterReturning("execution(public * com.ttv.cashflow.config.security.CustomAuthenticationSuccessHandler.onAuthenticationSuccess(..))")
    public void loginSucces() {
        saveHistory(Constant.LOG_MODULE.LOGIN.getValue(), "Login Successfully");
    }

    @AfterReturning("execution(public * com.ttv.cashflow.controller.HomeController.login(..)) && args(.., model)")
    public void loginFail(ModelAndView model) {
        String error = (String) model.getModel().get("error");
        if (StringUtils.isNotEmpty(error)) {
            getJdbcTemplate().update(SQL_INSERT, null, null, null, "Login", "Login Fail", null, null, Calendar.getInstance());
        }
    }


}
