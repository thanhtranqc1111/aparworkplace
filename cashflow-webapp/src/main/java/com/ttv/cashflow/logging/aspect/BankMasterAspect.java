package com.ttv.cashflow.logging.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class BankMasterAspect extends OnMasterAspect {

    private static final String MODULE = Constant.LOG_MODULE.BANK_MASTER.getValue();
    
    @AfterReturning(pointcut = "execution(public * com.ttv.cashflow.controller.BankMasterController.saveBankMaster(..)) && args(request, bankMaster,..)", returning = "retVal")
    public void logAfterSaveSuccessfully(HttpServletRequest request, BankMaster bankMaster, Object retVal) {
        if (((String) retVal).contains(ResponseUtil.ERROR)) {
            return;
        }

        String mode = request.getParameter("mode");

        if (Constant.MODE.NEW.getValue().equals(mode)) {
            saveHistory(ACTION_TYPE_CREATE, MODULE, bankMaster.getCode());
        } else {
            saveHistory(ACTION_TYPE_MODIFY, MODULE, bankMaster.getCode());
        }
    }

    @AfterReturning(pointcut = "execution(public * com.ttv.cashflow.controller.BankMasterController.deleteBankMaster(..)) && args(request, ..)", returning = "retVal")
    public void logAfterDeleteSuccessfully(HttpServletRequest request, Object retVal) {
        if (((String) retVal).contains(ResponseUtil.ERROR)) {
            return;
        }

        saveHistory(ACTION_TYPE_DELETE, MODULE, request.getParameter("id"));
    }
}
