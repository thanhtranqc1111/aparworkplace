package com.ttv.cashflow.logging.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.form.SubsidiaryForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class ManagementSubsidiaryAspect extends BaseAspect{

    @AfterReturning("execution(public * com.ttv.cashflow.controller.SubsidiaryController.save(..)) && args(subsidiaryForm, ..)")
    public void save(SubsidiaryForm subsidiaryForm) {
        saveHistory(Constant.LOG_MODULE.MANAGEMENT_SUBSIDIARY.getValue(), "Save subsidiary : " + subsidiaryForm.getCode());
    }
    
}
