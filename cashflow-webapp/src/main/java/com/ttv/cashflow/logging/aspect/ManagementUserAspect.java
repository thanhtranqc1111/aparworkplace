package com.ttv.cashflow.logging.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.dto.UserAccountAdminForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class ManagementUserAspect extends BaseAspect{

    @AfterReturning("execution(public * com.ttv.cashflow.controller.UserAccountController.saveForAdmin(..)) && args(request,userAccount, ..)")
    public void saveForAdmin(HttpServletRequest request, UserAccountAdminForm userAccount) {
        saveHistory(Constant.LOG_MODULE.MANAGEMENT_USER.getValue(), "Save user : " + userAccount.getEmail());
    }
    
}
