package com.ttv.cashflow.logging.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.form.PaymentForm;
import com.ttv.cashflow.form.PaymentPayrollForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class PaymentAspect extends BaseAspect{
    
    ///////////////////////////////AP INVOICE/////////////////////////////////

    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.PaymentController.action(..)) && args(paymentForm, request, ..)")
    public void apInvoiceLog(PaymentForm paymentForm, HttpServletRequest request) {
        
        String action = request.getParameter("action");
        
        if("save".equals(action)){
            saveHistory(Constant.LOG_MODULE.PAYMENT_ORDER.getValue(), "Save Payment Order (AP Invoice) No : " + paymentForm.getPaymentOrderNo());
        }
        
    }
    
    
    ///////////////////////////////PAYROLL/////////////////////////////////
    
//    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.PaymentPayrollController.action(..)) && args(paymentForm, request, ..)")
//    public void payrollLog(PaymentPayrollForm paymentForm, HttpServletRequest request) {
//        
//        String action = request.getParameter("action");
//        
//        if("save".equals(action)){
//            saveHistory(Constant.LOG_MODULE.PAYMENT_ORDER.getValue(), "Save Payment Order (Payroll) No : " + paymentForm.getPaymentOrderNo());
//        }
//        
//    }
    
}
