package com.ttv.cashflow.logging.aspect;

import java.util.Map;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class PayrollAspect extends BaseAspect{

    @SuppressWarnings("rawtypes")
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.PayrollController.save(..)) && args(data, ..)", returning="retVal")
    public void save(String data, Object retVal) {
        Gson gson = new Gson();
        Map mapData = gson.fromJson((String) data, Map.class);
        Map mapRet = gson.fromJson((String) retVal, Map.class);
        
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.PAYROLL.getValue(), "Save Payroll No : " + mapData.get("payrollNo"));
        }
        
    }

    
}
