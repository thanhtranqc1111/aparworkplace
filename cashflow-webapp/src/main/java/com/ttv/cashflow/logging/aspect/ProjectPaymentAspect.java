package com.ttv.cashflow.logging.aspect;


import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.util.Constant;


/**
 * @author binh.lv
 * created date May 8, 2018
 */
@Aspect
@Configuration
public class ProjectPaymentAspect extends BaseAspect{

    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ProjectPaymentController.index(..)) && args(monthFrom, monthTo, ..)", returning="retVal")
    public void index(Object retVal, String monthFrom, String monthTo) {
        ModelAndView mav = (ModelAndView) retVal;
        if(!mav.getViewName().equals("redirect:/error")){
            saveHistory(Constant.LOG_MODULE.PROJECT_PAYMENT.getValue(), "Save Project Payroll Details from " + monthFrom + " to " + monthTo);
        }
    }
}
