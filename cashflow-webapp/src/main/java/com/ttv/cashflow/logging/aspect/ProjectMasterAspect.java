package com.ttv.cashflow.logging.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class ProjectMasterAspect extends OnMasterAspect {
    
    private static final String MODULE = Constant.LOG_MODULE.PROJECT_MASTER.getValue();

    @AfterReturning(pointcut = "execution(public * com.ttv.cashflow.controller.ProjectMasterController.savePayeeTypeMaster(..)) && args(request, projectMaster,..)", returning = "retVal")
    public void logAfterSaveSuccessfully(HttpServletRequest request, ProjectMaster projectMaster, Object retVal) {
        if (((String) retVal).contains(ResponseUtil.ERROR)) {
            return;
        }

        String mode = request.getParameter("mode");

        if (Constant.MODE.NEW.getValue().equals(mode)) {
            saveHistory(ACTION_TYPE_CREATE, MODULE, projectMaster.getCode());
        } else {
            saveHistory(ACTION_TYPE_MODIFY, MODULE, projectMaster.getCode());
        }
    }

    @AfterReturning(pointcut = "execution(public * com.ttv.cashflow.controller.ProjectMasterController.deleteProjectMaster(..)) && args(request, ..)", returning = "retVal")
    public void logAfterDeleteSuccessfully(HttpServletRequest request, Object retVal) {
        if (((String) retVal).contains(ResponseUtil.ERROR)) {
            return;
        }

        saveHistory(ACTION_TYPE_DELETE, MODULE, request.getParameter("id"));
    }
}
