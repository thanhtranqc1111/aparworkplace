package com.ttv.cashflow.logging.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.form.ARInvoiceForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class ARInvoiceAspect extends BaseAspect{

    @AfterReturning("execution(public * com.ttv.cashflow.controller.ARInvoiceController.save(..)) && args(arInvoiceForm, ..)")
    public void save(ARInvoiceForm arInvoiceForm) {
        saveHistory(Constant.LOG_MODULE.AR_INVOICE.getValue(), "Save ARInvoice No : " + arInvoiceForm.getArInvoiceNo());
    }
    
    
    @AfterReturning("execution(public * com.ttv.cashflow.controller.ARInvoiceController.delete(..))")
    public void delete() {
        saveHistory(Constant.LOG_MODULE.AR_INVOICE.getValue(), "Delete ARInvoice detail");
    }
    
}
