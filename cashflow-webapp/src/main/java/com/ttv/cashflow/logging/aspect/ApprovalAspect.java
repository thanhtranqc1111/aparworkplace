package com.ttv.cashflow.logging.aspect;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.ttv.cashflow.form.ApprovalPurchaseRequestForm;
import com.ttv.cashflow.form.ApprovalSalesOrderRequestForm;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class ApprovalAspect extends BaseAspect{

    
    /////////////////////////////////ApprovalPurchaseRequest/////////////////////////////////
    
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ApprovalPurchaseRequestController.save(..)) && args(request, approvalForm, ..)", returning="retVal")
    public void saveApprovalPurchaseRequest(Object retVal, HttpServletRequest request, ApprovalPurchaseRequestForm approvalForm) {
        
        @SuppressWarnings("rawtypes")
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.APPROVAL.getValue(), "Save ApprovalPurchaseRequest No : " + approvalForm.getApprovalCode());
        }
    }
    
    
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ApprovalPurchaseRequestController.delete(..)) && args(request, ..)")
    public void deleteApprovalPurchaseRequest(HttpServletRequest request) {
        
        String codes = request.getParameter("id");
        saveHistory(Constant.LOG_MODULE.APPROVAL.getValue(), "Delete ApprovalPurchaseRequest ID : " + codes);
    }
    
    /////////////////////////////////ApprovalSalesOrderRequest/////////////////////////////////
    
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ApprovalSalesOrderRequestController.save(..)) && args(request, approvalForm, ..)", returning="retVal")
    public void saveApprovalSalesOrderRequest(Object retVal, HttpServletRequest request, ApprovalSalesOrderRequestForm approvalForm) {
        
        @SuppressWarnings("rawtypes")
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.APPROVAL.getValue(), "Save ApprovalSalesOrderRequest No : " + approvalForm.getApprovalCode());
        }
    }
    
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ApprovalSalesOrderRequestController.delete(..)) && args(request, ..)")
    public void deleteApprovalSalesOrderRequest(HttpServletRequest request) {
        
        String codes = request.getParameter("id");
        saveHistory(Constant.LOG_MODULE.APPROVAL.getValue(), "Delete ApprovalSalesOrderRequest ID : " + codes);
    }
    
   
}
