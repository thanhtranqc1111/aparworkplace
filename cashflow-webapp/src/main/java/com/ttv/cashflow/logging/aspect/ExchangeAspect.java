package com.ttv.cashflow.logging.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.form.ExportForm;
import com.ttv.cashflow.form.ImportForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class ExchangeAspect extends BaseAspect{

    
    /////////////////////////////////IMPORT/////////////////////////////////
    
    @AfterReturning(pointcut = "execution( * com.ttv.cashflow.controller.ExchangeController.handelImport(..)) && args(request, importForm, ..)")
    public void importLog(HttpServletRequest request, ImportForm importForm) {
        
        String fileName = importForm.getFileName();
        String uri = request.getRequestURI();
        
        String module = "";
        String action = "";
        
        if (uri.contains("/approvalPurchaseRequest/")) {
            module = Constant.LOG_MODULE.APPROVAL.getValue();
            action = "ApprovalPurchaseRequest";
        
        } else if (uri.contains("/approvalSalesOrderRequest/")) {
            module = Constant.LOG_MODULE.APPROVAL.getValue();
            action = "ApprovalSalesOrderRequest";
        
        } else if (uri.contains("/apinvoice/")) {
            module = Constant.LOG_MODULE.AP_INVOICE.getValue();
            action = "AP Invoice";
        
        } else if (uri.contains("/payroll/")) {
            module = Constant.LOG_MODULE.PAYROLL.getValue();
            action = "Payroll";
        
        } else if (uri.contains("/payment/")) {
            module = Constant.LOG_MODULE.PAYMENT_ORDER.getValue();
            action = "Payment Order - Ap Invoice";
            
        } else if (uri.contains("/paymentPayroll/")) {
            module = Constant.LOG_MODULE.PAYMENT_ORDER.getValue();
            action = "Payment Order - Payroll";
        
        } else if (uri.contains("/arinvoice/")) {
            module = Constant.LOG_MODULE.AR_INVOICE.getValue();
            action = "AR Invoice";

        } else if (uri.contains("/bankStatement/")) {
            module = Constant.LOG_MODULE.BANK_STATEMENT.getValue();
            action = "Bank Statement";

        } else if (uri.contains("/approvalTypePurchaseMaster/")) {
            module = Constant.LOG_MODULE.APPROVAL_TYPE_MASTER.getValue();
            action = "Approval Type Purchase Master";

        } else if (uri.contains("/approvalTypeSalesMaster/")) {
            module = Constant.LOG_MODULE.APPROVAL_TYPE_MASTER.getValue();
            action = "Approval Type Sales Master";

        } else if (uri.contains("/accountMaster/")) {
            module = Constant.LOG_MODULE.ACCOUNT_MASTER.getValue();
            action = "Account Master";

        } else if (uri.contains("/bankMaster/")) {
            module = Constant.LOG_MODULE.BANK_MASTER.getValue();
            action = "Bank Master";

        } else if (uri.contains("/bankAccountMaster/")) {
            module = Constant.LOG_MODULE.BANK_ACCOUNT_MASTER.getValue();
            action = "Bank Account Master";

        } else if (uri.contains("/businessTypeMaster/")) {
            module = Constant.LOG_MODULE.BUSINESS_TYPE_MASTER.getValue();
            action = "Business Type Master";

        } else if (uri.contains("/currencyMaster/")) {
            module = Constant.LOG_MODULE.CURRENCY_MASTER.getValue();
            action = "Currency Master";

        } else if (uri.contains("/claimTypeMaster/")) {
            module = Constant.LOG_MODULE.CLAIM_TYPE_MASTER.getValue();
            action = "Claim Type Master";

        } else if (uri.contains("/divisionMaster/")) {
            module = Constant.LOG_MODULE.DIVISION_MASTER.getValue();
            action = "Division Master";

        } else if (uri.contains("/employeeMaster/")) {
            module = Constant.LOG_MODULE.EMPLOYEE_MASTER.getValue();
            action = "Employee Master";

        } else if (uri.contains("/gstMaster/")) {
            module = Constant.LOG_MODULE.GST_MASTER.getValue();
            action = "GST Master";

        } else if (uri.contains("/payeeTypeMaster/")) {
            module = Constant.LOG_MODULE.PAYEE_TYPE_MASTER.getValue();
            action = "Payee Type Master";

        } else if (uri.contains("/payeeMaster/")) {
            module = Constant.LOG_MODULE.PAYEE_MASTER.getValue();
            action = "Payee Master";

        } else if (uri.contains("/projectMaster/")) {
            module = Constant.LOG_MODULE.PROJECT_MASTER.getValue();
            action = "Project Master";
        }
        
        saveHistory(module, String.format("Import %s with file name : %s", action, fileName));
        
    }
    
    
    /////////////////////////////////EXPORT/////////////////////////////////
    
    
    @AfterReturning(pointcut = "execution( * com.ttv.cashflow.controller.ExchangeController.handelExport(..)) && args(request,exportForm, ..)")
    public void exportLog(HttpServletRequest request, ExportForm exportForm) {
        
        String fileName = exportForm.getName() + Constant.XLSX;
        
        String uri = request.getRequestURI();
        
        String module = "";
        String action = "";
        
        
        if (uri.contains("/approvalPurchaseRequest/")) {
            module = Constant.LOG_MODULE.APPROVAL.getValue();
            action = "ApprovalPurchaseRequest";
        
        } else if (uri.contains("/approvalSalesOrderRequest/")) {
            module = Constant.LOG_MODULE.APPROVAL.getValue();
            action = "ApprovalSalesOrderRequest";
        
        } else if (uri.contains("/apinvoice/")) {
            module = Constant.LOG_MODULE.AP_INVOICE.getValue();
            action = "AP Invoice";
        
        } else if (uri.contains("/payroll/")) {
            module = Constant.LOG_MODULE.PAYROLL.getValue();
            action = "Payroll";
        
        } else if (uri.contains("/payment/")) {
            module = Constant.LOG_MODULE.PAYMENT_ORDER.getValue();
            action = "Payment Order - Ap Invoice";
            
        } else if (uri.contains("/paymentPayroll/")) {
            module = Constant.LOG_MODULE.PAYMENT_ORDER.getValue();
            action = "Payment Order - Payroll";
        
        } else if (uri.contains("/arinvoice/")) {
            module = Constant.LOG_MODULE.AR_INVOICE.getValue();
            action = "AR Invoice";

        } else if (uri.contains("/bankStatement/")) {
            module = Constant.LOG_MODULE.BANK_STATEMENT.getValue();
            action = "Bank Statement";

        } else if (uri.contains("/approvalTypePurchaseMaster/")) {
            module = Constant.LOG_MODULE.APPROVAL_TYPE_MASTER.getValue();
            action = "Approval Type Purchase Master";

        } else if (uri.contains("/approvalTypeSalesMaster/")) {
            module = Constant.LOG_MODULE.APPROVAL_TYPE_MASTER.getValue();
            action = "Approval Type Sales Master";

        } else if (uri.contains("/accountMaster/")) {
            module = Constant.LOG_MODULE.ACCOUNT_MASTER.getValue();
            action = "Account Master";

        } else if (uri.contains("/bankMaster/")) {
            module = Constant.LOG_MODULE.BANK_MASTER.getValue();
            action = "Bank Master";

        } else if (uri.contains("/bankAccountMaster/")) {
            module = Constant.LOG_MODULE.BANK_ACCOUNT_MASTER.getValue();
            action = "Bank Account Master";

        } else if (uri.contains("/businessTypeMaster/")) {
            module = Constant.LOG_MODULE.BUSINESS_TYPE_MASTER.getValue();
            action = "Business Type Master";

        } else if (uri.contains("/currencyMaster/")) {
            module = Constant.LOG_MODULE.CURRENCY_MASTER.getValue();
            action = "Currency Master";

        } else if (uri.contains("/claimTypeMaster/")) {
            module = Constant.LOG_MODULE.CLAIM_TYPE_MASTER.getValue();
            action = "Claim Type Master";

        } else if (uri.contains("/divisionMaster/")) {
            module = Constant.LOG_MODULE.DIVISION_MASTER.getValue();
            action = "Division Master";

        } else if (uri.contains("/employeeMaster/")) {
            module = Constant.LOG_MODULE.EMPLOYEE_MASTER.getValue();
            action = "Employee Master";

        } else if (uri.contains("/gstMaster/")) {
            module = Constant.LOG_MODULE.GST_MASTER.getValue();
            action = "GST Master";

        } else if (uri.contains("/payeeTypeMaster/")) {
            module = Constant.LOG_MODULE.PAYEE_TYPE_MASTER.getValue();
            action = "Payee Type Master";

        } else if (uri.contains("/payeeMaster/")) {
            module = Constant.LOG_MODULE.PAYEE_MASTER.getValue();
            action = "Payee Master";

        } else if (uri.contains("/projectMaster/")) {
            module = Constant.LOG_MODULE.PROJECT_MASTER.getValue();
            action = "Project Master";
        }

        saveHistory(module, String.format("Export %s with file name : %s", action, fileName));
    }
    
   
}
