package com.ttv.cashflow.logging.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.ttv.cashflow.form.ApInvoiceForm;
import com.ttv.cashflow.util.Constant;

@Aspect
@Configuration
public class APInvoiceAspect extends BaseAspect{

    @AfterReturning("execution(public * com.ttv.cashflow.controller.APInvoiceController.save(..)) && args(apInvoiceForm, ..)")
    public void save(ApInvoiceForm apInvoiceForm) {
        saveHistory(Constant.LOG_MODULE.AP_INVOICE.getValue(), "Save APInvoice No : " + apInvoiceForm.getApInvoiceNo());
    }
    
    
    @AfterReturning("execution(public * com.ttv.cashflow.controller.APInvoiceController.delete(..))")
    public void delete() {
        saveHistory(Constant.LOG_MODULE.AP_INVOICE.getValue(), "Delete APInvoice detail");
    }
    
}
