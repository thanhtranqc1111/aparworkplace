package com.ttv.cashflow.logging.aspect;

import java.math.BigInteger;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class CollectionManagementAspect extends BaseAspect {

    @SuppressWarnings("rawtypes")
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.ReceiptOrderController.applyMatching(..)) && args(request, bankStatementId, ..)", returning="retVal")
    public void save(Object retVal, HttpServletRequest request, BigInteger bankStatementId) {
        
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.COLLECTION_MANAGEMENT.getValue(), "Matching bank transaction no : " + bankStatementId);
        }
    }
    
    @SuppressWarnings("rawtypes")
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.BankStatementController.resetReceiptOrderNo(..)) && args(id)", returning="retVal")
    public void unmatching(Object retVal, BigInteger id) {
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.COLLECTION_MANAGEMENT.getValue(), "Unmatching bank transaction no : " + id);
        }
    }
    
    
}
