package com.ttv.cashflow.logging.aspect;


public class OnMasterAspect extends BaseAspect {
    
    protected static final int ACTION_TYPE_CREATE = 1;
    protected static final int ACTION_TYPE_MODIFY = 2;
    protected static final int ACTION_TYPE_DELETE = 3;

    /**
     * Save history to master table.
     */
    protected void saveHistory(int type, String module, String code) {

        switch (type) {
        case ACTION_TYPE_CREATE:
            saveHistory(module, String.format("Create new %s code : %s", module, code));
            break;

        case ACTION_TYPE_MODIFY:
            saveHistory(module, String.format("Modify %s code : %s", module, code));
            break;

        case ACTION_TYPE_DELETE:
            saveHistory(module, String.format("Delete %s code : %s", module, code));
            break;

        }

    }
    

}
