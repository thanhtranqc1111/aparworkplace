package com.ttv.cashflow.logging.aspect;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Aspect
@Configuration
public class BankStatementAspect extends BaseAspect{

    @SuppressWarnings("rawtypes")
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.BankStatementController.save(..)) && args(request, dataList, ..)", returning="retVal")
    public void save(Object retVal, HttpServletRequest request, String dataList) {
        
        JsonArray jsonArray = new JsonParser().parse(dataList).getAsJsonArray();
        
        //Get list transaction number.
        List<String> transactionNo = new ArrayList<>(); 
        for(int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            transactionNo.add(jsonObject.get("id").getAsString());
        }
        
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.BANK_STATEMENT.getValue(), "Save Bank Statement on transaction no : " + transactionNo);
        }
    }
    
    @SuppressWarnings("rawtypes")
    @AfterReturning(pointcut="execution(public * com.ttv.cashflow.controller.BankStatementController.resetPaymentOrderNo(..)) && args(id)", returning="retVal")
    public void unmatching(Object retVal, BigInteger id) {
        Map mapRet = new Gson().fromJson((String) retVal, Map.class);
        if(mapRet.get("STATUS").equals(ResponseUtil.SUCCESS)){
            saveHistory(Constant.LOG_MODULE.BANK_STATEMENT.getValue(), "Unmatching bank transaction no : " + id);
        }
    }
    
    
}
