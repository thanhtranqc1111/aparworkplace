package com.ttv.cashflow.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.ReimbursementExportComponent;
import com.ttv.cashflow.component.ReimbursementImportComponent;
import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;
import com.ttv.cashflow.domain.GstMaster;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.dto.ReimbursementDTO;
import com.ttv.cashflow.dto.ReimbursementDetailsDTO;
import com.ttv.cashflow.helper.ClaimTypeMasterDataHelper;
import com.ttv.cashflow.helper.CurrencyMasterDataHelper;
import com.ttv.cashflow.helper.EmployeeMasterDataHelper;
import com.ttv.cashflow.service.ApprovalSalesOrderRequestService;
import com.ttv.cashflow.service.ReimbursementService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;


/**
 * @author thoai.nh
 * created date Apr 6, 2018
 */
@Controller()
@RequestMapping("/reimbursement")
@PrototypeScope
public class ReimbursementController extends ExchangeController{
	@Autowired
	private GstMasterDAO gstMasterDAO;
	@Autowired
	private ReimbursementService reimbursementService;
	@Autowired
    private ClaimTypeMasterDataHelper claimTypeMasterHelper;
	@Autowired
	private CurrencyMasterDataHelper currencyMasterHelper;
	@Autowired
	private ApprovalSalesOrderRequestService approvalSalesOrderRequestService;
	@Autowired
	private EmployeeMasterDataHelper employeeMasterDataHelper;
	@Autowired
    private ReimbursementExportComponent exportComponent;
	@Autowired
	private ReimbursementImportComponent importComponent;
	private static final Logger LOGGER = LoggerFactory.getLogger(ReimbursementController.class);
	
	@RequestMapping(value = {"/list"}, method = RequestMethod.GET)
	public ModelAndView mapList(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("reimbursement/list");
		model.addObject("mapStatus", systemValueHelper.toMap(Constant.PREFIX_API));
		model.addObject("mapEmployee", employeeMasterDataHelper.toMap());
		return model;
	}
	
	@GetMapping(value = {"/detail", "/detail/{reimbursementNo}"})
    public ModelAndView mapDetail(@PathVariable Optional<String> reimbursementNo, HttpServletRequest httpServletRequest) {
		Reimbursement reimbursement = null;
		if(reimbursementNo.isPresent()) {
			reimbursement = reimbursementService.findReimbursementByReimbursementNo(reimbursementNo.get());
			if(reimbursement == null)
			{
				return new ModelAndView("redirect:/");
			}
		}
		else {
			reimbursement = new Reimbursement();
			reimbursement.setBookingDate(Calendar.getInstance());
			reimbursement.setMonth(Calendar.getInstance());
			reimbursement.setReimbursementNo(reimbursementService.getReimbursementNo());
			reimbursement.setStatus(Constant.API_NOT_PAID);
			reimbursement.setFxRate(new BigDecimal("1"));
			reimbursement.setClaimType(Constant.CLAIM_TYPE_REIMBURSEMENT);
			reimbursement.setId(BigInteger.ZERO);
			Subsidiary crSubsidiary = (Subsidiary) httpServletRequest.getSession().getAttribute(Constant.SESSSION_NAME_CURRENT_SUBSIDIARY);
			if(crSubsidiary != null) {
				reimbursement.setOriginalCurrencyCode(crSubsidiary.getCurrency());
			}
		}
		ReimbursementDTO reimbursementDTO = new ReimbursementDTO();
		reimbursementService.copyDomainToDTO(reimbursement, reimbursementDTO);
		setSystemValueForReimbursementDTO(reimbursementDTO);
		return prepareModelForReimbursementDetails(reimbursementDTO);
	}
	
	@GetMapping(value = { "/copy/{reimbursementNo}" })
	public ModelAndView mapCopy(@PathVariable String reimbursementNo) {
		Reimbursement reimbursement = reimbursementService.findReimbursementByReimbursementNo(reimbursementNo);
		if (reimbursement == null) {
			return new ModelAndView("redirect:/");
		} else {
			ReimbursementDTO reimbursementDTO = reimbursementService.clone(reimbursement);
			setSystemValueForReimbursementDTO(reimbursementDTO);
			return prepareModelForReimbursementDetails(reimbursementDTO);
		}
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public String save(@ModelAttribute("data") String data, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(data).getAsJsonObject();
			ReimbursementDTO reimbursementDTO = reimbursementService.parseReimbursementJSON(jsonObject);
			rs = reimbursementService.saveReimbursement(reimbursementDTO, false).intValue();
		} catch (Exception e) {
			LOGGER.info("Save reimbursement error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		if(rs > 0)
		{
			LOGGER.info("Save reimbursement suscess. {}", data);
		}
		else {
			LOGGER.info("Save reimbursement error: {}.", rs);
		}
		return ResponseUtil.createAjaxSuccess(rs);
	}
	
	@RequestMapping(value = "/api/getAllPagingReimbursement", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingReimbursement(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
			String orderBy = request.getParameter("order[0][dir]");
			Integer status = NumberUtils.toInt(request.getParameter("status"));
			String fromReimbursementNo = request.getParameter("fromReimbursementNo");
			String toReimbursementNo = request.getParameter("toReimbursementNo");
			Calendar calendarMonthFrom = CalendarUtil.getCalendar(request.getParameter("fromMonth"), "MM/yyyy");
			Calendar calendarMonthTo = CalendarUtil.getCalendar(request.getParameter("toMonth"), "MM/yyyy");
			String employeeCode = request.getParameter("employeeCode");
			jsonData = reimbursementService.findReimbursementPaging(startpage, lengthOfpage, calendarMonthFrom, calendarMonthTo,
																    fromReimbursementNo, toReimbursementNo, employeeCode, orderColumn, orderBy, status);
		} catch (Exception e) {
			LOGGER.info("getAllPagingReimbursement error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	private void setSystemValueForReimbursementDTO(ReimbursementDTO reimbursementDTO) {
		/*
         * load value for status code
         */
		for(ReimbursementDetailsDTO reimbursementDetailsDTO:  reimbursementDTO.getReimbursementDetailses()) {
			 //load approval status
	       	 if(reimbursementDetailsDTO.getIsApproval()) {
	       		reimbursementDetailsDTO.setApprStatusCode(Constant.APR_APPROVAL);
	       		reimbursementDetailsDTO.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_APPROVAL).getValue());
	       		//load approval amount
	            Set<ApprovalSalesOrderRequest> setApproval = approvalSalesOrderRequestService.findApprovalByApprovalCode(reimbursementDetailsDTO.getApprovalCode());
	            if(!setApproval.isEmpty()){
	               ApprovalSalesOrderRequest approval = setApproval.iterator().next();
                   reimbursementDetailsDTO.setApprovalIncludeGstOriginalAmount(approval.getIncludeGstOriginalAmount());
                }
            }
            else
            {
            	reimbursementDetailsDTO.setApprStatusCode(Constant.APR_WAITING);
            	reimbursementDetailsDTO.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());
            }
		}
        Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_PR);
        reimbursementDTO.setStatusVal(mapStatus.get(reimbursementDTO.getStatus()));
	}
	
	private ModelAndView prepareModelForReimbursementDetails(ReimbursementDTO reimbursementDTO) {
		ModelAndView mav = new ModelAndView("reimbursement/detail");
		mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
		mav.addObject("currencyMap", currencyMasterHelper.toMap());
        mav.addObject("reimbursement", reimbursementDTO);
        mav.addObject("mapApprovalStatus", systemValueHelper.toMap(Constant.PREFIX_APR));
        mav.addObject("mapReimbursementStatus", systemValueHelper.toMap(Constant.PREFIX_PR));
        Set<GstMaster> setGst = gstMasterDAO.findAllGstMasters();
        if(reimbursementDTO.getId().equals(BigInteger.ZERO) && !setGst.isEmpty()) {
        	Iterator<GstMaster> iterator = setGst.iterator();
        	GstMaster gstMaster = iterator.next();
        	reimbursementDTO.setGstRate(gstMaster.getRate());
        }
        mav.addObject("listGstMaster", setGst);
		return mav;
	}
	
	@Override
	public String getRelatePath(String type) {
	    return systemValueHelper.get(type, "REI").getValue();
	}

	@Override
	public ImportComponent getImportComponent() {
		return importComponent;
	}

	@Override
	public ExportComponent getExportComponent() {
		return exportComponent;
	}
	
}
