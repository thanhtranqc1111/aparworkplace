package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.DivisionImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.service.DivisionMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Controller()
@RequestMapping("/admin/divisionMaster")
@PrototypeScope
public class DivisionMasterController extends ExchangeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DivisionMasterController.class);

    @Autowired
    private DivisionMasterService divisionMasterService;

    @Autowired
    private DivisionImportComponent divisionImportComponent;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView divisionMasterView = new ModelAndView("master/division");
        divisionMasterView.addObject("divisionMasterForm", new DivisionMaster());
        return divisionMasterView;
    }

    @RequestMapping(value = "/api/getAllPagingDivision", method = { RequestMethod.GET }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingDivision(final HttpServletRequest request, Model model) {
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer numOfRecords = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");

            return divisionMasterService.findDivisionMasterJson(startpage, numOfRecords, orderColumn, orderBy, keyword);
        } catch (Exception e) {
            LOGGER.info("getAllPagingDivision failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveDivisionMaster(HttpServletRequest request, @ModelAttribute("divisionMasterForm") @Valid DivisionMaster divisionMaster) {
        try {
            String mode = request.getParameter("mode");

            if (Constant.MODE.NEW.getValue().equals(mode)) {
                DivisionMaster existingDivision = divisionMasterService.findDivisionMasterByCode(divisionMaster.getCode());

                if (existingDivision != null) {
                    LOGGER.info("The code {} is existed in db.", divisionMaster.getCode());
                    return "redirect:/admin/divisionMaster/list";
                } else {
                    divisionMasterService.createDivisionMaster(divisionMaster);
                }
            } else if (Constant.MODE.EDIT.getValue().equals(mode)) {
                divisionMasterService.updateDivisionMaster(divisionMaster);
            }

            return "redirect:/admin/divisionMaster/list";
        } catch (Exception e) {
            LOGGER.info("Save failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteDivisionMaster(final HttpServletRequest request, Model model) {
        try {
            String[] codes = request.getParameterValues("id");
            divisionMasterService.deleteDivisionMasterByCodes(codes);
            LOGGER.info("Delete suscessfully, Division Master: {}.", codes.toString());

        } catch (Exception e) {
            LOGGER.info("deleteDivisionMaster failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return "redirect:/admin/divisionMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return divisionImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "DI").getValue();
    }

    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
