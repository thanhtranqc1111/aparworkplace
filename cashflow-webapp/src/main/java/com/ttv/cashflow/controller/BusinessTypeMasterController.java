package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.BusinessTypeMasterImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.BusinessTypeMaster;
import com.ttv.cashflow.service.BusinessTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 06, 2017
 */
@Controller()
@RequestMapping("/admin/businessTypeMaster")
@PrototypeScope
public class BusinessTypeMasterController extends ExchangeController{
	
	@Autowired
	private BusinessTypeMasterService businessTypeMasterService;
	
	@Autowired
	private BusinessTypeMasterImportComponent businessTypeMasterImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(BusinessTypeMasterController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/business_type");
       payeeMasterView.addObject("businessTypeMasterForm", new BusinessTypeMaster());
       payeeMasterView.addObject("adminPage", "businessTypeMaster");
       return payeeMasterView;
    }
	
	@RequestMapping(value = "/api/getAllPagingBusinessType", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingBusinessType(final HttpServletRequest request, Model model) {
    	String jBusinessTypeMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jBusinessTypeMaster = businessTypeMasterService.findBusinessTypeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jBusinessTypeMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBusinessTypeMaster(HttpServletRequest request, @ModelAttribute("BusinessTypeMasterForm") @Valid BusinessTypeMaster BusinessTypeMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 BusinessTypeMaster existBusinessType  = businessTypeMasterService.findBusinessTypeMasterByPrimaryKey(BusinessTypeMaster.getCode());
                     if (existBusinessType != null)
                         return "redirect:/admin/businessTypeMaster/list"; 
                 } 
                 businessTypeMasterService.saveBusinessTypeMaster(BusinessTypeMaster);
                 logger.info("Save suscess, Business Type Master: {}.", BusinessTypeMaster.toString());
                 
                return "redirect:/admin/businessTypeMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteBusinessTypeMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                businessTypeMasterService.deleteBusinessTypeMasterByCodes(codes);
                logger.info("Delete suscess, Business Type Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/businessTypeMaster/list"; 
    }

    @Override
    public ImportComponent getImportComponent(){
    	return businessTypeMasterImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "BTM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
