package com.ttv.cashflow.controller;

import java.math.BigInteger;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.form.ApprovalPurchaseRequestForm;
import com.ttv.cashflow.service.ApprovalPurchaseRequestService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.ResponseUtil;
@RestController
@RequestMapping("/restApi/approvalPurchaseRequest")
public class ApprovalPurchaseRestController {
	@Autowired
	private ApprovalPurchaseRequestDAO dao;
	@Autowired
	private ApprovalPurchaseRequestService approvalService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalPurchaseRequestController.class);
	
	@GetMapping(value = "/findAll")
	public ResponseEntity<Set<ApprovalPurchaseRequest>> listAllUsers() {
		return new ResponseEntity<Set<ApprovalPurchaseRequest>>(dao.findAllApprovalPurchaseRequests(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/findOneByApprovalCode/{approvalCode}")
    public ResponseEntity<ApprovalPurchaseRequest> getUser(@PathVariable("approvalCode") String approvalCode) {
        Set<ApprovalPurchaseRequest> set = dao.findApprovalPurchaseRequestByApprovalCode(approvalCode);
        if(set.isEmpty()) {
        	return new ResponseEntity<ApprovalPurchaseRequest>(HttpStatus.NOT_FOUND);
        }
        else {
        	return new ResponseEntity<ApprovalPurchaseRequest>(set.iterator().next(), HttpStatus.OK);
        }
    }
	
	@GetMapping(value = "/findOneById/{id}")
    public ResponseEntity<ApprovalPurchaseRequest> getUser(@PathVariable("id") BigInteger id) {
		ApprovalPurchaseRequest ap = dao.findApprovalPurchaseRequestById(id);
        if(ap == null) {
        	return new ResponseEntity<ApprovalPurchaseRequest>(HttpStatus.NOT_FOUND);
        }
        else {
        	return new ResponseEntity<ApprovalPurchaseRequest>(ap, HttpStatus.OK);
        }
    }
	
	@PostMapping(value = "/save")
    public String save(@RequestBody  @Valid ApprovalPurchaseRequestForm approvalForm, BindingResult result) {
		try {
			if(result.hasErrors()) {
				return ResponseUtil.createAjaxError(result.getAllErrors());
			}
			else {
				ApprovalPurchaseRequest approval = new ApprovalPurchaseRequest();
				BeanUtils.copyProperties(approvalForm, approval);
				return approvalService.saveApprovalPurchaseRequestForRestAPI(approval);
			}
		} catch (Exception e) {
			LOGGER.info("save approval error: {}.", e);
			return ResponseUtil.createAjaxError(0, e.getMessage());
		}
    }
	
	@PostMapping(value = "/updateStatus")
    public String updateStatus(HttpEntity<String> httpEntity) {
		try {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(httpEntity.getBody()).getAsJsonObject();
			String taskId = null;
			if(jsonObject.has("taskId")) {
				taskId = jsonObject.get("taskId").getAsString();
			}
			return approvalService.updateStatusForRestAPI(jsonObject.get("approvalCode").getAsString(), 
														  jsonObject.get("status").getAsString(), 
														  CalendarUtil.getCalendar(jsonObject.get("approvedDate").getAsString(), "yyy-MM-dd"), taskId);	
			
		} catch (Exception e) {
			LOGGER.info("updateStatus approval error: {}.", e);
			return ResponseUtil.createAjaxError(0, e.getMessage());
		}
    }
}
