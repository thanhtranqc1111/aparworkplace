package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApprovalTypeSalesMasterImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;
import com.ttv.cashflow.service.ApprovalTypeSalesMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;


/**
 * @author thoai.nh
 * created date Feb 6, 2018
 */
@Controller()
@RequestMapping("/admin/approvalTypeSalesMaster")
@PrototypeScope
public class ApprovalTypeSalesMasterController extends ExchangeController{
	private static final String APPROVAL_TYPE_MASTER_VIEW = "/list";
	
	@Autowired
	private ApprovalTypeSalesMasterService approvalTypeMasterService;
	
	@Autowired
	private ApprovalTypeSalesMasterImportComponent approvalTypeMasterImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(ApprovalTypeSalesMasterController.class);
	
    @RequestMapping(value = APPROVAL_TYPE_MASTER_VIEW, method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/approval-type-sales");
       payeeMasterView.addObject("approvalTypeMasterForm", new ApprovalTypeSalesMaster());
       payeeMasterView.addObject("adminPage", "approvalTypeSalesMaster");
       return payeeMasterView;
    }
    
    @RequestMapping(value = "/api/getAllPagingApprovalType", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingApprovalType(final HttpServletRequest request, Model model) {
    	String jGstMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jGstMaster = approvalTypeMasterService.findApprovalTypeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jGstMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveApprovalTypeMaster(HttpServletRequest request, @ModelAttribute("approvalTypeMasterForm") @Valid ApprovalTypeSalesMaster approvalTypeMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 ApprovalTypeSalesMaster existApprovalType  = approvalTypeMasterService.findApprovalTypeSalesMasterByPrimaryKey(approvalTypeMaster.getCode());
                     if (existApprovalType != null)
                         return "redirect:/admin/approvalTypeSalesMaster/list"; 
                 } 
                 approvalTypeMasterService.saveApprovalTypeSalesMaster(approvalTypeMaster);
                 logger.info("Save suscess, Approval Type Sales Master: {}.", approvalTypeMaster.toString());
                 
                 return "redirect:/admin/approvalTypeSalesMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteApprovalTypeMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                approvalTypeMasterService.deleteApprovalTypeMasterByCodes(codes);
                logger.info("Delete suscess, Approval Type Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/approvalTypeSalesMaster/list"; 
    }

    @Override
    public ImportComponent getImportComponent(){
    	return approvalTypeMasterImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "ATS").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
	