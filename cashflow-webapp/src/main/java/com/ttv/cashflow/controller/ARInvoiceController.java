package com.ttv.cashflow.controller;

import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ArInvoiceExportComponent;
import com.ttv.cashflow.component.ArInvoiceImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApprovalSalesOrderRequestDAO;
import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;
import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.form.ARInvoiceDetailForm;
import com.ttv.cashflow.form.ARInvoiceForm;
import com.ttv.cashflow.helper.ClaimTypeMasterDataHelper;
import com.ttv.cashflow.helper.CurrencyMasterDataHelper;
import com.ttv.cashflow.helper.PayeePayerMasterDataHelper;
import com.ttv.cashflow.helper.SystemValueHelper;
import com.ttv.cashflow.service.ArInvoiceAccountDetailsService;
import com.ttv.cashflow.service.ArInvoiceClassDetailsService;
import com.ttv.cashflow.service.ArInvoiceService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Controller
@RequestMapping("/arinvoice")
@PrototypeScope
public class ARInvoiceController extends ExchangeController {
    
    @Autowired
    private PayeePayerMasterDataHelper payeePayerMasterHelper;

    @Autowired
    private SystemValueHelper systemValueHelper; 
    
    @Autowired
    private ArInvoiceExportComponent exportComponent;
    
    @Autowired
    private ArInvoiceService arInvoiceService;
    
    @Autowired
    private ArInvoiceImportComponent importComponent;
    
    @Autowired
    private ClaimTypeMasterDataHelper claimTypeMasterHelper;

    @Autowired
    private CurrencyMasterDataHelper currencyMasterHelper;
    
    @Autowired
    private ArInvoiceDAO arInvoiceDAO;
    
    @Autowired
    private AccountMasterDAO accountMasterDAO;

    @Autowired
    private GstMasterDAO gstMasterDAO;
    
    @Autowired
    private ArInvoiceClassDetailsService arInvoiceClassDetailsService;
    
    @Autowired
    private ArInvoiceAccountDetailsService arInvoiceAccountDetailsService;
    
    @Autowired
    private ApprovalSalesOrderRequestDAO approvalSalesOrderRequestDAO;
    
    private static final Logger logger = LoggerFactory.getLogger(ARInvoiceController.class);
    
    @RequestMapping(value = "/list")
    public ModelAndView listARInvoice() {
    	 ModelAndView model = new ModelAndView("arinvoice/list");
    	 Set<PayeePayerMaster> lstPayeePayer = payeePayerMasterHelper.list();
 		 Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_ARI);
    	 
    	 model.addObject("lstPayeePayer", lstPayeePayer);
    	 model.addObject("mapStatus", mapStatus);
         return model;
    }
    
    @RequestMapping(value = "/api/getAllPagingARInvoice", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingARInvoice(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			String orderBy = request.getParameter("order[0][dir]");
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));

			String fromMonth = request.getParameter("fromMonth");
			String toMonth = request.getParameter("toMonth");
			String fromArNo = request.getParameter("fromArNo");
			String toArNo = request.getParameter("toArNo");
			String payerName = request.getParameter("payerName");
			String invoiceNo = request.getParameter("invoiceNo");
			String status = request.getParameter("status");
			 
			jsonData = arInvoiceService.findARInvoiceJSON(startpage, lengthOfpage, fromMonth, toMonth,
					fromArNo, toArNo, payerName, invoiceNo, status, orderColumn, orderBy);
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
    }

    @GetMapping(value = {"/detail", "/detail/{arNo}", "/detail/{arNo}/{approvalCode}"})
    public ModelAndView detail(@PathVariable Optional<String> arNo, @PathVariable Optional<String> approvalCode) {
        ModelAndView mav = new ModelAndView("arinvoice/detail");
        mav.addObject("currencyMap", currencyMasterHelper.toMap());
        mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
        
        ARInvoiceForm arInvoiceForm = new ARInvoiceForm();

        ArInvoice arInvoice = null;

        if (arNo.isPresent()) {
        	Set<ArInvoice>  listArInvoice = arInvoiceDAO.findArInvoiceByArInvoiceNo(arNo.get());
        	if(listArInvoice.size() > 0) {
        	    Map<String, String> approvalMap = new HashMap<>();
	            Iterator<ArInvoice> iter = listArInvoice.iterator();
	            arInvoice = iter.next();
	
	            // Copy from entity to form
	            BeanUtils.copyProperties(arInvoice, arInvoiceForm);
	
	            // Update detail for ap - invoice
	            List<ARInvoiceDetailForm> invoiceDetailForms = new ArrayList<ARInvoiceDetailForm>();
	
	            List<ARInvoiceDetail> invoiceDetails = arInvoiceService.getARInvoiceDetail(arInvoice.getId());
	            for (ARInvoiceDetail invoiceDetail : invoiceDetails) {
	                ARInvoiceDetailForm invoiceDetailForm = new ARInvoiceDetailForm();
	                BeanUtils.copyProperties(invoiceDetail, invoiceDetailForm);
	                if(invoiceDetail.getIsApproval() != null && invoiceDetail.getIsApproval()) {
	                	invoiceDetailForm.setIsApprovalCode(Constant.APR_APPROVAL);
	                	invoiceDetailForm.setIsApprovalStatus(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_APPROVAL).getValue());
	                }
	                else
	                {
	                	invoiceDetailForm.setIsApprovalCode(Constant.APR_WAITING);
	                	invoiceDetailForm.setIsApprovalStatus(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());
	                }
	                invoiceDetailForms.add(invoiceDetailForm);
	                
	                //build map approval code - amount
                    String apCode = invoiceDetail.getApprovalCode();
                    Iterator<ApprovalSalesOrderRequest> approvalIter = approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestByApprovalCode(apCode).iterator();
	                if(approvalIter.hasNext()){
	                    ApprovalSalesOrderRequest approval = approvalIter.next();
                        approvalMap.put(apCode, ObjectUtils.toString(approval.getIncludeGstOriginalAmount()));
                    }
	            }
	            
	            //add approval(as json string) to model.
                mav.addObject("approvalJson", new Gson().toJson(approvalMap));
	
	            Map<String, String> invStatus = systemValueHelper.toMap(Constant.PREFIX_ARI);
	            arInvoiceForm.setStatusName(invStatus.get(arInvoiceForm.getStatus()));
	            arInvoiceForm.setDetailArForms(invoiceDetailForms);
        	}
        	else {
        		return new ModelAndView("redirect:/");
        	}
        } else {
            arInvoiceForm.setArInvoiceNo(arInvoiceDAO.getArInvoiceNo(Calendar.getInstance()));
        }
        
        mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
        mav.addObject("listGstMaster", gstMasterDAO.findAllGstMasters());
        mav.addObject("arInvoiceForm", arInvoiceForm);
        if(approvalCode.isPresent()){
    	    mav.addObject("approvalCode", approvalCode.get());
    	}
        mav.addObject("apr002", systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING));
        return mav;
    }
    
    

    @PostMapping(value = "/save")
    public String save(@ModelAttribute("arInvoiceForm") ARInvoiceForm arInvoiceForm, BindingResult result, RedirectAttributes redirectAttributes) {
        ArInvoice arInvoice = new ArInvoice();
        BeanUtils.copyProperties(arInvoiceForm, arInvoice);

        if(arInvoice.getId() == null){
        	arInvoice.setStatus(Constant.ARI_NOT_RECEIVED);
        } else {
        	arInvoice.setStatus(arInvoiceForm.getStatus());
        }
        
        Calendar month = (Calendar) arInvoiceForm.getInvoiceIssuedDate().clone();
    	month.set(Calendar.DAY_OF_MONTH, 1);
    	arInvoice.setMonth(month);
        arInvoice.setModifiedDate(Calendar.getInstance());

        arInvoiceService.saveArInvoice(arInvoice);
        List<ARInvoiceDetailForm> invoiceDetailForms = arInvoiceForm.getDetailArForms();
        removeDetailInvalid(invoiceDetailForms);
        
        doSaveArInvoiceDetails(invoiceDetailForms, arInvoice);
        logger.info("Save success, AR Invoice: {}.", arInvoice.toString());
        
        redirectAttributes.addFlashAttribute("message", arInvoice.getArInvoiceNo() + " saved successfully.");
        return "redirect:detail/" + arInvoice.getArInvoiceNo();
    }
    
    private void doSaveArInvoiceDetails(List<ARInvoiceDetailForm> ARInvoiceDetailForms, ArInvoice arInvoice){
        Map<String, List<ARInvoiceDetailForm>> mapAccountWithDetail = getMapAccountCodeWithDetail(ARInvoiceDetailForms);
        
        //Save detail
        for (Map.Entry<String, List<ARInvoiceDetailForm>> entry : mapAccountWithDetail.entrySet()) {

            String accountCode = entry.getKey();
            List<ARInvoiceDetailForm> invoiceDetailFormBelongAccountCode = entry.getValue();
            BigInteger arInvoiceAccountDetailId = getArInvoiceAccountDetailId(accountCode, ARInvoiceDetailForms);

            /////Do save invoice account/////
            ArInvoiceAccountDetails arInvoiceAccountDetails = new ArInvoiceAccountDetails();
            
            BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
            BigDecimal excludeGstConvertedAmount = new BigDecimal(0); 
            BigDecimal gstOriginalAmount = new BigDecimal(0);
            BigDecimal gstConvertedAmount = new BigDecimal(0);
            BigDecimal includeGstOriginalAmount = new BigDecimal(0);
            BigDecimal includeGstConvertedAmount = new BigDecimal(0);
            
            ARInvoiceDetailForm invoiceDetailForm = new ARInvoiceDetailForm();
            for (int i = 0; i < invoiceDetailFormBelongAccountCode.size(); i++) {
                invoiceDetailForm = invoiceDetailFormBelongAccountCode.get(i);

                excludeGstOriginalAmount = excludeGstOriginalAmount.add(invoiceDetailForm.getExcludeGstOriginalAmount());
                excludeGstConvertedAmount = excludeGstConvertedAmount.add(invoiceDetailForm.getExcludeGstConvertedAmount());
                gstOriginalAmount = gstOriginalAmount.add(invoiceDetailForm.getGstOriginalAmount());
                gstConvertedAmount = gstConvertedAmount.add(invoiceDetailForm.getGstConvertedAmount());
                includeGstOriginalAmount = includeGstOriginalAmount.add(invoiceDetailForm.getIncludeGstOriginalAmount());
                includeGstConvertedAmount = includeGstConvertedAmount.add(invoiceDetailForm.getIncludeGstConvertedAmount());
            }
            
            arInvoiceAccountDetails.setId(arInvoiceAccountDetailId);
            arInvoiceAccountDetails.setArInvoice(arInvoice);
            arInvoiceAccountDetails.setAccountCode(accountCode);
            arInvoiceAccountDetails.setAccountName(invoiceDetailForm.getAccountName());
            arInvoiceAccountDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
            arInvoiceAccountDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
            arInvoiceAccountDetails.setGstOriginalAmount(gstOriginalAmount);
            arInvoiceAccountDetails.setGstConvertedAmount(gstConvertedAmount);
            arInvoiceAccountDetails.setIncludeGstOriginalAmount(includeGstOriginalAmount);
            arInvoiceAccountDetails.setIncludeGstConvertedAmount(includeGstConvertedAmount);
            arInvoiceAccountDetails.setCreatedDate(Calendar.getInstance());
            arInvoiceAccountDetails.setModifiedDate(Calendar.getInstance());

            arInvoiceAccountDetailsService.saveArInvoiceAccountDetails(arInvoiceAccountDetails);

            //save invoice class detail flowing account
            doSaveArInvoiceClassDetail(invoiceDetailFormBelongAccountCode, arInvoiceAccountDetails);
        }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request) {
        //
        String arInvoiceId = request.getParameter("arInvoiceId");
        String arInvoiceAccountDetailId = request.getParameter("arInvoiceAccountDetailId");
        String arInvoiceClassDetailId = request.getParameter("arInvoiceClassDetailId");
        
        //
        ArInvoice arInvoice = arInvoiceService.findArInvoiceByPrimaryKey(new BigInteger(arInvoiceId));
        ArInvoiceAccountDetails arInvoiceAccountDetails = arInvoiceAccountDetailsService.findArInvoiceAccountDetailsByPrimaryKey(new BigInteger(arInvoiceAccountDetailId));
        ArInvoiceClassDetails arinvoiceclassdetails = arInvoiceClassDetailsService.findArInvoiceClassDetailsByPrimaryKey(new BigInteger(arInvoiceClassDetailId));
        
        //
        BigDecimal clsExcludeGstOriginalAmount = arinvoiceclassdetails.getExcludeGstOriginalAmount();
        BigDecimal clsExcludeGstConvertedAmount = arinvoiceclassdetails.getExcludeGstConvertedAmount();
        BigDecimal clsGstOriginalAmount = arinvoiceclassdetails.getGstOriginalAmount();
        BigDecimal clsGstConvertedAmount = arinvoiceclassdetails.getGstConvertedAmount();
        BigDecimal clsIncludeGstOriginalAmount = arinvoiceclassdetails.getIncludeGstOriginalAmount();
        BigDecimal clsIncludeGstConvertedAmount = arinvoiceclassdetails.getIncludeGstConvertedAmount();
        
        //UPDATE ARINVOICE ACCOUNT DETAIL
        BigDecimal accExcludeGstOriginalAmount =  arInvoiceAccountDetails.getExcludeGstOriginalAmount();
        BigDecimal accExcludeGstConvertedAmount = arInvoiceAccountDetails.getExcludeGstConvertedAmount();
        BigDecimal accGstConvertedAmount = arInvoiceAccountDetails.getGstConvertedAmount();
        BigDecimal accIncludeGstConvertedAmount = arInvoiceAccountDetails.getIncludeGstConvertedAmount();
        
        arInvoiceAccountDetails.setExcludeGstOriginalAmount(accExcludeGstOriginalAmount.subtract(clsExcludeGstOriginalAmount));
        arInvoiceAccountDetails.setExcludeGstConvertedAmount(accExcludeGstConvertedAmount.subtract(clsExcludeGstConvertedAmount));
        arInvoiceAccountDetails.setGstConvertedAmount(accGstConvertedAmount.subtract(clsGstConvertedAmount));
        arInvoiceAccountDetails.setIncludeGstConvertedAmount(accIncludeGstConvertedAmount.subtract(clsIncludeGstConvertedAmount));
        
        arInvoiceAccountDetailsService.saveArInvoiceAccountDetails(arInvoiceAccountDetails);
        
        //UPDATE ARINVOICE CLASS DETAIL
        BigDecimal arExcludeGstOriginalAmount =  arInvoice.getExcludeGstOriginalAmount();
        BigDecimal arExcludeGstConvertedAmount = arInvoice.getExcludeGstConvertedAmount();
        BigDecimal arGstConvertedAmount = arInvoice.getGstConvertedAmount();
        BigDecimal arGstOriginalAmount = arInvoice.getGstOriginalAmount();
        BigDecimal arIncludeGstConvertedAmount = arInvoice.getIncludeGstConvertedAmount();
        BigDecimal arIncludeGstOriginalAmount = arInvoice.getIncludeGstOriginalAmount();
        
        arInvoice.setExcludeGstOriginalAmount(arExcludeGstOriginalAmount.subtract(clsExcludeGstOriginalAmount));
        arInvoice.setExcludeGstConvertedAmount(arExcludeGstConvertedAmount.subtract(clsExcludeGstConvertedAmount));
        
        arInvoice.setGstOriginalAmount(arGstOriginalAmount.subtract(clsGstOriginalAmount));
        arInvoice.setGstConvertedAmount(arGstConvertedAmount.subtract(clsGstConvertedAmount));
        
        arInvoice.setIncludeGstOriginalAmount(arIncludeGstOriginalAmount.subtract(clsIncludeGstOriginalAmount));
        arInvoice.setIncludeGstConvertedAmount(arIncludeGstConvertedAmount.subtract(clsIncludeGstConvertedAmount));
        
        arInvoiceService.saveArInvoice(arInvoice);

        //DELETE
        arInvoiceClassDetailsService.deleteArInvoiceClassDetails(arinvoiceclassdetails);
        
        //
        logger.info("Delete success, AR Invoice Class Detail: {}.", arinvoiceclassdetails.toString());
        return ResponseUtil.ajaxSuccess();
        
    }
    
    @GetMapping(value = {"/copy/{arNo}"})
    public ModelAndView copy(@PathVariable Optional<String> arNo) {
        ModelAndView mav = new ModelAndView("arinvoice/detail");
        mav.addObject("currencyMap", currencyMasterHelper.toMap());
        mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
        
        ARInvoiceForm arInvoiceForm = new ARInvoiceForm();

        ArInvoice arInvoice = null;

        if (arNo.isPresent()) {
        	Set<ArInvoice>  listArInvoice = arInvoiceDAO.findArInvoiceByArInvoiceNo(arNo.get());
        	if(listArInvoice.size() > 0) {
        	    Map<String, String> approvalMap = new HashMap<>();
	            Iterator<ArInvoice> iter = listArInvoice.iterator();
	            arInvoice = iter.next();
	
	            // Copy from entity to form
	            BeanUtils.copyProperties(arInvoice, arInvoiceForm);
	            
                //Set new ApInvoiceNo
	            arInvoiceForm.setArInvoiceNo(arInvoiceDAO.getArInvoiceNo());
	
	            // Update detail for ap - invoice
	            List<ARInvoiceDetailForm> invoiceDetailForms = new ArrayList<ARInvoiceDetailForm>();
	
	            List<ARInvoiceDetail> invoiceDetails = arInvoiceService.getARInvoiceDetail(arInvoice.getId());
	            for (ARInvoiceDetail invoiceDetail : invoiceDetails) {
	                ARInvoiceDetailForm invoiceDetailForm = new ARInvoiceDetailForm();
	                BeanUtils.copyProperties(invoiceDetail, invoiceDetailForm);

	                invoiceDetailForm.setArInvoiceAccountDetailId(null);
	                invoiceDetailForm.setArInvoiceClassDetailId(null);
                	invoiceDetailForm.setIsApprovalCode(Constant.APR_WAITING);
                	invoiceDetailForm.setIsApprovalStatus(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());
	                invoiceDetailForms.add(invoiceDetailForm);
	                
	                //build map approval code - amount
                    String apCode = invoiceDetail.getApprovalCode();
                    Iterator<ApprovalSalesOrderRequest> approvalIter = approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestByApprovalCode(apCode).iterator();
	                if(approvalIter.hasNext()){
	                    ApprovalSalesOrderRequest approval = approvalIter.next();
                        approvalMap.put(apCode, ObjectUtils.toString(approval.getIncludeGstOriginalAmount()));
                    }
	            }
	            
	            //add approval(as json string) to model.
                mav.addObject("approvalJson", new Gson().toJson(approvalMap));
	
	            Map<String, String> invStatus = systemValueHelper.toMap(Constant.PREFIX_ARI);
	            arInvoiceForm.setStatusName(invStatus.get(arInvoiceForm.getStatus()));
	            arInvoiceForm.setDetailArForms(invoiceDetailForms);
        	}
        	else {
        		return new ModelAndView("redirect:/");
        	}
        } else {
            arInvoiceForm.setArInvoiceNo(arInvoiceDAO.getArInvoiceNo(Calendar.getInstance()));
        }
        
        mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
        mav.addObject("listGstMaster", gstMasterDAO.findAllGstMasters());

        //Ignore ID
        arInvoiceForm.setId(null);
        mav.addObject("arInvoiceForm", arInvoiceForm);
        mav.addObject("isCopy", true);
        mav.addObject("apr002", systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING));
        return mav;
    }
    
    
    private void removeDetailInvalid(List<ARInvoiceDetailForm> invoiceDetailForms) {
		Iterator<ARInvoiceDetailForm> iter = invoiceDetailForms.iterator();

		while (iter.hasNext()) {
			ARInvoiceDetailForm object = iter.next();
			if (StringUtils.isEmpty(object.getAccountCode())) {
				iter.remove();
			}
		}
	}
    
    /**
     * Build map account code flowing list detail. 
     */
    private Map<String, List<ARInvoiceDetailForm>> getMapAccountCodeWithDetail(List<ARInvoiceDetailForm> invoiceDetailForms) {
        Map<String, List<ARInvoiceDetailForm>> mapAccountWithDetail = new HashMap<>();
        
        for(ARInvoiceDetailForm invoiceDetailForm : invoiceDetailForms){
            String accountCode = invoiceDetailForm.getAccountCode();
            List<ARInvoiceDetailForm> invoiceDetailFormFiters = new ArrayList<>();
            
            if (!mapAccountWithDetail.containsKey(accountCode)) {
                
                for(ARInvoiceDetailForm invoiceDetailForm2 : invoiceDetailForms){
                    String accountCode2 = invoiceDetailForm2.getAccountCode();
                    if(accountCode.equals(accountCode2)){
                        invoiceDetailFormFiters.add(invoiceDetailForm2);
                    }
                }
                
                mapAccountWithDetail.put(accountCode, invoiceDetailFormFiters);
            }
        }
        
        return mapAccountWithDetail;
    }
    
    /**
     * Get ApInvoiceAccountDetailId belong accountCode from the form.
     */
    private BigInteger getArInvoiceAccountDetailId(String accountCode, List<ARInvoiceDetailForm> invoiceDetailForms) {
        for(ARInvoiceDetailForm form : invoiceDetailForms){
            if(accountCode.equals(form.getAccountCode())){
                if(Objects.nonNull(form.getArInvoiceAccountDetailId())){
                    return form.getArInvoiceAccountDetailId();
                }
            }
        }
        
        return null;
    }
    
    private void doSaveArInvoiceClassDetail(List<ARInvoiceDetailForm> invoiceDetailForms, ArInvoiceAccountDetails account) {
        for(ARInvoiceDetailForm invoiceDetailForm : invoiceDetailForms){
            ArInvoiceClassDetails arInvoiceClassDetails = new ArInvoiceClassDetails();
            
            BeanUtils.copyProperties(invoiceDetailForm, arInvoiceClassDetails);
            
            if(null == invoiceDetailForm.getArInvoiceClassDetailId()){
            	arInvoiceClassDetails.setCreatedDate(Calendar.getInstance());
            } else {
            	arInvoiceClassDetails.setId(invoiceDetailForm.getArInvoiceClassDetailId());
            }
            
            arInvoiceClassDetails.setArInvoiceAccountDetails(account);
            arInvoiceClassDetails.setIsApproval(hasText(arInvoiceClassDetails.getApprovalCode()));
            arInvoiceClassDetails.setModifiedDate(Calendar.getInstance());
            //
            arInvoiceClassDetailsService.saveArInvoiceClassDetails(arInvoiceClassDetails);
        }
        
        
    }
    
    @GetMapping(value = "/getListApprovalAvailable")
    @ResponseBody
    public List<Map<String, String>> getApprovalAvailable(@RequestParam String keyword, @RequestParam String issueDate) {
        List<Map<String, String>> approvals = new ArrayList<Map<String, String>>();
        Map<String,String> approvalMap;
        
        //List<Approval> approvalList = approvalDAO.findApprovalAvailable(keyword, invoiceNo, Constant.APPROVAL_CLASS_AR_INVOICE);
        List<ApprovalSalesOrderRequest> approvalList = approvalSalesOrderRequestDAO.findApprovalAvailable(keyword, issueDate);
        
        for(ApprovalSalesOrderRequest approvalGet : approvalList){
            approvalMap = new LinkedHashMap<String, String>();
            
            String amountAsStr = StringUtils.EMPTY;
            BigDecimal amount = approvalGet.getIncludeGstOriginalAmount();
            if (amount != null) {
                amountAsStr = amount.toString();
            }
            
            
            approvalMap.put("approvalNo", approvalGet.getApprovalCode());
            approvalMap.put("amount", amountAsStr);
            approvalMap.put("approvalDate", CalendarUtil.toString(approvalGet.getApplicationDate()));
            approvalMap.put("applicant", approvalGet.getApplicant());
            approvalMap.put("purpose", approvalGet.getPurpose());
            //
            approvals.add(approvalMap);
        }
        
        return approvals;
    }


    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "ARI").getValue();
    }

    @Override
    public ImportComponent getImportComponent() {
        return importComponent;
    }

    @Override
    public ExportComponent getExportComponent() {
        return exportComponent;
    }
}