package com.ttv.cashflow.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.form.ExportForm;
import com.ttv.cashflow.form.ImportForm;
import com.ttv.cashflow.form.UploadForm;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.Constant.IMPORT_MODE;
import com.ttv.cashflow.vo.ExportReturn;
import com.ttv.cashflow.vo.FileExportDetail;
import com.ttv.cashflow.vo.FileImportDetail;
import com.ttv.cashflow.vo.ImportReturn;
import com.ttv.cashflow.vo.LogFileDetail;


public abstract class ExchangeController extends BaseController {
    
    protected static final String SEPARATOR = System.getProperty("file.separator");
    private static final String IMPORT_INDEX = "import";
    private static final String EXPORT_INDEX = "export";
    
    protected IMPORT_MODE importMode;
    
    private String fileName;
    
    public abstract String getRelatePath(String type);
    
    public abstract ImportComponent getImportComponent();
    
    public abstract ExportComponent getExportComponent();
    
    @GetMapping(value = "/import")
    public ModelAndView importIndex(ModelMap model, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("import/index"); 
        UploadForm uploadForm = new UploadForm();
        ImportForm importForm = new ImportForm();
        
        // list import file
        List<FileImportDetail> fileImportDetails = getListFileImport(); 
        fileImportDetails.sort((o1, o2) -> o2.getUploadedDate().compareTo(o1.getUploadedDate()));   
        
        // list import log
		List<LogFileDetail> logFileDetails = getListLogFileImport();
		logFileDetails.sort((o1, o2) -> o2.getImportedDate().compareTo(o1.getImportedDate()));
		
		mav.addObject("uploadForm", uploadForm);
		mav.addObject("fileImportDetails", fileImportDetails);
		mav.addObject("importForm", importForm);
		mav.addObject("logFileDetails", logFileDetails);
        return mav;
    }
    
    @PostMapping(value = "import.upload")
    public String uploadImportFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        StringBuilder message = new StringBuilder();
        Locale locale = LocaleContextHolder.getLocale();
        try {
            if (validate(file, message)) {

                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(getImportDir(), file.getOriginalFilename())));

                outputStream.write(file.getBytes());
                outputStream.flush();
                outputStream.close();
                
                message.append(messageSource.getMessage("cashflow.common.upload.fileUpload", new Object[]{file.getOriginalFilename()}, locale));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        redirectAttributes.addFlashAttribute("activeMessage", message);

        return redirectImportIndex();
    }
    
    
    @PostMapping(value = "/import.do")
    public String handelImport(HttpServletRequest request, @ModelAttribute("importForm") ImportForm importForm, RedirectAttributes redirectAttributes) {
    	Locale locale = LocaleContextHolder.getLocale();
    	// just select file to do import
        if(null != importForm.getFileName()) {
            setImportMode(importForm.getMode());
            setFileName(importForm.getFileName());
            
            ImportReturn imp = doImport(getImportWorkBook(importForm.getFileName()));
            
            String templateMsg = messageSource.getMessage("cashflow.common.logImportMessage", new Object[]{}, locale);
            String detailMsg = messageSource.getMessage("cashflow.common.logDetailImportMessage", new Object[]{}, locale);
            
            redirectAttributes.addFlashAttribute("message", imp.getMessageLog(templateMsg, detailMsg));
        } else {
            redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.import.requiredFile", new Object[]{}, locale));
        }
        
        return redirectImportIndex();
    }

	/**
	 * Download template import.
	 */
	@GetMapping(value = "/downTemplate")
	public void downloadTemplate(HttpServletRequest request, HttpServletResponse response) {
		// import template real path
		String temPath = getImportTemplateDir();
		String fileName = getImportComponent().getName().replaceAll(" ", "_") + "_IMPORT_TEMPLATE" + (temPath.endsWith(Constant.XLS) ? Constant.XLS : Constant.XLSX); 
		doDownload(response, fileName, temPath, Constant.CONTENT_TYPE_EXCEL);
	}
	
	/**
	 * Download import file.
	 */
	@GetMapping(value = "/import.download")
    public void downloadImportFile(@RequestParam("fileName") String fileName, RedirectAttributes redirectAttributes, HttpServletResponse response) {
		String path = getImportDir() + SEPARATOR + fileName;
		doDownload(response, fileName, path, Constant.CONTENT_TYPE_EXCEL);
	}
	
	/**
	 * Download import log file.
	 */
	@GetMapping(value = "/import.downLog")
	public void downloadImportLogFile(@RequestParam("fileName") String fileName, RedirectAttributes redirectAttributes, HttpServletResponse response){
		String path = getImportLogDir() + SEPARATOR + fileName;
		doDownload(response, fileName, path, Constant.CONTENT_TYPE_LOG);
	}
    
	/**
	 * Delete import file
	 */
    @GetMapping(value = "/import.delete")
    public String deleteImportFile(@RequestParam("fileName") String fileName, RedirectAttributes redirectAttributes){
    	Locale locale = LocaleContextHolder.getLocale();
        File file = new File(getImportDir(), fileName);
        if(!file.exists()){
            redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.delete.notExist", new Object[]{fileName}, locale));
        } else {
            if(file.delete()){
                redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.delete.fileImport", new Object[]{fileName}, locale));
            }
        }
        return redirectImportIndex();
    }
    
    /**
	 * Delete import log file
	 */
	@GetMapping(value = "/import.deleteLog")
	public String deleteImportLogFile(@RequestParam("fileName") String fileName, RedirectAttributes redirectAttributes){
		Locale locale = LocaleContextHolder.getLocale();
		File file = new File(getImportLogDir(), fileName);
		if(!file.exists()){
			redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.delete.notExist", new Object[]{fileName}, locale));
		} else {
			if(file.delete()){
				redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.delete.logFileImport", new Object[]{fileName}, locale));
			}
		}
		
		return redirectImportIndex();
	}
    
    private ImportReturn doImport(Workbook workbook) {
        
        ImportReturn importReturn = new ImportReturn();
        ImportComponent importComponent = getImportComponent();
        
        //get file name
        String fileName = getFileName().substring(0, getFileName().lastIndexOf("."));
		importComponent.setLogPath(getImportLogDir() + SEPARATOR + fileName);
		
        switch (getImportMode()) {
	        case MERGE:
	            importReturn = importComponent.doMerge(workbook);
	            break;
	
	        case UPDATE:
	            importReturn = importComponent.doUpdate(workbook);
	            break;
	
	        case REPLACE:
	            importReturn = importComponent.doReplace(workbook);
	            break;
	
	        case DELETE:
	            importReturn = importComponent.doDelete(workbook);
	            break;
	
	        default:
	            break;
        }
        
        return importReturn;
    }
    
    /////////////////////-----------EXPORT-----------/////////////////////
    
    @GetMapping(value = "/export")
    public ModelAndView exportIndex(ModelMap model, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("export/index"); 
        ExportForm exportForm = new ExportForm();
    	List<FileExportDetail> fileExportDetails = getListFileExport();
        
        //Order by date decrement
        fileExportDetails.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));

        //
        mav.addObject("exportForm", exportForm);
        mav.addObject("fileExportDetails", fileExportDetails);
        
        return mav;
    }
    
    @PostMapping(value = "/export.do")
    public String handelExport(HttpServletRequest request, @ModelAttribute("exportForm") ExportForm exportForm, RedirectAttributes redirectAttributes) {
        String fileName = exportForm.getName();
        Locale locale = LocaleContextHolder.getLocale();
        
        // just select file to do import
        if (StringUtils.isNotEmpty(fileName)) {

            ExportReturn ret = doExport(getExportWorkBook(), fileName, request);

            String builder = messageSource.getMessage("cashflow.common.logExportMessage", new Object[]{fileName}, locale);
            
            redirectAttributes.addFlashAttribute("message", ret.getMessageLog(builder));
        } else {
            redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.export.requiredName", new Object[]{fileName}, locale));
        }
        
        return redirectExportIndex(buildQueryString(request, "name"));
    }

    @GetMapping(value = "/export.download")
    public void downloadExportFile(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        String path = getExportDir() + SEPARATOR + fileName;
        
        doDownload(response, fileName, path, Constant.CONTENT_TYPE_EXCEL);
    }
    
    @GetMapping(value = "/export.delete")
    public String deleteExportFile(@RequestParam("fileName") String fileName, HttpServletRequest request, RedirectAttributes redirectAttributes){
        File file = new File(getExportDir(), fileName);
        Locale locale = LocaleContextHolder.getLocale();
        
        if(!file.exists()){
            redirectAttributes.addFlashAttribute("activeMessage", messageSource.getMessage("cashflow.common.delete.notExist", new Object[]{fileName}, locale));
        } else {
            if(file.delete()){
                redirectAttributes.addFlashAttribute("activeMessage",  messageSource.getMessage("cashflow.common.delete.fileImport", new Object[]{fileName}, locale));
            }
        }
        
        return redirectExportIndex(buildQueryString(request, "fileName"));
    }
    
    private ExportReturn doExport(Workbook workbook, String fileName, HttpServletRequest request) {
        
        ExportComponent exportComponent = getExportComponent();
        
        exportComponent.setFilePath(getExportDir() + SEPARATOR + fileName + Constant.XLSX);
        
        return exportComponent.doExport(workbook, request);
    }
    
    /////////////////////-----------END EXPORT-----------/////////////////////
    
    /**
     * Get directory import
     */
    private String getImportDir() {
        String realDir = servletContext.getRealPath("/") + getRelatePath(Type.IMPORT);
        
        File dir = new File(realDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        
        return realDir;
    }
    
    /**
     * Get directory export
     */
    protected String getExportDir() {
        String realDir = servletContext.getRealPath("/") + getRelatePath(Type.EXPORT);
        
        File dir = new File(realDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        
        return realDir;
    }
    
    /**
     * validate file upload
     * @param file
     * @param message
     * @return
     */
    private boolean validate(MultipartFile file, StringBuilder message) {
    	Locale locale = LocaleContextHolder.getLocale();
    	
        if (file.isEmpty()) {
        	message.append(messageSource.getMessage("cashflow.common.upload.requiredFile", new Object[]{}, locale));
            
            return false;
        }
        
        String fileName = file.getOriginalFilename().toLowerCase();
        if (!(fileName.endsWith(".xlsx") || fileName.endsWith(".xls"))) {
            message.append(messageSource.getMessage("cashflow.common.upload.excelRequiredFile", new Object[]{}, locale));
            
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Get list files in import folder 
     */
    private List<FileImportDetail> getListFileImport() {
        File folder = new File(getImportDir());
        File[] listOfFiles = folder.listFiles();

        List<FileImportDetail> fileImportDetails = new ArrayList<>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                FileImportDetail detail = new FileImportDetail();

                detail.setFileName(file.getName());
                detail.setFileSize(Double.parseDouble(new DecimalFormat("##.##").format((double)file.length() / 1024)));

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(file.lastModified());
                detail.setUploadedDate(cal);

                fileImportDetails.add(detail);
            }
        }
        return fileImportDetails;
    }
    
    /**
     * Get list files in export folder 
     */
    private List<FileExportDetail> getListFileExport() {
        File folder = new File(getExportDir());
        
        File[] listOfFiles = folder.listFiles();

        List<FileExportDetail> fileExportDetails = new ArrayList<>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                FileExportDetail detail = new FileExportDetail();

                detail.setName(file.getName());
                detail.setSize(Double.parseDouble(new DecimalFormat("##.##").format((double)file.length() / 1024)));

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(file.lastModified());
                detail.setDate(cal);

                fileExportDetails.add(detail);
            }
        }
        
        return fileExportDetails;
    }
    
	/**
	 * Get list log import files in import
	 */
	private List<LogFileDetail> getListLogFileImport() {
		File folder = new File(getImportLogDir());
		File[] listOfFiles = folder.listFiles();

		List<LogFileDetail> logFileDetail = new ArrayList<>();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				LogFileDetail detail = new LogFileDetail();

				detail.setLogName(file.getName());

				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(file.lastModified());
				detail.setImportedDate(cal);

				logFileDetail.add(detail);
			}
		}
		return logFileDetail;
	}
	
	/**
	 * get log import real path 
	 * @return
	 */
	public String getImportLogDir() {
		String logDir = servletContext.getRealPath("/") + getRelatePath(Type.IMPORT_LOG);
		
		File dir = new File(logDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		return logDir;
	}
	
	/**
	 * Do download file. 
	 */
	private String doDownload(HttpServletResponse response, String fileName, String path, String contentType){
        String message = "";
        Locale locale = LocaleContextHolder.getLocale();
        
        ServletOutputStream outStream = null;
        Path file = Paths.get(path);
        
        if (Files.exists(file)) {
            try {
            	fileName  = URLEncoder.encode(fileName,"UTF-8");
            	response.setContentType(contentType);
            	response.setCharacterEncoding("UTF-8");
            	response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            	
                outStream = response.getOutputStream();
                // copies all bytes from a file to an output stream
                Files.copy(file, outStream);

                // flushes output stream
                outStream.flush();
                message = messageSource.getMessage("cashflow.common.downloaded", new Object[]{fileName}, locale);
            } catch (UnsupportedEncodingException e) {
				e.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }  finally {
                if (Objects.nonNull(outStream)) {
                    try {
                        outStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            message = messageSource.getMessage("cashflow.common.fileNotFound", new Object[]{fileName}, locale);
        }
        
        return message;
	}
	
	/**
	 * get import template real path 
	 * @return
	 */
	private String getImportTemplateDir() {
		String realDir = servletContext.getRealPath("/") + getRelatePath(Type.TEMPLATE);
		
		File dir = new File(realDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		return realDir;
	}
	
    
    private Workbook getImportWorkBook(String fileName) {

        Workbook workbook = null;
        try {
            FileInputStream inputStream = new FileInputStream(getImportDir() + SEPARATOR + fileName);
            workbook = WorkbookFactory.create(inputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return workbook;
    }
    
    /**
     * get import template real path 
     * @return
     */
    private String getExportTemplateDir() {
        String realDir = servletContext.getRealPath("/") + getRelatePath(Type.EXPORT_TEMPLATE);
        
        File dir = new File(realDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        
        return realDir;
    }
    
    private XSSFWorkbook getExportWorkBook() {
        XSSFWorkbook workbook = null;
        try {
            workbook = new XSSFWorkbook(new FileInputStream(new File(getExportTemplateDir())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return workbook;

    }
    
    private String redirectImportIndex(){
        return "redirect:" + IMPORT_INDEX;
    }
    
    private String redirectExportIndex(){
        return redirectExportIndex(null);
    }
    
    private String redirectExportIndex(String queryString) {
        String redirectURL = "redirect:" + EXPORT_INDEX;
        if (!StringUtils.isEmpty(queryString)) {
            redirectURL += "?" + queryString;
        }
        return redirectURL;
    }
    
    private String buildQueryString(HttpServletRequest request, String... ignoreParameterNames) {
        Enumeration<String> parameterNames = request.getParameterNames();
        StringBuilder queryString = new StringBuilder();
        while(parameterNames.hasMoreElements()){
            
            String parameterName = parameterNames.nextElement();
                
            if(Arrays.asList(ignoreParameterNames).contains(parameterName)){
                continue;
            }
            queryString.append(parameterName).append("=").append(request.getParameter(parameterName)).append("&");
        }
        return queryString.toString();
    }
    
    public IMPORT_MODE getImportMode() {
        return importMode;
    }
    
    public void setImportMode(IMPORT_MODE importMode) {
        this.importMode = importMode;
    }
    
    public String getFileName() {
		return fileName;
	}
    
    public void setFileName(String fileName) {
		this.fileName = fileName;
	}
    
    class Action{
    	static final String IMPORT = "IMPORT";
        static final String DOWNLOAD_TEMP = "DOWNLOAD TEMPLATE";
        static final String EXPORT = "EXPORT";
        static final String DELETE_IMPORT_FILE = "EXPORT IMPORT FILE";
        static final String DELETE_IMPORT_LOG_FILE = "EXPORT IMPORT LOG FILE";
        static final String DOWNLOAD_IMPORT_FILE = "DOWNLOAD IMPORT FILE";
        static final String DOWNLOAD_IMPORT_LOG_FILE = "DOWNLOAD IMPORT LOG FILE";
        static final String DOWNLOAD_EXPORT_FILE = "DOWNLOAD EXPORT FILE";
    }
    
    class Type {
        static final String IMPORT = "import";
        static final String IMPORT_LOG = "import_log";
        static final String TEMPLATE = "template";
        static final String EXPORT = "export";
        static final String EXPORT_TEMPLATE = "export_template";
    }
}