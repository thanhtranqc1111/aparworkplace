package com.ttv.cashflow.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dto.DashboardChartOneDTO;
import com.ttv.cashflow.service.DashboardService;
import com.ttv.cashflow.util.CalendarUtil;

/**
 * @author thoai.nh
 * created date Oct 4, 2017
 */
@Controller()
@RequestMapping("/dashboard")
@PrototypeScope
public class DashboardController {
	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardController.class);
    @Autowired
    private DashboardService dashboardService;
	@RequestMapping("/")
	public String index(HttpServletRequest request, Model model) {
		return "dashboard";
	}
	
	@GetMapping(value="/getListChartOne")
	@ResponseBody
	public List<DashboardChartOneDTO> getListChartOne(@RequestParam String fromMonth, @RequestParam String toMonth)
	{
		try {
			return dashboardService.getListChartOne(CalendarUtil.getCalendar(fromMonth, "MM/yyyy"), CalendarUtil.getCalendar(toMonth, "MM/yyyy"));
		} catch (Exception e) {
			LOGGER.info("Dashboard GetListChartOne error: {}.", e);
		}
		return null;
	}
}