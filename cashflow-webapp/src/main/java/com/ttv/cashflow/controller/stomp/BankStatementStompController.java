package com.ttv.cashflow.controller.stomp;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.ttv.cashflow.util.BankAccInfo;
import com.ttv.cashflow.util.Constant;

/**
 * @author thoai.nh
 * created date Feb 9, 2018
 */
@Controller
public class BankStatementStompController {
	@Autowired
	private SimpMessagingTemplate template;
    @MessageMapping("/bankStatementStompController/edit")
    public void send(BankAccInfo bankAccInfo, SimpMessageHeaderAccessor headerAccessor) throws Exception {
    	String sessionId = headerAccessor.getSessionId();
    	String sessionIdEditng = BankAccInfo.MAP_EDITING_USER.get(bankAccInfo);
    	if(sessionIdEditng == null) {
    		if(bankAccInfo.getBankAccNo().trim().length() > 0)
    		{
    			//remove old editing account
    			for (Iterator<String> it = BankAccInfo.MAP_EDITING_USER.values().iterator(); it.hasNext();) {
    				if (sessionId.equals(it.next())) {
    					it.remove();
    					break;
    				}
    			}
    			//set user editing this bank account
    			BankAccInfo.MAP_EDITING_USER.put(bankAccInfo, sessionId + Constant.TOKEN_SEPERATOR + bankAccInfo.getUserName());
    		}
    		this.template.convertAndSend("/bankStatementStompController/messages", bankAccInfo);
    	}
    	else if(sessionId.equals(sessionIdEditng)) {
    		this.template.convertAndSend("/bankStatementStompController/messages", bankAccInfo);
    	}
    }

}