package com.ttv.cashflow.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.service.UserAccountService;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Mar 12, 2018
 */
@Controller
@RequestMapping("/publicAuthenticate")
@PrototypeScope
public class PublicAuthenticateController {
	@Autowired
	private UserAccountService userAccountService;
	
	/*
	 * before login
	 */
	@RequestMapping(value = "/preAuthenticateUserAccount", method = RequestMethod.POST)
	@ResponseBody
	public String preAuthenticateUserAccount(@RequestParam("email") final String email, @RequestParam("password") final String password) throws UnsupportedEncodingException {
		UserAccount userAccount = userAccountService.preAuthenticateUserAccount(email, password);
		if(userAccount != null)
		{
			return ResponseUtil.createAjaxSuccess(userAccount.getIsUsing2fa());
		}
		else {
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	/*
	 * show QR code image for user
	 */
	@RequestMapping(value = "/qrCode", method = RequestMethod.GET)
	public String confirmRegistration(final HttpServletRequest request, final Model model,
									 @RequestParam("token") final String token, @RequestParam("userName") final String userName) throws UnsupportedEncodingException {
		List<UserAccount> list = userAccountService.findUserAccountByEmail(userName).stream().collect(Collectors.toList());
		if(!list.isEmpty()) {
			Locale locale = request.getLocale();
			model.addAttribute("qr", generateQRUrl(list.get(0)));
			model.addAttribute("lang", locale.getLanguage());
			return "qrcode";
		}
		return "redirect:/";
	}
	
	public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";

	public String generateQRUrl(UserAccount user) {
		try {
			return QR_PREFIX + URLEncoder.encode(String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", "AP/AR Application",
					user.getEmail(), user.getSecretToken(), "AP/AR Application"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
