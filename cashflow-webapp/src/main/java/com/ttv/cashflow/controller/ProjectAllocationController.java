package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.ProjectAllocationExportComponent;
import com.ttv.cashflow.component.ProjectAllocationImportComponent;
import com.ttv.cashflow.helper.EmployeeMasterDataHelper;
import com.ttv.cashflow.service.ProjectAllocationService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Apr 27, 2018
 */
@Controller
@RequestMapping("/projectAllocation")
@PrototypeScope
public class ProjectAllocationController extends ExchangeController {
	@Autowired
	private ProjectAllocationService projectAllocationService;
	@Autowired
	private EmployeeMasterDataHelper employeeMasterDataHelper;
	@Autowired
    private ProjectAllocationExportComponent exportComponent;
	@Autowired
	private ProjectAllocationImportComponent projectAllocationImportComponent;
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectAllocationController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
       ModelAndView bankMasterView = new ModelAndView("project-allocation/list");
       Calendar fromMonth = CalendarUtil.getCalendar("01/01/" + Calendar.getInstance().get(Calendar.YEAR));
       Calendar toMonth = CalendarUtil.getCalendar("01/12/" + Calendar.getInstance().get(Calendar.YEAR));
       String employeeCode = "";
       //get from session
       if(request.getSession().getAttribute(Constant.PA_MONTH_FROM) != null) {
    	   fromMonth = (Calendar) request.getSession().getAttribute(Constant.PA_MONTH_FROM);
	   }
       if(request.getSession().getAttribute(Constant.PA_MONTH_TO) != null) {
    	   toMonth = (Calendar) request.getSession().getAttribute(Constant.PA_MONTH_TO);
	   }
       if(request.getSession().getAttribute(Constant.PA_EMPLOYEE_CODE) != null) {
    	   employeeCode = request.getSession().getAttribute(Constant.PA_EMPLOYEE_CODE).toString();
	   }
       bankMasterView.addObject("mapEmployee", employeeMasterDataHelper.toMap());
       bankMasterView.addObject("fromMonth", fromMonth);
       bankMasterView.addObject("toMonth", toMonth);
       bankMasterView.addObject("employeeCode", employeeCode);
       return bankMasterView;
    }
	
	@RequestMapping(value = "/api/findProjectAllocation", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findProjectAllocation(final HttpServletRequest request, Model model) {
		try {
			Calendar calendarMonthFrom = CalendarUtil.getCalendar(request.getParameter("fromMonth"), "MM/yyyy");
			Calendar calendarMonthTo = CalendarUtil.getCalendar(request.getParameter("toMonth"), "MM/yyyy");
			String employeeCode = request.getParameter("employeeCode");
			request.getSession().setAttribute(Constant.PA_MONTH_FROM, calendarMonthFrom);
			request.getSession().setAttribute(Constant.PA_MONTH_TO, calendarMonthTo);
			request.getSession().setAttribute(Constant.PA_EMPLOYEE_CODE, employeeCode);
			return ResponseUtil.createAjaxSuccess(projectAllocationService.findProjectAllocation(employeeCode, calendarMonthFrom, calendarMonthTo));
		} catch (Exception e) {
			LOGGER.info("findProjectAllocation error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("dataList") String dataList, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(dataList).getAsJsonArray();
			rs = projectAllocationService.saveListProjectAllocation(jsonArray);
			LOGGER.info("Save list project allocation suscess: {}", rs);
		} catch (Exception e) {
			LOGGER.info("Save list project allocation error: {}.", e);
		}
		return ResponseUtil.createAjaxSuccess(rs);
	}

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PA").getValue();
    }

    @Override
    public ImportComponent getImportComponent() {
        return projectAllocationImportComponent;
    }

    @Override
    public ExportComponent getExportComponent() {
        return exportComponent;
    }
}
