package com.ttv.cashflow.controller.stomp;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.ttv.cashflow.util.BankAccInfo;
import com.ttv.cashflow.util.Constant;

/**
 * @author thoai.nh 
 * created date Feb 9, 2018
 */
@Component
public class WebSocketEventListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);
	@EventListener
	public void handleWebSocketConnectListener(SessionConnectedEvent event) {
		LOGGER.info("Received a new web socket connection");
	}

	@EventListener
	public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
		//remove user for map editing bank account
		String sessionId = event.getSessionId();
		for (Iterator<String> it = BankAccInfo.MAP_EDITING_USER.values().iterator(); it.hasNext();) {
			String valueInfo = it.next();
			String tokens[] = valueInfo.split(Constant.TOKEN_SEPERATOR);
			if (tokens.length == 2 && sessionId.equals(tokens[0])) {
				it.remove();
				break;
			}
		}
		LOGGER.info("Handle WebSocket Disconnect Listener:" + sessionId);
	}
}
