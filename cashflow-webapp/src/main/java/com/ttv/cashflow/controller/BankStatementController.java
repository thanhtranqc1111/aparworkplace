package com.ttv.cashflow.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.BankStatementExportComponent;
import com.ttv.cashflow.component.BankStatementImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.service.BankAccountMasterService;
import com.ttv.cashflow.service.BankMasterService;
import com.ttv.cashflow.service.BankStatementService;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.service.ReceiptOrderService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh 
 * created date Oct 13, 2017
 */
@Controller
@RequestMapping("/bankStatement")
@PrototypeScope
public class BankStatementController extends ExchangeController{
	@Autowired
	private BankMasterService bankMasterService;
	@Autowired
	private BankAccountMasterService bankAccountMasterService;
	@Autowired
	private PaymentOrderService paymentOrderService;
	@Autowired
	private BankStatementService bankStatementService;
	@Autowired
	private BankStatementImportComponent bankStatementImportComponent;
	@Autowired
	private BankStatementExportComponent bankStatementExportComponent;
	@Autowired
	private ReceiptOrderService receiptOrderService;
	private static final Logger LOGGER = LoggerFactory.getLogger(BankStatementController.class);
	
	@RequestMapping(value = {"/list","/list/{id}"}, method = RequestMethod.GET)
	public ModelAndView mapList(@PathVariable Optional<BigInteger> id, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("bank-statement/list");
		//load bank master data
		List<BankMaster> listBankMaster = bankMasterService.findAllBankMasters(-1, -1);
		String defaultBankCode = null;//use to load bank account list
		//when load only one transaction to view
		if (id.isPresent()) {
			model.addObject("transactionNumber", id.get());
			BankStatement bankStatement = bankStatementService.findBankStatementByPrimaryKey(id.get());
			if(bankStatement != null) {
				model.addObject("bankAccNo", bankStatement.getBankAccountNo());
				model.addObject("bankName", bankStatement.getBankName());
				model.addObject("transactionFrom", null);
				model.addObject("transactionTo", null);
				model.addObject("transDateFilter", 0);
				List<BankMaster> listBankMasterTemp = listBankMaster.stream().filter(item -> item.getName().equals(bankStatement.getBankName())).collect(Collectors.toList());
				if(!listBankMasterTemp.isEmpty()) {
					defaultBankCode = listBankMasterTemp.get(0).getCode();
				}
			}
			else 
			{
				return new ModelAndView("redirect:/404");
			}
		}
		else {
			//get session to load data 
			if(request.getSession().getAttribute(Constant.BSM_BANK_ACC_CODE) != null) {
				model.addObject("bankAccNo", request.getSession().getAttribute(Constant.BSM_BANK_ACC_CODE));
			}
			if(request.getSession().getAttribute(Constant.BSM_BANK_NAME) != null) {
				model.addObject("bankName", request.getSession().getAttribute(Constant.BSM_BANK_NAME));
			}
			if(request.getSession().getAttribute(Constant.BSM_BANK_CODE) != null) {
				defaultBankCode = request.getSession().getAttribute(Constant.BSM_BANK_CODE).toString();
			}
			if(request.getSession().getAttribute(Constant.BSM_TRANSACTION_FROM) != null) {
				model.addObject("transactionFrom", request.getSession().getAttribute(Constant.BSM_TRANSACTION_FROM));
			}
			if(request.getSession().getAttribute(Constant.BSM_TRANSACTION_TO) != null) {
				model.addObject("transactionTo", request.getSession().getAttribute(Constant.BSM_TRANSACTION_TO));
			}
			if(request.getSession().getAttribute(Constant.BSM_TRANSACTION_DATE_FILTER) != null) {
				model.addObject("transDateFilter", request.getSession().getAttribute(Constant.BSM_TRANSACTION_DATE_FILTER));
			}
			else {
				model.addObject("transDateFilter", 3);
			}
		}
		//load bank account master data
		if (defaultBankCode != null)
		{
			Set<BankAccountMaster> setbankAccountMaster = bankAccountMasterService.findBankAccountMasterByBankCode(defaultBankCode);
			model.addObject("bankAccountMasterFilter", setbankAccountMaster);
		}
		model.addObject("bankMaster", listBankMaster);
		return model;
	}
	
	@Deprecated
	@GetMapping(value = {"/redirectToPaymentOrderDetail/{paymentNo}"})
    public String redirectPaymentOrder(@PathVariable String paymentNo) {
		PaymentOrder paymentOrder = paymentOrderService.findPaymentOrderByPaymentOrderNoOnly(paymentNo);
		if(paymentOrder == null) {
			return "redirect:/";
		}
		else {
			return (paymentOrder.getPaymentType() == Constant.PAYMENT_ORDER_PAYROLL ? "redirect:/paymentPayroll/detail/" : "redirect:/payment/detail/") + paymentNo;
		}
	}
	
	@RequestMapping(value = "/api/getBankAccountMasterByBankCode", method = { RequestMethod.GET })
	@ResponseBody
	public String getBankAccountMasterByBankCode(@ModelAttribute("bankCode") String bankCode) {
		try {
			Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterService.findBankAccountMasterByBankCode(bankCode);
			return ResponseUtil.createAjaxSuccess(setBankAccountMaster);
		} catch (Exception e) {
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}

	@RequestMapping(value = "/api/getAllPagingBankStatement", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingBankStatementType(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String bankName = request.getParameter("bankName");
			String bankCode = request.getParameter("bankCode");
			String bankAccNo = request.getParameter("bankAccNo");
			String transactionFrom = request.getParameter("transactionFrom");
			String transactionTo = request.getParameter("transactionTo");
			Integer subsidiaryId = NumberUtils.toInt(request.getParameter("subsidiaryId"));
			Integer transDateFilter = NumberUtils.toInt(request.getParameter("transDateFilter"));
			String transactionNumberString = request.getParameter("transactionNumber");
			BigInteger transactionNumber = null;
			if(!StringUtils.isEmpty(transactionNumberString)) {
				transactionNumber = new BigInteger(request.getParameter("transactionNumber"));
			}
			Calendar calendarTransactionFrom = CalendarUtil.getCalendar(transactionFrom, "dd/MM/yyyy");
			Calendar calendarTransactionTo = CalendarUtil.getCalendar(transactionTo, "dd/MM/yyyy");
			if(transactionNumber == null || transactionNumber.compareTo(BigInteger.ZERO) == 0) {
				request.getSession().setAttribute(Constant.BSM_BANK_NAME, bankName);
				request.getSession().setAttribute(Constant.BSM_BANK_CODE, bankCode);
				request.getSession().setAttribute(Constant.BSM_BANK_ACC_CODE, bankAccNo);
				request.getSession().setAttribute(Constant.BSM_TRANSACTION_FROM, transactionFrom);
				request.getSession().setAttribute(Constant.BSM_TRANSACTION_TO, transactionTo);
				request.getSession().setAttribute(Constant.BSM_TRANSACTION_DATE_FILTER, transDateFilter);
			}
			jsonData = bankStatementService.findBankStatementJson(bankName, bankAccNo, calendarTransactionFrom, calendarTransactionTo, transactionNumber, transDateFilter, subsidiaryId);
		} catch (Exception e) {
			LOGGER.info("getAllPagingBankStatementType error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("dataList") String dataList,  @ModelAttribute("endingBalance") String endingBalance, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(dataList).getAsJsonArray();
			List<BankStatement> list = bankStatementService.parseBankStatementJSON(jsonArray);
			rs = bankStatementService.saveListBankStatement(list);
			LOGGER.info("Save list bank statement suscess: {}", rs);
			//update ending balance in bank account master data where insert new data
			if(jsonArray.size() > 0) {
				JsonObject bankStatementObject = jsonArray.get(0).getAsJsonObject();
				if(bankStatementObject.has("bankAccountNo"))
				{
					bankStatementService.updateEndingBalance(bankStatementObject.get("bankAccountNo").getAsString(), NumberUtil.getBigDecimalScaleDown(2, endingBalance));
				}
			}
		} catch (Exception e) {
			LOGGER.info("Save list bank statement error: {}.", e);
		}
		return ResponseUtil.createAjaxSuccess(rs);
	}

	@RequestMapping(value = "/api/findPaymentOrderByPaymentOrderNoContaining", method = {RequestMethod.GET }, produces = { "application/json; charset=UTF-8" })
	@ResponseBody
	public String findPaymentOrderByPaymentOrderNoContaining(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String paymentOrderNo = request.getParameter("paymentOrderNo");
			jsonData = paymentOrderService.findPaymentOrderByPaymentOrderNoContaining(paymentOrderNo, systemValueHelper.get("PAY", "002").getCode());
		} catch (Exception e) {
			LOGGER.info("findPaymentOrderByPaymentOrderNoContaining error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/selectImportTemplate", method = RequestMethod.POST)
	@ResponseBody
	public String selectImportTemplate(HttpServletRequest request,  @ModelAttribute("bankName") String bankName, 
									   @ModelAttribute("bankAccCode") String bankAccCode,  @ModelAttribute("currencyCode") String currencyCode) {
		try {
			request.getSession().setAttribute(Constant.BSM_BANK_NAME, bankName);
			request.getSession().setAttribute(Constant.BSM_BANK_ACC_CODE, bankAccCode);
			request.getSession().setAttribute(Constant.BSM_CURRENCY_CODE, currencyCode);
			return ResponseUtil.createAjaxSuccess(null);
		} catch (Exception e) {
			LOGGER.info("selectImportTemplate error: {}.", e);
			return ResponseUtil.createAjaxError(null);
		}
	}
	
	@RequestMapping(value = "/api/getMaxTransactionDate", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getMaxTransactionDate(@ModelAttribute("bankCode") String bankCode, @ModelAttribute("bankAccNo") String bankAccNo) {
		try {
			Calendar maxTransactionDate = bankStatementService.getMaxTransactionDate(bankCode, bankAccNo);
			return ResponseUtil.createAjaxSuccess(CalendarUtil.toString(maxTransactionDate));
		} catch (Exception e) {
			LOGGER.info("getMaxTransactionDate error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@RequestMapping(value = "/api/resetPaymentOrderNo", method = { RequestMethod.POST })
	@ResponseBody
	public String resetPaymentOrderNo(@ModelAttribute("id") BigInteger id) {
		try {
			return ResponseUtil.createAjaxSuccess(bankStatementService.resetPaymentOrderStatus(id));
		} catch (Exception e) {
			LOGGER.info("resetPaymentOrderNo error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@RequestMapping(value = "/api/resetReceiptOrderNo", method = { RequestMethod.POST })
	@ResponseBody
	public String resetReceiptOrderNo(@ModelAttribute("id") BigInteger id) {
		try {
			return ResponseUtil.createAjaxSuccess(receiptOrderService.resetRecepitOrder(id));
		} catch (Exception e) {
			LOGGER.info("resetReceiptOrderNo error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@GetMapping(value = "/import")
	public ModelAndView importIndex(ModelMap model, HttpServletRequest request) {
		String bankName = null, bankAccCode = null;
		try {
			bankName = request.getSession().getAttribute(Constant.BSM_BANK_NAME).toString();
			bankAccCode = request.getSession().getAttribute(Constant.BSM_BANK_ACC_CODE).toString();
		} catch (Exception e) {
			LOGGER.info("importIndex error: {}.", e);
		}
		if(bankName != null && bankAccCode != null) {
			return super.importIndex(model, request);
		}
		else 
		{
			return new ModelAndView("redirect:/bankStatement/list");
		}
	}
	
	@RequestMapping(value = "/api/validateTransactionDateBeforeEdit", method = { RequestMethod.POST })
	@ResponseBody
	public String validateTransactionDateBeforeEdit(@ModelAttribute("id") BigInteger id) {
		try {
			boolean rs = bankStatementService.validateTransactionDateBeforeEdit(id);
			if(rs)
			{
				return ResponseUtil.createAjaxSuccess(rs);
			}
			else {
				return ResponseUtil.createAjaxSuccess(rs, systemValueHelper.get("BSM_DAYS", "BSM_DAYS").getValue());
			}
		} catch (Exception e) {
			LOGGER.info("validateTransactionDateBeforeEdit error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@RequestMapping(value = "/api/getPaymentOrderNoBasedOnBankRef", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getPaymentOrderNoBasedOnBankRef(@ModelAttribute("bankName") String bankName, @ModelAttribute("bankCode") String bankCode, @ModelAttribute("bankAccNo") String bankAccNo,
			@ModelAttribute("transactionFrom") String transactionFrom, @ModelAttribute("transactionTo") String transactionTo,  @ModelAttribute("transDateFilter") Integer transDateFilter) {
		try {
			Calendar calendarTransactionFrom = CalendarUtil.getCalendar(transactionFrom, "dd/MM/yyyy");
			Calendar calendarTransactionTo = CalendarUtil.getCalendar(transactionTo, "dd/MM/yyyy");
			return ResponseUtil.createAjaxSuccess(paymentOrderService.getPaymentOrderNoBasedOnBankRef(bankName, bankAccNo, calendarTransactionFrom, calendarTransactionTo, transDateFilter));
		} catch (Exception e) {
			LOGGER.info("getPaymentOrderNoBasedOnBankRef error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
	}
	
	@Override
	public ImportComponent getImportComponent() {
		return bankStatementImportComponent;
	}

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "BSM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return bankStatementExportComponent;
    }
}
