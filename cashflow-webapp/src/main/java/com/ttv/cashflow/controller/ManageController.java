package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.dto.EmployeeMasterDTO;
import com.ttv.cashflow.service.AccountMasterService;
import com.ttv.cashflow.service.PayeePayerMasterService;
import com.ttv.cashflow.service.ProjectMasterService;
import com.ttv.cashflow.util.CalendarUtil;

@Controller
@RequestMapping("/manage")
@PrototypeScope
public class ManageController {
	
	@Autowired
	private PayeePayerMasterService payeeMasterService;
	
	@Autowired
	private ProjectMasterService projectMasterService;
	
	@Autowired
	private AccountMasterService accountMasterService;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageController.class);
	/**
	 * Gets the payee.
	 *
	 * @param keyword the keyword
	 * @return the payee
	 */
	@GetMapping(value="/payeeMasters")
	@ResponseBody
	public List<Map<String,String>> getPayee(@RequestParam String keyword){
		
		// Create a list of payee for autocomplete box
		List<Map<String,String>> payees = new ArrayList<Map<String,String>>();
		Map<String,String> payee;
		Set<PayeePayerMaster> payerMasters = payeeMasterService.findPayeePayerMasterByAccountAndName(keyword);
		
		// Build data for autocomplete
		for (PayeePayerMaster payeePayerMaster : payerMasters) {
			payee = new LinkedHashMap<String, String>();
			payee.put("code", payeePayerMaster.getCode());
			payee.put("payeeAccount", payeePayerMaster.getCode());
			payee.put("payeeName", payeePayerMaster.getName());
			payee.put("address", payeePayerMaster.getAddress());
			payee.put("phone", payeePayerMaster.getPhone());
			payee.put("typeCode", payeePayerMaster.getPayeePayerTypeMaster().getCode());
			payee.put("typeName", payeePayerMaster.getPayeePayerTypeMaster().getName());
			payee.put("paymentTerm", payeePayerMaster.getPaymentTerm());
			payee.put("accountCode", payeePayerMaster.getAccountCode());
			payee.put("accountName", accountMasterService.findAccountNameByCode(payeePayerMaster.getAccountCode()));
			payee.put("defaultCurrency", payeePayerMaster.getDefaultCurrency());
			payees.add(payee);
		}
		return payees;
	}
	
	@GetMapping(value="/projectMasters")
	@ResponseBody
	public Set<ProjectMaster> getProjects(@RequestParam String keyword)
	{
		Set<ProjectMaster> set = projectMasterService.findProjectMasterByCodeNameContaining(keyword);
		for(ProjectMaster p: set) {
			p.setProjectAllocations(null);
			p.setProjectPayrollDetailses(null);
		}
		return set;
	}
	
	@RequestMapping(value="/accountMasters", method = RequestMethod.GET)
	@ResponseBody
	public Set<AccountMaster> getAccounts(@RequestParam String keyword)
	{
		return accountMasterService.findAccountMasterByNameCodeContaining(keyword);
	}
	
	@RequestMapping(value="/getEmployeeMaster", method = RequestMethod.GET)
	@ResponseBody
	public List<EmployeeMasterDTO> getEmployeeMaster(@RequestParam String keyword)
	{
		return employeeMasterDAO.getEmployeeMaster(-1, -1, 1, "desc", keyword);
	}
	
	@RequestMapping(value="/getEmployeeMasterForPayroll", method = RequestMethod.GET)
	@ResponseBody
	public List<EmployeeMasterDTO> getEmployeeMasterForPayroll(@RequestParam String keyword, @RequestParam String paymentScheduleDate)
	{
		try {
			Calendar calendarPaymentScheduleDate = CalendarUtil.getCalendar(paymentScheduleDate, "dd/MM/yyyy");
			return employeeMasterDAO.getEmployeeMasterForPayroll(-1, -1, 1, "desc", keyword, calendarPaymentScheduleDate);
		} catch (Exception e) {
			LOGGER.info("getEmployeeMasterForPayroll error: {}.", e);
			return null;
		}
	}
}