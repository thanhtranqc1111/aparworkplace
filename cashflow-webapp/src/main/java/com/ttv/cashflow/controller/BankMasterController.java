package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.BankImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.service.BankMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 3, 2017
 */
@Controller()
@RequestMapping("/admin/bankMaster")
@PrototypeScope
public class BankMasterController extends ExchangeController {
    
    @Autowired
    BankMasterService bankMasterService;
    
    @Autowired
    BankImportComponent bankImportComponent;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView bankMasterView = new ModelAndView("master/bank");
       bankMasterView.addObject("bankMasterForm", new BankMaster());
       bankMasterView.addObject("adminPage", "bankMaster");
       return bankMasterView;
    }
    
    @RequestMapping(value = "/api/getAllPagingBank", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingBank(final HttpServletRequest request, Model model) {
        String jbankMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jbankMaster = bankMasterService.findBankMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
        	return ResponseUtil.createAjaxError(null);
        }
        return jbankMaster;
    }
   
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBankMaster(HttpServletRequest request, @ModelAttribute("bankMasterForm") @Valid BankMaster bank,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 BankMaster existBank  = bankMasterService.findBankMasterByPrimaryKey(bank.getCode());
                     if (existBank != null)
                         return "redirect:/admin/bankMaster/list"; 
                 } 
                bankMasterService.saveBankMaster(bank);
                return "redirect:/admin/bankMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteBankMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                bankMasterService.deleteBankMasterByCodes(codes);
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/bankMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return bankImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "BM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }

}