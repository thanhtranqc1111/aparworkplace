package com.ttv.cashflow.controller;

import com.ttv.cashflow.annotation.PrototypeScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by truong.hv on 04/06/2018.
 */
@Controller
@RequestMapping("/approval")
@PrototypeScope
public class ApprovalProcessController {
    @RequestMapping(value = "/process", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("approval/process");
        return model;
    }
}
