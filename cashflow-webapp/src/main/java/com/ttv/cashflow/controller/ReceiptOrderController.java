package com.ttv.cashflow.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;
import com.ttv.cashflow.service.BankAccountMasterService;
import com.ttv.cashflow.service.BankMasterService;
import com.ttv.cashflow.service.ReceiptOrderService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.util.ResponseUtil;


/**
 * @author thoai.nh
 * created date Dec 5, 2017
 */
@Controller
@RequestMapping("/receiptOrder")
@PrototypeScope
public class ReceiptOrderController{
	@Autowired
	private BankMasterService bankMasterService;
	@Autowired
	private BankAccountMasterService bankAccountMasterService;
	@Autowired
	private ReceiptOrderService receiptOrderService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiptOrderController.class);
	
	@GetMapping(value = {"/list","/list/{receiptOrderNo}"})
	public ModelAndView list(@PathVariable Optional<String> receiptOrderNo, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("receipt-order/list");
		List<BankMaster> listBankMaster = bankMasterService.findAllBankMasters(-1, -1);
		model.addObject("bankMaster", listBankMaster);
		model.addObject("bankAccountMasterAll", bankAccountMasterService.findBankAccountMasterByBankCode(null));
		if (receiptOrderNo.isPresent()) {
			model.addObject("receiptOrderNo", receiptOrderNo.get());
		}
		else
		{
			model.addObject("receiptOrderNo", "");
		}
		return model;
	}
	
	@RequestMapping(value = "/api/getAllPagingReceiptOrder", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingReceiptOrder(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			String bankName = request.getParameter("bankName");
			String bankAccNo = request.getParameter("bankAccNo");
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            Integer option = NumberUtils.toInt(request.getParameter("creditOption"));
            Integer status = NumberUtils.toInt(request.getParameter("status"));
            BigDecimal credit = null;
            String receiptOrderNo = request.getParameter("receiptOrderNo");
            String creditString = request.getParameter("credit");
			if(!org.springframework.util.StringUtils.isEmpty(creditString)) {
				credit = NumberUtil.getBigDecimalScaleDown(2, creditString.replaceAll(",", ""));
			}
			Calendar calendarTransactionFrom = CalendarUtil.getCalendar(request.getParameter("transactionFrom"), "dd/MM/yyyy");
			Calendar calendarTransactionTo = CalendarUtil.getCalendar(request.getParameter("transactionTo"), "dd/MM/yyyy");
			jsonData = receiptOrderService.findReceiptOrderJSON(startpage, lengthOfpage, bankName, bankAccNo, calendarTransactionFrom, calendarTransactionTo, receiptOrderNo, credit, option, orderColumn, orderBy, status);
		} catch (Exception e) {
			LOGGER.info("getAllPagingReceiptOrder error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/api/getArInvoice", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getArInvoice(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			String fromApNo = StringUtils.stripToEmpty(request.getParameter("fromApNo"));
			String toApNo = StringUtils.stripToEmpty(request.getParameter("toApNo"));
			String fromMonth = StringUtils.stripToEmpty(request.getParameter("fromMonth"));
			String toMonth = StringUtils.stripToEmpty(request.getParameter("toMonth"));
			String payerName = StringUtils.stripToEmpty(request.getParameter("payerName"));
			boolean isApproved = Boolean.parseBoolean(request.getParameter("isApproved"));
			boolean isNotReceived = Boolean.parseBoolean(request.getParameter("isNotReceived"));
            Integer option = NumberUtils.toInt(request.getParameter("creditOption"));
            String creditString = request.getParameter("credit");
            BigDecimal credit = null;
            if(!org.springframework.util.StringUtils.isEmpty(creditString)) {
            	credit = NumberUtil.getBigDecimalScaleDown(2, request.getParameter("credit").replaceAll(",", ""));
            }
			jsonData = receiptOrderService.findArInvoiceToMatchRecepitOrderJSON(startpage, lengthOfpage, fromApNo, toApNo, fromMonth, toMonth, payerName, isApproved, credit, option, isNotReceived);
		} catch (Exception e) {
			LOGGER.info("getArInvoice error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/applyMatching", method = RequestMethod.POST)
	@ResponseBody
	public String applyMatching(HttpServletRequest request, @ModelAttribute("bankStatementId") BigInteger bankStatementId, @ModelAttribute("data") String data, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(data).getAsJsonArray();
			List<ArInvoiceReceiptOrderDTO> listArInvoiceReDTO = receiptOrderService.parseArInvoiceJSON(jsonArray);
			rs = receiptOrderService.appyMatch(bankStatementId, listArInvoiceReDTO);
		} catch (Exception e) {
			LOGGER.error("ApplyMatching failure. {}", e);
		}
		
		LOGGER.info("ApplyMatching suscess. {}", data);
		return ResponseUtil.createAjaxSuccess(rs);
	}
}
