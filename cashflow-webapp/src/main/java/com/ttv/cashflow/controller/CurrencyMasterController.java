package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.CurrencyImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 6, 2017
 */
@Controller()
@RequestMapping("/admin/currencyMaster")
@PrototypeScope
public class CurrencyMasterController extends ExchangeController {
    
	@Autowired
	private CurrencyMasterService currencyMasterService;
	
	@Autowired
    private CurrencyImportComponent currencyImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(CurrencyMasterController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/currency");
       payeeMasterView.addObject("currencyMasterForm", new CurrencyMaster());
       payeeMasterView.addObject("adminPage", "currencyMaster");
       return payeeMasterView;
    }
	
	@RequestMapping(value = "/api/getAllPagingCurrency", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingCurrency(final HttpServletRequest request, Model model) {
    	String jCurrencyMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jCurrencyMaster = currencyMasterService.findCurrencyMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jCurrencyMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveCurrencyMaster(HttpServletRequest request, @ModelAttribute("CurrencyMasterForm") @Valid CurrencyMaster currencyMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 CurrencyMaster existCurrency  = currencyMasterService.findCurrencyMasterByPrimaryKey(currencyMaster.getCode());
                     if (existCurrency != null)
                         return "redirect:/admin/currencyMaster/list"; 
                 } 
                 currencyMasterService.saveCurrencyMaster(currencyMaster);
                 logger.info("Save suscess, Currency Master: {}.", currencyMaster.toString());
                 
                return "redirect:/admin/currencyMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteCurrencyMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                currencyMasterService.deleteCurrencyMasterByCodes(codes);
                logger.info("Delete suscess, Currency Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/currencyMaster/list"; 
    }

    @Override
    public ImportComponent getImportComponent() {
        return currencyImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "CRM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
