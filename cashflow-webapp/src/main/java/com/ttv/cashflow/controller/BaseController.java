package com.ttv.cashflow.controller;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.ttv.cashflow.helper.SystemValueHelper;
import com.ttv.cashflow.session.LoginVisited;


public class BaseController {
    
    @Autowired
    protected LoginVisited visit;
    
    @Autowired
    protected SystemValueHelper systemValueHelper;
    
    @Autowired 
    protected ServletContext servletContext;
    
    @Autowired
    protected MessageSource messageSource;
    
    
}