package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.EmployeeImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.service.DivisionMasterService;
import com.ttv.cashflow.service.EmployeeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Controller()
@RequestMapping("/admin/employeeMaster")
@PrototypeScope
public class EmployeeMasterController extends ExchangeController {

    @Autowired
    private EmployeeMasterService employeeMasterService;

    @Autowired
    private DivisionMasterService divisionMasterService;

    @Autowired
    private EmployeeImportComponent employeeImportComponent;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeMasterController.class);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView employeeMasterView = new ModelAndView("master/employee");
        employeeMasterView.addObject("employeeMasterForm", new EmployeeMaster());
        employeeMasterView.addObject("divisionMasterList", divisionMasterService.findAllDivisionMasters(-1, -1));
        return employeeMasterView;
    }

    @RequestMapping(value = "/api/getAllPagingEmployee", method = { RequestMethod.GET }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingEmployee(final HttpServletRequest request, Model model) {
        String jEmployeeMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jEmployeeMaster = employeeMasterService.findEmployeeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
            LOGGER.info("getAllPagingEmployee failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jEmployeeMaster;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveEmployeeMaster(HttpServletRequest request, @ModelAttribute("employeeMasterForm") @Valid EmployeeMaster employeeMaster) {
        try {
            String mode = request.getParameter("mode");
            DivisionMaster divisionMaster = divisionMasterService.findDivisionMasterByCode(employeeMaster.getDivisionMaster().getCode());
            employeeMaster.setDivisionMaster(divisionMaster);

            if (Constant.MODE.NEW.getValue().equals(mode)) {
                EmployeeMaster existingEmployee = employeeMasterService.findEmployeeMasterByCode(employeeMaster.getCode());

                if (existingEmployee != null) {
                    LOGGER.info("The code {} existed in db.", employeeMaster.getCode());
                    return "redirect:/admin/employeeMaster/list";
                } else {
                    employeeMasterService.createEmployeeMaster(employeeMaster);
                }
            } else if (Constant.MODE.EDIT.getValue().equals(mode)) {
                employeeMasterService.updateEmployeeMaster(employeeMaster);
            }

            return "redirect:/admin/employeeMaster/list";
        } catch (Exception e) {
            LOGGER.info("saveEmployeeMaster failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
    }

    @RequestMapping(value = "/delete")
    public String deleteEmployeeMaster(final HttpServletRequest request, Model model) {
        try {
            String codes = request.getParameter("id");
            employeeMasterService.deleteEmployeeMasterByCode(codes);
        } catch (Exception e) {
            LOGGER.info("deleteEmployeeMaster failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return "redirect:/admin/employeeMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return employeeImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "EMP").getValue();
    }

    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
