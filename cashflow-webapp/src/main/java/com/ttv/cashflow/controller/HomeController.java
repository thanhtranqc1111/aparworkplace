package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.helper.MapDataSourceLookupHelper;
import com.ttv.cashflow.service.UserAccountService;
import com.ttv.cashflow.session.LoginVisited;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResourceBundleFileNames;
import com.ttv.cashflow.util.ResponseUtil;
import com.ttv.cashflow.config.security.CustomAuthenticationFailureHandler;
/**
 * @author thoai.nh created date Oct 3, 2017
 */
@Controller
@PrototypeScope
public class HomeController {
	@Autowired
	private SubsidiaryDAO subsidiaryDAO;

	@Autowired
	public UserAccountService accountService;

	@Autowired
	public LoginVisited loginInfo;

	@Autowired
	private MapDataSourceLookupHelper mapDataSourceHelper;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showLogin(HttpServletRequest request, Model model) {
		return "redirect:dashboard/";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request, ModelAndView model) {
		if (loginInfo != null && loginInfo.getUserName() != null) {
			model.setViewName("redirect:/");
			return model;
		} else {
			//get error message from CustomAuthenticationFailureHandler, other case error code is a parameter receive from PublicAuthenticateController
			if("true".equals(error) && request.getSession().getAttribute(Constant.SESSSION_LOGIN_ERROR_CODE) != null) {
				error = request.getSession().getAttribute(Constant.SESSSION_LOGIN_ERROR_CODE).toString();
				request.getSession().setAttribute(Constant.SESSSION_LOGIN_ERROR_CODE, null);
			}
			
			if(error != null) {
				if (error.equals(CustomAuthenticationFailureHandler.ERROR_CODE_INVALID_USERNAME_CREDENTIALS)) {
					model.addObject("error", "cashflow.login.errorMessage1");
				}
				else if (error.equals(CustomAuthenticationFailureHandler.ERROR_CODE_INVALID_VERFICATION_CODE)) {
					model.addObject("error", "cashflow.login.errorMessage2");
				}
				else {
					model.addObject("error", "cashflow.login.errorMessage");
				}
			}
			// case logout
			else if (logout != null) {
				model.addObject("msg", "cashflow.login.logoutMessage");
			}
			model.setViewName("index");

			return model;
		}
	}

	@RequestMapping(value = "/changeSubsidiary", method = RequestMethod.POST)
	@ResponseBody
	public String changeSubsidiary(@ModelAttribute("id") Integer id, Model model, HttpServletRequest request) {
		Subsidiary subsidiary = subsidiaryDAO.findSubsidiaryById(id);
		if (subsidiary != null) {
			HttpSession session = request.getSession();
			UserAccount authUser = (UserAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			//Store user info to session.
			loginInfo.setUserName(authUser.getEmail());
            loginInfo.setFirstName(authUser.getFirstName());
            loginInfo.setLastName(authUser.getLastName());
            loginInfo.setEmail(authUser.getEmail());
            loginInfo.setUserAccountId(authUser.getId());
			loginInfo.setSubsidiaryName(subsidiary.getName());
			loginInfo.setSubsidiaryId(subsidiary.getId());
			session.setAttribute(Constant.SESSSION_NAME_CURRENT_SUBSIDIARY, subsidiary);

			// add datasource belong user login.
			mapDataSourceHelper.addDataSourceIfNotExist(subsidiary.getCode());

			// set tenant code to session for decide get right datasource.
			session.setAttribute(Constant.SESSSION_NAME_TENANT_ID, subsidiary.getCode());

			return ResponseUtil.createAjaxSuccess(null);

		} else {
			return ResponseUtil.createAjaxError(null);
		}
	}

	@GetMapping(value = "/i18n.js")
	public ModelAndView getAllResourceBundel(@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		if (page.contains("detail/")) {
			page = page.substring(0, page.indexOf("detail/") + 6);
		}
		// Retrieve the locale of the User
		Locale locale = RequestContextUtils.getLocale(request);
		// Use the path to your bundle
		List<String> bundleNames = new ArrayList<String>();
		bundleNames.add(ResourceBundleFileNames.COMMON.toString());
		Set<String> keySet = new HashSet<String>();
		switch (page) {
		case "/dashboard/":
			bundleNames.add(ResourceBundleFileNames.DASHBOARD.toString());
			break;
		case "/apinvoice/list":
			bundleNames.add(ResourceBundleFileNames.AP_INVOICE_LIST.toString());
			break;
		case "/apinvoice/detail":
			bundleNames.add(ResourceBundleFileNames.AP_INVOICE_DETAIL.toString());
			break;
		case "/payment/list":
			bundleNames.add(ResourceBundleFileNames.PAYMENT_LIST.toString());
			break;
		case "/payment/detail":
			bundleNames.add(ResourceBundleFileNames.PAYMENT_DETAIL.toString());
			break;
		case "/arinvoice/list":
			bundleNames.add(ResourceBundleFileNames.AR_INVOICE_LIST.toString());
			break;
		case "/arinvoice/detail":
			bundleNames.add(ResourceBundleFileNames.AR_INVOICE_DETAIL.toString());
			break;
		case "/receiptOrder/list":
			bundleNames.add(ResourceBundleFileNames.RECEIPT_ORDER.toString());
			break;
		case "/bankStatement/list":
			bundleNames.add(ResourceBundleFileNames.BANK_STATEMENT.toString());
			break;
		case "/accountMaster/list":
			bundleNames.add(ResourceBundleFileNames.ACCOUNT_MASTER.toString());
			break;
		case "/admin/bankMaster/list":
			bundleNames.add(ResourceBundleFileNames.BANK_MASTER.toString());
			break;
		case "/admin/bankAccountMaster/list":
			bundleNames.add(ResourceBundleFileNames.BANK_ACCOUNT_MASTER.toString());
			break;
		case "/admin/businessTypeMaster/list":
			bundleNames.add(ResourceBundleFileNames.BUSINESS_TYPE_MASTER.toString());
			break;
		case "/admin/currencyMaster/list":
			bundleNames.add(ResourceBundleFileNames.CURRENCY_MASTER.toString());
			break;
		case "/admin/claimTypeMaster/list":
			bundleNames.add(ResourceBundleFileNames.CLAIM_TYPE_MASTER.toString());
			break;
		case "/admin/gstMaster/list":
			bundleNames.add(ResourceBundleFileNames.GST_MASTER.toString());
			break;
		case "/admin/payeeTypeMaster/list":
			bundleNames.add(ResourceBundleFileNames.PAYEE_TYPE_MASTER.toString());
			break;
		case "/admin/payeeMaster/list":
			bundleNames.add(ResourceBundleFileNames.PAYEE_MASTER.toString());
			break;
		case "/admin/projectMaster/list":
            bundleNames.add(ResourceBundleFileNames.PROJECT_MASTER.toString());
            break;
        case "/admin/divisionMaster/list":
            bundleNames.add(ResourceBundleFileNames.DIVISION_MASTER.toString());
            break;
        case "/admin/employeenMaster/list":
            bundleNames.add(ResourceBundleFileNames.EMPLOYEE_MASTER.toString());
			break;
		case "/userAccount/list":
			bundleNames.add(ResourceBundleFileNames.USER_ACCOUNT.toString());
			break;
		case "/role/list":
			bundleNames.add(ResourceBundleFileNames.ROLE.toString());
			break;
		case "/userAccount/detail/":
			bundleNames.add(ResourceBundleFileNames.USER_ACCOUNT_DETAIL.toString());
			break;
		case "/permission/list":
			bundleNames.add(ResourceBundleFileNames.PERMISSION.toString());
			break;
		case "/grantPermission/list":
			bundleNames.add(ResourceBundleFileNames.ROLE_PERMISSION.toString());
			break;
		case "/subsidiary/list":
			bundleNames.add(ResourceBundleFileNames.SUBSIDIARY.toString());
			break;
		case "/subsidiary/grantSub":
			bundleNames.add(ResourceBundleFileNames.GRANT_SUBSIDIARY.toString());
			break;
		case "/subsidiary/detail":
			bundleNames.add(ResourceBundleFileNames.SUBSIDIARY_DETAIL.toString());
			break;
		case "/apinvoice/import":
			bundleNames.add(ResourceBundleFileNames.IMPORT.toString());
			break;
		case "/apinvoice/export":
			bundleNames.add(ResourceBundleFileNames.EXPORT.toString());
			break;
		case "/approvalPurchaseRequest/list":
			bundleNames.add(ResourceBundleFileNames.APPROVAL_PURCHASE_REQUEST.toString());
			break;
		case "/approvalSalesOrderRequest/list":
			bundleNames.add(ResourceBundleFileNames.APPROVAL_SALES_ORDER_REQUEST.toString());
			break;
		case "/admin/approvalTypePurchaseMaster/list":
			bundleNames.add(ResourceBundleFileNames.APPROVAL_TYPE_PURCHASE_MASTER.toString());
			break;
		case "/admin/approvalTypeSalesMaster/list":
			bundleNames.add(ResourceBundleFileNames.APPROVAL_TYPE_SALES_MASTER.toString());
			break;
		case "/paymentPayroll/list":
	        bundleNames.add(ResourceBundleFileNames.PAYMENT_PAYROLL_LIST.toString());
			break;
        case "/paymentPayroll/detail":
	        bundleNames.add(ResourceBundleFileNames.PAYMENT_PAYROLL_DETAIL.toString());
			break;
        case "/payroll/list":
            bundleNames.add(ResourceBundleFileNames.PAYROLL_LIST.toString());
            break;
        case "/userHistory":
            bundleNames.add(ResourceBundleFileNames.USER_HISTORY.toString());
			break;
        case "/payroll/detail":
	        bundleNames.add(ResourceBundleFileNames.PAYROLL_DETAIL.toString());
			break;
        case "/reimbursement/detail":
	        bundleNames.add(ResourceBundleFileNames.REIMBURSEMENT_DETAIL.toString());
			break;
        case "/reimbursement/list":
	        bundleNames.add(ResourceBundleFileNames.REIMBURSEMENT_LIST.toString());
			break;
        case "/projectAllocation/list":
	        bundleNames.add(ResourceBundleFileNames.PROJECT_ALLOCATION.toString());
			break;
		default:
			for (ResourceBundleFileNames resourceBundleFileNames : ResourceBundleFileNames.values()) {
				ResourceBundle bundle = ResourceBundle.getBundle(resourceBundleFileNames.toString(), locale);
				Enumeration<String> tmp = bundle.getKeys();
				while (tmp.hasMoreElements()) {
					String key = tmp.nextElement();
					keySet.add(key);
				}
			}
			return new ModelAndView("i18n", "keys", keySet);
		}
		for (String bundleName : bundleNames) {
			ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
			Enumeration<String> tmp = bundle.getKeys();
			while (tmp.hasMoreElements()) {
				String key = tmp.nextElement();
				keySet.add(key);
			}
		}
		return new ModelAndView("i18n", "keys", keySet);
	}
}	