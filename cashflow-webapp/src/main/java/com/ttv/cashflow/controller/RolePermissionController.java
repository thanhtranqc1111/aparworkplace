package com.ttv.cashflow.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.service.PermissionService;
import com.ttv.cashflow.service.RolePermissionService;
import com.ttv.cashflow.service.RoleService;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 30, 2017
 */
@Controller()
@RequestMapping("/grantPermission")
@PrototypeScope
public class RolePermissionController {
	@Autowired
	private PermissionService permissionService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private RolePermissionService rolePermissionService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
	    ModelAndView model = new ModelAndView("role-permission/role-permission");
	    model.addObject("roles", roleService.findAllRoles(-1, -1));
	    model.addObject("permissions", permissionService.findAllPermissions(-1, -1));
	    model.addObject("rolePermissionMap", rolePermissionService.getMapRolePermission());
	    System.out.println(rolePermissionService.getMapRolePermission());
        return model;
    }
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("data") String data, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(data).getAsJsonArray();
			rs = rolePermissionService.save(jsonArray);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ResponseUtil.createAjaxSuccess(rs);
	}
}
