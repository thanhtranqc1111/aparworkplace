package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.PayeeImportComponent;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.helper.SystemValueHelper;
import com.ttv.cashflow.service.AccountMasterService;
import com.ttv.cashflow.service.BankAccountMasterService;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.service.PayeePayerMasterService;
import com.ttv.cashflow.service.PayeePayerTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 3, 2017
 */
@Controller()
@RequestMapping("/admin/payeeMaster")
@PrototypeScope
public class PayeeMasterController extends ExchangeController {    
    @Autowired
    PayeePayerMasterService payeeMasterService;
    
    @Autowired
    BankAccountMasterService bankAccountMasterService;
    
    @Autowired
    AccountMasterService accountMasterService;
    
    @Autowired
    PayeePayerTypeMasterService payeeTypeMasterService;
    
    @Autowired
    private SystemValueHelper systemValueHelper;
    
    @Autowired
    private PayeeImportComponent payeeImportComponent;
    
    @Autowired
    private CurrencyMasterService currencyMasterService;
    
    private static final Logger logger = LoggerFactory.getLogger(PayeeMasterController.class);
    
    /**
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/payee");
       payeeMasterView.addObject("payeeMasterForm", new PayeePayerMaster());
       payeeMasterView.addObject("currencyList", parserCurrencyMasterList());
       payeeMasterView.addObject("payeeTypes", parserPayeeTypeMasterList());
       payeeMasterView.addObject("accountMasters", parserAccountList());
       payeeMasterView.addObject("bankAccounts", parserBankAccountList());
       payeeMasterView.addObject("adminPage", "payeeMaster");
       return payeeMasterView;
    }
    
    /**
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/api/getAllPagingPayee", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingPayee(final HttpServletRequest request, Model model) {
        String jPayeeMaster = "";
        try {
            Integer startpage = NumberUtils .toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jPayeeMaster = payeeMasterService.findPayeeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
        	e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jPayeeMaster;
    }
    
    @RequestMapping(value = "/api/findPayeePayerMasterByPrimaryKey/{id}", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String findPayeePayerMasterByPrimaryKey(@PathVariable("id") String id) {
        String jPayeeMaster = "";
        try {
            jPayeeMaster = payeeMasterService.findPayeePayerMasterByPrimaryKeyJson(id);
        } catch (Exception e) {
        	e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jPayeeMaster;
    }
    
    private Map<String, String> parserCurrencyMasterList() {
        List<CurrencyMaster> currencyList =currencyMasterService.findAllCurrencyMasters(-1, -1);
        Map<String, String> map = new TreeMap<String, String>();
        for (CurrencyMaster currencyMaster : currencyList) {
            map.put(String.valueOf(currencyMaster.getCode()), currencyMaster.getName());
        }
        return map;
    }
    
    /**
     * @return
     */
    private Map<String, String> parserPayeeTypeMasterList() {
        List<PayeePayerTypeMaster> payeeTypes = payeeTypeMasterService.findAllPayeePayerTypeMasters(-1, -1);
        Map<String, String> payeeTypesMap = new TreeMap<String, String>();
        for (PayeePayerTypeMaster payeeType : payeeTypes) {
            payeeTypesMap.put(String.valueOf(payeeType.getCode()), payeeType.getName());
        }
        return payeeTypesMap;
    }
    
    /**
     * @return
     */
    private Map<String, String> parserBankAccountList() {
        List<BankAccountMaster> bankAccounts = bankAccountMasterService.findAllBankAccountMasters(-1, -1);
        Map<String, String> bankAccountMap = new LinkedHashMap<String, String>();
        for (BankAccountMaster bankAccount : bankAccounts) {
            bankAccountMap.put(String.valueOf(bankAccount.getCode()), bankAccount.getBankAccountNo());
        }
        bankAccountMap.put("", "Don't have bank account");
        return bankAccountMap;
    }
    
    /**
     * @return
     */
    private Map<String, String> parserAccountList() {
        List<AccountMaster> accounts = accountMasterService.findAllAccountMasters(-1, -1);
        Map<String, String> accountMap = new LinkedHashMap<String, String>();
        for (AccountMaster account : accounts) {
            accountMap.put(account.getCode(), String.join(" - ", account.getCode(), account.getName()));
        }
        return accountMap;
    }
    
    /**
     * @param request
     * @param payeeMaster
     * @param result
     * @param model
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String savePayeeTypeMaster(HttpServletRequest request, @ModelAttribute("payeeMasterForm") @Valid PayeePayerMaster payeeMaster,
            BindingResult result, Model model) {
        try {
            String mode = request.getParameter("mode");
            if (Constant.MODE.NEW.getValue().equals(mode)) {
                
                PayeePayerMaster existPayeeMaster  = payeeMasterService.findPayeePayerMasterByPrimaryKey(payeeMaster.getCode());
                if (existPayeeMaster != null) {
                    return "redirect:/admin/payeeMaster/list";
                } 
            } 
            payeeMasterService.savePayeePayerMaster(payeeMaster);
            logger.info("Save suscess, Payee Payer Master: {}.", payeeMaster.toString());
            
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return "redirect:/admin/payeeMaster/list";
    }
    
    /**
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deletePayeeMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                payeeMasterService.deletePayeePayerMasterByCodes(codes);
                logger.info("Delete suscess, Payee Payer Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/payeeMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return payeeImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }

}