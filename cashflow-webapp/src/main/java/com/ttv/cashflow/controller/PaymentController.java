package com.ttv.cashflow.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.PaymentExportComponent;
import com.ttv.cashflow.component.PaymentOrderImportComponent;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.dao.BankMasterDAO;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dao.PayrollPaymentStatusDAO;
import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentStatusDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.domain.PayrollPaymentStatus;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.domain.ReimbursementPaymentStatus;
import com.ttv.cashflow.form.PaymentApInvoiceDetailForm;
import com.ttv.cashflow.form.PaymentForm;
import com.ttv.cashflow.form.PaymentPayrollDetailForm;
import com.ttv.cashflow.form.PaymentReimDetailForm;
import com.ttv.cashflow.form.PaymentSearchForm;
import com.ttv.cashflow.helper.ClaimTypeMasterDataHelper;
import com.ttv.cashflow.helper.CurrencyMasterDataHelper;
import com.ttv.cashflow.service.ApInvoicePaymentDetailsService;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.service.PayrollPaymentDetailsService;
import com.ttv.cashflow.service.PayrollService;
import com.ttv.cashflow.service.ReimbursementPaymentDetailsService;
import com.ttv.cashflow.service.ReimbursementService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

@Controller
@RequestMapping("/payment")
@PrototypeScope
public class PaymentController extends ExchangeController{
	
    @Autowired
	private PaymentOrderService paymentService;
	
	@Autowired
    private CurrencyMasterDataHelper currencyMasterHelper;
	
	@Autowired
    private ClaimTypeMasterDataHelper claimTypeMasterHelper;
	
	@Autowired
    private ApInvoicePaymentStatusDAO apInvoicePaymentStatusDAO;
	
	@Autowired
	private PayrollPaymentStatusDAO payrollPaymentStatusDAO;
	
	@Autowired
	private ReimbursementPaymentStatusDAO reimbursementPaymentStatusDAO;
    
    @Autowired
    private PaymentOrderDAO paymentDAO;
    
    @Autowired
    private PayeePayerMasterDAO payeePayerDAO;  
    
    @Autowired
    private ApInvoicePaymentDetailsService apInvoicePaymentDetailsService;
    
    @Autowired
    private PayrollPaymentDetailsService payrollPaymentDetailsService;
    
    @Autowired
    private ReimbursementPaymentDetailsService reimbursementPaymentDetailsService;
    
    @Autowired
    private ApInvoiceService apInvoiceService;
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private PayrollService payrollService;
    
    @Autowired
    private ReimbursementService reimbursementService;
    
    @Autowired
    private PayrollDAO payrollDAO;
    
    @Autowired
    private ReimbursementDAO reimbursementDAO;
    
    @Autowired
    private PaymentOrderImportComponent paymentImportComponent;
    
    @Autowired
    private PaymentExportComponent paymentExportComponent;
	
    @Autowired
    private BankMasterDAO bankMasterDAO;
    
    @Autowired
    private AccountMasterDAO accountMasterDAO;
    
    @Autowired
    private BankAccountMasterDAO bankAccountMasterDAO;
    
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
	
	@RequestMapping(value = "/list")
    public ModelAndView listPaymentOrder() {
    	 ModelAndView model = new ModelAndView("payment/list"); 
    	 Map<String, String> payStatus = systemValueHelper.toMap(Constant.PREFIX_PAY);
    	 model.addObject("payStatus", payStatus);
         return model;
    }
	
    
	@RequestMapping(value = "/api/getAllPagingPaymentOrder", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
   	@ResponseBody
   	public String getAllPagingPaymentOrder(final HttpServletRequest request, Model model) {
   		String jsonData = "";
   		try {
   			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
   			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
   			String orderBy = request.getParameter("order[0][dir]");
   			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));

   			String fromDate = request.getParameter("fromDate");
   			String toDate = request.getParameter("toDate");
   			String bankRef = request.getParameter("bankRef");
   			
   			// binh.lv
   			String paymentOrderNoFrom = request.getParameter("paymentOrderNoFrom");
   			String paymentOrderNoTo = request.getParameter("paymentOrderNoTo");
 
   			String status = request.getParameter("status");
   			 
   			jsonData = paymentService.findPaymentOrderJSON(startpage, lengthOfpage, fromDate, toDate,
   					bankRef, paymentOrderNoFrom, paymentOrderNoTo, status, orderColumn, orderBy);

   		} catch (Exception e) {
   			e.printStackTrace();
   			return ResponseUtil.createAjaxError(new ArrayList<>());
   		}
   		return jsonData;
    }
	
    @GetMapping(value = {"/detail", "/detail/{paymentNo}"})
    public ModelAndView detail(@PathVariable Optional<String> paymentNo) {
        
        ModelAndView mav = new ModelAndView("payment/detail");
        mav.addObject("claimTypeMap",  claimTypeMasterHelper.toMap());
        
        PaymentForm paymentForm = new PaymentForm();
        
        if (paymentNo.isPresent()) {
        	PaymentOrder paymentOrder = paymentService.findPaymentOrderByPaymentOrderNoOnly(paymentNo.get());
        	
        	if(paymentOrder != null) {
                BeanUtils.copyProperties(paymentOrder, paymentForm);
                
                List<PaymentApInvoiceDetailForm> detailForms = loadPaymentAPDetail(paymentOrder);
                paymentForm.setDetailApForms(detailForms);
                
                List<PaymentPayrollDetailForm> detailPRForms = loadPaymentPRDetail(paymentOrder);
                paymentForm.setDetailPayrollForms(detailPRForms);
                
                List<PaymentReimDetailForm> detailReimForms = loadPaymentReimDetail(paymentOrder);
                paymentForm.setDetailReimForms(detailReimForms);
                
                //thoai.nh get status to show in UI
                Map<String, String> payStatus = systemValueHelper.toMap(Constant.PREFIX_PAY);
                paymentForm.setStatusCode(paymentForm.getStatus());
                paymentForm.setStatus(payStatus.get(paymentForm.getStatus()));
                mav.addObject("mapPaymentStatus", payStatus);
        	} else {
        		return new ModelAndView("redirect:/");
        	}
        } else {
            paymentForm.getSearchForm().setIsPaid(Boolean.TRUE);
            paymentForm.getSearchForm().setIsApproval(Boolean.TRUE);
            paymentForm.setPaymentOrderNo(paymentDAO.getPaymentNo());
        }
        
    	mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
    	mav.addObject("listPayeeMaster", payeePayerDAO.findAllPayeePayerMasters());
    	Set<BankMaster> setBankMaster =  bankMasterDAO.findAllBankMasters();
    	mav.addObject("listBankMaster", setBankMaster);
    	if(setBankMaster.size() > 0) {
    		Iterator<BankMaster> iterator = setBankMaster.iterator();
    		BankMaster bankMaster = iterator.next();
    		mav.addObject("listBankAccountMaster", bankAccountMasterDAO.findBankAccountMasterByBankCode(bankMaster.getCode()));
    	}
        mav.addObject("paymentForm", paymentForm);
        mav.addObject("mapPaymentStatus", systemValueHelper.toMap(Constant.PREFIX_PAY));
        return mav;
    }

    private List<PaymentApInvoiceDetailForm> loadPaymentAPDetail(PaymentOrder paymentOrder) {
        List<PaymentApInvoiceDetailForm> detailForms = new ArrayList<>();

        Set<ApInvoicePaymentDetails> getApInvoicePaymentDetailses = paymentOrder.getApInvoicePaymentDetailses();
        
        for(Iterator<ApInvoicePaymentDetails> iter = getApInvoicePaymentDetailses.iterator(); iter.hasNext();){
            ApInvoicePaymentDetails detail = iter.next();
            
            PaymentApInvoiceDetailForm detailForm = new PaymentApInvoiceDetailForm();
            BeanUtils.copyProperties(detail, detailForm);
            
            //
            detailForm.setDetailId(detail.getId());
            
            //Get status paid set to form
            ApInvoicePaymentStatus invoicePaymentStatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByApInvoiceId(detail.getApInvoice().getId());
            detailForm.setPaidIncludeGstOriginalAmount(invoicePaymentStatus.getPaidIncludeGstOriginalAmount());
            detailForm.setUnpaidIncludeGstOriginalAmount(invoicePaymentStatus.getUnpaidIncludeGstOriginalAmount());
            
            //Get ap invoice and set to form
            ApInvoice apInvoice = apInvoiceService.findApInvoiceByPrimaryKey(detail.getApInvoice().getId());
            
            BeanUtils.copyProperties(apInvoice, detailForm);
            detailForm.setApInvoiceId(apInvoice.getId());
            
            // get total original amount of ap invoice without original amount of this payment order 
            BigDecimal totalOriginalAmount = apInvoicePaymentDetailsService.getTotalOriginalAmountByApInvoiceId(paymentOrder.getId(), detail.getApInvoice().getId());
            BigDecimal totalOriginalAmountInput = detailForm.getIncludeGstOriginalAmount().subtract(totalOriginalAmount == null ? BigDecimal.ZERO : totalOriginalAmount);
            detailForm.setPaidIncludeGstOriginalAmountWaiting(totalOriginalAmountInput);
            
            //
            detailForms.add(detailForm);
        }
        
        //Sort payment no
        Collections.sort(detailForms, (o1, o2) -> o1.getApInvoiceNo().compareTo(o2.getApInvoiceNo()));
        
        //
        return detailForms;
    }
    
    private List<PaymentPayrollDetailForm> loadPaymentPRDetail(PaymentOrder paymentOrder) {
        List<PaymentPayrollDetailForm> detailForms = new ArrayList<>();

        Set<PayrollPaymentDetails> getPayrollPaymentDetails = paymentOrder.getPayrollPaymentDetailses();
        
        for(Iterator<PayrollPaymentDetails> iter = getPayrollPaymentDetails.iterator(); iter.hasNext();){
        	PayrollPaymentDetails detail = iter.next();
            
            PaymentPayrollDetailForm detailForm = new PaymentPayrollDetailForm();
            BeanUtils.copyProperties(detail, detailForm);
            
            //
            detailForm.setDetailId(detail.getId());
            
            //Get status paid set to form
    		Set<PayrollPaymentStatus> setPayrollPaymentStatus = payrollPaymentStatusDAO.findPayrollPaymentStatusByPayrollId(detail.getPayroll().getId());
			if(!setPayrollPaymentStatus.isEmpty()) {
				Iterator<PayrollPaymentStatus> iterator = setPayrollPaymentStatus.iterator();
				PayrollPaymentStatus payrollPaymentStatus = iterator.next();
				detailForm.setPaidNetPayment(payrollPaymentStatus.getPaidNetPayment());
	            detailForm.setUnpaidNetPayment(payrollPaymentStatus.getUnpaidNetPayment());
			}
            
            //Get payroll and set to form
			Payroll payroll = payrollService.findPayrollByPrimaryKey(detail.getPayroll().getId());
            
            BeanUtils.copyProperties(payroll, detailForm);
            detailForm.setPayrollId(payroll.getId());
            
            // get total original amount of payroll without original amount of this payment order 
            BigDecimal totalOriginalAmount = payrollPaymentDetailsService.getTotalNetPaymentAmountOriginalByPayrollId(paymentOrder.getId(), detail.getPayroll().getId());
            BigDecimal totalOriginalAmountInput = detailForm.getTotalNetPaymentOriginal().subtract(totalOriginalAmount == null ? BigDecimal.ZERO : totalOriginalAmount);
            detailForm.setPaidNetPaymentAmountWaiting(totalOriginalAmountInput);
            
            //
            detailForms.add(detailForm);
        }
        
        //Sort payment no
        Collections.sort(detailForms, (o1, o2) -> o1.getPayrollNo().compareTo(o2.getPayrollNo()));
        
        //
        return detailForms;
    }
    
    
    private List<PaymentReimDetailForm> loadPaymentReimDetail(PaymentOrder paymentOrder) {
        List<PaymentReimDetailForm> detailForms = new ArrayList<>();

        Set<ReimbursementPaymentDetails> getReimPaymentDetailses = paymentOrder.getReimbursementPaymentDetailses();
        
        for(Iterator<ReimbursementPaymentDetails> iter = getReimPaymentDetailses.iterator(); iter.hasNext();){
        	ReimbursementPaymentDetails detail = iter.next();
            
        	PaymentReimDetailForm detailForm = new PaymentReimDetailForm();
            BeanUtils.copyProperties(detail, detailForm);
            
            //
            detailForm.setDetailId(detail.getId());
            detailForm.setPaymentReimOriginalAmount(detail.getPaymentOriginalAmount());
            detailForm.setPaymentReimConvertedAmount(detail.getPaymentConvertedAmount());
            
            //Get status paid set to form
            Set<ReimbursementPaymentStatus> reimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(detail.getId());
            reimbursementPaymentStatus.stream().forEach( r -> {
            	detailForm.setPaidOriginalAmount(r.getPaidReimbursementOriginalPayment());
            	detailForm.setUnpaidOriginalAmount(r.getUnpaidReimbursementOriginalPayment());
            });
            
            //Get reimbursement and set to form
            Reimbursement reimbursement = reimbursementService.findReimbursementByPrimaryKey(detail.getReimbursement().getId());
            
            BeanUtils.copyProperties(reimbursement, detailForm);
            detailForm.setReimId(reimbursement.getId());
            
            // get total orignal amount of reimbursement no without payment of this payment order
            BigDecimal totolReimAmountOriginal = reimbursementPaymentDetailsService.getTotalReimAmountOriginalByReimId(paymentOrder.getId(), reimbursement.getId());
            BigDecimal totolReimAmountOriginalInput = detailForm.getIncludeGstTotalOriginalAmount().subtract(totolReimAmountOriginal == null ? BigDecimal.ZERO : totolReimAmountOriginal);
            detailForm.setPaidOriginalAmountWaiting(totolReimAmountOriginalInput);
            
            //
            detailForms.add(detailForm);
        }
        
        //Sort payment no
        Collections.sort(detailForms, (o1, o2) -> o1.getReimbursementNo().compareTo(o2.getReimbursementNo()));
        
        //
        return detailForms;
    }
    
    @RequestMapping(value = "/action", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView action(@ModelAttribute("paymentForm") PaymentForm paymentForm,  HttpServletRequest request, 
                            Model model, RedirectAttributes redirectAttr,  BindingResult result) {
        
        String action = request.getParameter("action");

        // change local
        if(null == action) {
        	return new ModelAndView(new RedirectView("detail"));
        	
        } else {
        	
        	if (action.equals("search")) {
        		return searchDetails(paymentForm, model);
        	} else {
        		PaymentOrder paymentOrder = savePaymentOrder(paymentForm);
        		savePaymentDetail(paymentForm, paymentOrder);
        		logger.info("Save success, Payment Order: {}.", paymentOrder.toString());
        		//
        		redirectAttr.addFlashAttribute("message", paymentOrder.getPaymentOrderNo() + " saved successfully.");
        		return new ModelAndView(new RedirectView("detail/"+paymentOrder.getPaymentOrderNo()));
        	}
        }
    }

    private void savePaymentDetail(PaymentForm paymentForm, PaymentOrder paymentOrder) {
        /*Save payment apinvoice*/
        List<PaymentApInvoiceDetailForm> detailForms = paymentForm.getDetailApForms();
        removeDetailInvalid(detailForms);
        for(PaymentApInvoiceDetailForm detailForm : detailForms){
            //Obtain data
            ApInvoicePaymentDetails detail = new ApInvoicePaymentDetails();
            BeanUtils.copyProperties(detailForm, detail);
            
            detail.setId(detailForm.getDetailId());
            
            detail.setPaymentOrder(paymentOrder);
            
            ApInvoice apInvoice = new ApInvoice(); apInvoice.setId(detailForm.getApInvoiceId());
            detail.setApInvoice(apInvoice);
            
            detail.setModifiedDate(Calendar.getInstance());
            
            if(null == detailForm.getDetailId()){
                detail.setCreatedDate(Calendar.getInstance());
            }
            
            //
            apInvoicePaymentDetailsService.saveApInvoicePaymentDetails(detail);
        }
        
        /*Save Payment Payroll*/
        List<PaymentPayrollDetailForm> detailPrForms = paymentForm.getDetailPayrollForms();
        removePRDetailInvalid(detailPrForms);
        for(PaymentPayrollDetailForm detailForm : detailPrForms){
            //Obtain data
            PayrollPaymentDetails detail = new PayrollPaymentDetails();
            BeanUtils.copyProperties(detailForm, detail);
            
            detail.setId(detailForm.getDetailId());
            
            detail.setPaymentOrder(paymentOrder);
            
            Payroll payroll = new Payroll();
            payroll.setId(detailForm.getPayrollId());
            detail.setPayroll(payroll);
            
            detail.setModifiedDate(Calendar.getInstance());
            
            if(null == detailForm.getDetailId()){
                detail.setCreatedDate(Calendar.getInstance());
            }
            
            //
            payrollPaymentDetailsService.savePayrollPaymentDetails(detail);
        }
        
        /*Save Payment Reimbursement*/
        List<PaymentReimDetailForm> detailReimForms = paymentForm.getDetailReimForms();
        removeReimDetailInvalid(detailReimForms);
        for(PaymentReimDetailForm detailForm : detailReimForms){
            //Obtain data
            ReimbursementPaymentDetails detail = new ReimbursementPaymentDetails();
            BeanUtils.copyProperties(detailForm, detail);
            
            detail.setId(detailForm.getDetailId());
            detail.setPaymentOrder(paymentOrder);
            
            Reimbursement reimbursement = new Reimbursement();
            reimbursement.setId(detailForm.getReimId());
            detail.setReimbursement(reimbursement);
            detail.setModifiedDate(Calendar.getInstance());
            detail.setPaymentOriginalAmount(detailForm.getPaymentReimOriginalAmount());
            detail.setPaymentConvertedAmount(detailForm.getPaymentReimConvertedAmount());
            
            if(null == detailForm.getDetailId()){
                detail.setCreatedDate(Calendar.getInstance());
            }
            
            //
            reimbursementPaymentDetailsService.saveReimbursementPaymentDetails(detail);
        }
    }

    private PaymentOrder savePaymentOrder(PaymentForm paymentForm) {
        PaymentOrder paymentOrder = new PaymentOrder();
        
        BeanUtils.copyProperties(paymentForm, paymentOrder);
        paymentOrder.setModifiedDate(Calendar.getInstance());
        if(null == paymentForm.getId()){
            paymentOrder.setStatus(Constant.PAY_PROCESSING);
            paymentOrder.setCreatedDate(Calendar.getInstance());
        }
        else 
        {
        	paymentOrder.setStatus(paymentForm.getStatusCode());
        }
        //
        paymentOrder = paymentService.savePaymentOrder(paymentOrder);
        
        return paymentOrder;
    }

    private ModelAndView searchDetails(PaymentForm paymentForm, Model model) {
        PaymentSearchForm searchForm = paymentForm.getSearchForm();

        // search ap detail
        List<PaymentApInvoiceDetailForm> detailApForms = doSearchDetails(searchForm);
        for(PaymentApInvoiceDetailForm detailForm : detailApForms){
           Boolean isApproval = apInvoiceDAO.checkApprovalApInvoice(detailForm.getApInvoiceNo());
           detailForm.setIsApproval(isApproval);
        }
        
        //search payroll detail
        List<PaymentPayrollDetailForm> detailPayrollForms = dosearchPayroll(searchForm);
        for(PaymentPayrollDetailForm detailForm : detailPayrollForms){
        	Boolean isApproval = payrollDAO.checkApprovalPayroll(detailForm.getPayrollNo());
        	detailForm.setIsApproval(isApproval);
        }
        
        //search reim detail
        List<PaymentReimDetailForm> detailReimForms = dosearchReimbursement(searchForm);
        for(PaymentReimDetailForm detailForm : detailReimForms){
        	Boolean isApproval = reimbursementDAO.checkApprovalReimbursement(detailForm.getReimbursementNo());
        	detailForm.setIsApproval(isApproval);
        }
        
        if (searchForm.getIsApproval()) {
        	//Remove detail is not approved when search AP invoices are approved.
            Iterator<PaymentApInvoiceDetailForm> detailApFormIter = detailApForms.iterator();
            while (detailApFormIter.hasNext()) {
                PaymentApInvoiceDetailForm detailForm = detailApFormIter.next();
                if (!detailForm.getIsApproval()) {
                	detailApFormIter.remove();
                }
            }
            
            //Remove detail is not approved when search payroll are approved.
        	Iterator<PaymentPayrollDetailForm> detailPRFormIter = detailPayrollForms.iterator();
            while (detailPRFormIter.hasNext()) {
                PaymentPayrollDetailForm detailForm = detailPRFormIter.next();
                if (!detailForm.getIsApproval()) {
                	detailPRFormIter.remove();
                }
            }
            
            //Remove detail is not approved when search payroll are approved.
        	Iterator<PaymentReimDetailForm> detailReimFormIter = detailReimForms.iterator();
            while (detailReimFormIter.hasNext()) {
            	PaymentReimDetailForm detailForm = detailReimFormIter.next();
                if (!detailForm.getIsApproval()) {
                	detailReimFormIter.remove();
                }
            }
            
        }
        
        paymentForm.setDetailApForms(detailApForms);
        paymentForm.setDetailPayrollForms(detailPayrollForms);
        paymentForm.setDetailReimForms(detailReimForms);
        
        model.addAttribute("claimTypeMap",  claimTypeMasterHelper.toMap());
        model.addAttribute("paymentForm", paymentForm);
        model.addAttribute("listAccountMaster", accountMasterDAO.findAllAccountMasters());
        model.addAttribute("listPayeeMaster", payeePayerDAO.findAllPayeePayerMasters());
        Set<BankMaster> setBankMaster =  bankMasterDAO.findAllBankMasters();
        model.addAttribute("listBankMaster", setBankMaster);
        model.addAttribute("listBankAccountMaster", bankAccountMasterDAO.findBankAccountMasterByBankCode(paymentForm.getBankName()));
        
        return new ModelAndView("payment/detail");
    }
    
    private List<PaymentApInvoiceDetailForm> doSearchDetails(PaymentSearchForm searchForm) {
        //Do search
    	String monthFrom = StringUtils.stripToEmpty(searchForm.getMonthFrom());
        String monthTo = StringUtils.stripToEmpty(searchForm.getMonthTo());
        String scheduledPaymentDateFrom = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateFrom());
        String scheduledPaymentDateTo = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateTo());
        String claimType = StringUtils.stripToEmpty(searchForm.getClaimType());
        String status = searchForm.getIsPaid() ? Constant.API_PAID : StringUtils.EMPTY;
        
        Set<ApInvoice> apInvoices = paymentService.searchApInvoiceDetails(monthFrom, monthTo, scheduledPaymentDateFrom ,scheduledPaymentDateTo, claimType, status, Constant.API_CANCELLED);
        
        // Copy data to form
        List<PaymentApInvoiceDetailForm> detailForms = new ArrayList<>();
        
        for(Iterator<ApInvoice> iter = apInvoices.iterator(); iter.hasNext();){
            ApInvoice apInvoice = iter.next();
            
            PaymentApInvoiceDetailForm detailForm = new PaymentApInvoiceDetailForm();
            
            BeanUtils.copyProperties(apInvoice, detailForm);
            
            //
            detailForm.setApInvoiceId(apInvoice.getId());
            
            //Get status paid
            ApInvoicePaymentStatus invoicePaymentStatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByApInvoiceId(apInvoice.getId());
            detailForm.setPaidIncludeGstOriginalAmount(invoicePaymentStatus.getPaidIncludeGstOriginalAmount());
            detailForm.setUnpaidIncludeGstOriginalAmount(invoicePaymentStatus.getUnpaidIncludeGstOriginalAmount());
            
            // Get bank name and code
            PayeePayerMaster payeePayerMaster = payeePayerDAO.findPayeePayerMasterByCode(apInvoice.getPayeeCode());
            detailForm.setPayeeBankName(payeePayerMaster.getBankName());
            detailForm.setPayeeBankAccNo(payeePayerMaster.getBankAccountCode());
            
            detailForm.setAccountCode(payeePayerMaster.getAccountCode());
            String accountName = accountMasterDAO.findAccountMasterByCode(payeePayerMaster.getAccountCode()).getName();
            detailForm.setAccountName(accountName);
            
            // get total original amount of ap invoice without original amount of this payment order 
            BigDecimal totalOriginalAmount = apInvoicePaymentDetailsService.getTotalOriginalAmountByApInvoiceId(BigInteger.ZERO, apInvoice.getId());
            BigDecimal totalOriginalAmountInput = detailForm.getIncludeGstOriginalAmount().subtract(totalOriginalAmount == null ? BigDecimal.ZERO : totalOriginalAmount);
            detailForm.setPaidIncludeGstOriginalAmountWaiting(totalOriginalAmountInput);
            
            //set status of invoice
            detailForm.setIsPaid(apInvoice.getStatus().equals(Constant.API_PAID));
            
            //
            detailForm.setIsPayEnough(isPayEnoughFor(apInvoice));
            
            //
            detailForms.add(detailForm);
        }
        
        return detailForms;
    }
    
    private List<PaymentPayrollDetailForm> dosearchPayroll(PaymentSearchForm searchForm) {
        //Do search
    	String monthFrom = StringUtils.stripToEmpty(searchForm.getMonthFrom());
        String monthTo = StringUtils.stripToEmpty(searchForm.getMonthTo());
        String scheduledPaymentDateFrom = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateFrom());
        String scheduledPaymentDateTo = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateTo());
        String claimType = StringUtils.stripToEmpty(searchForm.getClaimType());
        String status = searchForm.getIsPaid() ? Constant.API_PAID : StringUtils.EMPTY;
        
        Set<Payroll> payrolls = payrollService.searchPayrollDetails(monthFrom, monthTo, scheduledPaymentDateFrom, scheduledPaymentDateTo, claimType, status, Constant.API_CANCELLED);
        
        // Copy data to form
        List<PaymentPayrollDetailForm> detailForms = new ArrayList<>();
        
        for(Iterator<Payroll> iter = payrolls.iterator(); iter.hasNext();){
        	Payroll payroll = iter.next();
            
            PaymentPayrollDetailForm detailForm = new PaymentPayrollDetailForm();
            
            BeanUtils.copyProperties(payroll, detailForm);
            
            //
            detailForm.setPayrollId(payroll.getId());
            
            //Get status paid set to form
            Set<PayrollPaymentStatus> payrollPaymentStatus = payrollPaymentStatusDAO.findPayrollPaymentStatusByPayrollId(payroll.getId());
            payrollPaymentStatus.stream().forEach(p -> {
            	detailForm.setPaidNetPayment(p.getPaidNetPayment());
            	detailForm.setUnpaidNetPayment(p.getUnpaidNetPayment());
            });
            
            // get total net payment amount of payroll no without net payment of this payment order 
            BigDecimal totalPaidNetPaymentAmount = payrollPaymentDetailsService.getTotalNetPaymentAmountOriginalByPayrollId(BigInteger.ZERO, payroll.getId());
            BigDecimal totalPaidNetPaymentAmountInput = detailForm.getTotalNetPaymentOriginal().subtract(totalPaidNetPaymentAmount == null ? BigDecimal.ZERO : totalPaidNetPaymentAmount);
            detailForm.setPaidNetPaymentAmountWaiting(totalPaidNetPaymentAmountInput);
            
            //set status of payroll
            detailForm.setIsPaid(payroll.getStatus().equals(Constant.API_PAID));
            
            //
            detailForm.setIsPayEnough(isPayEnoughForPR(payroll));
            
            //
            detailForms.add(detailForm);
        }
        
        return detailForms;
    }
    
    private List<PaymentReimDetailForm> dosearchReimbursement(PaymentSearchForm searchForm) {
        //Do search
    	String monthFrom = StringUtils.stripToEmpty(searchForm.getMonthFrom());
        String monthTo = StringUtils.stripToEmpty(searchForm.getMonthTo());
        String scheduledPaymentDateFrom = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateFrom());
        String scheduledPaymentDateTo = StringUtils.stripToEmpty(searchForm.getScheduledPaymentDateTo());
        String claimType = StringUtils.stripToEmpty(searchForm.getClaimType());
        String status = searchForm.getIsPaid() ? Constant.API_PAID : StringUtils.EMPTY;
        
        Set<Reimbursement> reimbursements = reimbursementService.searchReimDetails(monthFrom, monthTo, scheduledPaymentDateFrom, scheduledPaymentDateTo, claimType, status, Constant.API_CANCELLED);
        
        // Copy data to form
        List<PaymentReimDetailForm> detailForms = new ArrayList<>();
        
        for(Iterator<Reimbursement> iter = reimbursements.iterator(); iter.hasNext();){
        	Reimbursement reim = iter.next();
            
        	PaymentReimDetailForm detailForm = new PaymentReimDetailForm();
            
            BeanUtils.copyProperties(reim, detailForm);
            
            //
            detailForm.setReimId(reim.getId());
            
            //Get status paid set to form
            Set<ReimbursementPaymentStatus> reimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(reim.getId());
            reimbursementPaymentStatus.stream().forEach( r -> {
            	detailForm.setPaidOriginalAmount(r.getPaidReimbursementOriginalPayment());
            	detailForm.setUnpaidOriginalAmount(r.getUnpaidReimbursementOriginalPayment());
            });
            // get total orignal amount of reimbursement no without payment of this payment order
            BigDecimal totolReimAmountOriginal = reimbursementPaymentDetailsService.getTotalReimAmountOriginalByReimId(BigInteger.ZERO, reim.getId());
            BigDecimal totolReimAmountOriginalInput = detailForm.getIncludeGstTotalOriginalAmount().subtract(totolReimAmountOriginal == null ? BigDecimal.ZERO : totolReimAmountOriginal);
            detailForm.setPaidOriginalAmountWaiting(totolReimAmountOriginalInput);
            
            //set status of reim
            detailForm.setIsPaid(reim.getStatus().equals(Constant.API_PAID));
            
            //
            detailForm.setIsPayEnough(isPayEnoughForReim(reim));
            
            //
            detailForms.add(detailForm);
        }
        
        return detailForms;
    }
    
    @GetMapping(value = {"/copy/{paymentNo}"})
    public ModelAndView copy(@PathVariable Optional<String> paymentNo) {
        
        ModelAndView mav = new ModelAndView("payment/detail");
        mav.addObject("currencyMap", currencyMasterHelper.toMap());
        
        PaymentForm paymentForm = new PaymentForm();
        
        if (paymentNo.isPresent()) {
        	PaymentOrder paymentOrder = paymentService.findPaymentOrderByPaymentOrderNoOnly(paymentNo.get());
        	if(!paymentOrder.equals(null)) {
                BeanUtils.copyProperties(paymentOrder, paymentForm);

                // set new payment no
                paymentForm.setPaymentOrderNo(paymentDAO.getPaymentNo());
                
                paymentForm.setDetailApForms(null);
                paymentForm.setIncludeGstConvertedAmount(BigDecimal.ZERO);
                paymentForm.setIncludeGstOriginalAmount(BigDecimal.ZERO);
                paymentForm.setStatusCode("");
                paymentForm.setStatus("");
                paymentForm.getSearchForm().setIsPaid(Boolean.TRUE);
                paymentForm.getSearchForm().setIsApproval(Boolean.TRUE);
        	} else {
        		return new ModelAndView("redirect:/");
        	}
        } else {
            paymentForm.getSearchForm().setIsPaid(Boolean.TRUE);
            paymentForm.getSearchForm().setIsApproval(Boolean.TRUE);
            paymentForm.setPaymentOrderNo(paymentDAO.getPaymentNo());
        }
        
    	mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
    	mav.addObject("listPayeeMaster", payeePayerDAO.findAllPayeePayerMasters());
    	Set<BankMaster> setBankMaster =  bankMasterDAO.findAllBankMasters();
    	mav.addObject("listBankMaster", setBankMaster);
    	if(setBankMaster.size() > 0) {
    		Iterator<BankMaster> iterator = setBankMaster.iterator();
    		BankMaster bankMaster = iterator.next();
    		mav.addObject("listBankAccountMaster", bankAccountMasterDAO.findBankAccountMasterByBankCode(bankMaster.getCode()));
    	}
    	
    	paymentForm.setId(null);
        mav.addObject("paymentForm", paymentForm);
        mav.addObject("isCopy", true);
        return mav;
    }
    
    /**
     * Check AP Invoice is have pay enough.
     */
    private boolean isPayEnoughFor(ApInvoice apInvoice){
        BigDecimal amountPayFor =  paymentDAO.getOriginalAmountPayFor(apInvoice.getId());
        if (amountPayFor != null && amountPayFor.compareTo(apInvoice.getIncludeGstOriginalAmount()) == 0) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Check Pyroll is have pay enough.
     */
    private boolean isPayEnoughForPR(Payroll payroll){
        BigDecimal amountPayFor =  paymentDAO.getOriginalAmountPayForPR(payroll.getId());
        if (amountPayFor != null && amountPayFor.compareTo(payroll.getTotalNetPaymentOriginal()) == 0) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Check Reimbursement is have pay enough.
     */
    private boolean isPayEnoughForReim(Reimbursement reim){
        BigDecimal amountPayFor =  paymentDAO.getOriginalAmountPayForReim(reim.getId());
        if (amountPayFor != null && amountPayFor.compareTo(reim.getIncludeGstTotalOriginalAmount()) == 0) {
            return true;
        }
        
        return false;
    }
    
    private void removeDetailInvalid(List<PaymentApInvoiceDetailForm> detailForms) {
        Iterator<PaymentApInvoiceDetailForm> iter = detailForms.iterator();

        while (iter.hasNext()) {
            PaymentApInvoiceDetailForm object = iter.next();
            if (null == object.getApInvoiceId()) {
                iter.remove();
            }
        }
    }
    
    private void removePRDetailInvalid(List<PaymentPayrollDetailForm> detailForms) {
        Iterator<PaymentPayrollDetailForm> iter = detailForms.iterator();

        while (iter.hasNext()) {
        	PaymentPayrollDetailForm object = iter.next();
            if (null == object.getPayrollId()) {
                iter.remove();
            }
        }
    }
    
    private void removeReimDetailInvalid(List<PaymentReimDetailForm> detailForms) {
        Iterator<PaymentReimDetailForm> iter = detailForms.iterator();

        while (iter.hasNext()) {
        	PaymentReimDetailForm object = iter.next();
            if (null == object.getReimId()) {
                iter.remove();
            }
        }
    }
    
    @Override
    public ImportComponent getImportComponent(){
    	return paymentImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PAY").getValue();
    }

    @Override
    public ExportComponent getExportComponent() {
        return paymentExportComponent;
    }
}