
package com.ttv.cashflow.controller;

import static org.springframework.util.StringUtils.hasText;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApInvoiceExportComponent;
import com.ttv.cashflow.component.ApInvoiceImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.ApInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.form.ApInvoiceForm;
import com.ttv.cashflow.form.InvoiceDetailForm;
import com.ttv.cashflow.helper.ClaimTypeMasterDataHelper;
import com.ttv.cashflow.helper.CurrencyMasterDataHelper;
import com.ttv.cashflow.helper.GstMasterDataHelper;
import com.ttv.cashflow.helper.PayeePayerMasterDataHelper;
import com.ttv.cashflow.helper.SystemValueHelper;
import com.ttv.cashflow.service.ApInvoiceAccountDetailsService;
import com.ttv.cashflow.service.ApInvoiceClassDetailsService;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;
import com.ttv.cashflow.util.ResponseUtil;
import com.ttv.cashflow.validate.ApInvoiceValidator;

@Controller
@RequestMapping("/apinvoice")
@PrototypeScope
public class APInvoiceController extends ExchangeController {
    
    @Autowired
    private GstMasterDataHelper gstMasterHelper;

    @Autowired
    private ClaimTypeMasterDataHelper claimTypeMasterHelper;

    @Autowired
    private CurrencyMasterDataHelper currencyMasterHelper;

    @Autowired
    private PayeePayerMasterDataHelper payeePayerMasterHelper;

    @Autowired
    private ApInvoiceService apInvoiceService;

    @Autowired
    private ApInvoiceAccountDetailsDAO apInvoiceAccountDetailsDAO;
    
    @Autowired
    private ApInvoiceAccountDetailsService apInvoiceAccountDetailsService;
    
    @Autowired
    private ApInvoiceClassDetailsService apInvoiceClassDetailsService;

    @Autowired
    private ApInvoiceValidator apInvoiceValidator;
    
    @Autowired
    private SystemValueHelper systemValueHelper; 
    
    @Autowired
    private ApInvoiceDAO apInvoiceDAO;
    
    @Autowired
    private ApInvoiceImportComponent importComponent;
    
    @Autowired
    private ApInvoiceExportComponent exportComponent;
    
    @Autowired
    private AccountMasterDAO accountMasterDAO;
    
    @Autowired
    private GstMasterDAO gstMasterDAO;
    
    @Autowired
    private ApprovalPurchaseRequestDAO approvalPurchaseRequestDAO;
    
    private static final Logger logger = LoggerFactory.getLogger(APInvoiceController.class);
    
    @InitBinder(value = "apInvoice")
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(apInvoiceValidator);
    }
    
    @GetMapping(value = {"/detail", "/detail/{apNo}", "/detail/{apNo}/with/{approvalCode}"})
    public ModelAndView detail(@PathVariable Optional<String> apNo, @PathVariable Optional<String> approvalCode) {
        ModelAndView mav = new ModelAndView("apinvoice/detail");
        mav.addObject("currencyMap", currencyMasterHelper.toMap());
        mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
        mav.addObject("gstMap", gstMasterHelper.toMap());
        
        ApInvoiceForm apInvoiceForm = new ApInvoiceForm();

        ApInvoice apInvoice = null;

        if (apNo.isPresent()) {
        	Set<ApInvoice>  listApInvoice = apInvoiceDAO.findApInvoiceByApInvoiceNo(apNo.get());
        	if(listApInvoice.size() > 0) {
        	    Map<String, String> approvalMap = new HashMap<>();
        	    
	            Iterator<ApInvoice> iter = listApInvoice.iterator();
	            apInvoice = iter.next();
	
	            // Copy from entity to form
	            BeanUtils.copyProperties(apInvoice, apInvoiceForm);
	            
	            
	            // Update detail for ap - invoice
	            List<InvoiceDetailForm> invoiceDetailForms = new ArrayList<InvoiceDetailForm>();
	
	            List<InvoiceDetail> invoiceDetails = apInvoiceService.getInvoiceDetail(apInvoice.getId());
	            for (InvoiceDetail invoiceDetail : invoiceDetails) {
	                InvoiceDetailForm invoiceDetailForm = new InvoiceDetailForm();
	                BeanUtils.copyProperties(invoiceDetail, invoiceDetailForm);
	                if(invoiceDetail.getIsApproval() != null && invoiceDetail.getIsApproval()) {
	                	invoiceDetailForm.setApprStatusCode(Constant.APR_APPROVAL);
	                	invoiceDetailForm.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_APPROVAL).getValue());
	                }
	                else
	                {
	                	invoiceDetailForm.setApprStatusCode(Constant.APR_WAITING);
	                	invoiceDetailForm.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());
	                }
	                invoiceDetailForms.add(invoiceDetailForm);
	                
	                //build map approval code - amount
	                String apCode = invoiceDetail.getApprovalCode();
	                Iterator<ApprovalPurchaseRequest> approvalIter = approvalPurchaseRequestDAO.findApprovalPurchaseRequestByApprovalCode(apCode).iterator();
	                
	                if(approvalIter.hasNext()){
	                    ApprovalPurchaseRequest approval = approvalIter.next();
	                    approvalMap.put(apCode, ObjectUtils.toString(approval.getIncludeGstOriginalAmount()));
	                }
	            }
	            
	            //add approval(as json string) to model.
	            mav.addObject("approvalJson", new Gson().toJson(approvalMap));

	            apInvoiceForm.setDetailForms(invoiceDetailForms);
	            
	            //thoai.nh get status to show in UI
	            Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_API);
	            apInvoiceForm.setStatusVal(mapStatus.get(apInvoiceForm.getStatus()));
	            
        	}
        	else {
        		return new ModelAndView("redirect:/");
        	}
        } else {
        	// binh.lv update
            apInvoiceForm.setBookingDate(Calendar.getInstance());
            apInvoiceForm.setMonth(Calendar.getInstance());
            apInvoiceForm.setApInvoiceNo(apInvoiceDAO.getApInvoiceNo());
        }

        mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
    	mav.addObject("listGstMaster", gstMasterDAO.findAllGstMasters());
    	if(approvalCode.isPresent()){
    	    mav.addObject("approvalCode", approvalCode.get());
    	}
    	
        mav.addObject("apInvoiceForm", apInvoiceForm);
        mav.addObject("apr002", systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING));
        mav.addObject("mapAPIStatus", systemValueHelper.toMap(Constant.PREFIX_API));
        return mav;
    }
    
    @PostMapping(value = "/save")
    public String save(@ModelAttribute("apInvoiceForm") ApInvoiceForm apInvoiceForm, BindingResult result, RedirectAttributes redirectAttributes) {
        ApInvoice apInvoice = new ApInvoice();
        BeanUtils.copyProperties(apInvoiceForm, apInvoice);

        if(apInvoice.getId() == null){
            apInvoice.setStatus(Constant.API_NOT_PAID);
            // apInvoice.setMonth(apInvoiceForm.getCreatedDate()); 
        }
        
        apInvoice.setModifiedDate(Calendar.getInstance());
        
        apInvoiceService.saveApInvoice(apInvoice);
        
        //
        List<InvoiceDetailForm> invoiceDetailForms = apInvoiceForm.getDetailForms();
        removeDetailInvalid(invoiceDetailForms);
        
        doSaveApInvoiceDetails(invoiceDetailForms, apInvoice);
        logger.info("Save success, AP Invoice: {}.", apInvoice.toString());
        
        redirectAttributes.addFlashAttribute("message", apInvoice.getApInvoiceNo() + " saved successfully.");
        return "redirect:detail/" + apInvoice.getApInvoiceNo();
    }

    private void doSaveApInvoiceDetails(List<InvoiceDetailForm> invoiceDetailForms, ApInvoice apInvoice){
        
        Map<String, List<InvoiceDetailForm>> mapAccountWithDetail = getMapAccountCodeWithDetail(invoiceDetailForms);
        
        //Save detail
        for (Map.Entry<String, List<InvoiceDetailForm>> entry : mapAccountWithDetail.entrySet()) {

            String accountCode = entry.getKey();
            List<InvoiceDetailForm> invoiceDetailFormBelongAccountCode = entry.getValue();
            BigInteger apInvoiceAccountDetailId = getApInvoiceAccountDetailId(accountCode, invoiceDetailForms);

            ApInvoiceAccountDetails apInvoiceAccountDetails = new ApInvoiceAccountDetails();
            //
            BigDecimal excludeGstOriginalAmount = new BigDecimal(0);
            BigDecimal excludeGstConvertedAmount = new BigDecimal(0);
            BigDecimal gstConvertedAmount = new BigDecimal(0);
            BigDecimal includeGstConvertedAmount = new BigDecimal(0);

            InvoiceDetailForm invoiceDetailForm = new InvoiceDetailForm();

            for (int i = 0; i < invoiceDetailFormBelongAccountCode.size(); i++) {
                invoiceDetailForm = invoiceDetailFormBelongAccountCode.get(i);

                excludeGstOriginalAmount = excludeGstOriginalAmount.add(invoiceDetailForm.getExcludeGstOriginalAmount());
                excludeGstConvertedAmount = excludeGstConvertedAmount.add(invoiceDetailForm.getExcludeGstConvertedAmount());
                gstConvertedAmount = gstConvertedAmount.add(invoiceDetailForm.getGstConvertedAmount());
                includeGstConvertedAmount = includeGstConvertedAmount.add(invoiceDetailForm.getIncludeGstConvertedAmount());
            }
            
            apInvoiceAccountDetails.setId(apInvoiceAccountDetailId);
            apInvoiceAccountDetails.setApInvoice(apInvoice);
            apInvoiceAccountDetails.setAccountCode(accountCode);
            apInvoiceAccountDetails.setAccountName(invoiceDetailForm.getAccountName());
            apInvoiceAccountDetails.setExcludeGstOriginalAmount(excludeGstOriginalAmount);
            apInvoiceAccountDetails.setExcludeGstConvertedAmount(excludeGstConvertedAmount);
            apInvoiceAccountDetails.setGstConvertedAmount(gstConvertedAmount);
            apInvoiceAccountDetails.setIncludeGstConvertedAmount(includeGstConvertedAmount);
            apInvoiceAccountDetails.setCreatedDate(Calendar.getInstance());
            apInvoiceAccountDetails.setModifiedDate(Calendar.getInstance());

            apInvoiceAccountDetailsService.saveApInvoiceAccountDetails(apInvoiceAccountDetails);

            //save invoice class detail flowing account
            doSaveApInvoiceClassDetail(invoiceDetailFormBelongAccountCode, apInvoiceAccountDetails);
        }
    }
    

    /**
     * Build map account code flowing list detail. 
     */
    private Map<String, List<InvoiceDetailForm>> getMapAccountCodeWithDetail(List<InvoiceDetailForm> invoiceDetailForms) {
        Map<String, List<InvoiceDetailForm>> mapAccountWithDetail = new HashMap<>();
        
        for(InvoiceDetailForm invoiceDetailForm : invoiceDetailForms){
            String accountCode = invoiceDetailForm.getAccountCode();
            List<InvoiceDetailForm> invoiceDetailFormFiters = new ArrayList<>();
            
            if (!mapAccountWithDetail.containsKey(accountCode)) {
                
                for(InvoiceDetailForm invoiceDetailForm2 : invoiceDetailForms){
                    String accountCode2 = invoiceDetailForm2.getAccountCode();
                    if(accountCode.equals(accountCode2)){
                        invoiceDetailFormFiters.add(invoiceDetailForm2);
                    }
                }
                
                mapAccountWithDetail.put(accountCode, invoiceDetailFormFiters);
            }
        }
        
        return mapAccountWithDetail;
    }
    
    /**
     * Get ApInvoiceAccountDetailId belong accountCode from the form.
     */
    private BigInteger getApInvoiceAccountDetailId(String accountCode, List<InvoiceDetailForm> invoiceDetailForms) {
        for(InvoiceDetailForm form : invoiceDetailForms){
            if(accountCode.equals(form.getAccountCode())){
                if(Objects.nonNull(form.getApInvoiceAccountDetailId())){
                    return form.getApInvoiceAccountDetailId();
                }
            }
        }
        
        return null;
    }

    private void doSaveApInvoiceClassDetail(List<InvoiceDetailForm> invoiceDetailForms, ApInvoiceAccountDetails account) {
        for(InvoiceDetailForm invoiceDetailForm : invoiceDetailForms){
            ApInvoiceClassDetails apInvoiceClassDetails = new ApInvoiceClassDetails();
            
            BeanUtils.copyProperties(invoiceDetailForm, apInvoiceClassDetails);
            
            if(null == invoiceDetailForm.getApInvoiceClassDetailId()){
                apInvoiceClassDetails.setCreatedDate(Calendar.getInstance());
            } else {
                apInvoiceClassDetails.setId(invoiceDetailForm.getApInvoiceClassDetailId());
            }
            
            apInvoiceClassDetails.setApInvoiceAccountDetails(account);
            apInvoiceClassDetails.setIsApproval(hasText(apInvoiceClassDetails.getApprovalCode()));
            apInvoiceClassDetails.setCreatedDate(Calendar.getInstance());
            apInvoiceClassDetails.setModifiedDate(Calendar.getInstance());
            //
            apInvoiceClassDetailsService.saveApInvoiceClassDetails(apInvoiceClassDetails);
        }
    }

    private void removeDetailInvalid(List<InvoiceDetailForm> invoiceDetailForms) {
		Iterator<InvoiceDetailForm> iter = invoiceDetailForms.iterator();

		while (iter.hasNext()) {
			InvoiceDetailForm object = iter.next();
			if (StringUtils.isEmpty(object.getAccountCode())) {
				iter.remove();
			}
		}
	}
    
    @RequestMapping(value = "/list")
    public ModelAndView listARInvoice() {
    	 ModelAndView model = new ModelAndView("apinvoice/list"); 
    	 Set<PayeePayerMaster> lstPayeePayer = payeePayerMasterHelper.list();
 		 Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_API);
 		 
    	 model.addObject("lstPayeePayer", lstPayeePayer);
    	 model.addObject("mapStatus", mapStatus);
         return model;
    }
    
    @RequestMapping(value = "/api/getAllPagingAPInvoice", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
   	@ResponseBody
   	public String getAllPagingAPInvoice(final HttpServletRequest request, Model model) {
   		String jsonData = "";
   		try {
   			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
   			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
   			String orderBy = request.getParameter("order[0][dir]");
   			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));

   			String fromMonth = request.getParameter("fromMonth");
   			String toMonth = request.getParameter("toMonth");
   			String fromApNo = request.getParameter("fromApNo");
   			String toApNo = request.getParameter("toApNo");
   			String payeeName = request.getParameter("payeeName");
   			String invoiceNo = request.getParameter("invoiceNo");
   			String status = request.getParameter("status");
   			 
   			jsonData = apInvoiceService.findAPInvoiceJSON(startpage, lengthOfpage, fromMonth, toMonth,
   					fromApNo, toApNo, payeeName, invoiceNo, status, orderColumn, orderBy);
//   			
   		} catch (Exception e) {
   			e.printStackTrace();
   			return ResponseUtil.createAjaxError(new ArrayList<>());
   		}
   		return jsonData;
    }
    
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request) {
        //
        String apInvoiceId = request.getParameter("apInvoiceId");
        String apInvoiceAccountDetailId = request.getParameter("apInvoiceAccountDetailId");
        String apInvoiceClassDetailId = request.getParameter("apInvoiceClassDetailId");
        
        //
        ApInvoice apInvoice = apInvoiceService.findApInvoiceByPrimaryKey(new BigInteger(apInvoiceId));
        ApInvoiceAccountDetails apInvoiceAccountDetails = apInvoiceAccountDetailsService.findApInvoiceAccountDetailsByPrimaryKey(new BigInteger(apInvoiceAccountDetailId));
        ApInvoiceClassDetails apinvoiceclassdetails = apInvoiceClassDetailsService.findApInvoiceClassDetailsByPrimaryKey(new BigInteger(apInvoiceClassDetailId));
        
        //
        BigDecimal clsExcludeGstOriginalAmount = apinvoiceclassdetails.getExcludeGstOriginalAmount();
        BigDecimal clsExcludeGstConvertedAmount = apinvoiceclassdetails.getExcludeGstConvertedAmount();
        BigDecimal clsGstConvertedAmount = apinvoiceclassdetails.getGstConvertedAmount();
        BigDecimal clsIncludeGstConvertedAmount = apinvoiceclassdetails.getIncludeGstConvertedAmount();
        
        //UPDATE APINVOICE ACCOUNT DETAIL
        BigDecimal accExcludeGstOriginalAmount =  apInvoiceAccountDetails.getExcludeGstOriginalAmount();
        BigDecimal accExcludeGstConvertedAmount = apInvoiceAccountDetails.getExcludeGstConvertedAmount();
        BigDecimal accGstConvertedAmount = apInvoiceAccountDetails.getGstConvertedAmount();
        BigDecimal accIncludeGstConvertedAmount = apInvoiceAccountDetails.getIncludeGstConvertedAmount();
        
        apInvoiceAccountDetails.setExcludeGstOriginalAmount(accExcludeGstOriginalAmount.subtract(clsExcludeGstOriginalAmount));
        apInvoiceAccountDetails.setExcludeGstConvertedAmount(accExcludeGstConvertedAmount.subtract(clsExcludeGstConvertedAmount));
        apInvoiceAccountDetails.setGstConvertedAmount(accGstConvertedAmount.subtract(clsGstConvertedAmount));
        apInvoiceAccountDetails.setIncludeGstConvertedAmount(accIncludeGstConvertedAmount.subtract(clsIncludeGstConvertedAmount));
        
        apInvoiceAccountDetailsService.saveApInvoiceAccountDetails(apInvoiceAccountDetails);
        
        //UPDATE APINVOICE CLASS DETAIL
        BigDecimal apExcludeGstOriginalAmount =  apInvoice.getExcludeGstOriginalAmount();
        BigDecimal apExcludeGstConvertedAmount = apInvoice.getExcludeGstConvertedAmount();
        BigDecimal apGstConvertedAmount = apInvoice.getGstConvertedAmount();
        BigDecimal apIncludeGstConvertedAmount = apInvoice.getIncludeGstConvertedAmount();
        
        apInvoice.setExcludeGstOriginalAmount(apExcludeGstOriginalAmount.subtract(clsExcludeGstOriginalAmount));
        apInvoice.setExcludeGstConvertedAmount(apExcludeGstConvertedAmount.subtract(clsExcludeGstConvertedAmount));
        apInvoice.setGstConvertedAmount(apGstConvertedAmount.subtract(clsGstConvertedAmount));
        apInvoice.setIncludeGstConvertedAmount(apIncludeGstConvertedAmount.subtract(clsIncludeGstConvertedAmount));
        
        apInvoiceService.saveApInvoice(apInvoice);

        //DELETE
        apInvoiceClassDetailsService.deleteApInvoiceClassDetails(apinvoiceclassdetails);
        
        //Delete ApInvoiceAccountDetails when have not any apinvoiceclassdetails.
        ApInvoiceAccountDetails existingApInvoiceAccountDetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(apInvoiceAccountDetails.getId());
        if (existingApInvoiceAccountDetails != null){
            if(existingApInvoiceAccountDetails.getApInvoiceClassDetailses().isEmpty()){
                apInvoiceAccountDetailsService.deleteApInvoiceAccountDetails(existingApInvoiceAccountDetails);
            }
        }
        
        //
        logger.info("Delete success, AP Invoice Class Detail: {}.", apinvoiceclassdetails.toString());
        return ResponseUtil.ajaxSuccess();
        
    }
    
    @GetMapping(value = "/getListApprovalAvailable")
    @ResponseBody
    public List<Map<String, String>> getApprovalAvailable(@RequestParam String keyword, @RequestParam String bookingDate) {
        List<Map<String, String>> approvals = new ArrayList<Map<String, String>>();
        Map<String,String> approvalMap;
        
        List<ApprovalPurchaseRequest> approvalList = approvalPurchaseRequestDAO.findApprovalAvailable(keyword, bookingDate);
        
        for(ApprovalPurchaseRequest approvalGet : approvalList){
            approvalMap = new LinkedHashMap<String, String>();
            
            String amountAsStr = StringUtils.EMPTY;
            BigDecimal amount = approvalGet.getIncludeGstOriginalAmount();
            if (amount != null) {
                amountAsStr = NumberUtil.format2Currency(amount.doubleValue());
            }
            
            approvalMap.put("approvalNo", approvalGet.getApprovalCode());
            approvalMap.put("amount", approvalGet.getCurrencyCode() + " " + amountAsStr);
            approvalMap.put("approvalDate", CalendarUtil.toString(approvalGet.getApplicationDate()));
            approvalMap.put("applicant", approvalGet.getApplicant());
            approvalMap.put("purpose", approvalGet.getPurpose());
            //
            approvals.add(approvalMap);
        }
        
        return approvals;
    }
    
	@GetMapping(value = {"/copy/{apNo}"})
    public ModelAndView copy(@PathVariable Optional<String> apNo) {
        
	    ModelAndView mav = new ModelAndView("apinvoice/detail");
        
        mav.addObject("currencyMap", currencyMasterHelper.toMap());
        mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
        mav.addObject("gstMap", gstMasterHelper.toMap());
        
        ApInvoiceForm apInvoiceForm = new ApInvoiceForm();

        if (apNo.isPresent()) {
            Iterator<ApInvoice> apInvoiceIter = apInvoiceDAO.findApInvoiceByApInvoiceNo(apNo.get()).iterator();
            
            if(apInvoiceIter.hasNext()){
                Map<String, String> approvalMap = new HashMap<>();

                // Copy from entity to form
                ApInvoice apInvoice = apInvoiceIter.next();
                BeanUtils.copyProperties(apInvoice, apInvoiceForm);
                
                //Set new ApInvoiceNo
                apInvoiceForm.setApInvoiceNo(apInvoiceDAO.getApInvoiceNo());

                // Update detail for ap - invoice
                List<InvoiceDetailForm> invoiceDetailForms = new ArrayList<InvoiceDetailForm>();

                List<InvoiceDetail> invoiceDetails = apInvoiceService.getInvoiceDetail(apInvoice.getId());

                for (InvoiceDetail invoiceDetail : invoiceDetails) {
                    InvoiceDetailForm invoiceDetailForm = new InvoiceDetailForm();
                    BeanUtils.copyProperties(invoiceDetail, invoiceDetailForm);
                    
                    invoiceDetailForm.setApInvoiceAccountDetailId(null);
                    invoiceDetailForm.setApInvoiceClassDetailId(null);
                    invoiceDetailForm.setApprStatusCode(Constant.APR_WAITING);
                    invoiceDetailForm.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());

                    invoiceDetailForms.add(invoiceDetailForm);

                    // build map approval code - amount
                    String apCode = invoiceDetail.getApprovalCode();
                    Iterator<ApprovalPurchaseRequest> approvalIter = approvalPurchaseRequestDAO.findApprovalPurchaseRequestByApprovalCode(apCode).iterator();

                    if (approvalIter.hasNext()) {
                        ApprovalPurchaseRequest approval = approvalIter.next();
                        approvalMap.put(apCode, ObjectUtils.toString(approval.getIncludeGstOriginalAmount()));
                    }
                }

                // add approval object(as JSON object) to model.
                mav.addObject("approvalJson", new Gson().toJson(approvalMap));

                apInvoiceForm.setDetailForms(invoiceDetailForms);

                // get status to show in UI
                Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_API);
                apInvoiceForm.setStatusVal(mapStatus.get(apInvoiceForm.getStatus()));
                
            }

        } else {
            apInvoiceForm.setCreatedDate(Calendar.getInstance());
            apInvoiceForm.setApInvoiceNo(apInvoiceDAO.getApInvoiceNo());
        }

        mav.addObject("listAccountMaster", accountMasterDAO.findAllAccountMasters());
        mav.addObject("listGstMaster", gstMasterDAO.findAllGstMasters());
        
        //Ignore ID, status
        apInvoiceForm.setId(null);
        apInvoiceForm.setStatus("");
        
        mav.addObject("apInvoiceForm", apInvoiceForm);
        
        mav.addObject("apr002", systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING));
        
        return mav;
    }
    

    @Override
    public ImportComponent getImportComponent() {
        return importComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "API").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return exportComponent;
    }
}