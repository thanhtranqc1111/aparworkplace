package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.ProjectPaymentExportComponent;
import com.ttv.cashflow.dto.ProjectPaymentDTO;
import com.ttv.cashflow.service.ProjectPayrollDetailsService;
import com.ttv.cashflow.util.CalendarUtil;


/**
 * @author Binh.lv
 * created date Apr 27, 2018
 */
@Controller
@RequestMapping("/projectPayment")
@PrototypeScope
public class ProjectPaymentController extends ExchangeController {
    
    @Autowired
    private ProjectPayrollDetailsService projectPayrollDetailsService;
    
    @Autowired
    private ProjectPaymentExportComponent exportComponent;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectPaymentController.class);
    
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(required=false) String monthFrom, 
                              @RequestParam(required=false) String monthTo,
                              HttpServletRequest request) {
 
    	//initialize
        ModelAndView projectPaymentView = new ModelAndView("project-payment/detail");
        Calendar calMonthFrom = CalendarUtil.getCalendar(monthFrom, "MM/yyyy");
        Calendar calMonthTo = CalendarUtil.getCalendar(monthTo, "MM/yyyy");
        
        if(calMonthFrom == null) {
			LOGGER.info("monthFrom is wrong format MM/yyyy");
        	return new ModelAndView("redirect:/error");
        }
        
        if(calMonthTo == null) {
			LOGGER.info("monthTo is wrong format MM/yyyy");
        	return new ModelAndView("redirect:/error");
        }

        // error 404 when monthFrom > monthTo
        if(calMonthFrom.compareTo(calMonthTo) > 0) {
        	LOGGER.info("monthFrom must be early than monthTo");
            return new ModelAndView("redirect:/error");
        }

        // update data to project_payroll_details
        String payrollDetailIds = "";
        try {
        	projectPayrollDetailsService.saveDataProjectPayroll(calMonthFrom, calMonthTo);
        	LOGGER.info("Save list project payroll suscess}.", payrollDetailIds);
        } catch (Exception e) {
        	LOGGER.info("Save list project allocation error: {}.", e);
        }

        // get List getProject Payment Details
        List<ProjectPaymentDTO> projectPaymentLst = new ArrayList<ProjectPaymentDTO>();
        projectPaymentLst = projectPayrollDetailsService.getProjectPaymentDTOLst(calMonthFrom, calMonthTo);

        projectPaymentView.addObject("projectPaymentLst", projectPaymentLst);
        return projectPaymentView;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PP").getValue();
    }

    @Override
    public ImportComponent getImportComponent() {
        return null;
    }

    @Override
    public ExportComponent getExportComponent() {
        return exportComponent;
    }
}
