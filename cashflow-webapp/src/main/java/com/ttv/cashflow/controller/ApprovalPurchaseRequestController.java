package com.ttv.cashflow.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.google.gson.Gson;
import com.ttv.cashflow.domain.ApprovalTypePurchaseMaster;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApprovalPurchaseRequestExportComponent;
import com.ttv.cashflow.component.ApprovalPurchaseRequestImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.form.ApprovalPurchaseRequestForm;
import com.ttv.cashflow.service.ApInvoiceClassDetailsService;
import com.ttv.cashflow.service.ApprovalPurchaseRequestService;
import com.ttv.cashflow.service.ApprovalTypePurchaseMasterService;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Jan 12, 2018
 */
@Controller
@RequestMapping("/approvalPurchaseRequest")
@PrototypeScope
public class ApprovalPurchaseRequestController extends ExchangeController{
	@Autowired
	private ApprovalPurchaseRequestService approvalService;
	@Autowired
	private ApprovalTypePurchaseMasterService approvalTypeMasterService;
	@Autowired
	private ApInvoiceClassDetailsService apInvoiceClassDetailsService;
	@Autowired
	private ApprovalPurchaseRequestImportComponent approvalImportComponent;
	@Autowired
	private ApprovalPurchaseRequestExportComponent approvalExportComponent;
	@Autowired
	private CurrencyMasterService currencyMasterService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalPurchaseRequestController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView model = new ModelAndView("approval/purchase-request");
		Map<String, String> mapStatus = systemValueHelper.toMap(Constant.APPROVAL_STT_TYPE);
		model.addObject("approvalTypeMaster", approvalTypeMasterService.findAllApprovalTypePurchaseMasters(-1, -1));
		model.addObject("currencyMaster", currencyMasterService.findAllCurrencyMasters(-1, -1));
		model.addObject("mapStatus", mapStatus);
		return model;
	}

	@RequestMapping(value = "/api/findAllApprovalTypePurchaseMasters", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findAllApprovalTypePurchaseMasters() {
		Gson gson = new Gson();
		List<ApprovalTypePurchaseMaster> approvalTypePurchaseMasters = approvalTypeMasterService.findAllApprovalTypePurchaseMasters(-1, -1);

		return gson.toJson(approvalTypePurchaseMasters);
	}

	@RequestMapping(value = "/api/getAllPagingApproval", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingApproval(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			String approvalType = request.getParameter("approvalType");
			String approvalCode = request.getParameter("approvalCode");
			String validityFrom = request.getParameter("validityFrom");
			String validityTo = request.getParameter("validityTo");
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            Calendar calendarValidityFrom = CalendarUtil.getCalendar(validityFrom, "dd/MM/yyyy");
			Calendar calendarValidityTo = CalendarUtil.getCalendar(validityTo, "dd/MM/yyyy");
			String status = request.getParameter("status").trim();
			jsonData = approvalService.findApprovalJson(startpage, lengthOfpage, approvalType, approvalCode,
															calendarValidityFrom, calendarValidityTo, status, orderColumn, orderBy);
		} catch (Exception e) {
			LOGGER.info("getAllPagingApproval error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("approvalForm") @Valid ApprovalPurchaseRequestForm approvalForm, BindingResult result, Model model) {
		try {
			if(result.hasErrors()) {
				return ResponseUtil.createAjaxError(result.getAllErrors());
			}
			else {
				if(approvalForm.getValidityFrom().compareTo(approvalForm.getValidityTo()) >= 0)
				{
					List<ObjectError> list = new ArrayList<>();
					ObjectError objectError = new ObjectError("Validity Date Error", "Validity Date To must greater than Validity Date From");
					list.add(objectError);
					return ResponseUtil.createAjaxError(list);
				}
				else if(approvalForm.getId().compareTo(BigInteger.ZERO) == 0 && !approvalService.findApprovalByApprovalCode(approvalForm.getApprovalCode()).isEmpty()) {
					List<ObjectError> list = new ArrayList<>();
					ObjectError objectError = new ObjectError("Approval No Exists", "Your Approval No is already exists.");
					list.add(objectError);
					return ResponseUtil.createAjaxError(list);
				}
				else 
				{
					String mode = request.getParameter("mode");
					ApprovalPurchaseRequest approval = new ApprovalPurchaseRequest();
					BeanUtils.copyProperties(approvalForm, approval);
					if (Constant.MODE.NEW.getValue().equals(mode)) {
						ApprovalPurchaseRequest existApproval = approvalService.findApprovalPurchaseRequestByPrimaryKey(approval.getId());
						if (existApproval != null)
						{
							return ResponseUtil.createAjaxSuccess(0);
						}
					}
					approvalService.saveApprovalPurchaseRequest(approval);
					return ResponseUtil.createAjaxSuccess(0);
				}
			}
		} catch (Exception e) {
			LOGGER.info("save ApprovalPurchaseRequest error: {}.", e);
			return ResponseUtil.createAjaxError(0);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
	public String delete(final HttpServletRequest request, Model model) {
		try {
			String[] codes = request.getParameter("id").split(",");
			BigInteger[] arrIds = new BigInteger[codes.length];
			for (int i = 0; i < codes.length; i++) {
				arrIds[i] = new BigInteger(codes[i]);
			}
			approvalService.deleteApprovalByIds(arrIds);
			LOGGER.info("Delete suscess, Approval: {}.", arrIds.toString());
		} catch (Exception e) {
			LOGGER.info("save ApprovalPurchaseRequest error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return "redirect:/approvalPurchaseRequest/list";
	}
	
	@RequestMapping(value = "/api/findByApprovalCodeNotApproved", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findByApprovalCodeNotApproved(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String approvalCode = request.getParameter("approvalCode");
			jsonData = apInvoiceClassDetailsService.findByApprovalCodeNotApprovedJSON(approvalCode);
		} catch (Exception e) {
			LOGGER.info("findByApprovalCodeNotApproved error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/api/findDetailByApprovalCode", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findDetailByApprovalCode(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String approvalCode = request.getParameter("approvalCode");
			jsonData = apInvoiceClassDetailsService.findDetailByApprovalCodeJSON(approvalCode);
		} catch (Exception e) {
			LOGGER.info("findDetailByApprovalCode error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@Override
	public ImportComponent getImportComponent() {
		return approvalImportComponent;
	}
	
	@Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "AIP").getValue();
    }
	
	@Override
    public ExportComponent getExportComponent() {
        return approvalExportComponent;
    }
}
