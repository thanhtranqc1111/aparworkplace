package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.PayeeTypeImportComponent;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.service.PayeePayerTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;


/**
 * @author thoai.nh
 * created date Oct 3, 2017
 */
@Controller()
@RequestMapping("/admin/payeeTypeMaster")
@PrototypeScope
public class PayeeTypeMasterController extends ExchangeController {
    
    private static final String PAYEE_TYPE_MASTER_VIEW = "/list";
    
    @Autowired
    PayeePayerTypeMasterService payeeTypeMasterService;
    
    @Autowired
    private PayeeTypeImportComponent payeeTypeImportComponent;
    
    private static final Logger logger = LoggerFactory.getLogger(PayeeTypeMasterController.class);
   
    @RequestMapping(value = PAYEE_TYPE_MASTER_VIEW, method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeTypeMasterView = new ModelAndView("master/payee_type");
       payeeTypeMasterView.addObject("payeeTypeMasterForm", new PayeePayerTypeMaster());
       payeeTypeMasterView.addObject("adminPage", "payeeTypeMaster");
       return payeeTypeMasterView;
    }
    
    @RequestMapping(value = "/api/getAllPagingPayeeType", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingPayeeType(final HttpServletRequest request, Model model) {
        String jPayeeTypeMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jPayeeTypeMaster = payeeTypeMasterService.findPayeeTypeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jPayeeTypeMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String savePayeeTypeMaster(HttpServletRequest request, @ModelAttribute("payeeTypeMasterForm") @Valid PayeePayerTypeMaster payeeType,
            BindingResult result, Model model) {
            try {
                String mode = request.getParameter("mode");
                if (Constant.MODE.NEW.getValue().equals(mode)) {
                    
                    PayeePayerTypeMaster existPayeeType  = payeeTypeMasterService.findPayeePayerTypeMasterByPrimaryKey(payeeType.getCode());
                    if (existPayeeType != null) {
                        return "redirect:/admin/projectMaster/list";
                    } 
                } 
                payeeTypeMasterService.savePayeePayerTypeMaster(payeeType);
                logger.info("Save suscess, Payee Payer Type Master: {}.", payeeType.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/payeeTypeMaster/list";
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deletePayeeTypeMaster(final HttpServletRequest request, Model model) {
            try {
                String[] ids = (String[])request.getParameterValues("id");
                payeeTypeMasterService.deletePayeePayerTypeMasterByCodes(ids);
                logger.info("Delete suscess, Payee Payer Type Master: {}.", ids.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/payeeTypeMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return payeeTypeImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PTM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}