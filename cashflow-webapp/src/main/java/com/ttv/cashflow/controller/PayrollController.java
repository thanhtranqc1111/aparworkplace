
package com.ttv.cashflow.controller;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.PayrollExportComponent;
import com.ttv.cashflow.component.PayrollImportComponent;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.dto.PayrollDTO;
import com.ttv.cashflow.dto.PayrollDetailsDTO;
import com.ttv.cashflow.helper.ClaimTypeMasterDataHelper;
import com.ttv.cashflow.helper.CurrencyMasterDataHelper;
import com.ttv.cashflow.service.ApprovalPurchaseRequestService;
import com.ttv.cashflow.service.EmployeeMasterService;
import com.ttv.cashflow.service.PayrollService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.PermissionCode;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh created date Mar 14, 2018
 */
@Controller
@RequestMapping("/payroll")
@PrototypeScope
public class PayrollController extends ExchangeController {
	@Autowired
	private ClaimTypeMasterDataHelper claimTypeMasterHelper;
	@Autowired
	private PayrollService payrollService;
	@Autowired
	private EmployeeMasterService employeeMasterService;
	@Autowired
	private PayrollImportComponent payrollImportComponent;
	@Autowired
	private PayrollExportComponent payrollExportComponent;
	@Autowired
	private ApprovalPurchaseRequestService approvalPurchaseRequestService;
	@Autowired
	private CurrencyMasterDataHelper currencyMasterHelper;
	private static final Logger LOGGER = LoggerFactory.getLogger(PayrollController.class);

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public ModelAndView mapList(HttpServletRequest request) {
		Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_API);
		ModelAndView model = new ModelAndView("payroll/list");
		model.addObject("mapStatus", mapStatus);
		return model;
	}

	@GetMapping(value = { "/detail", "/detail/{payrollNo}" })
	public ModelAndView mapDetail(@PathVariable Optional<String> payrollNo, HttpServletRequest httpServletRequest) {
		Payroll payroll = null;
		if (payrollNo.isPresent()) {
			payroll = payrollService.findPayrollByPayrollNo(payrollNo.get());
			if (payroll == null) {
				return new ModelAndView("redirect:/");
			}
		} else {
			payroll = new Payroll();
			payroll.setBookingDate(Calendar.getInstance());
			payroll.setMonth(Calendar.getInstance());
			payroll.setPayrollNo(payrollService.getPayrollNo());
			payroll.setStatus(Constant.API_NOT_PAID);
			payroll.setFxRate(new BigDecimal("1"));
			payroll.setClaimType(Constant.CLAIM_TYPE_PAYROLL);
			payroll.setId(BigInteger.ZERO);
			Subsidiary crSubsidiary = (Subsidiary) httpServletRequest.getSession().getAttribute(Constant.SESSSION_NAME_CURRENT_SUBSIDIARY);
			if(crSubsidiary != null) {
				payroll.setOriginalCurrencyCode(crSubsidiary.getCurrency());
			}
		}
		PayrollDTO payrollDTO = new PayrollDTO();
		payrollService.copyDomainToDTO(payroll, payrollDTO);
		setSystemValueForPayrollDTO(payrollDTO);
		return prepareModelForPayrollDetails(payrollDTO);
	}

	@GetMapping(value = { "/copy/{payrollNo}" })
	public ModelAndView mapCopy(@PathVariable String payrollNo) {
		Payroll payroll = payrollService.findPayrollByPayrollNo(payrollNo);
		if (payroll == null) {
			return new ModelAndView("redirect:/");
		} else {
			PayrollDTO payrollDTO = payrollService.clone(payroll);
			setSystemValueForPayrollDTO(payrollDTO);
			return prepareModelForPayrollDetails(payrollDTO);
		}
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public String save(@ModelAttribute("data") String data, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(data).getAsJsonObject();
			PayrollDTO payrollDTO = payrollService.parsePayrollJSON(jsonObject);
			rs = payrollService.savePayroll(payrollDTO, false).intValue();
		} catch (Exception e) {
			LOGGER.info("Save payroll error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		LOGGER.info("Save payroll suscess. {}", data);
		return ResponseUtil.createAjaxSuccess(rs);
	}

	@RequestMapping(value = "/api/getAllPagingPayroll", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingPayroll(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
			String orderBy = request.getParameter("order[0][dir]");
			Integer status = NumberUtils.toInt(request.getParameter("status"));
			String fromPayrollNo = request.getParameter("fromPayrollNo");
			String toPayrollNo = request.getParameter("toPayrollNo");
			Calendar calendarMonthFrom = CalendarUtil.getCalendar(request.getParameter("fromMonth"), "MM/yyyy");
			Calendar calendarMonthTo = CalendarUtil.getCalendar(request.getParameter("toMonth"), "MM/yyyy");
			;
			jsonData = payrollService.findPayrollPaging(startpage, lengthOfpage, calendarMonthFrom, calendarMonthTo,
					fromPayrollNo, toPayrollNo, orderColumn, orderBy, status);
		} catch (Exception e) {
			LOGGER.info("getAllPagingPayroll error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}

	private void setSystemValueForPayrollDTO(PayrollDTO payrollDTO) {
		/*
		 * load value for status code
		 */
		for (PayrollDetailsDTO payrollDetailsDTO : payrollDTO.getPayrollDetailses()) {
			// load approval status
			if (payrollDetailsDTO.getIsApproval()) {
				payrollDetailsDTO.setApprStatusCode(Constant.APR_APPROVAL);
				payrollDetailsDTO.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_APPROVAL).getValue());
				// load approval amount
				Set<ApprovalPurchaseRequest> setApproval = approvalPurchaseRequestService.findApprovalByApprovalCode(payrollDetailsDTO.getApprovalCode());
				if (!setApproval.isEmpty()) {
					ApprovalPurchaseRequest approval = setApproval.iterator().next();
					payrollDetailsDTO.setApprovalIncludeGstOriginalAmount(approval.getIncludeGstOriginalAmount());
				}
			} else {
				payrollDetailsDTO.setApprStatusCode(Constant.APR_WAITING);
				payrollDetailsDTO.setApprStatusVal(systemValueHelper.get(Constant.PREFIX_APR, Constant.APR_WAITING).getValue());
			}
			// load name
			if (!StringUtils.isEmpty(payrollDetailsDTO.getEmployeeCode())) {
				EmployeeMaster emp = employeeMasterService.findEmployeeMasterByCode(payrollDetailsDTO.getEmployeeCode());
				if (emp != null) {
					payrollDetailsDTO.setName(emp.getName());
				}
			}
		}
		Map<String, String> mapStatus = systemValueHelper.toMap(Constant.PREFIX_PR);
		payrollDTO.setStatusVal(mapStatus.get(payrollDTO.getStatus()));
	}
	
	private ModelAndView prepareModelForPayrollDetails(PayrollDTO payrollDTO) {
		ModelAndView mav = new ModelAndView("payroll/detail");
		mav.addObject("claimTypeMap", claimTypeMasterHelper.toMap());
		mav.addObject("currencyMap", currencyMasterHelper.toMap());
		mav.addObject("payroll", payrollDTO);
		mav.addObject("mapApprovalStatus", systemValueHelper.toMap(Constant.PREFIX_APR));
		mav.addObject("mapPayrollStatus", systemValueHelper.toMap(Constant.PREFIX_PR));
		return mav;
	}
	
	@Override
	public String getRelatePath(String type) {
		return systemValueHelper.get(type, "PR").getValue();
	}

	@Override
	public ImportComponent getImportComponent() {
		return payrollImportComponent;
	}

	@Override
	public ExportComponent getExportComponent() {
		return payrollExportComponent;
	}
	
    protected String getExportDir() {
        String realDir = servletContext.getRealPath("/") + getRelatePath(Type.EXPORT);

        if (AuthorityUtils.authorityListToSet(
                    SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_009.toString())) {
            realDir = realDir + SEPARATOR + "ADMIN";
            
        }

        File dir = new File(realDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return realDir;
    }

}