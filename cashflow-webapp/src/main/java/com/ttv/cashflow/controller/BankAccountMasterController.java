package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.BankAccountMasterImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.service.BankAccountMasterService;
import com.ttv.cashflow.service.BankMasterService;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 09, 2017
 */
@Controller()
@RequestMapping("/admin/bankAccountMaster")
@PrototypeScope
public class BankAccountMasterController extends ExchangeController{
	@Autowired
	private BankAccountMasterService bankAccountMasterService;
	
	@Autowired
	private BankMasterService bankMasterService;
	
	@Autowired
    private BankAccountMasterImportComponent bankAccountMasterImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(BankAccountMasterController.class);
	
	@Autowired
	private CurrencyMasterService currencyMasterService;
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/bank_account");
       payeeMasterView.addObject("bankAccountMasterForm", new BankAccountMaster());
       payeeMasterView.addObject("adminPage", "bankAccountMaster");
       payeeMasterView.addObject("bankMasters", parseBankMasterList());
       payeeMasterView.addObject("currencyMasters", parseCurrencyMasterList());
       return payeeMasterView;
    }
	
	@RequestMapping(value = "/api/getAllPagingBankAccount", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingBankAccount(final HttpServletRequest request, Model model) {
    	String jBankAccountMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jBankAccountMaster = bankAccountMasterService.findBankAccountMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
        	e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jBankAccountMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBankAccountMaster(HttpServletRequest request, @ModelAttribute("BankAccountMasterForm") @Valid BankAccountMaster bankAccountMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 BankAccountMaster existBankAccount  = bankAccountMasterService.findBankAccountMasterByPrimaryKey(bankAccountMaster.getCode());
                     if (existBankAccount != null)
                         return "redirect:/admin/bankAccountMaster/list"; 
                 } 
                 bankAccountMasterService.saveBankAccountMaster(bankAccountMaster);
                 logger.info("Save suscess, Bank Account Master: {}.", bankAccountMaster.toString());
                 
                return "redirect:/admin/bankAccountMaster/list";
            } catch (Exception e) {
            	e.printStackTrace();
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteBankAccountMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                bankAccountMasterService.deleteBankAccountMasterByCodes(codes);
                logger.info("Delete suscess, Bank Account Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/bankAccountMaster/list"; 
    }
    
    private Map<String, String> parseBankMasterList(){
    	List<BankMaster> list = bankMasterService.findAllBankMasters(-1, -1);
    	 Map<String, String> map = new LinkedHashMap<String, String>();
         for (BankMaster bankMaster : list) {
             map.put(String.valueOf(bankMaster.getCode()), bankMaster.getName());
         }
    	return map;
    }

    private Map<String, String> parseCurrencyMasterList(){
    	 List<CurrencyMaster> list = currencyMasterService.findAllCurrencyMasters(-1, -1);
    	 Map<String, String> map = new LinkedHashMap<String, String>();
         for (CurrencyMaster currencyMaster : list) {
             map.put(String.valueOf(currencyMaster.getCode()), currencyMaster.getName());
         }
    	return map;
    }

    @Override
    public ImportComponent getImportComponent(){
    	return bankAccountMasterImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "BAM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
