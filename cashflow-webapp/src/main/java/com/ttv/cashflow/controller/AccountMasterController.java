package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.AccountImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.service.AccountMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 06, 2017
 */
@Controller()
@RequestMapping("/admin/accountMaster")
@PrototypeScope
public class AccountMasterController extends ExchangeController {
    
    @Autowired
	private AccountMasterService accountMasterService;
	
	@Autowired
    private AccountImportComponent accountImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(AccountMasterController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView accountMasterView = new ModelAndView("master/account");
       accountMasterView.addObject("accountMasterForm", new AccountMaster());
       accountMasterView.addObject("adminPage", "accountMaster");
       return accountMasterView;
    }
	
	@RequestMapping(value = "/api/getAllPagingAccount", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingAccount(final HttpServletRequest request, Model model) {
    	String jAccountMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jAccountMaster = accountMasterService.findAccountMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jAccountMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveAccountMaster(HttpServletRequest request, @ModelAttribute("AccountMasterForm") @Valid AccountMaster accountMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 AccountMaster existAccount  = accountMasterService.findAccountMasterByPrimaryKey(accountMaster.getCode());
                     if (existAccount != null)
                     {
                    	 return "redirect:/admin/accountMaster/list"; 
                     }
                 } 
                 accountMasterService.saveAccountMaster(accountMaster);
                 logger.info("Save suscess, Account Master: {}.", accountMaster.toString());
                 
                 return "redirect:/admin/accountMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteAccountMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                accountMasterService.deleteAccountMasterByCodes(codes);
                logger.info("Delete suscess, Account Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/accountMaster/list"; 
    }

    @Override
    public ImportComponent getImportComponent() {
        return accountImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "AM").getValue();
    }

    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
