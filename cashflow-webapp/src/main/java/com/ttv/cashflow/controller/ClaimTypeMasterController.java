package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ClaimMasterImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.service.ClaimTypeMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 06, 2017
 */
@Controller()
@RequestMapping("/admin/claimTypeMaster")
@PrototypeScope
public class ClaimTypeMasterController extends ExchangeController{
	@Autowired
	private ClaimTypeMasterService claimTypeMasterService;
	
	@Autowired
	private ClaimMasterImportComponent claimMasterImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(ClaimTypeMasterController.class);
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/claim_type");
       payeeMasterView.addObject("claimTypeMasterForm", new ClaimTypeMaster());
       payeeMasterView.addObject("adminPage", "claimTypeMaster");
       return payeeMasterView;
    }
	
	@RequestMapping(value = "/api/getAllPagingClaimType", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingClaimType(final HttpServletRequest request, Model model) {
    	String jClaimTypeMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jClaimTypeMaster = claimTypeMasterService.findClaimTypeMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jClaimTypeMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveClaimTypeMaster(HttpServletRequest request, @ModelAttribute("ClaimTypeMasterForm") @Valid ClaimTypeMaster ClaimTypeMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 ClaimTypeMaster existClaimType  = claimTypeMasterService.findClaimTypeMasterByPrimaryKey(ClaimTypeMaster.getCode());
                     if (existClaimType != null)
                         return "redirect:/admin/claimTypeMaster/list"; 
                 } 
                 claimTypeMasterService.saveClaimTypeMaster(ClaimTypeMaster);
                 logger.info("Save suscess, Claim Type Master: {}.", ClaimTypeMaster.toString());
                 
                return "redirect:/admin/claimTypeMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteClaimTypeMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                claimTypeMasterService.deleteClaimTypeMasterByCodes(codes);
                logger.info("Delete suscess, Claim Type Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/claimTypeMaster/list"; 
    }

    @Override
    public ImportComponent getImportComponent(){
    	return claimMasterImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "CM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
