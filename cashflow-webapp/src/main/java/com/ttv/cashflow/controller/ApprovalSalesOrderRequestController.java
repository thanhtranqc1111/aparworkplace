package com.ttv.cashflow.controller;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ApprovalSalesOrderRequestExportComponent;
import com.ttv.cashflow.component.ApprovalSalesOrderRequestImportComponent;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;
import com.ttv.cashflow.form.ApprovalSalesOrderRequestForm;
import com.ttv.cashflow.helper.SystemValueHelper;
import com.ttv.cashflow.service.ApInvoiceClassDetailsService;
import com.ttv.cashflow.service.ApprovalSalesOrderRequestService;
import com.ttv.cashflow.service.ApprovalTypeSalesMasterService;
import com.ttv.cashflow.service.CurrencyMasterService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Jan 12, 2018
 */
@Controller
@RequestMapping("/approvalSalesOrderRequest")
@PrototypeScope
public class ApprovalSalesOrderRequestController extends ExchangeController{
	@Autowired
	private ApprovalSalesOrderRequestService approvalService;
	@Autowired
	private ApprovalTypeSalesMasterService approvalTypeMasterService;
	@Autowired
	private ApInvoiceClassDetailsService apInvoiceClassDetailsService;
	@Autowired
	private SystemValueHelper systemValueHelper;
	@Autowired
	private ApprovalSalesOrderRequestImportComponent approvalImportComponent;
	@Autowired
	private ApprovalSalesOrderRequestExportComponent approvalExportComponent;
	@Autowired	
	private CurrencyMasterService currencyMasterService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalSalesOrderRequestController.class);
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView model = new ModelAndView("approval/sales-order-request");
		model.addObject("approvalTypeMaster", approvalTypeMasterService.findAllApprovalTypeSalesMasters(-1, -1));
		model.addObject("currencyMaster", currencyMasterService.findAllCurrencyMasters(-1, -1));
		return model;
	}

	@RequestMapping(value = "/api/getAllPagingApproval", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingApprovalType(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			Integer startpage = NumberUtils.toInt(request.getParameter("start"));
			Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
			String approvalType = request.getParameter("approvalType");
			String approvalCode = request.getParameter("approvalCode");
			String validityFrom = request.getParameter("validityFrom");
			String validityTo = request.getParameter("validityTo");
			Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            Calendar calendarValidityFrom = CalendarUtil.getCalendar(validityFrom, "dd/MM/yyyy");
			Calendar calendarValidityTo = CalendarUtil.getCalendar(validityTo, "dd/MM/yyyy");
			jsonData = approvalService.findApprovalJson(startpage, lengthOfpage, approvalType, approvalCode,
														calendarValidityFrom, calendarValidityTo, orderColumn, orderBy);
		} catch (Exception e) {
			LOGGER.info("getAllPagingApproval error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("approvalForm") @Valid ApprovalSalesOrderRequestForm approvalForm, BindingResult result, Model model) {
		try {
			if(result.hasErrors()) {
				return ResponseUtil.createAjaxError(result.getAllErrors());
			}
			else {
				if(approvalForm.getValidityFrom().compareTo(approvalForm.getValidityTo()) >= 0)
				{
					List<ObjectError> list = new ArrayList<ObjectError>();
					ObjectError objectError = new ObjectError("Validity Date Error", "Validity Date To must greater than Validity Date From");
					list.add(objectError);
					return ResponseUtil.createAjaxError(list);
				}
				else if(approvalForm.getId().compareTo(BigInteger.ZERO) == 0 && !approvalService.findApprovalByApprovalCode(approvalForm.getApprovalCode()).isEmpty()) {
					List<ObjectError> list = new ArrayList<>();
					ObjectError objectError = new ObjectError("Approval No Exists", "Your Approval No is already exists.");
					list.add(objectError);
					return ResponseUtil.createAjaxError(list);
				}
				else 
				{
					String mode = request.getParameter("mode");
					ApprovalSalesOrderRequest approval = new ApprovalSalesOrderRequest();
					BeanUtils.copyProperties(approvalForm, approval);
					if (Constant.MODE.NEW.getValue().equals(mode)) {
						ApprovalSalesOrderRequest existApproval = approvalService.findApprovalSalesOrderRequestByPrimaryKey(approval.getId());
						if (existApproval != null)
						{
							return ResponseUtil.createAjaxSuccess(0);
						}
					}
					approvalService.saveApprovalSalesOrderRequest(approval);
					LOGGER.info("Save suscess, Approval Sales Order Request: {}.", approval.toString());
					
					return ResponseUtil.createAjaxSuccess(0);
				}
			}
		} catch (Exception e) {
			LOGGER.info("save ApprovalPurchaseRequest error: {}.", e);
			return ResponseUtil.createAjaxError(0);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
	public String delete(final HttpServletRequest request, Model model) {
		try {
			String[] codes = request.getParameter("id").split(",");
			BigInteger[] arrIds = new BigInteger[codes.length];
			for (int i = 0; i < codes.length; i++) {
				arrIds[i] = new BigInteger(codes[i]);
			}
			approvalService.deleteApprovalByIds(arrIds);
			LOGGER.info("Delete suscess, Approval: {}.", arrIds.toString());

		} catch (Exception e) {
			LOGGER.info("save ApprovalSaleOrder error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return "redirect:/approvalSalesOrderRequest/list";
	}
	
	@RequestMapping(value = "/api/findByApprovalCodeNotApproved", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findByApprovalCodeNotApproved(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String approvalCode = request.getParameter("approvalCode");
			jsonData = apInvoiceClassDetailsService.findByApprovalCodeNotApprovedJSON(approvalCode);
		} catch (Exception e) {
			LOGGER.info("findByApprovalCodeNotApproved error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/api/findDetailByApprovalCode", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String findDetailByApprovalCode(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String approvalCode = request.getParameter("approvalCode");
			jsonData = apInvoiceClassDetailsService.findDetailByApprovalCodeJSON(approvalCode);
		} catch (Exception e) {
			LOGGER.info("findDetailByApprovalCode error: {}.", e);
			return ResponseUtil.createAjaxError(new ArrayList<>());
		}
		return jsonData;
	}
	
    @GetMapping(value = "/api/getApprovalSalesOrderRequestMaps")
    @ResponseBody
    public List<Map<String, String>> getApprovalSalesOrderRequestMaps(@RequestParam String keyword) {
        // Create a list of for
        List<Map<String, String>> approvalSalesOrderRequestMaps = new ArrayList<>();
        List<ApprovalSalesOrderRequest> approvalSalesOrderRequests = approvalService.findApproval(keyword);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // Build data
        for (ApprovalSalesOrderRequest approvalSalesOrderRequest : approvalSalesOrderRequests) {
            Map<String, String> approvalSalesOrderRequestMap = new LinkedHashMap<>();
            approvalSalesOrderRequestMap.put("approvalNo", approvalSalesOrderRequest.getApprovalCode());
            approvalSalesOrderRequestMap.put("approvaType", approvalSalesOrderRequest.getApprovalType());
            if(approvalSalesOrderRequest.getApplicationDate() != null)
            {
            	approvalSalesOrderRequestMap.put("approvedDate", formatter.format(approvalSalesOrderRequest.getApplicationDate().getTime()));
            }
            else
            {
            	approvalSalesOrderRequestMap.put("approvedDate", "");
            }
            approvalSalesOrderRequestMap.put("applicant", approvalSalesOrderRequest.getApplicant());
            approvalSalesOrderRequestMap.put("purpose", approvalSalesOrderRequest.getPurpose());
            approvalSalesOrderRequestMaps.add(approvalSalesOrderRequestMap);
        }

        return approvalSalesOrderRequestMaps;
    }

	@Override
	public ImportComponent getImportComponent() {
		return approvalImportComponent;
	}
	
	@Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "AIS").getValue();
    }
	
	@Override
    public ExportComponent getExportComponent() {
        return approvalExportComponent;
    }
}
