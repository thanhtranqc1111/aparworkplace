package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.form.SubsidiaryForm;
import com.ttv.cashflow.helper.TenantHelper;
import com.ttv.cashflow.service.SubsidiaryService;
import com.ttv.cashflow.service.SubsidiaryUserDetailService;
import com.ttv.cashflow.service.UserAccountService;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author tien.tm
 * created date Oct 31, 2017
 */
@Controller()
@RequestMapping("/subsidiary")
@PrototypeScope
public class SubsidiaryController {
	@Autowired
	private SubsidiaryService subsidiaryService;
	
	@Autowired
	private SubsidiaryDAO subsidiaryDAO;
	
	@Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private SubsidiaryUserDetailService subsidiaryUserDetailService;
    
    @Autowired
	TenantHelper tenantHelper;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
	    ModelAndView model = new ModelAndView("subsidiary/list");
        return model;
    }
	
	@RequestMapping(value = "/api/getAllSubsidiary", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllSubsidiary(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try { 
			String keyword = StringUtils.stripToEmpty(request.getParameter("keyword"));
			jsonData = subsidiaryService.findSubsidiaryJson(keyword);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createAjaxError(null);
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
	public String delete(final HttpServletRequest request, Model model) {
		try {
			String[] codes = request.getParameter("id").split(",");
			Integer[] arrIds = new Integer[codes.length];
			for (int i = 0; i < codes.length; i++) {
				arrIds[i] = new Integer(codes[i]);
			}
			subsidiaryService.deleteSubsidiaryByIds(arrIds);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createAjaxError(null);
		}
		return "redirect:/subsidiary/list";
	}
	
	@RequestMapping(value = {"/detail", "/detail/{id}"}, method = RequestMethod.GET)
    public ModelAndView subsidiary(@PathVariable Optional<Integer> id) {
	    ModelAndView model = new ModelAndView("subsidiary/detail");
	    SubsidiaryForm subsidiaryForm = new SubsidiaryForm();
	    
	    if(id.isPresent()) {
	    	Subsidiary subsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(id.get());
	    	 BeanUtils.copyProperties(subsidiary, subsidiaryForm);
	    }	    
	    
	    model.addObject("subsidiaryForm", subsidiaryForm);
        return model;
    }
	
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("subsidiaryForm") SubsidiaryForm subsidiaryForm, HttpServletRequest request, RedirectAttributes redirectAttributes){
		Subsidiary subsidiary = new Subsidiary();
		BeanUtils.copyProperties(subsidiaryForm, subsidiary);
		
		// set new modified date 
		subsidiary.setModifiedDate(Calendar.getInstance());
		if(null == subsidiary.getId()){
			// check subsidiary code is exist
			String subCode = subsidiary.getCode();
			if(null != subCode){
				Set<Subsidiary> existingSubsidiary = subsidiaryDAO.findSubsidiaryByCode(subCode);
				if (existingSubsidiary.size() > 0) { // do not create new sub w/i code is exist
					redirectAttributes.addFlashAttribute("message", "Subsidiary ["+subsidiary.getCode()+"] Code is exist.");
					return "redirect:/subsidiary/detail";
				}
			}
			subsidiary.setCreatedDate(Calendar.getInstance());
		}
		
		subsidiaryService.saveSubsidiary(subsidiary);
		redirectAttributes.addFlashAttribute("message", "Subsidiary ["+subsidiary.getCode()+"] Code saved successfully.");

		if(null == subsidiary.getId()){
		    tenantHelper.doCreateTanent(subsidiary.getCode().trim());
		}

		return "redirect:/subsidiary/list";
	}
	
    @GetMapping(value = "/grantSub")
    public ModelAndView grantSub() {
        ModelAndView model = new ModelAndView("subsidiary/grant-sub");
        
        List<UserAccount> lstUser = new ArrayList<UserAccount>();
        lstUser = userAccountService.findAllUserAccounts(-1, -1);
        
        List<Subsidiary> lstSub =  new ArrayList<Subsidiary>();
        lstSub = subsidiaryService.findAllSubsidiarys(-1, -1);
        
        model.addObject("subsidiaryUserMap", subsidiaryUserDetailService.getSubsidiaryUserMap());
        model.addObject("userAccounts", lstUser);
        model.addObject("subsidiarys", lstSub);
        
        return model;
    }
    
    @PostMapping(value = "grantSub/save")
    @ResponseBody
    public String doSaveGrantSub(HttpServletRequest request, @ModelAttribute("data") String data, BindingResult result, Model model) {
        int rs = 0;
        try {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(data).getAsJsonArray();
            rs = subsidiaryUserDetailService.saveSubsidiaryUserDetail(jsonArray);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return ResponseUtil.createAjaxSuccess(rs);
    }
	
}
