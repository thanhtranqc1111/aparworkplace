package com.ttv.cashflow.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.UserHistory;
import com.ttv.cashflow.service.UserHistoryService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.Constant.LOG_MODULE;
import com.ttv.cashflow.util.ResponseUtil;

@Controller
@RequestMapping("/userHistory")
@PrototypeScope
public class UserHistoryController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserHistoryController.class);

    @Autowired
    private UserHistoryService userHistoryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView loadPage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("user-history/list");

        HttpSession httpSession = request.getSession();

        @SuppressWarnings("unchecked")
        List<Subsidiary> subsidiaries = (List<Subsidiary>) httpSession.getAttribute(Constant.SESSSION_NAME_SUBSIDIARIES);

        // get all module which are defined by enum
        LOG_MODULE[] moduleEnumArr = Constant.LOG_MODULE.values();
        List<String> modules = new ArrayList<>();

        for (LOG_MODULE item : moduleEnumArr) {
            modules.add(item.getValue());
        }

        modelAndView.addObject("moduleList", modules);
        modelAndView.addObject("subsidiaryList", subsidiaries);
        return modelAndView;
    }

    @RequestMapping(value = "/getAllPagingUserHistory", method = { RequestMethod.GET }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingUserHistory(HttpServletRequest request) {
        try {

            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            String orderBy = request.getParameter("order[0][dir]");
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));

            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            String module = request.getParameter("module");
            String tenantCode = request.getParameter("tenantCode");
            String fromMonth = request.getParameter("fromMonth");
            String toMonth = request.getParameter("toMonth");

            UserHistory userHistory = new UserHistory(firstName, lastName, email, module, tenantCode);

            return userHistoryService.findUserHistoryJson(userHistory, fromMonth, toMonth, startpage, lengthOfpage, orderColumn, orderBy);
        } catch (Exception e) {
            LOGGER.info("getAllPagingUserHistory failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
    }
}
