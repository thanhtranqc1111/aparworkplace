package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.ProjectMasterImportComponent;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.service.ApprovalSalesOrderRequestService;
import com.ttv.cashflow.service.BusinessTypeMasterService;
import com.ttv.cashflow.service.PayeePayerMasterService;
import com.ttv.cashflow.service.ProjectMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh created date Oct 3, 2017
 */
@Controller()
@RequestMapping("/admin/projectMaster")
@PrototypeScope
public class ProjectMasterController extends ExchangeController {

    private static final String TRANSACTION_TYPE_CUSTOMER = "Customer";

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectMasterController.class);

    @Autowired
    private ProjectMasterService projectMasterService;

    @Autowired
    private BusinessTypeMasterService businessTypeMasterService;

    @Autowired
    private ApprovalSalesOrderRequestService approvalSalesOrderRequestService;

    @Autowired
    private PayeePayerMasterService transactionPartyMasterService;

    @Autowired
    private ProjectMasterImportComponent projectMasterImportComponent;

    private static final Logger logger = LoggerFactory.getLogger(ProjectMasterController.class);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView projectMasterView = new ModelAndView("master/project");
        projectMasterView.addObject("projectMasterForm", new ProjectMaster());
        projectMasterView.addObject("adminPage", "projectMaster");
        projectMasterView.addObject("businessTypeMasterList", businessTypeMasterService.findAllBusinessTypeMasters(-1, -1));
        projectMasterView.addObject("approvalSalesOrderRequestList", approvalSalesOrderRequestService.findAllApprovalSalesOrderRequests(-1, -1));
        projectMasterView.addObject("transactionPartyMasterList", transactionPartyMasterService.findTransactionPartyMastersByTransactionType(TRANSACTION_TYPE_CUSTOMER));
        return projectMasterView;
    }

    @RequestMapping(value = "/api/getAllPagingProject", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingProject(final HttpServletRequest request, Model model) {
        String jProjectMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jProjectMaster = projectMasterService.findProjectMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
            LOGGER.info("getAllPagingProject failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jProjectMaster;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String savePayeeTypeMaster(HttpServletRequest request, @ModelAttribute("projectMasterForm") @Valid ProjectMaster projectMaster,
            BindingResult result, Model model) {
        try {
            String mode = request.getParameter("mode");

            if (Constant.MODE.NEW.getValue().equals(mode)) {
                ProjectMaster existingProject = projectMasterService.findProjectMasterByPrimaryKey(projectMaster.getCode());

                if (existingProject != null) {
                    LOGGER.info("The code {} existed in db.", projectMaster.getCode());
                    return "redirect:/admin/projectMaster/list";
                } else {
                    projectMasterService.createProjectMaster(projectMaster);
                }
            } else if (Constant.MODE.EDIT.getValue().equals(mode)) {
                projectMasterService.updateProjectMaster(projectMaster);
            }

            return "redirect:/admin/projectMaster/list";
        } catch (Exception e) {
            LOGGER.info("savePayeeTypeMaster failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteProjectMaster(final HttpServletRequest request, Model model) {
        try {
            String[] codes = (String[]) request.getParameterValues("id");
            projectMasterService.deleteProjectMasterByCodes(codes);
            logger.info("Delete suscess, Project Master: {}.", codes.toString());

        } catch (Exception e) {
            LOGGER.info("deleteProjectMaster failingly: {}.", e);
            return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return "redirect:/admin/projectMaster/list";
    }

    @Override
    public ImportComponent getImportComponent() {
        return projectMasterImportComponent;
    }

    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "PRM").getValue();
    }

    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}