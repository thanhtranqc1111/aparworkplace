package com.ttv.cashflow.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.service.PermissionService;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 30, 2017
 */
@Controller()
@RequestMapping("/permission")
@PrototypeScope
public class PermissionController {
	@Autowired
	private PermissionService permissionService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
	    ModelAndView model = new ModelAndView("role-permission/permission");
        return model;
    }
	
	@RequestMapping(value = "/api/getAllPagingPermission", method = { RequestMethod.GET }, produces = {"application/json; charset=UTF-8" })
	@ResponseBody
	public String getAllPagingPermission(final HttpServletRequest request, Model model) {
		String jsonData = "";
		try {
			String keyword = request.getParameter("keyword");
			jsonData = permissionService.findPermissionJson(keyword);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createAjaxError(null);
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("data") String data, BindingResult result, Model model) {
		int rs = 0;
		try {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(data).getAsJsonArray();
			List<Permission> list = permissionService.parsePermissionJSON(jsonArray);
			for(Permission permission: list)
			{
				permissionService.savePermission(permission);
			}
			rs = 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ResponseUtil.createAjaxSuccess(rs);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
	public String deleteProjectMaster(final HttpServletRequest request, Model model) {
		try {
			String[] codes = request.getParameter("id").split(",");
			Integer[] arrIds = new Integer[codes.length];
			for (int i = 0; i < codes.length; i++) {
				arrIds[i] = new Integer(codes[i]);
			}
			permissionService.deletePermissionByIds(arrIds);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createAjaxError(null);
		}
		return "redirect:/permission/list";
	}
}
