package com.ttv.cashflow.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.RolePermission;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.dto.UserAccountAdminForm;
import com.ttv.cashflow.dto.UserAccountForm;
import com.ttv.cashflow.service.RolePermissionService;
import com.ttv.cashflow.service.RoleService;
import com.ttv.cashflow.service.SubsidiaryService;
import com.ttv.cashflow.service.SubsidiaryUserDetailService;
import com.ttv.cashflow.service.UserAccountService;
import com.ttv.cashflow.service.impl.UserAccountServiceImpl;
import com.ttv.cashflow.session.LoginVisited;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.PermissionCode;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 26, 2017
 */
@Controller()
@RequestMapping("/userAccount")
@PrototypeScope
public class UserAccountController {
	@Autowired
	private UserAccountService userAccountService;
	@Autowired
	private LoginVisited loginInfo;
	@Autowired
	private RoleService roleService;
	@Autowired
	private SubsidiaryService subsidiaryService;
	@Autowired
	private SubsidiaryUserDetailService subsidiaryUserDetailService;
	@Autowired
	private RolePermissionService rolePermissionService;
	@Autowired
    protected MessageSource messageSource;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        return new ModelAndView("user-account/list");
    }
	
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public ModelAndView detail(@PathVariable Integer id) {
	   UserAccount userAccount = userAccountService.findUserAccountByPrimaryKey(id);
	   if(userAccount != null) {
		   boolean allowAccess;
		   //current user login has ADMIN role can access all user detail
		   if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_008.toString())){
			   allowAccess = true;
		   }
		   //manager only can access user in the same subsidiary
		   else {
			   allowAccess = userAccount.getSubsidiaryUserDetails().stream().anyMatch(sub -> 
				   sub.getSubsidiary().getId().compareTo(loginInfo.getSubsidiaryId()) == 0
			   );
		   }
		   if(allowAccess)
		   {
			   ModelAndView model = new ModelAndView("user-account/detail");
		       model.addObject("role", "SYS");
		       model.addObject("roleList", getAllRole());
		       model.addObject("subsidiatyList", getAllowanceSubsidiariesUserCanAccess());
		       model.addObject("userAccount", userAccount);
		       model.addObject("qr", generateQRUrl(userAccount));
		       return model;
		   }
	   }
       return new ModelAndView("redirect:/");
    }
	
	@RequestMapping(value = "/addNew", method = RequestMethod.GET)
    public ModelAndView addNew() {
	    UserAccount userAccount = new UserAccount();
	    userAccount.setId(0);
	    ModelAndView model = new ModelAndView("user-account/detail");
	    model.addObject("role", "SYS");
        model.addObject("userAccount", userAccount);
        model.addObject("roleList", getAllowanceRolesUserCanAccess());
        model.addObject("subsidiatyList", getAllowanceSubsidiariesUserCanAccess());
        return model;
    }
	
	@RequestMapping(value = "/myProfile", method = RequestMethod.GET)
    public ModelAndView myProfile() {
	   if(loginInfo != null) {
		   UserAccount userAccount = userAccountService.findUserAccountByPrimaryKey(loginInfo.getUserAccountId());
		   if(userAccount != null) {
		       ModelAndView model = new ModelAndView("user-account/detail");
		       model.addObject("userAccount", userAccount);
		       model.addObject("qr", generateQRUrl(userAccount));
		       return model;
		   }
	   }
       return new ModelAndView("redirect:/");
    }
	
	@RequestMapping(value = "/api/getAllPagingUserAccount", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingUserAccount(final HttpServletRequest request, Model model) {
    	String jsonResult = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            String keyword = request.getParameter("txtSearch");
            if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_008.toString()))
            {
            	//load all user
            	jsonResult = userAccountService.findUserAccountJson(startpage, lengthOfpage, 0, keyword);
            }
            else {
            	//load user of user's subsidiary
            	jsonResult = userAccountService.findUserAccountJson(startpage, lengthOfpage, loginInfo.getSubsidiaryId(), keyword);
            }
        } catch (Exception e) {
           e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jsonResult;
    }
	
	@RequestMapping(value = "/saveMyProfile", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, @ModelAttribute("userAccount") @Valid UserAccountForm userAccount, BindingResult bindingResult, Model model) {
		Locale locale = LocaleContextHolder.getLocale();
		try {
			if(bindingResult.hasErrors()) {
				return ResponseUtil.createAjaxError(bindingResult.getAllErrors());
			}
			else {
				int rs = userAccountService.saveUserAccount(userAccount);
				List<ObjectError> listObjectError = new ArrayList<>();
				switch (rs) {
				case 0:
					return ResponseUtil.createAjaxSuccess(0);
				case UserAccountServiceImpl.SAVE_ERROR_INVAILD_PASSWORD:
					
					listObjectError.add(new ObjectError("Validity Password Format Error", messageSource.getMessage("cashflow.userAccount.validityPasswordFormat", new Object[]{}, locale)));
					break;
				case UserAccountServiceImpl.SAVE_ERROR_PASSWORD_NOT_IDENTICAL:
					listObjectError.add(new ObjectError("Validity Password Match Error", messageSource.getMessage("cashflow.userAccount.validityPasswordMatch", new Object[]{}, locale)));
					break;
				case UserAccountServiceImpl.SAVE_ERROR_EMAIL_OR_PASSWORD_IS_EXISTS:
					listObjectError.add(new ObjectError("Validity Login Name Exists", messageSource.getMessage("cashflow.userAccount.ValidityLoginNameExistst", new Object[]{}, locale)));
					break;
				default:
					break;
				}
				if(!listObjectError.isEmpty())
					return ResponseUtil.createAjaxError(listObjectError);
				else
					return ResponseUtil.createAjaxSuccess(rs);
			}
		} catch (Exception e) {
			return ResponseUtil.createAjaxError(0);
		}
	}
	
	@RequestMapping(value = "/admin.save", method = RequestMethod.POST)
	@ResponseBody
	public String saveForAdmin(HttpServletRequest request, @RequestBody @Valid UserAccountAdminForm userAccount, BindingResult bindingResult, Model model) {
		Locale locale = LocaleContextHolder.getLocale();
		try {
			if(bindingResult.hasErrors()) {
				return ResponseUtil.createAjaxError(bindingResult.getAllErrors());
			}
			else {
				int rs = userAccountService.saveUserAccountAdmin(userAccount);
				List<ObjectError> listObjectError = new ArrayList<>();
				switch (rs) {
				case 0:
					return ResponseUtil.createAjaxSuccess(0);
				case UserAccountServiceImpl.SAVE_ERROR_INVAILD_PASSWORD:
					listObjectError.add(new ObjectError("Validity Password Format Error", messageSource.getMessage("cashflow.userAccount.validityPasswordFormat", new Object[]{}, locale)));
					break;
				case UserAccountServiceImpl.SAVE_ERROR_PASSWORD_NOT_IDENTICAL:
					listObjectError.add(new ObjectError("Validity Password Match Error", messageSource.getMessage("cashflow.userAccount.validityPasswordMatch", new Object[]{}, locale)));
					break;
				case UserAccountServiceImpl.SAVE_ERROR_EMAIL_OR_PASSWORD_IS_EXISTS:
					listObjectError.add(new ObjectError("Validity Login Name Exists", messageSource.getMessage("cashflow.userAccount.ValidityLoginNameExistst", new Object[]{}, locale)));
					break;
				default:
					break;
				}
				if(!listObjectError.isEmpty())
					return ResponseUtil.createAjaxError(listObjectError);
				else
					return ResponseUtil.createAjaxSuccess(rs);
			}
		} catch (Exception e) {
			return ResponseUtil.createAjaxError(0);
		}
	}
	
	@RequestMapping(value = "/generatePassword", method = RequestMethod.POST)
	@ResponseBody
	public String generatePassword() {
		try {
			return ResponseUtil.createAjaxSuccess(userAccountService.generatePassword());
		} catch (Exception e) {
			return ResponseUtil.createAjaxError(0);
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
	public String delete(final HttpServletRequest request, Model model) {
		try {
			String[] codes = request.getParameter("id").split(",");
			Integer[] arrIds = new Integer[codes.length];
			for (int i = 0; i < codes.length; i++) {
				arrIds[i] = new Integer(codes[i]);
			}
			userAccountService.deleteUserAccountByIds(arrIds);
		} catch (Exception e) {
			return "500";
		}
		return "redirect:/userAccount/list";
	}
	
	@RequestMapping(value = "/toogle2FA", method = RequestMethod.POST)
	@ResponseBody
	public String toogle2FA(@ModelAttribute("value") Boolean value, @ModelAttribute("id") Integer id, BindingResult bindingResult) {
		try {
			if(bindingResult.hasErrors()) {
				return ResponseUtil.createAjaxError(bindingResult.getAllErrors());
			}
			else {
				boolean roleUpdate = false;
				UserAccount userAccount = userAccountService.findUserAccountByPrimaryKey(id);
				//owner or admin user
				if(loginInfo.getUserAccountId() == id || AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_008.toString()))
				{
					roleUpdate = true;
				}
				//manager of tantent
				else if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_007.toString())) {
					if(userAccount.getSubsidiaryUserDetails().stream().anyMatch(p -> p.getSubsidiary().getId() == loginInfo.getSubsidiaryId()))
					{
						roleUpdate = true;
					}
				}
				if(roleUpdate) {
					userAccount.setIsUsing2fa(value);
					userAccountService.saveUserAccount(userAccount);
					return ResponseUtil.createAjaxSuccess(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseUtil.createAjaxError(0);
	}
	
	private List<Subsidiary> getAllowanceSubsidiariesUserCanAccess() {
		if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_008.toString()))
		{
			return subsidiaryService.findAllSubsidiarys(-1, -1);
		}
		else {
			return subsidiaryUserDetailService.findSubsidiaryUserDetailByUserId(loginInfo.getUserAccountId()).stream().map(sub -> sub.getSubsidiary()).collect(Collectors.toList());
		}
	}
	
	private List<Role> getAllowanceRolesUserCanAccess() {
		if(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()).contains(PermissionCode.PMS_008.toString())) {
			return getAllRole();
		}
		else {
			List<Role> list = getAllRole();
			return list.stream().filter(role -> role.getRoleType().equals(Constant.ROLE_TYPE.MEMBER.getValue())).collect(Collectors.toList());
		}
	}
	
	private List<Role> getAllRole(){
		List<Role> listRole = roleService.findAllRoles(-1, -1);
		for(Role role: listRole) {
			Set<RolePermission> setPermission = rolePermissionService.findRolePermissionByRoleId(role.getId());
			if(setPermission.stream().anyMatch(rp -> rp.getPermission().getCode().equals(PermissionCode.PMS_008.toString())))
			{
				role.setRoleType(Constant.ROLE_TYPE.ADMIN.getValue());
			}
			else if(setPermission.stream().anyMatch(rp -> rp.getPermission().getCode().equals(PermissionCode.PMS_007.toString())))
			{
				role.setRoleType(Constant.ROLE_TYPE.MANAGER.getValue());
			}
			else {
				role.setRoleType(Constant.ROLE_TYPE.MEMBER.getValue());
			}
		}
		return listRole;
	}
	
	public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";

	public String generateQRUrl(UserAccount user) {
		try {
			return QR_PREFIX + URLEncoder.encode(String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", "APARApplication",
					user.getEmail(), user.getSecretToken(), "APARApplication"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
