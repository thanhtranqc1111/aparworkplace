package com.ttv.cashflow.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ttv.cashflow.annotation.PrototypeScope;
import com.ttv.cashflow.component.ExportComponent;
import com.ttv.cashflow.component.ImportComponent;
import com.ttv.cashflow.component.GstMasterImportComponent;
import com.ttv.cashflow.domain.GstMaster;
import com.ttv.cashflow.service.GstMasterService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * @author thoai.nh
 * created date Oct 4, 2017
 */
@Controller()
@RequestMapping("/admin/gstMaster")
@PrototypeScope
public class GstMasterController extends ExchangeController{
	@Autowired
	private GstMasterService gstMasterService;
	
	@Autowired
    private GstMasterImportComponent gstMasterImportComponent;
	
	private static final Logger logger = LoggerFactory.getLogger(GstMasterController.class);
	
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView index() {
       ModelAndView payeeMasterView = new ModelAndView("master/gst");
       payeeMasterView.addObject("gstMasterForm", new GstMaster());
       payeeMasterView.addObject("adminPage", "gstMaster");
       return payeeMasterView;
    }
    
    @RequestMapping(value = "/api/getAllPagingGst", method = { RequestMethod.POST }, produces = { "application/json; charset=UTF-8" })
    @ResponseBody
    public String getAllPagingVat(final HttpServletRequest request, Model model) {
    	String jGstMaster = "";
        try {
            Integer startpage = NumberUtils.toInt(request.getParameter("start"));
            Integer lengthOfpage = NumberUtils.toInt(request.getParameter("length"));
            Integer orderColumn = NumberUtils.toInt(request.getParameter("order[0][column]"));
            String orderBy = request.getParameter("order[0][dir]");
            String keyword = request.getParameter("search[value]");
            jGstMaster = gstMasterService.findGstMasterJson(startpage, lengthOfpage, orderColumn, orderBy, keyword);
        } catch (Exception e) {
        	e.printStackTrace();
           return ResponseUtil.createAjaxError(new ArrayList<>());
        }
        return jGstMaster;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveGstMaster(HttpServletRequest request, @ModelAttribute("GstMasterForm") @Valid GstMaster gstMaster,
            BindingResult result, Model model) {
            try {
            	 String mode = request.getParameter("mode");
                 if (Constant.MODE.NEW.getValue().equals(mode)) {
                     
                	 GstMaster existVat  = gstMasterService.findGstMasterByPrimaryKey(gstMaster.getCode());
                     if (existVat != null)
                         return "redirect:/admin/gstMaster/list"; 
                 } 
                gstMasterService.saveGstMaster(gstMaster);
                logger.info("Save suscess, Gst Master: {}.", gstMaster.toString());
                
                return "redirect:/admin/gstMaster/list";
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json; charset=UTF-8" })
    public String deleteGstMaster(final HttpServletRequest request, Model model) {
            try {
                String[] codes = (String[])request.getParameterValues("id");
                gstMasterService.deleteGstMasterByCodes(codes);
                logger.info("Delete suscess, Gst Master: {}.", codes.toString());
                
            } catch (Exception e) {
               return ResponseUtil.createAjaxError(new ArrayList<>());
            }
            return "redirect:/admin/gstMaster/list"; 
    }
    
    @Override
    public ImportComponent getImportComponent(){
    	return gstMasterImportComponent;
    }
    
    @Override
    public String getRelatePath(String type) {
        return systemValueHelper.get(type, "VM").getValue();
    }
    
    @Override
    public ExportComponent getExportComponent() {
        return null;
    }
}
	