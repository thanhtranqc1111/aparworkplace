package com.ttv.cashflow.helper;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.service.PayeePayerMasterService;

@Component
public class PayeePayerMasterDataHelper {

    @Autowired
    private PayeePayerMasterService service;

    public Set<PayeePayerMaster> list() {
        return service.loadPayeePayerMasters();
    }

    public PayeePayerMaster get(String code) {
        return service.findPayeePayerMasterByPrimaryKey(code);
    }

    public String getName(String code) {
        return get(code).getName();
    }

    public String getPaymentTerm(String code) {
        if (StringUtils.isEmpty(code)) {
            return "0";
        }
        return get(code).getPaymentTerm();
    }

}
