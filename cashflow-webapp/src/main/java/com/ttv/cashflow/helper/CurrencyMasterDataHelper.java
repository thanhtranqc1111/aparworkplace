package com.ttv.cashflow.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.service.CurrencyMasterService;

@Component
public class CurrencyMasterDataHelper {

    @Autowired
    private CurrencyMasterService service;

    public Set<CurrencyMaster> list() {
        return service.loadCurrencyMasters();
    }

    
    public Map<String,String> toMap(){
        Map<String,String> map = new LinkedHashMap<String,String>();
        
        Iterator<CurrencyMaster> iter =  list().iterator();
        while(iter.hasNext()){
            CurrencyMaster master = iter.next();
            map.put(master.getCode(), master.getName());
        }
        
        return map;
    }
    
}
