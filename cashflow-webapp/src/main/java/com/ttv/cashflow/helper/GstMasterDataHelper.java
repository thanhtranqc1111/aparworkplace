package com.ttv.cashflow.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.domain.GstMaster;
import com.ttv.cashflow.service.GstMasterService;

@Component
public class GstMasterDataHelper {

    @Autowired
    private GstMasterService service;

    public Set<GstMaster> list() {
        return service.loadGstMasters();
    }

    
    public Map<String,String> toMap(){
        Map<String,String> map = new LinkedHashMap<String,String>();
        
        Iterator<GstMaster> iter =  list().iterator();
        while(iter.hasNext()){
            GstMaster master = iter.next();
            map.put(master.getCode(), master.getType());
        }
        
        return map;
    }
    
    public Map<String,String> toMapCodeAndRate(){
        Map<String,String> map = new LinkedHashMap<String,String>();
        
        Iterator<GstMaster> iter =  list().iterator();
        while(iter.hasNext()){
            GstMaster master = iter.next();
            map.put(master.getCode(), String.valueOf(master.getRate().doubleValue()));
        }
        
        return map;
    }
    
}
