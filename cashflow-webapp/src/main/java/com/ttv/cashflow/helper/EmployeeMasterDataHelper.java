package com.ttv.cashflow.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.service.EmployeeMasterService;

/**
 * @author thoai.nh
 * created date Apr 12, 2018
 */
@Component
public class EmployeeMasterDataHelper {
	@Autowired
	private EmployeeMasterService service;
	
	public Set<EmployeeMaster> list() {
        return service.loadEmployeeMasters();
    }
	
	public Map<String,String> toMap(){
        Map<String,String> map = new LinkedHashMap<>();
        
        Iterator<EmployeeMaster> iter =  list().iterator();
        while(iter.hasNext()){
        	EmployeeMaster master = iter.next();
            map.put(master.getCode(), master.getName() + " (" + master.getCode() + ")");
        }
        
        return map;
    }
}
