package com.ttv.cashflow.helper;

import java.util.Iterator;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.DataSourceLookupFailureException;
import org.springframework.jdbc.datasource.lookup.MapDataSourceLookup;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.dao.TenantDAO;
import com.ttv.cashflow.domain.Tenant;

@Component
public class MapDataSourceLookupHelper {
    
    private static final Logger logger = LoggerFactory.getLogger(MapDataSourceLookupHelper.class);
    
    @Autowired
    private MapDataSourceLookup dataSourceLookup;
    
    @Autowired
    private TenantDAO tenantDAO;
    
    /**
     * Add new datasource if MapDataSource not have exist.
     */
    public void addDataSourceIfNotExist(String tenantCode){
        try {
            
            //check datasource is exist on system. If have, 
            // throw exception.
            dataSourceLookup.getDataSource(tenantCode);
        
        } catch (DataSourceLookupFailureException dslfe) {
            // when not exist datasrouce in MapDataSource
            javax.sql.DataSource newDataSource = createTenantDataSource(tenantCode);
            
            if (null != newDataSource) {
                dataSourceLookup.addDataSource(tenantCode, newDataSource);
            }
        }
    }
    
    /**
     * Create new datasource belong tenant code when it have in database, 
     * otherwise return null.
     *
     */
    private DataSource createTenantDataSource(String tenantCode) {
        
        DataSource ds = null;
        
        Iterator<Tenant> tenantIter = tenantDAO.findTenantByCode(tenantCode).iterator();
        
        if(tenantIter.hasNext()){
            
            ds = new DataSource();

            Tenant tenant = tenantIter.next();
            
            ds.setUrl(tenant.getDbUrl().trim());
            ds.setUsername(tenant.getDbUsername().trim());
            ds.setPassword(tenant.getDbPassword().trim());
            ds.setDriverClassName(tenant.getDbDriverClassName().trim());
            
            logger.info("Create a datasource for database: " + tenant.getDbName());
        }
        
        return ds;
    }
    
}
