package com.ttv.cashflow.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.lookup.MapDataSourceLookup;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.dao.TenantDAO;
import com.ttv.cashflow.dao.TenantScriptInitializationDAO;
import com.ttv.cashflow.domain.Tenant;
import com.ttv.cashflow.domain.TenantScriptInitialization;
import com.ttv.cashflow.service.TenantService;
import com.ttv.cashflow.util.Constant;

@Component
public class TenantHelper {
    
    private static final Logger logger = LoggerFactory.getLogger(TenantHelper.class);
    
    @Autowired
    private MapDataSourceLookup dataSourceLookup;
    
    //PROPERTIES FOR CONNECT TO DATABASE.
    @Value( "${postgresql.default.pre_url}" )
    private String preUrl;
    
    @Value( "${postgresql.default.url}" )
    private String url;
    
    @Value( "${postgresql.default.username}" )
    private String username;
    
    @Value( "${postgresql.default.password}" )
    private String password;
    
    @Value( "${postgresql.default.driver-class-name}" )
    private String driverClassName;
    
    @Value( "${postgresql.cashflow.schema}" )
    private String schema;
    
    @Autowired 
    protected TenantService tenantService;
    
    @Autowired 
    protected TenantDAO tenantDAO;
    
    @Autowired 
    protected TenantScriptInitializationDAO tenantInitDAO;
    
    /**
     * Create new tenant. That is create new database.
     */
    public void doCreateTanent(String code) {

        String dbName = Constant.PREFIX_DB_NAME + code;
        
        // Create new database.
        createNewDb(dbName);
    
        applyDDLforNewDb(code, dbName);
        
        // Add new configuration for new tenant.
        addConfigForNewDb(code, dbName);
        
        logger.info("Create new tenant successfully.");
        
    }

    /**
     * Create new database for new tenant.
     */
    private void createNewDb(String name) {
        
        Connection conn = null;
        Statement stmt = null;
        try {
            
            // create connection.
            Class.forName(driverClassName);
            
            conn = DriverManager.getConnection(url, username, password);

            stmt = conn.createStatement();
            
            //
            String dbName = "\"" + name + "\"";
            stmt.executeUpdate("DROP DATABASE IF EXISTS " + dbName + "; CREATE DATABASE " + dbName);
            
            logger.info("Created database : " + name);
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null){
                    stmt.close();
                }
            } catch (SQLException se2) {}
            try {
                if (conn != null){
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    
    /**
     * Get DDL from general database and apply for new database (as tenant).
     */
    private void applyDDLforNewDb(String code, String dbName) {
        
        Connection conn = null;
        Statement stmt = null;

        try {

            String toUrl = preUrl + dbName + "?currentSchema=" + schema;
            // create connection.
            Class.forName(driverClassName);
    
            conn = DriverManager.getConnection(toUrl, username, password);

            stmt = conn.createStatement();
            
            stmt.executeUpdate("CREATE SCHEMA " + schema);
            
            // Create table, function, view and initial system value for system.
            stmt.executeUpdate(getScriptDbConfig(Constant.CONFIG_CODE_CREATE_TABLE));
            logger.info("Created table successfully.");
            
            stmt.executeUpdate(getScriptDbConfig(Constant.CONFIG_CODE_CREATE_FUNCTION));
            logger.info("Created function successfully.");
            
            stmt.executeUpdate(getScriptDbConfig(Constant.CONFIG_CODE_INIT_SYSTEM_VALUE).replaceAll("@@@tenant_id@@@", code));
            logger.info("Init system value successfully.");
            
            stmt.executeUpdate(getScriptDbConfig(Constant.CONFIG_CODE_CREATE_VIEW));
            logger.info("Created view successfully.");

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {}
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        
    }
    
    
    /**
     * Add configuration for new database (new tenant).
     */
    private void addConfigForNewDb(String code, String dbName) {

        //
        String toUrl = preUrl + dbName + "?currentSchema=" + schema;
        Tenant tenant = new Tenant();
        
        //
        tenant.setCode(code);
        tenant.setDbName(dbName);
        tenant.setDbUrl(toUrl);
        tenant.setDbUsername(username);
        tenant.setDbPassword(password);
        tenant.setDbDriverClassName(driverClassName);
        
        //
        tenantService.saveTenant(tenant);
    }
    
    /**
     * Get script configuration in general database.
     * This script will be use for new database (new tenant).
     */
    private String getScriptDbConfig(String code){
        
        Iterator<TenantScriptInitialization> tenantInitIter = tenantInitDAO.findTenantScriptInitializationByCode(code).iterator();
        if(tenantInitIter.hasNext()){
            TenantScriptInitialization tenantInit = tenantInitIter.next();
            
            return tenantInit.getValue();
        }
        
        return "SELECT 1";
    }
    
}
