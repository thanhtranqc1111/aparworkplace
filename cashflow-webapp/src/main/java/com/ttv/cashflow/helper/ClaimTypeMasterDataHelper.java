package com.ttv.cashflow.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.service.ClaimTypeMasterService;

@Component
public class ClaimTypeMasterDataHelper {

    @Autowired
    ClaimTypeMasterService service;

    public Set<ClaimTypeMaster> list() {
        return service.loadClaimTypeMasters();
    }

    
    public Map<String,String> toMap(){
        Map<String,String> map = new LinkedHashMap<String,String>();
        
        Iterator<ClaimTypeMaster> iter =  list().iterator();
        while(iter.hasNext()){
            ClaimTypeMaster master = iter.next();
            map.put(master.getCode(), master.getName());
        }
        
        return map;
    }
    
}
