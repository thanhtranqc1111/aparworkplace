package com.ttv.cashflow.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.service.SystemValueService;

@Component
public class SystemValueHelper {

    
    @Autowired
    private SystemValueDAO dao;
    
    @Autowired
    private SystemValueService service;

    public Set<SystemValue> list() {
        return service.loadSystemValues();
    }
    
    public SystemValue get(String type, String code) {
        return dao.findSystemValueByTypeAndCode(type, code);
    }
    
    
    public Set<SystemValue> get(String type) {
        return dao.findSystemValueByType(type);
    }

    public Map<String, String> toMap(String type) {
        Map<String, String> map = new LinkedHashMap<String, String>();

        Iterator<SystemValue> iter = get(type).iterator();
        while (iter.hasNext()) {
            SystemValue master = iter.next();
            map.put(master.getCode(), master.getValue());
        }

        return map;
    }
    
}
