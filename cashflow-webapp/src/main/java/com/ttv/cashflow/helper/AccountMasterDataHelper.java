package com.ttv.cashflow.helper;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.service.AccountMasterService;

@Component
public class AccountMasterDataHelper {

    @Autowired
    private AccountMasterService service;

    public Set<AccountMaster> list() {
        return service.loadAccountMasters();
    }

    public AccountMaster getByCode(String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        
        return service.findAccountMasterByPrimaryKey(code);
    }

    public String getNameByCode(String code) {
        return getByCode(code).getName();
    }
}
