package com.ttv.cashflow.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

@Component
public class LogHelper {
	
    private FileHandler fh = null;
    private Logger logger = null;

	public void init(Logger logger, String path) {
	    this.logger = logger;
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyyHHmmss");
		
		try {
			fh = new FileHandler(path + "_" + format.format(Calendar.getInstance().getTime()) + ".log");
		} catch (Exception e) {
			e.printStackTrace();
		}

		fh.setFormatter(new Formatter() {
			@Override
			public String format(LogRecord record) {
				SimpleDateFormat logTime = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                Calendar cal = new GregorianCalendar();
                cal.setTimeInMillis(record.getMillis());
                return  logTime.format(cal.getTime())
						+ " [" + record.getLevel() + "] "
						+ record.getLoggerName()
                        + " : "
                        + record.getMessage() + System.lineSeparator();
			}
		});
		logger.addHandler(fh);
	}
	
	public void writeErrorLog(String message){
        this.logger.severe(message);
    }
	
	public void writeErrorLog(int row, String message){
        this.logger.severe(String.format("Row [%s], %s.",row, message));
    }
	
	public void writeInfoLog(String message){
        this.logger.info(message);
    }
	
	public void writeWarningLog(int row, String column, String validate){
	    this.logger.warning(String.format("Row [%s], %s is %s.",row, column, validate));
    }
    
	public void writeWarningLog(int row, String column, String value, String validate) {
        this.logger.warning(String.format("Row [%s], %s : %s is %s.", row, column, value, validate));
    }
	
	public void writeErrorLog(int row, String column, String value, String column2, String value2) {
		this.logger.warning(String.format("Row [%s], Data not found with %s : %s and %s : %s", row, column, value, column2, value2));
    }
    
	public void writeWarningExistLog(int row, String column) {
        this.logger.warning(String.format("Row [%s], %s has already imported.", row, column));
    }
    
	public void writeWarningExistLog(int row, String column, String value) {
        this.logger.warning(String.format("Row [%s], %s : %s has already imported.", row, column, value));
    }
	
	public void writeSummaryLog(int nRecordSuccess, int nRecordFail) {
        this.logger.info(String.format("Successed: %s record(s), Failed: %s record(s).", nRecordSuccess, nRecordFail));
    }
	
	public void writeSummaryLog(int nRecordSuccess, int nRowSuccess, int nRowFail) {
        this.logger.info(String.format("Successed %s %s(s) of %s rows(s), Failed %s rows(s).", 
                                            nRecordSuccess, logger.getName(), nRowSuccess, nRowFail));
    }
    
	public void close() {
		fh.close();
	}
}
