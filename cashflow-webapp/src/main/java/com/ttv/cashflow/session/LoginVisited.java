package com.ttv.cashflow.session;

import org.apache.commons.lang.StringUtils;

import com.ttv.cashflow.annotation.Session;

@Session("LoginVisited")
public class LoginVisited {

	private String email;
	private String userName;
	private String firstName;
	private String lastName;
	private Integer userAccountId;
	private String subsidiaryName;
	private Integer subsidiaryId;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFirstName() {
        return firstName;
    }
	
	public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
	
	public String getLastName() {
        return lastName;
    }
	
	public void setLastName(String lastName) {
        this.lastName = lastName;
    }

	public boolean isLogin() {
		return StringUtils.isNotEmpty(email);

	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}

	public String getSubsidiaryName() {
		return subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}

	public Integer getSubsidiaryId() {
		return subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}
	
}