package com.ttv.cashflow.util;

/**
 * @author thoai.nh
 * created date Dec 4, 2017
 */
public enum ResourceBundleFileNames {
	AP_INVOICE_DETAIL("i18n/ap_invoice_detail_messages"),
	
	BANK_STATEMENT("i18n/bank_statement_messages"),
	
	COMMON("i18n/common_messages"),
	
	EXPORT("i18n/export_messages"),
	
	GRANT_SUBSIDIARY("i18n/grant_subsidiary_messages"),
	
	IMPORT("i18n/import_messages"),
	
	PAYMENT_DETAIL("i18n/payment_detail_messages"),
	
	PERMISSION("i18n/permission_messages"),
	
	ROLE("i18n/role_messages"),
	
	ROLE_PERMISSION("i18n/role_permission_messages"),
	
	SUBSIDIARY_DETAIL("i18n/subsidiary_detail_messages"),
	
	SUBSIDIARY("i18n/subsidiary_messages"),
	
	USER_ACCOUNT_DETAIL("i18n/user_account_detail_messages"),
	
	USER_ACCOUNT("i18n/user_account_messages"),
	
	ACCOUNT_MASTER("i18n/account_master_messages"),
		
	BANK_ACCOUNT_MASTER("i18n/bank_account_master_messages"),
	
	BANK_MASTER("i18n/bank_master_messages"),
	
	BUSINESS_TYPE_MASTER("i18n/business_type_master_messages"),
	
	CLAIM_TYPE_MASTER("i18n/claim_type_master_messages"),
	
	CURRENCY_MASTER("i18n/currency_master_messages"),
	
	GST_MASTER("i18n/gst_master_messages"),
	
	PAYEE_MASTER("i18n/payee_master_messages"),
	
	PAYEE_TYPE_MASTER("i18n/payee_type_master_messages"),
	
	PROJECT_MASTER("i18n/project_master_messages"),
	
    DIVISION_MASTER("i18n/division_master_messages"),

    EMPLOYEE_MASTER("i18n/employee_master_messages"),

	PAYMENT_LIST("i18n/payment_list_messages"),
	
	AP_INVOICE_LIST("i18n/ap_invoice_list_messages"),
	
	LOGIN("i18n/login_messages"),
	
	AR_INVOICE_LIST("i18n/ar_invoice_list_messages"),
	
	AR_INVOICE_DETAIL("i18n/ar_invoice_detail_messages"),
	
	RECEIPT_ORDER("i18n/receipt_order_messages"),
	
	APPROVAL_PURCHASE_REQUEST("i18n/approval_purchase_request_messages"),
	
	APPROVAL_SALES_ORDER_REQUEST("i18n/approval_sales_order_request_messages"),
	
	APPROVAL_TYPE_PURCHASE_MASTER("i18n/approval_type_purchase_master_messages"),
	
    APPROVAL_TYPE_SALES_MASTER("i18n/approval_type_sales_master_messages"),

	PAYMENT_PAYROLL_DETAIL("i18n/payment_payroll_detail_messages"),
	
	PAYMENT_PAYROLL_LIST("i18n/payment_payroll_list_messages"),
	
    PAYROLL_LIST("i18n/payroll_list_messages"),

    PAYROLL_DETAIL("i18n/payroll_detail_messages"),

    USER_HISTORY("i18n/user_history_messages"),
	
	REIMBURSEMENT_DETAIL("i18n/reimbursement_detail_messages"),
	
	REIMBURSEMENT_LIST("i18n/reimbursement_list_messages"),
	
	PROJECT_ALLOCATION("i18n/project_allocation_messages"),
	
	PROJECT_PAYMENT_DETAILS("i18n/project_payment_detail_messages"),
	
	DASHBOARD("i18n/dashboard_messages");
    private final String text;

    /**
     * @param text
     */
    private ResourceBundleFileNames(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
