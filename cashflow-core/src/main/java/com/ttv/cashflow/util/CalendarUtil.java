/**
 * 
 */
package com.ttv.cashflow.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author tientm
 *
 */
public class CalendarUtil {

    /**
     * Get Calendar with format.
     */
    public static Calendar getCalendar(String date, String pattern) {
        if (StringUtils.isEmpty(pattern)) {
            pattern = Constant.DDMMYYYY;
        }
        Calendar cal = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            cal.setTime(sdf.parse(date));

            return cal;
        } catch (NullPointerException | ParseException e) {
            return null;
        }
    }

    /**
     * Get Calendar with format "dd/MM/yyyy"
     */
    public static Calendar getCalendar(String date) {
        return getCalendar(date, null);
    }

    /**
     * Get Calendar from Date
     */
    public static Calendar getCalendar(Date date) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
        } catch (Exception e) {

        }

        return null;
    }
    
    /**
     * Return date as string with pattern.
     */
    public static String toString(Calendar cal, String pattern) {
        if(cal == null) return "";
        
        if(pattern == null){
            pattern = Constant.DDMMYYYY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        return sdf.format(cal.getTime());
    }
    
    /**
     * Return date as string with pattern is dd/MM/yyyy.
     */
    public static String toString(Calendar cal) {
        return toString(cal, null);
    }
}
