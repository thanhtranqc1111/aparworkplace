package com.ttv.cashflow.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * NumberUtil
 * @author thachnx
 *
 */
public class NumberUtil {

    /**
     * Get BigDecimal having scale.
     * @param scale scale of the BigDecimal value to be returned.
     * @param value String representation of BigDecimal.
     */
    public static BigDecimal getBigDecimalScaleDown(int scale, String value) {
        Assert.notNull(value, "value must not be null");
        
        if(!StringUtils.hasText(value)){
            return BigDecimal.ZERO;
        }
        
        String valueTrimed = StringUtils.trimAllWhitespace(value);
        if(scale == -1)
        {
        	return new BigDecimal(valueTrimed);
        }
        else {
        	return new BigDecimal(valueTrimed).setScale(scale, RoundingMode.DOWN);
        }
    }
    
    /**
     * Get BigDecimal having scale.
     * @param scale scale of the BigDecimal value to be returned.
     * @param value a BigDecimal to be scale.
     */
    public static BigDecimal getBigDecimalScaleDown(int scale, BigDecimal value) {
        if(Objects.isNull(value)){
            return BigDecimal.ZERO;
        }
        
        return value.setScale(scale, RoundingMode.DOWN);
    }
    
    public static BigDecimal ensureBigDecimal(BigDecimal value) {
        if(Objects.isNull(value)){
            return BigDecimal.ZERO;
        }
        
        return value;
    }
    
    /**
     * Format value of double to currency format.
     */
    public static String format2Currency(double val){
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "US"));
        
        return formatter.format(val).substring(1);
    }
}
