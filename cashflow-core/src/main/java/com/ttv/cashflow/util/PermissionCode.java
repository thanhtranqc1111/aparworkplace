package com.ttv.cashflow.util;

/**
 * @author thoai.nh
 * created date Mar 9, 2018
 */
public enum PermissionCode {
	PMS_001("PMS-001"),
	PMS_002("PMS-002"),
	PMS_003("PMS-003"),
	PMS_004("PMS-004"),
	PMS_005("PMS-005"),
	PMS_006("PMS-006"),
	PMS_007("PMS-007"),
	PMS_008("PMS-008"),
	PMS_009("PMS-009");
	private final String code;
	
    private PermissionCode(final String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
