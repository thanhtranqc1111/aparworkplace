package com.ttv.cashflow.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

public class ResponseUtil {
    
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    
    public static String createAjaxError(Object result, String... messages) {
        Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("STATUS", ERROR);
        map.put("RESULT", result);
        for (String message : messages) {
            map.put("MESSAGE", message);
        }
        return gson.toJson(map);
    }

    public static String createAjaxSuccess(Object result, String... messages) {
        Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("STATUS", SUCCESS);
        map.put("RESULT", result);
        for (String message : messages) {
            map.put("MESSAGE", message);
        }
        return gson.toJson(map);
    }

    public static Map<String, Object> ajaxSuccess(Map<String, Object> params, String message) {
        Map<String, Object> ret = new HashMap<String, Object>();
        ret.put("STATUS", SUCCESS);

        if(StringUtils.isNotEmpty(message)){
            ret.put("MESSAGE", message);
        }

        if(Objects.nonNull(params)){
            ret.putAll(params);
        }
        
        //
        return ret;
    }

    public static Map<String, Object> ajaxSuccess(Map<String, Object> params) {
        return ajaxSuccess(params, null);
    }
    
    public static Map<String, Object> ajaxSuccess() {
        return ajaxSuccess(null, null);
    }
}
