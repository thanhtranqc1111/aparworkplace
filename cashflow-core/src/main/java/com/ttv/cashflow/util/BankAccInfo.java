package com.ttv.cashflow.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author thoai.nh
 * created date Feb 9, 2018
 */
public class BankAccInfo {
	/*
	 * map hold info who is editing bank account <BankAccInfo,SessionIdString>
	 */
	public static Map<BankAccInfo, String> MAP_EDITING_USER = new HashMap<BankAccInfo, String>();
	private int subsidiaryId;
	private String bankName;
	private String bankAccNo;
	private String userName;
	public BankAccInfo() {}
	public BankAccInfo(int subsidiaryId, String bankName, String bankAccNo) {
		super();
		this.subsidiaryId = subsidiaryId;
		this.bankName = bankName;
		this.bankAccNo = bankAccNo;
	}
	public int getSubsidiaryId() {
		return subsidiaryId;
	}
	public void setSubsidiaryId(int subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccNo() {
		return bankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return String.join("_", String.valueOf(subsidiaryId), bankName, bankAccNo, userName);
	}
	@Override
	public int hashCode() {
		return bankName.length() + bankAccNo.length();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccInfo) {
			BankAccInfo bankAccInfo = (BankAccInfo) obj;
			if(this.getSubsidiaryId() == subsidiaryId &&
			   this.bankName.equals(bankAccInfo.getBankName()) &&
			   this.bankAccNo.equals(bankAccInfo.getBankAccNo()))
			{
				return true;
			}
		}
		return false;
	}
}
