
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApprovalPurchaseRequests", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApplicant", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.applicant = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApplicantContaining", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.applicant like ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApplicationDate", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.applicationDate = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApplicationDateAfter", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.applicationDate > ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApplicationDateBefore", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.applicationDate < ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovalCode", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvalCode = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovalCodeContaining", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvalCode like ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovalType", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvalType = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovalTypeContaining", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvalType like ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovedDate", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvedDate = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovedDateAfter", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvedDate > ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByApprovedDateBefore", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.approvedDate < ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByCreatedDate", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.createdDate = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByCurrencyCode", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.currencyCode = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByCurrencyCodeContaining", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.currencyCode like ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestById", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.id = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByIncludeGstOriginalAmount", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByModifiedDate", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.modifiedDate = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByPrimaryKey", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.id = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByPurpose", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.purpose = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByPurposeContaining", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.purpose like ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityFrom", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityFrom = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityFromAfter", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityFrom > ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityFromBefore", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityFrom < ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityTo", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityTo = ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityToAfter", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityTo > ?1"),
		@NamedQuery(name = "findApprovalPurchaseRequestByValidityToBefore", query = "select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest where myApprovalPurchaseRequest.validityTo < ?1"),
		@NamedQuery(name = "countApprovalPurchaseRequestAll", query = "select count(1) from ApprovalPurchaseRequest"),
		@NamedQuery(name = "checkApprovalPurchaseRequestExists", query = "select (1) from ApprovalPurchaseRequest where approvalCode = ?1 and includeGstOriginalAmount = ?2"),
})

@Table(schema = "cashflow", name = "approval_purchase_request")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApprovalPurchaseRequest")

public class ApprovalPurchaseRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "approval_type", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalType;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar applicationDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "validity_from")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar validityFrom;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "validity_to")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar validityTo;
	/**
	 */

	@Column(name = "currency_code", length = 5)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currencyCode;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "purpose", length = 400)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String purpose;
	/**
	 */

	@Column(name = "applicant", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String applicant;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "approved_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar approvedDate;
	
	/**
	 */

	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	
	@Transient
	String statusValue;
	
	/**
	 */

	@Column(name = "task_id", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String taskId;	
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;
	
	@Transient
	Set<String> apInVoicesApplied;
	
	@Transient
	Set<String> payrollsApplied;
	
	@Transient
	Set<String> reimbursementsApplied;
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}

	/**
	 */
	public String getApprovalType() {
		return this.approvalType;
	}

	/**
	 */
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 */
	public Calendar getApplicationDate() {
		return this.applicationDate;
	}

	/**
	 */
	public void setValidityFrom(Calendar validityFrom) {
		this.validityFrom = validityFrom;
	}

	/**
	 */
	public Calendar getValidityFrom() {
		return this.validityFrom;
	}

	/**
	 */
	public void setValidityTo(Calendar validityTo) {
		this.validityTo = validityTo;
	}

	/**
	 */
	public Calendar getValidityTo() {
		return this.validityTo;
	}

	/**
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 */
	public String getCurrencyCode() {
		return this.currencyCode;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 */
	public String getPurpose() {
		return this.purpose;
	}

	/**
	 */
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	/**
	 */
	public String getApplicant() {
		return this.applicant;
	}

	/**
	 */
	public void setApprovedDate(Calendar approvedDate) {
		this.approvedDate = approvedDate;
	}

	/**
	 */
	public Calendar getApprovedDate() {
		return this.approvedDate;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}
	
	public Set<String> getApInVoicesApplied() {
		return apInVoicesApplied;
	}

	public void setApInVoicesApplied(Set<String> apInVoicesApplied) {
		this.apInVoicesApplied = apInVoicesApplied;
	}
	
	public Set<String> getPayrollsApplied() {
		return payrollsApplied;
	}

	public void setPayrollsApplied(Set<String> payrollsApplied) {
		this.payrollsApplied = payrollsApplied;
	}
	
	
	public Set<String> getReimbursementsApplied() {
		return reimbursementsApplied;
	}

	public void setReimbursementsApplied(Set<String> reimbursementsApplied) {
		this.reimbursementsApplied = reimbursementsApplied;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 */
	public ApprovalPurchaseRequest() {
	}
	
	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApprovalPurchaseRequest that) {
		setId(that.getId());
		setApprovalCode(that.getApprovalCode());
		setApprovalType(that.getApprovalType());
		setApplicationDate(that.getApplicationDate());
		setValidityFrom(that.getValidityFrom());
		setValidityTo(that.getValidityTo());
		setCurrencyCode(that.getCurrencyCode());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setPurpose(that.getPurpose());
		setApplicant(that.getApplicant());
		setApprovedDate(that.getApprovedDate());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("approvalType=[").append(approvalType).append("] ");
		buffer.append("applicationDate=[").append(applicationDate).append("] ");
		buffer.append("validityFrom=[").append(validityFrom).append("] ");
		buffer.append("validityTo=[").append(validityTo).append("] ");
		buffer.append("currencyCode=[").append(currencyCode).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("purpose=[").append(purpose).append("] ");
		buffer.append("applicant=[").append(applicant).append("] ");
		buffer.append("approvedDate=[").append(approvedDate).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApprovalPurchaseRequest))
			return false;
		ApprovalPurchaseRequest equalCheck = (ApprovalPurchaseRequest) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
