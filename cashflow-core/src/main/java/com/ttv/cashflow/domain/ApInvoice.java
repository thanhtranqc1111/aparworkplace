
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.dto.InvoicePayment;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoices", query = "select myApInvoice from ApInvoice myApInvoice"),
		@NamedQuery(name = "findApInvoiceByAccountPayableCode", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.accountPayableCode = ?1"),
		@NamedQuery(name = "findApInvoiceByAccountPayableCodeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.accountPayableCode like ?1"),
		@NamedQuery(name = "findApInvoiceByAccountPayableName", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.accountPayableName = ?1"),
		@NamedQuery(name = "findApInvoiceByAccountPayableNameContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.accountPayableName like ?1"),
		@NamedQuery(name = "findApInvoiceByApInvoiceNo", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.apInvoiceNo = ?1"),
		@NamedQuery(name = "findApInvoiceByApInvoiceNoContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.apInvoiceNo like ?1"),
		@NamedQuery(name = "findApInvoiceByClaimType", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.claimType = ?1"),
		@NamedQuery(name = "findApInvoiceByClaimTypeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.claimType like ?1"),
		@NamedQuery(name = "findApInvoiceByCreatedDate", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.createdDate = ?1"),
		@NamedQuery(name = "findApInvoiceByDescription", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.description = ?1"),
		@NamedQuery(name = "findApInvoiceByDescriptionContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.description like ?1"),
		@NamedQuery(name = "findApInvoiceByExcludeGstAmount", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoiceByExcludeGstConvertedAmount", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceByFxRate", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.fxRate = ?1"),
		@NamedQuery(name = "findApInvoiceByGst", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.gstRate = ?1"),
		@NamedQuery(name = "findApInvoiceByGstAmount", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceById", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.id = ?1"),
		@NamedQuery(name = "findApInvoiceByInvTotalAmount", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceByInvoiceIssuedDate", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.invoiceIssuedDate = ?1"),
		@NamedQuery(name = "findApInvoiceByInvoiceNo", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.invoiceNo = ?1"),
		@NamedQuery(name = "findApInvoiceByInvoiceNoContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.invoiceNo like ?1"),
		@NamedQuery(name = "findApInvoiceByModifiedDate", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.modifiedDate = ?1"),
		@NamedQuery(name = "findApInvoiceByOriginalCurrencyCode", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findApInvoiceByOriginalCurrencyCodeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeCode", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeCode = ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeCodeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeCode like ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeName", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeName = ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeNameContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeName like ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeTypeCode", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeTypeCode = ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeTypeCodeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeTypeCode like ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeTypeName", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeTypeName = ?1"),
		@NamedQuery(name = "findApInvoiceByPayeeTypeNameContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.payeeTypeName like ?1"),
		@NamedQuery(name = "findApInvoiceByPaymentTerm", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.paymentTerm = ?1"),
		@NamedQuery(name = "findApInvoiceByPaymentTermContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.paymentTerm like ?1"),
		@NamedQuery(name = "findApInvoiceByPrimaryKey", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.id = ?1"),
		@NamedQuery(name = "findApInvoiceByScheduledPaymentDate", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.scheduledPaymentDate = ?1"),
		@NamedQuery(name = "findApInvoiceByStatus", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.status = ?1"),
		@NamedQuery(name = "findApInvoiceByStatusContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.status like ?1"),
		@NamedQuery(name = "findApInvoiceByVatCode", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.gstType = ?1"),
		@NamedQuery(name = "findApInvoiceByVatCodeContaining", query = "select myApInvoice from ApInvoice myApInvoice where myApInvoice.gstType like ?1") })

@SqlResultSetMappings({
    @SqlResultSetMapping(
        name = "InvoiceDetailResult", 
        classes = { 
            @ConstructorResult(
                targetClass = InvoiceDetail.class,
                columns = {
                    @ColumnResult(name = "apInvoiceAccountDetailId", type=BigInteger.class),
                    @ColumnResult(name = "accountCode"), 
                    @ColumnResult(name = "accountName"), 
                    @ColumnResult(name = "apInvoiceClassDetailId", type=BigInteger.class),
                    @ColumnResult(name = "classCode"),
                    @ColumnResult(name = "className"),
                    @ColumnResult(name = "description"),
                    @ColumnResult(name = "poNo"),
                    @ColumnResult(name = "approvalCode"),
                    @ColumnResult(name = "excludeGstOriginalAmount", type=BigDecimal.class),
                    @ColumnResult(name = "fxRate", type=BigDecimal.class),
                    @ColumnResult(name = "excludeGstConvertedAmount", type=BigDecimal.class),
                    @ColumnResult(name = "gstType"),
                    @ColumnResult(name = "gstRate", type=BigDecimal.class),
                    @ColumnResult(name = "gstOriginalAmount", type=BigDecimal.class),
                    @ColumnResult(name = "gstConvertedAmount", type=BigDecimal.class),
                    @ColumnResult(name = "includeGstOriginalAmount", type=BigDecimal.class),
                    @ColumnResult(name = "includeGstConvertedAmount", type=BigDecimal.class),
                    @ColumnResult(name = "isApproval", type=Boolean.class)
                }
            )
        }
    ), //end InvoiceDetailResult
    @SqlResultSetMapping(
        name = "findInvoicePaymentDetail", 
        classes = { 
            @ConstructorResult(
                targetClass = InvoicePayment.class,
                columns = {
                    @ColumnResult(name = "paymentId", type=BigInteger.class),
                    @ColumnResult(name = "paymentOrderNo"),
                    @ColumnResult(name = "valueDate", type=Calendar.class),
                    @ColumnResult(name = "bankRefNo"),
                    @ColumnResult(name = "includeGstOriginalAmount", type=BigDecimal.class),
                    @ColumnResult(name = "includeGstConvertedAmount", type=BigDecimal.class),
                    @ColumnResult(name = "paymentStatus"),
                    @ColumnResult(name = "apInvoiceId", type=BigInteger.class),
                    @ColumnResult(name = "apInvoiceNo"),
                    @ColumnResult(name = "apInvoiceDate"),
                    @ColumnResult(name = "payeeName"),
                    @ColumnResult(name = "payeeCode"),
                    @ColumnResult(name = "invoiceNo"),
                    @ColumnResult(name = "invIncludeGstOriginalAmount", type=BigDecimal.class),
                    @ColumnResult(name = "invIncludeGstConvertedAmount", type=BigDecimal.class),
                    @ColumnResult(name = "invoiceStatus"),
                    @ColumnResult(name = "payeeAccount"),
                    @ColumnResult(name = "claimType"),
                    @ColumnResult(name = "month", type=Calendar.class),
                    @ColumnResult(name = "bankName"),
                    @ColumnResult(name = "bankAccount"),
                    @ColumnResult(name = "apInvoiceCurrency"),
                    @ColumnResult(name = "paymentCurrency"),
                }
            ) 
        }
    ) // end findInvoicePaymentDetail
    
})

@Table(schema = "cashflow", name = "ap_invoice")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoice")
@XmlRootElement(namespace = "cashflow/com/ttv/cashflow/domain")
public class ApInvoice implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payee_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeCode;
	/**
	 */

	@Column(name = "payee_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeName;
	/**
	 */

	@Column(name = "payee_type_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeTypeCode;
	/**
	 */

	@Column(name = "payee_type_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeTypeName;
	/**
	 */

	@Column(name = "ap_invoice_no", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoiceNo;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */

	@Column(name = "original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invoice_issued_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar invoiceIssuedDate;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "gst_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "account_payable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	
	/**
	 */
	@Column(name = "gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal gstOriginalAmount;
	
	
	/**
     */
    @Column(name = "include_gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal includeGstOriginalAmount;
	
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Column(name = "payment_term", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentTerm;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "scheduled_payment_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar scheduledPaymentDate;
	/**
	 */

	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "booking_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar bookingDate;
	
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	
	@Column(name = "import_key", length = 200)
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    String importKey;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "apInvoice", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ApInvoiceAccountDetails> apInvoiceAccountDetailses;
	
	/**
	 */
	@OneToMany(mappedBy = "apInvoice", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ApInvoicePaymentDetails> apInvoicePaymentDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPayeeCode(String payeeCode) {
		this.payeeCode = payeeCode;
	}

	/**
	 */
	public String getPayeeCode() {
		return this.payeeCode;
	}

	/**
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	/**
	 */
	public String getPayeeName() {
		return this.payeeName;
	}

	/**
	 */
	public void setPayeeTypeCode(String payeeTypeCode) {
		this.payeeTypeCode = payeeTypeCode;
	}

	/**
	 */
	public String getPayeeTypeCode() {
		return this.payeeTypeCode;
	}

	/**
	 */
	public void setPayeeTypeName(String payeeTypeName) {
		this.payeeTypeName = payeeTypeName;
	}

	/**
	 */
	public String getPayeeTypeName() {
		return this.payeeTypeName;
	}

	/**
	 */
	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}

	/**
	 */
	public String getApInvoiceNo() {
		return this.apInvoiceNo;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }
	
	public BigDecimal getGstRate() {
        return gstRate;
    }

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}

	/**
	 */
	public Calendar getInvoiceIssuedDate() {
		return this.invoiceIssuedDate;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeOriginalGstAmount) {
		this.excludeGstOriginalAmount = excludeOriginalGstAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }
	
	public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
	
	public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }
	
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }

	public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
	
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }
	
	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 */
	public String getPaymentTerm() {
		return this.paymentTerm;
	}

	/**
	 */
	public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
		this.scheduledPaymentDate = scheduledPaymentDate;
	}

	/**
	 */
	public Calendar getScheduledPaymentDate() {
		return this.scheduledPaymentDate;
	}

	/**
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 */
	public String getStatus() {
		return this.status;
	}

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}
	
	public Calendar getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getImportKey() {
        return importKey;
    }
	
	public void setImportKey(String importKey) {
        this.importKey = importKey;
    }

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setApInvoiceAccountDetailses(Set<ApInvoiceAccountDetails> apInvoiceAccountDetailses) {
		this.apInvoiceAccountDetailses = apInvoiceAccountDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ApInvoiceAccountDetails> getApInvoiceAccountDetailses() {
		if (apInvoiceAccountDetailses == null) {
			apInvoiceAccountDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoiceAccountDetails>();
		}
		return apInvoiceAccountDetailses;
	}

	/**
	 */
	public void setApInvoicePaymentDetailses(Set<ApInvoicePaymentDetails> apInvoicePaymentDetailses) {
		this.apInvoicePaymentDetailses = apInvoicePaymentDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ApInvoicePaymentDetails> getApInvoicePaymentDetailses() {
		if (apInvoicePaymentDetailses == null) {
			apInvoicePaymentDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoicePaymentDetails>();
		}
		return apInvoicePaymentDetailses;
	}

	/**
	 */
	public ApInvoice() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoice that) {
		setId(that.getId());
		setPayeeCode(that.getPayeeCode());
		setPayeeName(that.getPayeeName());
		setPayeeTypeCode(that.getPayeeTypeCode());
		setPayeeTypeName(that.getPayeeTypeName());
		setApInvoiceNo(that.getApInvoiceNo());
		setInvoiceNo(that.getInvoiceNo());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setFxRate(that.getFxRate());
		setGstRate(that.getGstRate());
		setDescription(that.getDescription());
		setInvoiceIssuedDate(that.getInvoiceIssuedDate());
		setClaimType(that.getClaimType());
		setGstType(that.getGstType());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setPaymentTerm(that.getPaymentTerm());
		setScheduledPaymentDate(that.getScheduledPaymentDate());
		setStatus(that.getStatus());
		setBookingDate(that.getBookingDate());
		setMonth(that.getMonth());
		setImportKey(that.getImportKey());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setApInvoiceAccountDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoiceAccountDetails>(that.getApInvoiceAccountDetailses()));
		setApInvoicePaymentDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoicePaymentDetails>(that.getApInvoicePaymentDetailses()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payeeCode=[").append(payeeCode).append("] ");
		buffer.append("payeeName=[").append(payeeName).append("] ");
		buffer.append("payeeTypeCode=[").append(payeeTypeCode).append("] ");
		buffer.append("payeeTypeName=[").append(payeeTypeName).append("] ");
		buffer.append("apInvoiceNo=[").append(apInvoiceNo).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("invoiceIssuedDate=[").append(invoiceIssuedDate).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("accountPayableCode=[").append(accountPayableCode).append("] ");
		buffer.append("accountPayableName=[").append(accountPayableName).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("paymentTerm=[").append(paymentTerm).append("] ");
		buffer.append("scheduledPaymentDate=[").append(scheduledPaymentDate).append("] ");
		buffer.append("status=[").append(status).append("] ");
		buffer.append("bookingDate=[").append(bookingDate).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("importKey=[").append(importKey).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApInvoice))
			return false;
		ApInvoice equalCheck = (ApInvoice) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
