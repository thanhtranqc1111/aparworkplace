
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApprovalSalesOrderRequests", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApplicant", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.applicant = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApplicantContaining", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.applicant like ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApplicationDate", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.applicationDate = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApplicationDateAfter", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.applicationDate > ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApplicationDateBefore", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.applicationDate < ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovalCode", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvalCode = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovalCodeContaining", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvalCode like ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovalType", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvalType = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovalTypeContaining", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvalType like ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovedDate", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvedDate = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovedDateAfter", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvedDate > ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByApprovedDateBefore", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.approvedDate < ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByCreatedDate", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.createdDate = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByCurrencyCode", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.currencyCode = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByCurrencyCodeContaining", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.currencyCode like ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestById", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.id = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByIncludeGstOriginalAmount", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByModifiedDate", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.modifiedDate = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByPrimaryKey", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.id = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByPurpose", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.purpose = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByPurposeContaining", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.purpose like ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityFrom", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityFrom = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityFromAfter", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityFrom > ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityFromBefore", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityFrom < ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityTo", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityTo = ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityToAfter", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityTo > ?1"),
		@NamedQuery(name = "findApprovalSalesOrderRequestByValidityToBefore", query = "select myApprovalSalesOrderRequest from ApprovalSalesOrderRequest myApprovalSalesOrderRequest where myApprovalSalesOrderRequest.validityTo < ?1"),
		@NamedQuery(name = "countApprovalSalesOrderRequestAll", query = "select count(1) from ApprovalSalesOrderRequest"),
		@NamedQuery(name = "checkApprovalSalesOrderRequestExists", query = "select (1) from ApprovalSalesOrderRequest where approvalCode = ?1 and includeGstOriginalAmount = ?2"),
})

@Table(schema = "cashflow", name = "approval_sales_order_request")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApprovalSalesOrderRequest")

public class ApprovalSalesOrderRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "approval_type", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalType;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar applicationDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "validity_from")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar validityFrom;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "validity_to")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar validityTo;
	/**
	 */

	@Column(name = "currency_code", length = 5)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currencyCode;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "purpose", length = 400)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String purpose;
	/**
	 */

	@Column(name = "applicant", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String applicant;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "approved_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar approvedDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;
		
	@Transient
	Set<String> arInVoicesApplied;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}

	/**
	 */
	public String getApprovalType() {
		return this.approvalType;
	}

	/**
	 */
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 */
	public Calendar getApplicationDate() {
		return this.applicationDate;
	}

	/**
	 */
	public void setValidityFrom(Calendar validityFrom) {
		this.validityFrom = validityFrom;
	}

	/**
	 */
	public Calendar getValidityFrom() {
		return this.validityFrom;
	}

	/**
	 */
	public void setValidityTo(Calendar validityTo) {
		this.validityTo = validityTo;
	}

	/**
	 */
	public Calendar getValidityTo() {
		return this.validityTo;
	}

	/**
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 */
	public String getCurrencyCode() {
		return this.currencyCode;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 */
	public String getPurpose() {
		return this.purpose;
	}

	/**
	 */
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	/**
	 */
	public String getApplicant() {
		return this.applicant;
	}

	/**
	 */
	public void setApprovedDate(Calendar approvedDate) {
		this.approvedDate = approvedDate;
	}

	/**
	 */
	public Calendar getApprovedDate() {
		return this.approvedDate;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public ApprovalSalesOrderRequest() {
	}

	public Set<String> getArInVoicesApplied() {
		return arInVoicesApplied;
	}

	public void setArInVoicesApplied(Set<String> arInVoicesApplied) {
		this.arInVoicesApplied = arInVoicesApplied;
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApprovalSalesOrderRequest that) {
		setId(that.getId());
		setApprovalCode(that.getApprovalCode());
		setApprovalType(that.getApprovalType());
		setApplicationDate(that.getApplicationDate());
		setValidityFrom(that.getValidityFrom());
		setValidityTo(that.getValidityTo());
		setCurrencyCode(that.getCurrencyCode());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setPurpose(that.getPurpose());
		setApplicant(that.getApplicant());
		setApprovedDate(that.getApprovedDate());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("approvalType=[").append(approvalType).append("] ");
		buffer.append("applicationDate=[").append(applicationDate).append("] ");
		buffer.append("validityFrom=[").append(validityFrom).append("] ");
		buffer.append("validityTo=[").append(validityTo).append("] ");
		buffer.append("currencyCode=[").append(currencyCode).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("purpose=[").append(purpose).append("] ");
		buffer.append("applicant=[").append(applicant).append("] ");
		buffer.append("approvedDate=[").append(approvedDate).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApprovalSalesOrderRequest))
			return false;
		ApprovalSalesOrderRequest equalCheck = (ApprovalSalesOrderRequest) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
