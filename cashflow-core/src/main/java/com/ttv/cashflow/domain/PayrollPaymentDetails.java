
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayrollPaymentDetailss", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails"),
		@NamedQuery(name = "findPayrollPaymentDetailsByCreatedDate", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.createdDate = ?1"),
		@NamedQuery(name = "findPayrollPaymentDetailsById", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.id = ?1"),
		@NamedQuery(name = "findPayrollPaymentDetailsByModifiedDate", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findPayrollPaymentDetailsByNetPaymentAmountOriginal", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.netPaymentAmountOriginal = ?1"),
		@NamedQuery(name = "findPayrollPaymentDetailsByNetPaymentAmountConverted", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.netPaymentAmountConverted = ?1"),
		@NamedQuery(name = "findPayrollPaymentDetailsByPaymentId", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.paymentOrder.id = ?1"),
		@NamedQuery(name = "findTotalNetPaymentAmountByPayrollId", query = "select sum(myPayrollPaymentDetails.netPaymentAmountOriginal) from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.paymentOrder.id != ?1 and myPayrollPaymentDetails.payroll.id = ?2"),
		@NamedQuery(name = "findPayrollPaymentDetailsByPrimaryKey", query = "select myPayrollPaymentDetails from PayrollPaymentDetails myPayrollPaymentDetails where myPayrollPaymentDetails.id = ?1") })

@Table(schema = "cashflow", name = "payroll_payment_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "PayrollPaymentDetails")

public class PayrollPaymentDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "net_payment_amount_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentAmountOriginal;
	/**
	 */
	
	@Column(name = "net_payment_amount_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentAmountConverted;
	/**
	 */
	
	@Column(name = "payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "payroll_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	Payroll payroll;
	
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "payment_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	PaymentOrder paymentOrder;
	
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	public BigDecimal getNetPaymentAmountOriginal() {
		return netPaymentAmountOriginal;
	}

	public void setNetPaymentAmountOriginal(BigDecimal netPaymentAmountOriginal) {
		this.netPaymentAmountOriginal = netPaymentAmountOriginal;
	}

	public BigDecimal getNetPaymentAmountConverted() {
		return netPaymentAmountConverted;
	}

	public void setNetPaymentAmountConverted(BigDecimal netPaymentAmountConverted) {
		this.netPaymentAmountConverted = netPaymentAmountConverted;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}

	/**
	 */
	public Payroll getPayroll() {
		return payroll;
	}

	
	public PaymentOrder getPaymentOrder() {
		return paymentOrder;
	}

	public void setPaymentOrder(PaymentOrder paymentOrder) {
		this.paymentOrder = paymentOrder;
	}

	/**
	 */
	public PayrollPaymentDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PayrollPaymentDetails that) {
		setId(that.getId());
		setPaymentRate(that.getPaymentRate());
		setNetPaymentAmountOriginal(that.getNetPaymentAmountOriginal());
		setNetPaymentAmountConverted(that.getNetPaymentAmountConverted());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setPayroll(that.getPayroll());
		setPaymentOrder(that.getPaymentOrder());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("netPaymentAmountOriginal=[").append(netPaymentAmountOriginal).append("] ");
		buffer.append("netPaymentAmountConverted=[").append(netPaymentAmountConverted).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PayrollPaymentDetails))
			return false;
		PayrollPaymentDetails equalCheck = (PayrollPaymentDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
