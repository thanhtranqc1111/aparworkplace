
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllUserAccounts", query = "select myUserAccount from UserAccount myUserAccount"),
		@NamedQuery(name = "findUserAccountByCreatedDate", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.createdDate = ?1"),
		@NamedQuery(name = "findUserAccountByEmail", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.email = ?1"),
		@NamedQuery(name = "findUserAccountByEmailContaining", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.email like ?1"),
		@NamedQuery(name = "findUserAccountByFirstName", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.firstName = ?1"),
		@NamedQuery(name = "findUserAccountByFirstNameContaining", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.firstName like ?1"),
		@NamedQuery(name = "findUserAccountById", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.id = ?1"),
		@NamedQuery(name = "findUserAccountByIsActive", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.isActive = ?1"),
		@NamedQuery(name = "findUserAccountByLastName", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.lastName = ?1"),
		@NamedQuery(name = "findUserAccountByLastNameContaining", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.lastName like ?1"),
		@NamedQuery(name = "findUserAccountByModifiedDate", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.modifiedDate = ?1"),
		@NamedQuery(name = "findUserAccountByPassword", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.password = ?1"),
		@NamedQuery(name = "findUserAccountByPasswordContaining", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.password like ?1"),
		@NamedQuery(name = "findUserAccountByPhone", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.phone = ?1"),
		@NamedQuery(name = "findUserAccountByPhoneContaining", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.phone like ?1"),
		@NamedQuery(name = "findUserAccountByPrimaryKey", query = "select myUserAccount from UserAccount myUserAccount where myUserAccount.id = ?1"),
		@NamedQuery(name = "findUserAccountManagerPaging", query =  "SELECT myUserAccount "
														   + "FROM UserAccount myUserAccount "
														   + "LEFT OUTER JOIN myUserAccount.subsidiaryUserDetails subsidiaryUserDetails "
														   + "WHERE (SELECT count(1) FROM RolePermission rp WHERE (rp.permission.code = 'PMS-007' or rp.permission.code = 'PMS-008') and rp.role.id = myUserAccount.role.id) = 0 and subsidiaryUserDetails.subsidiary.id = ?1 "
														   + "and "
														   + "(lower(myUserAccount.firstName) like lower(CONCAT('%', ?2, '%')) or "
														   + "lower(myUserAccount.lastName) like lower(CONCAT('%', ?2, '%')) " 
														   + ") ORDER by myUserAccount.id desc"),
		@NamedQuery(name = "findUserAccountAdminPaging", query =  "SELECT myUserAccount "
														   + "FROM UserAccount myUserAccount "
														   + "WHERE "
														   + "(lower(myUserAccount.firstName) like lower(CONCAT('%', ?1, '%')) or "
														   + "lower(myUserAccount.lastName) like lower(CONCAT('%', ?1, '%')) " 
														   + ") ORDER by myUserAccount.id desc"),
		@NamedQuery(name = "countUserAccountManagerPaging",  query =  "SELECT count(1) "
														   + "FROM UserAccount myUserAccount "
														   + "LEFT OUTER JOIN myUserAccount.subsidiaryUserDetails subsidiaryUserDetails "
														   + "WHERE (SELECT count(1) FROM RolePermission rp WHERE (rp.permission.code = 'PMS-007' or rp.permission.code = 'PMS-008') and rp.role.id = myUserAccount.role.id) = 0 and subsidiaryUserDetails.subsidiary.id = ?1 "
														   + "and "
														   + "(lower(myUserAccount.firstName) like lower(CONCAT('%', ?2, '%')) or "
														   + "lower(myUserAccount.lastName) like lower(CONCAT('%', ?2, '%')))"),
		@NamedQuery(name = "countUserAccountAdminPaging",  query =  "SELECT count(1) "
														   + "FROM UserAccount myUserAccount "
														   + "WHERE "
														   + "(lower(myUserAccount.firstName) like lower(CONCAT('%', ?1, '%')) or "
														   + "lower(myUserAccount.lastName) like lower(CONCAT('%', ?1, '%')))")
})


@Table(schema = "cashflow", name = "user_account")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "UserAccount")
@XmlRootElement(namespace = "cashflow_test/com/ttv/cashflow/domain")
public class UserAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "email", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String email;
	/**
	 */

	@Column(name = "password")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String password;
	/**
	 */

	@Column(name = "first_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String firstName;
	/**
	 */

	@Column(name = "last_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String lastName;
	/**
	 */

	@Column(name = "phone", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String phone;
	/**
	 */

	@Column(name = "is_active")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isActive;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "role_id", referencedColumnName = "id") })
	@XmlTransient
	Role role;
	/**
	 */
	@OneToMany(mappedBy = "userAccount", cascade = { CascadeType.REMOVE }, fetch = FetchType.EAGER)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.SubsidiaryUserDetail> subsidiaryUserDetails;
	
	/**
	 */

	@Column(name = "is_using_2fa")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isUsing2fa;
	
	/**
	 */

	@Column(name = "secret_token", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String secretToken;
	
	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}
	/**
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 */
	public Boolean getIsActive() {
		return this.isActive;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 */
	public Role getRole() {
		return role;
	}

	/**
	 */
	public void setSubsidiaryUserDetails(Set<SubsidiaryUserDetail> subsidiaryUserDetails) {
		this.subsidiaryUserDetails = subsidiaryUserDetails;
	}

	/**
	 */
	public Set<SubsidiaryUserDetail> getSubsidiaryUserDetails() {
		if (subsidiaryUserDetails == null) {
			subsidiaryUserDetails = new java.util.LinkedHashSet<com.ttv.cashflow.domain.SubsidiaryUserDetail>();
		}
		return subsidiaryUserDetails;
	}
	
	/**
	 */
	public Boolean getIsUsing2fa() {
		return isUsing2fa;
	}
	
	/**
	 */
	public void setIsUsing2fa(Boolean isUsing2fa) {
		this.isUsing2fa = isUsing2fa;
	}
	
	/**
	 */
	public String getSecretToken() {
		return secretToken;
	}
	
	/**
	 */
	public void setSecretToken(String secretToken) {
		this.secretToken = secretToken;
	}

	/**
	 */
	public UserAccount() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(UserAccount that) {
		setId(that.getId());
		setEmail(that.getEmail());
		setPassword(that.getPassword());
		setFirstName(that.getFirstName());
		setLastName(that.getLastName());
		setPhone(that.getPhone());
		setIsActive(that.getIsActive());
		setIsUsing2fa(that.isUsing2fa);
		setSecretToken(that.getSecretToken());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setRole(that.getRole());
		setSubsidiaryUserDetails(new java.util.LinkedHashSet<com.ttv.cashflow.domain.SubsidiaryUserDetail>(that.getSubsidiaryUserDetails()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("email=[").append(email).append("] ");
		buffer.append("password=[").append(password).append("] ");
		buffer.append("firstName=[").append(firstName).append("] ");
		buffer.append("lastName=[").append(lastName).append("] ");
		buffer.append("phone=[").append(phone).append("] ");
		buffer.append("isActive=[").append(isActive).append("] ");
		buffer.append("isUsing2fa=[").append(isUsing2fa).append("] ");
		buffer.append("secretToken=[").append(secretToken).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof UserAccount))
			return false;
		UserAccount equalCheck = (UserAccount) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
