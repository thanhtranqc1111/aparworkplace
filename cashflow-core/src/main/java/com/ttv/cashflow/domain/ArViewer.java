
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllArViewers", query = "select myArViewer from ArViewer myArViewer"),
		@NamedQuery(name = "findArViewerByAccountName", query = "select myArViewer from ArViewer myArViewer where myArViewer.accountName = ?1"),
		@NamedQuery(name = "findArViewerByAccountNameContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.accountName like ?1"),
		@NamedQuery(name = "findArViewerByApprovalCode", query = "select myArViewer from ArViewer myArViewer where myArViewer.approvalCode = ?1"),
		@NamedQuery(name = "findArViewerByApprovalCodeContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.approvalCode like ?1"),
		@NamedQuery(name = "findArViewerByArInvoiceNo", query = "select myArViewer from ArViewer myArViewer where myArViewer.arInvoiceNo = ?1"),
		@NamedQuery(name = "findArViewerByArInvoiceNoContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.arInvoiceNo like ?1"),
		@NamedQuery(name = "findArViewerByClassName", query = "select myArViewer from ArViewer myArViewer where myArViewer.className = ?1"),
		@NamedQuery(name = "findArViewerByClassNameContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.className like ?1"),
		@NamedQuery(name = "findArViewerByDescription", query = "select myArViewer from ArViewer myArViewer where myArViewer.description = ?1"),
		@NamedQuery(name = "findArViewerByDescriptionContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.description like ?1"),
		@NamedQuery(name = "findArViewerByExcludeGstOriginalAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArViewerByFxRate", query = "select myArViewer from ArViewer myArViewer where myArViewer.fxRate = ?1"),
		@NamedQuery(name = "findArViewerByGstRate", query = "select myArViewer from ArViewer myArViewer where myArViewer.gstRate = ?1"),
		@NamedQuery(name = "findArViewerByGstType", query = "select myArViewer from ArViewer myArViewer where myArViewer.gstType = ?1"),
		@NamedQuery(name = "findArViewerByGstTypeContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.gstType like ?1"),
		@NamedQuery(name = "findArViewerById", query = "select myArViewer from ArViewer myArViewer where myArViewer.id = ?1"),
		@NamedQuery(name = "findArViewerByIncludeGstConvertedAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArViewerByIncludeGstOriginalAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArViewerByInvoiceNo", query = "select myArViewer from ArViewer myArViewer where myArViewer.invoiceNo = ?1"),
		@NamedQuery(name = "findArViewerByInvoiceNoContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.invoiceNo like ?1"),
		@NamedQuery(name = "findArViewerByMonth", query = "select myArViewer from ArViewer myArViewer where myArViewer.month = ?1"),
		@NamedQuery(name = "findArViewerByMonthAfter", query = "select myArViewer from ArViewer myArViewer where myArViewer.month > ?1"),
		@NamedQuery(name = "findArViewerByMonthBefore", query = "select myArViewer from ArViewer myArViewer where myArViewer.month < ?1"),
		@NamedQuery(name = "findArViewerByOriginalCurrencyCode", query = "select myArViewer from ArViewer myArViewer where myArViewer.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findArViewerByOriginalCurrencyCodeContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findArViewerByPayerName", query = "select myArViewer from ArViewer myArViewer where myArViewer.payerName = ?1"),
		@NamedQuery(name = "findArViewerByPayerNameContaining", query = "select myArViewer from ArViewer myArViewer where myArViewer.payerName like ?1"),
		@NamedQuery(name = "findArViewerByPrimaryKey", query = "select myArViewer from ArViewer myArViewer where myArViewer.id = ?1"),
		@NamedQuery(name = "findArViewerByReceiptOrderId", query = "select myArViewer from ArViewer myArViewer where myArViewer.receiptOrderId = ?1"),
		@NamedQuery(name = "findArViewerByReceiptRate", query = "select myArViewer from ArViewer myArViewer where myArViewer.receiptRate = ?1"),
		@NamedQuery(name = "findArViewerByRemainIncludeGstOriginalAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.remainIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArViewerBySettedIncludeGstConvertedAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.settedIncludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArViewerBySettedIncludeGstOriginalAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.settedIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArViewerByTransactionDate", query = "select myArViewer from ArViewer myArViewer where myArViewer.transactionDate = ?1"),
		@NamedQuery(name = "findArViewerByTransactionDateAfter", query = "select myArViewer from ArViewer myArViewer where myArViewer.transactionDate > ?1"),
		@NamedQuery(name = "findArViewerByTransactionDateBefore", query = "select myArViewer from ArViewer myArViewer where myArViewer.transactionDate < ?1"),
		@NamedQuery(name = "findArViewerByTransactionId", query = "select myArViewer from ArViewer myArViewer where myArViewer.transactionId = ?1"),
		@NamedQuery(name = "findArViewerByVarianceAmount", query = "select myArViewer from ArViewer myArViewer where myArViewer.varianceAmount = ?1") })

@Table(schema = "cashflow", name = "ar_ledger")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "temp/com/ttv/cashflow/domain", name = "ArViewer")

public class ArViewer implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "ar_invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String arInvoiceNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "payer_name", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payerName;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "original_currency_code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "gst_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Column(name = "account_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "class_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String className;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "receipt_order_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger receiptOrderId;
	/**
	 */

	@Column(name = "transaction_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger transactionId;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "transaction_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar transactionDate;
	/**
	 */

	@Column(name = "setted_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal settedIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "receipt_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal receiptRate;
	/**
	 */

	@Column(name = "setted_include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal settedIncludeGstConvertedAmount;
	/**
	 */

	@Column(name = "variance_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "remain_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal remainIncludeGstOriginalAmount;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}

	/**
	 */
	public String getArInvoiceNo() {
		return this.arInvoiceNo;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	/**
	 */
	public String getPayerName() {
		return this.payerName;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setReceiptOrderId(BigInteger receiptOrderId) {
		this.receiptOrderId = receiptOrderId;
	}

	/**
	 */
	public BigInteger getReceiptOrderId() {
		return this.receiptOrderId;
	}

	/**
	 */
	public void setTransactionId(BigInteger transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 */
	public BigInteger getTransactionId() {
		return this.transactionId;
	}

	/**
	 */
	public void setTransactionDate(Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 */
	public Calendar getTransactionDate() {
		return this.transactionDate;
	}

	/**
	 */
	public void setSettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount) {
		this.settedIncludeGstOriginalAmount = settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getSettedIncludeGstOriginalAmount() {
		return this.settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public void setReceiptRate(BigDecimal receiptRate) {
		this.receiptRate = receiptRate;
	}

	/**
	 */
	public BigDecimal getReceiptRate() {
		return this.receiptRate;
	}

	/**
	 */
	public void setSettedIncludeGstConvertedAmount(BigDecimal settedIncludeGstConvertedAmount) {
		this.settedIncludeGstConvertedAmount = settedIncludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getSettedIncludeGstConvertedAmount() {
		return this.settedIncludeGstConvertedAmount;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setRemainIncludeGstOriginalAmount(BigDecimal remainIncludeGstOriginalAmount) {
		this.remainIncludeGstOriginalAmount = remainIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getRemainIncludeGstOriginalAmount() {
		return this.remainIncludeGstOriginalAmount;
	}

	/**
	 */
	public ArViewer() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArViewer that) {
		setId(that.getId());
		setArInvoiceNo(that.getArInvoiceNo());
		setMonth(that.getMonth());
		setPayerName(that.getPayerName());
		setInvoiceNo(that.getInvoiceNo());
		setApprovalCode(that.getApprovalCode());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setFxRate(that.getFxRate());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setAccountName(that.getAccountName());
		setClassName(that.getClassName());
		setDescription(that.getDescription());
		setReceiptOrderId(that.getReceiptOrderId());
		setTransactionId(that.getTransactionId());
		setTransactionDate(that.getTransactionDate());
		setSettedIncludeGstOriginalAmount(that.getSettedIncludeGstOriginalAmount());
		setReceiptRate(that.getReceiptRate());
		setSettedIncludeGstConvertedAmount(that.getSettedIncludeGstConvertedAmount());
		setVarianceAmount(that.getVarianceAmount());
		setRemainIncludeGstOriginalAmount(that.getRemainIncludeGstOriginalAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("arInvoiceNo=[").append(arInvoiceNo).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("payerName=[").append(payerName).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("className=[").append(className).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("receiptOrderId=[").append(receiptOrderId).append("] ");
		buffer.append("transactionId=[").append(transactionId).append("] ");
		buffer.append("transactionDate=[").append(transactionDate).append("] ");
		buffer.append("settedIncludeGstOriginalAmount=[").append(settedIncludeGstOriginalAmount).append("] ");
		buffer.append("receiptRate=[").append(receiptRate).append("] ");
		buffer.append("settedIncludeGstConvertedAmount=[").append(settedIncludeGstConvertedAmount).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("remainIncludeGstOriginalAmount=[").append(remainIncludeGstOriginalAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArViewer))
			return false;
		ArViewer equalCheck = (ArViewer) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
