
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;
import java.math.BigInteger;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllInvoicePaymentOrderTemps", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp"),
		@NamedQuery(name = "findInvoicePaymentOrderTempByApInvoiceId", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp where myInvoicePaymentOrderTemp.apInvoiceId = ?1"),
		@NamedQuery(name = "findInvoicePaymentOrderTempById", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp where myInvoicePaymentOrderTemp.id = ?1"),
		@NamedQuery(name = "findInvoicePaymentOrderTempByPaymentOrderNo", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp where myInvoicePaymentOrderTemp.paymentOrderNo = ?1"),
		@NamedQuery(name = "findInvoicePaymentOrderTempByPaymentOrderNoContaining", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp where myInvoicePaymentOrderTemp.paymentOrderNo like ?1"),
		@NamedQuery(name = "findInvoicePaymentOrderTempByPrimaryKey", query = "select myInvoicePaymentOrderTemp from InvoicePaymentOrderTemp myInvoicePaymentOrderTemp where myInvoicePaymentOrderTemp.id = ?1") })

@Table(schema = "cashflow", name = "invoice_payment_order_temp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "InvoicePaymentOrderTemp")

public class InvoicePaymentOrderTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "ap_invoice_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger apInvoiceId;
	/**
	 */

	@Column(name = "payment_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setApInvoiceId(BigInteger apInvoiceId) {
		this.apInvoiceId = apInvoiceId;
	}

	/**
	 */
	public BigInteger getApInvoiceId() {
		return this.apInvoiceId;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public InvoicePaymentOrderTemp() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(InvoicePaymentOrderTemp that) {
		setId(that.getId());
		setApInvoiceId(that.getApInvoiceId());
		setPaymentOrderNo(that.getPaymentOrderNo());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("apInvoiceId=[").append(apInvoiceId).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof InvoicePaymentOrderTemp))
			return false;
		InvoicePaymentOrderTemp equalCheck = (InvoicePaymentOrderTemp) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
