
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;
import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.*;

import javax.xml.bind.annotation.*;

import org.hibernate.type.SetType;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllGstMasters", query = "select myGstMaster from GstMaster myGstMaster"),
		@NamedQuery(name = "findGstMasterByCode", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.code = ?1"),
		@NamedQuery(name = "findGstMasterByCodeContaining", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.code like ?1"),
		@NamedQuery(name = "findGstMasterByName", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.type = ?1"),
		@NamedQuery(name = "findGstMasterByNameContaining", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.type like ?1"),
		@NamedQuery(name = "findGstMasterByPrimaryKey", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.code = ?1"),
		@NamedQuery(name = "findGstMasterByRate", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.rate = ?1"),
		@NamedQuery(name = "findGstMasterByRateContaining", query = "select myGstMaster from GstMaster myGstMaster where myGstMaster.rate like ?1"),
		@NamedQuery(name = "countGstMasterAll", query = "select count(1) from GstMaster"),
		@NamedQuery(name = "countGstMasterPaging", query = "select count(1) from GstMaster where lower(code) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(type) like lower(CONCAT('%', ?1, '%'))")
})

@Table(schema = "cashflow", name = "gst_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "GstMaster")

public class GstMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "type", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String type;
	/**
	 */

	@Column(name = "rate", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal rate;

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	/**
	 */
	public GstMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(GstMaster that) {
		setCode(that.getCode());
		setType(that.getType());
		setRate(that.getRate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();	

		buffer.append("code=[").append(code).append("] ");
		buffer.append("type=[").append(type).append("] ");
		buffer.append("rate=[").append(rate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((code == null) ? 0 : code.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof GstMaster))
			return false;
		GstMaster equalCheck = (GstMaster) obj;
		if ((code == null && equalCheck.code != null) || (code != null && equalCheck.code == null))
			return false;
		if (code != null && !code.equals(equalCheck.code))
			return false;
		return true;
	}
}
