
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoicePaymentDetailss", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByCreatedDate", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.createdDate = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsById", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.id = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByModifiedDate", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPayeeBankAccNo", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.payeeBankAccNo = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPayeeBankAccNoContaining", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.payeeBankAccNo like ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPayeeBankName", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.payeeBankName = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPayeeBankNameContaining", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.payeeBankName like ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPaymentIncludeGstOriginalAmount", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.paymentIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPaymentIncludeGstConvertedAmount", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.paymentIncludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPaymentRate", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.paymentRate = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPrimaryKey", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.id = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByVarianceAccountCode", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.varianceAccountCode = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByVarianceAccountCodeContaining", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.varianceAccountCode like ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByVarianceAmount", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.varianceAmount = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByPaymentId", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.paymentOrder.id = ?1"),
		@NamedQuery(name = "findApInvoicePaymentDetailsByApInvoiceId", query = "select myApInvoicePaymentDetails from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.apInvoice.id = ?1"),
		@NamedQuery(name = "findTotalOriginalAmountByApInvoiceId", query = "select sum(myApInvoicePaymentDetails.paymentIncludeGstOriginalAmount) from ApInvoicePaymentDetails myApInvoicePaymentDetails where myApInvoicePaymentDetails.paymentOrder.id != ?1 and myApInvoicePaymentDetails.apInvoice.id = ?2")
})

@Table(schema = "cashflow", name = "ap_invoice_payment_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoicePaymentDetails")

public class ApInvoicePaymentDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payment_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */

	@Column(name = "payment_include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentIncludeGstConvertedAmount;
	/**
	 */

	@Column(name = "payee_bank_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeBankName;
	/**
	 */

	@Column(name = "payee_bank_acc_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeBankAccNo;
	/**
	 */

	@Column(name = "variance_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "variance_account_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String varianceAccountCode;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "ap_invoice_id", referencedColumnName = "id") })
	@XmlTransient
	ApInvoice apInvoice;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "payment_id", referencedColumnName = "id") })
	@XmlTransient
	PaymentOrder paymentOrder;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	public BigDecimal getPaymentIncludeGstOriginalAmount() {
		return paymentIncludeGstOriginalAmount;
	}

	public void setPaymentIncludeGstOriginalAmount(BigDecimal paymentIncludeGstOriginalAmount) {
		this.paymentIncludeGstOriginalAmount = paymentIncludeGstOriginalAmount;
	}

	public BigDecimal getPaymentIncludeGstConvertedAmount() {
		return paymentIncludeGstConvertedAmount;
	}

	public void setPaymentIncludeGstConvertedAmount(BigDecimal paymentIncludeGstConvertedAmount) {
		this.paymentIncludeGstConvertedAmount = paymentIncludeGstConvertedAmount;
	}

	/**
	 */
	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public BigDecimal getPaymentRate() {
		return this.paymentRate;
	}

	/**
	 */
	public void setPayeeBankName(String payeeBankName) {
		this.payeeBankName = payeeBankName;
	}

	/**
	 */
	public String getPayeeBankName() {
		return this.payeeBankName;
	}

	/**
	 */
	public void setPayeeBankAccNo(String payeeBankAccNo) {
		this.payeeBankAccNo = payeeBankAccNo;
	}

	/**
	 */
	public String getPayeeBankAccNo() {
		return this.payeeBankAccNo;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setVarianceAccountCode(String varianceAccountCode) {
		this.varianceAccountCode = varianceAccountCode;
	}

	/**
	 */
	public String getVarianceAccountCode() {
		return this.varianceAccountCode;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setApInvoice(ApInvoice apInvoice) {
		this.apInvoice = apInvoice;
	}

	/**
	 */
	@JsonIgnore
	public ApInvoice getApInvoice() {
		return apInvoice;
	}

	/**
	 */
	public void setPaymentOrder(PaymentOrder paymentOrder) {
		this.paymentOrder = paymentOrder;
	}

	/**
	 */
	@JsonIgnore
	public PaymentOrder getPaymentOrder() {
		return paymentOrder;
	}

	/**
	 */
	public ApInvoicePaymentDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoicePaymentDetails that) {
		setId(that.getId());
		setPaymentIncludeGstConvertedAmount(that.getPaymentIncludeGstConvertedAmount());
		setPaymentIncludeGstOriginalAmount(that.getPaymentIncludeGstOriginalAmount());
		setPaymentRate(that.getPaymentRate());
		setPayeeBankName(that.getPayeeBankName());
		setPayeeBankAccNo(that.getPayeeBankAccNo());
		setVarianceAmount(that.getVarianceAmount());
		setVarianceAccountCode(that.getVarianceAccountCode());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setApInvoice(that.getApInvoice());
		setPaymentOrder(that.getPaymentOrder());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("paymentIncludeGstConvertedAmount=[").append(paymentIncludeGstConvertedAmount).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("paymentIncludeGstOriginalAmount=[").append(paymentIncludeGstOriginalAmount).append("] ");
		buffer.append("payeeBankName=[").append(payeeBankName).append("] ");
		buffer.append("payeeBankAccNo=[").append(payeeBankAccNo).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("varianceAccountCode=[").append(varianceAccountCode).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApInvoicePaymentDetails))
			return false;
		ApInvoicePaymentDetails equalCheck = (ApInvoicePaymentDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
