
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Calendar;
import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllRolePermissions", query = "select myRolePermission from RolePermission myRolePermission"),
		@NamedQuery(name = "findRolePermissionByCreatedDate", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.createdDate = ?1"),
		@NamedQuery(name = "findRolePermissionById", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.id = ?1"),
		@NamedQuery(name = "findRolePermissionByModifiedDate", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.modifiedDate = ?1"),
		@NamedQuery(name = "findRolePermissionByPrimaryKey", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.id = ?1"),
		@NamedQuery(name = "findRolePermissionByRoleIdAndPermissionId", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.role.id = ?1 and myRolePermission.permission.id = ?2"),
		@NamedQuery(name = "findRolePermissionByRoleId", query = "select myRolePermission from RolePermission myRolePermission where myRolePermission.role.id = ?1")
})

@Table(schema = "cashflow", name = "role_permission")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "RolePermission")

public class RolePermission implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "permission_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	Permission permission;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	Role role;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	/**
	 */
	public Permission getPermission() {
		return permission;
	}

	/**
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 */
	public Role getRole() {
		return role;
	}

	/**
	 */
	public RolePermission() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(RolePermission that) {
		setId(that.getId());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setPermission(that.getPermission());
		setRole(that.getRole());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof RolePermission))
			return false;
		RolePermission equalCheck = (RolePermission) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
