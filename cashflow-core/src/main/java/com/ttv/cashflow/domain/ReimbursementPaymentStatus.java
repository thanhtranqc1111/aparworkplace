
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllReimbursementPaymentStatuss", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus"),
		@NamedQuery(name = "findReimbursementPaymentStatusById", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus where myReimbursementPaymentStatus.id = ?1"),
		@NamedQuery(name = "findReimbursementPaymentStatusByPaidReimbursementOriginalPayment", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus where myReimbursementPaymentStatus.paidReimbursementOriginalPayment = ?1"),
		@NamedQuery(name = "findReimbursementPaymentStatusByPrimaryKey", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus where myReimbursementPaymentStatus.id = ?1"),
		@NamedQuery(name = "findReimbursementPaymentStatusByReimbursementId", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus where myReimbursementPaymentStatus.reimbursementId = ?1"),
		@NamedQuery(name = "findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment", query = "select myReimbursementPaymentStatus from ReimbursementPaymentStatus myReimbursementPaymentStatus where myReimbursementPaymentStatus.unpaidReimbursementOriginalPayment = ?1") })

@Table(schema = "cashflow", name = "reimbursement_payment_status")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "first/com/ttv/cashflow/domain/domain", name = "ReimbursementPaymentStatus")

public class ReimbursementPaymentStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "reimbursement_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger reimbursementId;
	/**
	 */

	@Column(name = "paid_reimbursement_original_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paidReimbursementOriginalPayment;
	/**
	 */

	@Column(name = "unpaid_reimbursement_original_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal unpaidReimbursementOriginalPayment;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}	

	public BigInteger getReimbursementId() {
		return reimbursementId;
	}

	public void setReimbursementId(BigInteger reimbursementId) {
		this.reimbursementId = reimbursementId;
	}

	/**
	 */
	public void setPaidReimbursementOriginalPayment(BigDecimal paidReimbursementOriginalPayment) {
		this.paidReimbursementOriginalPayment = paidReimbursementOriginalPayment;
	}

	/**
	 */
	public BigDecimal getPaidReimbursementOriginalPayment() {
		return this.paidReimbursementOriginalPayment;
	}

	/**
	 */
	public void setUnpaidReimbursementOriginalPayment(BigDecimal unpaidReimbursementOriginalPayment) {
		this.unpaidReimbursementOriginalPayment = unpaidReimbursementOriginalPayment;
	}

	/**
	 */
	public BigDecimal getUnpaidReimbursementOriginalPayment() {
		return this.unpaidReimbursementOriginalPayment;
	}

	/**
	 */
	public ReimbursementPaymentStatus() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ReimbursementPaymentStatus that) {
		setId(that.getId());
		setReimbursementId(that.getReimbursementId());
		setPaidReimbursementOriginalPayment(that.getPaidReimbursementOriginalPayment());
		setUnpaidReimbursementOriginalPayment(that.getUnpaidReimbursementOriginalPayment());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("reimbursementId=[").append(reimbursementId).append("] ");
		buffer.append("paidReimbursementOriginalPayment=[").append(paidReimbursementOriginalPayment).append("] ");
		buffer.append("unpaidReimbursementOriginalPayment=[").append(unpaidReimbursementOriginalPayment).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ReimbursementPaymentStatus))
			return false;
		ReimbursementPaymentStatus equalCheck = (ReimbursementPaymentStatus) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
