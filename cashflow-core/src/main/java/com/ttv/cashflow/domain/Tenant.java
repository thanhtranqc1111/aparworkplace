
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllTenants", query = "select myTenant from Tenant myTenant"),
		@NamedQuery(name = "findTenantByCode", query = "select myTenant from Tenant myTenant where myTenant.code = ?1"),
		@NamedQuery(name = "findTenantByCodeContaining", query = "select myTenant from Tenant myTenant where myTenant.code like ?1"),
		@NamedQuery(name = "findTenantByDbDriverClassName", query = "select myTenant from Tenant myTenant where myTenant.dbDriverClassName = ?1"),
		@NamedQuery(name = "findTenantByDbDriverClassNameContaining", query = "select myTenant from Tenant myTenant where myTenant.dbDriverClassName like ?1"),
		@NamedQuery(name = "findTenantByDbName", query = "select myTenant from Tenant myTenant where myTenant.dbName = ?1"),
		@NamedQuery(name = "findTenantByDbNameContaining", query = "select myTenant from Tenant myTenant where myTenant.dbName like ?1"),
		@NamedQuery(name = "findTenantByDbPassword", query = "select myTenant from Tenant myTenant where myTenant.dbPassword = ?1"),
		@NamedQuery(name = "findTenantByDbPasswordContaining", query = "select myTenant from Tenant myTenant where myTenant.dbPassword like ?1"),
		@NamedQuery(name = "findTenantByDbUrl", query = "select myTenant from Tenant myTenant where myTenant.dbUrl = ?1"),
		@NamedQuery(name = "findTenantByDbUrlContaining", query = "select myTenant from Tenant myTenant where myTenant.dbUrl like ?1"),
		@NamedQuery(name = "findTenantByDbUsername", query = "select myTenant from Tenant myTenant where myTenant.dbUsername = ?1"),
		@NamedQuery(name = "findTenantByDbUsernameContaining", query = "select myTenant from Tenant myTenant where myTenant.dbUsername like ?1"),
		@NamedQuery(name = "findTenantById", query = "select myTenant from Tenant myTenant where myTenant.id = ?1"),
		@NamedQuery(name = "findTenantByPrimaryKey", query = "select myTenant from Tenant myTenant where myTenant.id = ?1") })

@Table(schema = "cashflow", name = "tenant")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "Tenant")

public class Tenant implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String code;
	/**
	 */

	@Column(name = "db_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String dbName;
	/**
	 */

	@Column(name = "db_url", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String dbUrl;
	/**
	 */

	@Column(name = "db_username", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String dbUsername;
	/**
	 */

	@Column(name = "db_password", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String dbPassword;
	/**
	 */

	@Column(name = "db_driver_class_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String dbDriverClassName;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 */
	public String getDbName() {
		return this.dbName;
	}

	/**
	 */
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	/**
	 */
	public String getDbUrl() {
		return this.dbUrl;
	}

	/**
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	/**
	 */
	public String getDbUsername() {
		return this.dbUsername;
	}

	/**
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	/**
	 */
	public String getDbPassword() {
		return this.dbPassword;
	}

	/**
	 */
	public void setDbDriverClassName(String dbDriverClassName) {
		this.dbDriverClassName = dbDriverClassName;
	}

	/**
	 */
	public String getDbDriverClassName() {
		return this.dbDriverClassName;
	}

	/**
	 */
	public Tenant() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Tenant that) {
		setId(that.getId());
		setCode(that.getCode());
		setDbName(that.getDbName());
		setDbUrl(that.getDbUrl());
		setDbUsername(that.getDbUsername());
		setDbPassword(that.getDbPassword());
		setDbDriverClassName(that.getDbDriverClassName());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("dbName=[").append(dbName).append("] ");
		buffer.append("dbUrl=[").append(dbUrl).append("] ");
		buffer.append("dbUsername=[").append(dbUsername).append("] ");
		buffer.append("dbPassword=[").append(dbPassword).append("] ");
		buffer.append("dbDriverClassName=[").append(dbDriverClassName).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Tenant))
			return false;
		Tenant equalCheck = (Tenant) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
