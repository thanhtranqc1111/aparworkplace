
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllTenantScriptInitializations", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization"),
		@NamedQuery(name = "findTenantScriptInitializationByCode", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.code = ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByCodeContaining", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.code like ?1"),
		@NamedQuery(name = "findTenantScriptInitializationById", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.id = ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByName", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.name = ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByNameContaining", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.name like ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByPrimaryKey", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.id = ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByValue", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.value = ?1"),
		@NamedQuery(name = "findTenantScriptInitializationByValueContaining", query = "select myTenantScriptInitialization from TenantScriptInitialization myTenantScriptInitialization where myTenantScriptInitialization.value like ?1") })

@Table(schema = "cashflow", name = "tenant_script_initialization")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "TenantScriptInitialization")

public class TenantScriptInitialization implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String code;
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "value")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String value;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 */
	public TenantScriptInitialization() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(TenantScriptInitialization that) {
		setId(that.getId());
		setCode(that.getCode());
		setName(that.getName());
		setValue(that.getValue());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("value=[").append(value).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof TenantScriptInitialization))
			return false;
		TenantScriptInitialization equalCheck = (TenantScriptInitialization) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
