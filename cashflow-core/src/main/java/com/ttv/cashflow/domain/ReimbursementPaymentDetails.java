
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllReimbursementPaymentDetailss", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByCreatedDate", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.createdDate = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsById", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.id = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByModifiedDate", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByPaymentConvertedAmount", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.paymentConvertedAmount = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByPaymentOriginalAmount", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.paymentOriginalAmount = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByPaymentRate", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.paymentRate = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByPrimaryKey", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.id = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByVarianceAccountCode", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.varianceAccountCode = ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByVarianceAccountCodeContaining", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.varianceAccountCode like ?1"),
		@NamedQuery(name = "findReimbursementPaymentDetailsByVarianceAmount", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.varianceAmount = ?1"), 
		@NamedQuery(name = "findTotalReimAmountOriginalByReimId", query = "select sum(myReimbursementPaymentDetails.paymentOriginalAmount) from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.paymentOrder.id != ?1 and myReimbursementPaymentDetails.reimbursement.id = ?2"),
		@NamedQuery(name = "findReimbursemenPaymentDetailsByPaymentId", query = "select myReimbursementPaymentDetails from ReimbursementPaymentDetails myReimbursementPaymentDetails where myReimbursementPaymentDetails.paymentOrder.id = ?1"),
})




@Table(schema = "cashflow", name = "reimbursement_payment_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "first/com/ttv/cashflow/domain/domain", name = "ReimbursementPaymentDetails")

public class ReimbursementPaymentDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payment_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentOriginalAmount;
	/**
	 */

	@Column(name = "payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */

	@Column(name = "payment_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentConvertedAmount;
	/**
	 */

	@Column(name = "variance_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "variance_account_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String varianceAccountCode;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "reimbursement_id", referencedColumnName = "id") })
	@XmlTransient
	Reimbursement reimbursement;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "payment_id", referencedColumnName = "id") })
	@XmlTransient
	PaymentOrder paymentOrder;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPaymentOriginalAmount(BigDecimal paymentOriginalAmount) {
		this.paymentOriginalAmount = paymentOriginalAmount;
	}

	/**
	 */
	public BigDecimal getPaymentOriginalAmount() {
		return this.paymentOriginalAmount;
	}

	/**
	 */
	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public BigDecimal getPaymentRate() {
		return this.paymentRate;
	}

	/**
	 */
	public void setPaymentConvertedAmount(BigDecimal paymentConvertedAmount) {
		this.paymentConvertedAmount = paymentConvertedAmount;
	}

	/**
	 */
	public BigDecimal getPaymentConvertedAmount() {
		return this.paymentConvertedAmount;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setVarianceAccountCode(String varianceAccountCode) {
		this.varianceAccountCode = varianceAccountCode;
	}

	/**
	 */
	public String getVarianceAccountCode() {
		return this.varianceAccountCode;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setReimbursement(Reimbursement reimbursement) {
		this.reimbursement = reimbursement;
	}

	/**
	 */
	public Reimbursement getReimbursement() {
		return reimbursement;
	}

	/**
	 */
	public void setPaymentOrder(PaymentOrder paymentOrder) {
		this.paymentOrder = paymentOrder;
	}

	/**
	 */
	public PaymentOrder getPaymentOrder() {
		return paymentOrder;
	}

	/**
	 */
	public ReimbursementPaymentDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ReimbursementPaymentDetails that) {
		setId(that.getId());
		setPaymentOriginalAmount(that.getPaymentOriginalAmount());
		setPaymentRate(that.getPaymentRate());
		setPaymentConvertedAmount(that.getPaymentConvertedAmount());
		setVarianceAmount(that.getVarianceAmount());
		setVarianceAccountCode(that.getVarianceAccountCode());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setReimbursement(that.getReimbursement());
		setPaymentOrder(that.getPaymentOrder());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("paymentOriginalAmount=[").append(paymentOriginalAmount).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("paymentConvertedAmount=[").append(paymentConvertedAmount).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("varianceAccountCode=[").append(varianceAccountCode).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ReimbursementPaymentDetails))
			return false;
		ReimbursementPaymentDetails equalCheck = (ReimbursementPaymentDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
