
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayrollPaymentStatuss", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus"),
		@NamedQuery(name = "findPayrollPaymentStatusById", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus where myPayrollPaymentStatus.id = ?1"),
		@NamedQuery(name = "findPayrollPaymentStatusByPaidNetPayment", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus where myPayrollPaymentStatus.paidNetPayment = ?1"),
		@NamedQuery(name = "findPayrollPaymentStatusByPayrollId", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus where myPayrollPaymentStatus.payrollId = ?1"),
		@NamedQuery(name = "findPayrollPaymentStatusByPrimaryKey", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus where myPayrollPaymentStatus.id = ?1"),
		@NamedQuery(name = "findPayrollPaymentStatusByUnpaidNetPayment", query = "select myPayrollPaymentStatus from PayrollPaymentStatus myPayrollPaymentStatus where myPayrollPaymentStatus.unpaidNetPayment = ?1") })

@Table(schema = "cashflow", name = "payroll_payment_status")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "PayrollPaymentStatus")

public class PayrollPaymentStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payroll_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger payrollId;
	/**
	 */

	@Column(name = "paid_net_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paidNetPayment;
	/**
	 */

	@Column(name = "unpaid_net_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal unpaidNetPayment;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	public BigInteger getPayrollId() {
		return payrollId;
	}

	public void setPayrollId(BigInteger payrollId) {
		this.payrollId = payrollId;
	}

	/**
	 */
	public void setPaidNetPayment(BigDecimal paidNetPayment) {
		this.paidNetPayment = paidNetPayment;
	}

	/**
	 */
	public BigDecimal getPaidNetPayment() {
		return this.paidNetPayment;
	}

	/**
	 */
	public void setUnpaidNetPayment(BigDecimal unpaidNetPayment) {
		this.unpaidNetPayment = unpaidNetPayment;
	}

	/**
	 */
	public BigDecimal getUnpaidNetPayment() {
		return this.unpaidNetPayment;
	}

	/**
	 */
	public PayrollPaymentStatus() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PayrollPaymentStatus that) {
		setId(that.getId());
		setPayrollId(that.getPayrollId());
		setPaidNetPayment(that.getPaidNetPayment());
		setUnpaidNetPayment(that.getUnpaidNetPayment());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payrollId=[").append(payrollId).append("] ");
		buffer.append("paidNetPayment=[").append(paidNetPayment).append("] ");
		buffer.append("unpaidNetPayment=[").append(unpaidNetPayment).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PayrollPaymentStatus))
			return false;
		PayrollPaymentStatus equalCheck = (PayrollPaymentStatus) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
