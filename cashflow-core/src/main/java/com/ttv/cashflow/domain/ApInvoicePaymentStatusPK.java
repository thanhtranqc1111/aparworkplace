
package com.ttv.cashflow.domain;

import java.io.Serializable;

import javax.persistence.Id;

import javax.persistence.*;

/**
 */
public class ApInvoicePaymentStatusPK implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */
	public ApInvoicePaymentStatusPK() {
	}

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	public Integer id;
	/**
	 */

	@Column(name = "ap_invoice_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	public Integer apInvoiceId;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setApInvoiceId(Integer apInvoiceId) {
		this.apInvoiceId = apInvoiceId;
	}

	/**
	 */
	public Integer getApInvoiceId() {
		return this.apInvoiceId;
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		result = (int) (prime * result + ((apInvoiceId == null) ? 0 : apInvoiceId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApInvoicePaymentStatusPK))
			return false;
		ApInvoicePaymentStatusPK equalCheck = (ApInvoicePaymentStatusPK) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		if ((apInvoiceId == null && equalCheck.apInvoiceId != null) || (apInvoiceId != null && equalCheck.apInvoiceId == null))
			return false;
		if (apInvoiceId != null && !apInvoiceId.equals(equalCheck.apInvoiceId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ApInvoicePaymentStatusPK");
		sb.append(" id: ").append(getId());
		sb.append(" apInvoiceId: ").append(getApInvoiceId());
		return sb.toString();
	}
}
