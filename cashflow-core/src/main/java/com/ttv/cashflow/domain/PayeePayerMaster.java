
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllPayeePayerMasters", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster"),
        @NamedQuery(name = "findPayeePayerMasterByAccountCode", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.accountCode = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByAccountCodeContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.accountCode like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByAddress", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.address = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByAddressContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.address like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByBankAccountCode", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.bankAccountCode = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByBankAccountCodeContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.bankAccountCode like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByBankName", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.bankName = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByBankNameContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.bankName like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByCode", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.code = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByCodeContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.code like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByName", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.name = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByNameContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.name like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByPaymentTerm", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.paymentTerm = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByPaymentTermContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.paymentTerm like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByPhone", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.phone = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByPhoneContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.phone like ?1"),
        @NamedQuery(name = "findPayeePayerMasterByPrimaryKey", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.code = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByTypeCode", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.typeCode = ?1"),
        @NamedQuery(name = "findPayeePayerMasterByTypeCodeContaining", query = "select myPayeePayerMaster from PayeePayerMaster myPayeePayerMaster where myPayeePayerMaster.typeCode like ?1"),
        
        
        @NamedQuery(name = "findPayeePayerMasterByAccountAndNameContaining", query = "select payee, payeeType  from PayeePayerMaster payee,"
                + " PayeePayerTypeMaster payeeType where (payee.typeCode = payeeType.code) and (lower(payee.code) like lower(?1) or lower(payee.name) like lower(?1))"),
        
        
        @NamedQuery(name = "findPayeeMasterPaging", query = "select myPayeePayerMaster, payeeTpye.name as typeName, bankAccount.bankAccountNo as bankAccountNumber, accountMaster.name as accountName "
                + "from PayeePayerMaster myPayeePayerMaster, PayeePayerTypeMaster payeeTpye, BankAccountMaster bankAccount, AccountMaster accountMaster "
                + "where myPayeePayerMaster.typeCode = payeeTpye.code and bankAccount.code = myPayeePayerMaster.bankAccountCode "
                + "and accountMaster.code = myPayeePayerMaster.accountCode and "
                + "(lower(myPayeePayerMaster.code) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.name) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.address) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.phone) like lower(CONCAT('%', ?1, '%')))"
                ),
        @NamedQuery(name = "findPayeeMasterPaging2", query = "select myPayeePayerMaster, payeeTpye.name as typeName, accountMaster.name as accountName "
                + "from PayeePayerMaster myPayeePayerMaster, PayeePayerTypeMaster payeeTpye, AccountMaster accountMaster "
                + "where myPayeePayerMaster.typeCode = payeeTpye.code "
                + "and accountMaster.code = myPayeePayerMaster.accountCode and "
                + "(lower(myPayeePayerMaster.code) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.name) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.address) like lower(CONCAT('%', ?1, '%')) or "
                + "lower(myPayeePayerMaster.phone) like lower(CONCAT('%', ?1, '%')))"
                )
       })

@Table(schema = "cashflow", name = "payee_payer_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "PayeePayerMaster")

public class PayeePayerMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     */

    @Column(name = "code", length = 20, nullable = false)
    @Basic(fetch = FetchType.EAGER)

    @Id
    @XmlElement
    String code;
    /**
     */

    @Column(name = "name", length = 100)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String name;
    /**
     */

    @Column(name = "type_code", length = 20)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String typeCode;
    /**
     */

    @Column(name = "payment_term", length = 40)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String paymentTerm;
    /**
     */

    @Column(name = "account_code", length = 20)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String accountCode;
    /**
     */

    @Column(name = "bank_name")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String bankName;
    /**
     */

    @Column(name = "bank_account_code", length = 20)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String bankAccountCode;
    /**
     */

    @Column(name = "address")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String address;
    /**
     */

    @Column(name = "phone", length = 20)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String phone;
    
    /**
     */
    @Column(name = "default_currency", length = 3)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String defaultCurrency;

    /** The payee payer type master. */
    @Transient
    PayeePayerTypeMaster payeePayerTypeMaster;
    /**
     */

    @Transient
    BankAccountMaster bankAccountMaster;
    
    @Transient
    AccountMaster accountMaster;

    public void setCode(String code) {
        this.code = code;
    }

    /**
     */
    public String getCode() {
        return this.code;
    }

    /**
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     */
    public String getName() {
        return this.name;
    }

    /**
     */
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    /**
     */
    public String getTypeCode() {
        return this.typeCode;
    }

    /**
     */
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     */
    public String getPaymentTerm() {
        return this.paymentTerm;
    }

    /**
     */
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    /**
     */
    public String getAccountCode() {
        return this.accountCode;
    }

    /**
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     */
    public String getBankName() {
        return this.bankName;
    }

    /**
     */
    public void setBankAccountCode(String bankAccountCode) {
        this.bankAccountCode = bankAccountCode;
    }

    /**
     */
    public String getBankAccountCode() {
        return this.bankAccountCode;
    }

    /**
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     */
    public String getAddress() {
        return this.address;
    }

    /**
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     */
    public String getPhone() {
        return this.phone;
    }

    public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	/**
     * Gets the payee payer type master.
     *
     * @return the payee payer type master
     */
    public PayeePayerTypeMaster getPayeePayerTypeMaster() {
        return payeePayerTypeMaster;
    }

    /**
     * Sets the payee payer type master.
     *
     * @param payeePayerTypeMaster
     *            the new payee payer type master
     */
    public void setPayeePayerTypeMaster(
            PayeePayerTypeMaster payeePayerTypeMaster) {
        this.payeePayerTypeMaster = payeePayerTypeMaster;
    }
    
    public BankAccountMaster getBankAccountMaster() {
        return bankAccountMaster;
    }

    public void setBankAccountMaster(BankAccountMaster bankAccountMaster) {
        this.bankAccountMaster = bankAccountMaster;
    }

    public AccountMaster getAccountMaster() {
        return accountMaster;
    }

    public void setAccountMaster(AccountMaster accountMaster) {
        this.accountMaster = accountMaster;
    }
    /**
     */
    public PayeePayerMaster() {
    }

    /**
     * Copies the contents of the specified bean into this bean.
     *
     */
    public void copy(PayeePayerMaster that) {
        setCode(that.getCode());
        setName(that.getName());
        setTypeCode(that.getTypeCode());
        setPaymentTerm(that.getPaymentTerm());
        setAccountCode(that.getAccountCode());
        setBankName(that.getBankName());
        setBankAccountCode(that.getBankAccountCode());
        setAddress(that.getAddress());
        setPhone(that.getPhone());
        setDefaultCurrency(that.getDefaultCurrency());
    }

    /**
     * Returns a textual representation of a bean.
     *
     */
    public String toString() {

        StringBuilder buffer = new StringBuilder();

        buffer.append("code=[").append(code).append("] ");
        buffer.append("name=[").append(name).append("] ");
        buffer.append("typeCode=[").append(typeCode).append("] ");
        buffer.append("paymentTerm=[").append(paymentTerm).append("] ");
        buffer.append("accountCode=[").append(accountCode).append("] ");
        buffer.append("bankName=[").append(bankName).append("] ");
        buffer.append("bankAccountCode=[").append(bankAccountCode).append("] ");
        buffer.append("address=[").append(address).append("] ");
        buffer.append("phone=[").append(phone).append("] ");
        buffer.append("defaultCurrency=[").append(defaultCurrency).append("] ");

        return buffer.toString();
    }

    /**
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (int) (prime * result
                + ((code == null) ? 0 : code.hashCode()));
        return result;
    }

    /**
     */
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof PayeePayerMaster))
            return false;
        PayeePayerMaster equalCheck = (PayeePayerMaster) obj;
        if ((code == null && equalCheck.code != null)
                || (code != null && equalCheck.code == null))
            return false;
        if (code != null && !code.equals(equalCheck.code))
            return false;
        return true;
    }
}
