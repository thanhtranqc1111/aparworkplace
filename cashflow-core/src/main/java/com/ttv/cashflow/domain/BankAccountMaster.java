
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;
import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllBankAccountMasters", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster order by myBankAccountMaster.bankAccountNo asc"),
		@NamedQuery(name = "findBankAccountMasterByBankAccountNo", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.bankAccountNo = ?1"),
		@NamedQuery(name = "findBankAccountMasterByBankAccountNoContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.bankAccountNo like ?1"),
		@NamedQuery(name = "findBankAccountMasterByBankCode", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.bankCode = coalesce(cast(?1 as text), myBankAccountMaster.bankCode)"),
		@NamedQuery(name = "findBankAccountMasterByBankCodeContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.bankCode like ?1"),
		@NamedQuery(name = "findBankAccountMasterByCode", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.code = ?1"),
		@NamedQuery(name = "findBankAccountMasterByCodeContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.code like ?1"),
		@NamedQuery(name = "findBankAccountMasterByCurrencyCode", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.currencyCode = ?1"),
		@NamedQuery(name = "findBankAccountMasterByCurrencyCodeContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.currencyCode like ?1"),
		@NamedQuery(name = "findBankAccountMasterByName", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.name = ?1"),
		@NamedQuery(name = "findBankAccountMasterByNameContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.name like ?1"),
		@NamedQuery(name = "findBankAccountMasterByPrimaryKey", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.code = ?1"),
		@NamedQuery(name = "findBankAccountMasterByType", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.type = ?1"),
		@NamedQuery(name = "findBankAccountMasterByTypeContaining", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.type like ?1") ,
		@NamedQuery(name = "countBankAccountMasterAll", query = "select count(1) from BankAccountMaster"),
		@NamedQuery(name = "countBankAccountMasterPaging", query = "select count(1) from BankAccountMaster where lower(code) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(type) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(name) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(bankAccountNo) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(bankCode) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(currencyCode) like lower(CONCAT('%', ?1, '%'))"),
		@NamedQuery(name = "findBankAccountMasterByBankNameAndBankAccountNo", query = "select myBankAccountMaster from BankAccountMaster myBankAccountMaster where myBankAccountMaster.name = ?1 and myBankAccountMaster.bankAccountNo = ?2")
})

@Table(schema = "cashflow", name = "bank_account_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "BankAccountMaster")

public class BankAccountMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "type", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String type;
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "bank_account_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankAccountNo;
	/**
	 */

	@Column(name = "bank_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankCode;
	/**
	 */

	@Column(name = "currency_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currencyCode;

	/**
	 */
	@Column(name = "beginning_balance", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal beginningBalance;
	
	/**
	 */
	@Column(name = "ending_balance", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal endingBalance;
	
	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 */
	public String getType() {
		return this.type;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	/**
	 */
	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	/**
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 */
	public String getBankCode() {
		return this.bankCode;
	}

	/**
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 */
	public String getCurrencyCode() {
		return this.currencyCode;
	}
	
	public BigDecimal getBeginningBalance() {
		return beginningBalance;
	}

	public void setBeginningBalance(BigDecimal beginningBalance) {
		this.beginningBalance = beginningBalance;
	}

	public BigDecimal getEndingBalance() {
		return endingBalance;
	}

	public void setEndingBalance(BigDecimal endingBalance) {
		this.endingBalance = endingBalance;
	}

	/**
	 */
	public BankAccountMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(BankAccountMaster that) {
		setCode(that.getCode());
		setType(that.getType());
		setName(that.getName());
		setBankAccountNo(that.getBankAccountNo());
		setBankCode(that.getBankCode());
		setCurrencyCode(that.getCurrencyCode());
		setBeginningBalance(that.getBeginningBalance());
		setEndingBalance(that.endingBalance);
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("code=[").append(code).append("] ");
		buffer.append("type=[").append(type).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("bankAccountNo=[").append(bankAccountNo).append("] ");
		buffer.append("bankCode=[").append(bankCode).append("] ");
		buffer.append("currencyCode=[").append(currencyCode).append("] ");
		buffer.append("beginningBalance=[").append(beginningBalance).append("] ");
		buffer.append("endingBalance=[").append(endingBalance).append("] ");
		
		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((code == null) ? 0 : code.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof BankAccountMaster))
			return false;
		BankAccountMaster equalCheck = (BankAccountMaster) obj;
		if ((code == null && equalCheck.code != null) || (code != null && equalCheck.code == null))
			return false;
		if (code != null && !code.equals(equalCheck.code))
			return false;
		return true;
	}
}
