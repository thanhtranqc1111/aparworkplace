
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findArInvoiceReceiptStatusById", query = "select myArInvoiceReceiptStatus from ArInvoiceReceiptStatus myArInvoiceReceiptStatus where myArInvoiceReceiptStatus.id = ?1"),
		@NamedQuery(name = "findArInvoiceReceiptStatusByArInvoiceId", query = "select myArInvoiceReceiptStatus from ArInvoiceReceiptStatus myArInvoiceReceiptStatus where myArInvoiceReceiptStatus.arInvoiceId = ?1"),
		@NamedQuery(name = "findArInvoiceReceiptStatusByPrimaryKey", query = "select myArInvoiceReceiptStatus from ArInvoiceReceiptStatus myArInvoiceReceiptStatus where myArInvoiceReceiptStatus.id = ?1"),
		@NamedQuery(name = "findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount", query = "select myArInvoiceReceiptStatus from ArInvoiceReceiptStatus myArInvoiceReceiptStatus where myArInvoiceReceiptStatus.settedIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount", query = "select myArInvoiceReceiptStatus from ArInvoiceReceiptStatus myArInvoiceReceiptStatus where myArInvoiceReceiptStatus.remainIncludeGstOriginalAmount = ?1") })

@Table(schema = "cashflow", name = "ar_invoice_receipt_status")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "ArInvoiceReceiptStatus")

public class ArInvoiceReceiptStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	
	/**
     */
	@Column(name = "ar_invoice_id")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigInteger arInvoiceId;

	/**
     */
	@Column(name = "setted_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal settedIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "remain_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal remainIncludeGstOriginalAmount;
	
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}
	
	public BigInteger getArInvoiceId() {
        return arInvoiceId;
    }
    
    public void setArInvoiceId(BigInteger arInvoiceId) {
        this.arInvoiceId = arInvoiceId;
    }

	/**
	 */
	public void setSettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount) {
		this.settedIncludeGstOriginalAmount = settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getSettedIncludeGstOriginalAmount() {
		return this.settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public void setRemainIncludeGstOriginalAmount(BigDecimal remainIncludeGstOriginalAmount) {
		this.remainIncludeGstOriginalAmount = remainIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getRemainIncludeGstOriginalAmount() {
		return this.remainIncludeGstOriginalAmount;
	}

	/**
	 */
	public ArInvoiceReceiptStatus() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArInvoiceReceiptStatus that) {
		setId(that.getId());
		setArInvoiceId(that.getArInvoiceId());
		setSettedIncludeGstOriginalAmount(that.getSettedIncludeGstOriginalAmount());
		setRemainIncludeGstOriginalAmount(that.getRemainIncludeGstOriginalAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("arInvoiceId=[").append(arInvoiceId).append("] ");
		buffer.append("settedIncludeGstOriginalAmount=[").append(settedIncludeGstOriginalAmount).append("] ");
		buffer.append("remainIncludeGstOriginalAmount=[").append(remainIncludeGstOriginalAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArInvoiceReceiptStatus))
			return false;
		ArInvoiceReceiptStatus equalCheck = (ArInvoiceReceiptStatus) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
