
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllReimbursementDetailss", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails"),
		@NamedQuery(name = "findReimbursementDetailsByApprovalCode", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.approvalCode = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByApprovalCodeContaining", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.approvalCode like ?1"),
		@NamedQuery(name = "findReimbursementDetailsByCreatedDate", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.createdDate = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByDescription", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.description = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByDescriptionContaining", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.description like ?1"),
		@NamedQuery(name = "findReimbursementDetailsById", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.id = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByModifiedDate", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByPrimaryKey", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.id = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByProjectCode", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.projectCode = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByProjectCodeContaining", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.projectCode like ?1"),
		@NamedQuery(name = "findReimbursementDetailsByProjectName", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.projectName = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByProjectNameContaining", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.projectName like ?1"),
		@NamedQuery(name = "findReimbursementDetailsByIncludeGstConvertedAmount", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findReimbursementDetailsByIncludeGstOriginalAmount", query = "select myReimbursementDetails from ReimbursementDetails myReimbursementDetails where myReimbursementDetails.includeGstOriginalAmount = ?1") })

@Table(schema = "cashflow", name = "reimbursement_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "first/com/ttv/cashflow/domain/domain", name = "ReimbursementDetails")

public class ReimbursementDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@XmlElement
	BigInteger id;
	/**
	 */
	
	@Column(name = "account_code", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountCode;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */
	
	@Column(name = "project_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectCode;
	/**
	 */

	@Column(name = "project_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectName;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "description", length = 500)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Column(name = "is_approval")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isApproval;
	
	/**
	 */

	@Column(name = "gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstOriginalAmount;
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "reimbursement_id", referencedColumnName = "id") })
	@XmlTransient
	Reimbursement reimbursement;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	/**
	 */
	public String getProjectCode() {
		return this.projectCode;
	}

	/**
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setReimbursement(Reimbursement reimbursement) {
		this.reimbursement = reimbursement;
	}

	/**
	 */
	public Reimbursement getReimbursement() {
		return reimbursement;
	}
	
	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}

	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}

	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}

	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}

	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public ReimbursementDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ReimbursementDetails that) {
		setId(that.getId());
		setProjectCode(that.getProjectCode());
		setProjectName(that.getProjectName());
		setApprovalCode(that.getApprovalCode());
		setDescription(that.getDescription());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setReimbursement(that.getReimbursement());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
	}

	@Override
	public String toString() {
		return "ReimbursementDetails [id=" + id + ", accountCode=" + accountCode + ", accountName=" + accountName
				+ ", projectCode=" + projectCode + ", projectName=" + projectName + ", approvalCode=" + approvalCode
				+ ", description=" + description + ", includeGstOriginalAmount=" + includeGstOriginalAmount
				+ ", includeGstConvertedAmount=" + includeGstConvertedAmount + ", isApproval=" + isApproval
				+ ", gstOriginalAmount=" + gstOriginalAmount + ", gstConvertedAmount=" + gstConvertedAmount
				+ ", excludeGstOriginalAmount=" + excludeGstOriginalAmount + ", excludeGstConvertedAmount="
				+ excludeGstConvertedAmount + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate
				+ ", reimbursement=" + reimbursement + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountCode == null) ? 0 : accountCode.hashCode());
		result = prime * result + ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result + ((approvalCode == null) ? 0 : approvalCode.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((excludeGstConvertedAmount == null) ? 0 : excludeGstConvertedAmount.hashCode());
		result = prime * result + ((excludeGstOriginalAmount == null) ? 0 : excludeGstOriginalAmount.hashCode());
		result = prime * result + ((gstConvertedAmount == null) ? 0 : gstConvertedAmount.hashCode());
		result = prime * result + ((gstOriginalAmount == null) ? 0 : gstOriginalAmount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((includeGstConvertedAmount == null) ? 0 : includeGstConvertedAmount.hashCode());
		result = prime * result + ((includeGstOriginalAmount == null) ? 0 : includeGstOriginalAmount.hashCode());
		result = prime * result + ((isApproval == null) ? 0 : isApproval.hashCode());
		result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
		result = prime * result + ((projectCode == null) ? 0 : projectCode.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReimbursementDetails other = (ReimbursementDetails) obj;
		if (accountCode == null) {
			if (other.accountCode != null)
				return false;
		} else if (!accountCode.equals(other.accountCode))
			return false;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (approvalCode == null) {
			if (other.approvalCode != null)
				return false;
		} else if (!approvalCode.equals(other.approvalCode))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (excludeGstConvertedAmount == null) {
			if (other.excludeGstConvertedAmount != null)
				return false;
		} else if (!excludeGstConvertedAmount.equals(other.excludeGstConvertedAmount))
			return false;
		if (excludeGstOriginalAmount == null) {
			if (other.excludeGstOriginalAmount != null)
				return false;
		} else if (!excludeGstOriginalAmount.equals(other.excludeGstOriginalAmount))
			return false;
		if (gstConvertedAmount == null) {
			if (other.gstConvertedAmount != null)
				return false;
		} else if (!gstConvertedAmount.equals(other.gstConvertedAmount))
			return false;
		if (gstOriginalAmount == null) {
			if (other.gstOriginalAmount != null)
				return false;
		} else if (!gstOriginalAmount.equals(other.gstOriginalAmount))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (includeGstConvertedAmount == null) {
			if (other.includeGstConvertedAmount != null)
				return false;
		} else if (!includeGstConvertedAmount.equals(other.includeGstConvertedAmount))
			return false;
		if (includeGstOriginalAmount == null) {
			if (other.includeGstOriginalAmount != null)
				return false;
		} else if (!includeGstOriginalAmount.equals(other.includeGstOriginalAmount))
			return false;
		if (isApproval == null) {
			if (other.isApproval != null)
				return false;
		} else if (!isApproval.equals(other.isApproval))
			return false;
		if (modifiedDate == null) {
			if (other.modifiedDate != null)
				return false;
		} else if (!modifiedDate.equals(other.modifiedDate))
			return false;
		if (projectCode == null) {
			if (other.projectCode != null)
				return false;
		} else if (!projectCode.equals(other.projectCode))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		if (reimbursement == null) {
			if (other.reimbursement != null)
				return false;
		} else if (!reimbursement.equals(other.reimbursement))
			return false;
		return true;
	}
}
