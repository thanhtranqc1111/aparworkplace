
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPermissions", query = "select myPermission from Permission myPermission order by id desc"),
		@NamedQuery(name = "findPermissionByCreatedDate", query = "select myPermission from Permission myPermission where myPermission.createdDate = ?1"),
		@NamedQuery(name = "findPermissionByDescription", query = "select myPermission from Permission myPermission where myPermission.description = ?1"),
		@NamedQuery(name = "findPermissionByDescriptionContaining", query = "select myPermission from Permission myPermission where myPermission.description like ?1"),
		@NamedQuery(name = "findPermissionById", query = "select myPermission from Permission myPermission where myPermission.id = ?1"),
		@NamedQuery(name = "findPermissionByModifiedDate", query = "select myPermission from Permission myPermission where myPermission.modifiedDate = ?1"),
		@NamedQuery(name = "findPermissionByName", query = "select myPermission from Permission myPermission where myPermission.name = ?1"),
		@NamedQuery(name = "findPermissionByNameContaining", query = "select myPermission from Permission myPermission where myPermission.name like ?1"),
		@NamedQuery(name = "findPermissionByPrimaryKey", query = "select myPermission from Permission myPermission where myPermission.id = ?1"),
		@NamedQuery(name = "findPermissionPaging", query = "select myPermission from Permission myPermission where lower(myPermission.name) like lower(CONCAT('%', ?1, '%')) order by id desc"),
		@NamedQuery(name = "countPermissionPaging", query = "select count(1) from Permission myPermission where lower(myPermission.name) like lower(CONCAT('%', ?1, '%'))")
})

@Table(schema = "cashflow", name = "permission")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "Permission")
@XmlRootElement(namespace = "cashflow_test/com/ttv/cashflow/domain")
public class Permission implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	
	/**
	 */

	@Column(name = "code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String code;
	
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "description", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "permission", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.RolePermission> rolePermissions;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setRolePermissions(Set<RolePermission> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}

	/**
	 */
	public Set<RolePermission> getRolePermissions() {
		if (rolePermissions == null) {
			rolePermissions = new java.util.LinkedHashSet<com.ttv.cashflow.domain.RolePermission>();
		}
		return rolePermissions;
	}

	/**
	 */
	public Permission() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Permission that) {
		setId(that.getId());
		setCode(that.getCode());
		setName(that.getName());
		setDescription(that.getDescription());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setRolePermissions(new java.util.LinkedHashSet<com.ttv.cashflow.domain.RolePermission>(that.getRolePermissions()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Permission))
			return false;
		Permission equalCheck = (Permission) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
