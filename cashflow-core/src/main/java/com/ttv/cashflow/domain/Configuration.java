
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Configuration.
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllConfigurations", query = "select myConfiguration from Configuration myConfiguration"),
		@NamedQuery(name = "findConfigurationById", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.id = ?1"),
		@NamedQuery(name = "findConfigurationByKey", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.key = ?1"),
		@NamedQuery(name = "findConfigurationByKeyContaining", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.key like ?1"),
		@NamedQuery(name = "findConfigurationByPrimaryKey", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.id = ?1"),
		@NamedQuery(name = "findConfigurationByValue", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.value = ?1"),
		@NamedQuery(name = "findConfigurationByValueContaining", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.value like ?1"),
		@NamedQuery(name = "findConfigurationByIds", query = "select myConfiguration from Configuration myConfiguration where myConfiguration.id in ?1 order by myConfiguration.id asc")})

@Table(schema = "cashflow", name = "configuration")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "Configuration")

public class Configuration implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant APNO_PREFIX. */
	public static final Integer APNO_PREFIX = 1;
	
	/** The Constant APNO_PREFIX_VALUE. */
	public static final String APNO_PREFIX_VALUE = "API";
	
	/** The Constant LAST_API_No. */
	public static final Integer LAST_API_No = 2;
	
	/** The Constant APNO_LAST_CREATED_MONTH. */
	public static final Integer APNO_LAST_CREATED_MONTH = 3;
	
	
	/** The id. */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	
	/** The key. */

	@Column(name = "key", length = 50)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String key;
	
	/** The value. */

	@Column(name = "value", length = 50)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String value;

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return this.key;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Instantiates a new configuration.
	 */
	public Configuration() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 * @param that the that
	 */
	public void copy(Configuration that) {
		setId(that.getId());
		setKey(that.getKey());
		setValue(that.getValue());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 * @return the string
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("key=[").append(key).append("] ");
		buffer.append("value=[").append(value).append("] ");

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Configuration))
			return false;
		Configuration equalCheck = (Configuration) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
