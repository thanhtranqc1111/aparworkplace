
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;

import com.ttv.cashflow.dto.ProjectPaymentDetail;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllProjectAllocations", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation"),
        @NamedQuery(name = "findProjectAllocationByCreatedDate", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.createdDate = ?1"),
        @NamedQuery(name = "findProjectAllocationById", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.id = ?1"),
        @NamedQuery(name = "findProjectAllocationByModifiedDate", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.modifiedDate = ?1"),
        @NamedQuery(name = "findProjectAllocationByMonth", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.month = ?1"),
        @NamedQuery(name = "findProjectAllocationByMonthAfter", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.month > ?1"),
        @NamedQuery(name = "findProjectAllocationByMonthBefore", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.month < ?1"),
        @NamedQuery(name = "findProjectAllocationByRangeMonth", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.month >= ?1 and myProjectAllocation.month <= ?2"),
        @NamedQuery(name = "findProjectAllocationByPercentAllocation", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.percentAllocation = ?1"),
        @NamedQuery(name = "findProjectAllocationByPrimaryKey", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.id = ?1"),
        @NamedQuery(name = "findProjectAllocationByMonthAndEmployeeId", query = "select myProjectAllocation from ProjectAllocation myProjectAllocation where myProjectAllocation.month = ?1 and myProjectAllocation.employeeMaster.id = ?2"),
})

@SqlResultSetMappings({
    @SqlResultSetMapping(name = "findProjectPaymentDetail", classes = {
            @ConstructorResult(targetClass = ProjectPaymentDetail.class, 
                columns = { 
                		@ColumnResult(name = "employeeCode"),
                		@ColumnResult(name = "employeeName"),
                		@ColumnResult(name = "employeeSalary", type = BigDecimal.class),
                		@ColumnResult(name = "projectCode"),
                		@ColumnResult(name = "projectName"),
                		@ColumnResult(name = "projectAssignedSalary", type = BigDecimal.class),
            })
        }),
})

@NamedStoredProcedureQuery(
		name = "get_list_payment_project", 
		procedureName = "get_list_payment_project", 
		resultSetMappings={"findProjectPaymentDetail"},
		parameters = {
				@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, type = void.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
		}
	)

@Table(schema = "cashflow", name = "project_allocation")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "CashFlowDriver/com/ttv/cashflow/domain", name = "ProjectAllocation")

public class ProjectAllocation implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     */

    @Column(name = "id", nullable = false)
    @Basic(fetch = FetchType.EAGER)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    BigInteger id;
    /**
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "month")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar month;
    /**
     */

    @Column(name = "percent_allocation", scale = 8, precision = 8)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal percentAllocation;
    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar createdDate;
    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar modifiedDate;

    /**
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "project_code", referencedColumnName = "code") })
    @XmlTransient
    ProjectMaster projectMaster;
    /**
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "employee_id", referencedColumnName = "id") })
    @XmlTransient
    EmployeeMaster employeeMaster;

    /**
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     */
    public BigInteger getId() {
        return this.id;
    }

    /**
     */
    public void setMonth(Calendar month) {
        this.month = month;
    }

    /**
     */
    public Calendar getMonth() {
        return this.month;
    }

    /**
     */
    public void setPercentAllocation(BigDecimal percentAllocation) {
        this.percentAllocation = percentAllocation;
    }

    /**
     */
    public BigDecimal getPercentAllocation() {
        return this.percentAllocation;
    }

    /**
     */
    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    /**
     */
    public Calendar getCreatedDate() {
        return this.createdDate;
    }

    /**
     */
    public void setModifiedDate(Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    /**
     */
    public Calendar getModifiedDate() {
        return this.modifiedDate;
    }

    /**
     */
    public void setProjectMaster(ProjectMaster projectMaster) {
        this.projectMaster = projectMaster;
    }

    /**
     */
    public ProjectMaster getProjectMaster() {
        return projectMaster;
    }

    /**
     */
    public void setEmployeeMaster(EmployeeMaster employeeMaster) {
        this.employeeMaster = employeeMaster;
    }

    /**
     */
    public EmployeeMaster getEmployeeMaster() {
        return employeeMaster;
    }

    /**
     */
    public ProjectAllocation() {
    }

    /**
     * Copies the contents of the specified bean into this bean.
     *
     */
    public void copy(ProjectAllocation that) {
        setId(that.getId());
        setMonth(that.getMonth());
        setPercentAllocation(that.getPercentAllocation());
        setCreatedDate(that.getCreatedDate());
        setModifiedDate(that.getModifiedDate());
        setProjectMaster(that.getProjectMaster());
        setEmployeeMaster(that.getEmployeeMaster());
    }

    /**
     * Returns a textual representation of a bean.
     *
     */
    public String toString() {

        StringBuilder buffer = new StringBuilder();

        buffer.append("id=[").append(id).append("] ");
        buffer.append("month=[").append(month).append("] ");
        buffer.append("percentAllocation=[").append(percentAllocation).append("] ");
        buffer.append("createdDate=[").append(createdDate).append("] ");
        buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

        return buffer.toString();
    }

    /**
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
        return result;
    }

    /**
     */
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof ProjectAllocation))
            return false;
        ProjectAllocation equalCheck = (ProjectAllocation) obj;
        if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
            return false;
        if (id != null && !id.equals(equalCheck.id))
            return false;
        return true;
    }
    
    public double getPercentageDouble() {
    	return percentAllocation.doubleValue();
    }
}
