package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.dto.PaymentPayroll;
import com.ttv.cashflow.util.Constant;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPaymentOrders", query = "select myPaymentOrder from PaymentOrder myPaymentOrder"),
		@NamedQuery(name = "findPaymentOrderByAccountPayableCode", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.accountPayableCode = ?1"),
		@NamedQuery(name = "findPaymentOrderByAccountPayableCodeContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.accountPayableCode like ?1"),
		@NamedQuery(name = "findPaymentOrderByAccountPayableName", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.accountPayableName = ?1"),
		@NamedQuery(name = "findPaymentOrderByAccountPayableNameContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.accountPayableName like ?1"),
		@NamedQuery(name = "findPaymentOrderByBankAccount", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankAccount = ?1"),
		@NamedQuery(name = "findPaymentOrderByBankAccountContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankAccount like ?1"),
		@NamedQuery(name = "findPaymentOrderByBankName", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankName = ?1"),
		@NamedQuery(name = "findPaymentOrderByBankNameContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankName like ?1"),
		@NamedQuery(name = "findPaymentOrderByBankRefNo", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankRefNo = ?1"),
		@NamedQuery(name = "findPaymentOrderByBankRefNoContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankRefNo like ?1"),
		@NamedQuery(name = "findPaymentOrderByCreatedDate", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.createdDate = ?1"),
		@NamedQuery(name = "findPaymentOrderByDescription", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.description = ?1"),
		@NamedQuery(name = "findPaymentOrderByDescriptionContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.description like ?1"),
		@NamedQuery(name = "findPaymentOrderById", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.id = ?1"),
		@NamedQuery(name = "findPaymentOrderByModifiedDate", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.modifiedDate = ?1"),
		@NamedQuery(name = "findPaymentOrderByPaymentApprovalNo", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentApprovalNo = ?1"),
		@NamedQuery(name = "findPaymentOrderByPaymentApprovalNoContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentApprovalNo like ?1"),
		@NamedQuery(name = "findPaymentOrderByPaymentOrderNo", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentOrderNo = ?1 and myPaymentOrder.paymentType = ?2"),
		@NamedQuery(name = "findPaymentOrderByPaymentOrderNoContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where lower(myPaymentOrder.paymentOrderNo) like lower(?1)"),
		@NamedQuery(name = "findPaymentOrderByOriginalCurrencyCode", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findPaymentOrderByPaymentRate", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentRate = ?1"),
		@NamedQuery(name = "findPaymentOrderByIncludeGstOriginalAmount", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderByIncludeGstConvertedAmount", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderByPrimaryKey", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.id = ?1"),
		@NamedQuery(name = "findPaymentOrderByStatus", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.status = ?1"),
		@NamedQuery(name = "findPaymentOrderByStatusContaining", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.status like ?1"),
		@NamedQuery(name = "findPaymentOrderByValueDate", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.valueDate = ?1"),
		@NamedQuery(name = "findPaymentIdByPaymentOrderNo", query = "select myPaymentOrder.id from PaymentOrder myPaymentOrder where myPaymentOrder.paymentOrderNo = ?1"),
		@NamedQuery(name = "findListPaymentOrderByPaymentOrderNo", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentOrderNo IN (?1) "),
		@NamedQuery(name = "findPaymentOrderByPaymentOrderNoContainingAndStatus", query = "select myPaymentOrder from PaymentOrder myPaymentOrder "
																						+ "where lower(myPaymentOrder.paymentOrderNo) like lower(?1) and (myPaymentOrder.status = ?2 or myPaymentOrder.status is null)"),
		@NamedQuery(name = "findPaymentOrderByPaymentOrderNoOnly", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.paymentOrderNo = ?1"),
		@NamedQuery(name = "findPaymentOrderByConvertedAmountAndValueDate", query = "select myPaymentOrder from PaymentOrder myPaymentOrder where myPaymentOrder.bankName = ?1 and myPaymentOrder.bankAccount = ?2 and myPaymentOrder.includeGstConvertedAmount = ?3 and myPaymentOrder.valueDate = ?4 "),
		})


@SqlResultSetMappings({
    @SqlResultSetMapping(
		name = "findPaymentInvoiceDetail",
		classes = { 
		    @ConstructorResult(
		        targetClass = PaymentInvoice.class,
		        columns = {
			    		@ColumnResult(name = "paymentId", type=BigInteger.class),
						@ColumnResult(name = "paymentOrderNo"),
						@ColumnResult(name = "valueDate", type=Calendar.class),
						@ColumnResult(name = "bankName"),
				        @ColumnResult(name = "bankAccount"),
						@ColumnResult(name = "bankRefNo"),
						@ColumnResult(name = "includeGstConvertedAmount", type=BigDecimal.class),
						@ColumnResult(name = "paymentStatus"),	
						@ColumnResult(name = "paymentType"),	
						@ColumnResult(name = "accountPayableId", type=BigInteger.class),
						@ColumnResult(name = "accountPayableNo"),
				        @ColumnResult(name = "month", type=Calendar.class),
				        @ColumnResult(name = "claimType"), 
						@ColumnResult(name = "totalAmount", type=BigDecimal.class),
				        @ColumnResult(name = "invoiceStatus"),
				        }
		    ) 
		}
	),
    @SqlResultSetMapping(
		name = "findPaymentNo",
		classes = { 
		    @ConstructorResult(
		        targetClass = PaymentInvoice.class,
		        columns = {
						@ColumnResult(name = "paymentOrderNo"),
						@ColumnResult(name = "paymentType"),
				        }
		    ) 
		}
	),
    @SqlResultSetMapping(
		name = "findPaymentPayrollDetail",
		classes = { 
		    @ConstructorResult(
	    		targetClass = PaymentPayroll.class,
	    		columns = {
    				@ColumnResult(name = "paymentId", type=BigInteger.class),
					@ColumnResult(name = "paymentOrderNo"),
					@ColumnResult(name = "valueDate", type=Calendar.class),
					@ColumnResult(name = "bankName"),
					@ColumnResult(name = "bankAccount"),
					@ColumnResult(name = "bankRefNo"),
					@ColumnResult(name = "includeGstOriginalAmount", type=BigDecimal.class),
					@ColumnResult(name = "includeGstConvertedAmount", type=BigDecimal.class),
					@ColumnResult(name = "payrollId", type=BigInteger.class),
					@ColumnResult(name = "payrollNo"),
					@ColumnResult(name = "month", type=Calendar.class),
					@ColumnResult(name = "totalNetPaymentAmountOriginal", type=BigDecimal.class),
					@ColumnResult(name = "totalNetPaymentAmountConverted", type=BigDecimal.class),
					@ColumnResult(name = "paymentStatus"),
					@ColumnResult(name = "payrollStatus"),
					@ColumnResult(name = "paymentCurrency"),
	    		}
		    )
		}
    )
})


@Table(schema = "cashflow", name = "payment_order")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "PaymentOrder")
@XmlRootElement(namespace = "cashflow/com/ttv/cashflow/domain")
public class PaymentOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payment_order_no", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;
	/**
	 */

	@Column(name = "bank_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankName;
	/**
	 */

	@Column(name = "bank_account", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankAccount;
	/**
	 */
	
	@Column(name = "payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */
	
	@Column(name = "original_currency_code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */
	
	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */
	
	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "value_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar valueDate;
	/**
	 */

	@Column(name = "bank_ref_no", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankRefNo;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "payment_approval_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentApprovalNo;
	/**
	 */

	@Column(name = "account_payable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */

	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	
	/**
	 */

	@Column(name = "payment_type")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentType;
	
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "paymentOrder", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ApInvoicePaymentDetails> apInvoicePaymentDetailses;
	
	/**
	 */
	@OneToMany(mappedBy = "paymentOrder", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.PayrollPaymentDetails> payrollPaymentDetailses;
	
	
	/**
     */
    @OneToMany(mappedBy = "paymentOrder", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

    @XmlElement(name = "", namespace = "")
    java.util.Set<com.ttv.cashflow.domain.ReimbursementPaymentDetails> reimbursementPaymentDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 */
	public String getBankName() {
		return this.bankName;
	}

	/**
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 */
	public String getBankAccount() {
		return this.bankAccount;
	}
	
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	/**
	 */
	public String getBankRefNo() {
		return this.bankRefNo;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setPaymentApprovalNo(String paymentApprovalNo) {
		this.paymentApprovalNo = paymentApprovalNo;
	}

	/**
	 */
	public String getPaymentApprovalNo() {
		return this.paymentApprovalNo;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 */
	public String getStatus() {
		return this.status;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setApInvoicePaymentDetailses(Set<ApInvoicePaymentDetails> apInvoicePaymentDetailses) {
		this.apInvoicePaymentDetailses = apInvoicePaymentDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ApInvoicePaymentDetails> getApInvoicePaymentDetailses() {
		if (apInvoicePaymentDetailses == null) {
			apInvoicePaymentDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoicePaymentDetails>();
		}
		return apInvoicePaymentDetailses;
	}
	
	/**
	 */
	public java.util.Set<com.ttv.cashflow.domain.PayrollPaymentDetails> getPayrollPaymentDetailses() {
		if (payrollPaymentDetailses == null) {
			payrollPaymentDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollPaymentDetails>();
		}
		return payrollPaymentDetailses;
		
	}

	/**
	 */
	@JsonIgnore
	public void setPayrollPaymentDetailses(
			java.util.Set<com.ttv.cashflow.domain.PayrollPaymentDetails> payrollPaymentDetailses) {
		this.payrollPaymentDetailses = payrollPaymentDetailses;
	}
	
	public void setReimbursementPaymentDetailses(java.util.Set<com.ttv.cashflow.domain.ReimbursementPaymentDetails> reimbursementPaymentDetailses) {
        this.reimbursementPaymentDetailses = reimbursementPaymentDetailses;
    }
	
	public java.util.Set<com.ttv.cashflow.domain.ReimbursementPaymentDetails> getReimbursementPaymentDetailses() {
        return reimbursementPaymentDetailses;
    }

	/**
	 */
	public PaymentOrder() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PaymentOrder that) {
		setId(that.getId());
		setPaymentOrderNo(that.getPaymentOrderNo());
		setBankName(that.getBankName());
		setBankAccount(that.getBankAccount());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setPaymentRate(that.getPaymentRate());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setValueDate(that.getValueDate());
		setBankRefNo(that.getBankRefNo());
		setDescription(that.getDescription());
		setPaymentApprovalNo(that.getPaymentApprovalNo());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setStatus(that.getStatus());
		setPaymentType(that.getPaymentType());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setApInvoicePaymentDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoicePaymentDetails>(that.getApInvoicePaymentDetailses()));
		setPayrollPaymentDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollPaymentDetails>(that.getPayrollPaymentDetailses()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");
		buffer.append("bankName=[").append(bankName).append("] ");
		buffer.append("bankAccount=[").append(bankAccount).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("bankRefNo=[").append(bankRefNo).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("paymentApprovalNo=[").append(paymentApprovalNo).append("] ");
		buffer.append("accountPayableCode=[").append(accountPayableCode).append("] ");
		buffer.append("accountPayableName=[").append(accountPayableName).append("] ");
		buffer.append("status=[").append(status).append("] ");
		buffer.append("paymentType=[").append(paymentType).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PaymentOrder))
			return false;
		PaymentOrder equalCheck = (PaymentOrder) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
	
	public boolean isPaymentOrderApInvoice() {
		return paymentType != null && paymentType.indexOf(Constant.PAYMENT_ORDER_APINVOICE) >= 0;
	}
	
	public boolean isPaymentOrderPayroll() {
		return paymentType != null && paymentType.indexOf(Constant.PAYMENT_ORDER_PAYROLL) >= 0;
	}
	
	public boolean isPaymentOrderReimbursement() {
		return paymentType != null && paymentType.indexOf(Constant.PAYMENT_ORDER_REIMBURSEMENT) >= 0;
	}
}
