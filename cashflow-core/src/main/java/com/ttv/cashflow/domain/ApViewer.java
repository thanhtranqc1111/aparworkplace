
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApViewers", query = "select myApViewer from ApViewer myApViewer"),
		@NamedQuery(name = "findApViewerByAccountName", query = "select myApViewer from ApViewer myApViewer where myApViewer.accountName = ?1"),
		@NamedQuery(name = "findApViewerByAccountNameContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.accountName like ?1"),
		@NamedQuery(name = "findApViewerByApInvoiceNo", query = "select myApViewer from ApViewer myApViewer where myApViewer.apInvoiceNo = ?1"),
		@NamedQuery(name = "findApViewerByApInvoiceNoContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.apInvoiceNo like ?1"),
		@NamedQuery(name = "findApViewerByApprovalCode", query = "select myApViewer from ApViewer myApViewer where myApViewer.approvalCode = ?1"),
		@NamedQuery(name = "findApViewerByApprovalCodeContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.approvalCode like ?1"),
		@NamedQuery(name = "findApViewerByClassName", query = "select myApViewer from ApViewer myApViewer where myApViewer.className = ?1"),
		@NamedQuery(name = "findApViewerByClassNameContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.className like ?1"),
		@NamedQuery(name = "findApViewerByDescription", query = "select myApViewer from ApViewer myApViewer where myApViewer.description = ?1"),
		@NamedQuery(name = "findApViewerByDescriptionContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.description like ?1"),
		@NamedQuery(name = "findApViewerByExcludeGstOriginalAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApViewerByFxRate", query = "select myApViewer from ApViewer myApViewer where myApViewer.fxRate = ?1"),
		@NamedQuery(name = "findApViewerByGstRate", query = "select myApViewer from ApViewer myApViewer where myApViewer.gstRate = ?1"),
		@NamedQuery(name = "findApViewerByGstType", query = "select myApViewer from ApViewer myApViewer where myApViewer.gstType = ?1"),
		@NamedQuery(name = "findApViewerByGstTypeContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.gstType like ?1"),
		@NamedQuery(name = "findApViewerById", query = "select myApViewer from ApViewer myApViewer where myApViewer.id = ?1"),
		@NamedQuery(name = "findApViewerByIncludeGstConvertedAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApViewerByIncludeGstOriginalAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApViewerByInvoiceNo", query = "select myApViewer from ApViewer myApViewer where myApViewer.invoiceNo = ?1"),
		@NamedQuery(name = "findApViewerByInvoiceNoContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.invoiceNo like ?1"),
		@NamedQuery(name = "findApViewerByMonth", query = "select myApViewer from ApViewer myApViewer where myApViewer.month = ?1"),
		@NamedQuery(name = "findApViewerByMonthAfter", query = "select myApViewer from ApViewer myApViewer where myApViewer.month > ?1"),
		@NamedQuery(name = "findApViewerByMonthBefore", query = "select myApViewer from ApViewer myApViewer where myApViewer.month < ?1"),
		@NamedQuery(name = "findApViewerByOriginalCurrencyCode", query = "select myApViewer from ApViewer myApViewer where myApViewer.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findApViewerByOriginalCurrencyCodeContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findApViewerByPayeeName", query = "select myApViewer from ApViewer myApViewer where myApViewer.payeeName = ?1"),
		@NamedQuery(name = "findApViewerByPayeeNameContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.payeeName like ?1"),
		@NamedQuery(name = "findApViewerByPaymentIncludeGstConvertedAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.paymentIncludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApViewerByPaymentIncludeGstOriginalAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.paymentIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApViewerByPaymentOrderNo", query = "select myApViewer from ApViewer myApViewer where myApViewer.paymentOrderNo = ?1"),
		@NamedQuery(name = "findApViewerByPaymentOrderNoContaining", query = "select myApViewer from ApViewer myApViewer where myApViewer.paymentOrderNo like ?1"),
		@NamedQuery(name = "findApViewerByPaymentRate", query = "select myApViewer from ApViewer myApViewer where myApViewer.paymentRate = ?1"),
		@NamedQuery(name = "findApViewerByPrimaryKey", query = "select myApViewer from ApViewer myApViewer where myApViewer.id = ?1"),
		@NamedQuery(name = "findApViewerByUnpaidIncludeGstOriginalAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.unpaidIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApViewerByValueDate", query = "select myApViewer from ApViewer myApViewer where myApViewer.valueDate = ?1"),
		@NamedQuery(name = "findApViewerByValueDateAfter", query = "select myApViewer from ApViewer myApViewer where myApViewer.valueDate > ?1"),
		@NamedQuery(name = "findApViewerByValueDateBefore", query = "select myApViewer from ApViewer myApViewer where myApViewer.valueDate < ?1"),
		@NamedQuery(name = "findApViewerByVarianceAmount", query = "select myApViewer from ApViewer myApViewer where myApViewer.varianceAmount = ?1") })

@Table(schema = "cashflow", name = "ap_ledger")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "ApViewer")

public class ApViewer implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "ap_invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoiceNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "payee_name", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeName;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "original_currency_code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "gst_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Column(name = "account_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "class_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String className;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "payment_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "value_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar valueDate;
	/**
	 */

	@Column(name = "payment_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "payment_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */

	@Column(name = "payment_include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentIncludeGstConvertedAmount;
	/**
	 */

	@Column(name = "variance_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "unpaid_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal unpaidIncludeGstOriginalAmount;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}

	/**
	 */
	public String getApInvoiceNo() {
		return this.apInvoiceNo;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	/**
	 */
	public String getPayeeName() {
		return this.payeeName;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setPaymentIncludeGstOriginalAmount(BigDecimal paymentIncludeGstOriginalAmount) {
		this.paymentIncludeGstOriginalAmount = paymentIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getPaymentIncludeGstOriginalAmount() {
		return this.paymentIncludeGstOriginalAmount;
	}

	/**
	 */
	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public BigDecimal getPaymentRate() {
		return this.paymentRate;
	}

	/**
	 */
	public void setPaymentIncludeGstConvertedAmount(BigDecimal paymentIncludeGstConvertedAmount) {
		this.paymentIncludeGstConvertedAmount = paymentIncludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getPaymentIncludeGstConvertedAmount() {
		return this.paymentIncludeGstConvertedAmount;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setUnpaidIncludeGstOriginalAmount(BigDecimal unpaidIncludeGstOriginalAmount) {
		this.unpaidIncludeGstOriginalAmount = unpaidIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getUnpaidIncludeGstOriginalAmount() {
		return this.unpaidIncludeGstOriginalAmount;
	}

	/**
	 */
	public ApViewer() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApViewer that) {
		setId(that.getId());
		setApInvoiceNo(that.getApInvoiceNo());
		setMonth(that.getMonth());
		setPayeeName(that.getPayeeName());
		setInvoiceNo(that.getInvoiceNo());
		setApprovalCode(that.getApprovalCode());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setFxRate(that.getFxRate());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setAccountName(that.getAccountName());
		setClassName(that.getClassName());
		setDescription(that.getDescription());
		setPaymentOrderNo(that.getPaymentOrderNo());
		setValueDate(that.getValueDate());
		setPaymentIncludeGstOriginalAmount(that.getPaymentIncludeGstOriginalAmount());
		setPaymentRate(that.getPaymentRate());
		setPaymentIncludeGstConvertedAmount(that.getPaymentIncludeGstConvertedAmount());
		setVarianceAmount(that.getVarianceAmount());
		setUnpaidIncludeGstOriginalAmount(that.getUnpaidIncludeGstOriginalAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("apInvoiceNo=[").append(apInvoiceNo).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("payeeName=[").append(payeeName).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("className=[").append(className).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("paymentIncludeGstOriginalAmount=[").append(paymentIncludeGstOriginalAmount).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("paymentIncludeGstConvertedAmount=[").append(paymentIncludeGstConvertedAmount).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("unpaidIncludeGstOriginalAmount=[").append(unpaidIncludeGstOriginalAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApViewer))
			return false;
		ApViewer equalCheck = (ApViewer) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
