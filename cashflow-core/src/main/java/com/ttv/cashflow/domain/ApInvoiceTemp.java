
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoiceTemps", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp"),
		@NamedQuery(name = "findApInvoiceTempByAccountCode", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountCode = ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountCodeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountCode like ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountName", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountName = ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountNameContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountName like ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountPayableCode", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountPayableCode = ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountPayableCodeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountPayableCode like ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountPayableName", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountPayableName = ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountPayableNameContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountPayableName like ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountType", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountType = ?1"),
		@NamedQuery(name = "findApInvoiceTempByAccountTypeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.accountType like ?1"),
		@NamedQuery(name = "findApInvoiceTempByAmountExcludeGstUnconverted", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoiceTempByApprovalCode", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.approvalCode = ?1"),
		@NamedQuery(name = "findApInvoiceTempByApprovalCodeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.approvalCode like ?1"),
		@NamedQuery(name = "findApInvoiceTempByClaimType", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.claimType = ?1"),
		@NamedQuery(name = "findApInvoiceTempByClaimTypeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.claimType like ?1"),
		@NamedQuery(name = "findApInvoiceTempByDescription", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.description = ?1"),
		@NamedQuery(name = "findApInvoiceTempByDescriptionContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.description like ?1"),
		@NamedQuery(name = "findApInvoiceTempByFxRate", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.fxRate = ?1"),
		@NamedQuery(name = "findApInvoiceTempByGstRate", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.gstRate = ?1"),
		@NamedQuery(name = "findApInvoiceTempByGstType", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.gstType = ?1"),
		@NamedQuery(name = "findApInvoiceTempByGstTypeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.gstType like ?1"),
		@NamedQuery(name = "findApInvoiceTempById", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.id = ?1"),
		@NamedQuery(name = "findApInvoiceTempByInvoiceIssuedDate", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.invoiceIssuedDate = ?1"),
		@NamedQuery(name = "findApInvoiceTempByInvoiceNo", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.invoiceNo = ?1"),
		@NamedQuery(name = "findApInvoiceTempByInvoiceNoContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.invoiceNo like ?1"),
		@NamedQuery(name = "findApInvoiceTempByMonth", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.month = ?1"),
		@NamedQuery(name = "findApInvoiceTempByMonthAfter", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.month > ?1"),
		@NamedQuery(name = "findApInvoiceTempByMonthBefore", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.month < ?1"),
		@NamedQuery(name = "findApInvoiceTempByOriginalCurrency", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.originalCurrency = ?1"),
		@NamedQuery(name = "findApInvoiceTempByOriginalCurrencyContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.originalCurrency like ?1"),
		@NamedQuery(name = "findApInvoiceTempByPayeeName", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.payeeName = ?1"),
		@NamedQuery(name = "findApInvoiceTempByPayeeNameContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.payeeName like ?1"),
		@NamedQuery(name = "findApInvoiceTempByPayeeTypeName", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.payeeTypeName = ?1"),
		@NamedQuery(name = "findApInvoiceTempByPayeeTypeNameContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.payeeTypeName like ?1"),
		@NamedQuery(name = "findApInvoiceTempByPaymentTerm", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.paymentTerm = ?1"),
		@NamedQuery(name = "findApInvoiceTempByPaymentTermContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.paymentTerm like ?1"),
		@NamedQuery(name = "findApInvoiceTempByPoNo", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.poNo = ?1"),
		@NamedQuery(name = "findApInvoiceTempByPoNoContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.poNo like ?1"),
		@NamedQuery(name = "findApInvoiceTempByPrimaryKey", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.id = ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectCode", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectCode = ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectCodeContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectCode like ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectDescription", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectDescription = ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectDescriptionContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectDescription like ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectName", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectName = ?1"),
		@NamedQuery(name = "findApInvoiceTempByProjectNameContaining", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.projectName like ?1"),
		@NamedQuery(name = "findApInvoiceTempByScheduledPaymentDate", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.scheduledPaymentDate = ?1"),
		@NamedQuery(name = "findApInvoiceTempByScheduledPaymentDateAfter", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.scheduledPaymentDate > ?1"),
		@NamedQuery(name = "findApInvoiceTempByScheduledPaymentDateBefore", query = "select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp where myApInvoiceTemp.scheduledPaymentDate < ?1") })

@Table(schema = "cashflow", name = "ap_invoice_temp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoiceTemp")

public class ApInvoiceTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	
	@Column(name = "payee_code", length = 40)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String payeeCode;
	
	/**
	 */

	@Column(name = "payee_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeName;
	/**
	 */

	@Column(name = "payee_type_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payeeTypeName;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invoice_issued_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar invoiceIssuedDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "gst_type", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */

	@Column(name = "original_currency", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrency;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */
	
	@Column(name = "total_exclude_gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal totalExcludeGstOriginalAmount;
	
	/**
     */
    
    @Column(name = "total_included_gst_converted_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal totalIncludedGstConvertedAmount;

	/**
	 */
	@Column(name = "account_payable_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */

	@Column(name = "payment_term", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentTerm;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "scheduled_payment_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar scheduledPaymentDate;
	/**
	 */

	@Column(name = "description", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "account_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountCode;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "account_type", length = 50)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountType;
	/**
	 */

	@Column(name = "project_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectCode;
	/**
	 */

	@Column(name = "project_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectName;
	/**
	 */

	@Column(name = "project_description", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectDescription;
	/**
	 */

	@Column(name = "po_no", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String poNo;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	
	/**
	 */
	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	
	/**
     */
    @Column(name = "included_gst_converted_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal includedGstConvertedAmount;
	
	/**
	 */
	@Column(name = "is_approval")
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    Boolean isApproval;
	
	

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}
	
	public String getPayeeCode() {
        return payeeCode;
    }
	
	public void setPayeeCode(String payeeCode) {
        this.payeeCode = payeeCode;
    }

	/**
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	/**
	 */
	public String getPayeeName() {
		return this.payeeName;
	}

	/**
	 */
	public void setPayeeTypeName(String payeeTypeName) {
		this.payeeTypeName = payeeTypeName;
	}

	/**
	 */
	public String getPayeeTypeName() {
		return this.payeeTypeName;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}

	/**
	 */
	public Calendar getInvoiceIssuedDate() {
		return this.invoiceIssuedDate;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}

	/**
	 */
	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	/**
	 */
	public String getOriginalCurrency() {
		return this.originalCurrency;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 */
	public String getPaymentTerm() {
		return this.paymentTerm;
	}

	/**
	 */
	public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
		this.scheduledPaymentDate = scheduledPaymentDate;
	}

	/**
	 */
	public Calendar getScheduledPaymentDate() {
		return this.scheduledPaymentDate;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 */
	public String getAccountCode() {
		return this.accountCode;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 */
	public String getAccountType() {
		return this.accountType;
	}

	/**
	 */
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	/**
	 */
	public String getProjectCode() {
		return this.projectCode;
	}

	/**
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 */
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	/**
	 */
	public String getProjectDescription() {
		return this.projectDescription;
	}

	/**
	 */
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	/**
	 */
	public String getPoNo() {
		return this.poNo;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	public void setIsApproval(Boolean isApproval) {
        this.isApproval = isApproval;
    }
	
	public Boolean getIsApproval() {
        return isApproval;
    }
	
	public BigDecimal getIncludedGstConvertedAmount() {
        return includedGstConvertedAmount;
    }
	
	public void setIncludedGstConvertedAmount(BigDecimal includedGstConvertedAmount) {
        this.includedGstConvertedAmount = includedGstConvertedAmount;
    }
	
	public BigDecimal getTotalExcludeGstOriginalAmount() {
        return totalExcludeGstOriginalAmount;
    }
	
	public void setTotalExcludeGstOriginalAmount(BigDecimal totalExcludeGstOriginalAmount) {
        this.totalExcludeGstOriginalAmount = totalExcludeGstOriginalAmount;
    }
	
	public BigDecimal getTotalIncludedGstConvertedAmount() {
        return totalIncludedGstConvertedAmount;
    }
	
	public void setTotalIncludedGstConvertedAmount(BigDecimal totalIncludedGstConvertedAmount) {
        this.totalIncludedGstConvertedAmount = totalIncludedGstConvertedAmount;
    }
	
	/**
	 */
	public ApInvoiceTemp() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoiceTemp that) {
		setId(that.getId());
		setPayeeCode(that.getPayeeCode());
		setPayeeName(that.getPayeeName());
		setPayeeTypeName(that.getPayeeTypeName());
		setInvoiceNo(that.getInvoiceNo());
		setInvoiceIssuedDate(that.getInvoiceIssuedDate());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setOriginalCurrency(that.getOriginalCurrency());
		setFxRate(that.getFxRate());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setPaymentTerm(that.getPaymentTerm());
		setScheduledPaymentDate(that.getScheduledPaymentDate());
		setDescription(that.getDescription());
		setAccountCode(that.getAccountCode());
		setAccountName(that.getAccountName());
		setAccountType(that.getAccountType());
		setProjectCode(that.getProjectCode());
		setProjectName(that.getProjectName());
		setProjectDescription(that.getProjectDescription());
		setPoNo(that.getPoNo());
		setApprovalCode(that.getApprovalCode());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setIsApproval(that.getIsApproval());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payeeCode=[").append(payeeCode).append("] ");
		buffer.append("payeeName=[").append(payeeName).append("] ");
		buffer.append("payeeTypeName=[").append(payeeTypeName).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("invoiceIssuedDate=[").append(invoiceIssuedDate).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("originalCurrency=[").append(originalCurrency).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("accountPayableCode=[").append(accountPayableCode).append("] ");
		buffer.append("accountPayableName=[").append(accountPayableName).append("] ");
		buffer.append("paymentTerm=[").append(paymentTerm).append("] ");
		buffer.append("scheduledPaymentDate=[").append(scheduledPaymentDate).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("accountCode=[").append(accountCode).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("accountType=[").append(accountType).append("] ");
		buffer.append("projectCode=[").append(projectCode).append("] ");
		buffer.append("projectName=[").append(projectName).append("] ");
		buffer.append("projectDescription=[").append(projectDescription).append("] ");
		buffer.append("poNo=[").append(poNo).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("amountExcludeGstUnconverted=[").append(excludeGstOriginalAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApInvoiceTemp))
			return false;
		ApInvoiceTemp equalCheck = (ApInvoiceTemp) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
