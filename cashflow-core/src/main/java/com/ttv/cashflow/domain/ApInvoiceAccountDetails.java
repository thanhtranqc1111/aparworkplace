
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoiceAccountDetailss", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByAccountCode", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.accountCode = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByAccountCodeContaining", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.accountCode like ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByAccountName", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.accountName = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByAccountNameContaining", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.accountName like ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByCreatedDate", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.createdDate = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByExcludeGstAmount", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByExcludeGstConvertedAmount", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByGstAmount", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsById", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.id = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByIncludeGstAmount", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByModifiedDate", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findApInvoiceAccountDetailsByPrimaryKey", query = "select myApInvoiceAccountDetails from ApInvoiceAccountDetails myApInvoiceAccountDetails where myApInvoiceAccountDetails.id = ?1") })

@Table(schema = "cashflow", name = "ap_invoice_account_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoiceAccountDetails")
@XmlRootElement(namespace = "cashflow/com/ttv/cashflow/domain")
public class ApInvoiceAccountDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "account_code", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountCode;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	
	/**
     */
    @Column(name = "gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal gstOriginalAmount;
	
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	
	/**
     */

    @Column(name = "include_gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal includeGstOriginalAmount;
	
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "ap_invoice_id", referencedColumnName = "id") })
	@XmlTransient
	ApInvoice apInvoice;
	/**
	 */
	@OneToMany(mappedBy = "apInvoiceAccountDetails", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ApInvoiceClassDetails> apInvoiceClassDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 */
	public String getAccountCode() {
		return this.accountCode;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }
	
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }
	
	public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
	
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }
	
	public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
	
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setApInvoice(ApInvoice apInvoice) {
		this.apInvoice = apInvoice;
	}

	/**
	 */
	@JsonIgnore
	public ApInvoice getApInvoice() {
		return apInvoice;
	}

	/**
	 */
	public void setApInvoiceClassDetailses(Set<ApInvoiceClassDetails> apInvoiceClassDetailses) {
		this.apInvoiceClassDetailses = apInvoiceClassDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ApInvoiceClassDetails> getApInvoiceClassDetailses() {
		if (apInvoiceClassDetailses == null) {
			apInvoiceClassDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoiceClassDetails>();
		}
		return apInvoiceClassDetailses;
	}

	/**
	 */
	public ApInvoiceAccountDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoiceAccountDetails that) {
		setId(that.getId());
		setAccountCode(that.getAccountCode());
		setAccountName(that.getAccountName());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setApInvoice(that.getApInvoice());
		setApInvoiceClassDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ApInvoiceClassDetails>(that.getApInvoiceClassDetailses()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("accountCode=[").append(accountCode).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("excludeGstAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountCode == null) ? 0 : accountCode.hashCode());
		result = prime * result
				+ ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result
				+ ((apInvoice == null) ? 0 : apInvoice.hashCode());
		result = prime
				* result
				+ ((apInvoiceClassDetailses == null) ? 0
						: apInvoiceClassDetailses.hashCode());
		result = prime * result
				+ ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime
				* result
				+ ((excludeGstOriginalAmount == null) ? 0 : excludeGstOriginalAmount.hashCode());
		result = prime
				* result
				+ ((excludeGstConvertedAmount == null) ? 0
						: excludeGstConvertedAmount.hashCode());
		result = prime * result
				+ ((gstConvertedAmount == null) ? 0 : gstConvertedAmount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((includeGstConvertedAmount == null) ? 0 : includeGstConvertedAmount.hashCode());
		result = prime * result
				+ ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApInvoiceAccountDetails other = (ApInvoiceAccountDetails) obj;
		if (accountCode == null) {
			if (other.accountCode != null)
				return false;
		} else if (!accountCode.equals(other.accountCode))
			return false;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (apInvoice == null) {
			if (other.apInvoice != null)
				return false;
		} else if (!apInvoice.equals(other.apInvoice))
			return false;
		if (apInvoiceClassDetailses == null) {
			if (other.apInvoiceClassDetailses != null)
				return false;
		} else if (!apInvoiceClassDetailses
				.equals(other.apInvoiceClassDetailses))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (excludeGstOriginalAmount == null) {
			if (other.excludeGstOriginalAmount != null)
				return false;
		} else if (!excludeGstOriginalAmount.equals(other.excludeGstOriginalAmount))
			return false;
		if (excludeGstConvertedAmount == null) {
			if (other.excludeGstConvertedAmount != null)
				return false;
		} else if (!excludeGstConvertedAmount
				.equals(other.excludeGstConvertedAmount))
			return false;
		if (gstConvertedAmount == null) {
			if (other.gstConvertedAmount != null)
				return false;
		} else if (!gstConvertedAmount.equals(other.gstConvertedAmount))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (includeGstConvertedAmount == null) {
			if (other.includeGstConvertedAmount != null)
				return false;
		} else if (!includeGstConvertedAmount.equals(other.includeGstConvertedAmount))
			return false;
		if (modifiedDate == null) {
			if (other.modifiedDate != null)
				return false;
		} else if (!modifiedDate.equals(other.modifiedDate))
			return false;
		return true;
	}

	
}
