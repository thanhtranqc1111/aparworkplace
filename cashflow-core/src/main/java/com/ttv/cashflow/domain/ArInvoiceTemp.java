
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllArInvoiceTemps", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp"),
		@NamedQuery(name = "findArInvoiceTempByAccountCode", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountCode = ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountCodeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountCode like ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountName", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountName = ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountNameContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountName like ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountReceivableCode", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountReceivableCode = ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountReceivableCodeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountReceivableCode like ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountReceivableName", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountReceivableName = ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountReceivableNameContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountReceivableName like ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountType", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountType = ?1"),
		@NamedQuery(name = "findArInvoiceTempByAccountTypeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.accountType like ?1"),
		@NamedQuery(name = "findArInvoiceTempByApprovalCode", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.approvalCode = ?1"),
		@NamedQuery(name = "findArInvoiceTempByApprovalCodeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.approvalCode like ?1"),
		@NamedQuery(name = "findArInvoiceTempByClaimType", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.claimType = ?1"),
		@NamedQuery(name = "findArInvoiceTempByClaimTypeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.claimType like ?1"),
		@NamedQuery(name = "findArInvoiceTempByDescription", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.description = ?1"),
		@NamedQuery(name = "findArInvoiceTempByDescriptionContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.description like ?1"),
		@NamedQuery(name = "findArInvoiceTempByExcludeGstOriginalAmount", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceTempByFxRate", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.fxRate = ?1"),
		@NamedQuery(name = "findArInvoiceTempByGstRate", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.gstRate = ?1"),
		@NamedQuery(name = "findArInvoiceTempByGstType", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.gstType = ?1"),
		@NamedQuery(name = "findArInvoiceTempByGstTypeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.gstType like ?1"),
		@NamedQuery(name = "findArInvoiceTempById", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.id = ?1"),
		@NamedQuery(name = "findArInvoiceTempByInvoiceIssuedDate", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.invoiceIssuedDate = ?1"),
		@NamedQuery(name = "findArInvoiceTempByInvoiceIssuedDateAfter", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.invoiceIssuedDate > ?1"),
		@NamedQuery(name = "findArInvoiceTempByInvoiceIssuedDateBefore", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.invoiceIssuedDate < ?1"),
		@NamedQuery(name = "findArInvoiceTempByInvoiceNo", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.invoiceNo = ?1"),
		@NamedQuery(name = "findArInvoiceTempByInvoiceNoContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.invoiceNo like ?1"),
		@NamedQuery(name = "findArInvoiceTempByMonth", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.month = ?1"),
		@NamedQuery(name = "findArInvoiceTempByMonthAfter", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.month > ?1"),
		@NamedQuery(name = "findArInvoiceTempByMonthBefore", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.month < ?1"),
		@NamedQuery(name = "findArInvoiceTempByOriginalCurrency", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.originalCurrency = ?1"),
		@NamedQuery(name = "findArInvoiceTempByOriginalCurrencyContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.originalCurrency like ?1"),
		@NamedQuery(name = "findArInvoiceTempByPayerName", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.payerName = ?1"),
		@NamedQuery(name = "findArInvoiceTempByPayerNameContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.payerName like ?1"),
		@NamedQuery(name = "findArInvoiceTempByPrimaryKey", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.id = ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectCode", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectCode = ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectCodeContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectCode like ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectDescription", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectDescription = ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectDescriptionContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectDescription like ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectName", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectName = ?1"),
		@NamedQuery(name = "findArInvoiceTempByProjectNameContaining", query = "select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp where myArInvoiceTemp.projectName like ?1") })

@Table(schema = "cashflow", name = "ar_invoice_temp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "temp/com/ttv/cashflow/domain", name = "ArInvoiceTemp")

public class ArInvoiceTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payer_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payerName;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "invoice_issued_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar invoiceIssuedDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "original_currency", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrency;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "account_receivable_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountReceivableCode;
	/**
	 */

	@Column(name = "account_receivable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountReceivableName;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "account_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountCode;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "account_type", length = 50)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountType;
	/**
	 */

	@Column(name = "project_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectCode;
	/**
	 */

	@Column(name = "project_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectName;
	/**
	 */

	@Column(name = "project_description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectDescription;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "gst_type", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	
	/**
	 */
	@Column(name = "is_approval")
    @Basic(fetch = FetchType.EAGER)
	@XmlElement
    Boolean isApproval;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	/**
	 */
	public String getPayerName() {
		return this.payerName;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}

	/**
	 */
	public Calendar getInvoiceIssuedDate() {
		return this.invoiceIssuedDate;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	/**
	 */
	public String getOriginalCurrency() {
		return this.originalCurrency;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setAccountReceivableCode(String accountReceivableCode) {
		this.accountReceivableCode = accountReceivableCode;
	}

	/**
	 */
	public String getAccountReceivableCode() {
		return this.accountReceivableCode;
	}

	/**
	 */
	public void setAccountReceivableName(String accountReceivableName) {
		this.accountReceivableName = accountReceivableName;
	}

	/**
	 */
	public String getAccountReceivableName() {
		return this.accountReceivableName;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 */
	public String getAccountCode() {
		return this.accountCode;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 */
	public String getAccountType() {
		return this.accountType;
	}

	/**
	 */
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	/**
	 */
	public String getProjectCode() {
		return this.projectCode;
	}

	/**
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 */
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	/**
	 */
	public String getProjectDescription() {
		return this.projectDescription;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}
	
	public void setIsApproval(Boolean isApproval) {
        this.isApproval = isApproval;
    }
	
	public Boolean getIsApproval() {
        return isApproval;
    }

	/**
	 */
	public ArInvoiceTemp() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArInvoiceTemp that) {
		setId(that.getId());
		setPayerName(that.getPayerName());
		setInvoiceNo(that.getInvoiceNo());
		setInvoiceIssuedDate(that.getInvoiceIssuedDate());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setOriginalCurrency(that.getOriginalCurrency());
		setFxRate(that.getFxRate());
		setAccountReceivableCode(that.getAccountReceivableCode());
		setAccountReceivableName(that.getAccountReceivableName());
		setDescription(that.getDescription());
		setAccountCode(that.getAccountCode());
		setAccountName(that.getAccountName());
		setAccountType(that.getAccountType());
		setProjectCode(that.getProjectCode());
		setProjectName(that.getProjectName());
		setProjectDescription(that.getProjectDescription());
		setApprovalCode(that.getApprovalCode());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setIsApproval(that.getIsApproval());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payerName=[").append(payerName).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("invoiceIssuedDate=[").append(invoiceIssuedDate).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("originalCurrency=[").append(originalCurrency).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("accountReceivableCode=[").append(accountReceivableCode).append("] ");
		buffer.append("accountReceivableName=[").append(accountReceivableName).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("accountCode=[").append(accountCode).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("accountType=[").append(accountType).append("] ");
		buffer.append("projectCode=[").append(projectCode).append("] ");
		buffer.append("projectName=[").append(projectName).append("] ");
		buffer.append("projectDescription=[").append(projectDescription).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArInvoiceTemp))
			return false;
		ArInvoiceTemp equalCheck = (ArInvoiceTemp) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
