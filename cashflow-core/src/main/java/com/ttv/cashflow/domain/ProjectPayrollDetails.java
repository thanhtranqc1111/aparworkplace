
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllProjectPayrollDetailss", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails"),
        @NamedQuery(name = "findProjectPayrollDetailsByCreatedDate", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.createdDate = ?1"),
        @NamedQuery(name = "findProjectPayrollDetailsById", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.id = ?1"),
        @NamedQuery(name = "findProjectPayrollDetailsByProjectCodeAndPayrollDetailId", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.projectMaster.code = ?1 and myProjectPayrollDetails.payrollDetails.id = ?2"),
        @NamedQuery(name = "findProjectPayrollDetailsByModifiedDate", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.modifiedDate = ?1"),
        @NamedQuery(name = "findProjectPayrollDetailsByPrimaryKey", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.id = ?1"),
        @NamedQuery(name = "findProjectPayrollDetailsByProjectAllocationAmount", query = "select myProjectPayrollDetails from ProjectPayrollDetails myProjectPayrollDetails where myProjectPayrollDetails.projectAllocationAmount = ?1") })

@Table(schema = "cashflow", name = "project_payroll_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "CashFlowDriver/com/ttv/cashflow/domain", name = "ProjectPayrollDetails")

public class ProjectPayrollDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     */

    @Column(name = "id", nullable = false)
    @Basic(fetch = FetchType.EAGER)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    BigInteger id;
    /**
     */

    @Column(name = "project_allocation_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal projectAllocationAmount;
    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar createdDate;
    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar modifiedDate;

    /**
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "project_code", referencedColumnName = "code") })
    @XmlTransient
    ProjectMaster projectMaster;
    /**
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "payroll_details_id", referencedColumnName = "id") })
    @XmlTransient
    PayrollDetails payrollDetails;

    /**
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     */
    public BigInteger getId() {
        return this.id;
    }

    /**
     */
    public void setProjectAllocationAmount(BigDecimal projectAllocationAmount) {
        this.projectAllocationAmount = projectAllocationAmount;
    }

    /**
     */
    public BigDecimal getProjectAllocationAmount() {
        return this.projectAllocationAmount;
    }

    /**
     */
    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    /**
     */
    public Calendar getCreatedDate() {
        return this.createdDate;
    }

    /**
     */
    public void setModifiedDate(Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    /**
     */
    public Calendar getModifiedDate() {
        return this.modifiedDate;
    }

    /**
     */
    public void setProjectMaster(ProjectMaster projectMaster) {
        this.projectMaster = projectMaster;
    }

    /**
     */
    public ProjectMaster getProjectMaster() {
        return projectMaster;
    }

    /**
     */
    public void setPayrollDetails(PayrollDetails payrollDetails) {
        this.payrollDetails = payrollDetails;
    }

    /**
     */
    public PayrollDetails getPayrollDetails() {
        return payrollDetails;
    }

    /**
     */
    public ProjectPayrollDetails() {
    }

    /**
     * Copies the contents of the specified bean into this bean.
     *
     */
    public void copy(ProjectPayrollDetails that) {
        setId(that.getId());
        setProjectAllocationAmount(that.getProjectAllocationAmount());
        setCreatedDate(that.getCreatedDate());
        setModifiedDate(that.getModifiedDate());
        setProjectMaster(that.getProjectMaster());
        setPayrollDetails(that.getPayrollDetails());
    }

    /**
     * Returns a textual representation of a bean.
     *
     */
    public String toString() {

        StringBuilder buffer = new StringBuilder();

        buffer.append("id=[").append(id).append("] ");
        buffer.append("projectAllocationAmount=[").append(projectAllocationAmount).append("] ");
        buffer.append("createdDate=[").append(createdDate).append("] ");
        buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

        return buffer.toString();
    }

    /**
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
        return result;
    }

    /**
     */
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof ProjectPayrollDetails))
            return false;
        ProjectPayrollDetails equalCheck = (ProjectPayrollDetails) obj;
        if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
            return false;
        if (id != null && !id.equals(equalCheck.id))
            return false;
        return true;
    }
}
