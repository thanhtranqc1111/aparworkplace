
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllArInvoiceClassDetailss", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails"),
		@NamedQuery(name = "findArInvoiceClassDetailsByApprovalCode", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.approvalCode = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByApprovalCodeContaining", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.approvalCode like ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByClassCode", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.classCode = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByClassCodeContaining", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.classCode like ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByClassName", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.className = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByClassNameContaining", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.className like ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByCreatedDate", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.createdDate = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByDescription", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.description = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByDescriptionContaining", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.description like ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByExcludeGstConvertedAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByExcludeGstOriginalAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByFxRate", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.fxRate = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByGstConvertedAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByGstOriginalAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.gstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByGstRate", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.gstRate = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByGstType", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.gstType = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByGstTypeContaining", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.gstType like ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsById", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.id = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByIncludeGstConvertedAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByIncludeGstOriginalAmount", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByIsApproval", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.isApproval = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByModifiedDate", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findArInvoiceClassDetailsByPrimaryKey", query = "select myArInvoiceClassDetails from ArInvoiceClassDetails myArInvoiceClassDetails where myArInvoiceClassDetails.id = ?1") })

@Table(schema = "cashflow", name = "ar_invoice_class_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "ArInvoiceClassDetails")

public class ArInvoiceClassDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "class_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String classCode;
	/**
	 */

	@Column(name = "class_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String className;
	/**
	 */

	@Column(name = "approval_code", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "gst_type", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */

	@Column(name = "gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstOriginalAmount;
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */

	@Column(name = "is_approval")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isApproval;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "ar_invoice_account_details_id", referencedColumnName = "id") })
	@XmlTransient
	ArInvoiceAccountDetails arInvoiceAccountDetails;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 */
	public String getClassCode() {
		return this.classCode;
	}

	/**
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}

	/**
	 */
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getGstOriginalAmount() {
		return this.gstOriginalAmount;
	}

	/**
	 */
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getGstConvertedAmount() {
		return this.gstConvertedAmount;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	/**
	 */
	public Boolean getIsApproval() {
		return this.isApproval;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setArInvoiceAccountDetails(ArInvoiceAccountDetails arInvoiceAccountDetails) {
		this.arInvoiceAccountDetails = arInvoiceAccountDetails;
	}

	/**
	 */
	@JsonIgnore
	public ArInvoiceAccountDetails getArInvoiceAccountDetails() {
		return arInvoiceAccountDetails;
	}

	/**
	 */
	public ArInvoiceClassDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArInvoiceClassDetails that) {
		setId(that.getId());
		setClassCode(that.getClassCode());
		setClassName(that.getClassName());
		setApprovalCode(that.getApprovalCode());
		setDescription(that.getDescription());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setFxRate(that.getFxRate());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setGstOriginalAmount(that.getGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setIsApproval(that.getIsApproval());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setArInvoiceAccountDetails(that.getArInvoiceAccountDetails());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("classCode=[").append(classCode).append("] ");
		buffer.append("className=[").append(className).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("gstOriginalAmount=[").append(gstOriginalAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("isApproval=[").append(isApproval).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArInvoiceClassDetails))
			return false;
		ArInvoiceClassDetails equalCheck = (ArInvoiceClassDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
