
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Calendar;
import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllSubsidiaryUserDetails", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail"),
		@NamedQuery(name = "findSubsidiaryUserDetailByCreatedDate", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.createdDate = ?1"),
		@NamedQuery(name = "findSubsidiaryUserDetailById", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.id = ?1"),
		@NamedQuery(name = "findSubsidiaryUserDetailByModifiedDate", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.modifiedDate = ?1"),
		@NamedQuery(name = "findSubsidiaryUserDetailByPrimaryKey", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.id = ?1"),
		@NamedQuery(name = "findSubsidiaryUserDetailByUserId", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.userAccount.id = ?1 and  mySubsidiaryUserDetail.subsidiary.isActive = true"),
		@NamedQuery(name = "findSubsidiaryUserBySubsidiaryIdAndUserId", query = "select mySubsidiaryUserDetail from SubsidiaryUserDetail mySubsidiaryUserDetail where mySubsidiaryUserDetail.subsidiary.id = ?1 and mySubsidiaryUserDetail.userAccount.id = ?2")
		})

@Table(schema = "cashflow", name = "subsidiary_user_detail")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "SubsidiaryUserDetail")

public class SubsidiaryUserDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	
	/**
	 */
	@Column(name = "is_default", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isDefault;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "subsidiary_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	Subsidiary subsidiary;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "user_account_id", referencedColumnName = "id", nullable = false) })
	@XmlTransient
	UserAccount userAccount;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}
	
	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setSubsidiary(Subsidiary subsidiary) {
		this.subsidiary = subsidiary;
	}

	/**
	 */
	public Subsidiary getSubsidiary() {
		return subsidiary;
	}

	/**
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	/**
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	/**
	 */
	public SubsidiaryUserDetail() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(SubsidiaryUserDetail that) {
		setId(that.getId());
		setIsDefault(that.getIsDefault());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setSubsidiary(that.getSubsidiary());
		setUserAccount(that.getUserAccount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("is_default=[").append(isDefault).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof SubsidiaryUserDetail))
			return false;
		SubsidiaryUserDetail equalCheck = (SubsidiaryUserDetail) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
