
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ttv.cashflow.dto.DivisionMasterDTO;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllDivisionMasters", query = "select myDivisionMaster from DivisionMaster myDivisionMaster"),
		@NamedQuery(name = "findDivisionMasterByCode", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.code = ?1"),
		@NamedQuery(name = "findDivisionMasterByCodeContaining", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.code like ?1"),
		@NamedQuery(name = "findDivisionMasterById", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.id = ?1"),
		@NamedQuery(name = "findDivisionMasterByName", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.name = ?1"),
		@NamedQuery(name = "findDivisionMasterByNameContaining", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.name like ?1"),
		@NamedQuery(name = "findDivisionMasterByPrimaryKey", query = "select myDivisionMaster from DivisionMaster myDivisionMaster where myDivisionMaster.id = ?1") })

@SqlResultSetMappings({
    @SqlResultSetMapping(name = "DivisionMasterDTO", classes = {
        @ConstructorResult(targetClass = DivisionMasterDTO.class, 
            columns = { 
                @ColumnResult(name = "id", type = BigInteger.class),
                @ColumnResult(name = "name"),
                @ColumnResult(name = "code")
        })
    })
})
@Table(schema = "cashflow", name = "division_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "DivisionMaster")
@XmlRootElement(namespace = "cashflow_generate/com/ttv/cashflow/domain")
public class DivisionMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "name", length = 200)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String name;

	/**
	 */
	@OneToMany(mappedBy = "divisionMaster", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.EmployeeMaster> employeeMasters;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setEmployeeMasters(Set<EmployeeMaster> employeeMasters) {
		this.employeeMasters = employeeMasters;
	}

	/**
	 */
	public Set<EmployeeMaster> getEmployeeMasters() {
		if (employeeMasters == null) {
			employeeMasters = new java.util.LinkedHashSet<com.ttv.cashflow.domain.EmployeeMaster>();
		}
		return employeeMasters;
	}

	/**
	 */
	public DivisionMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(DivisionMaster that) {
		setId(that.getId());
		setCode(that.getCode());
		setName(that.getName());
		setEmployeeMasters(new java.util.LinkedHashSet<com.ttv.cashflow.domain.EmployeeMaster>(that.getEmployeeMasters()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof DivisionMaster))
			return false;
		DivisionMaster equalCheck = (DivisionMaster) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
