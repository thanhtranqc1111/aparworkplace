
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllBankStatements", query = "select myBankStatement from BankStatement myBankStatement"),
		@NamedQuery(name = "findBankStatementByBankAccountNo", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.bankAccountNo = ?1"),
		@NamedQuery(name = "findBankStatementByBankAccountNoContaining", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.bankAccountNo like ?1"),
		@NamedQuery(name = "findBankStatementByBankName", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.bankName = ?1"),
		@NamedQuery(name = "findBankStatementByBankNameContaining", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.bankName like ?1"),
		@NamedQuery(name = "findBankStatementByCreatedDate", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.createdDate = ?1"),
		@NamedQuery(name = "findBankStatementByCredit", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.credit = ?1"),
		@NamedQuery(name = "findBankStatementByDebit", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.debit = ?1"),
		@NamedQuery(name = "findBankStatementByDescription1", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.description1 = ?1"),
		@NamedQuery(name = "findBankStatementByDescription1Containing", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.description1 like ?1"),
		@NamedQuery(name = "findBankStatementByDescription2", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.description2 = ?1"),
		@NamedQuery(name = "findBankStatementByDescription2Containing", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.description2 like ?1"),
		@NamedQuery(name = "findBankStatementById", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.id = ?1"),
		@NamedQuery(name = "findBankStatementByModifiedDate", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.modifiedDate = ?1"),
		@NamedQuery(name = "findBankStatementByPaymentOrderNo", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.paymentOrderNo = ?1"),
		@NamedQuery(name = "findBankStatementByPaymentOrderNoContaining", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.paymentOrderNo like ?1"),
		@NamedQuery(name = "findBankStatementByPrimaryKey", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.id = ?1"),
		@NamedQuery(name = "findBankStatementByReference1", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.reference1 = ?1"),
		@NamedQuery(name = "findBankStatementByReference1Containing", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.reference1 like ?1"),
		@NamedQuery(name = "findBankStatementByReference2", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.reference2 = ?1"),
		@NamedQuery(name = "findBankStatementByReference2Containing", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.reference2 like ?1"),
		@NamedQuery(name = "findBankStatementByTransactionDate", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.transactionDate = ?1"),
		@NamedQuery(name = "findBankStatementByValueDate", query = "select myBankStatement from BankStatement myBankStatement where myBankStatement.valueDate = ?1"),
		@NamedQuery(name = "countBankStatementAll", query = "select count(1) from BankStatement"),
		@NamedQuery(name = "countBankStatementPaging", query = "select count(1) from BankStatement " 
																+ "where bankName = coalesce(cast(?1 as text),bankName) "
																+ "and bankAccountNo = coalesce(cast(?2 as text), bankAccountNo) "
																+ "and transactionDate >= cast(?3 as date) and transactionDate <= cast(?4 as date)"),
		@NamedQuery(name = "checkBankStatementExists", query = "select count(1) from BankStatement " 
															 +  "where transactionDate = cast(?1 as date) "
															 +  "and debit = ?2 "
															 +  "and credit = ?3 "
															 +  "and bankName = ?4 "
															 +  "and bankAccountNo = ?5"),
		@NamedQuery(name = "getMaxTransactionDate", query = "select max(myBankStatement.transactionDate) from BankStatement myBankStatement where myBankStatement.bankName = ?1 and myBankStatement.bankAccountNo = ?2"),
})

@Table(schema = "cashflow", name = "bank_statement")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "BankStatement")

public class BankStatement implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "bank_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankName;
	/**
	 */

	@Column(name = "bank_account_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankAccountNo;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar transactionDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "value_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar valueDate;
	/**
	 */

	@Column(name = "description1")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description1;
	/**
	 */

	@Column(name = "description2")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description2;
	/**
	 */

	@Column(name = "reference1", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String reference1;
	/**
	 */

	@Column(name = "reference2", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String reference2;
	/**
	 */

	@Column(name = "debit", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal debit;
	/**
	 */

	@Column(name = "credit", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal credit;

	/**
	 */
	@Column(name = "payment_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;
	
	/**
	 */
	@Column(name = "balance", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal balance;
	
	/**
	 */
	@Column(name = "beginning_balance", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal beginningBalance;
	
	/**
	 */
	@Transient
	BigInteger paymentOrderId;
	
	/**
	 */
	@Column(name = "currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currencyCode;
	
	/**
	 */
	@Column(name = "receipt_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String receiptOrderNo;
	
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 */
	public String getBankName() {
		return this.bankName;
	}

	/**
	 */
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	/**
	 */
	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	/**
	 */
	public void setTransactionDate(Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 */
	public Calendar getTransactionDate() {
		return this.transactionDate;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	/**
	 */
	public String getDescription1() {
		return this.description1;
	}

	/**
	 */
	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	/**
	 */
	public String getDescription2() {
		return this.description2;
	}

	/**
	 */
	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	/**
	 */
	public String getReference1() {
		return this.reference1;
	}

	/**
	 */
	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	/**
	 */
	public String getReference2() {
		return this.reference2;
	}

	/**
	 */
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	/**
	 */
	public BigDecimal getDebit() {
		return this.debit;
	}

	/**
	 */
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	/**
	 */
	public BigDecimal getCredit() {
		return this.credit;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}
	
	public BigInteger getPaymentOrderId() {
		return paymentOrderId;
	}

	public void setPaymentOrderId(BigInteger paymentOrderId) {
		this.paymentOrderId = paymentOrderId;
	}
	
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public BigDecimal getBeginningBalance() {
		return beginningBalance;
	}

	public void setBeginningBalance(BigDecimal beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
	
	public String getReceiptOrderNo() {
		return receiptOrderNo;
	}

	public void setReceiptOrderNo(String receiptOrderNo) {
		this.receiptOrderNo = receiptOrderNo;
	}

	/**
	 */
	public BankStatement() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(BankStatement that) {
		setId(that.getId());
		setBankName(that.getBankName());
		setBankAccountNo(that.getBankAccountNo());
		setTransactionDate(that.getTransactionDate());
		setValueDate(that.getValueDate());
		setDescription1(that.getDescription1());
		setDescription2(that.getDescription2());
		setReference1(that.getReference1());
		setReference2(that.getReference2());
		setDebit(that.getDebit());
		setCredit(that.getCredit());
		setPaymentOrderNo(that.getPaymentOrderNo());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setCurrencyCode(that.getCurrencyCode());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("bankName=[").append(bankName).append("] ");
		buffer.append("bankAccountNo=[").append(bankAccountNo).append("] ");
		buffer.append("transactionDate=[").append(transactionDate).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("description1=[").append(description1).append("] ");
		buffer.append("description2=[").append(description2).append("] ");
		buffer.append("reference1=[").append(reference1).append("] ");
		buffer.append("reference2=[").append(reference2).append("] ");
		buffer.append("debit=[").append(debit).append("] ");
		buffer.append("credit=[").append(credit).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");
		buffer.append("currencyCode=[").append(currencyCode).append("] ");
		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof BankStatement))
			return false;
		BankStatement equalCheck = (BankStatement) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
