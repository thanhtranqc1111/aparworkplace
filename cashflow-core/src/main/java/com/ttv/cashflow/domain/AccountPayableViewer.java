
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAccountPayableViewerByAccountName", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.accountName = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByAccountNameContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.accountName like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByAccountPayableNo", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.accountPayableNo = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByAccountPayableNoContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.accountPayableNo like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByApprovalCode", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.approvalCode = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByApprovalCodeContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.approvalCode like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByClaimType", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.claimType = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByClaimTypeContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.claimType like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByConvertedAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.convertedAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByCurrency", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.currency = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByCurrencyContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.currency like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByDescription", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.description = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByDescriptionContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.description like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByFxRate", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.fxRate = ?1"),
		@NamedQuery(name = "findAccountPayableViewerById", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.id = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByInvoiceNo", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.invoiceNo = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByInvoiceNoContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.invoiceNo like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByMonth", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.month = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByMonthAfter", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.month > ?1"),
		@NamedQuery(name = "findAccountPayableViewerByMonthBefore", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.month < ?1"),
		@NamedQuery(name = "findAccountPayableViewerByOriginalAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.originalAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentConvertedAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentConvertedAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentOrderNo", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentOrderNo = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentOrderNoContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentOrderNo like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentOriginalAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentOriginalAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentRate", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentRate = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPrimaryKey", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.id = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByProjectName", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.projectName = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByProjectNameContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.projectName like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByRemainAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.remainAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByValueDate", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.valueDate = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByVarianceAmount", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.varianceAmount = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByVendor", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.vendor = ?1"),
		@NamedQuery(name = "findAccountPayableViewerByVendorContaining", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.vendor like ?1"),
		@NamedQuery(name = "findAccountPayableViewerByPaymentOrderNoList", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.paymentOrderNo IN (?1) "),
		@NamedQuery(name = "findAccountPayableViewerByAccountPayableNoList", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer where myAccountPayableViewer.accountPayableNo IN (?1) "),
		@NamedQuery(name = "findAllAccountPayableViewers", query = "select myAccountPayableViewer from AccountPayableViewer myAccountPayableViewer") })

@Table(schema = "cashflow", name = "account_payable_viewer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "first/com/ttv/cashflow/domain", name = "AccountPayableViewer")

public class AccountPayableViewer implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "account_payable_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "vendor", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String vendor;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "currency", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currency;
	/**
	 */

	@Column(name = "original_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal originalAmount;
	/**
	 */

	@Column(name = "fx_rate")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "converted_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal convertedAmount;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "project_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String projectName;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "payment_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "value_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar valueDate;
	/**
	 */

	@Column(name = "payment_original_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentOriginalAmount;
	/**
	 */

	@Column(name = "payment_rate")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */

	@Column(name = "payment_converted_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentConvertedAmount;
	/**
	 */

	@Column(name = "variance_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "remain_amount")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal remainAmount;
	
	@Column(name = "account_payable_type", length = 2)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    String accountPayableType;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setAccountPayableNo(String accountPayableNo) {
		this.accountPayableNo = accountPayableNo;
	}

	/**
	 */
	public String getAccountPayableNo() {
		return this.accountPayableNo;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 */
	public String getVendor() {
		return this.vendor;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 */
	public String getCurrency() {
		return this.currency;
	}

	/**
	 */
	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	/**
	 */
	public BigDecimal getOriginalAmount() {
		return this.originalAmount;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setConvertedAmount(BigDecimal convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

	/**
	 */
	public BigDecimal getConvertedAmount() {
		return this.convertedAmount;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setPaymentOriginalAmount(BigDecimal paymentOriginalAmount) {
		this.paymentOriginalAmount = paymentOriginalAmount;
	}

	/**
	 */
	public BigDecimal getPaymentOriginalAmount() {
		return this.paymentOriginalAmount;
	}

	/**
	 */
	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public BigDecimal getPaymentRate() {
		return this.paymentRate;
	}

	/**
	 */
	public void setPaymentConvertedAmount(BigDecimal paymentConvertedAmount) {
		this.paymentConvertedAmount = paymentConvertedAmount;
	}

	/**
	 */
	public BigDecimal getPaymentConvertedAmount() {
		return this.paymentConvertedAmount;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}

	/**
	 */
	public BigDecimal getRemainAmount() {
		return this.remainAmount;
	}
	
	public String getAccountPayableType() {
        return accountPayableType;
    }
	
	public void setAccountPayableType(String accountPayableType) {
        this.accountPayableType = accountPayableType;
    }

	/**
	 */
	public AccountPayableViewer() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(AccountPayableViewer that) {
		setId(that.getId());
		setAccountPayableNo(that.getAccountPayableNo());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setVendor(that.getVendor());
		setInvoiceNo(that.getInvoiceNo());
		setApprovalCode(that.getApprovalCode());
		setCurrency(that.getCurrency());
		setOriginalAmount(that.getOriginalAmount());
		setFxRate(that.getFxRate());
		setConvertedAmount(that.getConvertedAmount());
		setAccountName(that.getAccountName());
		setProjectName(that.getProjectName());
		setDescription(that.getDescription());
		setPaymentOrderNo(that.getPaymentOrderNo());
		setValueDate(that.getValueDate());
		setPaymentOriginalAmount(that.getPaymentOriginalAmount());
		setPaymentRate(that.getPaymentRate());
		setPaymentConvertedAmount(that.getPaymentConvertedAmount());
		setVarianceAmount(that.getVarianceAmount());
		setRemainAmount(that.getRemainAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("accountPayableNo=[").append(accountPayableNo).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("vendor=[").append(vendor).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("currency=[").append(currency).append("] ");
		buffer.append("originalAmount=[").append(originalAmount).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("convertedAmount=[").append(convertedAmount).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("projectName=[").append(projectName).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("paymentOriginalAmount=[").append(paymentOriginalAmount).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("paymentConvertedAmount=[").append(paymentConvertedAmount).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("remainAmount=[").append(remainAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof AccountPayableViewer))
			return false;
		AccountPayableViewer equalCheck = (AccountPayableViewer) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
