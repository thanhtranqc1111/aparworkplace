
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllSystemValues", query = "select mySystemValue from SystemValue mySystemValue"),
		@NamedQuery(name = "findSystemValueByCode", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.code = ?1"),
		@NamedQuery(name = "findSystemValueByCodeContaining", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.code like ?1"),
		@NamedQuery(name = "findSystemValueByDescription", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.description = ?1"),
		@NamedQuery(name = "findSystemValueByDescriptionContaining", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.description like ?1"),
		@NamedQuery(name = "findSystemValueById", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.id = ?1"),
		@NamedQuery(name = "findSystemValueByPrimaryKey", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.id = ?1"),
		@NamedQuery(name = "findSystemValueByType", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.type = ?1"),
		@NamedQuery(name = "findSystemValueByTypeContaining", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.type like ?1"),
		@NamedQuery(name = "findSystemValueByValue", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.value = ?1"),
		@NamedQuery(name = "findSystemValueByValueContaining", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.value like ?1"),
		@NamedQuery(name = "findSystemValueByTypeAndCode", query = "select mySystemValue from SystemValue mySystemValue where mySystemValue.type = ?1 and mySystemValue.code = ?2")
		})

@Table(schema = "cashflow", name = "system_value")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "SystemValue")

public class SystemValue implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "type", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String type;
	/**
	 */

	@Column(name = "code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String code;
	/**
	 */

	@Column(name = "value", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String value;
	/**
	 */

	@Column(name = "description", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 */
	public String getType() {
		return this.type;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public SystemValue() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(SystemValue that) {
		setId(that.getId());
		setType(that.getType());
		setCode(that.getCode());
		setValue(that.getValue());
		setDescription(that.getDescription());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("type=[").append(type).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("description=[").append(description).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof SystemValue))
			return false;
		SystemValue equalCheck = (SystemValue) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
