
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayrollDetailss", query = "select myPayrollDetails from PayrollDetails myPayrollDetails"),
		@NamedQuery(name = "findPayrollDetailsByApprovalCode", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.approvalCode = ?1"),
		@NamedQuery(name = "findPayrollDetailsByApprovalCodeContaining", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.approvalCode like ?1"),
		@NamedQuery(name = "findPayrollDetailsByCreatedDate", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.createdDate = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionOther1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionOther1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionOther2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionOther2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionOther3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionOther3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployee1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployee1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployee2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployee2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployee3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployee3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployer1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployer1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployer2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployer2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionSocialInsuranceEmployer3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionSocialInsuranceEmployer3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDeductionWht", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.deductionWht = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDivision", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.division = ?1"),
		@NamedQuery(name = "findPayrollDetailsByDivisionContaining", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.division like ?1"),
		@NamedQuery(name = "findPayrollDetailsByEmployeeCode", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.employeeCode = ?1"),
		@NamedQuery(name = "findPayrollDetailsByEmployeeCodeAndMonth", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.employeeCode in (?1) and (myPayrollDetails.payroll.month >= ?2 and myPayrollDetails.payroll.month <=?3) and myPayrollDetails.payroll.status <> '004'"),
		@NamedQuery(name = "findPayrollDetailsByEmployeeCodeContaining", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.employeeCode like ?1"),
		@NamedQuery(name = "findPayrollDetailsById", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.id = ?1"),
		@NamedQuery(name = "findPayrollDetailsByIsApproval", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.isApproval = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborAdditionalPayment1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborAdditionalPayment1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborAdditionalPayment2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborAdditionalPayment2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborAdditionalPayment3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborAdditionalPayment3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborBaseSalaryPayment", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborBaseSalaryPayment = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborOtherPayment1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborOtherPayment1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborOtherPayment2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborOtherPayment2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborOtherPayment3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborOtherPayment3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborSocialInsuranceEmployer1", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborSocialInsuranceEmployer1 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborSocialInsuranceEmployer2", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborSocialInsuranceEmployer2 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByLaborSocialInsuranceEmployer3", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.laborSocialInsuranceEmployer3 = ?1"),
		@NamedQuery(name = "findPayrollDetailsByModifiedDate", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findPayrollDetailsByNetPaymentOriginal", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.netPaymentOriginal = ?1"),
		@NamedQuery(name = "findPayrollDetailsByPrimaryKey", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.id = ?1"),
		@NamedQuery(name = "findPayrollDetailsByRoleTitle", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.roleTitle = ?1"),
		@NamedQuery(name = "findPayrollDetailsByRoleTitleContaining", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.roleTitle like ?1"),
		@NamedQuery(name = "findPayrollDetailsByTeam", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.team = ?1"),
		@NamedQuery(name = "findPayrollDetailsByTeamContaining", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.team like ?1"),
		@NamedQuery(name = "findPayrollDetailsByTotalDeductionOriginal", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.totalDeductionOriginal = ?1"),
		@NamedQuery(name = "findPayrollDetailsByTotalLaborCostOriginal", query = "select myPayrollDetails from PayrollDetails myPayrollDetails where myPayrollDetails.totalLaborCostOriginal = ?1") })

@Table(schema = "cashflow", name = "payroll_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "PayrollDetails")

public class PayrollDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "employee_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String employeeCode;
	/**
	 */

	@Column(name = "role_title", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String roleTitle;
	/**
	 */

	@Column(name = "division", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String division;
	/**
	 */

	@Column(name = "team", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String team;
	/**
	 */

	@Column(name = "labor_base_salary_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborBaseSalaryPayment;
	/**
	 */

	@Column(name = "labor_additional_payment_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment1;
	/**
	 */

	@Column(name = "labor_additional_payment_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment2;
	/**
	 */

	@Column(name = "labor_additional_payment_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment3;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer1;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer2;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer3;
	/**
	 */

	@Column(name = "labor_other_payment_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment1;
	/**
	 */

	@Column(name = "labor_other_payment_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment2;
	/**
	 */

	@Column(name = "labor_other_payment_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment3;
	/**
	 */

	@Column(name = "total_labor_cost_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalLaborCostOriginal;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer1;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer2;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer3;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee1;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee2;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee3;
	/**
	 */

	@Column(name = "deduction_wht", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionWht;
	/**
	 */

	@Column(name = "deduction_other_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther1;
	/**
	 */

	@Column(name = "deduction_other_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther2;
	/**
	 */

	@Column(name = "deduction_other_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther3;
	/**
	 */

	@Column(name = "total_deduction_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalDeductionOriginal;
	/**
	 */

	@Column(name = "net_payment_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentOriginal;
	/**
	 */
	
	@Column(name = "net_payment_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentConverted;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "is_approval")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isApproval;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "payroll_id", referencedColumnName = "id") })
	@XmlTransient
	Payroll payroll;
	

    /**
     */
    @OneToMany(mappedBy = "payrollDetails", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

    @XmlElement(name = "", namespace = "")
    java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> projectPayrollDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 */
	public String getEmployeeCode() {
		return this.employeeCode;
	}

	/**
	 */
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}

	/**
	 */
	public String getRoleTitle() {
		return this.roleTitle;
	}

	/**
	 */
	public void setDivision(String division) {
		this.division = division;
	}

	/**
	 */
	public String getDivision() {
		return this.division;
	}

	/**
	 */
	public void setTeam(String team) {
		this.team = team;
	}

	/**
	 */
	public String getTeam() {
		return this.team;
	}

	/**
	 */
	public void setLaborBaseSalaryPayment(BigDecimal laborBaseSalaryPayment) {
		this.laborBaseSalaryPayment = laborBaseSalaryPayment;
	}

	/**
	 */
	public BigDecimal getLaborBaseSalaryPayment() {
		return this.laborBaseSalaryPayment;
	}

	/**
	 */
	public void setLaborAdditionalPayment1(BigDecimal laborAdditionalPayment1) {
		this.laborAdditionalPayment1 = laborAdditionalPayment1;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment1() {
		return this.laborAdditionalPayment1;
	}

	/**
	 */
	public void setLaborAdditionalPayment2(BigDecimal laborAdditionalPayment2) {
		this.laborAdditionalPayment2 = laborAdditionalPayment2;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment2() {
		return this.laborAdditionalPayment2;
	}

	/**
	 */
	public void setLaborAdditionalPayment3(BigDecimal laborAdditionalPayment3) {
		this.laborAdditionalPayment3 = laborAdditionalPayment3;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment3() {
		return this.laborAdditionalPayment3;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer1(BigDecimal laborSocialInsuranceEmployer1) {
		this.laborSocialInsuranceEmployer1 = laborSocialInsuranceEmployer1;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer1() {
		return this.laborSocialInsuranceEmployer1;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer2(BigDecimal laborSocialInsuranceEmployer2) {
		this.laborSocialInsuranceEmployer2 = laborSocialInsuranceEmployer2;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer2() {
		return this.laborSocialInsuranceEmployer2;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer3(BigDecimal laborSocialInsuranceEmployer3) {
		this.laborSocialInsuranceEmployer3 = laborSocialInsuranceEmployer3;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer3() {
		return this.laborSocialInsuranceEmployer3;
	}

	/**
	 */
	public void setLaborOtherPayment1(BigDecimal laborOtherPayment1) {
		this.laborOtherPayment1 = laborOtherPayment1;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment1() {
		return this.laborOtherPayment1;
	}

	/**
	 */
	public void setLaborOtherPayment2(BigDecimal laborOtherPayment2) {
		this.laborOtherPayment2 = laborOtherPayment2;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment2() {
		return this.laborOtherPayment2;
	}

	/**
	 */
	public void setLaborOtherPayment3(BigDecimal laborOtherPayment3) {
		this.laborOtherPayment3 = laborOtherPayment3;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment3() {
		return this.laborOtherPayment3;
	}

	

	public BigDecimal getTotalLaborCostOriginal() {
		return totalLaborCostOriginal;
	}

	public void setTotalLaborCostOriginal(BigDecimal totalLaborCostOriginal) {
		this.totalLaborCostOriginal = totalLaborCostOriginal;
	}

	public BigDecimal getTotalDeductionOriginal() {
		return totalDeductionOriginal;
	}

	public void setTotalDeductionOriginal(BigDecimal totalDeductionOriginal) {
		this.totalDeductionOriginal = totalDeductionOriginal;
	}

	public BigDecimal getNetPaymentOriginal() {
		return netPaymentOriginal;
	}

	public void setNetPaymentOriginal(BigDecimal netPaymentOriginal) {
		this.netPaymentOriginal = netPaymentOriginal;
	}

	public BigDecimal getNetPaymentConverted() {
		return netPaymentConverted;
	}

	public void setNetPaymentConverted(BigDecimal netPaymentConverted) {
		this.netPaymentConverted = netPaymentConverted;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer1(BigDecimal deductionSocialInsuranceEmployer1) {
		this.deductionSocialInsuranceEmployer1 = deductionSocialInsuranceEmployer1;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer1() {
		return this.deductionSocialInsuranceEmployer1;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer2(BigDecimal deductionSocialInsuranceEmployer2) {
		this.deductionSocialInsuranceEmployer2 = deductionSocialInsuranceEmployer2;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer2() {
		return this.deductionSocialInsuranceEmployer2;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer3(BigDecimal deductionSocialInsuranceEmployer3) {
		this.deductionSocialInsuranceEmployer3 = deductionSocialInsuranceEmployer3;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer3() {
		return this.deductionSocialInsuranceEmployer3;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee1(BigDecimal deductionSocialInsuranceEmployee1) {
		this.deductionSocialInsuranceEmployee1 = deductionSocialInsuranceEmployee1;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee1() {
		return this.deductionSocialInsuranceEmployee1;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee2(BigDecimal deductionSocialInsuranceEmployee2) {
		this.deductionSocialInsuranceEmployee2 = deductionSocialInsuranceEmployee2;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee2() {
		return this.deductionSocialInsuranceEmployee2;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee3(BigDecimal deductionSocialInsuranceEmployee3) {
		this.deductionSocialInsuranceEmployee3 = deductionSocialInsuranceEmployee3;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee3() {
		return this.deductionSocialInsuranceEmployee3;
	}

	/**
	 */
	public void setDeductionWht(BigDecimal deductionWht) {
		this.deductionWht = deductionWht;
	}

	/**
	 */
	public BigDecimal getDeductionWht() {
		return this.deductionWht;
	}

	/**
	 */
	public void setDeductionOther1(BigDecimal deductionOther1) {
		this.deductionOther1 = deductionOther1;
	}

	/**
	 */
	public BigDecimal getDeductionOther1() {
		return this.deductionOther1;
	}

	/**
	 */
	public void setDeductionOther2(BigDecimal deductionOther2) {
		this.deductionOther2 = deductionOther2;
	}

	/**
	 */
	public BigDecimal getDeductionOther2() {
		return this.deductionOther2;
	}

	/**
	 */
	public void setDeductionOther3(BigDecimal deductionOther3) {
		this.deductionOther3 = deductionOther3;
	}

	/**
	 */
	public BigDecimal getDeductionOther3() {
		return this.deductionOther3;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	/**
	 */
	public Boolean getIsApproval() {
		return this.isApproval;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}

	/**
	 */
	public Payroll getPayroll() {
		return payroll;
	}
	
	public void setProjectPayrollDetailses(java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> projectPayrollDetailses) {
        this.projectPayrollDetailses = projectPayrollDetailses;
    }
	
	public java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> getProjectPayrollDetailses() {
        return projectPayrollDetailses;
    }

	/**
	 */
	public PayrollDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PayrollDetails that) {
		setId(that.getId());
		setEmployeeCode(that.getEmployeeCode());
		setRoleTitle(that.getRoleTitle());
		setDivision(that.getDivision());
		setTeam(that.getTeam());
		setLaborBaseSalaryPayment(that.getLaborBaseSalaryPayment());
		setLaborAdditionalPayment1(that.getLaborAdditionalPayment1());
		setLaborAdditionalPayment2(that.getLaborAdditionalPayment2());
		setLaborAdditionalPayment3(that.getLaborAdditionalPayment3());
		setLaborSocialInsuranceEmployer1(that.getLaborSocialInsuranceEmployer1());
		setLaborSocialInsuranceEmployer2(that.getLaborSocialInsuranceEmployer2());
		setLaborSocialInsuranceEmployer3(that.getLaborSocialInsuranceEmployer3());
		setLaborOtherPayment1(that.getLaborOtherPayment1());
		setLaborOtherPayment2(that.getLaborOtherPayment2());
		setLaborOtherPayment3(that.getLaborOtherPayment3());
		setTotalLaborCostOriginal(that.getTotalLaborCostOriginal());
		setDeductionSocialInsuranceEmployer1(that.getDeductionSocialInsuranceEmployer1());
		setDeductionSocialInsuranceEmployer2(that.getDeductionSocialInsuranceEmployer2());
		setDeductionSocialInsuranceEmployer3(that.getDeductionSocialInsuranceEmployer3());
		setDeductionSocialInsuranceEmployee1(that.getDeductionSocialInsuranceEmployee1());
		setDeductionSocialInsuranceEmployee2(that.getDeductionSocialInsuranceEmployee2());
		setDeductionSocialInsuranceEmployee3(that.getDeductionSocialInsuranceEmployee3());
		setDeductionWht(that.getDeductionWht());
		setDeductionOther1(that.getDeductionOther1());
		setDeductionOther2(that.getDeductionOther2());
		setDeductionOther3(that.getDeductionOther3());
		setTotalDeductionOriginal(that.getTotalDeductionOriginal());
		setNetPaymentOriginal(that.getNetPaymentOriginal());
		setNetPaymentConverted(that.getNetPaymentConverted());
		setApprovalCode(that.getApprovalCode());
		setIsApproval(that.getIsApproval());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setPayroll(that.getPayroll());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("employeeCode=[").append(employeeCode).append("] ");
		buffer.append("roleTitle=[").append(roleTitle).append("] ");
		buffer.append("division=[").append(division).append("] ");
		buffer.append("team=[").append(team).append("] ");
		buffer.append("laborBaseSalaryPayment=[").append(laborBaseSalaryPayment).append("] ");
		buffer.append("laborAdditionalPayment1=[").append(laborAdditionalPayment1).append("] ");
		buffer.append("laborAdditionalPayment2=[").append(laborAdditionalPayment2).append("] ");
		buffer.append("laborAdditionalPayment3=[").append(laborAdditionalPayment3).append("] ");
		buffer.append("laborSocialInsuranceEmployer1=[").append(laborSocialInsuranceEmployer1).append("] ");
		buffer.append("laborSocialInsuranceEmployer2=[").append(laborSocialInsuranceEmployer2).append("] ");
		buffer.append("laborSocialInsuranceEmployer3=[").append(laborSocialInsuranceEmployer3).append("] ");
		buffer.append("laborOtherPayment1=[").append(laborOtherPayment1).append("] ");
		buffer.append("laborOtherPayment2=[").append(laborOtherPayment2).append("] ");
		buffer.append("laborOtherPayment3=[").append(laborOtherPayment3).append("] ");
		buffer.append("totalLaborCostOriginal=[").append(totalLaborCostOriginal).append("] ");
		buffer.append("deductionSocialInsuranceEmployer1=[").append(deductionSocialInsuranceEmployer1).append("] ");
		buffer.append("deductionSocialInsuranceEmployer2=[").append(deductionSocialInsuranceEmployer2).append("] ");
		buffer.append("deductionSocialInsuranceEmployer3=[").append(deductionSocialInsuranceEmployer3).append("] ");
		buffer.append("deductionSocialInsuranceEmployee1=[").append(deductionSocialInsuranceEmployee1).append("] ");
		buffer.append("deductionSocialInsuranceEmployee2=[").append(deductionSocialInsuranceEmployee2).append("] ");
		buffer.append("deductionSocialInsuranceEmployee3=[").append(deductionSocialInsuranceEmployee3).append("] ");
		buffer.append("deductionWht=[").append(deductionWht).append("] ");
		buffer.append("deductionOther1=[").append(deductionOther1).append("] ");
		buffer.append("deductionOther2=[").append(deductionOther2).append("] ");
		buffer.append("deductionOther3=[").append(deductionOther3).append("] ");
		buffer.append("totalDeductionOriginal=[").append(totalDeductionOriginal).append("] ");
		buffer.append("netPaymentOriginal=[").append(netPaymentOriginal).append("] ");
		buffer.append("netPaymentConverted=[").append(netPaymentConverted).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("isApproval=[").append(isApproval).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PayrollDetails))
			return false;
		PayrollDetails equalCheck = (PayrollDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
