
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;
import javax.xml.bind.annotation.*;
import com.ttv.cashflow.dto.ReimbursementPaymentDTO;
import javax.persistence.*;


/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllReimbursements", query = "select myReimbursement from Reimbursement myReimbursement"),
		@NamedQuery(name = "findReimbursementByAccountPayableCode", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.accountPayableCode = ?1"),
		@NamedQuery(name = "findReimbursementByAccountPayableCodeContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.accountPayableCode like ?1"),
		@NamedQuery(name = "findReimbursementByAccountPayableName", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.accountPayableName = ?1"),
		@NamedQuery(name = "findReimbursementByAccountPayableNameContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.accountPayableName like ?1"),
		@NamedQuery(name = "findReimbursementByBookingDate", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.bookingDate = ?1"),
		@NamedQuery(name = "findReimbursementByBookingDateAfter", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.bookingDate > ?1"),
		@NamedQuery(name = "findReimbursementByBookingDateBefore", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.bookingDate < ?1"),
		@NamedQuery(name = "findReimbursementByClaimType", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.claimType = ?1"),
		@NamedQuery(name = "findReimbursementByClaimTypeContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.claimType like ?1"),
		@NamedQuery(name = "findReimbursementByCreatedDate", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.createdDate = ?1"),
		@NamedQuery(name = "findReimbursementByDescription", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.description = ?1"),
		@NamedQuery(name = "findReimbursementByDescriptionContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.description like ?1"),
		@NamedQuery(name = "findReimbursementByEmployeeCode", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.employeeCode = ?1"),
		@NamedQuery(name = "findReimbursementByEmployeeCodeContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.employeeCode like ?1"),
		@NamedQuery(name = "findReimbursementByEmployeeName", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.employeeName = ?1"),
		@NamedQuery(name = "findReimbursementByEmployeeNameContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.employeeName like ?1"),
		@NamedQuery(name = "findReimbursementByFxRate", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.fxRate = ?1"),
		@NamedQuery(name = "findReimbursementById", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.id = ?1"),
		@NamedQuery(name = "findReimbursementByModifiedDate", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.modifiedDate = ?1"),
		@NamedQuery(name = "findReimbursementByMonth", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.month = ?1"),
		@NamedQuery(name = "findReimbursementByMonthAfter", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.month > ?1"),
		@NamedQuery(name = "findReimbursementByMonthBefore", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.month < ?1"),
		@NamedQuery(name = "findReimbursementByOriginalCurrencyCode", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findReimbursementByOriginalCurrencyCodeContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findReimbursementByPrimaryKey", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.id = ?1"),
		@NamedQuery(name = "findReimbursementByReimbursementNo", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.reimbursementNo = ?1"),
		@NamedQuery(name = "findReimbursementByReimbursementNoContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.reimbursementNo like ?1"),
		@NamedQuery(name = "findReimbursementByScheduledPaymentDate", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.scheduledPaymentDate = ?1"),
		@NamedQuery(name = "findReimbursementByScheduledPaymentDateAfter", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.scheduledPaymentDate > ?1"),
		@NamedQuery(name = "findReimbursementByScheduledPaymentDateBefore", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.scheduledPaymentDate < ?1"),
		@NamedQuery(name = "findReimbursementByStatus", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.status = ?1"),
		@NamedQuery(name = "findReimbursementByStatusContaining", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.status like ?1"),
		@NamedQuery(name = "findReimbursementByIncludGstTotalConvertedAmount", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.includeGstTotalConvertedAmount = ?1"),
		@NamedQuery(name = "findReimbursementByIncludGstTotalOriginalAmount", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.includeGstTotalOriginalAmount = ?1"),
		@NamedQuery(name = "findReimbursementByImportKey", query = "select myReimbursement from Reimbursement myReimbursement where myReimbursement.importKey = ?1")
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "ReimbursementPaymentDTO", 
		classes = { 
		    @ConstructorResult(
		        targetClass = ReimbursementPaymentDTO.class,
		        columns = {
		            @ColumnResult(name = "id", type = BigInteger.class),
		            @ColumnResult(name = "month", type = Calendar.class), 
		            @ColumnResult(name = "reimbursementNo"), 
		            @ColumnResult(name = "claimType"),
		            @ColumnResult(name = "employeeName"),
		            @ColumnResult(name = "employeeCode"),
		            @ColumnResult(name = "originalCurrencyCode"),
		            @ColumnResult(name = "paymentOrderNo"),
		            @ColumnResult(name = "valueDate", type = Calendar.class),
		            @ColumnResult(name = "bankName"),
		            @ColumnResult(name = "bankAccount"),
		            @ColumnResult(name = "bankRefNo"),
		            @ColumnResult(name = "includeGstTotalAmountConverted", type = BigDecimal.class),
		            @ColumnResult(name = "paymentOrderStatus"),
		            @ColumnResult(name = "reimbursementStatus"),
		            @ColumnResult(name = "includeGstConvertedAmount", type = BigDecimal.class),
		            @ColumnResult(name = "invoiceNo")
		        }
		    )
		}
	)
})

@Table(schema = "cashflow", name = "reimbursement")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "first/com/ttv/cashflow/domain/domain", name = "Reimbursement")
@XmlRootElement(namespace = "first/com/ttv/cashflow/domain/domain")
public class Reimbursement implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "employee_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String employeeCode;
	/**
	 */

	@Column(name = "employee_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String employeeName;
	/**
	 */

	@Column(name = "reimbursement_no", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String reimbursementNo;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	
	/**
	 */

	@Column(name = "original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "booking_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar bookingDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "account_payable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "include_gst_total_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstTotalOriginalAmount;
	/**
	 */

	@Column(name = "include_gst_total_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstTotalConvertedAmount;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "scheduled_payment_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar scheduledPaymentDate;
	/**
	 */

	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	/**
	 */

	@Column(name = "import_key", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String importKey;
	
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	
	/**
	 */

	@Column(name = "gst_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	
	/**
	 */

	@Column(name = "exclude_gst_total_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstTotalOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_total_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstTotalConvertedAmount;
	
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	
	/**
	 */
	@Column(name = "gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal gstOriginalAmount;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "reimbursement", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ReimbursementDetails> reimbursementDetailses;
	/**
	 */
	@OneToMany(mappedBy = "reimbursement", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ReimbursementPaymentDetails> reimbursementPaymentDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 */
	public String getEmployeeCode() {
		return this.employeeCode;
	}

	/**
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 */
	public String getEmployeeName() {
		return this.employeeName;
	}

	/**
	 */
	public void setReimbursementNo(String reimbursementNo) {
		this.reimbursementNo = reimbursementNo;
	}

	/**
	 */
	public String getReimbursementNo() {
		return this.reimbursementNo;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 */
	public Calendar getBookingDate() {
		return this.bookingDate;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 */
	public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
		this.scheduledPaymentDate = scheduledPaymentDate;
	}

	/**
	 */
	public Calendar getScheduledPaymentDate() {
		return this.scheduledPaymentDate;
	}

	/**
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setReimbursementDetailses(Set<ReimbursementDetails> reimbursementDetailses) {
		this.reimbursementDetailses = reimbursementDetailses;
	}

	/**
	 */
	public Set<ReimbursementDetails> getReimbursementDetailses() {
		if (reimbursementDetailses == null) {
			reimbursementDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ReimbursementDetails>();
		}
		return reimbursementDetailses;
	}

	/**
	 */
	public void setReimbursementPaymentDetailses(Set<ReimbursementPaymentDetails> reimbursementPaymentDetailses) {
		this.reimbursementPaymentDetailses = reimbursementPaymentDetailses;
	}

	/**
	 */
	public Set<ReimbursementPaymentDetails> getReimbursementPaymentDetailses() {
		if (reimbursementPaymentDetailses == null) {
			reimbursementPaymentDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ReimbursementPaymentDetails>();
		}
		return reimbursementPaymentDetailses;
	}
	
	public String getImportKey() {
		return importKey;
	}

	public void setImportKey(String importKey) {
		this.importKey = importKey;
	}
	
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	public BigDecimal getIncludeGstTotalOriginalAmount() {
		return includeGstTotalOriginalAmount;
	}

	public void setIncludeGstTotalOriginalAmount(BigDecimal includeGstTotalAmountOriginal) {
		this.includeGstTotalOriginalAmount = includeGstTotalAmountOriginal;
	}

	public BigDecimal getIncludeGstTotalConvertedAmount() {
		return includeGstTotalConvertedAmount;
	}

	public void setIncludeGstTotalConvertedAmount(BigDecimal includeGstTotalAmountConverted) {
		this.includeGstTotalConvertedAmount = includeGstTotalAmountConverted;
	}
	
	public BigDecimal getGstRate() {
		return gstRate;
	}

	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public BigDecimal getExcludeGstTotalOriginalAmount() {
		return excludeGstTotalOriginalAmount;
	}

	public void setExcludeGstTotalOriginalAmount(BigDecimal excludeGstTotalOriginalAmount) {
		this.excludeGstTotalOriginalAmount = excludeGstTotalOriginalAmount;
	}

	public BigDecimal getExcludeGstTotalConvertedAmount() {
		return excludeGstTotalConvertedAmount;
	}

	public void setExcludeGstTotalConvertedAmount(BigDecimal excludeGstTotalConvertedAmount) {
		this.excludeGstTotalConvertedAmount = excludeGstTotalConvertedAmount;
	}

	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}

	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}

	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	/**
	 */
	public Reimbursement() {
	}
	
	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Reimbursement that) {
		setId(that.getId());
		setEmployeeCode(that.getEmployeeCode());
		setEmployeeName(that.getEmployeeName());
		setReimbursementNo(that.getReimbursementNo());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setFxRate(that.getFxRate());
		setBookingDate(that.getBookingDate());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setDescription(that.getDescription());
		setIncludeGstTotalOriginalAmount(that.getIncludeGstTotalOriginalAmount());
		setIncludeGstTotalConvertedAmount(that.getIncludeGstTotalConvertedAmount());
		setScheduledPaymentDate(that.getScheduledPaymentDate());
		setStatus(that.getStatus());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setReimbursementDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ReimbursementDetails>(that.getReimbursementDetailses()));
		setReimbursementPaymentDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ReimbursementPaymentDetails>(that.getReimbursementPaymentDetailses()));
		setExcludeGstTotalConvertedAmount(that.getExcludeGstTotalConvertedAmount());
		setExcludeGstTotalOriginalAmount(that.getExcludeGstTotalOriginalAmount());
		setGstType(that.getGstType());
		setGstRate(that.getGstRate());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
	}

	@Override
	public String toString() {
		return "Reimbursement [id=" + id + ", employeeCode=" + employeeCode + ", employeeName=" + employeeName
				+ ", reimbursementNo=" + reimbursementNo + ", invoiceNo=" + invoiceNo + ", originalCurrencyCode="
				+ originalCurrencyCode + ", fxRate=" + fxRate + ", bookingDate=" + bookingDate + ", month=" + month
				+ ", claimType=" + claimType + ", accountPayableCode=" + accountPayableCode + ", accountPayableName="
				+ accountPayableName + ", description=" + description + ", includeGstTotalOriginalAmount="
				+ includeGstTotalOriginalAmount + ", includeGstTotalConvertedAmount=" + includeGstTotalConvertedAmount
				+ ", scheduledPaymentDate=" + scheduledPaymentDate + ", status=" + status + ", importKey=" + importKey
				+ ", gstRate=" + gstRate + ", gstType=" + gstType + ", excludeGstTotalOriginalAmount="
				+ excludeGstTotalOriginalAmount + ", excludeGstTotalConvertedAmount=" + excludeGstTotalConvertedAmount
				+ ", gstConvertedAmount=" + gstConvertedAmount + ", gstOriginalAmount=" + gstOriginalAmount
				+ ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", reimbursementDetailses="
				+ reimbursementDetailses + ", reimbursementPaymentDetailses=" + reimbursementPaymentDetailses + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountPayableCode == null) ? 0 : accountPayableCode.hashCode());
		result = prime * result + ((accountPayableName == null) ? 0 : accountPayableName.hashCode());
		result = prime * result + ((bookingDate == null) ? 0 : bookingDate.hashCode());
		result = prime * result + ((claimType == null) ? 0 : claimType.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((employeeCode == null) ? 0 : employeeCode.hashCode());
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result
				+ ((excludeGstTotalConvertedAmount == null) ? 0 : excludeGstTotalConvertedAmount.hashCode());
		result = prime * result
				+ ((excludeGstTotalOriginalAmount == null) ? 0 : excludeGstTotalOriginalAmount.hashCode());
		result = prime * result + ((fxRate == null) ? 0 : fxRate.hashCode());
		result = prime * result + ((gstConvertedAmount == null) ? 0 : gstConvertedAmount.hashCode());
		result = prime * result + ((gstOriginalAmount == null) ? 0 : gstOriginalAmount.hashCode());
		result = prime * result + ((gstRate == null) ? 0 : gstRate.hashCode());
		result = prime * result + ((gstType == null) ? 0 : gstType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((importKey == null) ? 0 : importKey.hashCode());
		result = prime * result
				+ ((includeGstTotalConvertedAmount == null) ? 0 : includeGstTotalConvertedAmount.hashCode());
		result = prime * result
				+ ((includeGstTotalOriginalAmount == null) ? 0 : includeGstTotalOriginalAmount.hashCode());
		result = prime * result + ((invoiceNo == null) ? 0 : invoiceNo.hashCode());
		result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((originalCurrencyCode == null) ? 0 : originalCurrencyCode.hashCode());
		result = prime * result + ((reimbursementDetailses == null) ? 0 : reimbursementDetailses.hashCode());
		result = prime * result + ((reimbursementNo == null) ? 0 : reimbursementNo.hashCode());
		result = prime * result
				+ ((reimbursementPaymentDetailses == null) ? 0 : reimbursementPaymentDetailses.hashCode());
		result = prime * result + ((scheduledPaymentDate == null) ? 0 : scheduledPaymentDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reimbursement other = (Reimbursement) obj;
		if (accountPayableCode == null) {
			if (other.accountPayableCode != null)
				return false;
		} else if (!accountPayableCode.equals(other.accountPayableCode))
			return false;
		if (accountPayableName == null) {
			if (other.accountPayableName != null)
				return false;
		} else if (!accountPayableName.equals(other.accountPayableName))
			return false;
		if (bookingDate == null) {
			if (other.bookingDate != null)
				return false;
		} else if (!bookingDate.equals(other.bookingDate))
			return false;
		if (claimType == null) {
			if (other.claimType != null)
				return false;
		} else if (!claimType.equals(other.claimType))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (employeeCode == null) {
			if (other.employeeCode != null)
				return false;
		} else if (!employeeCode.equals(other.employeeCode))
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (excludeGstTotalConvertedAmount == null) {
			if (other.excludeGstTotalConvertedAmount != null)
				return false;
		} else if (!excludeGstTotalConvertedAmount.equals(other.excludeGstTotalConvertedAmount))
			return false;
		if (excludeGstTotalOriginalAmount == null) {
			if (other.excludeGstTotalOriginalAmount != null)
				return false;
		} else if (!excludeGstTotalOriginalAmount.equals(other.excludeGstTotalOriginalAmount))
			return false;
		if (fxRate == null) {
			if (other.fxRate != null)
				return false;
		} else if (!fxRate.equals(other.fxRate))
			return false;
		if (gstConvertedAmount == null) {
			if (other.gstConvertedAmount != null)
				return false;
		} else if (!gstConvertedAmount.equals(other.gstConvertedAmount))
			return false;
		if (gstOriginalAmount == null) {
			if (other.gstOriginalAmount != null)
				return false;
		} else if (!gstOriginalAmount.equals(other.gstOriginalAmount))
			return false;
		if (gstRate == null) {
			if (other.gstRate != null)
				return false;
		} else if (!gstRate.equals(other.gstRate))
			return false;
		if (gstType == null) {
			if (other.gstType != null)
				return false;
		} else if (!gstType.equals(other.gstType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (importKey == null) {
			if (other.importKey != null)
				return false;
		} else if (!importKey.equals(other.importKey))
			return false;
		if (includeGstTotalConvertedAmount == null) {
			if (other.includeGstTotalConvertedAmount != null)
				return false;
		} else if (!includeGstTotalConvertedAmount.equals(other.includeGstTotalConvertedAmount))
			return false;
		if (includeGstTotalOriginalAmount == null) {
			if (other.includeGstTotalOriginalAmount != null)
				return false;
		} else if (!includeGstTotalOriginalAmount.equals(other.includeGstTotalOriginalAmount))
			return false;
		if (invoiceNo == null) {
			if (other.invoiceNo != null)
				return false;
		} else if (!invoiceNo.equals(other.invoiceNo))
			return false;
		if (modifiedDate == null) {
			if (other.modifiedDate != null)
				return false;
		} else if (!modifiedDate.equals(other.modifiedDate))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (originalCurrencyCode == null) {
			if (other.originalCurrencyCode != null)
				return false;
		} else if (!originalCurrencyCode.equals(other.originalCurrencyCode))
			return false;
		if (reimbursementDetailses == null) {
			if (other.reimbursementDetailses != null)
				return false;
		} else if (!reimbursementDetailses.equals(other.reimbursementDetailses))
			return false;
		if (reimbursementNo == null) {
			if (other.reimbursementNo != null)
				return false;
		} else if (!reimbursementNo.equals(other.reimbursementNo))
			return false;
		if (reimbursementPaymentDetailses == null) {
			if (other.reimbursementPaymentDetailses != null)
				return false;
		} else if (!reimbursementPaymentDetailses.equals(other.reimbursementPaymentDetailses))
			return false;
		if (scheduledPaymentDate == null) {
			if (other.scheduledPaymentDate != null)
				return false;
		} else if (!scheduledPaymentDate.equals(other.scheduledPaymentDate))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
}
