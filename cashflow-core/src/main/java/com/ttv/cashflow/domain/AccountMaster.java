
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAccountMasterByCode", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.code = ?1"),
		@NamedQuery(name = "findAccountMasterByCodeContaining", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.code like ?1"),
		@NamedQuery(name = "findAccountMasterByName", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.name = ?1"),
		@NamedQuery(name = "findAccountMasterByNameContaining", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.name like ?1"),
		@NamedQuery(name = "findAccountMasterByParentCode", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.parentCode = ?1"),
		@NamedQuery(name = "findAccountMasterByParentCodeContaining", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.parentCode like ?1"),
		@NamedQuery(name = "findAccountMasterByPrimaryKey", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.code = ?1"),
		@NamedQuery(name = "findAccountMasterByType", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.type = ?1"),
		@NamedQuery(name = "findAccountMasterByTypeContaining", query = "select myAccountMaster from AccountMaster myAccountMaster where myAccountMaster.type like ?1"),
		@NamedQuery(name = "findAllAccountMasters", query = "select myAccountMaster from AccountMaster myAccountMaster order by myAccountMaster.name asc"),
		@NamedQuery(name = "findAccountMasterByNameCodeContaining", query = "select myAccountMaster from AccountMaster myAccountMaster where lower(myAccountMaster.name) like lower(?1) or lower(myAccountMaster.code) like lower(?1)"),
		@NamedQuery(name = "countAccountMasterAll", query = "select count(1) from AccountMaster"),
		@NamedQuery(name = "countAccountMasterPaging", query = "select count(1) from AccountMaster myAccountMaster where lower(myAccountMaster.code) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(myAccountMaster.name) like lower(CONCAT('%',?1, '%')) or lower(myAccountMaster.parentCode) like lower(CONCAT('%',?1, '%')) or lower(myAccountMaster.type) like lower(CONCAT('%',?1, '%'))")
})

@Table(schema = "cashflow", name = "account_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "AccountMaster")

public class AccountMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "parent_code", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String parentCode;
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String type;

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	/**
	 */
	public String getParentCode() {
		return this.parentCode;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 */
	public String getType() {
		return this.type;
	}

	/**
	 */
	public AccountMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(AccountMaster that) {
		setCode(that.getCode());
		setParentCode(that.getParentCode());
		setName(that.getName());
		setType(that.getType());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("code=[").append(code).append("] ");
		buffer.append("parentCode=[").append(parentCode).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("type=[").append(type).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((code == null) ? 0 : code.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof AccountMaster))
			return false;
		AccountMaster equalCheck = (AccountMaster) obj;
		if ((code == null && equalCheck.code != null) || (code != null && equalCheck.code == null))
			return false;
		if (code != null && !code.equals(equalCheck.code))
			return false;
		return true;
	}
}
