
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ttv.cashflow.dto.PayrollPaymentDTO;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayrolls", query = "select myPayroll from Payroll myPayroll"),
		@NamedQuery(name = "findPayrollByAccountPayableCode", query = "select myPayroll from Payroll myPayroll where myPayroll.accountPayableCode = ?1"),
		@NamedQuery(name = "findPayrollByAccountPayableCodeContaining", query = "select myPayroll from Payroll myPayroll where myPayroll.accountPayableCode like ?1"),
		@NamedQuery(name = "findPayrollByAccountPayableName", query = "select myPayroll from Payroll myPayroll where myPayroll.accountPayableName = ?1"),
		@NamedQuery(name = "findPayrollByAccountPayableNameContaining", query = "select myPayroll from Payroll myPayroll where myPayroll.accountPayableName like ?1"),
		@NamedQuery(name = "findPayrollByBookingDate", query = "select myPayroll from Payroll myPayroll where myPayroll.bookingDate = ?1"),
		@NamedQuery(name = "findPayrollByBookingDateAfter", query = "select myPayroll from Payroll myPayroll where myPayroll.bookingDate > ?1"),
		@NamedQuery(name = "findPayrollByBookingDateBefore", query = "select myPayroll from Payroll myPayroll where myPayroll.bookingDate < ?1"),
		@NamedQuery(name = "findPayrollByClaimType", query = "select myPayroll from Payroll myPayroll where myPayroll.claimType = ?1"),
		@NamedQuery(name = "findPayrollByClaimTypeContaining", query = "select myPayroll from Payroll myPayroll where myPayroll.claimType like ?1"),
		@NamedQuery(name = "findPayrollByCreatedDate", query = "select myPayroll from Payroll myPayroll where myPayroll.createdDate = ?1"),
		@NamedQuery(name = "findPayrollById", query = "select myPayroll from Payroll myPayroll where myPayroll.id = ?1"),
		@NamedQuery(name = "findPayrollByModifiedDate", query = "select myPayroll from Payroll myPayroll where myPayroll.modifiedDate = ?1"),
		@NamedQuery(name = "findPayrollByMonth", query = "select myPayroll from Payroll myPayroll where myPayroll.month = ?1"),
		@NamedQuery(name = "findPayrollByMonthAfter", query = "select myPayroll from Payroll myPayroll where myPayroll.month > ?1"),
		@NamedQuery(name = "findPayrollByMonthBefore", query = "select myPayroll from Payroll myPayroll where myPayroll.month < ?1"),
		@NamedQuery(name = "findPayrollByPaymentScheduleDate", query = "select myPayroll from Payroll myPayroll where myPayroll.paymentScheduleDate = ?1"),
		@NamedQuery(name = "findPayrollByPaymentScheduleDateAfter", query = "select myPayroll from Payroll myPayroll where myPayroll.paymentScheduleDate > ?1"),
		@NamedQuery(name = "findPayrollByPaymentScheduleDateBefore", query = "select myPayroll from Payroll myPayroll where myPayroll.paymentScheduleDate < ?1"),
		@NamedQuery(name = "findPayrollByPayrollNo", query = "select myPayroll from Payroll myPayroll where myPayroll.payrollNo = ?1"),
		@NamedQuery(name = "findPayrollByPayrollNoContaining", query = "select myPayroll from Payroll myPayroll where myPayroll.payrollNo like ?1"),
		@NamedQuery(name = "findPayrollByPrimaryKey", query = "select myPayroll from Payroll myPayroll where myPayroll.id = ?1"),
		@NamedQuery(name = "findPayrollByTotalDeductionAmount", query = "select myPayroll from Payroll myPayroll where myPayroll.totalDeductionOriginal = ?1"),
		@NamedQuery(name = "findPayrollByTotalLaborAmount", query = "select myPayroll from Payroll myPayroll where myPayroll.totalLaborCostOriginal = ?1"),
		@NamedQuery(name = "findPayrollByTotalNetPayment", query = "select myPayroll from Payroll myPayroll where myPayroll.totalNetPaymentOriginal = ?1"),
		@NamedQuery(name = "findPayrollByImportKey", query = "select myPayroll from Payroll myPayroll where myPayroll.importKey = ?1")
})
@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "PayrollPaymentDTO", 
		classes = { 
		    @ConstructorResult(
		        targetClass = PayrollPaymentDTO.class,
		        columns = {
		            @ColumnResult(name = "id", type = BigInteger.class),
		            @ColumnResult(name = "month", type = Calendar.class), 
		            @ColumnResult(name = "payrollNo"), 
		            @ColumnResult(name = "claimType"),
		            @ColumnResult(name = "totalNetPayment", type = BigDecimal.class),
		            @ColumnResult(name = "payrollStatus"),
		            @ColumnResult(name = "paymentOrderNo"),
		            @ColumnResult(name = "valueDate", type = Calendar.class),
		            @ColumnResult(name = "bankName"),
		            @ColumnResult(name = "bankAccount"),
		            @ColumnResult(name = "includeGstConvertedAmount", type = BigDecimal.class),
		            @ColumnResult(name = "bankRefNo"),
		            @ColumnResult(name = "paymentOrderStatus"),
		            @ColumnResult(name = "invoiceNo")
		        }
		    )
		}
	)
})
@Table(schema = "cashflow", name = "payroll")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "Payroll")
@XmlRootElement(namespace = "cashflow_generate/com/ttv/cashflow/domain")
public class Payroll implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "payroll_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payrollNo;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "booking_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar bookingDate;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */
	
	@Column(name = "original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "account_payable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_schedule_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar paymentScheduleDate;
	/**
	 */

	@Column(name = "total_labor_cost_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalLaborCostOriginal;
	/**
	 */

	@Column(name = "total_deduction_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalDeductionOriginal;
	/**
	 */

	@Column(name = "total_net_payment_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)
	
	@XmlElement
	BigDecimal totalNetPaymentOriginal;
	/**
	 */
	
	@Column(name = "total_net_payment_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalNetPaymentConverted;
	/**
	 */
	
	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	
	/**
	 */

	@Column(name = "import_key", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String importKey;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "payroll", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.PayrollPaymentDetails> payrollPaymentDetailses;
	/**
	 */
	@OneToMany(mappedBy = "payroll", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.PayrollDetails> payrollDetailses;
	
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}

	/**
	 */
	public String getPayrollNo() {
		return this.payrollNo;
	}

	/**
	 */
	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 */
	public Calendar getBookingDate() {
		return this.bookingDate;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setPaymentScheduleDate(Calendar paymentScheduleDate) {
		this.paymentScheduleDate = paymentScheduleDate;
	}

	/**
	 */
	public Calendar getPaymentScheduleDate() {
		return this.paymentScheduleDate;
	}

	public BigDecimal getTotalLaborCostOriginal() {
		return totalLaborCostOriginal;
	}

	public void setTotalLaborCostOriginal(BigDecimal totalLaborCostOriginal) {
		this.totalLaborCostOriginal = totalLaborCostOriginal;
	}

	public BigDecimal getTotalDeductionOriginal() {
		return totalDeductionOriginal;
	}

	public void setTotalDeductionOriginal(BigDecimal totalDeductionOriginal) {
		this.totalDeductionOriginal = totalDeductionOriginal;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public BigDecimal getFxRate() {
		return fxRate;
	}

	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	public BigDecimal getTotalNetPaymentOriginal() {
		return totalNetPaymentOriginal;
	}

	public void setTotalNetPaymentOriginal(BigDecimal totalNetPaymentOriginal) {
		this.totalNetPaymentOriginal = totalNetPaymentOriginal;
	}

	public BigDecimal getTotalNetPaymentConverted() {
		return totalNetPaymentConverted;
	}

	public void setTotalNetPaymentConverted(BigDecimal totalNetPaymentConverted) {
		this.totalNetPaymentConverted = totalNetPaymentConverted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setPayrollPaymentDetailses(Set<PayrollPaymentDetails> payrollPaymentDetailses) {
		this.payrollPaymentDetailses = payrollPaymentDetailses;
	}

	/**
	 */
	public Set<PayrollPaymentDetails> getPayrollPaymentDetailses() {
		if (payrollPaymentDetailses == null) {
			payrollPaymentDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollPaymentDetails>();
		}
		return payrollPaymentDetailses;
	}

	/**
	 */
	public void setPayrollDetailses(Set<PayrollDetails> payrollDetailses) {
		this.payrollDetailses = payrollDetailses;
	}

	/**
	 */
	public Set<PayrollDetails> getPayrollDetailses() {
		if (payrollDetailses == null) {
			payrollDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollDetails>();
		}
		return payrollDetailses;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public Payroll() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Payroll that) {
		setId(that.getId());
		setMonth(that.getMonth());
		setPayrollNo(that.getPayrollNo());
		setBookingDate(that.getBookingDate());
		setClaimType(that.getClaimType());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setFxRate(that.getFxRate());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setPaymentScheduleDate(that.getPaymentScheduleDate());
		setTotalLaborCostOriginal(that.getTotalLaborCostOriginal());
		setTotalDeductionOriginal(that.getTotalDeductionOriginal());
		setTotalNetPaymentOriginal(that.getTotalNetPaymentOriginal());
		setTotalNetPaymentConverted(that.getTotalNetPaymentConverted());
		setStatus(that.getStatus());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setPayrollPaymentDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollPaymentDetails>(that.getPayrollPaymentDetailses()));
		setPayrollDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.PayrollDetails>(that.getPayrollDetailses()));
	}

	@Override
	public String toString() {
		return "Payroll [id=" + id + ", month=" + month + ", payrollNo=" + payrollNo + ", bookingDate=" + bookingDate
				+ ", claimType=" + claimType + ", originalCurrencyCode=" + originalCurrencyCode + ", fxRate=" + fxRate
				+ ", accountPayableCode=" + accountPayableCode + ", accountPayableName=" + accountPayableName
				+ ", paymentScheduleDate=" + paymentScheduleDate + ", totalLaborCostOriginal=" + totalLaborCostOriginal
				+ ", totalDeductionOriginal=" + totalDeductionOriginal + ", totalNetPaymentOriginal="
				+ totalNetPaymentOriginal + ", totalNetPaymentConverted=" + totalNetPaymentConverted + ", status="
				+ status + ", description=" + description + ", importKey=" + importKey + ", createdDate=" + createdDate
				+ ", modifiedDate=" + modifiedDate + ", payrollPaymentDetailses=" + payrollPaymentDetailses
				+ ", payrollDetailses=" + payrollDetailses + "]";
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Payroll))
			return false;
		Payroll equalCheck = (Payroll) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}

	public String getImportKey() {
		return importKey;
	}

	public void setImportKey(String importKey) {
		this.importKey = importKey;
	}
	
	
}
