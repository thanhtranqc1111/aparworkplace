
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllRoles", query = "select myRole from Role myRole order by id desc"),
		@NamedQuery(name = "findRoleByCreatedDate", query = "select myRole from Role myRole where myRole.createdDate = ?1"),
		@NamedQuery(name = "findRoleByDescription", query = "select myRole from Role myRole where myRole.description = ?1"),
		@NamedQuery(name = "findRoleByDescriptionContaining", query = "select myRole from Role myRole where myRole.description like ?1"),
		@NamedQuery(name = "findRoleById", query = "select myRole from Role myRole where myRole.id = ?1"),
		@NamedQuery(name = "findRoleByModifiedDate", query = "select myRole from Role myRole where myRole.modifiedDate = ?1"),
		@NamedQuery(name = "findRoleByName", query = "select myRole from Role myRole where myRole.name = ?1"),
		@NamedQuery(name = "findRoleByNameContaining", query = "select myRole from Role myRole where myRole.name like ?1"),
		@NamedQuery(name = "findRoleByPrimaryKey", query = "select myRole from Role myRole where myRole.id = ?1"),
		@NamedQuery(name = "findRolePaging", query = "select role from Role role where lower(role.name) like lower(CONCAT('%', ?1, '%')) order by id desc"),
		@NamedQuery(name = "countRolePaging", query = "select count(1) from Role role where lower(role.name) like lower(CONCAT('%', ?1, '%'))")
})

@Table(schema = "cashflow", name = "role")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "Role")
@XmlRootElement(namespace = "cashflow_test/com/ttv/cashflow/domain")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "description", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "role", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.UserAccount> userAccounts;
	/**
	 */
	@OneToMany(mappedBy = "role", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.RolePermission> rolePermissions;
	
	@Transient
	private String roleType;// ADMIN , manager or member
	
	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setUserAccounts(Set<UserAccount> userAccounts) {
		this.userAccounts = userAccounts;
	}

	/**
	 */
	public Set<UserAccount> getUserAccounts() {
		if (userAccounts == null) {
			userAccounts = new java.util.LinkedHashSet<com.ttv.cashflow.domain.UserAccount>();
		}
		return userAccounts;
	}

	/**
	 */
	public void setRolePermissions(Set<RolePermission> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}

	/**
	 */
	public Set<RolePermission> getRolePermissions() {
		if (rolePermissions == null) {
			rolePermissions = new java.util.LinkedHashSet<com.ttv.cashflow.domain.RolePermission>();
		}
		return rolePermissions;
	}
	
	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 */
	public Role() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Role that) {
		setId(that.getId());
		setName(that.getName());
		setDescription(that.getDescription());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setUserAccounts(new java.util.LinkedHashSet<com.ttv.cashflow.domain.UserAccount>(that.getUserAccounts()));
		setRolePermissions(new java.util.LinkedHashSet<com.ttv.cashflow.domain.RolePermission>(that.getRolePermissions()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Role))
			return false;
		Role equalCheck = (Role) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
