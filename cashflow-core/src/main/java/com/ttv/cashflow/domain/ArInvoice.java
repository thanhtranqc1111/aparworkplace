
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.dto.ArInvoiceDTO;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllArInvoices", query = "select myArInvoice from ArInvoice myArInvoice"),
		@NamedQuery(name = "findArInvoiceByAccountReceivableCode", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.accountReceivableCode = ?1"),
		@NamedQuery(name = "findArInvoiceByAccountReceivableCodeContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.accountReceivableCode like ?1"),
		@NamedQuery(name = "findArInvoiceByAccountReceivableName", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.accountReceivableName = ?1"),
		@NamedQuery(name = "findArInvoiceByAccountReceivableNameContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.accountReceivableName like ?1"),
		@NamedQuery(name = "findArInvoiceByApprovalCode", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.approvalCode = ?1"),
		@NamedQuery(name = "findArInvoiceByApprovalCodeContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.approvalCode like ?1"),
		@NamedQuery(name = "findArInvoiceByArInvoiceNo", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.arInvoiceNo = ?1"),
		@NamedQuery(name = "findArInvoiceByArInvoiceNoContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.arInvoiceNo like ?1"),
		@NamedQuery(name = "findArInvoiceByClaimType", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.claimType = ?1"),
		@NamedQuery(name = "findArInvoiceByClaimTypeContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.claimType like ?1"),
		@NamedQuery(name = "findArInvoiceByCreatedDate", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.createdDate = ?1"),
		@NamedQuery(name = "findArInvoiceByDescription", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.description = ?1"),
		@NamedQuery(name = "findArInvoiceByDescriptionContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.description like ?1"),
		@NamedQuery(name = "findArInvoiceByExcludeGstConvertedAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByExcludeGstOriginalAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByFxRate", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.fxRate = ?1"),
		@NamedQuery(name = "findArInvoiceByGstConvertedAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByGstOriginalAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.gstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByGstRate", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.gstRate = ?1"),
		@NamedQuery(name = "findArInvoiceByGstType", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.gstType = ?1"),
		@NamedQuery(name = "findArInvoiceByGstTypeContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.gstType like ?1"),
		@NamedQuery(name = "findArInvoiceById", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.id = ?1"),
		@NamedQuery(name = "findArInvoiceByIncludeGstConvertedAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByIncludeGstOriginalAmount", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceByInvoiceIssuedDate", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.invoiceIssuedDate = ?1"),
		@NamedQuery(name = "findArInvoiceByInvoiceIssuedDateAfter", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.invoiceIssuedDate > ?1"),
		@NamedQuery(name = "findArInvoiceByInvoiceIssuedDateBefore", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.invoiceIssuedDate < ?1"),
		@NamedQuery(name = "findArInvoiceByModifiedDate", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.modifiedDate = ?1"),
		@NamedQuery(name = "findArInvoiceByMonth", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.month = ?1"),
		@NamedQuery(name = "findArInvoiceByMonthAfter", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.month > ?1"),
		@NamedQuery(name = "findArInvoiceByMonthBefore", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.month < ?1"),
		@NamedQuery(name = "findArInvoiceByOriginalCurrencyCode", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findArInvoiceByOriginalCurrencyCodeContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findArInvoiceByPayerName", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.payerName = ?1"),
		@NamedQuery(name = "findArInvoiceByPayerNameContaining", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.payerName like ?1"),
		@NamedQuery(name = "findArInvoiceByPrimaryKey", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.id = ?1"),
		@NamedQuery(name = "findArInvoiceByInvoiceNo", query = "select myArInvoice from ArInvoice myArInvoice where myArInvoice.invoiceNo = ?1") })


@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "ArInvoiceResult", 
		classes = { 
		    @ConstructorResult(
		        targetClass = ArInvoiceReceiptOrderDTO.class,
		        columns = {
		            @ColumnResult(name = "id", type = BigInteger.class),
		            @ColumnResult(name = "payerName"), 
		            @ColumnResult(name = "arInvoiceNo"), 
		            @ColumnResult(name = "originalCurrencyCode"),
		            @ColumnResult(name = "fxRate", type = BigDecimal.class),
		            @ColumnResult(name = "gstRate", type = BigDecimal.class),
		            @ColumnResult(name = "invoiceIssuedDate", type = Calendar.class),
		            @ColumnResult(name = "month", type = Calendar.class),
		            @ColumnResult(name = "claimType"),
		            @ColumnResult(name = "gstType"),
		            @ColumnResult(name = "approvalCode"),
		            @ColumnResult(name = "accountReceivableCode"),
		            @ColumnResult(name = "accountReceivableName"),
		            @ColumnResult(name = "description"),
		            @ColumnResult(name = "excludeGstOriginalAmount", type = BigDecimal.class),
		            @ColumnResult(name = "excludeGstConvertedAmount", type = BigDecimal.class),
		            @ColumnResult(name = "gstOriginalAmount", type = BigDecimal.class),
		            @ColumnResult(name = "gstConvertedAmount", type = BigDecimal.class),
		            @ColumnResult(name = "includeGstConvertedAmount", type = BigDecimal.class),
		            @ColumnResult(name = "includeGstOriginalAmount", type = BigDecimal.class),
		            @ColumnResult(name = "remainIncludeGstOriginalAmount", type = BigDecimal.class),
		            @ColumnResult(name = "isApproved"),
		            @ColumnResult(name = "invoiceNo")
		        }
		    )
		}
	),
	@SqlResultSetMapping(
		name = "ARInvoiceReceiptDetail", 
		classes = { 
		    @ConstructorResult(
		        targetClass = ArInvoiceDTO.class,
		        columns = {
		    		@ColumnResult(name = "arId", type=BigInteger.class),
					@ColumnResult(name = "arInvoiceNo"),
					@ColumnResult(name = "month", type=Calendar.class),
					@ColumnResult(name = "payerAccount"),
					@ColumnResult(name = "claimType"),
					@ColumnResult(name = "payerName"),
					@ColumnResult(name = "invoiceNo"),
					@ColumnResult(name = "totalAmount", type=BigDecimal.class),
					@ColumnResult(name = "remainAmount", type=BigDecimal.class),
					@ColumnResult(name = "statusCode"),
					@ColumnResult(name = "status"),
					@ColumnResult(name = "bankTrans", type=BigInteger.class),
					@ColumnResult(name = "bankName"),
					@ColumnResult(name = "bankAccount"),
					@ColumnResult(name = "transDate", type=Calendar.class),
					@ColumnResult(name = "issuedDate", type=Calendar.class),
					@ColumnResult(name = "refNo"),
					@ColumnResult(name = "creditAmount", type=BigDecimal.class),
					@ColumnResult(name = "originalCurrencyCode"),
					@ColumnResult(name = "bankCurrencyCode"),
		        }
		    )
		}
	),
	@SqlResultSetMapping(
		name = "ARInvoiceDetailResult", 
		classes = { 
		    @ConstructorResult(
		        targetClass = ARInvoiceDetail.class,
		        columns = {
		            @ColumnResult(name = "arInvoiceAccountDetailId", type=BigInteger.class),
		            @ColumnResult(name = "accountCode"), 
		            @ColumnResult(name = "accountName"), 
		            @ColumnResult(name = "arInvoiceClassDetailId", type=BigInteger.class),
		            @ColumnResult(name = "classCode"),
		            @ColumnResult(name = "className"),
		            @ColumnResult(name = "approvalCode"),
		            @ColumnResult(name = "description"),
		            @ColumnResult(name = "excludeGstOriginalAmount", type=BigDecimal.class),
		            @ColumnResult(name = "fxRate", type=BigDecimal.class),
		            @ColumnResult(name = "excludeGstConvertedAmount", type=BigDecimal.class),
		            @ColumnResult(name = "gstType"),
		            @ColumnResult(name = "gstRate", type=BigDecimal.class),
		            @ColumnResult(name = "gstOriginalAmount", type=BigDecimal.class),
		            @ColumnResult(name = "gstConvertedAmount", type=BigDecimal.class),
		            @ColumnResult(name = "includeGstOriginalAmount", type=BigDecimal.class),
		            @ColumnResult(name = "includeGstConvertedAmount", type=BigDecimal.class),
		            @ColumnResult(name = "isApproval", type=Boolean.class)
		        }
		    )
		}
	)
})

@Table(schema = "cashflow", name = "ar_invoice")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "ArInvoice")
@XmlRootElement(namespace = "cashflow_temp/com/ttv/cashflow/domain")
public class ArInvoice implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payer_name", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payerName;
	/**
	 */

	@Column(name = "ar_invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String arInvoiceNo;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	
	/**
	 */

	@Column(name = "original_currency_code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "gst_rate", scale = 8, precision = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstRate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "invoice_issued_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar invoiceIssuedDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */

	@Column(name = "gst_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String gstType;
	/**
	 */

	@Column(name = "approval_code", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "account_receivable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountReceivableCode;
	/**
	 */

	@Column(name = "account_receivable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountReceivableName;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	/**
	 */

	@Column(name = "gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstOriginalAmount;
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	
	/**
	 */
	@Column(name = "status", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String status;
	
	@Column(name = "import_key", length = 200)
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    String importKey;
		
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "arInvoice", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ArInvoiceAccountDetails> arInvoiceAccountDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	/**
	 */
	public String getPayerName() {
		return this.payerName;
	}

	/**
	 */
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}

	/**
	 */
	public String getArInvoiceNo() {
		return this.arInvoiceNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	/**
	 */
	public BigDecimal getGstRate() {
		return this.gstRate;
	}

	/**
	 */
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}

	/**
	 */
	public Calendar getInvoiceIssuedDate() {
		return this.invoiceIssuedDate;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	/**
	 */
	public String getGstType() {
		return this.gstType;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setAccountReceivableCode(String accountReceivableCode) {
		this.accountReceivableCode = accountReceivableCode;
	}

	/**
	 */
	public String getAccountReceivableCode() {
		return this.accountReceivableCode;
	}

	/**
	 */
	public void setAccountReceivableName(String accountReceivableName) {
		this.accountReceivableName = accountReceivableName;
	}

	/**
	 */
	public String getAccountReceivableName() {
		return this.accountReceivableName;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	/**
	 */
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getGstOriginalAmount() {
		return this.gstOriginalAmount;
	}

	/**
	 */
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getGstConvertedAmount() {
		return this.gstConvertedAmount;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setImportKey(String importKey) {
        this.importKey = importKey;
    }
	
	public String getImportKey() {
        return importKey;
    }

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setArInvoiceAccountDetailses(Set<ArInvoiceAccountDetails> arInvoiceAccountDetailses) {
		this.arInvoiceAccountDetailses = arInvoiceAccountDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ArInvoiceAccountDetails> getArInvoiceAccountDetailses() {
		if (arInvoiceAccountDetailses == null) {
			arInvoiceAccountDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ArInvoiceAccountDetails>();
		}
		return arInvoiceAccountDetailses;
	}

	/**
	 */
	public ArInvoice() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArInvoice that) {
		setId(that.getId());
		setPayerName(that.getPayerName());
		setArInvoiceNo(that.getArInvoiceNo());
		setInvoiceNo(that.getInvoiceNo());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setFxRate(that.getFxRate());
		setGstRate(that.getGstRate());
		setInvoiceIssuedDate(that.getInvoiceIssuedDate());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setGstType(that.getGstType());
		setApprovalCode(that.getApprovalCode());
		setAccountReceivableCode(that.getAccountReceivableCode());
		setAccountReceivableName(that.getAccountReceivableName());
		setDescription(that.getDescription());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setStatus(that.getStatus());
		setImportKey(that.getImportKey());
		setArInvoiceAccountDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ArInvoiceAccountDetails>(that.getArInvoiceAccountDetailses()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payerName=[").append(payerName).append("] ");
		buffer.append("arInvoiceNo=[").append(arInvoiceNo).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("gstRate=[").append(gstRate).append("] ");
		buffer.append("invoiceIssuedDate=[").append(invoiceIssuedDate).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("gstType=[").append(gstType).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("accountReceivableCode=[").append(accountReceivableCode).append("] ");
		buffer.append("accountReceivableName=[").append(accountReceivableName).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("gstOriginalAmount=[").append(gstOriginalAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("status=[").append(status).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArInvoice))
			return false;
		ArInvoice equalCheck = (ArInvoice) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
