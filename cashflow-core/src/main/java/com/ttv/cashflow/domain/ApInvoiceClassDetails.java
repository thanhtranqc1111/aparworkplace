
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoiceClassDetailss", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails"),
		@NamedQuery(name = "findApInvoiceClassDetailsByApprovalCode", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.approvalCode = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByApprovalCodeContaining", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.approvalCode like ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByClassCode", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.classCode = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByClassCodeContaining", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.classCode like ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByClassName", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.className = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByClassNameContaining", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.className like ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByCreatedDate", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.createdDate = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByDescription", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.description = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByDescriptionContaining", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.description like ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByExcludeGstAmount", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByExcludeGstConvertedAmount", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByGstAmount", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsById", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.id = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByIncludeGstAmount", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByModifiedDate", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByPoNo", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.poNo = ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByPoNoContaining", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.poNo like ?1"),
		@NamedQuery(name = "findApInvoiceClassDetailsByPrimaryKey", query = "select myApInvoiceClassDetails from ApInvoiceClassDetails myApInvoiceClassDetails where myApInvoiceClassDetails.id = ?1")
		 
		})

@Table(schema = "cashflow", name = "ap_invoice_class_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoiceClassDetails")

public class ApInvoiceClassDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "class_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String classCode;
	/**
	 */

	@Column(name = "class_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String className;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "po_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String poNo;
	/**
	 */

	@Column(name = "approval_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String approvalCode;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount ;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	/**
	 */
	
	@Column(name = "gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal gstOriginalAmount;
	
	/**
	 */
	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	
	/**
     */
	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigDecimal includeGstOriginalAmount;
	
	/**
	 */
	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@Column(name = "is_approval")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isApproval;
	
	
	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "ap_invoice_account_details_id", referencedColumnName = "id") })
	@XmlTransient
	ApInvoiceAccountDetails apInvoiceAccountDetails;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 */
	public String getClassCode() {
		return this.classCode;
	}

	/**
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 */
	public String getClassName() {
		return this.className;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	/**
	 */
	public String getPoNo() {
		return this.poNo;
	}

	/**
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 */
	public String getApprovalCode() {
		return this.approvalCode;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	
	public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }
	 
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }

	public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
	
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }

	public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
	
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }
	
	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setApInvoiceAccountDetails(ApInvoiceAccountDetails apInvoiceAccountDetails) {
		this.apInvoiceAccountDetails = apInvoiceAccountDetails;
	}

	/**
	 */
	@JsonIgnore
	public ApInvoiceAccountDetails getApInvoiceAccountDetails() {
		return apInvoiceAccountDetails;
	}

	/**
	 */
	public ApInvoiceClassDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoiceClassDetails that) {
		setId(that.getId());
		setClassCode(that.getClassCode());
		setClassName(that.getClassName());
		setDescription(that.getDescription());
		setPoNo(that.getPoNo());
		setApprovalCode(that.getApprovalCode());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setApInvoiceAccountDetails(that.getApInvoiceAccountDetails());
		setIsApproval(that.getIsApproval());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("classCode=[").append(classCode).append("] ");
		buffer.append("className=[").append(className).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("poNo=[").append(poNo).append("] ");
		buffer.append("approvalCode=[").append(approvalCode).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");
		buffer.append("isApproval=[").append(isApproval).append("] ");
		
		return buffer.toString();
	}


}
