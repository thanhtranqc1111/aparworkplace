
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPaymentOrderTemps", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp"),
		@NamedQuery(name = "findPaymentOrderTempByAccountPayableCode", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.accountPayableCode = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByAccountPayableCodeContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.accountPayableCode like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByAccountPayableName", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.accountPayableName = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByAccountPayableNameContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.accountPayableName like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoiceNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoiceNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoiceNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoiceNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoiceOriginalCurrencyCode", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoiceOriginalCurrencyCode = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoiceOriginalCurrencyCode like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePayeeBankAccNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePayeeBankAccNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePayeeBankAccNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePayeeBankAccNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePayeeBankName", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePayeeBankName = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePayeeBankNameContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePayeeBankName like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePaymentConvertedAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePaymentConvertedAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePaymentOriginalAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePaymentOriginalAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByApInvoicePaymentRate", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.apInvoicePaymentRate = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankAccount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankAccount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankAccountContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankAccount like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankName", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankName = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankNameContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankName like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankRefNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankRefNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByBankRefNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.bankRefNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByDescription", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.description = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByDescriptionContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.description like ?1"),
		@NamedQuery(name = "findPaymentOrderTempById", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.id = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPaymentApprovalNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.paymentApprovalNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPaymentApprovalNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.paymentApprovalNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollOriginalCurrencyCode", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollOriginalCurrencyCode = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollOriginalCurrencyCode like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollPaymentConvertedAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollPaymentConvertedAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollPaymentOriginalAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollPaymentOriginalAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPayrollPaymentRate", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.payrollPaymentRate = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByPrimaryKey", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.id = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementNo", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementNo = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementNoContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementNo like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementOriginalCurrencyCode", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementOriginalCurrencyCode = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementOriginalCurrencyCode like ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementPaymentConvertedAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementPaymentConvertedAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementPaymentOriginalAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementPaymentOriginalAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByReimbursementPaymentRate", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.reimbursementPaymentRate = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByTotalAmountConvertedAmount", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.totalAmountConvertedAmount = ?1"),
		@NamedQuery(name = "findPaymentOrderTempByValueDate", query = "select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp where myPaymentOrderTemp.valueDate = ?1") })

@Table(schema = "cashflow", name = "payment_order_temp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "First/com/ttv/cashflow/domain", name = "PaymentOrderTemp")

public class PaymentOrderTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
    @Basic(fetch = FetchType.EAGER)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    BigInteger id;
	/**
	 */

	@Column(name = "bank_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankName;
	/**
	 */

	@Column(name = "bank_account", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankAccount;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "value_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar valueDate;
	/**
	 */

	@Column(name = "total_amount_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalAmountConvertedAmount;
	/**
	 */

	@Column(name = "bank_ref_no", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String bankRefNo;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "payment_approval_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentApprovalNo;
	/**
	 */

	@Column(name = "account_payable_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableCode;
	/**
	 */

	@Column(name = "account_payable_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountPayableName;
	/**
	 */

	@Column(name = "ap_invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoiceNo;
	/**
	 */

	@Column(name = "ap_invoice_original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoiceOriginalCurrencyCode;
	/**
	 */

	@Column(name = "ap_invoice_payment_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal apInvoicePaymentOriginalAmount;
	/**
	 */

	@Column(name = "ap_invoice_payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal apInvoicePaymentRate;
	/**
	 */

	@Column(name = "ap_invoice_payment_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal apInvoicePaymentConvertedAmount;
	/**
	 */

	@Column(name = "ap_invoice_payee_bank_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoicePayeeBankName;
	/**
	 */

	@Column(name = "ap_invoice_payee_bank_acc_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String apInvoicePayeeBankAccNo;
	/**
	 */

	@Column(name = "payroll_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payrollNo;
	/**
	 */

	@Column(name = "payroll_original_currency_code", length = 10)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payrollOriginalCurrencyCode;
	/**
	 */

	@Column(name = "payroll_payment_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal payrollPaymentOriginalAmount;
	/**
	 */

	@Column(name = "payroll_payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal payrollPaymentRate;
	/**
	 */

	@Column(name = "payroll_payment_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal payrollPaymentConvertedAmount;
	/**
	 */

	@Column(name = "reimbursement_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String reimbursementNo;
	/**
	 */

	@Column(name = "reimbursement_original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String reimbursementOriginalCurrencyCode;
	/**
	 */

	@Column(name = "reimbursement_payment_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal reimbursementPaymentOriginalAmount;
	/**
	 */

	@Column(name = "reimbursement_payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal reimbursementPaymentRate;
	/**
	 */

	@Column(name = "reimbursement_payment_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal reimbursementPaymentConvertedAmount;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 */
	public String getBankName() {
		return this.bankName;
	}

	/**
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 */
	public String getBankAccount() {
		return this.bankAccount;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setTotalAmountConvertedAmount(BigDecimal totalAmountConvertedAmount) {
		this.totalAmountConvertedAmount = totalAmountConvertedAmount;
	}

	/**
	 */
	public BigDecimal getTotalAmountConvertedAmount() {
		return this.totalAmountConvertedAmount;
	}

	/**
	 */
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	/**
	 */
	public String getBankRefNo() {
		return this.bankRefNo;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setPaymentApprovalNo(String paymentApprovalNo) {
		this.paymentApprovalNo = paymentApprovalNo;
	}

	/**
	 */
	public String getPaymentApprovalNo() {
		return this.paymentApprovalNo;
	}

	/**
	 */
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}

	/**
	 */
	public String getAccountPayableCode() {
		return this.accountPayableCode;
	}

	/**
	 */
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}

	/**
	 */
	public String getAccountPayableName() {
		return this.accountPayableName;
	}

	/**
	 */
	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}

	/**
	 */
	public String getApInvoiceNo() {
		return this.apInvoiceNo;
	}

	/**
	 */
	public void setApInvoiceOriginalCurrencyCode(String apInvoiceOriginalCurrencyCode) {
		this.apInvoiceOriginalCurrencyCode = apInvoiceOriginalCurrencyCode;
	}

	/**
	 */
	public String getApInvoiceOriginalCurrencyCode() {
		return this.apInvoiceOriginalCurrencyCode;
	}

	/**
	 */
	public void setApInvoicePaymentOriginalAmount(BigDecimal apInvoicePaymentOriginalAmount) {
		this.apInvoicePaymentOriginalAmount = apInvoicePaymentOriginalAmount;
	}

	/**
	 */
	public BigDecimal getApInvoicePaymentOriginalAmount() {
		return this.apInvoicePaymentOriginalAmount;
	}

	/**
	 */
	public void setApInvoicePaymentRate(BigDecimal apInvoicePaymentRate) {
		this.apInvoicePaymentRate = apInvoicePaymentRate;
	}

	/**
	 */
	public BigDecimal getApInvoicePaymentRate() {
		return this.apInvoicePaymentRate;
	}

	/**
	 */
	public void setApInvoicePaymentConvertedAmount(BigDecimal apInvoicePaymentConvertedAmount) {
		this.apInvoicePaymentConvertedAmount = apInvoicePaymentConvertedAmount;
	}

	/**
	 */
	public BigDecimal getApInvoicePaymentConvertedAmount() {
		return this.apInvoicePaymentConvertedAmount;
	}

	/**
	 */
	public void setApInvoicePayeeBankName(String apInvoicePayeeBankName) {
		this.apInvoicePayeeBankName = apInvoicePayeeBankName;
	}

	/**
	 */
	public String getApInvoicePayeeBankName() {
		return this.apInvoicePayeeBankName;
	}

	/**
	 */
	public void setApInvoicePayeeBankAccNo(String apInvoicePayeeBankAccNo) {
		this.apInvoicePayeeBankAccNo = apInvoicePayeeBankAccNo;
	}

	/**
	 */
	public String getApInvoicePayeeBankAccNo() {
		return this.apInvoicePayeeBankAccNo;
	}

	/**
	 */
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}

	/**
	 */
	public String getPayrollNo() {
		return this.payrollNo;
	}

	/**
	 */
	public void setPayrollOriginalCurrencyCode(String payrollOriginalCurrencyCode) {
		this.payrollOriginalCurrencyCode = payrollOriginalCurrencyCode;
	}

	/**
	 */
	public String getPayrollOriginalCurrencyCode() {
		return this.payrollOriginalCurrencyCode;
	}

	/**
	 */
	public void setPayrollPaymentOriginalAmount(BigDecimal payrollPaymentOriginalAmount) {
		this.payrollPaymentOriginalAmount = payrollPaymentOriginalAmount;
	}

	/**
	 */
	public BigDecimal getPayrollPaymentOriginalAmount() {
		return this.payrollPaymentOriginalAmount;
	}

	/**
	 */
	public void setPayrollPaymentRate(BigDecimal payrollPaymentRate) {
		this.payrollPaymentRate = payrollPaymentRate;
	}

	/**
	 */
	public BigDecimal getPayrollPaymentRate() {
		return this.payrollPaymentRate;
	}

	/**
	 */
	public void setPayrollPaymentConvertedAmount(BigDecimal payrollPaymentConvertedAmount) {
		this.payrollPaymentConvertedAmount = payrollPaymentConvertedAmount;
	}

	/**
	 */
	public BigDecimal getPayrollPaymentConvertedAmount() {
		return this.payrollPaymentConvertedAmount;
	}

	/**
	 */
	public void setReimbursementNo(String reimbursementNo) {
		this.reimbursementNo = reimbursementNo;
	}

	/**
	 */
	public String getReimbursementNo() {
		return this.reimbursementNo;
	}

	/**
	 */
	public void setReimbursementOriginalCurrencyCode(String reimbursementOriginalCurrencyCode) {
		this.reimbursementOriginalCurrencyCode = reimbursementOriginalCurrencyCode;
	}

	/**
	 */
	public String getReimbursementOriginalCurrencyCode() {
		return this.reimbursementOriginalCurrencyCode;
	}

	/**
	 */
	public void setReimbursementPaymentOriginalAmount(BigDecimal reimbursementPaymentOriginalAmount) {
		this.reimbursementPaymentOriginalAmount = reimbursementPaymentOriginalAmount;
	}

	/**
	 */
	public BigDecimal getReimbursementPaymentOriginalAmount() {
		return this.reimbursementPaymentOriginalAmount;
	}

	/**
	 */
	public void setReimbursementPaymentRate(BigDecimal reimbursementPaymentRate) {
		this.reimbursementPaymentRate = reimbursementPaymentRate;
	}

	/**
	 */
	public BigDecimal getReimbursementPaymentRate() {
		return this.reimbursementPaymentRate;
	}

	/**
	 */
	public void setReimbursementPaymentConvertedAmount(BigDecimal reimbursementPaymentConvertedAmount) {
		this.reimbursementPaymentConvertedAmount = reimbursementPaymentConvertedAmount;
	}

	/**
	 */
	public BigDecimal getReimbursementPaymentConvertedAmount() {
		return this.reimbursementPaymentConvertedAmount;
	}

	/**
	 */
	public PaymentOrderTemp() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PaymentOrderTemp that) {
		setId(that.getId());
		setBankName(that.getBankName());
		setBankAccount(that.getBankAccount());
		setValueDate(that.getValueDate());
		setTotalAmountConvertedAmount(that.getTotalAmountConvertedAmount());
		setBankRefNo(that.getBankRefNo());
		setDescription(that.getDescription());
		setPaymentApprovalNo(that.getPaymentApprovalNo());
		setAccountPayableCode(that.getAccountPayableCode());
		setAccountPayableName(that.getAccountPayableName());
		setApInvoiceNo(that.getApInvoiceNo());
		setApInvoiceOriginalCurrencyCode(that.getApInvoiceOriginalCurrencyCode());
		setApInvoicePaymentOriginalAmount(that.getApInvoicePaymentOriginalAmount());
		setApInvoicePaymentRate(that.getApInvoicePaymentRate());
		setApInvoicePaymentConvertedAmount(that.getApInvoicePaymentConvertedAmount());
		setApInvoicePayeeBankName(that.getApInvoicePayeeBankName());
		setApInvoicePayeeBankAccNo(that.getApInvoicePayeeBankAccNo());
		setPayrollNo(that.getPayrollNo());
		setPayrollOriginalCurrencyCode(that.getPayrollOriginalCurrencyCode());
		setPayrollPaymentOriginalAmount(that.getPayrollPaymentOriginalAmount());
		setPayrollPaymentRate(that.getPayrollPaymentRate());
		setPayrollPaymentConvertedAmount(that.getPayrollPaymentConvertedAmount());
		setReimbursementNo(that.getReimbursementNo());
		setReimbursementOriginalCurrencyCode(that.getReimbursementOriginalCurrencyCode());
		setReimbursementPaymentOriginalAmount(that.getReimbursementPaymentOriginalAmount());
		setReimbursementPaymentRate(that.getReimbursementPaymentRate());
		setReimbursementPaymentConvertedAmount(that.getReimbursementPaymentConvertedAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("bankName=[").append(bankName).append("] ");
		buffer.append("bankAccount=[").append(bankAccount).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("totalAmountConvertedAmount=[").append(totalAmountConvertedAmount).append("] ");
		buffer.append("bankRefNo=[").append(bankRefNo).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("paymentApprovalNo=[").append(paymentApprovalNo).append("] ");
		buffer.append("accountPayableCode=[").append(accountPayableCode).append("] ");
		buffer.append("accountPayableName=[").append(accountPayableName).append("] ");
		buffer.append("apInvoiceNo=[").append(apInvoiceNo).append("] ");
		buffer.append("apInvoiceOriginalCurrencyCode=[").append(apInvoiceOriginalCurrencyCode).append("] ");
		buffer.append("apInvoicePaymentOriginalAmount=[").append(apInvoicePaymentOriginalAmount).append("] ");
		buffer.append("apInvoicePaymentRate=[").append(apInvoicePaymentRate).append("] ");
		buffer.append("apInvoicePaymentConvertedAmount=[").append(apInvoicePaymentConvertedAmount).append("] ");
		buffer.append("apInvoicePayeeBankName=[").append(apInvoicePayeeBankName).append("] ");
		buffer.append("apInvoicePayeeBankAccNo=[").append(apInvoicePayeeBankAccNo).append("] ");
		buffer.append("payrollNo=[").append(payrollNo).append("] ");
		buffer.append("payrollOriginalCurrencyCode=[").append(payrollOriginalCurrencyCode).append("] ");
		buffer.append("payrollPaymentOriginalAmount=[").append(payrollPaymentOriginalAmount).append("] ");
		buffer.append("payrollPaymentRate=[").append(payrollPaymentRate).append("] ");
		buffer.append("payrollPaymentConvertedAmount=[").append(payrollPaymentConvertedAmount).append("] ");
		buffer.append("reimbursementNo=[").append(reimbursementNo).append("] ");
		buffer.append("reimbursementOriginalCurrencyCode=[").append(reimbursementOriginalCurrencyCode).append("] ");
		buffer.append("reimbursementPaymentOriginalAmount=[").append(reimbursementPaymentOriginalAmount).append("] ");
		buffer.append("reimbursementPaymentRate=[").append(reimbursementPaymentRate).append("] ");
		buffer.append("reimbursementPaymentConvertedAmount=[").append(reimbursementPaymentConvertedAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PaymentOrderTemp))
			return false;
		PaymentOrderTemp equalCheck = (PaymentOrderTemp) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
