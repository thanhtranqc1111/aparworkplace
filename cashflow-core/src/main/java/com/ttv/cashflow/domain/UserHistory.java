
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllUserHistorys", query = "select myUserHistory from UserHistory myUserHistory"),
		@NamedQuery(name = "findUserHistoryByAction", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.action = ?1"),
		@NamedQuery(name = "findUserHistoryByActionContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.action like ?1"),
		@NamedQuery(name = "findUserHistoryByActionDate", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.actionDate = ?1"),
		@NamedQuery(name = "findUserHistoryByEmail", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.email = ?1"),
		@NamedQuery(name = "findUserHistoryByEmailContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.email like ?1"),
        @NamedQuery(name = "findUserHistoryByFristName", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.firstName = ?1"),
        @NamedQuery(name = "findUserHistoryByFristNameContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.firstName like ?1"),
		@NamedQuery(name = "findUserHistoryById", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.id = ?1"),
		@NamedQuery(name = "findUserHistoryByLastName", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.lastName = ?1"),
		@NamedQuery(name = "findUserHistoryByLastNameContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.lastName like ?1"),
		@NamedQuery(name = "findUserHistoryByModule", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.module = ?1"),
		@NamedQuery(name = "findUserHistoryByModuleContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.module like ?1"),
		@NamedQuery(name = "findUserHistoryByPrimaryKey", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.id = ?1"),
		@NamedQuery(name = "findUserHistoryByTenantCode", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.tenantCode = ?1"),
		@NamedQuery(name = "findUserHistoryByTenantCodeContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.tenantCode like ?1"),
		@NamedQuery(name = "findUserHistoryByTenantName", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.tenantName = ?1"),
		@NamedQuery(name = "findUserHistoryByTenantNameContaining", query = "select myUserHistory from UserHistory myUserHistory where myUserHistory.tenantName like ?1") })

@Table(schema = "cashflow", name = "user_history")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "UserHistory")

public class UserHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

    @Column(name = "first_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String firstName;
	/**
	 */

	@Column(name = "last_name", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String lastName;
	/**
	 */

	@Column(name = "email", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String email;
	/**
	 */

	@Column(name = "module", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String module;
	/**
	 */

	@Column(name = "action", length = 400)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String action;
	/**
	 */

	@Column(name = "tenant_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String tenantCode;
	/**
	 */

	@Column(name = "tenant_name", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String tenantName;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar actionDate;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setFirstName(String fristName) {
		this.firstName = fristName;
	}

	/**
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 */
	public void setModule(String module) {
		this.module = module;
	}

	/**
	 */
	public String getModule() {
		return this.module;
	}

	/**
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 */
	public String getAction() {
		return this.action;
	}

	/**
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	/**
	 */
	public String getTenantCode() {
		return this.tenantCode;
	}

	/**
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 */
	public String getTenantName() {
		return this.tenantName;
	}

	/**
	 */
	public void setActionDate(Calendar actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 */
	public Calendar getActionDate() {
		return this.actionDate;
	}

	/**
	 */
	public UserHistory() {
	}

    public UserHistory(String firstName, String lastName, String email, String module, String tenantCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.module = module;
        this.tenantCode = tenantCode;
    }

    /**
     * Copies the contents of the specified bean into this bean.
     *
     */
	public void copy(UserHistory that) {
		setId(that.getId());
		setFirstName(that.getFirstName());
		setLastName(that.getLastName());
		setEmail(that.getEmail());
		setModule(that.getModule());
		setAction(that.getAction());
		setTenantCode(that.getTenantCode());
		setTenantName(that.getTenantName());
		setActionDate(that.getActionDate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("fristName=[").append(firstName).append("] ");
		buffer.append("lastName=[").append(lastName).append("] ");
		buffer.append("email=[").append(email).append("] ");
		buffer.append("module=[").append(module).append("] ");
		buffer.append("action=[").append(action).append("] ");
		buffer.append("tenantCode=[").append(tenantCode).append("] ");
		buffer.append("tenantName=[").append(tenantName).append("] ");
		buffer.append("actionDate=[").append(actionDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof UserHistory))
			return false;
		UserHistory equalCheck = (UserHistory) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
