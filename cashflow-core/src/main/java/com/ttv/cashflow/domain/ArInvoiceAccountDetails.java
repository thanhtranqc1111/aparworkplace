
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllArInvoiceAccountDetailss", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByAccountCode", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.accountCode = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByAccountCodeContaining", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.accountCode like ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByAccountName", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.accountName = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByAccountNameContaining", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.accountName like ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByCreatedDate", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.createdDate = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByExcludeGstConvertedAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.excludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByExcludeGstOriginalAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.excludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByGstConvertedAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.gstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByGstOriginalAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.gstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsById", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.id = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByIncludeGstConvertedAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.includeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByIncludeGstOriginalAmount", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.includeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByModifiedDate", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.modifiedDate = ?1"),
		@NamedQuery(name = "findArInvoiceAccountDetailsByPrimaryKey", query = "select myArInvoiceAccountDetails from ArInvoiceAccountDetails myArInvoiceAccountDetails where myArInvoiceAccountDetails.id = ?1") })

@Table(schema = "cashflow", name = "ar_invoice_account_details")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_temp/com/ttv/cashflow/domain", name = "ArInvoiceAccountDetails")
@XmlRootElement(namespace = "cashflow_temp/com/ttv/cashflow/domain")
public class ArInvoiceAccountDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "account_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountCode;
	/**
	 */

	@Column(name = "account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String accountName;
	/**
	 */

	@Column(name = "exclude_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstOriginalAmount;
	/**
	 */

	@Column(name = "exclude_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal excludeGstConvertedAmount;
	/**
	 */

	@Column(name = "gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstOriginalAmount;
	/**
	 */

	@Column(name = "gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal gstConvertedAmount;
	/**
	 */

	@Column(name = "include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstOriginalAmount;
	/**
	 */

	@Column(name = "include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal includeGstConvertedAmount;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "ar_invoice_id", referencedColumnName = "id") })
	@XmlTransient
	ArInvoice arInvoice;
	/**
	 */
	@OneToMany(mappedBy = "arInvoiceAccountDetails", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.ArInvoiceClassDetails> arInvoiceClassDetailses;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 */
	public String getAccountCode() {
		return this.accountCode;
	}

	/**
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 */
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstOriginalAmount() {
		return this.excludeGstOriginalAmount;
	}

	/**
	 */
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getExcludeGstConvertedAmount() {
		return this.excludeGstConvertedAmount;
	}

	/**
	 */
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getGstOriginalAmount() {
		return this.gstOriginalAmount;
	}

	/**
	 */
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getGstConvertedAmount() {
		return this.gstConvertedAmount;
	}

	/**
	 */
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstOriginalAmount() {
		return this.includeGstOriginalAmount;
	}

	/**
	 */
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getIncludeGstConvertedAmount() {
		return this.includeGstConvertedAmount;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setArInvoice(ArInvoice arInvoice) {
		this.arInvoice = arInvoice;
	}

	/**
	 */
	@JsonIgnore
	public ArInvoice getArInvoice() {
		return arInvoice;
	}

	/**
	 */
	public void setArInvoiceClassDetailses(Set<ArInvoiceClassDetails> arInvoiceClassDetailses) {
		this.arInvoiceClassDetailses = arInvoiceClassDetailses;
	}

	/**
	 */
	@JsonIgnore
	public Set<ArInvoiceClassDetails> getArInvoiceClassDetailses() {
		if (arInvoiceClassDetailses == null) {
			arInvoiceClassDetailses = new java.util.LinkedHashSet<com.ttv.cashflow.domain.ArInvoiceClassDetails>();
		}
		return arInvoiceClassDetailses;
	}

	/**
	 */
	public ArInvoiceAccountDetails() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ArInvoiceAccountDetails that) {
		setId(that.getId());
		setAccountCode(that.getAccountCode());
		setAccountName(that.getAccountName());
		setExcludeGstOriginalAmount(that.getExcludeGstOriginalAmount());
		setExcludeGstConvertedAmount(that.getExcludeGstConvertedAmount());
		setGstOriginalAmount(that.getGstOriginalAmount());
		setGstConvertedAmount(that.getGstConvertedAmount());
		setIncludeGstOriginalAmount(that.getIncludeGstOriginalAmount());
		setIncludeGstConvertedAmount(that.getIncludeGstConvertedAmount());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setArInvoice(that.getArInvoice());
		setArInvoiceClassDetailses(new java.util.LinkedHashSet<com.ttv.cashflow.domain.ArInvoiceClassDetails>(that.getArInvoiceClassDetailses()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("accountCode=[").append(accountCode).append("] ");
		buffer.append("accountName=[").append(accountName).append("] ");
		buffer.append("excludeGstOriginalAmount=[").append(excludeGstOriginalAmount).append("] ");
		buffer.append("excludeGstConvertedAmount=[").append(excludeGstConvertedAmount).append("] ");
		buffer.append("gstOriginalAmount=[").append(gstOriginalAmount).append("] ");
		buffer.append("gstConvertedAmount=[").append(gstConvertedAmount).append("] ");
		buffer.append("includeGstOriginalAmount=[").append(includeGstOriginalAmount).append("] ");
		buffer.append("includeGstConvertedAmount=[").append(includeGstConvertedAmount).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ArInvoiceAccountDetails))
			return false;
		ArInvoiceAccountDetails equalCheck = (ArInvoiceAccountDetails) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
