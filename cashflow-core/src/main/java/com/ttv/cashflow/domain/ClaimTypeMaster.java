
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllClaimTypeMasters", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster"),
		@NamedQuery(name = "findClaimTypeMasterByCode", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where myClaimTypeMaster.code = ?1"),
		@NamedQuery(name = "findClaimTypeMasterByCodeContaining", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where myClaimTypeMaster.code like ?1"),
		@NamedQuery(name = "findClaimTypeMasterByName", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where myClaimTypeMaster.name = ?1"),
		@NamedQuery(name = "findClaimTypeMasterByNameContaining", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where myClaimTypeMaster.name like ?1"),
		@NamedQuery(name = "findClaimTypeMasterByPrimaryKey", query = "select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where myClaimTypeMaster.code = ?1"),
		@NamedQuery(name = "countClaimTypeMasterAll", query = "select count(1) from ClaimTypeMaster"),
		@NamedQuery(name = "countClaimTypeMasterPaging", query = "select count(1) from ClaimTypeMaster where lower(code) like lower(CONCAT('%', ?1, '%')) or "
					    + "lower(name) like lower(CONCAT('%', ?1, '%'))")
})

@Table(schema = "cashflow", name = "claim_type_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ClaimTypeMaster")

public class ClaimTypeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public ClaimTypeMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ClaimTypeMaster that) {
		setCode(that.getCode());
		setName(that.getName());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((code == null) ? 0 : code.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ClaimTypeMaster))
			return false;
		ClaimTypeMaster equalCheck = (ClaimTypeMaster) obj;
		if ((code == null && equalCheck.code != null) || (code != null && equalCheck.code == null))
			return false;
		if (code != null && !code.equals(equalCheck.code))
			return false;
		return true;
	}
}
