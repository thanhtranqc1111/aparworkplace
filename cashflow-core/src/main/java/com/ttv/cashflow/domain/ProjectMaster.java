
package com.ttv.cashflow.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllProjectMasters", query = "select myProjectMaster from ProjectMaster myProjectMaster"),
		@NamedQuery(name = "findProjectMasterByCode", query = "select myProjectMaster from ProjectMaster myProjectMaster where myProjectMaster.code = ?1"),
		@NamedQuery(name = "findProjectMasterByCodeContaining", query = "select myProjectMaster from ProjectMaster myProjectMaster where myProjectMaster.code like ?1"),
		@NamedQuery(name = "findProjectMasterByName", query = "select myProjectMaster from ProjectMaster myProjectMaster where myProjectMaster.name = ?1"),
		@NamedQuery(name = "findProjectMasterByNameContaining", query = "select myProjectMaster from ProjectMaster myProjectMaster where myProjectMaster.name like ?1"),
		@NamedQuery(name = "findProjectMasterByPrimaryKey", query = "select myProjectMaster from ProjectMaster myProjectMaster where myProjectMaster.code = ?1"),
		@NamedQuery(name = "findProjectMasterByCodeNameContaining", query = "select myProjectMaster from ProjectMaster myProjectMaster where lower(myProjectMaster.code) like lower(?1) or lower(myProjectMaster.name) like lower(?1)"),
		@NamedQuery(name = "findProjectMasterPaging", query = "select myProjectMaster from ProjectMaster myProjectMaster where lower(myProjectMaster.code) like lower(CONCAT('%', ?1, '%')) or "
                        + "lower(myProjectMaster.name) like lower(CONCAT('%', ?1, '%'))")
		})

@Table(schema = "cashflow", name = "project_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ProjectMaster")

public class ProjectMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "code", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	String code;

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String name;

    @Column(name = "business_type_name", length = 20)
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    String businessTypeName;

    @Column(name = "approval_code", length = 40)
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    String approvalCode;

    @Column(name = "customer_name", length = 100)
    @Basic(fetch = FetchType.EAGER)
    @XmlElement
    String customerName;
    
    /**
     */
    @Column(name = "is_active")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Boolean isActive;
    
    /**
     */
    @OneToMany(mappedBy = "projectMaster", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

    @XmlElement(name = "", namespace = "")
    java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> projectPayrollDetailses;
    /**
     */
    @OneToMany(mappedBy = "projectMaster", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

    @XmlElement(name = "", namespace = "")
    java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> projectAllocations;

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

    public String getBusinessTypeName() {
        return businessTypeName;
    }

    public void setBusinessTypeName(String businessTypeName) {
        this.businessTypeName = businessTypeName;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getCustomerName() {
        return customerName;
    }
    
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
    
    public Boolean getIsActive() {
        return isActive;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    public void setProjectAllocations(java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> projectAllocations) {
        this.projectAllocations = projectAllocations;
    }
    
    public java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> getProjectAllocations() {
        return projectAllocations;
    }
    
    public void setProjectPayrollDetailses(java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> projectPayrollDetailses) {
        this.projectPayrollDetailses = projectPayrollDetailses;
    }
    
    public java.util.Set<com.ttv.cashflow.domain.ProjectPayrollDetails> getProjectPayrollDetailses() {
        return projectPayrollDetailses;
    }

    public ProjectMaster(String code, String name) {
    	this.code = code;
    	this.name = name;
	}
    
    public ProjectMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ProjectMaster that) {
		setCode(that.getCode());
		setName(that.getName());
        setBusinessTypeName(that.getBusinessTypeName());
        setApprovalCode(that.getApprovalCode());
        setCustomerName(that.getCustomerName());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");
        buffer.append("businessTypeName=[").append(businessTypeName).append("] ");
        buffer.append("approvalCode=[").append(approvalCode).append("] ");
        buffer.append("customerName=[").append(customerName).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((code == null) ? 0 : code.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ProjectMaster))
			return false;
		ProjectMaster equalCheck = (ProjectMaster) obj;
		if ((code == null && equalCheck.code != null) || (code != null && equalCheck.code == null))
			return false;
		if (code != null && !code.equals(equalCheck.code))
			return false;
		return true;
	}
}
