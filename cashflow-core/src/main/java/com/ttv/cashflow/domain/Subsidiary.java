
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllSubsidiarys", query = "select mySubsidiary from Subsidiary mySubsidiary"),
		@NamedQuery(name = "findSubsidiaryByAddress", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.address = ?1"),
		@NamedQuery(name = "findSubsidiaryByAddressContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.address like ?1"),
		@NamedQuery(name = "findSubsidiaryByCode", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.code = ?1"),
		@NamedQuery(name = "findSubsidiaryByCodeContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.code like ?1"),
		@NamedQuery(name = "findSubsidiaryByCreatedDate", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.createdDate = ?1"),
		@NamedQuery(name = "findSubsidiaryByDescription", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.description = ?1"),
		@NamedQuery(name = "findSubsidiaryByDescriptionContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.description like ?1"),
		@NamedQuery(name = "findSubsidiaryById", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.id = ?1"),
		@NamedQuery(name = "findSubsidiaryByIsActive", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.isActive = ?1"),
		@NamedQuery(name = "findSubsidiaryByModifiedDate", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.modifiedDate = ?1"),
		@NamedQuery(name = "findSubsidiaryByName", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.name = ?1"),
		@NamedQuery(name = "findSubsidiaryByNameContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.name like ?1"),
		@NamedQuery(name = "findSubsidiaryByPhone", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.phone = ?1"),
		@NamedQuery(name = "findSubsidiaryByPhoneContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.phone like ?1"),
		@NamedQuery(name = "findSubsidiaryByPrimaryKey", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.id = ?1"),
		@NamedQuery(name = "findSubsidiaryByWebsite", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.website = ?1"),
		@NamedQuery(name = "findSubsidiaryByWebsiteContaining", query = "select mySubsidiary from Subsidiary mySubsidiary where mySubsidiary.website like ?1"),
		@NamedQuery(name = "findSubsidiaryByNameOrCode", query = "select mySubsidiary from Subsidiary mySubsidiary where (lower(mySubsidiary.name) like lower(CONCAT('%', ?1, '%')) or lower(mySubsidiary.code) like lower(CONCAT('%', ?1, '%'))) ORDER BY mySubsidiary.createdDate"),
	})

@Table(schema = "cashflow", name = "subsidiary")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_test/com/ttv/cashflow/domain", name = "Subsidiary")
@XmlRootElement(namespace = "cashflow_test/com/ttv/cashflow/domain")
public class Subsidiary implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "code", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String code;
	/**
	 */

	@Column(name = "name", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "address", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String address;
	/**
	 */

	@Column(name = "phone", length = 20)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String phone;
	/**
	 */

	@Column(name = "website", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String website;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	
	/**
	 */
	@Column(name = "currency", length = 8)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String currency;
	
	/**
	 */

	@Column(name = "is_active", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isActive;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifiedDate;

	/**
	 */
	@OneToMany(mappedBy = "subsidiary", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<com.ttv.cashflow.domain.SubsidiaryUserDetail> subsidiaryUserDetails;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 */
	public String getWebsite() {
		return this.website;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 */
	public Boolean getIsActive() {
		return this.isActive;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 */
	public Calendar getModifiedDate() {
		return this.modifiedDate;
	}

	/**
	 */
	public void setSubsidiaryUserDetails(Set<SubsidiaryUserDetail> subsidiaryUserDetails) {
		this.subsidiaryUserDetails = subsidiaryUserDetails;
	}

	/**
	 */
	public Set<SubsidiaryUserDetail> getSubsidiaryUserDetails() {
		if (subsidiaryUserDetails == null) {
			subsidiaryUserDetails = new java.util.LinkedHashSet<com.ttv.cashflow.domain.SubsidiaryUserDetail>();
		}
		return subsidiaryUserDetails;
	}

	/**
	 */
	public Subsidiary() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Subsidiary that) {
		setId(that.getId());
		setCode(that.getCode());
		setName(that.getName());
		setAddress(that.getAddress());
		setPhone(that.getPhone());
		setWebsite(that.getWebsite());
		setDescription(that.getDescription());
		setCurrency(that.getCurrency());
		setIsActive(that.getIsActive());
		setCreatedDate(that.getCreatedDate());
		setModifiedDate(that.getModifiedDate());
		setSubsidiaryUserDetails(new java.util.LinkedHashSet<com.ttv.cashflow.domain.SubsidiaryUserDetail>(that.getSubsidiaryUserDetails()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("address=[").append(address).append("] ");
		buffer.append("phone=[").append(phone).append("] ");
		buffer.append("website=[").append(website).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("currency=[").append(currency).append("] ");
		buffer.append("isActive=[").append(isActive).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifiedDate=[").append(modifiedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Subsidiary))
			return false;
		Subsidiary equalCheck = (Subsidiary) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
