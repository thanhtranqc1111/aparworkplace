
package com.ttv.cashflow.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.*;

import com.ttv.cashflow.dto.ArInvoiceDTO;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;
import com.ttv.cashflow.dto.ReceiptOrderDTO;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllReceiptOrders", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder"),
		@NamedQuery(name = "findReceiptOrderByArInvoiceId", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.arInvoiceId = ?1"),
		@NamedQuery(name = "findReceiptOrderByCreatedDate", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.createdDate = ?1"),
		@NamedQuery(name = "findReceiptOrderById", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.id = ?1"),
		@NamedQuery(name = "findReceiptOrderByModifedDate", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.modifedDate = ?1"),
		@NamedQuery(name = "findReceiptOrderByPrimaryKey", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.id = ?1"),
		@NamedQuery(name = "findReceiptOrderByReceiptRate", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.receiptRate = ?1"),
		@NamedQuery(name = "findReceiptOrderBySettedIncludeGstConvertedAmount", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.settedIncludeGstConvertedAmount = ?1"),
		@NamedQuery(name = "findReceiptOrderBySettedIncludeGstOriginalAmount", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.settedIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findReceiptOrderByTransactionId", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.transactionId = ?1"),
		@NamedQuery(name = "findReceiptOrderByVarianceAccountName", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.varianceAccountName = ?1"),
		@NamedQuery(name = "findReceiptOrderByVarianceAccountNameContaining", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.varianceAccountName like ?1"),
		@NamedQuery(name = "findReceiptOrderByVarianceAmount", query = "select myReceiptOrder from ReceiptOrder myReceiptOrder where myReceiptOrder.varianceAmount = ?1") })

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "ReceiptOrderDTO", 
		classes = { 
		    @ConstructorResult(
		        targetClass = ReceiptOrderDTO.class,
		        columns = {
		            @ColumnResult(name = "bankStatementId", type = BigInteger.class),
		            @ColumnResult(name = "transactionDate", type = Calendar.class), 
		            @ColumnResult(name = "valueDate", type = Calendar.class), 
		            @ColumnResult(name = "currencyCode"),
		            @ColumnResult(name = "credit", type = BigDecimal.class),
		            @ColumnResult(name = "arStatus"),
		            @ColumnResult(name = "arStatusValue"),
		            @ColumnResult(name = "arInvoiceNo"),
		            @ColumnResult(name = "month", type = Calendar.class),
		            @ColumnResult(name = "payerName"),
		            @ColumnResult(name = "invoiceNo"),
		            @ColumnResult(name = "totalAmount", type = BigDecimal.class),
		            @ColumnResult(name = "totalAmountConverted", type = BigDecimal.class),
		            @ColumnResult(name = "remainAmount", type = BigDecimal.class),
		            @ColumnResult(name = "varianceAmount", type = BigDecimal.class),
		            @ColumnResult(name = "originalCurrencyCode")
		        }
		    )
		}
	)
})

@Table(schema = "cashflow", name = "receipt_order")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "temp/com/ttv/cashflow/domain", name = "ReceiptOrder")

public class ReceiptOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "transaction_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger transactionId;
	/**
	 */

	@Column(name = "ar_invoice_id")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigInteger arInvoiceId;
	/**
	 */

	@Column(name = "setted_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal settedIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "receipt_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal receiptRate;
	/**
	 */

	@Column(name = "setted_include_gst_converted_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal settedIncludeGstConvertedAmount;
	/**
	 */

	@Column(name = "variance_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal varianceAmount;
	/**
	 */

	@Column(name = "variance_account_name", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String varianceAccountName;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createdDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifed_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar modifedDate;
	
	/**
	 */

	@Column(name = "receipt_order_no", length = 40, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String receiptOrderNo;
	
	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	public BigInteger getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(BigInteger transactionId) {
		this.transactionId = transactionId;
	}

	public BigInteger getArInvoiceId() {
		return arInvoiceId;
	}

	public void setArInvoiceId(BigInteger arInvoiceId) {
		this.arInvoiceId = arInvoiceId;
	}

	/**
	 */
	public void setSettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount) {
		this.settedIncludeGstOriginalAmount = settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public BigDecimal getSettedIncludeGstOriginalAmount() {
		return this.settedIncludeGstOriginalAmount;
	}

	/**
	 */
	public void setReceiptRate(BigDecimal receiptRate) {
		this.receiptRate = receiptRate;
	}

	/**
	 */
	public BigDecimal getReceiptRate() {
		return this.receiptRate;
	}

	/**
	 */
	public void setSettedIncludeGstConvertedAmount(BigDecimal settedIncludeGstConvertedAmount) {
		this.settedIncludeGstConvertedAmount = settedIncludeGstConvertedAmount;
	}

	/**
	 */
	public BigDecimal getSettedIncludeGstConvertedAmount() {
		return this.settedIncludeGstConvertedAmount;
	}

	/**
	 */
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	/**
	 */
	public BigDecimal getVarianceAmount() {
		return this.varianceAmount;
	}

	/**
	 */
	public void setVarianceAccountName(String varianceAccountName) {
		this.varianceAccountName = varianceAccountName;
	}

	/**
	 */
	public String getVarianceAccountName() {
		return this.varianceAccountName;
	}

	/**
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 */
	public Calendar getCreatedDate() {
		return this.createdDate;
	}

	/**
	 */
	public void setModifedDate(Calendar modifedDate) {
		this.modifedDate = modifedDate;
	}

	/**
	 */
	public Calendar getModifedDate() {
		return this.modifedDate;
	}
	
	public String getReceiptOrderNo() {
		return receiptOrderNo;
	}

	public void setReceiptOrderNo(String receiptOrderNo) {
		this.receiptOrderNo = receiptOrderNo;
	}

	/**
	 */
	public ReceiptOrder() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ReceiptOrder that) {
		setId(that.getId());
		setTransactionId(that.getTransactionId());
		setArInvoiceId(that.getArInvoiceId());
		setSettedIncludeGstOriginalAmount(that.getSettedIncludeGstOriginalAmount());
		setReceiptRate(that.getReceiptRate());
		setSettedIncludeGstConvertedAmount(that.getSettedIncludeGstConvertedAmount());
		setVarianceAmount(that.getVarianceAmount());
		setVarianceAccountName(that.getVarianceAccountName());
		setCreatedDate(that.getCreatedDate());
		setModifedDate(that.getModifedDate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("transactionId=[").append(transactionId).append("] ");
		buffer.append("arInvoiceId=[").append(arInvoiceId).append("] ");
		buffer.append("settedIncludeGstOriginalAmount=[").append(settedIncludeGstOriginalAmount).append("] ");
		buffer.append("receiptRate=[").append(receiptRate).append("] ");
		buffer.append("settedIncludeGstConvertedAmount=[").append(settedIncludeGstConvertedAmount).append("] ");
		buffer.append("varianceAmount=[").append(varianceAmount).append("] ");
		buffer.append("varianceAccountName=[").append(varianceAccountName).append("] ");
		buffer.append("createdDate=[").append(createdDate).append("] ");
		buffer.append("modifedDate=[").append(modifedDate).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ReceiptOrder))
			return false;
		ReceiptOrder equalCheck = (ReceiptOrder) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
