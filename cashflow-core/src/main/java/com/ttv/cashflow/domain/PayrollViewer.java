
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.lang.StringBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayrollViewers", query = "select myPayrollViewer from PayrollViewer myPayrollViewer"),
		@NamedQuery(name = "findPayrollViewerByClaimType", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.claimType = ?1"),
		@NamedQuery(name = "findPayrollViewerByClaimTypeContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.claimType like ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionOther1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionOther1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionOther2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionOther2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionOther3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionOther3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployee1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployee1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployee2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployee2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployee3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployee3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployer1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployer1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployer2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployer2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionSocialInsuranceEmployer3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionSocialInsuranceEmployer3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByDeductionWht", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.deductionWht = ?1"),
		@NamedQuery(name = "findPayrollViewerByDescription", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.description = ?1"),
		@NamedQuery(name = "findPayrollViewerByDescriptionContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.description like ?1"),
		@NamedQuery(name = "findPayrollViewerByDivision", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.division = ?1"),
		@NamedQuery(name = "findPayrollViewerByDivisionContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.division like ?1"),
		@NamedQuery(name = "findPayrollViewerByEmployeeCode", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.employeeCode = ?1"),
		@NamedQuery(name = "findPayrollViewerByEmployeeCodeContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.employeeCode like ?1"),
		@NamedQuery(name = "findPayrollViewerByFxRate", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.fxRate = ?1"),
		@NamedQuery(name = "findPayrollViewerById", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.id = ?1"),
		@NamedQuery(name = "findPayrollViewerByInvoiceNo", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.invoiceNo = ?1"),
		@NamedQuery(name = "findPayrollViewerByInvoiceNoContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.invoiceNo like ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborAdditionalPayment1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborAdditionalPayment1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborAdditionalPayment2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborAdditionalPayment2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborAdditionalPayment3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborAdditionalPayment3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborBaseSalaryPayment", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborBaseSalaryPayment = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborOtherPayment1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborOtherPayment1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborOtherPayment2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborOtherPayment2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborOtherPayment3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborOtherPayment3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborSocialInsuranceEmployer1", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborSocialInsuranceEmployer1 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborSocialInsuranceEmployer2", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborSocialInsuranceEmployer2 = ?1"),
		@NamedQuery(name = "findPayrollViewerByLaborSocialInsuranceEmployer3", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.laborSocialInsuranceEmployer3 = ?1"),
		@NamedQuery(name = "findPayrollViewerByMonth", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.month = ?1"),
		@NamedQuery(name = "findPayrollViewerByMonthAfter", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.month > ?1"),
		@NamedQuery(name = "findPayrollViewerByMonthBefore", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.month < ?1"),
		@NamedQuery(name = "findPayrollViewerByNetPaymentConverted", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.netPaymentConverted = ?1"),
		@NamedQuery(name = "findPayrollViewerByNetPaymentOriginal", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.netPaymentOriginal = ?1"),
		@NamedQuery(name = "findPayrollViewerByOriginalCurrencyCode", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.originalCurrencyCode = ?1"),
		@NamedQuery(name = "findPayrollViewerByOriginalCurrencyCodeContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.originalCurrencyCode like ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentAmountConverted", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentAmountConverted = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentAmountOriginal", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentAmountOriginal = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentOrderNo", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentOrderNo = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentRate", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentRate = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentRemainAmount", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentRemainAmount = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentScheduleDate", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentScheduleDate = ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentScheduleDateAfter", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentScheduleDate > ?1"),
		@NamedQuery(name = "findPayrollViewerByPaymentScheduleDateBefore", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.paymentScheduleDate < ?1"),
		@NamedQuery(name = "findPayrollViewerByPayrollNo", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.payrollNo = ?1"),
		@NamedQuery(name = "findPayrollViewerByPayrollNoContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.payrollNo like ?1"),
		@NamedQuery(name = "findPayrollViewerByPrimaryKey", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.id = ?1"),
		@NamedQuery(name = "findPayrollViewerByRoleTitle", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.roleTitle = ?1"),
		@NamedQuery(name = "findPayrollViewerByRoleTitleContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.roleTitle like ?1"),
		@NamedQuery(name = "findPayrollViewerByTeam", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.team = ?1"),
		@NamedQuery(name = "findPayrollViewerByTeamContaining", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.team like ?1"),
		@NamedQuery(name = "findPayrollViewerByTotalDeduction", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.totalDeduction = ?1"),
		@NamedQuery(name = "findPayrollViewerByTotalLaborCost", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.totalLaborCost = ?1"),
		@NamedQuery(name = "findPayrollViewerByTotalNetPaymentConverted", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.totalNetPaymentConverted = ?1"),
		@NamedQuery(name = "findPayrollViewerByTotalNetPaymentOriginal", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.totalNetPaymentOriginal = ?1"),
		@NamedQuery(name = "findPayrollViewerByValueDate", query = "select myPayrollViewer from PayrollViewer myPayrollViewer where myPayrollViewer.valueDate = ?1") })

@Table(schema = "cashflow", name = "payroll_viewer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "CashFlowDriver/com/ttv/cashflow/domain", name = "PayrollViewer")

public class PayrollViewer implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "payroll_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String payrollNo;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "month")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar month;
	/**
	 */

	@Column(name = "claim_type", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String claimType;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_schedule_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar paymentScheduleDate;
	/**
	 */

	@Column(name = "description")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String description;
	/**
	 */

	@Column(name = "invoice_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String invoiceNo;
	/**
	 */

	@Column(name = "original_currency_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String originalCurrencyCode;
	/**
	 */

	@Column(name = "total_net_payment_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalNetPaymentOriginal;
	/**
	 */

	@Column(name = "fx_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal fxRate;
	/**
	 */

	@Column(name = "total_net_payment_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalNetPaymentConverted;
	/**
	 */

	@Column(name = "employee_code", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String employeeCode;
	/**
	 */

	@Column(name = "role_title", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String roleTitle;
	/**
	 */

	@Column(name = "division", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String division;
	/**
	 */

	@Column(name = "team", length = 100)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String team;
	/**
	 */

	@Column(name = "labor_base_salary_payment", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborBaseSalaryPayment;
	/**
	 */

	@Column(name = "labor_additional_payment_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment1;
	/**
	 */

	@Column(name = "labor_additional_payment_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment2;
	/**
	 */

	@Column(name = "labor_additional_payment_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborAdditionalPayment3;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer1;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer2;
	/**
	 */

	@Column(name = "labor_social_insurance_employer_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborSocialInsuranceEmployer3;
	/**
	 */

	@Column(name = "labor_other_payment_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment1;
	/**
	 */

	@Column(name = "labor_other_payment_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment2;
	/**
	 */

	@Column(name = "labor_other_payment_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal laborOtherPayment3;
	/**
	 */

	@Column(name = "total_labor_cost", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalLaborCost;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer1;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer2;
	/**
	 */

	@Column(name = "deduction_social_insurance_employer_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployer3;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee1;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee2;
	/**
	 */

	@Column(name = "deduction_social_insurance_employee_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionSocialInsuranceEmployee3;
	/**
	 */

	@Column(name = "deduction_wht", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionWht;
	/**
	 */

	@Column(name = "deduction_other_1", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther1;
	/**
	 */

	@Column(name = "deduction_other_2", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther2;
	/**
	 */

	@Column(name = "deduction_other_3", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal deductionOther3;
	/**
	 */

	@Column(name = "total_deduction", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal totalDeduction;
	/**
	 */

	@Column(name = "net_payment_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentOriginal;
	/**
	 */

	@Column(name = "net_payment_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal netPaymentConverted;
	/**
	 */

	@Column(name = "payment_order_no", length = 40)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String paymentOrderNo;
	/**
	 */

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "value_date")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    Calendar valueDate;
	/**
	 */

	@Column(name = "payment_amount_original", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentAmountOriginal;
	/**
	 */

	@Column(name = "payment_rate", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRate;
	/**
	 */

	@Column(name = "payment_amount_converted", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentAmountConverted;
	/**
	 */

	@Column(name = "payment_remain_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paymentRemainAmount;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}

	/**
	 */
	public String getPayrollNo() {
		return this.payrollNo;
	}

	/**
	 */
	public void setMonth(Calendar month) {
		this.month = month;
	}

	/**
	 */
	public Calendar getMonth() {
		return this.month;
	}

	/**
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 */
	public String getClaimType() {
		return this.claimType;
	}

	/**
	 */
	public void setPaymentScheduleDate(Calendar paymentScheduleDate) {
		this.paymentScheduleDate = paymentScheduleDate;
	}

	/**
	 */
	public Calendar getPaymentScheduleDate() {
		return this.paymentScheduleDate;
	}

	/**
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	/**
	 */
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	/**
	 */
	public String getOriginalCurrencyCode() {
		return this.originalCurrencyCode;
	}

	/**
	 */
	public void setTotalNetPaymentOriginal(BigDecimal totalNetPaymentOriginal) {
		this.totalNetPaymentOriginal = totalNetPaymentOriginal;
	}

	/**
	 */
	public BigDecimal getTotalNetPaymentOriginal() {
		return this.totalNetPaymentOriginal;
	}

	/**
	 */
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 */
	public BigDecimal getFxRate() {
		return this.fxRate;
	}

	/**
	 */
	public void setTotalNetPaymentConverted(BigDecimal totalNetPaymentConverted) {
		this.totalNetPaymentConverted = totalNetPaymentConverted;
	}

	/**
	 */
	public BigDecimal getTotalNetPaymentConverted() {
		return this.totalNetPaymentConverted;
	}

	/**
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 */
	public String getEmployeeCode() {
		return this.employeeCode;
	}

	/**
	 */
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}

	/**
	 */
	public String getRoleTitle() {
		return this.roleTitle;
	}

	/**
	 */
	public void setDivision(String division) {
		this.division = division;
	}

	/**
	 */
	public String getDivision() {
		return this.division;
	}

	/**
	 */
	public void setTeam(String team) {
		this.team = team;
	}

	/**
	 */
	public String getTeam() {
		return this.team;
	}

	/**
	 */
	public void setLaborBaseSalaryPayment(BigDecimal laborBaseSalaryPayment) {
		this.laborBaseSalaryPayment = laborBaseSalaryPayment;
	}

	/**
	 */
	public BigDecimal getLaborBaseSalaryPayment() {
		return this.laborBaseSalaryPayment;
	}

	/**
	 */
	public void setLaborAdditionalPayment1(BigDecimal laborAdditionalPayment1) {
		this.laborAdditionalPayment1 = laborAdditionalPayment1;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment1() {
		return this.laborAdditionalPayment1;
	}

	/**
	 */
	public void setLaborAdditionalPayment2(BigDecimal laborAdditionalPayment2) {
		this.laborAdditionalPayment2 = laborAdditionalPayment2;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment2() {
		return this.laborAdditionalPayment2;
	}

	/**
	 */
	public void setLaborAdditionalPayment3(BigDecimal laborAdditionalPayment3) {
		this.laborAdditionalPayment3 = laborAdditionalPayment3;
	}

	/**
	 */
	public BigDecimal getLaborAdditionalPayment3() {
		return this.laborAdditionalPayment3;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer1(BigDecimal laborSocialInsuranceEmployer1) {
		this.laborSocialInsuranceEmployer1 = laborSocialInsuranceEmployer1;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer1() {
		return this.laborSocialInsuranceEmployer1;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer2(BigDecimal laborSocialInsuranceEmployer2) {
		this.laborSocialInsuranceEmployer2 = laborSocialInsuranceEmployer2;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer2() {
		return this.laborSocialInsuranceEmployer2;
	}

	/**
	 */
	public void setLaborSocialInsuranceEmployer3(BigDecimal laborSocialInsuranceEmployer3) {
		this.laborSocialInsuranceEmployer3 = laborSocialInsuranceEmployer3;
	}

	/**
	 */
	public BigDecimal getLaborSocialInsuranceEmployer3() {
		return this.laborSocialInsuranceEmployer3;
	}

	/**
	 */
	public void setLaborOtherPayment1(BigDecimal laborOtherPayment1) {
		this.laborOtherPayment1 = laborOtherPayment1;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment1() {
		return this.laborOtherPayment1;
	}

	/**
	 */
	public void setLaborOtherPayment2(BigDecimal laborOtherPayment2) {
		this.laborOtherPayment2 = laborOtherPayment2;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment2() {
		return this.laborOtherPayment2;
	}

	/**
	 */
	public void setLaborOtherPayment3(BigDecimal laborOtherPayment3) {
		this.laborOtherPayment3 = laborOtherPayment3;
	}

	/**
	 */
	public BigDecimal getLaborOtherPayment3() {
		return this.laborOtherPayment3;
	}

	/**
	 */
	public void setTotalLaborCost(BigDecimal totalLaborCost) {
		this.totalLaborCost = totalLaborCost;
	}

	/**
	 */
	public BigDecimal getTotalLaborCost() {
		return this.totalLaborCost;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer1(BigDecimal deductionSocialInsuranceEmployer1) {
		this.deductionSocialInsuranceEmployer1 = deductionSocialInsuranceEmployer1;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer1() {
		return this.deductionSocialInsuranceEmployer1;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer2(BigDecimal deductionSocialInsuranceEmployer2) {
		this.deductionSocialInsuranceEmployer2 = deductionSocialInsuranceEmployer2;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer2() {
		return this.deductionSocialInsuranceEmployer2;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployer3(BigDecimal deductionSocialInsuranceEmployer3) {
		this.deductionSocialInsuranceEmployer3 = deductionSocialInsuranceEmployer3;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployer3() {
		return this.deductionSocialInsuranceEmployer3;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee1(BigDecimal deductionSocialInsuranceEmployee1) {
		this.deductionSocialInsuranceEmployee1 = deductionSocialInsuranceEmployee1;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee1() {
		return this.deductionSocialInsuranceEmployee1;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee2(BigDecimal deductionSocialInsuranceEmployee2) {
		this.deductionSocialInsuranceEmployee2 = deductionSocialInsuranceEmployee2;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee2() {
		return this.deductionSocialInsuranceEmployee2;
	}

	/**
	 */
	public void setDeductionSocialInsuranceEmployee3(BigDecimal deductionSocialInsuranceEmployee3) {
		this.deductionSocialInsuranceEmployee3 = deductionSocialInsuranceEmployee3;
	}

	/**
	 */
	public BigDecimal getDeductionSocialInsuranceEmployee3() {
		return this.deductionSocialInsuranceEmployee3;
	}

	/**
	 */
	public void setDeductionWht(BigDecimal deductionWht) {
		this.deductionWht = deductionWht;
	}

	/**
	 */
	public BigDecimal getDeductionWht() {
		return this.deductionWht;
	}

	/**
	 */
	public void setDeductionOther1(BigDecimal deductionOther1) {
		this.deductionOther1 = deductionOther1;
	}

	/**
	 */
	public BigDecimal getDeductionOther1() {
		return this.deductionOther1;
	}

	/**
	 */
	public void setDeductionOther2(BigDecimal deductionOther2) {
		this.deductionOther2 = deductionOther2;
	}

	/**
	 */
	public BigDecimal getDeductionOther2() {
		return this.deductionOther2;
	}

	/**
	 */
	public void setDeductionOther3(BigDecimal deductionOther3) {
		this.deductionOther3 = deductionOther3;
	}

	/**
	 */
	public BigDecimal getDeductionOther3() {
		return this.deductionOther3;
	}

	/**
	 */
	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	/**
	 */
	public BigDecimal getTotalDeduction() {
		return this.totalDeduction;
	}

	/**
	 */
	public void setNetPaymentOriginal(BigDecimal netPaymentOriginal) {
		this.netPaymentOriginal = netPaymentOriginal;
	}

	/**
	 */
	public BigDecimal getNetPaymentOriginal() {
		return this.netPaymentOriginal;
	}

	/**
	 */
	public void setNetPaymentConverted(BigDecimal netPaymentConverted) {
		this.netPaymentConverted = netPaymentConverted;
	}

	/**
	 */
	public BigDecimal getNetPaymentConverted() {
		return this.netPaymentConverted;
	}

	/**
	 */
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	/**
	 */
	public String getPaymentOrderNo() {
		return this.paymentOrderNo;
	}

	/**
	 */
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 */
	public Calendar getValueDate() {
		return this.valueDate;
	}

	/**
	 */
	public void setPaymentAmountOriginal(BigDecimal paymentAmountOriginal) {
		this.paymentAmountOriginal = paymentAmountOriginal;
	}

	/**
	 */
	public BigDecimal getPaymentAmountOriginal() {
		return this.paymentAmountOriginal;
	}

	/**
	 */
	public void setPaymentRate(BigDecimal paymentRate) {
		this.paymentRate = paymentRate;
	}

	/**
	 */
	public BigDecimal getPaymentRate() {
		return this.paymentRate;
	}

	/**
	 */
	public void setPaymentAmountConverted(BigDecimal paymentAmountConverted) {
		this.paymentAmountConverted = paymentAmountConverted;
	}

	/**
	 */
	public BigDecimal getPaymentAmountConverted() {
		return this.paymentAmountConverted;
	}

	/**
	 */
	public void setPaymentRemainAmount(BigDecimal paymentRemainAmount) {
		this.paymentRemainAmount = paymentRemainAmount;
	}

	/**
	 */
	public BigDecimal getPaymentRemainAmount() {
		return this.paymentRemainAmount;
	}

	/**
	 */
	public PayrollViewer() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(PayrollViewer that) {
		setId(that.getId());
		setPayrollNo(that.getPayrollNo());
		setMonth(that.getMonth());
		setClaimType(that.getClaimType());
		setPaymentScheduleDate(that.getPaymentScheduleDate());
		setDescription(that.getDescription());
		setInvoiceNo(that.getInvoiceNo());
		setOriginalCurrencyCode(that.getOriginalCurrencyCode());
		setTotalNetPaymentOriginal(that.getTotalNetPaymentOriginal());
		setFxRate(that.getFxRate());
		setTotalNetPaymentConverted(that.getTotalNetPaymentConverted());
		setEmployeeCode(that.getEmployeeCode());
		setRoleTitle(that.getRoleTitle());
		setDivision(that.getDivision());
		setTeam(that.getTeam());
		setLaborBaseSalaryPayment(that.getLaborBaseSalaryPayment());
		setLaborAdditionalPayment1(that.getLaborAdditionalPayment1());
		setLaborAdditionalPayment2(that.getLaborAdditionalPayment2());
		setLaborAdditionalPayment3(that.getLaborAdditionalPayment3());
		setLaborSocialInsuranceEmployer1(that.getLaborSocialInsuranceEmployer1());
		setLaborSocialInsuranceEmployer2(that.getLaborSocialInsuranceEmployer2());
		setLaborSocialInsuranceEmployer3(that.getLaborSocialInsuranceEmployer3());
		setLaborOtherPayment1(that.getLaborOtherPayment1());
		setLaborOtherPayment2(that.getLaborOtherPayment2());
		setLaborOtherPayment3(that.getLaborOtherPayment3());
		setTotalLaborCost(that.getTotalLaborCost());
		setDeductionSocialInsuranceEmployer1(that.getDeductionSocialInsuranceEmployer1());
		setDeductionSocialInsuranceEmployer2(that.getDeductionSocialInsuranceEmployer2());
		setDeductionSocialInsuranceEmployer3(that.getDeductionSocialInsuranceEmployer3());
		setDeductionSocialInsuranceEmployee1(that.getDeductionSocialInsuranceEmployee1());
		setDeductionSocialInsuranceEmployee2(that.getDeductionSocialInsuranceEmployee2());
		setDeductionSocialInsuranceEmployee3(that.getDeductionSocialInsuranceEmployee3());
		setDeductionWht(that.getDeductionWht());
		setDeductionOther1(that.getDeductionOther1());
		setDeductionOther2(that.getDeductionOther2());
		setDeductionOther3(that.getDeductionOther3());
		setTotalDeduction(that.getTotalDeduction());
		setNetPaymentOriginal(that.getNetPaymentOriginal());
		setNetPaymentConverted(that.getNetPaymentConverted());
		setPaymentOrderNo(that.getPaymentOrderNo());
		setValueDate(that.getValueDate());
		setPaymentAmountOriginal(that.getPaymentAmountOriginal());
		setPaymentRate(that.getPaymentRate());
		setPaymentAmountConverted(that.getPaymentAmountConverted());
		setPaymentRemainAmount(that.getPaymentRemainAmount());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("payrollNo=[").append(payrollNo).append("] ");
		buffer.append("month=[").append(month).append("] ");
		buffer.append("claimType=[").append(claimType).append("] ");
		buffer.append("paymentScheduleDate=[").append(paymentScheduleDate).append("] ");
		buffer.append("description=[").append(description).append("] ");
		buffer.append("invoiceNo=[").append(invoiceNo).append("] ");
		buffer.append("originalCurrencyCode=[").append(originalCurrencyCode).append("] ");
		buffer.append("totalNetPaymentOriginal=[").append(totalNetPaymentOriginal).append("] ");
		buffer.append("fxRate=[").append(fxRate).append("] ");
		buffer.append("totalNetPaymentConverted=[").append(totalNetPaymentConverted).append("] ");
		buffer.append("employeeCode=[").append(employeeCode).append("] ");
		buffer.append("roleTitle=[").append(roleTitle).append("] ");
		buffer.append("division=[").append(division).append("] ");
		buffer.append("team=[").append(team).append("] ");
		buffer.append("laborBaseSalaryPayment=[").append(laborBaseSalaryPayment).append("] ");
		buffer.append("laborAdditionalPayment1=[").append(laborAdditionalPayment1).append("] ");
		buffer.append("laborAdditionalPayment2=[").append(laborAdditionalPayment2).append("] ");
		buffer.append("laborAdditionalPayment3=[").append(laborAdditionalPayment3).append("] ");
		buffer.append("laborSocialInsuranceEmployer1=[").append(laborSocialInsuranceEmployer1).append("] ");
		buffer.append("laborSocialInsuranceEmployer2=[").append(laborSocialInsuranceEmployer2).append("] ");
		buffer.append("laborSocialInsuranceEmployer3=[").append(laborSocialInsuranceEmployer3).append("] ");
		buffer.append("laborOtherPayment1=[").append(laborOtherPayment1).append("] ");
		buffer.append("laborOtherPayment2=[").append(laborOtherPayment2).append("] ");
		buffer.append("laborOtherPayment3=[").append(laborOtherPayment3).append("] ");
		buffer.append("totalLaborCost=[").append(totalLaborCost).append("] ");
		buffer.append("deductionSocialInsuranceEmployer1=[").append(deductionSocialInsuranceEmployer1).append("] ");
		buffer.append("deductionSocialInsuranceEmployer2=[").append(deductionSocialInsuranceEmployer2).append("] ");
		buffer.append("deductionSocialInsuranceEmployer3=[").append(deductionSocialInsuranceEmployer3).append("] ");
		buffer.append("deductionSocialInsuranceEmployee1=[").append(deductionSocialInsuranceEmployee1).append("] ");
		buffer.append("deductionSocialInsuranceEmployee2=[").append(deductionSocialInsuranceEmployee2).append("] ");
		buffer.append("deductionSocialInsuranceEmployee3=[").append(deductionSocialInsuranceEmployee3).append("] ");
		buffer.append("deductionWht=[").append(deductionWht).append("] ");
		buffer.append("deductionOther1=[").append(deductionOther1).append("] ");
		buffer.append("deductionOther2=[").append(deductionOther2).append("] ");
		buffer.append("deductionOther3=[").append(deductionOther3).append("] ");
		buffer.append("totalDeduction=[").append(totalDeduction).append("] ");
		buffer.append("netPaymentOriginal=[").append(netPaymentOriginal).append("] ");
		buffer.append("netPaymentConverted=[").append(netPaymentConverted).append("] ");
		buffer.append("paymentOrderNo=[").append(paymentOrderNo).append("] ");
		buffer.append("valueDate=[").append(valueDate).append("] ");
		buffer.append("paymentAmountOriginal=[").append(paymentAmountOriginal).append("] ");
		buffer.append("paymentRate=[").append(paymentRate).append("] ");
		buffer.append("paymentAmountConverted=[").append(paymentAmountConverted).append("] ");
		buffer.append("paymentRemainAmount=[").append(paymentRemainAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof PayrollViewer))
			return false;
		PayrollViewer equalCheck = (PayrollViewer) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
