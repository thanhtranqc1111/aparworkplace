
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllApInvoicePaymentStatuss", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus"),
		@NamedQuery(name = "findApInvoicePaymentStatusById", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus where myApInvoicePaymentStatus.id = ?1"),
		@NamedQuery(name = "findApInvoicePaymentStatusByApInvoiceId", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus where myApInvoicePaymentStatus.apInvoiceId = ?1"),
		@NamedQuery(name = "findApInvoicePaymentStatusByPaidUnconvertedAmount", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus where myApInvoicePaymentStatus.paidIncludeGstOriginalAmount = ?1"),
		@NamedQuery(name = "findApInvoicePaymentStatusByPrimaryKey", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus where myApInvoicePaymentStatus.id = ?1"),
		@NamedQuery(name = "findApInvoicePaymentStatusByUnpaidUnconvertedAmount", query = "select myApInvoicePaymentStatus from ApInvoicePaymentStatus myApInvoicePaymentStatus where myApInvoicePaymentStatus.unpaidIncludeGstOriginalAmount = ?1") })

@Table(schema = "cashflow", name = "ap_invoice_payment_status")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow/com/ttv/cashflow/domain", name = "ApInvoicePaymentStatus")

public class ApInvoicePaymentStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "paid_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal paidIncludeGstOriginalAmount;
	/**
	 */

	@Column(name = "unpaid_include_gst_original_amount", scale = 17, precision = 17)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal unpaidIncludeGstOriginalAmount;

	@Column(name = "ap_invoice_id")
    @Basic(fetch = FetchType.EAGER)

    @XmlElement
    BigInteger apInvoiceId;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	public BigDecimal getPaidIncludeGstOriginalAmount() {
		return paidIncludeGstOriginalAmount;
	}

	public void setPaidIncludeGstOriginalAmount(BigDecimal paidIncludeGstOriginalAmount) {
		this.paidIncludeGstOriginalAmount = paidIncludeGstOriginalAmount;
	}

	public BigDecimal getUnpaidIncludeGstOriginalAmount() {
		return unpaidIncludeGstOriginalAmount;
	}

	public void setUnpaidIncludeGstOriginalAmount(BigDecimal unpaidIncludeGstOriginalAmount) {
		this.unpaidIncludeGstOriginalAmount = unpaidIncludeGstOriginalAmount;
	}

	public BigInteger getApInvoiceId() {
        return apInvoiceId;
    }
	
	public void setApInvoiceId(BigInteger apInvoiceId) {
        this.apInvoiceId = apInvoiceId;
    }

	/**
	 */
	public ApInvoicePaymentStatus() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ApInvoicePaymentStatus that) {
		setId(that.getId());
		setPaidIncludeGstOriginalAmount(that.getPaidIncludeGstOriginalAmount());
		setUnpaidIncludeGstOriginalAmount(that.getUnpaidIncludeGstOriginalAmount());
		setApInvoiceId(that.getApInvoiceId());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("paidIncludeGstOriginalAmount=[").append(paidIncludeGstOriginalAmount).append("] ");
		buffer.append("unpaidIncludeGstOriginalAmount=[").append(unpaidIncludeGstOriginalAmount).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ApInvoicePaymentStatus))
			return false;
		ApInvoicePaymentStatus equalCheck = (ApInvoicePaymentStatus) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
