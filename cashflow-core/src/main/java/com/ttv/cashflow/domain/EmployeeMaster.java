
package com.ttv.cashflow.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.format.annotation.DateTimeFormat;

import com.ttv.cashflow.dto.EmployeeMasterDTO;
import com.ttv.cashflow.util.Constant;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllEmployeeMasters", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster"),
		@NamedQuery(name = "findEmployeeMasterByCode", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.code = ?1"),
		@NamedQuery(name = "findEmployeeMasterByCodeContaining", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.code like ?1"),
		@NamedQuery(name = "findEmployeeMasterById", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.id = ?1"),
		@NamedQuery(name = "findEmployeeMasterByName", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.name = ?1"),
		@NamedQuery(name = "findEmployeeMasterByNameContaining", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.name like ?1"),
		@NamedQuery(name = "findEmployeeMasterByPrimaryKey", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.id = ?1"),
		@NamedQuery(name = "findEmployeeMasterByRoleTitle", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.roleTitle = ?1"),
		@NamedQuery(name = "findEmployeeMasterByRoleTitleContaining", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.roleTitle like ?1"),
		@NamedQuery(name = "findEmployeeMasterByTeam", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.team = ?1"),
		@NamedQuery(name = "findEmployeeMasterByTeamContaining", query = "select myEmployeeMaster from EmployeeMaster myEmployeeMaster where myEmployeeMaster.team like ?1") })

@SqlResultSetMappings({
    @SqlResultSetMapping(name = "EmployeeMasterDTO", classes = {
        @ConstructorResult(targetClass = EmployeeMasterDTO.class, 
            columns = { 
                @ColumnResult(name = "id", type = BigInteger.class),
                @ColumnResult(name = "code"),
                @ColumnResult(name = "name"),
                @ColumnResult(name = "roleTitle"),
                @ColumnResult(name = "team"),
                @ColumnResult(name = "divisionName"),
                @ColumnResult(name = "divisionCode"),
                @ColumnResult(name = "joinDate", type=Calendar.class),
                @ColumnResult(name = "resignDate", type=Calendar.class)
        })
    })
})

@Table(schema = "cashflow", name = "employee_master")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "cashflow_generate/com/ttv/cashflow/domain", name = "EmployeeMaster")

public class EmployeeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement
	BigInteger id;
	/**
	 */

	@Column(name = "code", length = 40)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String code;
	/**
	 */

	@Column(name = "name", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String name;
	/**
	 */

	@Column(name = "role_title", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String roleTitle;
	/**
	 */

	@Column(name = "team", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String team;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "join_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	Calendar joinDate;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "resign_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	@DateTimeFormat(pattern = Constant.DDMMYYYY)
	Calendar resignDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "division_id", referencedColumnName = "id") })
	@XmlTransient
	DivisionMaster divisionMaster;
	
	/**
     */
    @OneToMany(mappedBy = "employeeMaster", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

    @XmlElement(name = "", namespace = "")
    java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> projectAllocations;

	/**
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 */
	public BigInteger getId() {
		return this.id;
	}

	/**
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}

	/**
	 */
	public String getRoleTitle() {
		return this.roleTitle;
	}

	/**
	 */
	public void setTeam(String team) {
		this.team = team;
	}

	/**
	 */
	public String getTeam() {
		return this.team;
	}

	/**
	 */
	public void setDivisionMaster(DivisionMaster divisionMaster) {
		this.divisionMaster = divisionMaster;
	}

	/**
	 */
	public DivisionMaster getDivisionMaster() {
		return divisionMaster;
	}

	/**
	 */
	public Calendar getJoinDate() {
		return joinDate;
	}

	/**
	 */
	public void setJoinDate(Calendar joinDate) {
		this.joinDate = joinDate;
	}

	/**
	 */
	public Calendar getResignDate() {
		return resignDate;
	}

	/**
	 */
	public void setResignDate(Calendar resignDate) {
		this.resignDate = resignDate;
	}
	
	public java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> getProjectAllocations() {
        return projectAllocations;
    }
	
	public void setProjectAllocations(java.util.Set<com.ttv.cashflow.domain.ProjectAllocation> projectAllocations) {
        this.projectAllocations = projectAllocations;
    }
		
	/**
	 */
	public EmployeeMaster() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(EmployeeMaster that) {
		setId(that.getId());
		setCode(that.getCode());
		setName(that.getName());
		setRoleTitle(that.getRoleTitle());
		setTeam(that.getTeam());
		setDivisionMaster(that.getDivisionMaster());
		setJoinDate(that.getJoinDate());
		setResignDate(that.getResignDate());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("code=[").append(code).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("roleTitle=[").append(roleTitle).append("] ");
		buffer.append("team=[").append(team).append("] ");
		buffer.append("joinDate=[").append(joinDate).append("] ");
		buffer.append("resignDate=[").append(resignDate).append("] ");
		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof EmployeeMaster))
			return false;
		EmployeeMaster equalCheck = (EmployeeMaster) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
