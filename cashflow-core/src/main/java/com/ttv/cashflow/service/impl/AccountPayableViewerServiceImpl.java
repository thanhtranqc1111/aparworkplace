package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.AccountPayableViewerDAO;
import com.ttv.cashflow.domain.AccountPayableViewer;
import com.ttv.cashflow.service.AccountPayableViewerService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for AccountPayableViewer entities
 * 
 */

@Service("AccountPayableViewerService")

@Transactional
public class AccountPayableViewerServiceImpl implements AccountPayableViewerService {

	/**
	 * DAO injected by Spring that manages AccountPayableViewer entities
	 * 
	 */
	@Autowired
	private AccountPayableViewerDAO accountPayableViewerDAO;

	/**
	 * Instantiates a new AccountPayableViewerServiceImpl.
	 *
	 */
	public AccountPayableViewerServiceImpl() {
	}

	/**
	 * Return a count of all AccountPayableViewer entity
	 * 
	 */
	@Transactional
	public Integer countAccountPayableViewers() {
		return ((Long) accountPayableViewerDAO.createQuerySingleResult("select count(o) from AccountPayableViewer o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing AccountPayableViewer entity
	 * 
	 */
	@Transactional
	public Set<AccountPayableViewer> loadAccountPayableViewers() {
		return accountPayableViewerDAO.findAllAccountPayableViewers();
	}

	/**
	 * Return all AccountPayableViewer entity
	 * 
	 */
	@Transactional
	public List<AccountPayableViewer> findAllAccountPayableViewers(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<AccountPayableViewer>(accountPayableViewerDAO.findAllAccountPayableViewers(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id) {
		return accountPayableViewerDAO.findAccountPayableViewerByPrimaryKey(id);
	}

	/**
	 * Save an existing AccountPayableViewer entity
	 * 
	 */
	@Transactional
	public void saveAccountPayableViewer(AccountPayableViewer accountpayableviewer) {
		AccountPayableViewer existingAccountPayableViewer = accountPayableViewerDAO.findAccountPayableViewerByPrimaryKey(accountpayableviewer.getId());

		if (existingAccountPayableViewer != null) {
			if (existingAccountPayableViewer != accountpayableviewer) {
				existingAccountPayableViewer.setId(accountpayableviewer.getId());
				existingAccountPayableViewer.setAccountPayableNo(accountpayableviewer.getAccountPayableNo());
				existingAccountPayableViewer.setMonth(accountpayableviewer.getMonth());
				existingAccountPayableViewer.setClaimType(accountpayableviewer.getClaimType());
				existingAccountPayableViewer.setVendor(accountpayableviewer.getVendor());
				existingAccountPayableViewer.setInvoiceNo(accountpayableviewer.getInvoiceNo());
				existingAccountPayableViewer.setApprovalCode(accountpayableviewer.getApprovalCode());
				existingAccountPayableViewer.setCurrency(accountpayableviewer.getCurrency());
				existingAccountPayableViewer.setOriginalAmount(accountpayableviewer.getOriginalAmount());
				existingAccountPayableViewer.setFxRate(accountpayableviewer.getFxRate());
				existingAccountPayableViewer.setConvertedAmount(accountpayableviewer.getConvertedAmount());
				existingAccountPayableViewer.setAccountName(accountpayableviewer.getAccountName());
				existingAccountPayableViewer.setProjectName(accountpayableviewer.getProjectName());
				existingAccountPayableViewer.setDescription(accountpayableviewer.getDescription());
				existingAccountPayableViewer.setPaymentOrderNo(accountpayableviewer.getPaymentOrderNo());
				existingAccountPayableViewer.setValueDate(accountpayableviewer.getValueDate());
				existingAccountPayableViewer.setPaymentOriginalAmount(accountpayableviewer.getPaymentOriginalAmount());
				existingAccountPayableViewer.setPaymentRate(accountpayableviewer.getPaymentRate());
				existingAccountPayableViewer.setPaymentConvertedAmount(accountpayableviewer.getPaymentConvertedAmount());
				existingAccountPayableViewer.setVarianceAmount(accountpayableviewer.getVarianceAmount());
				existingAccountPayableViewer.setRemainAmount(accountpayableviewer.getRemainAmount());
			}
			accountpayableviewer = accountPayableViewerDAO.store(existingAccountPayableViewer);
		} else {
			accountpayableviewer = accountPayableViewerDAO.store(accountpayableviewer);
		}
		accountPayableViewerDAO.flush();
	}

	/**
	 * Delete an existing AccountPayableViewer entity
	 * 
	 */
	@Transactional
	public void deleteAccountPayableViewer(AccountPayableViewer accountpayableviewer) {
		accountPayableViewerDAO.remove(accountpayableviewer);
		accountPayableViewerDAO.flush();
	}
}
