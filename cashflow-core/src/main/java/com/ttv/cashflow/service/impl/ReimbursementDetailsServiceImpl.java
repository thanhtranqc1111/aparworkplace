package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dao.ReimbursementDetailsDAO;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementDetails;
import com.ttv.cashflow.service.ReimbursementDetailsService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ReimbursementDetails entities
 * 
 */

@Service("ReimbursementDetailsService")

@Transactional
public class ReimbursementDetailsServiceImpl implements ReimbursementDetailsService {

	/**
	 * DAO injected by Spring that manages Reimbursement entities
	 * 
	 */
	@Autowired
	private ReimbursementDAO reimbursementDAO;

	/**
	 * DAO injected by Spring that manages ReimbursementDetails entities
	 * 
	 */
	@Autowired
	private ReimbursementDetailsDAO reimbursementDetailsDAO;

	/**
	 * Instantiates a new ReimbursementDetailsServiceImpl.
	 *
	 */
	public ReimbursementDetailsServiceImpl() {
	}

	/**
	 * Load an existing ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public Set<ReimbursementDetails> loadReimbursementDetailss() {
		return reimbursementDetailsDAO.findAllReimbursementDetailss();
	}

	/**
	 * Return all ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public List<ReimbursementDetails> findAllReimbursementDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ReimbursementDetails>(reimbursementDetailsDAO.findAllReimbursementDetailss(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id) {
		return reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(id);
	}

	/**
	 * Delete an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public ReimbursementDetails deleteReimbursementDetailsReimbursement(BigInteger reimbursementdetails_id, BigInteger related_reimbursement_id) {
		ReimbursementDetails reimbursementdetails = reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(reimbursementdetails_id, -1, -1);
		Reimbursement related_reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(related_reimbursement_id, -1, -1);

		reimbursementdetails.setReimbursement(null);
		related_reimbursement.getReimbursementDetailses().remove(reimbursementdetails);
		reimbursementdetails = reimbursementDetailsDAO.store(reimbursementdetails);
		reimbursementDetailsDAO.flush();

		related_reimbursement = reimbursementDAO.store(related_reimbursement);
		reimbursementDAO.flush();

		reimbursementDAO.remove(related_reimbursement);
		reimbursementDAO.flush();

		return reimbursementdetails;
	}

	/**
	 * Return a count of all ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public Integer countReimbursementDetailss() {
		return ((Long) reimbursementDetailsDAO.createQuerySingleResult("select count(o) from ReimbursementDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public ReimbursementDetails saveReimbursementDetailsReimbursement(BigInteger id, Reimbursement related_reimbursement) {
		ReimbursementDetails reimbursementdetails = reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(id, -1, -1);
		Reimbursement existingreimbursement = reimbursementDAO.findReimbursementByPrimaryKey(related_reimbursement.getId());

		// copy into the existing record to preserve existing relationships
		if (existingreimbursement != null) {
			existingreimbursement.setId(related_reimbursement.getId());
			existingreimbursement.setEmployeeCode(related_reimbursement.getEmployeeCode());
			existingreimbursement.setEmployeeName(related_reimbursement.getEmployeeName());
			existingreimbursement.setReimbursementNo(related_reimbursement.getReimbursementNo());
			existingreimbursement.setOriginalCurrencyCode(related_reimbursement.getOriginalCurrencyCode());
			existingreimbursement.setFxRate(related_reimbursement.getFxRate());
			existingreimbursement.setBookingDate(related_reimbursement.getBookingDate());
			existingreimbursement.setMonth(related_reimbursement.getMonth());
			existingreimbursement.setClaimType(related_reimbursement.getClaimType());
			existingreimbursement.setAccountPayableCode(related_reimbursement.getAccountPayableCode());
			existingreimbursement.setAccountPayableName(related_reimbursement.getAccountPayableName());
			existingreimbursement.setDescription(related_reimbursement.getDescription());
			existingreimbursement.setIncludeGstTotalOriginalAmount(related_reimbursement.getIncludeGstTotalOriginalAmount());
			existingreimbursement.setIncludeGstTotalConvertedAmount(related_reimbursement.getIncludeGstTotalConvertedAmount());
			existingreimbursement.setScheduledPaymentDate(related_reimbursement.getScheduledPaymentDate());
			existingreimbursement.setStatus(related_reimbursement.getStatus());
			existingreimbursement.setCreatedDate(related_reimbursement.getCreatedDate());
			existingreimbursement.setModifiedDate(related_reimbursement.getModifiedDate());
			related_reimbursement = existingreimbursement;
		} else {
			related_reimbursement = reimbursementDAO.store(related_reimbursement);
			reimbursementDAO.flush();
		}

		reimbursementdetails.setReimbursement(related_reimbursement);
		related_reimbursement.getReimbursementDetailses().add(reimbursementdetails);
		reimbursementdetails = reimbursementDetailsDAO.store(reimbursementdetails);
		reimbursementDetailsDAO.flush();

		related_reimbursement = reimbursementDAO.store(related_reimbursement);
		reimbursementDAO.flush();

		return reimbursementdetails;
	}

	/**
	 * Save an existing ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public void saveReimbursementDetails(ReimbursementDetails reimbursementdetails) {
		ReimbursementDetails existingReimbursementDetails = reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(reimbursementdetails.getId());

		if (existingReimbursementDetails != null) {
			if (existingReimbursementDetails != reimbursementdetails) {
				existingReimbursementDetails.setId(reimbursementdetails.getId());
				existingReimbursementDetails.setProjectCode(reimbursementdetails.getProjectCode());
				existingReimbursementDetails.setProjectName(reimbursementdetails.getProjectName());
				existingReimbursementDetails.setApprovalCode(reimbursementdetails.getApprovalCode());
				existingReimbursementDetails.setDescription(reimbursementdetails.getDescription());
				existingReimbursementDetails.setIncludeGstOriginalAmount(reimbursementdetails.getIncludeGstOriginalAmount());
				existingReimbursementDetails.setIncludeGstConvertedAmount(reimbursementdetails.getIncludeGstConvertedAmount());
				existingReimbursementDetails.setCreatedDate(reimbursementdetails.getCreatedDate());
				existingReimbursementDetails.setModifiedDate(reimbursementdetails.getModifiedDate());
			}
			reimbursementdetails = reimbursementDetailsDAO.store(existingReimbursementDetails);
		} else {
			reimbursementdetails = reimbursementDetailsDAO.store(reimbursementdetails);
		}
		reimbursementDetailsDAO.flush();
	}

	/**
	 * Delete an existing ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public void deleteReimbursementDetails(ReimbursementDetails reimbursementdetails) {
		reimbursementDetailsDAO.remove(reimbursementdetails);
		reimbursementDetailsDAO.flush();
	}
}
