
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.BankAccountMaster;

import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

/**
 * Spring service that handles CRUD requests for BankAccountMaster entities
 * 
 */
public interface BankAccountMasterService {

	/**
	 * Return all BankAccountMaster entity
	 * 
	 */
	public List<BankAccountMaster> findAllBankAccountMasters(Integer startResult, Integer maxRows);

	/**
	 * Save an existing BankAccountMaster entity
	 * 
	 */
	public void saveBankAccountMaster(BankAccountMaster bankaccountmaster);

	/**
	 * Load an existing BankAccountMaster entity
	 * 
	 */
	public Set<BankAccountMaster> loadBankAccountMasters();

	/**
	 * Return a count of all BankAccountMaster entity
	 * 
	 */
	public Integer countBankAccountMasters();

	/**
	 * Delete an existing BankAccountMaster entity
	 * 
	 */
	public void deleteBankAccountMaster(BankAccountMaster bankaccountmaster_1);

	/**
	 */
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code);

	public void deleteBankAccountMasterByCodes(String codes[]);

	public String findBankAccountMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode_1) throws DataAccessException;
}