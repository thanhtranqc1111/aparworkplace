package com.ttv.cashflow.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.RoleDAO;
import com.ttv.cashflow.dao.RolePermissionDAO;
import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.RolePermission;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.dto.RoleDTO;
import com.ttv.cashflow.service.RoleService;

/**
 * Spring service that handles CRUD requests for Role entities
 * 
 */

@Service("RoleService")

@Transactional
public class RoleServiceImpl implements RoleService {

	/**
	 * DAO injected by Spring that manages Role entities
	 * 
	 */
	@Autowired
	private RoleDAO roleDAO;

	/**
	 * DAO injected by Spring that manages RolePermission entities
	 * 
	 */
	@Autowired
	private RolePermissionDAO rolePermissionDAO;

	/**
	 * DAO injected by Spring that manages UserAccount entities
	 * 
	 */
	@Autowired
	private UserAccountDAO userAccountDAO;

	/**
	 * Instantiates a new RoleServiceImpl.
	 *
	 */
	public RoleServiceImpl() {
	}

	/**
	 * Delete an existing Role entity
	 * 
	 */
	@Transactional
	public void deleteRole(Role role) {
		roleDAO.remove(role);
		roleDAO.flush();
	}

	/**
	 * Load an existing Role entity
	 * 
	 */
	@Transactional
	public Set<Role> loadRoles() {
		return roleDAO.findAllRoles();
	}

	/**
	 * Delete an existing RolePermission entity
	 * 
	 */
	@Transactional
	public Role deleteRoleRolePermissions(Integer role_id, Integer related_rolepermissions_id) {
		RolePermission related_rolepermissions = rolePermissionDAO.findRolePermissionByPrimaryKey(related_rolepermissions_id, -1, -1);

		Role role = roleDAO.findRoleByPrimaryKey(role_id, -1, -1);

		related_rolepermissions.setRole(null);
		role.getRolePermissions().remove(related_rolepermissions);

		rolePermissionDAO.remove(related_rolepermissions);
		rolePermissionDAO.flush();

		return role;
	}

	/**
	 * Return a count of all Role entity
	 * 
	 */
	@Transactional
	public Integer countRoles() {
		return ((Long) roleDAO.createQuerySingleResult("select count(o) from Role o").getSingleResult()).intValue();
	}

	/**
	 * Return all Role entity
	 * 
	 */
	@Transactional
	public List<Role> findAllRoles(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Role>(roleDAO.findAllRoles(startResult, maxRows));
	}

	/**
	 * Save an existing Role entity
	 * 
	 */
	@Transactional
	public void saveRole(Role role) {
		Role existingRole = roleDAO.findRoleByPrimaryKey(role.getId());

		if (existingRole != null) {
			if (existingRole != role) {
				existingRole.setId(role.getId());
				existingRole.setName(role.getName());
				existingRole.setDescription(role.getDescription());
				existingRole.setModifiedDate(Calendar.getInstance());
			}
			role = roleDAO.store(existingRole);
		} else {
			role.setCreatedDate(Calendar.getInstance());
			role = roleDAO.store(role);
		}
		roleDAO.flush();
	}

	/**
	 * Save an existing UserAccount entity
	 * 
	 */
	@Transactional
	public Role saveRoleUserAccounts(Integer id, UserAccount related_useraccounts) {
		Role role = roleDAO.findRoleByPrimaryKey(id, -1, -1);
		UserAccount existinguserAccounts = userAccountDAO.findUserAccountByPrimaryKey(related_useraccounts.getId());

		// copy into the existing record to preserve existing relationships
		if (existinguserAccounts != null) {
			existinguserAccounts.setId(related_useraccounts.getId());
			existinguserAccounts.setEmail(related_useraccounts.getEmail());
			existinguserAccounts.setPassword(related_useraccounts.getPassword());
			existinguserAccounts.setFirstName(related_useraccounts.getFirstName());
			existinguserAccounts.setLastName(related_useraccounts.getLastName());
			existinguserAccounts.setPhone(related_useraccounts.getPhone());
			existinguserAccounts.setIsActive(related_useraccounts.getIsActive());
			existinguserAccounts.setCreatedDate(related_useraccounts.getCreatedDate());
			existinguserAccounts.setModifiedDate(related_useraccounts.getModifiedDate());
			existinguserAccounts.setIsUsing2fa(related_useraccounts.getIsUsing2fa());
			related_useraccounts = existinguserAccounts;
		} else {
			related_useraccounts = userAccountDAO.store(related_useraccounts);
			userAccountDAO.flush();
		}

		related_useraccounts.setRole(role);
		role.getUserAccounts().add(related_useraccounts);
		related_useraccounts = userAccountDAO.store(related_useraccounts);
		userAccountDAO.flush();

		role = roleDAO.store(role);
		roleDAO.flush();

		return role;
	}

	/**
	 * Delete an existing UserAccount entity
	 * 
	 */
	@Transactional
	public Role deleteRoleUserAccounts(Integer role_id, Integer related_useraccounts_id) {
		UserAccount related_useraccounts = userAccountDAO.findUserAccountByPrimaryKey(related_useraccounts_id, -1, -1);

		Role role = roleDAO.findRoleByPrimaryKey(role_id, -1, -1);

		related_useraccounts.setRole(null);
		role.getUserAccounts().remove(related_useraccounts);

		userAccountDAO.remove(related_useraccounts);
		userAccountDAO.flush();

		return role;
	}

	/**
	 * Save an existing RolePermission entity
	 * 
	 */
	@Transactional
	public Role saveRoleRolePermissions(Integer id, RolePermission related_rolepermissions) {
		Role role = roleDAO.findRoleByPrimaryKey(id, -1, -1);
		RolePermission existingrolePermissions = rolePermissionDAO.findRolePermissionByPrimaryKey(related_rolepermissions.getId());

		// copy into the existing record to preserve existing relationships
		if (existingrolePermissions != null) {
			existingrolePermissions.setId(related_rolepermissions.getId());
			existingrolePermissions.setCreatedDate(related_rolepermissions.getCreatedDate());
			existingrolePermissions.setModifiedDate(related_rolepermissions.getModifiedDate());
			related_rolepermissions = existingrolePermissions;
		}

		related_rolepermissions.setRole(role);
		role.getRolePermissions().add(related_rolepermissions);
		related_rolepermissions = rolePermissionDAO.store(related_rolepermissions);
		rolePermissionDAO.flush();

		role = roleDAO.store(role);
		roleDAO.flush();

		return role;
	}

	/**
	 */
	@Transactional
	public Role findRoleByPrimaryKey(Integer id) {
		return roleDAO.findRoleByPrimaryKey(id);
	}

	public String findRoleJson(String name) {
		// TODO Auto-generated method stub
		List<Role> list = new java.util.ArrayList<Role>(roleDAO.findRolePaging(name, -1, -1));
        List<RoleDTO> listDTO = new ArrayList<RoleDTO>();
        for(Role role: list)
        {
        	RoleDTO roleDTO = new RoleDTO();
        	BeanUtils.copyProperties(role, roleDTO);
        	listDTO.add(roleDTO);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listDTO);
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	public List<Role> parseRoleJSON(JsonArray jsonArray) {
		// TODO Auto-generated method stub
		List<Role> list = new ArrayList<Role>();
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Role role = new Role();
				role.setId(jsonObject.get("id").getAsInt());
				if(jsonObject.has("name"))
					role.setName(jsonObject.get("name").getAsString());
				if(jsonObject.has("description"))
					role.setDescription(jsonObject.get("description").getAsString());
				list.add(role);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public void deleteRoleByIds(Integer[] ids) {
		// TODO Auto-generated method stub
		for (Integer id: ids) {
			Role role = roleDAO.findRoleById(id);
	        if (role != null) {
	           deleteRole(role);
	        }
	    }
	}
}
