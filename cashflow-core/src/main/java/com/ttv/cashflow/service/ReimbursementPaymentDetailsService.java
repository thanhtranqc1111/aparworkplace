
package com.ttv.cashflow.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;

/**
 * Spring service that handles CRUD requests for ReimbursementPaymentDetails entities
 * 
 */
public interface ReimbursementPaymentDetailsService {

	/**
	* Delete an existing ReimbursementPaymentDetails entity
	* 
	 */
	public void deleteReimbursementPaymentDetails(ReimbursementPaymentDetails reimbursementpaymentdetails);

	/**
	* Save an existing ReimbursementPaymentDetails entity
	* 
	 */
    public ReimbursementPaymentDetails saveReimbursementPaymentDetails(ReimbursementPaymentDetails reimbursementpaymentdetails_1);

	/**
	* Return a count of all ReimbursementPaymentDetails entity
	* 
	 */
	public Integer countReimbursementPaymentDetailss();

	/**
	* Save an existing Reimbursement entity
	* 
	 */
	public ReimbursementPaymentDetails saveReimbursementPaymentDetailsReimbursement(BigInteger id, Reimbursement related_reimbursement);

	/**
	* Delete an existing Reimbursement entity
	* 
	 */
	public ReimbursementPaymentDetails deleteReimbursementPaymentDetailsReimbursement(BigInteger reimbursementpaymentdetails_id, BigInteger related_reimbursement_id);

	/**
	* Delete an existing PaymentOrder entity
	* 
	 */
	public ReimbursementPaymentDetails deleteReimbursementPaymentDetailsPaymentOrder(BigInteger reimbursementpaymentdetails_id_1, BigInteger related_paymentorder_id);

	/**
	 */
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id_1);

	/**
	* Load an existing ReimbursementPaymentDetails entity
	* 
	 */
	public Set<ReimbursementPaymentDetails> loadReimbursementPaymentDetailss();

	/**
	* Return all ReimbursementPaymentDetails entity
	* 
	 */
	public List<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss(Integer startResult, Integer maxRows);

	/**
	* Save an existing PaymentOrder entity
	* 
	 */
	public ReimbursementPaymentDetails saveReimbursementPaymentDetailsPaymentOrder(BigInteger id_2, PaymentOrder related_paymentorder);
	
	/**
	 * get total reimbursement amount original by reim id
	 * @param paymentId
	 * @param reimId
	 * @return
	 */
	public BigDecimal getTotalReimAmountOriginalByReimId(BigInteger paymentId, BigInteger reimId);
}