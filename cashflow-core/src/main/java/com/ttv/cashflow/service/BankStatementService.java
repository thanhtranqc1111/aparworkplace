
package com.ttv.cashflow.service;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.dto.PaymentOrderDTO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for BankStatement entities
 * 
 */
public interface BankStatementService {

	/**
	* Return a count of all BankStatement entity
	* 
	 */
	public Integer countBankStatements();

	/**
	* Return all BankStatement entity
	* 
	 */
	public List<BankStatement> findAllBankStatements(Integer startResult, Integer maxRows);

	/**
	* Load an existing BankStatement entity
	* 
	 */
	public Set<BankStatement> loadBankStatements();

	/**
	* Save an existing BankStatement entity
	* 
	 */
	public void saveBankStatement(BankStatement bankstatement);

	/**
	 */
	public BankStatement findBankStatementByPrimaryKey(BigInteger id);

	/**
	* Delete an existing BankStatement entity
	* 
	 */
	public void deleteBankStatement(BankStatement bankstatement_1);
	/**
	 * @param ids
	 */
	public void deleteBankStatementByIds(BigInteger ids[]);
	/**
	 * @param start
	 * @param end
	 * @param bankName
	 * @param bankAccNo
	 * @param transactionFrom
	 * @param transactionTo
	 * @return
	 */
	public String findBankStatementJson(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, BigInteger transactionNumber, Integer transDateFilter, Integer subsidiaryId);
	/**
	 * @param data
	 * @return
	 */
	public List<BankStatement> parseBankStatementJSON(JsonArray data);
	public int updatePaymentOrderStatus(Set<String> setPaymentOrderNo, Map<String, String> mapPaymentOrderNoRef);
	public int checkExists(BankStatement bankStatement);
	public Calendar getMaxTransactionDate(String bankName, String bankAccNo);
	public int saveListBankStatement(List<BankStatement> listBankStatements);
	public int updateEndingBalance(String bankAccNo, BigDecimal endingBalance);
	public int resetPaymentOrderStatus(BigInteger bankStatementId);
	public boolean validateTransactionDateBeforeEdit(BigInteger bankStatementId);
}