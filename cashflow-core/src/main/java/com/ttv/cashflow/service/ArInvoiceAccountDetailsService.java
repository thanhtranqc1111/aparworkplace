
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;

/**
 * Spring service that handles CRUD requests for ArInvoiceAccountDetails entities
 * 
 */
public interface ArInvoiceAccountDetailsService {

	/**
	* Save an existing ArInvoiceAccountDetails entity
	* 
	 */
	public void saveArInvoiceAccountDetails(ArInvoiceAccountDetails arinvoiceaccountdetails);

	/**
	* Delete an existing ArInvoiceClassDetails entity
	* 
	 */
	public ArInvoiceAccountDetails deleteArInvoiceAccountDetailsArInvoiceClassDetailses(BigInteger arinvoiceaccountdetails_id, BigInteger related_arinvoiceclassdetailses_id);

	/**
	* Load an existing ArInvoiceAccountDetails entity
	* 
	 */
	public Set<ArInvoiceAccountDetails> loadArInvoiceAccountDetailss();

	/**
	* Return a count of all ArInvoiceAccountDetails entity
	* 
	 */
	public Integer countArInvoiceAccountDetailss();

	/**
	* Save an existing ArInvoiceClassDetails entity
	* 
	 */
	public ArInvoiceAccountDetails saveArInvoiceAccountDetailsArInvoiceClassDetailses(BigInteger id, ArInvoiceClassDetails related_arinvoiceclassdetailses);

	/**
	* Save an existing ArInvoice entity
	* 
	 */
	public ArInvoiceAccountDetails saveArInvoiceAccountDetailsArInvoice(BigInteger id_1, ArInvoice related_arinvoice);

	/**
	* Delete an existing ArInvoiceAccountDetails entity
	* 
	 */
	public void deleteArInvoiceAccountDetails(ArInvoiceAccountDetails arinvoiceaccountdetails_1);

	/**
	 */
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id_2);

	/**
	* Delete an existing ArInvoice entity
	* 
	 */
	public ArInvoiceAccountDetails deleteArInvoiceAccountDetailsArInvoice(BigInteger arinvoiceaccountdetails_id_1, BigInteger related_arinvoice_id);

	/**
	* Return all ArInvoiceAccountDetails entity
	* 
	 */
	public List<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss(Integer startResult, Integer maxRows);
	
	
	public BigInteger getLastId();
	
}