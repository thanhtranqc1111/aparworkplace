package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentDetailsDAO;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.service.ReimbursementPaymentDetailsService;

/**
 * Spring service that handles CRUD requests for ReimbursementPaymentDetails entities
 * 
 */

@Service("ReimbursementPaymentDetailsService")

@Transactional
public class ReimbursementPaymentDetailsServiceImpl implements ReimbursementPaymentDetailsService {

	/**
	 * DAO injected by Spring that manages PaymentOrder entities
	 * 
	 */
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;

	/**
	 * DAO injected by Spring that manages Reimbursement entities
	 * 
	 */
	@Autowired
	private ReimbursementDAO reimbursementDAO;

	/**
	 * DAO injected by Spring that manages ReimbursementPaymentDetails entities
	 * 
	 */
	@Autowired
	private ReimbursementPaymentDetailsDAO reimbursementPaymentDetailsDAO;

	/**
	 * Instantiates a new ReimbursementPaymentDetailsServiceImpl.
	 *
	 */
	public ReimbursementPaymentDetailsServiceImpl() {
	}

	/**
	 * Delete an existing ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public void deleteReimbursementPaymentDetails(ReimbursementPaymentDetails reimbursementpaymentdetails) {
		reimbursementPaymentDetailsDAO.remove(reimbursementpaymentdetails);
		reimbursementPaymentDetailsDAO.flush();
	}

	/**
	 * Save an existing ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
    public ReimbursementPaymentDetails saveReimbursementPaymentDetails(ReimbursementPaymentDetails reimbursementpaymentdetails) {
        ReimbursementPaymentDetails existingReimbursementPaymentDetails = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(reimbursementpaymentdetails.getId());
        ReimbursementPaymentDetails resultReimbursementPaymentDetails = null;

        if (existingReimbursementPaymentDetails != null) {
            existingReimbursementPaymentDetails.setPaymentOriginalAmount(reimbursementpaymentdetails.getPaymentOriginalAmount());
            existingReimbursementPaymentDetails.setPaymentRate(reimbursementpaymentdetails.getPaymentRate());
            existingReimbursementPaymentDetails.setPaymentConvertedAmount(reimbursementpaymentdetails.getPaymentConvertedAmount());
            existingReimbursementPaymentDetails.setVarianceAmount(reimbursementpaymentdetails.getVarianceAmount());
            existingReimbursementPaymentDetails.setVarianceAccountCode(reimbursementpaymentdetails.getVarianceAccountCode());
            existingReimbursementPaymentDetails.setCreatedDate(reimbursementpaymentdetails.getCreatedDate());
            existingReimbursementPaymentDetails.setModifiedDate(reimbursementpaymentdetails.getModifiedDate());
            resultReimbursementPaymentDetails = reimbursementPaymentDetailsDAO.store(existingReimbursementPaymentDetails);
        } else {
            resultReimbursementPaymentDetails = reimbursementPaymentDetailsDAO.store(reimbursementpaymentdetails);
        }
        reimbursementPaymentDetailsDAO.flush();
        return resultReimbursementPaymentDetails;
    }

	/**
	 * Return a count of all ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public Integer countReimbursementPaymentDetailss() {
		return ((Long) reimbursementPaymentDetailsDAO.createQuerySingleResult("select count(o) from ReimbursementPaymentDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public ReimbursementPaymentDetails saveReimbursementPaymentDetailsReimbursement(BigInteger id, Reimbursement related_reimbursement) {
		ReimbursementPaymentDetails reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(id, -1, -1);
		Reimbursement existingreimbursement = reimbursementDAO.findReimbursementByPrimaryKey(related_reimbursement.getId());

		// copy into the existing record to preserve existing relationships
		if (existingreimbursement != null) {
			existingreimbursement.setId(related_reimbursement.getId());
			existingreimbursement.setEmployeeCode(related_reimbursement.getEmployeeCode());
			existingreimbursement.setEmployeeName(related_reimbursement.getEmployeeName());
			existingreimbursement.setReimbursementNo(related_reimbursement.getReimbursementNo());
			existingreimbursement.setOriginalCurrencyCode(related_reimbursement.getOriginalCurrencyCode());
			existingreimbursement.setFxRate(related_reimbursement.getFxRate());
			existingreimbursement.setBookingDate(related_reimbursement.getBookingDate());
			existingreimbursement.setMonth(related_reimbursement.getMonth());
			existingreimbursement.setClaimType(related_reimbursement.getClaimType());
			existingreimbursement.setAccountPayableCode(related_reimbursement.getAccountPayableCode());
			existingreimbursement.setAccountPayableName(related_reimbursement.getAccountPayableName());
			existingreimbursement.setDescription(related_reimbursement.getDescription());
			existingreimbursement.setIncludeGstTotalConvertedAmount(related_reimbursement.getIncludeGstTotalConvertedAmount());
			existingreimbursement.setScheduledPaymentDate(related_reimbursement.getScheduledPaymentDate());
			existingreimbursement.setScheduledPaymentDate(related_reimbursement.getScheduledPaymentDate());
			existingreimbursement.setStatus(related_reimbursement.getStatus());
			existingreimbursement.setCreatedDate(related_reimbursement.getCreatedDate());
			existingreimbursement.setModifiedDate(related_reimbursement.getModifiedDate());
			related_reimbursement = existingreimbursement;
		} else {
			related_reimbursement = reimbursementDAO.store(related_reimbursement);
			reimbursementDAO.flush();
		}

		reimbursementpaymentdetails.setReimbursement(related_reimbursement);
		related_reimbursement.getReimbursementPaymentDetailses().add(reimbursementpaymentdetails);
		reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.store(reimbursementpaymentdetails);
		reimbursementPaymentDetailsDAO.flush();

		related_reimbursement = reimbursementDAO.store(related_reimbursement);
		reimbursementDAO.flush();

		return reimbursementpaymentdetails;
	}

	/**
	 * Delete an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public ReimbursementPaymentDetails deleteReimbursementPaymentDetailsReimbursement(BigInteger reimbursementpaymentdetails_id, BigInteger related_reimbursement_id) {
		ReimbursementPaymentDetails reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(reimbursementpaymentdetails_id, -1, -1);
		Reimbursement related_reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(related_reimbursement_id, -1, -1);

		reimbursementpaymentdetails.setReimbursement(null);
		related_reimbursement.getReimbursementPaymentDetailses().remove(reimbursementpaymentdetails);
		reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.store(reimbursementpaymentdetails);
		reimbursementPaymentDetailsDAO.flush();

		related_reimbursement = reimbursementDAO.store(related_reimbursement);
		reimbursementDAO.flush();

		reimbursementDAO.remove(related_reimbursement);
		reimbursementDAO.flush();

		return reimbursementpaymentdetails;
	}

	/**
	 * Delete an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public ReimbursementPaymentDetails deleteReimbursementPaymentDetailsPaymentOrder(BigInteger reimbursementpaymentdetails_id, BigInteger related_paymentorder_id) {
		ReimbursementPaymentDetails reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(reimbursementpaymentdetails_id, -1, -1);
		PaymentOrder related_paymentorder = paymentOrderDAO.findPaymentOrderByPrimaryKey(related_paymentorder_id, -1, -1);

		reimbursementpaymentdetails.setPaymentOrder(null);
		related_paymentorder.getReimbursementPaymentDetailses().remove(reimbursementpaymentdetails);
		reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.store(reimbursementpaymentdetails);
		reimbursementPaymentDetailsDAO.flush();

		related_paymentorder = paymentOrderDAO.store(related_paymentorder);
		paymentOrderDAO.flush();

		paymentOrderDAO.remove(related_paymentorder);
		paymentOrderDAO.flush();

		return reimbursementpaymentdetails;
	}

	/**
	 */
	@Transactional
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id) {
		return reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(id);
	}

	/**
	 * Load an existing ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> loadReimbursementPaymentDetailss() {
		return reimbursementPaymentDetailsDAO.findAllReimbursementPaymentDetailss();
	}

	/**
	 * Return all ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public List<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ReimbursementPaymentDetails>(reimbursementPaymentDetailsDAO.findAllReimbursementPaymentDetailss(startResult, maxRows));
	}

	/**
	 * Save an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public ReimbursementPaymentDetails saveReimbursementPaymentDetailsPaymentOrder(BigInteger id, PaymentOrder related_paymentorder) {
		ReimbursementPaymentDetails reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(id, -1, -1);
		PaymentOrder existingpaymentOrder = paymentOrderDAO.findPaymentOrderByPrimaryKey(related_paymentorder.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpaymentOrder != null) {
			existingpaymentOrder.setId(related_paymentorder.getId());
			existingpaymentOrder.setPaymentOrderNo(related_paymentorder.getPaymentOrderNo());
			existingpaymentOrder.setBankName(related_paymentorder.getBankName());
			existingpaymentOrder.setBankAccount(related_paymentorder.getBankAccount());
			existingpaymentOrder.setOriginalCurrencyCode(related_paymentorder.getOriginalCurrencyCode());
			existingpaymentOrder.setPaymentRate(related_paymentorder.getPaymentRate());
			existingpaymentOrder.setIncludeGstOriginalAmount(related_paymentorder.getIncludeGstOriginalAmount());
			existingpaymentOrder.setIncludeGstConvertedAmount(related_paymentorder.getIncludeGstConvertedAmount());
			existingpaymentOrder.setValueDate(related_paymentorder.getValueDate());
			existingpaymentOrder.setBankRefNo(related_paymentorder.getBankRefNo());
			existingpaymentOrder.setDescription(related_paymentorder.getDescription());
			existingpaymentOrder.setPaymentApprovalNo(related_paymentorder.getPaymentApprovalNo());
			existingpaymentOrder.setAccountPayableCode(related_paymentorder.getAccountPayableCode());
			existingpaymentOrder.setAccountPayableName(related_paymentorder.getAccountPayableName());
			existingpaymentOrder.setStatus(related_paymentorder.getStatus());
			existingpaymentOrder.setPaymentType(related_paymentorder.getPaymentType());
			existingpaymentOrder.setCreatedDate(related_paymentorder.getCreatedDate());
			existingpaymentOrder.setModifiedDate(related_paymentorder.getModifiedDate());
			related_paymentorder = existingpaymentOrder;
		} else {
			related_paymentorder = paymentOrderDAO.store(related_paymentorder);
			paymentOrderDAO.flush();
		}

		reimbursementpaymentdetails.setPaymentOrder(related_paymentorder);
		related_paymentorder.getReimbursementPaymentDetailses().add(reimbursementpaymentdetails);
		reimbursementpaymentdetails = reimbursementPaymentDetailsDAO.store(reimbursementpaymentdetails);
		reimbursementPaymentDetailsDAO.flush();

		related_paymentorder = paymentOrderDAO.store(related_paymentorder);
		paymentOrderDAO.flush();

		return reimbursementpaymentdetails;
	}
	
	public BigDecimal getTotalReimAmountOriginalByReimId(BigInteger paymentId, BigInteger reimId) {
		return reimbursementPaymentDetailsDAO.getTotalReimAmountOriginalByReimId(paymentId, reimId);
	}
}
