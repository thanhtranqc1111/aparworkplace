
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.SystemValue;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for SystemValue entities
 * 
 */
public interface SystemValueService {

	/**
	* Return a count of all SystemValue entity
	* 
	 */
	public Integer countSystemValues();

	/**
	 */
	public SystemValue findSystemValueByPrimaryKey(Integer id);

	/**
	* Save an existing SystemValue entity
	* 
	 */
	public void saveSystemValue(SystemValue systemvalue);

	/**
	* Load an existing SystemValue entity
	* 
	 */
	public Set<SystemValue> loadSystemValues();

	/**
	* Return all SystemValue entity
	* 
	 */
	public List<SystemValue> findAllSystemValues(Integer startResult, Integer maxRows);

	/**
	* Delete an existing SystemValue entity
	* 
	 */
	public void deleteSystemValue(SystemValue systemvalue_1);
	
	public Map<String, String> findSystemValueAsMapByType(String type);
}