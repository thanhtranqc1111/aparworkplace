
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.dto.ArInvoiceDTO;

/**
 * Spring service that handles CRUD requests for ArInvoice entities
 * 
 */
public interface ArInvoiceService {

	/**
	* Return a count of all ArInvoice entity
	* 
	 */
	public Integer countArInvoices();

	/**
	* Load an existing ArInvoice entity
	* 
	 */
	public Set<ArInvoice> loadArInvoices();

	/**
	* Save an existing ArInvoiceAccountDetails entity
	* 
	 */
	public ArInvoice saveArInvoiceArInvoiceAccountDetailses(BigInteger id, ArInvoiceAccountDetails related_arinvoiceaccountdetailses);

	/**
	* Save an existing ArInvoice entity
	* 
	 */
	public void saveArInvoice(ArInvoice arinvoice);

	/**
	 */
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id_2);

	/**
	* Delete an existing ArInvoice entity
	* 
	 */
	public void deleteArInvoice(ArInvoice arinvoice_1);

	/**
	* Return all ArInvoice entity
	* 
	 */
	public List<ArInvoice> findAllArInvoices(Integer startResult, Integer maxRows);

	/**
	* Delete an existing ArInvoiceAccountDetails entity
	* 
	 */
	public ArInvoice deleteArInvoiceArInvoiceAccountDetailses(BigInteger arinvoice_id_2, BigInteger related_arinvoiceaccountdetailses_id);
	
	public String findARInvoiceJSON(Integer startpage, Integer endPage, String calendarMonthFrom, String calendarMonthTo,
			String fromArNo, String toArNo, String payerName, String invoiceNo, String status, Integer orderColumn, String orderBy);
	
	public BigInteger countSearch(String fromMonth, String toMonth, String invoice, String payerName, String status, String fromArNo, String toArNo);
	
	public List<String> searchArInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
            String invoiceNo, String payerName, String status,  String fromArNo, String toArNo, Integer orderColumn, String orderBy);
	 
	public List<ArInvoiceDTO> searchARInvoiceDetail(List<String> arInvoiceNoLst, Integer orderColumn, String orderBy);
	
	public List<ARInvoiceDetail> getARInvoiceDetail(BigInteger apInvoiceId);
	
	public BigInteger getLastId();
}