
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollDetails;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for PayrollDetails entities
 * 
 */
public interface PayrollDetailsService {

	/**
	 */
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id);

	/**
	* Load an existing PayrollDetails entity
	* 
	 */
	public Set<PayrollDetails> loadPayrollDetailss();

	/**
	* Delete an existing PayrollDetails entity
	* 
	 */
	public void deletePayrollDetails(PayrollDetails payrolldetails);

	/**
	* Save an existing PayrollDetails entity
	* 
	 */
	public void savePayrollDetails(PayrollDetails payrolldetails_1);

	/**
	* Return all PayrollDetails entity
	* 
	 */
	public List<PayrollDetails> findAllPayrollDetailss(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Payroll entity
	* 
	 */
	public PayrollDetails deletePayrollDetailsPayroll(BigInteger payrolldetails_id, BigInteger related_payroll_id);

	/**
	* Return a count of all PayrollDetails entity
	* 
	 */
	public Integer countPayrollDetailss();

	/**
	* Save an existing Payroll entity
	* 
	 */
	public PayrollDetails savePayrollDetailsPayroll(BigInteger id_1, Payroll related_payroll);
}