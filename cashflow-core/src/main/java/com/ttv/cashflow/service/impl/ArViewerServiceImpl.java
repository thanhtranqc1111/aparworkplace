package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ArViewerDAO;

import com.ttv.cashflow.domain.ArViewer;
import com.ttv.cashflow.service.ArViewerService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ArViewer entities
 * 
 */

@Service("ArViewerService")

@Transactional
public class ArViewerServiceImpl implements ArViewerService {

	/**
	 * DAO injected by Spring that manages ArViewer entities
	 * 
	 */
	@Autowired
	private ArViewerDAO ArViewerDAO;

	/**
	 * Instantiates a new ArViewerServiceImpl.
	 *
	 */
	public ArViewerServiceImpl() {
	}

	/**
	 * Load an existing ArViewer entity
	 * 
	 */
	@Transactional
	public Set<ArViewer> loadArViewers() {
		return ArViewerDAO.findAllArViewers();
	}

	/**
	 * Delete an existing ArViewer entity
	 * 
	 */
	@Transactional
	public void deleteArViewer(ArViewer arViewer) {
		ArViewerDAO.remove(arViewer);
		ArViewerDAO.flush();
	}

	/**
	 * Save an existing ArViewer entity
	 * 
	 */
	@Transactional
	public void saveArViewer(ArViewer arViewer) {
		ArViewer existingArViewer = ArViewerDAO.findArViewerByPrimaryKey(arViewer.getId());

		if (existingArViewer != null) {
			if (existingArViewer != arViewer) {
				existingArViewer.setId(arViewer.getId());
				existingArViewer.setArInvoiceNo(arViewer.getArInvoiceNo());
				existingArViewer.setMonth(arViewer.getMonth());
				existingArViewer.setPayerName(arViewer.getPayerName());
				existingArViewer.setInvoiceNo(arViewer.getInvoiceNo());
				existingArViewer.setApprovalCode(arViewer.getApprovalCode());
				existingArViewer.setOriginalCurrencyCode(arViewer.getOriginalCurrencyCode());
				existingArViewer.setExcludeGstOriginalAmount(arViewer.getExcludeGstOriginalAmount());
				existingArViewer.setGstType(arViewer.getGstType());
				existingArViewer.setGstRate(arViewer.getGstRate());
				existingArViewer.setIncludeGstOriginalAmount(arViewer.getIncludeGstOriginalAmount());
				existingArViewer.setFxRate(arViewer.getFxRate());
				existingArViewer.setIncludeGstConvertedAmount(arViewer.getIncludeGstConvertedAmount());
				existingArViewer.setAccountName(arViewer.getAccountName());
				existingArViewer.setClassName(arViewer.getClassName());
				existingArViewer.setDescription(arViewer.getDescription());
				existingArViewer.setReceiptOrderId(arViewer.getReceiptOrderId());
				existingArViewer.setTransactionId(arViewer.getTransactionId());
				existingArViewer.setTransactionDate(arViewer.getTransactionDate());
				existingArViewer.setSettedIncludeGstOriginalAmount(arViewer.getSettedIncludeGstOriginalAmount());
				existingArViewer.setReceiptRate(arViewer.getReceiptRate());
				existingArViewer.setSettedIncludeGstConvertedAmount(arViewer.getSettedIncludeGstConvertedAmount());
				existingArViewer.setVarianceAmount(arViewer.getVarianceAmount());
				existingArViewer.setRemainIncludeGstOriginalAmount(arViewer.getRemainIncludeGstOriginalAmount());
			}
			arViewer = ArViewerDAO.store(existingArViewer);
		} else {
			arViewer = ArViewerDAO.store(arViewer);
		}
		ArViewerDAO.flush();
	}

	/**
	 * Return a count of all ArViewer entity
	 * 
	 */
	@Transactional
	public Integer countArViewers() {
		return ((Long) ArViewerDAO.createQuerySingleResult("select count(o) from ArViewer o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public ArViewer findArViewerByPrimaryKey(BigInteger id) {
		return ArViewerDAO.findArViewerByPrimaryKey(id);
	}

	/**
	 * Return all ArViewer entity
	 * 
	 */
	@Transactional
	public List<ArViewer> findAllArViewers(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArViewer>(ArViewerDAO.findAllArViewers(startResult, maxRows));
	}
}
