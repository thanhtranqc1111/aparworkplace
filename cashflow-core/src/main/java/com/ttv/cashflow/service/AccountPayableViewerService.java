
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.AccountPayableViewer;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for AccountPayableViewer entities
 * 
 */
public interface AccountPayableViewerService {

	/**
	* Return a count of all AccountPayableViewer entity
	* 
	 */
	public Integer countAccountPayableViewers();

	/**
	* Load an existing AccountPayableViewer entity
	* 
	 */
	public Set<AccountPayableViewer> loadAccountPayableViewers();

	/**
	* Return all AccountPayableViewer entity
	* 
	 */
	public List<AccountPayableViewer> findAllAccountPayableViewers(Integer startResult, Integer maxRows);

	/**
	 */
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id);

	/**
	* Save an existing AccountPayableViewer entity
	* 
	 */
	public void saveAccountPayableViewer(AccountPayableViewer accountpayableviewer);

	/**
	* Delete an existing AccountPayableViewer entity
	* 
	 */
	public void deleteAccountPayableViewer(AccountPayableViewer accountpayableviewer_1);
}