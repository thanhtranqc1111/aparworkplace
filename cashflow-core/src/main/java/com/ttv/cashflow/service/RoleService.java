
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.RolePermission;
import com.ttv.cashflow.domain.UserAccount;

/**
 * Spring service that handles CRUD requests for Role entities
 * 
 */
public interface RoleService {

	/**
	* Delete an existing Role entity
	* 
	 */
	public void deleteRole(Role role);

	/**
	* Load an existing Role entity
	* 
	 */
	public Set<Role> loadRoles();

	/**
	* Delete an existing RolePermission entity
	* 
	 */
	public Role deleteRoleRolePermissions(Integer role_id, Integer related_rolepermissions_id);

	/**
	* Return a count of all Role entity
	* 
	 */
	public Integer countRoles();

	/**
	* Return all Role entity
	* 
	 */
	public List<Role> findAllRoles(Integer startResult, Integer maxRows);

	/**
	* Save an existing Role entity
	* 
	 */
	public void saveRole(Role role_1);

	/**
	* Save an existing UserAccount entity
	* 
	 */
	public Role saveRoleUserAccounts(Integer id, UserAccount related_useraccounts);

	/**
	* Delete an existing UserAccount entity
	* 
	 */
	public Role deleteRoleUserAccounts(Integer role_id_1, Integer related_useraccounts_id);

	/**
	* Save an existing RolePermission entity
	* 
	 */
	public Role saveRoleRolePermissions(Integer id_1, RolePermission related_rolepermissions);

	/**
	 */
	public Role findRoleByPrimaryKey(Integer id_2);
	
	/**
	 */
	public String findRoleJson(String keyword);
	public List<Role> parseRoleJSON(JsonArray jsonArray);
	public void deleteRoleByIds(Integer ids[]);
}