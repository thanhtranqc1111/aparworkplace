
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.InvoicePaymentOrderTemp;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for InvoicePaymentOrderTemp entities
 * 
 */
public interface InvoicePaymentOrderTempService {

	/**
	* Return all InvoicePaymentOrderTemp entity
	* 
	 */
	public List<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps(Integer startResult, Integer maxRows);

	/**
	* Return a count of all InvoicePaymentOrderTemp entity
	* 
	 */
	public Integer countInvoicePaymentOrderTemps();

	/**
	 */
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id);

	/**
	* Delete an existing InvoicePaymentOrderTemp entity
	* 
	 */
	public void deleteInvoicePaymentOrderTemp(InvoicePaymentOrderTemp invoicepaymentordertemp);

	/**
	* Load an existing InvoicePaymentOrderTemp entity
	* 
	 */
	public Set<InvoicePaymentOrderTemp> loadInvoicePaymentOrderTemps();

	/**
	* Save an existing InvoicePaymentOrderTemp entity
	* 
	 */
	public void saveInvoicePaymentOrderTemp(InvoicePaymentOrderTemp invoicepaymentordertemp_1);
}