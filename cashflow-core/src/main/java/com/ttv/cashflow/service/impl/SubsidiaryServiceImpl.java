package com.ttv.cashflow.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.dao.SubsidiaryUserDetailDAO;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.dto.SubsidiaryDTO;
import com.ttv.cashflow.service.SubsidiaryService;

/**
 * Spring service that handles CRUD requests for Subsidiary entities
 * 
 */

@Service("SubsidiaryService")

@Transactional
public class SubsidiaryServiceImpl implements SubsidiaryService {

	/**
	 * DAO injected by Spring that manages Subsidiary entities
	 * 
	 */
	@Autowired
	private SubsidiaryDAO subsidiaryDAO;

	/**
	 * DAO injected by Spring that manages SubsidiaryUserDetail entities
	 * 
	 */
	@Autowired
	private SubsidiaryUserDetailDAO subsidiaryUserDetailDAO;

	/**
	 * Instantiates a new SubsidiaryServiceImpl.
	 *
	 */
	public SubsidiaryServiceImpl() {
	}

	/**
	 * Return all Subsidiary entity
	 * 
	 */
	@Transactional
	public List<Subsidiary> findAllSubsidiarys(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Subsidiary>(subsidiaryDAO.findAllSubsidiarys(startResult, maxRows));
	}

	/**
	 * Return a count of all Subsidiary entity
	 * 
	 */
	@Transactional
	public Integer countSubsidiarys() {
		return ((Long) subsidiaryDAO.createQuerySingleResult("select count(o) from Subsidiary o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public void saveSubsidiary(Subsidiary subsidiary) {
		Subsidiary existingSubsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(subsidiary.getId());
		if (existingSubsidiary != null) {
			if (existingSubsidiary != subsidiary) {
				existingSubsidiary.setId(subsidiary.getId());
				//do not update subsidiary code
				// existingSubsidiary.setCode(existingSubsidiary.getCode());
				existingSubsidiary.setName(subsidiary.getName());
				existingSubsidiary.setAddress(subsidiary.getAddress());
				existingSubsidiary.setPhone(subsidiary.getPhone());
				existingSubsidiary.setWebsite(subsidiary.getWebsite());
				existingSubsidiary.setDescription(subsidiary.getDescription());
				existingSubsidiary.setCurrency(subsidiary.getCurrency());
				existingSubsidiary.setIsActive(subsidiary.getIsActive());
				// existingSubsidiary.setCreatedDate(subsidiary.getCreatedDate());
				existingSubsidiary.setModifiedDate(subsidiary.getModifiedDate());
			}
			subsidiary = subsidiaryDAO.store(existingSubsidiary);
		} else {
			subsidiary = subsidiaryDAO.store(subsidiary);
		}
		
		subsidiaryDAO.flush();
	}

	/**
	 */
	@Transactional
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id) {
		return subsidiaryDAO.findSubsidiaryByPrimaryKey(id);
	}

	/**
	 * Load an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public Set<Subsidiary> loadSubsidiarys() {
		return subsidiaryDAO.findAllSubsidiarys();
	}

	/**
	 * Delete an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public void deleteSubsidiary(Subsidiary subsidiary) {
		subsidiaryDAO.remove(subsidiary);
		subsidiaryDAO.flush();
	}

	/**
	 * Save an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public Subsidiary saveSubsidiarySubsidiaryUserDetails(Integer id, SubsidiaryUserDetail related_subsidiaryuserdetails) {
		Subsidiary subsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(id, -1, -1);
		SubsidiaryUserDetail existingsubsidiaryUserDetails = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(related_subsidiaryuserdetails.getId());

		// copy into the existing record to preserve existing relationships
		if (existingsubsidiaryUserDetails != null) {
			existingsubsidiaryUserDetails.setId(related_subsidiaryuserdetails.getId());
			existingsubsidiaryUserDetails.setCreatedDate(related_subsidiaryuserdetails.getCreatedDate());
			existingsubsidiaryUserDetails.setModifiedDate(related_subsidiaryuserdetails.getModifiedDate());
			related_subsidiaryuserdetails = existingsubsidiaryUserDetails;
		}

		related_subsidiaryuserdetails.setSubsidiary(subsidiary);
		subsidiary.getSubsidiaryUserDetails().add(related_subsidiaryuserdetails);
		related_subsidiaryuserdetails = subsidiaryUserDetailDAO.store(related_subsidiaryuserdetails);
		subsidiaryUserDetailDAO.flush();

		subsidiary = subsidiaryDAO.store(subsidiary);
		subsidiaryDAO.flush();

		return subsidiary;
	}

	/**
	 * Delete an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public Subsidiary deleteSubsidiarySubsidiaryUserDetails(Integer subsidiary_id, Integer related_subsidiaryuserdetails_id) {
		SubsidiaryUserDetail related_subsidiaryuserdetails = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(related_subsidiaryuserdetails_id, -1, -1);

		Subsidiary subsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(subsidiary_id, -1, -1);

		related_subsidiaryuserdetails.setSubsidiary(null);
		subsidiary.getSubsidiaryUserDetails().remove(related_subsidiaryuserdetails);

		subsidiaryUserDetailDAO.remove(related_subsidiaryuserdetails);
		subsidiaryUserDetailDAO.flush();

		return subsidiary;
	}
	
	/**
	 * find subsidiary list return json list subsidiary dto object
	 */
	public String findSubsidiaryJson(String keywork) {
		List<Subsidiary> list = new java.util.ArrayList<Subsidiary>(
				subsidiaryDAO.findSubsidiaryByNameOrCode(keywork));
		List<SubsidiaryDTO> listDTO = new ArrayList<SubsidiaryDTO>();
		for (Subsidiary subsidiary : list) {
			SubsidiaryDTO subsidiaryDTO = new SubsidiaryDTO(subsidiary.getId(), subsidiary.getCode(),
					subsidiary.getName(), subsidiary.getAddress(), subsidiary.getIsActive());
			// BeanUtils.copyProperties(subsidiary, subsidiaryDTO);
			listDTO.add(subsidiaryDTO);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", listDTO);
		Gson gson = new Gson();
		return gson.toJson(map);
	}
	
	public void deleteSubsidiaryByIds(Integer[] ids) {
		for (Integer id: ids) {
			Subsidiary subsidiary = subsidiaryDAO.findSubsidiaryById(id);
	        if (subsidiary != null) {
	        	deleteSubsidiary(subsidiary);
	        }
	    }
	}

    @Override
    public List<Subsidiary> findListSubsidiaryByUserId(Integer userId) {
        return subsidiaryDAO.findListSubsidiaryByUserId(userId);
    }
}
