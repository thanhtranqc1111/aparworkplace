
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ProjectMaster;

/**
 * Spring service that handles CRUD requests for ProjectMaster entities
 * 
 */
public interface ProjectMasterService {

    /**
     * Load an existing ProjectMaster entity
     * 
     */
    public Set<ProjectMaster> loadProjectMasters();

    /**
     */
    public ProjectMaster findProjectMasterByPrimaryKey(String code);

    /**
     * Save an existing ProjectMaster entity
     * 
     */
    public void saveProjectMaster(ProjectMaster projectmaster);

    /**
     * Delete an existing ProjectMaster entity
     * 
     */
    public void deleteProjectMaster(ProjectMaster projectmaster_1);

    /**
     * Return a count of all ProjectMaster entity
     * 
     */
    public Integer countProjectMasters();

    /**
     * Return all ProjectMaster entity
     * 
     */
    public List<ProjectMaster> findAllProjectMasters(Integer startResult, Integer maxRows);

    /**
     * Find project master by code name containing.
     *
     * @param codeName
     *            the code name
     * @return the sets the
     */
    public Set<ProjectMaster> findProjectMasterByCodeNameContaining(String codeName);

    public String findProjectMasterJson(Integer start, Integer end,
            							Integer orderColumn, String orderBy, String keyword);

    /**
     * Delete list existing PayeePayerTypeMaster entity by codes
     * 
     */
    public void deleteProjectMasterByCodes(String codes[]);

    boolean updateProjectMaster(ProjectMaster projectMaster);

    boolean createProjectMaster(ProjectMaster projectMaster);

    boolean checkExistingProjectMasterCode(String code);
}