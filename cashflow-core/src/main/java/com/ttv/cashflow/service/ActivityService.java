
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.Activity;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Activity entities
 * 
 */
public interface ActivityService {

	/**
	* Load an existing Activity entity
	* 
	 */
	public Set<Activity> loadActivitys();

	/**
	* Return a count of all Activity entity
	* 
	 */
	public Integer countActivitys();

	/**
	* Delete an existing Activity entity
	* 
	 */
	public void deleteActivity(Activity activity);

	/**
	* Return all Activity entity
	* 
	 */
	public List<Activity> findAllActivitys(Integer startResult, Integer maxRows);

	/**
	 */
	public Activity findActivityByPrimaryKey(Integer id);

	/**
	* Save an existing Activity entity
	* 
	 */
	public void saveActivity(Activity activity_1);
}