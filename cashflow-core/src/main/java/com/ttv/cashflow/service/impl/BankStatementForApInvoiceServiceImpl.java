package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.service.BankStatementUtilService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for BankStatement entities
 * 
 */

@Service("BankStatementForApInvoiceServiceImpl")
@Transactional
public class BankStatementForApInvoiceServiceImpl implements BankStatementUtilService {
	@Autowired
	private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;
	
	@Autowired
	private ApInvoiceDAO apInvoiceDAO;
	
	@Autowired
	private ApInvoicePaymentStatusDAO apInvoicePaymentStatusDAO;
	
	@Override
	public int updatePaymentStatus(PaymentOrder paymentOrder) {
		try {
			List<ApInvoicePaymentDetails> listApInvoicePaymentDetails = apInvoicePaymentDetailsDAO.findApInvPayDetailIdByPaymentId(paymentOrder.getId());
			listApInvoicePaymentDetails.forEach(apInvoicePaymentDetails -> {
				//update payment status
				ApInvoice apInvoice = apInvoicePaymentDetails.getApInvoice();
				BigDecimal totalAmount = apInvoice.getIncludeGstOriginalAmount();
				BigDecimal totalPaid = apInvoicePaymentDetailsDAO.getTotalPaymentOrderUnconvertedAmountByApInvoiceId(apInvoice.getId());
				if(totalPaid.doubleValue() > 0) {
					if(totalPaid.compareTo(totalAmount) >= 0)
					{
						apInvoice.setStatus(Constant.API_PAID);//PAID
					}
					else 
					{
						apInvoice.setStatus(Constant.API_PARTIAL_PAID);//PARTIAL PAID
					}
					apInvoiceDAO.store(apInvoice);
					
					//update ap invoice paid status
					ApInvoicePaymentStatus apInvoicePaymentStatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByApInvoiceId(apInvoice.getId());
					if(null == apInvoicePaymentStatus.getId())
					{
						apInvoicePaymentStatus = new ApInvoicePaymentStatus();
						apInvoicePaymentStatus.setApInvoiceId(apInvoice.getId());
					}
					apInvoicePaymentStatus.setPaidIncludeGstOriginalAmount(totalPaid);
					apInvoicePaymentStatus.setUnpaidIncludeGstOriginalAmount(totalAmount.subtract(totalPaid));
					apInvoicePaymentStatusDAO.store(apInvoicePaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int resetPaymentOrderStatus(PaymentOrder paymentOrder) {
		try {
			List<ApInvoicePaymentDetails> listApInvoicePaymentDetails = apInvoicePaymentDetailsDAO.findApInvPayDetailIdByPaymentId(paymentOrder.getId());
			listApInvoicePaymentDetails.forEach(apInvoicePaymentDetails -> {
				//reset AP status
				ApInvoice apInvoice = apInvoicePaymentDetails.getApInvoice();
				BigDecimal totalAmount = apInvoice.getIncludeGstOriginalAmount();
				BigDecimal totalPaid = apInvoicePaymentDetailsDAO.getTotalPaymentOrderUnconvertedAmountByApInvoiceId(apInvoice.getId());
				if(totalPaid.compareTo(totalAmount) >= 0)
				{
					apInvoice.setStatus(Constant.API_PAID);
				}
				else 
				{
					if(totalPaid.compareTo(BigDecimal.ZERO) > 0){
						apInvoice.setStatus(Constant.API_PARTIAL_PAID);
					}
					else {
						apInvoice.setStatus(Constant.API_NOT_PAID);
					}
				}
				apInvoiceDAO.store(apInvoice);
				
				/*
				 * reset AP invoice paid status
				 */
				ApInvoicePaymentStatus apInvoicePaymentStatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByApInvoiceId(apInvoice.getId());
				if(apInvoicePaymentStatus != null && apInvoicePaymentStatus.getId() != null)
				{
					apInvoicePaymentStatus.setPaidIncludeGstOriginalAmount(totalPaid);
					apInvoicePaymentStatus.setUnpaidIncludeGstOriginalAmount(totalAmount.subtract(totalPaid));
					apInvoicePaymentStatusDAO.store(apInvoicePaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
