package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.dto.InvoicePayment;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.service.ApInvoiceService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for ApInvoice entities
 * 
 */

@Service("ApInvoiceService")

@Transactional
public class ApInvoiceServiceImpl implements ApInvoiceService {

    SimpleDateFormat sdf = new SimpleDateFormat(Constant.DDMMYYYY, Locale.ENGLISH);
	/**
	 * DAO injected by Spring that manages ApInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ApInvoiceAccountDetailsDAO apInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ApInvoice entities
	 * 
	 */
	@Autowired
	private ApInvoiceDAO apInvoiceDAO;

	/**
	 * DAO injected by Spring that manages ApInvoicePaymentDetails entities
	 * 
	 */
	@Autowired
	private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;

	/**
	 * DAO injected by Spring that manages ApInvoicePaymentStatus entities
	 * 
	 */
	@Autowired
	private ApInvoicePaymentStatusDAO apInvoicePaymentStatusDAO;

	/**
	 * DAO injected by Spring that manages Subsidiary entities
	 * 
	 */
	@Autowired
	private SubsidiaryDAO subsidiaryDAO;

	@Autowired
	private BankStatementDAO bankStatementDAO;
	
	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
	
	@Autowired
    private SystemValueDAO systemValueDAO;
	
	/**
	 * Instantiates a new ApInvoiceServiceImpl.
	 *
	 */
	public ApInvoiceServiceImpl() {
	}

	/**
	 * Save an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public void saveApInvoice(ApInvoice apinvoice) {
		ApInvoice existingApInvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(apinvoice.getId());

		if (existingApInvoice != null) {
			if (existingApInvoice != apinvoice) {
				existingApInvoice.setId(apinvoice.getId());
				existingApInvoice.setPayeeCode(apinvoice.getPayeeCode());
				existingApInvoice.setPayeeName(apinvoice.getPayeeName());
				existingApInvoice.setPayeeTypeCode(apinvoice.getPayeeTypeCode());
				existingApInvoice.setPayeeTypeName(apinvoice.getPayeeTypeName());
				existingApInvoice.setApInvoiceNo(apinvoice.getApInvoiceNo());
				existingApInvoice.setInvoiceNo(apinvoice.getInvoiceNo());
				existingApInvoice.setOriginalCurrencyCode(apinvoice.getOriginalCurrencyCode());
				existingApInvoice.setFxRate(apinvoice.getFxRate());
				existingApInvoice.setGstRate(apinvoice.getGstRate());
				existingApInvoice.setDescription(apinvoice.getDescription());
				existingApInvoice.setInvoiceIssuedDate(apinvoice.getInvoiceIssuedDate());
				existingApInvoice.setClaimType(apinvoice.getClaimType());
				existingApInvoice.setGstType(apinvoice.getGstType());
				existingApInvoice.setAccountPayableCode(apinvoice.getAccountPayableCode());
				existingApInvoice.setAccountPayableName(apinvoice.getAccountPayableName());
				existingApInvoice.setExcludeGstOriginalAmount(apinvoice.getExcludeGstOriginalAmount());
				existingApInvoice.setExcludeGstConvertedAmount(apinvoice.getExcludeGstConvertedAmount());
				existingApInvoice.setGstConvertedAmount(apinvoice.getGstConvertedAmount());
				existingApInvoice.setGstOriginalAmount(apinvoice.getGstOriginalAmount());
				existingApInvoice.setIncludeGstOriginalAmount(apinvoice.getIncludeGstOriginalAmount());
				existingApInvoice.setIncludeGstConvertedAmount(apinvoice.getIncludeGstConvertedAmount());
				existingApInvoice.setPaymentTerm(apinvoice.getPaymentTerm());
				existingApInvoice.setScheduledPaymentDate(apinvoice.getScheduledPaymentDate());

				if(StringUtils.isNotEmpty(apinvoice.getStatus())){
				    existingApInvoice.setStatus(apinvoice.getStatus());
				}
				
				existingApInvoice.setMonth(apinvoice.getMonth());
				existingApInvoice.setBookingDate(apinvoice.getBookingDate());
			
				existingApInvoice.setImportKey(apinvoice.getImportKey());
				existingApInvoice.setCreatedDate(apinvoice.getCreatedDate());
				existingApInvoice.setModifiedDate(apinvoice.getModifiedDate());
			}
			apinvoice = apInvoiceDAO.store(existingApInvoice);
		} else {
			apinvoice.setCreatedDate(Calendar.getInstance());
		    ApInvoice ret = apInvoiceDAO.store(apinvoice);
		    apinvoice.copy(ret);
		}
		apInvoiceDAO.flush();
	}

	/**
	 * Delete an existing ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public ApInvoice deleteApInvoiceApInvoicePaymentStatuses(BigInteger apinvoice_id, BigInteger related_apinvoicepaymentstatuses_id) {
		ApInvoicePaymentStatus related_apinvoicepaymentstatuses = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByPrimaryKey(related_apinvoicepaymentstatuses_id, -1, -1);

		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(apinvoice_id, -1, -1);

		apInvoicePaymentStatusDAO.remove(related_apinvoicepaymentstatuses);
		apInvoicePaymentStatusDAO.flush();

		return apinvoice;
	}

	/**
	 * Save an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public ApInvoice saveApInvoiceSubsidiary(BigInteger id, Subsidiary related_subsidiary) {
		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(id, -1, -1);
		Subsidiary existingsubsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(related_subsidiary.getId());

		// copy into the existing record to preserve existing relationships
		if (existingsubsidiary != null) {
			existingsubsidiary.setId(related_subsidiary.getId());
			existingsubsidiary.setCode(related_subsidiary.getCode());
			existingsubsidiary.setName(related_subsidiary.getName());
			existingsubsidiary.setDescription(related_subsidiary.getDescription());
			existingsubsidiary.setIsActive(related_subsidiary.getIsActive());
			existingsubsidiary.setAddress(related_subsidiary.getAddress());
			existingsubsidiary.setPhone(related_subsidiary.getPhone());
			existingsubsidiary.setWebsite(related_subsidiary.getWebsite());
			existingsubsidiary.setCreatedDate(related_subsidiary.getCreatedDate());
			existingsubsidiary.setModifiedDate(related_subsidiary.getModifiedDate());
			related_subsidiary = existingsubsidiary;
		} else {
			related_subsidiary = subsidiaryDAO.store(related_subsidiary);
			subsidiaryDAO.flush();
		}

		apinvoice = apInvoiceDAO.store(apinvoice);
		apInvoiceDAO.flush();

		related_subsidiary = subsidiaryDAO.store(related_subsidiary);
		subsidiaryDAO.flush();

		return apinvoice;
	}

	/**
	 * Load an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public Set<ApInvoice> loadApInvoices() {
		return apInvoiceDAO.findAllApInvoices();
	}

	/**
	 * Delete an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public void deleteApInvoice(ApInvoice apinvoice) {
		apInvoiceDAO.remove(apinvoice);
		apInvoiceDAO.flush();
	}

	/**
	 * Delete an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public ApInvoice deleteApInvoiceSubsidiary(BigInteger apinvoice_id, Integer related_subsidiary_id) {
		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(apinvoice_id, -1, -1);
		Subsidiary related_subsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(related_subsidiary_id, -1, -1);

		apinvoice = apInvoiceDAO.store(apinvoice);
		apInvoiceDAO.flush();

		related_subsidiary = subsidiaryDAO.store(related_subsidiary);
		subsidiaryDAO.flush();

		subsidiaryDAO.remove(related_subsidiary);
		subsidiaryDAO.flush();

		return apinvoice;
	}

	/**
	 * Save an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public ApInvoice saveApInvoiceApInvoicePaymentDetailses(BigInteger id, ApInvoicePaymentDetails related_apinvoicepaymentdetailses) {
		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(id, -1, -1);
		ApInvoicePaymentDetails existingapInvoicePaymentDetailses = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(related_apinvoicepaymentdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoicePaymentDetailses != null) {
			existingapInvoicePaymentDetailses.setId(related_apinvoicepaymentdetailses.getId());
			existingapInvoicePaymentDetailses.setPaymentIncludeGstConvertedAmount(related_apinvoicepaymentdetailses.getPaymentIncludeGstConvertedAmount());
			existingapInvoicePaymentDetailses.setPaymentIncludeGstOriginalAmount(related_apinvoicepaymentdetailses.getPaymentIncludeGstOriginalAmount());
			existingapInvoicePaymentDetailses.setPaymentRate(related_apinvoicepaymentdetailses.getPaymentRate());
			existingapInvoicePaymentDetailses.setPayeeBankName(related_apinvoicepaymentdetailses.getPayeeBankName());
			existingapInvoicePaymentDetailses.setPayeeBankAccNo(related_apinvoicepaymentdetailses.getPayeeBankAccNo());
			existingapInvoicePaymentDetailses.setVarianceAmount(related_apinvoicepaymentdetailses.getVarianceAmount());
			existingapInvoicePaymentDetailses.setVarianceAccountCode(related_apinvoicepaymentdetailses.getVarianceAccountCode());
			existingapInvoicePaymentDetailses.setCreatedDate(related_apinvoicepaymentdetailses.getCreatedDate());
			existingapInvoicePaymentDetailses.setModifiedDate(related_apinvoicepaymentdetailses.getModifiedDate());
			related_apinvoicepaymentdetailses = existingapInvoicePaymentDetailses;
		} else {
			related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.store(related_apinvoicepaymentdetailses);
			apInvoicePaymentDetailsDAO.flush();
		}

		related_apinvoicepaymentdetailses.setApInvoice(apinvoice);
		apinvoice.getApInvoicePaymentDetailses().add(related_apinvoicepaymentdetailses);
		related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.store(related_apinvoicepaymentdetailses);
		apInvoicePaymentDetailsDAO.flush();

		apinvoice = apInvoiceDAO.store(apinvoice);
		apInvoiceDAO.flush();

		return apinvoice;
	}

	/**
	 * Delete an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public ApInvoice deleteApInvoiceApInvoicePaymentDetailses(BigInteger apinvoice_id, BigInteger related_apinvoicepaymentdetailses_id) {
		ApInvoicePaymentDetails related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(related_apinvoicepaymentdetailses_id, -1, -1);

		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(apinvoice_id, -1, -1);

		related_apinvoicepaymentdetailses.setApInvoice(null);
		apinvoice.getApInvoicePaymentDetailses().remove(related_apinvoicepaymentdetailses);

		apInvoicePaymentDetailsDAO.remove(related_apinvoicepaymentdetailses);
		apInvoicePaymentDetailsDAO.flush();

		return apinvoice;
	}

	/**
	 * Return a count of all ApInvoice entity
	 * 
	 */
	@Transactional
	public Integer countApInvoices() {
		return ((Long) apInvoiceDAO.createQuerySingleResult("select count(o) from ApInvoice o").getSingleResult()).intValue();
	}

	/**
	 * Return all ApInvoice entity
	 * 
	 */
	@Transactional
	public List<ApInvoice> findAllApInvoices(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoice>(apInvoiceDAO.findAllApInvoices(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id) {
		return apInvoiceDAO.findApInvoiceByPrimaryKey(id);
	}

	/**
	 * Save an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ApInvoice saveApInvoiceApInvoiceAccountDetailses(BigInteger id, ApInvoiceAccountDetails related_apinvoiceaccountdetailses) {
		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(id, -1, -1);
		ApInvoiceAccountDetails existingapInvoiceAccountDetailses = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(related_apinvoiceaccountdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoiceAccountDetailses != null) {
			existingapInvoiceAccountDetailses.setId(related_apinvoiceaccountdetailses.getId());
			existingapInvoiceAccountDetailses.setAccountCode(related_apinvoiceaccountdetailses.getAccountCode());
			existingapInvoiceAccountDetailses.setAccountName(related_apinvoiceaccountdetailses.getAccountName());
			existingapInvoiceAccountDetailses.setExcludeGstOriginalAmount(related_apinvoiceaccountdetailses.getExcludeGstOriginalAmount());
			existingapInvoiceAccountDetailses.setExcludeGstConvertedAmount(related_apinvoiceaccountdetailses.getExcludeGstConvertedAmount());
			existingapInvoiceAccountDetailses.setGstConvertedAmount(related_apinvoiceaccountdetailses.getGstConvertedAmount());
			existingapInvoiceAccountDetailses.setIncludeGstConvertedAmount(related_apinvoiceaccountdetailses.getIncludeGstConvertedAmount());
			existingapInvoiceAccountDetailses.setCreatedDate(related_apinvoiceaccountdetailses.getCreatedDate());
			existingapInvoiceAccountDetailses.setModifiedDate(related_apinvoiceaccountdetailses.getModifiedDate());
			related_apinvoiceaccountdetailses = existingapInvoiceAccountDetailses;
		} else {
			related_apinvoiceaccountdetailses = apInvoiceAccountDetailsDAO.store(related_apinvoiceaccountdetailses);
			apInvoiceAccountDetailsDAO.flush();
		}

		related_apinvoiceaccountdetailses.setApInvoice(apinvoice);
		apinvoice.getApInvoiceAccountDetailses().add(related_apinvoiceaccountdetailses);
		related_apinvoiceaccountdetailses = apInvoiceAccountDetailsDAO.store(related_apinvoiceaccountdetailses);
		apInvoiceAccountDetailsDAO.flush();

		apinvoice = apInvoiceDAO.store(apinvoice);
		apInvoiceDAO.flush();

		return apinvoice;
	}

	/**
	 * Delete an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ApInvoice deleteApInvoiceApInvoiceAccountDetailses(BigInteger apinvoice_id, BigInteger related_apinvoiceaccountdetailses_id) {
		ApInvoiceAccountDetails related_apinvoiceaccountdetailses = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(related_apinvoiceaccountdetailses_id, -1, -1);

		ApInvoice apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(apinvoice_id, -1, -1);

		related_apinvoiceaccountdetailses.setApInvoice(null);
		apinvoice.getApInvoiceAccountDetailses().remove(related_apinvoiceaccountdetailses);

		apInvoiceAccountDetailsDAO.remove(related_apinvoiceaccountdetailses);
		apInvoiceAccountDetailsDAO.flush();

		return apinvoice;
	}

	public List<InvoiceDetail> getInvoiceDetail(BigInteger apInvoiceId){
        return apInvoiceDAO.getInvoiceDetail(apInvoiceId);
    }
    
    public BigInteger getLastId(){
        return apInvoiceDAO.getLastId();
    }

    public BigInteger countSearch(String fromMonth, String toMonth, String invoice, String payeeName,
            					 String status, String fromApNo, String toApNo) {
    	BigInteger count = apInvoiceDAO.searchCount(fromMonth,toMonth,invoice,payeeName,status,fromApNo,toApNo);
        return count;
    }

   
    
    public String findAPInvoiceJSON(Integer startpage, Integer endPage, String fromMonth, String toMonth,
			String fromApNo, String toApNo, String payeeName, String invoiceNo, String status, Integer orderColumn, String orderBy){
		
		// list ar invoice no
		List<String> listApInvoiceNo = searchApInvoice(startpage, endPage, fromMonth, toMonth, invoiceNo, payeeName, status, fromApNo, toApNo, orderColumn, orderBy);
		
		//
		List<InvoicePayment> listApInvoice = new ArrayList<InvoicePayment>();
		listApInvoice = searchInvoiceDetail(listApInvoiceNo, orderColumn, orderBy);
		
		
		Map<String, String> invStatus = findSystemValueAsMapByType(Constant.PREFIX_API);
		Map<String, String> payStatus = findSystemValueAsMapByType(Constant.PREFIX_PAY);
		
		for (InvoicePayment invoicePayment : listApInvoice) {

			List<BankStatement> bankStatements = new ArrayList<BankStatement>(bankStatementDAO.findBankStatementByPaymentOrderNo(invoicePayment.getPaymentOrderNo()));
			invoicePayment.setBankStatement(bankStatements);
			//thoai.nh status to render class css
			invoicePayment.setPaymentStatusCode(invoicePayment.getPaymentStatus());
			invoicePayment.setInvoiceStatusCode(invoicePayment.getInvoiceStatus());
			//set status (code -> value)
			invoicePayment.setPaymentStatus(payStatus.get(invoicePayment.getPaymentStatus()));
			invoicePayment.setInvoiceStatus(invStatus.get(invoicePayment.getInvoiceStatus()));
			
			// set value for claim type 
			ClaimTypeMaster claimTypeMaster = claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(invoicePayment.getClaimType());
			if(null != claimTypeMaster){
			    invoicePayment.setClaimTypeVal(claimTypeMaster.getName());
			}
		}
		
		// AP invoice count
		BigInteger recordsFiltered = BigInteger.ZERO;
		recordsFiltered = countSearch(fromMonth, toMonth, invoiceNo, payeeName, status, fromApNo, toApNo);
        
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listApInvoice);
        map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
		
		return jsonData;
	}
    
    
    
    public List<String> searchApInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
            String invoiceNo, String payeeName, String status,  String fromApNo, String toApNo, Integer orderColumn, String orderBy) {
        List<PaymentInvoice> lstApinvoice = apInvoiceDAO.searchInvoice(startResult, maxRow, fromMonth, toMonth, invoiceNo, payeeName, status, fromApNo, toApNo, orderColumn, orderBy);
        
        List<String> list = new ArrayList<String>();
		for (Object data : lstApinvoice) {
			list.add(data.toString());
		}
        return list;
    }
    
    public List<PaymentInvoice> searchInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
            String invoice, String payeeName, String status,  String fromApNo, String toApNo) {
        List<PaymentInvoice> lstApinvoice = apInvoiceDAO.searchInvoice(startResult, maxRow, fromMonth, toMonth, invoice,payeeName,status,fromApNo,toApNo, -1, null);
        return lstApinvoice;
    }
    
    public List<InvoicePayment> searchInvoiceDetail(List<String> apInvoiceNoLst, Integer orderColumn, String orderBy){
		List<InvoicePayment> lstPayment = apInvoiceDAO.searchInvoiceDetail(apInvoiceNoLst, orderColumn, orderBy);
        return lstPayment;
	}
    
    public Map<String, String> findSystemValueAsMapByType(String type) {
        Map<String, String> ret = new LinkedHashMap<String, String>();
        
        Iterator<SystemValue> iter = systemValueDAO.findSystemValueByType(type).iterator();
        while (iter.hasNext()) {
            SystemValue sysVal = iter.next();
            ret.put(sysVal.getCode(), sysVal.getValue());
        }

        return ret;
    }
    
    @Override
    public ApInvoice findApInvoiceByApInvoiceNo(String apInvoiceNo) {
        return apInvoiceDAO.findApInvoiceByApNo(apInvoiceNo);
    }
}
