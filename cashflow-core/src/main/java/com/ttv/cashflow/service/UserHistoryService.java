
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.UserHistory;

/**
 * Spring service that handles CRUD requests for UserHistory entities
 * 
 */
public interface UserHistoryService {

	/**
	 */
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id);

	/**
	* Delete an existing UserHistory entity
	* 
	 */
	public void deleteUserHistory(UserHistory userhistory);

	/**
	* Load an existing UserHistory entity
	* 
	 */
	public Set<UserHistory> loadUserHistorys();

	/**
	* Save an existing UserHistory entity
	* 
	 */
	public void saveUserHistory(UserHistory userhistory_1);

	/**
	* Return all UserHistory entity
	* 
	 */
	public List<UserHistory> findAllUserHistorys(Integer startResult, Integer maxRows);

	/**
	* Return a count of all UserHistory entity
	* 
	 */
	public Integer countUserHistorys();

    public String findUserHistoryJson(UserHistory userHistory, String fromMonth, String toMonth, Integer startpage, Integer lengthOfpage,
            Integer orderColumn, String orderBy);
}