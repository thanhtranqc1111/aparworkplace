
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.dto.UserAccountAdminForm;
import com.ttv.cashflow.dto.UserAccountForm;

/**
 * Spring service that handles CRUD requests for UserAccount entities
 * 
 */
public interface UserAccountService {

	/**
	* Save an existing SubsidiaryUserDetail entity
	* 
	 */
	public UserAccount saveUserAccountSubsidiaryUserDetails(Integer id, SubsidiaryUserDetail related_subsidiaryuserdetails);

	/**
	* Delete an existing UserAccount entity
	* 
	 */
	public void deleteUserAccount(UserAccount useraccount);

	/**
	* Return a count of all UserAccount entity
	* 
	 */
	public Integer countUserAccounts();

	/**
	* Save an existing Role entity
	* 
	 */
	public UserAccount saveUserAccountRole(Integer id_1, Role related_role);

	/**
	* Return all UserAccount entity
	* 
	 */
	public List<UserAccount> findAllUserAccounts(Integer startResult, Integer maxRows);

	/**
	* Delete an existing SubsidiaryUserDetail entity
	* 
	 */
	public UserAccount deleteUserAccountSubsidiaryUserDetails(Integer useraccount_id, Integer related_subsidiaryuserdetails_id);

	/**
	* Load an existing UserAccount entity
	* 
	 */
	public Set<UserAccount> loadUserAccounts();

	/**
	 */
	public UserAccount findUserAccountByPrimaryKey(Integer id_2);

	/**
	* Delete an existing Role entity
	* 
	 */
	public UserAccount deleteUserAccountRole(Integer useraccount_id_1, Integer related_role_id);

	/**
	* Save an existing UserAccount entity
	* 
	 */
	public void saveUserAccount(UserAccount useraccount_1);
	
	/**
	 * Get Account by email
	 */
	
	public Set<UserAccount> findUserAccountByEmail(String email);
	/**
	 * save user info
	 * @param userAccountDTO
	 * @return 1: success, 0: error, -1: Invalid password, the password must contain uppercase, lowercase, numbers and minimum 8 characters ,-2: invalid old password, -3: two new password don't match
	 */
	/**
	 * service update profile, all user can use
	 * @param userAccountDTO
	 * @return
	 */
	public int saveUserAccount(UserAccountForm userAccountDTO);
	/**
	 * service update user account for admin
	 * @param userAccountDTO
	 * @return
	 */
	public int saveUserAccountAdmin(UserAccountAdminForm userAccountDTO);
	public String generatePassword();
	public String findUserAccountJson(Integer start, Integer end, Integer tenantId, String keyword);
	public void deleteUserAccountByIds(Integer ids[]);
	public UserAccount preAuthenticateUserAccount(String email, String password);
}