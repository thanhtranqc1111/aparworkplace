package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.service.PayeePayerMasterService;

/**
 * Spring service that handles CRUD requests for PayeePayerMaster entities
 * 
 */

@Service("PayeePayerMasterService")

@Transactional
public class PayeePayerMasterServiceImpl implements PayeePayerMasterService {

	/**
	 * DAO injected by Spring that manages PayeePayerMaster entities
	 * 
	 */
	@Autowired
	private PayeePayerMasterDAO payeePayerMasterDAO;

	/**
	 * Instantiates a new PayeePayerMasterServiceImpl.
	 *
	 */
	public PayeePayerMasterServiceImpl() {
	}

	/* (non-Javadoc)
	* @see com.ttv.cashflow.service.PayeePayerMasterService#findPayeePayerMasterByAccountAndName(java.lang.String)
	*/
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountAndName(String keyword) {
		return payeePayerMasterDAO.findPayeePayerMasterByAccountAndName(keyword);
	}
		
	/**
	 * Return a count of all PayeePayerMaster entity
	 * 
	 */
	@Transactional
	public Integer countPayeePayerMasters() {
		return ((Long) payeePayerMasterDAO.createQuerySingleResult("select count(o) from PayeePayerMaster o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing PayeePayerMaster entity
	 * 
	 */
	@Transactional
	public void savePayeePayerMaster(PayeePayerMaster payeepayermaster) {
		PayeePayerMaster existingPayeePayerMaster = payeePayerMasterDAO.findPayeePayerMasterByPrimaryKey(payeepayermaster.getCode());

		if (existingPayeePayerMaster != null) {
			if (existingPayeePayerMaster != payeepayermaster) {
				existingPayeePayerMaster.setCode(payeepayermaster.getCode());
				existingPayeePayerMaster.setName(payeepayermaster.getName());
				existingPayeePayerMaster.setTypeCode(payeepayermaster.getTypeCode());
				existingPayeePayerMaster.setPaymentTerm(payeepayermaster.getPaymentTerm());
				existingPayeePayerMaster.setAccountCode(payeepayermaster.getAccountCode());
				if(payeepayermaster.getBankName() != null && !payeepayermaster.getBankName().isEmpty())
				{
					existingPayeePayerMaster.setBankName(payeepayermaster.getBankName());
				}
				if(payeepayermaster.getBankAccountCode() != null && !payeepayermaster.getBankAccountCode().isEmpty())
				{
					existingPayeePayerMaster.setBankAccountCode(payeepayermaster.getBankAccountCode());
				}
				if(payeepayermaster.getAddress() != null && !payeepayermaster.getAddress().isEmpty())
				{
					existingPayeePayerMaster.setAddress(payeepayermaster.getAddress());
				}
				if(payeepayermaster.getPhone() != null && !payeepayermaster.getPhone().isEmpty())
				{
					existingPayeePayerMaster.setPhone(payeepayermaster.getPhone());
				}
				if(payeepayermaster.getDefaultCurrency() != null && !payeepayermaster.getDefaultCurrency().isEmpty())
				{
					existingPayeePayerMaster.setDefaultCurrency(payeepayermaster.getDefaultCurrency());
				}
			}
			payeepayermaster = payeePayerMasterDAO.store(existingPayeePayerMaster);
		} else {
			payeepayermaster = payeePayerMasterDAO.store(payeepayermaster);
		}
		payeePayerMasterDAO.flush();
	}

	/**
	 */
	@Transactional
	public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code) {
		return payeePayerMasterDAO.findPayeePayerMasterByPrimaryKey(code);
	}

	/**
	 * Delete an existing PayeePayerMaster entity
	 * 
	 */
	@Transactional
	public void deletePayeePayerMaster(PayeePayerMaster payeepayermaster) {
		payeePayerMasterDAO.remove(payeepayermaster);
		payeePayerMasterDAO.flush();
	}

	/**
	 * Load an existing PayeePayerMaster entity
	 * 
	 */
	@Transactional
	public Set<PayeePayerMaster> loadPayeePayerMasters() {
		return payeePayerMasterDAO.findAllPayeePayerMasters();
	}

	/**
	 * Return all PayeePayerMaster entity
	 * 
	 */
	@Transactional
	public List<PayeePayerMaster> findAllPayeePayerMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PayeePayerMaster>(payeePayerMasterDAO.findAllPayeePayerMasters(startResult, maxRows));
	}

    public String findPayeeMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword) {
        Integer recordsTotal = 0;
        recordsTotal = payeePayerMasterDAO.findAllPayeePayerMasters().size();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", payeePayerMasterDAO.findPayeeMasterPaging(start, end, orderColumn, orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", payeePayerMasterDAO.countPayeeMasterFilter(start,
                end, orderColumn, orderBy, keyword));
        Gson gson = new Gson();
        String jPayeeMaster = gson.toJson(map);
        return jPayeeMaster;
    }

    @Transactional
    public void deletePayeePayerMasterByCodes(String[] codes) {
        for (String code : codes) {
            PayeePayerMaster payeePayerMaster = payeePayerMasterDAO
                    .findPayeePayerMasterByCode(code);
            if (payeePayerMaster != null) {
                deletePayeePayerMaster(payeePayerMaster);
            }
        }
        
    }

	public String findPayeePayerMasterByPrimaryKeyJson(String code) {
		// TODO Auto-generated method stub
		PayeePayerMaster payeePayerMaster = payeePayerMasterDAO.findPayeePayerMasterByPrimaryKey(code);
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", payeePayerMaster);
        Gson gson = new Gson();
        return gson.toJson(map);
	}

    @Override
    public List<PayeePayerMaster> findTransactionPartyMastersByTransactionType(String transactionType) {
        return payeePayerMasterDAO.findTransactionPartyMastersByTransactionType(transactionType);
    }

    @Override
    public boolean checkExistingCustomerName(String customerName) {
        return payeePayerMasterDAO.checkExistingCustomerName(customerName);
    }
}
