
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ApInvoiceAccountDetails entities
 * 
 */
public interface ApInvoiceAccountDetailsService {

	/**
	* Return all ApInvoiceAccountDetails entity
	* 
	 */
	public List<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss(Integer startResult, Integer maxRows);

	/**
	* Load an existing ApInvoiceAccountDetails entity
	* 
	 */
	public Set<ApInvoiceAccountDetails> loadApInvoiceAccountDetailss();

	/**
	* Return a count of all ApInvoiceAccountDetails entity
	* 
	 */
	public Integer countApInvoiceAccountDetailss();

	/**
	* Save an existing ApInvoiceClassDetails entity
	* 
	 */
	public ApInvoiceAccountDetails saveApInvoiceAccountDetailsApInvoiceClassDetailses(BigInteger id, ApInvoiceClassDetails related_apinvoiceclassdetailses);

	/**
	* Delete an existing ApInvoiceClassDetails entity
	* 
	 */
	public ApInvoiceAccountDetails deleteApInvoiceAccountDetailsApInvoiceClassDetailses(BigInteger apinvoiceaccountdetails_id, BigInteger related_apinvoiceclassdetailses_id);

	/**
	* Save an existing ApInvoice entity
	* 
	 */
	public ApInvoiceAccountDetails saveApInvoiceAccountDetailsApInvoice(BigInteger id_1, ApInvoice related_apinvoice);

	/**
	* Delete an existing ApInvoice entity
	* 
	 */
	public ApInvoiceAccountDetails deleteApInvoiceAccountDetailsApInvoice(BigInteger apinvoiceaccountdetails_id_1, BigInteger related_apinvoice_id);

	/**
	 */
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id_2);

	/**
	* Save an existing ApInvoiceAccountDetails entity
	* 
	 */
	public void saveApInvoiceAccountDetails(ApInvoiceAccountDetails apinvoiceaccountdetails);

	/**
	* Delete an existing ApInvoiceAccountDetails entity
	* 
	 */
	public void deleteApInvoiceAccountDetails(ApInvoiceAccountDetails apinvoiceaccountdetails_1);

	/**
	 * Get last id
	 */
	public BigInteger getLastId();
}