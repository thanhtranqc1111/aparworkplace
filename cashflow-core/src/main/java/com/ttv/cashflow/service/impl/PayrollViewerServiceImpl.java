package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.PayrollViewerDAO;
import com.ttv.cashflow.domain.PayrollViewer;
import com.ttv.cashflow.service.PayrollViewerService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for PayrollViewer entities
 * 
 */

@Service("PayrollViewerService")

@Transactional
public class PayrollViewerServiceImpl implements PayrollViewerService {

	/**
	 * DAO injected by Spring that manages PayrollViewer entities
	 * 
	 */
	@Autowired
	private PayrollViewerDAO payrollViewerDAO;

	/**
	 * Instantiates a new PayrollViewerServiceImpl.
	 *
	 */
	public PayrollViewerServiceImpl() {
	}

	/**
	 * Return a count of all PayrollViewer entity
	 * 
	 */
	@Transactional
	public Integer countPayrollViewers() {
		return ((Long) payrollViewerDAO.createQuerySingleResult("select count(o) from PayrollViewer o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id) {
		return payrollViewerDAO.findPayrollViewerByPrimaryKey(id);
	}

	/**
	 * Return all PayrollViewer entity
	 * 
	 */
	@Transactional
	public List<PayrollViewer> findAllPayrollViewers(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PayrollViewer>(payrollViewerDAO.findAllPayrollViewers(startResult, maxRows));
	}

	/**
	 * Delete an existing PayrollViewer entity
	 * 
	 */
	@Transactional
	public void deletePayrollViewer(PayrollViewer payrollviewer) {
		payrollViewerDAO.remove(payrollviewer);
		payrollViewerDAO.flush();
	}

	/**
	 * Save an existing PayrollViewer entity
	 * 
	 */
	@Transactional
	public void savePayrollViewer(PayrollViewer payrollviewer) {
		PayrollViewer existingPayrollViewer = payrollViewerDAO.findPayrollViewerByPrimaryKey(payrollviewer.getId());

		if (existingPayrollViewer != null) {
			if (existingPayrollViewer != payrollviewer) {
				existingPayrollViewer.setId(payrollviewer.getId());
				existingPayrollViewer.setPayrollNo(payrollviewer.getPayrollNo());
				existingPayrollViewer.setMonth(payrollviewer.getMonth());
				existingPayrollViewer.setClaimType(payrollviewer.getClaimType());
				existingPayrollViewer.setPaymentScheduleDate(payrollviewer.getPaymentScheduleDate());
				existingPayrollViewer.setDescription(payrollviewer.getDescription());
				existingPayrollViewer.setInvoiceNo(payrollviewer.getInvoiceNo());
				existingPayrollViewer.setOriginalCurrencyCode(payrollviewer.getOriginalCurrencyCode());
				existingPayrollViewer.setTotalNetPaymentOriginal(payrollviewer.getTotalNetPaymentOriginal());
				existingPayrollViewer.setFxRate(payrollviewer.getFxRate());
				existingPayrollViewer.setTotalNetPaymentConverted(payrollviewer.getTotalNetPaymentConverted());
				existingPayrollViewer.setEmployeeCode(payrollviewer.getEmployeeCode());
				existingPayrollViewer.setRoleTitle(payrollviewer.getRoleTitle());
				existingPayrollViewer.setDivision(payrollviewer.getDivision());
				existingPayrollViewer.setTeam(payrollviewer.getTeam());
				existingPayrollViewer.setLaborBaseSalaryPayment(payrollviewer.getLaborBaseSalaryPayment());
				existingPayrollViewer.setLaborAdditionalPayment1(payrollviewer.getLaborAdditionalPayment1());
				existingPayrollViewer.setLaborAdditionalPayment2(payrollviewer.getLaborAdditionalPayment2());
				existingPayrollViewer.setLaborAdditionalPayment3(payrollviewer.getLaborAdditionalPayment3());
				existingPayrollViewer.setLaborSocialInsuranceEmployer1(payrollviewer.getLaborSocialInsuranceEmployer1());
				existingPayrollViewer.setLaborSocialInsuranceEmployer2(payrollviewer.getLaborSocialInsuranceEmployer2());
				existingPayrollViewer.setLaborSocialInsuranceEmployer3(payrollviewer.getLaborSocialInsuranceEmployer3());
				existingPayrollViewer.setLaborOtherPayment1(payrollviewer.getLaborOtherPayment1());
				existingPayrollViewer.setLaborOtherPayment2(payrollviewer.getLaborOtherPayment2());
				existingPayrollViewer.setLaborOtherPayment3(payrollviewer.getLaborOtherPayment3());
				existingPayrollViewer.setTotalLaborCost(payrollviewer.getTotalLaborCost());
				existingPayrollViewer.setDeductionSocialInsuranceEmployer1(payrollviewer.getDeductionSocialInsuranceEmployer1());
				existingPayrollViewer.setDeductionSocialInsuranceEmployer2(payrollviewer.getDeductionSocialInsuranceEmployer2());
				existingPayrollViewer.setDeductionSocialInsuranceEmployer3(payrollviewer.getDeductionSocialInsuranceEmployer3());
				existingPayrollViewer.setDeductionSocialInsuranceEmployee1(payrollviewer.getDeductionSocialInsuranceEmployee1());
				existingPayrollViewer.setDeductionSocialInsuranceEmployee2(payrollviewer.getDeductionSocialInsuranceEmployee2());
				existingPayrollViewer.setDeductionSocialInsuranceEmployee3(payrollviewer.getDeductionSocialInsuranceEmployee3());
				existingPayrollViewer.setDeductionWht(payrollviewer.getDeductionWht());
				existingPayrollViewer.setDeductionOther1(payrollviewer.getDeductionOther1());
				existingPayrollViewer.setDeductionOther2(payrollviewer.getDeductionOther2());
				existingPayrollViewer.setDeductionOther3(payrollviewer.getDeductionOther3());
				existingPayrollViewer.setTotalDeduction(payrollviewer.getTotalDeduction());
				existingPayrollViewer.setNetPaymentOriginal(payrollviewer.getNetPaymentOriginal());
				existingPayrollViewer.setNetPaymentConverted(payrollviewer.getNetPaymentConverted());
				existingPayrollViewer.setPaymentOrderNo(payrollviewer.getPaymentOrderNo());
				existingPayrollViewer.setValueDate(payrollviewer.getValueDate());
				existingPayrollViewer.setPaymentAmountOriginal(payrollviewer.getPaymentAmountOriginal());
				existingPayrollViewer.setPaymentRate(payrollviewer.getPaymentRate());
				existingPayrollViewer.setPaymentAmountConverted(payrollviewer.getPaymentAmountConverted());
				existingPayrollViewer.setPaymentRemainAmount(payrollviewer.getPaymentRemainAmount());
			}
			payrollviewer = payrollViewerDAO.store(existingPayrollViewer);
		} else {
			payrollviewer = payrollViewerDAO.store(payrollviewer);
		}
		payrollViewerDAO.flush();
	}

	/**
	 * Load an existing PayrollViewer entity
	 * 
	 */
	@Transactional
	public Set<PayrollViewer> loadPayrollViewers() {
		return payrollViewerDAO.findAllPayrollViewers();
	}
}
