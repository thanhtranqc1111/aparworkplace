package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.service.ApInvoiceAccountDetailsService;

/**
 * Spring service that handles CRUD requests for ApInvoiceAccountDetails entities
 * 
 */

@Service("ApInvoiceAccountDetailsService")

@Transactional
public class ApInvoiceAccountDetailsServiceImpl implements ApInvoiceAccountDetailsService {

	/**
	 * DAO injected by Spring that manages ApInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ApInvoiceAccountDetailsDAO apInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ApInvoiceClassDetails entities
	 * 
	 */
	@Autowired
	private ApInvoiceClassDetailsDAO apInvoiceClassDetailsDAO;

	/**
	 * DAO injected by Spring that manages ApInvoice entities
	 * 
	 */
	@Autowired
	private ApInvoiceDAO apInvoiceDAO;

	/**
	 * Instantiates a new ApInvoiceAccountDetailsServiceImpl.
	 *
	 */
	public ApInvoiceAccountDetailsServiceImpl() {
	}

	/**
	 * Return all ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public List<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoiceAccountDetails>(apInvoiceAccountDetailsDAO.findAllApInvoiceAccountDetailss(startResult, maxRows));
	}

	/**
	 * Load an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> loadApInvoiceAccountDetailss() {
		return apInvoiceAccountDetailsDAO.findAllApInvoiceAccountDetailss();
	}

	/**
	 * Return a count of all ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public Integer countApInvoiceAccountDetailss() {
		return ((Long) apInvoiceAccountDetailsDAO.createQuerySingleResult("select count(o) from ApInvoiceAccountDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public ApInvoiceAccountDetails saveApInvoiceAccountDetailsApInvoiceClassDetailses(BigInteger id, ApInvoiceClassDetails related_apinvoiceclassdetailses) {
		ApInvoiceAccountDetails apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
		ApInvoiceClassDetails existingapInvoiceClassDetailses = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(related_apinvoiceclassdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoiceClassDetailses != null) {
			existingapInvoiceClassDetailses.setId(related_apinvoiceclassdetailses.getId());
			existingapInvoiceClassDetailses.setClassCode(related_apinvoiceclassdetailses.getClassCode());
			existingapInvoiceClassDetailses.setClassName(related_apinvoiceclassdetailses.getClassName());
			existingapInvoiceClassDetailses.setDescription(related_apinvoiceclassdetailses.getDescription());
			existingapInvoiceClassDetailses.setPoNo(related_apinvoiceclassdetailses.getPoNo());
			existingapInvoiceClassDetailses.setApprovalCode(related_apinvoiceclassdetailses.getApprovalCode());
			existingapInvoiceClassDetailses.setExcludeGstOriginalAmount(related_apinvoiceclassdetailses.getExcludeGstOriginalAmount());
			existingapInvoiceClassDetailses.setExcludeGstConvertedAmount(related_apinvoiceclassdetailses.getExcludeGstConvertedAmount());
			existingapInvoiceClassDetailses.setGstOriginalAmount(related_apinvoiceclassdetailses.getGstOriginalAmount());
			existingapInvoiceClassDetailses.setGstConvertedAmount(related_apinvoiceclassdetailses.getGstConvertedAmount());
			existingapInvoiceClassDetailses.setIncludeGstConvertedAmount(related_apinvoiceclassdetailses.getIncludeGstConvertedAmount());
			//existingapInvoiceClassDetailses.setCreatedDate(related_apinvoiceclassdetailses.getCreatedDate());
			existingapInvoiceClassDetailses.setModifiedDate(related_apinvoiceclassdetailses.getModifiedDate());
			related_apinvoiceclassdetailses = existingapInvoiceClassDetailses;
		} else {
			related_apinvoiceclassdetailses = apInvoiceClassDetailsDAO.store(related_apinvoiceclassdetailses);
			apInvoiceClassDetailsDAO.flush();
		}

		related_apinvoiceclassdetailses.setApInvoiceAccountDetails(apinvoiceaccountdetails);
		apinvoiceaccountdetails.getApInvoiceClassDetailses().add(related_apinvoiceclassdetailses);
		related_apinvoiceclassdetailses = apInvoiceClassDetailsDAO.store(related_apinvoiceclassdetailses);
		apInvoiceClassDetailsDAO.flush();

		apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		return apinvoiceaccountdetails;
	}

	/**
	 * Delete an existing ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public ApInvoiceAccountDetails deleteApInvoiceAccountDetailsApInvoiceClassDetailses(BigInteger apinvoiceaccountdetails_id, BigInteger related_apinvoiceclassdetailses_id) {
		ApInvoiceClassDetails related_apinvoiceclassdetailses = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(related_apinvoiceclassdetailses_id, -1, -1);

		ApInvoiceAccountDetails apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(apinvoiceaccountdetails_id, -1, -1);

		related_apinvoiceclassdetailses.setApInvoiceAccountDetails(null);
		apinvoiceaccountdetails.getApInvoiceClassDetailses().remove(related_apinvoiceclassdetailses);

		apInvoiceClassDetailsDAO.remove(related_apinvoiceclassdetailses);
		apInvoiceClassDetailsDAO.flush();

		return apinvoiceaccountdetails;
	}

	/**
	 * Save an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public ApInvoiceAccountDetails saveApInvoiceAccountDetailsApInvoice(BigInteger id, ApInvoice related_apinvoice) {
		ApInvoiceAccountDetails apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
		ApInvoice existingapInvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(related_apinvoice.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoice != null) {
			existingapInvoice.setId(related_apinvoice.getId());
			existingapInvoice.setPayeeCode(related_apinvoice.getPayeeCode());
			existingapInvoice.setPayeeName(related_apinvoice.getPayeeName());
			existingapInvoice.setPayeeTypeCode(related_apinvoice.getPayeeTypeCode());
			existingapInvoice.setPayeeTypeName(related_apinvoice.getPayeeTypeName());
			existingapInvoice.setApInvoiceNo(related_apinvoice.getApInvoiceNo());
			existingapInvoice.setInvoiceNo(related_apinvoice.getInvoiceNo());
			existingapInvoice.setOriginalCurrencyCode(related_apinvoice.getOriginalCurrencyCode());
			existingapInvoice.setFxRate(related_apinvoice.getFxRate());
			existingapInvoice.setGstRate(related_apinvoice.getGstRate());
			existingapInvoice.setDescription(related_apinvoice.getDescription());
			existingapInvoice.setInvoiceIssuedDate(related_apinvoice.getInvoiceIssuedDate());
			existingapInvoice.setClaimType(related_apinvoice.getClaimType());
			existingapInvoice.setGstType(related_apinvoice.getGstType());
			existingapInvoice.setAccountPayableCode(related_apinvoice.getAccountPayableCode());
			existingapInvoice.setAccountPayableName(related_apinvoice.getAccountPayableName());
			existingapInvoice.setExcludeGstOriginalAmount(related_apinvoice.getExcludeGstOriginalAmount());
			existingapInvoice.setExcludeGstConvertedAmount(related_apinvoice.getExcludeGstConvertedAmount());
			existingapInvoice.setGstOriginalAmount(related_apinvoice.getGstOriginalAmount());
			existingapInvoice.setGstConvertedAmount(related_apinvoice.getGstConvertedAmount());
			existingapInvoice.setIncludeGstOriginalAmount(related_apinvoice.getIncludeGstOriginalAmount());
			existingapInvoice.setIncludeGstConvertedAmount(related_apinvoice.getIncludeGstConvertedAmount());
			existingapInvoice.setPaymentTerm(related_apinvoice.getPaymentTerm());
			existingapInvoice.setScheduledPaymentDate(related_apinvoice.getScheduledPaymentDate());
			existingapInvoice.setStatus(related_apinvoice.getStatus());
			existingapInvoice.setCreatedDate(related_apinvoice.getCreatedDate());
			existingapInvoice.setModifiedDate(related_apinvoice.getModifiedDate());
			related_apinvoice = existingapInvoice;
		} else {
			related_apinvoice = apInvoiceDAO.store(related_apinvoice);
			apInvoiceDAO.flush();
		}

		apinvoiceaccountdetails.setApInvoice(related_apinvoice);
		related_apinvoice.getApInvoiceAccountDetailses().add(apinvoiceaccountdetails);
		apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		related_apinvoice = apInvoiceDAO.store(related_apinvoice);
		apInvoiceDAO.flush();

		return apinvoiceaccountdetails;
	}

	/**
	 * Delete an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public ApInvoiceAccountDetails deleteApInvoiceAccountDetailsApInvoice(BigInteger apinvoiceaccountdetails_id, BigInteger related_apinvoice_id) {
		ApInvoiceAccountDetails apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(apinvoiceaccountdetails_id, -1, -1);
		ApInvoice related_apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(related_apinvoice_id, -1, -1);

		apinvoiceaccountdetails.setApInvoice(null);
		related_apinvoice.getApInvoiceAccountDetailses().remove(apinvoiceaccountdetails);
		apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		related_apinvoice = apInvoiceDAO.store(related_apinvoice);
		apInvoiceDAO.flush();

		apInvoiceDAO.remove(related_apinvoice);
		apInvoiceDAO.flush();

		return apinvoiceaccountdetails;
	}

	/**
	 */
	@Transactional
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id) {
		return apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(id);
	}

	/**
	 * Save an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public void saveApInvoiceAccountDetails(ApInvoiceAccountDetails apinvoiceaccountdetails) {
		ApInvoiceAccountDetails existingApInvoiceAccountDetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(apinvoiceaccountdetails.getId());

		if (existingApInvoiceAccountDetails != null) {
			if (existingApInvoiceAccountDetails != apinvoiceaccountdetails) {
				existingApInvoiceAccountDetails.setId(apinvoiceaccountdetails.getId());
				existingApInvoiceAccountDetails.setAccountCode(apinvoiceaccountdetails.getAccountCode());
				existingApInvoiceAccountDetails.setAccountName(apinvoiceaccountdetails.getAccountName());
				existingApInvoiceAccountDetails.setExcludeGstOriginalAmount(apinvoiceaccountdetails.getExcludeGstOriginalAmount());
				existingApInvoiceAccountDetails.setExcludeGstConvertedAmount(apinvoiceaccountdetails.getExcludeGstConvertedAmount());
				existingApInvoiceAccountDetails.setGstConvertedAmount(apinvoiceaccountdetails.getGstConvertedAmount());
				existingApInvoiceAccountDetails.setIncludeGstConvertedAmount(apinvoiceaccountdetails.getIncludeGstConvertedAmount());
				//existingApInvoiceAccountDetails.setCreatedDate(apinvoiceaccountdetails.getCreatedDate());
				existingApInvoiceAccountDetails.setModifiedDate(apinvoiceaccountdetails.getModifiedDate());
			}
			apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(existingApInvoiceAccountDetails);
		} else {
			ApInvoiceAccountDetails ret = apInvoiceAccountDetailsDAO.store(apinvoiceaccountdetails);
			apinvoiceaccountdetails.copy(ret);
		}
		apInvoiceAccountDetailsDAO.flush();
	}

	/**
	 * Delete an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public void deleteApInvoiceAccountDetails(ApInvoiceAccountDetails apinvoiceaccountdetails) {
		apInvoiceAccountDetailsDAO.remove(apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();
	}

	@Transactional
	public BigInteger getLastId(){
	    return apInvoiceAccountDetailsDAO.getLastId();
	}

}
