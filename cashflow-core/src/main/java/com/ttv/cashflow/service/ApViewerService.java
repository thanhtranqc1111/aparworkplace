
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ApViewer;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ApViewer entities
 * 
 */
public interface ApViewerService {

	/**
	* Load an existing ApViewer entity
	* 
	 */
	public Set<ApViewer> loadApViewers();

	/**
	* Return a count of all ApViewer entity
	* 
	 */
	public Integer countApViewers();

	/**
	 */
	public ApViewer findApViewerByPrimaryKey(BigInteger id);

	/**
	* Return all ApViewer entity
	* 
	 */
	public List<ApViewer> findAllApViewers(Integer startResult, Integer maxRows);

	/**
	* Delete an existing ApViewer entity
	* 
	 */
	public void deleteApViewer(ApViewer apViewer);

	/**
	* Save an existing ApViewer entity
	* 
	 */
	public void saveApViewer(ApViewer apViewer);
}