package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ArInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.dto.ArInvoiceDTO;
import com.ttv.cashflow.service.ArInvoiceReceiptStatusService;
import com.ttv.cashflow.service.ArInvoiceService;

/**
 * Spring service that handles CRUD requests for ArInvoice entities
 * 
 */

@Service("ArInvoiceService")

@Transactional
public class ArInvoiceServiceImpl implements ArInvoiceService {

	/**
	 * DAO injected by Spring that manages ArInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ArInvoiceAccountDetailsDAO arInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ArInvoice entities
	 * 
	 */
	@Autowired
	private ArInvoiceDAO arInvoiceDAO;
	
	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;

	@Autowired
	private ArInvoiceReceiptStatusService arInvoiceReceiptStatusService;
	
	/**
	 * Instantiates a new ArInvoiceServiceImpl.
	 *
	 */
	public ArInvoiceServiceImpl() {
	}

	/**
	 * Return a count of all ArInvoice entity
	 * 
	 */
	@Transactional
	public Integer countArInvoices() {
		return ((Long) arInvoiceDAO.createQuerySingleResult("select count(o) from ArInvoice o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ArInvoice entity
	 * 
	 */
	@Transactional
	public Set<ArInvoice> loadArInvoices() {
		return arInvoiceDAO.findAllArInvoices();
	}

	/**
	 * Save an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ArInvoice saveArInvoiceArInvoiceAccountDetailses(BigInteger id, ArInvoiceAccountDetails related_arinvoiceaccountdetailses) {
		ArInvoice arinvoice = arInvoiceDAO.findArInvoiceByPrimaryKey(id, -1, -1);
		ArInvoiceAccountDetails existingarInvoiceAccountDetailses = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(related_arinvoiceaccountdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingarInvoiceAccountDetailses != null) {
			existingarInvoiceAccountDetailses.setId(related_arinvoiceaccountdetailses.getId());
			existingarInvoiceAccountDetailses.setAccountCode(related_arinvoiceaccountdetailses.getAccountCode());
			existingarInvoiceAccountDetailses.setAccountName(related_arinvoiceaccountdetailses.getAccountName());
			existingarInvoiceAccountDetailses.setExcludeGstOriginalAmount(related_arinvoiceaccountdetailses.getExcludeGstOriginalAmount());
			existingarInvoiceAccountDetailses.setExcludeGstConvertedAmount(related_arinvoiceaccountdetailses.getExcludeGstConvertedAmount());
			existingarInvoiceAccountDetailses.setGstOriginalAmount(related_arinvoiceaccountdetailses.getGstOriginalAmount());
			existingarInvoiceAccountDetailses.setGstConvertedAmount(related_arinvoiceaccountdetailses.getGstConvertedAmount());
			existingarInvoiceAccountDetailses.setIncludeGstOriginalAmount(related_arinvoiceaccountdetailses.getIncludeGstOriginalAmount());
			existingarInvoiceAccountDetailses.setIncludeGstConvertedAmount(related_arinvoiceaccountdetailses.getIncludeGstConvertedAmount());
			existingarInvoiceAccountDetailses.setCreatedDate(related_arinvoiceaccountdetailses.getCreatedDate());
			existingarInvoiceAccountDetailses.setModifiedDate(related_arinvoiceaccountdetailses.getModifiedDate());
			related_arinvoiceaccountdetailses = existingarInvoiceAccountDetailses;
		} else {
			related_arinvoiceaccountdetailses = arInvoiceAccountDetailsDAO.store(related_arinvoiceaccountdetailses);
			arInvoiceAccountDetailsDAO.flush();
		}

		related_arinvoiceaccountdetailses.setArInvoice(arinvoice);
		arinvoice.getArInvoiceAccountDetailses().add(related_arinvoiceaccountdetailses);
		related_arinvoiceaccountdetailses = arInvoiceAccountDetailsDAO.store(related_arinvoiceaccountdetailses);
		arInvoiceAccountDetailsDAO.flush();

		arinvoice = arInvoiceDAO.store(arinvoice);
		arInvoiceDAO.flush();

		return arinvoice;
	}


	/**
	 * Save an existing ArInvoice entity
	 * 
	 */
	@Transactional
	public void saveArInvoice(ArInvoice arinvoice) {
		ArInvoice existingArInvoice = arInvoiceDAO.findArInvoiceByPrimaryKey(arinvoice.getId());

		if (existingArInvoice != null) {
			if (existingArInvoice != arinvoice) {
				existingArInvoice.setId(arinvoice.getId());
				existingArInvoice.setPayerName(arinvoice.getPayerName());
				existingArInvoice.setArInvoiceNo(arinvoice.getArInvoiceNo());
				existingArInvoice.setInvoiceNo(arinvoice.getInvoiceNo());
				existingArInvoice.setOriginalCurrencyCode(arinvoice.getOriginalCurrencyCode());
				existingArInvoice.setFxRate(arinvoice.getFxRate());
				existingArInvoice.setGstRate(arinvoice.getGstRate());
				existingArInvoice.setInvoiceIssuedDate(arinvoice.getInvoiceIssuedDate());
				existingArInvoice.setClaimType(arinvoice.getClaimType());
				existingArInvoice.setGstType(arinvoice.getGstType());
				existingArInvoice.setApprovalCode(arinvoice.getApprovalCode());
				existingArInvoice.setAccountReceivableCode(arinvoice.getAccountReceivableCode());
				existingArInvoice.setAccountReceivableName(arinvoice.getAccountReceivableName());
				existingArInvoice.setDescription(arinvoice.getDescription());
				existingArInvoice.setExcludeGstOriginalAmount(arinvoice.getExcludeGstOriginalAmount());
				existingArInvoice.setExcludeGstConvertedAmount(arinvoice.getExcludeGstConvertedAmount());
				existingArInvoice.setGstOriginalAmount(arinvoice.getGstOriginalAmount());
				existingArInvoice.setGstConvertedAmount(arinvoice.getGstConvertedAmount());
				existingArInvoice.setIncludeGstOriginalAmount(arinvoice.getIncludeGstOriginalAmount());
				existingArInvoice.setIncludeGstConvertedAmount(arinvoice.getIncludeGstConvertedAmount());
				existingArInvoice.setStatus(arinvoice.getStatus());
				existingArInvoice.setCreatedDate(arinvoice.getCreatedDate());
				existingArInvoice.setModifiedDate(arinvoice.getModifiedDate());
				existingArInvoice.setMonth(arinvoice.getMonth());
			}
			arinvoice = arInvoiceDAO.store(existingArInvoice);
		} else {
		    ArInvoice ret = arInvoiceDAO.store(arinvoice);
            arinvoice.copy(ret);
		}
		arInvoiceDAO.flush();
		
		// save/update ar invoice receipt status
		ArInvoiceReceiptStatus arInvoiceReceiptStatus = arInvoiceReceiptStatusService.exstingArInvoiceReceiptStatus(arinvoice.getId());
		arInvoiceReceiptStatus.setArInvoiceId(arinvoice.getId());
		arInvoiceReceiptStatus.setRemainIncludeGstOriginalAmount(arinvoice.getIncludeGstOriginalAmount());
		arInvoiceReceiptStatus.setSettedIncludeGstOriginalAmount(BigDecimal.ZERO);
		arInvoiceReceiptStatusService.saveArInvoiceReceiptStatus(arInvoiceReceiptStatus);
	}
	
	/**
	 */
	@Transactional
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id) {
		return arInvoiceDAO.findArInvoiceByPrimaryKey(id);
	}

	/**
	 * Delete an existing ArInvoice entity
	 * 
	 */
	@Transactional
	public void deleteArInvoice(ArInvoice arinvoice) {
		arInvoiceDAO.remove(arinvoice);
		arInvoiceDAO.flush();
	}

	/**
	 * Return all ArInvoice entity
	 * 
	 */
	@Transactional
	public List<ArInvoice> findAllArInvoices(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArInvoice>(arInvoiceDAO.findAllArInvoices(startResult, maxRows));
	}


	/**
	 * Delete an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ArInvoice deleteArInvoiceArInvoiceAccountDetailses(BigInteger arinvoice_id, BigInteger related_arinvoiceaccountdetailses_id) {
		ArInvoiceAccountDetails related_arinvoiceaccountdetailses = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(related_arinvoiceaccountdetailses_id, -1, -1);

		ArInvoice arinvoice = arInvoiceDAO.findArInvoiceByPrimaryKey(arinvoice_id, -1, -1);

		related_arinvoiceaccountdetailses.setArInvoice(null);
		arinvoice.getArInvoiceAccountDetailses().remove(related_arinvoiceaccountdetailses);

		arInvoiceAccountDetailsDAO.remove(related_arinvoiceaccountdetailses);
		arInvoiceAccountDetailsDAO.flush();

		return arinvoice;
	}
	
	public String findARInvoiceJSON(Integer startpage, Integer endPage, String fromMonth, String toMonth,
			String fromArNo, String toArNo, String payerName, String invoiceNo, String status, Integer orderColumn, String orderBy){
		
		// list ar invoice no
		List<String> listArInvoiceNo = searchArInvoice(startpage, endPage, fromMonth, toMonth, invoiceNo, payerName, status, fromArNo, toArNo, orderColumn, orderBy);
		
		//
		List<ArInvoiceDTO> listArInvoice = new ArrayList<ArInvoiceDTO>();
		listArInvoice = searchARInvoiceDetail(listArInvoiceNo, orderColumn, orderBy);
		
		//set value of claim type.
		for(ArInvoiceDTO arInvoiceDTO : listArInvoice){
		    ClaimTypeMaster claimTypeMaster = claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(arInvoiceDTO.getClaimType());
		    arInvoiceDTO.setClaimTypeVal(claimTypeMaster.getName());
		}
		
		// ar invoice count
		BigInteger recordsFiltered = BigInteger.ZERO;
		recordsFiltered = countSearch(fromMonth, toMonth, invoiceNo, payerName, status, fromArNo, toArNo);
        
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listArInvoice);
        map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
		
		return jsonData;
	}
	
	public List<String> searchArInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
            String invoiceNo, String payerName, String status,  String fromArNo, String toArNo, Integer orderColumn, String orderBy) {
        List<ArInvoiceDTO> lstArinvoice = arInvoiceDAO.searchArInvoice(startResult, maxRow, fromMonth, toMonth, invoiceNo, payerName, status, fromArNo, toArNo, orderColumn, orderBy);
        
        List<String> list = new ArrayList<String>();
		for (Object data : lstArinvoice) {
			list.add(data.toString());
		}
        return list;
    }
	
	public List<ArInvoiceDTO> searchARInvoiceDetail(List<String> arInvoiceNoLst, Integer orderColumn, String orderBy){
		List<ArInvoiceDTO> lstArInvoiceReceipt = arInvoiceDAO.searchARInvoiceReceiptDetail(arInvoiceNoLst, orderColumn, orderBy);
        return lstArInvoiceReceipt;
	}

    public BigInteger countSearch(String fromMonth, String toMonth, String invoiceNo, String payerName, String status, String fromArNo, String toArNo) {
    	BigInteger count = arInvoiceDAO.searchCount(fromMonth, toMonth, invoiceNo, payerName, status, fromArNo, toArNo);
        return count;
    }
    
    public List<ARInvoiceDetail> getARInvoiceDetail(BigInteger arInvoiceId){
        return arInvoiceDAO.getARInvoiceDetail(arInvoiceId);
    }
    
    public BigInteger getLastId(){
        return arInvoiceDAO.getLastId();
    }
}
