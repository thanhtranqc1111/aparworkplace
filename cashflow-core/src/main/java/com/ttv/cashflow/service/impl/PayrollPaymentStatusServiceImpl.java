package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.PayrollPaymentStatusDAO;

import com.ttv.cashflow.domain.PayrollPaymentStatus;
import com.ttv.cashflow.service.PayrollPaymentStatusService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for PayrollPaymentStatus entities
 * 
 */

@Service("PayrollPaymentStatusService")

@Transactional
public abstract class PayrollPaymentStatusServiceImpl implements PayrollPaymentStatusService {

	/**
	 * DAO injected by Spring that manages PayrollPaymentStatus entities
	 * 
	 */
	@Autowired
	private PayrollPaymentStatusDAO payrollPaymentStatusDAO;

	/**
	 * Instantiates a new PayrollPaymentStatusServiceImpl.
	 *
	 */
	public PayrollPaymentStatusServiceImpl() {
	}

	/**
	 * Return a count of all PayrollPaymentStatus entity
	 * 
	 */
	@Transactional
	public Integer countPayrollPaymentStatuss() {
		return ((Long) payrollPaymentStatusDAO.createQuerySingleResult("select count(o) from PayrollPaymentStatus o").getSingleResult()).intValue();
	}

	/**
	 * Return all PayrollPaymentStatus entity
	 * 
	 */
	@Transactional
	public List<PayrollPaymentStatus> findAllPayrollPaymentStatuss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PayrollPaymentStatus>(payrollPaymentStatusDAO.findAllPayrollPaymentStatuss(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id) {
		return payrollPaymentStatusDAO.findPayrollPaymentStatusByPrimaryKey(id);
	}

	/**
	 * Save an existing PayrollPaymentStatus entity
	 * 
	 */
	@Transactional
	public void savePayrollPaymentStatus(PayrollPaymentStatus payrollpaymentstatus) {
		PayrollPaymentStatus existingPayrollPaymentStatus = payrollPaymentStatusDAO.findPayrollPaymentStatusByPrimaryKey(payrollpaymentstatus.getId());

		if (existingPayrollPaymentStatus != null) {
			if (existingPayrollPaymentStatus != payrollpaymentstatus) {
				existingPayrollPaymentStatus.setId(payrollpaymentstatus.getId());
				existingPayrollPaymentStatus.setPayrollId(payrollpaymentstatus.getPayrollId());
				existingPayrollPaymentStatus.setPaidNetPayment(payrollpaymentstatus.getPaidNetPayment());
				existingPayrollPaymentStatus.setUnpaidNetPayment(payrollpaymentstatus.getUnpaidNetPayment());
			}
			payrollpaymentstatus = payrollPaymentStatusDAO.store(existingPayrollPaymentStatus);
		} else {
			payrollpaymentstatus = payrollPaymentStatusDAO.store(payrollpaymentstatus);
		}
		payrollPaymentStatusDAO.flush();
	}

	/**
	 * Delete an existing PayrollPaymentStatus entity
	 * 
	 */
	@Transactional
	public void deletePayrollPaymentStatus(PayrollPaymentStatus payrollpaymentstatus) {
		payrollPaymentStatusDAO.remove(payrollpaymentstatus);
		payrollPaymentStatusDAO.flush();
	}

	/**
	 * Load an existing PayrollPaymentStatus entity
	 * 
	 */
	@Transactional
	public Set<PayrollPaymentStatus> loadPayrollPaymentStatuss() {
		return payrollPaymentStatusDAO.findAllPayrollPaymentStatuss();
	}
}
