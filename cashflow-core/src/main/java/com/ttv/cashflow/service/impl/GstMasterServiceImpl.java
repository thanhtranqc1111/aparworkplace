package com.ttv.cashflow.service.impl;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.GstMaster;
import com.ttv.cashflow.service.GstMasterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for GstMaster entities
 * 
 */

@Service("GstMasterService")

@Transactional
public class GstMasterServiceImpl implements GstMasterService {

	/**
	 * DAO injected by Spring that manages GstMaster entities
	 * 
	 */
	@Autowired
	private GstMasterDAO gstMasterDAO;

	/**
	 * Instantiates a new GstMasterServiceImpl.
	 *
	 */
	public GstMasterServiceImpl() {
	}

	/**
	 * Return a count of all GstMaster entity
	 * 
	 */
	@Transactional
	public Integer countGstMasters() {
		return ((Long) gstMasterDAO.createQuerySingleResult("select count(o) from GstMaster o").getSingleResult()).intValue();
	}

	/**
	 * Return all GstMaster entity
	 * 
	 */
	@Transactional
	public List<GstMaster> findAllGstMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<GstMaster>(gstMasterDAO.findAllGstMasters(startResult, maxRows));
	}

	/**
	 * Delete an existing GstMaster entity
	 * 
	 */
	@Transactional
	public void deleteGstMaster(GstMaster GstMaster) {
		gstMasterDAO.remove(GstMaster);
		gstMasterDAO.flush();
	}

	/**
	 * Load an existing GstMaster entity
	 * 
	 */
	@Transactional
	public Set<GstMaster> loadGstMasters() {
		return gstMasterDAO.findAllGstMasters();
	}

	/**
	 */
	@Transactional
	public GstMaster findGstMasterByPrimaryKey(String code) {
		return gstMasterDAO.findGstMasterByPrimaryKey(code);
	}

	/**
	 * Save an existing GstMaster entity
	 * 
	 */
	@Transactional
	public void saveGstMaster(GstMaster GstMaster) {
		GstMaster existingGstMaster = gstMasterDAO.findGstMasterByPrimaryKey(GstMaster.getCode());

		if (existingGstMaster != null) {
			if (existingGstMaster != GstMaster) {
				existingGstMaster.setCode(GstMaster.getCode());
				existingGstMaster.setType(GstMaster.getType());
				existingGstMaster.setRate(GstMaster.getRate());
			}
			GstMaster = gstMasterDAO.store(existingGstMaster);
		} else {
			GstMaster = gstMasterDAO.store(GstMaster);
		}
		gstMasterDAO.flush();
	}

	public String findGstMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
		// TODO Auto-generated method stub
		Integer recordsTotal = gstMasterDAO.countGstMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", gstMasterDAO.findGstMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", gstMasterDAO.countGstMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	public void deleteGstMasterByCodes(String[] codes) {
		// TODO Auto-generated method stub
		for (String code : codes) {
	        GstMaster GstMaster = gstMasterDAO.findGstMasterByCode(code);
	        if (GstMaster != null) {
	            deleteGstMaster(GstMaster);
	        }
	    }
	}
}
