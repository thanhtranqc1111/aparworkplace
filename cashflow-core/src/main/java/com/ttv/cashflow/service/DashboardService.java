
package com.ttv.cashflow.service;

import java.util.Calendar;
import java.util.List;

import com.ttv.cashflow.dto.DashboardChartOneDTO;

public interface DashboardService {
	public List<DashboardChartOneDTO> getListChartOne(Calendar fromMonth, Calendar toMonth);
}