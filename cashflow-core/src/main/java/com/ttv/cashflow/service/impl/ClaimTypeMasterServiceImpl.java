package com.ttv.cashflow.service.impl;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;

import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.service.ClaimTypeMasterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ClaimTypeMaster entities
 * 
 */

@Service("ClaimTypeMasterService")

@Transactional
public class ClaimTypeMasterServiceImpl implements ClaimTypeMasterService {

	/**
	 * DAO injected by Spring that manages ClaimTypeMaster entities
	 * 
	 */
	@Autowired
	private ClaimTypeMasterDAO claimTypeMasterDAO;

	/**
	 * Instantiates a new ClaimTypeMasterServiceImpl.
	 *
	 */
	public ClaimTypeMasterServiceImpl() {
	}

	/**
	 */
	@Transactional
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code) {
		return claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(code);
	}

	/**
	 * Return all ClaimTypeMaster entity
	 * 
	 */
	@Transactional
	public List<ClaimTypeMaster> findAllClaimTypeMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ClaimTypeMaster>(claimTypeMasterDAO.findAllClaimTypeMasters(startResult, maxRows));
	}

	/**
	 * Return a count of all ClaimTypeMaster entity
	 * 
	 */
	@Transactional
	public Integer countClaimTypeMasters() {
		return ((Long) claimTypeMasterDAO.createQuerySingleResult("select count(o) from ClaimTypeMaster o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ClaimTypeMaster entity
	 * 
	 */
	@Transactional
	public Set<ClaimTypeMaster> loadClaimTypeMasters() {
		return claimTypeMasterDAO.findAllClaimTypeMasters();
	}

	/**
	 * Delete an existing ClaimTypeMaster entity
	 * 
	 */
	@Transactional
	public void deleteClaimTypeMaster(ClaimTypeMaster claimtypemaster) {
		claimTypeMasterDAO.remove(claimtypemaster);
		claimTypeMasterDAO.flush();
	}

	/**
	 * Save an existing ClaimTypeMaster entity
	 * 
	 */
	@Transactional
	public void saveClaimTypeMaster(ClaimTypeMaster claimtypemaster) {
		ClaimTypeMaster existingClaimTypeMaster = claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(claimtypemaster.getCode());
		if (existingClaimTypeMaster != null) {
			if (existingClaimTypeMaster != claimtypemaster) {
				existingClaimTypeMaster.setCode(claimtypemaster.getCode());
				existingClaimTypeMaster.setName(claimtypemaster.getName());
			}
			claimtypemaster = claimTypeMasterDAO.store(existingClaimTypeMaster);
		} else {
			claimtypemaster = claimTypeMasterDAO.store(claimtypemaster);
		}
		claimTypeMasterDAO.flush();
	}

	public void deleteClaimTypeMasterByCodes(String[] codes) {
	    for (String code : codes) {
	        ClaimTypeMaster claimTypemaster = claimTypeMasterDAO.findClaimTypeMasterByCode(code);
	        if (claimTypemaster != null) {
	            deleteClaimTypeMaster(claimTypemaster);
	        }
	    }
	}

	public String findClaimTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
		Integer recordsTotal = claimTypeMasterDAO.countClaimTypeMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", claimTypeMasterDAO.findClaimTypeMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", claimTypeMasterDAO.countClaimTypeMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
