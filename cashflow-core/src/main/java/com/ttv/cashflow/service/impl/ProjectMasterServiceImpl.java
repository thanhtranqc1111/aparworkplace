package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.service.ProjectMasterService;

/**
 * Spring service that handles CRUD requests for ProjectMaster entities
 * 
 */

@Service("ProjectMasterService")

@Transactional
public class ProjectMasterServiceImpl implements ProjectMasterService {

    /**
     * DAO injected by Spring that manages ProjectMaster entities
     * 
     */
    @Autowired
    private ProjectMasterDAO projectMasterDAO;

    /**
     * Instantiates a new ProjectMasterServiceImpl.
     *
     */
    public ProjectMasterServiceImpl() {
    }

    /**
     * Load an existing ProjectMaster entity
     * 
     */
    @Transactional
    public Set<ProjectMaster> loadProjectMasters() {
        return projectMasterDAO.findAllProjectMasters();
    }

    /**
     */
    @Transactional
    public ProjectMaster findProjectMasterByPrimaryKey(String code) {
        return projectMasterDAO.findProjectMasterByPrimaryKey(code);
    }

    /**
     * Save an existing ProjectMaster entity
     * 
     */
    @Transactional
    public void saveProjectMaster(ProjectMaster projectmaster) {
        ProjectMaster existingProjectMaster = projectMasterDAO
                .findProjectMasterByPrimaryKey(projectmaster.getCode());

		if (existingProjectMaster != null) {
			existingProjectMaster.setCode(projectmaster.getCode());
			existingProjectMaster.setName(projectmaster.getName());
			existingProjectMaster.setBusinessTypeName(projectmaster.getBusinessTypeName());
			existingProjectMaster.setApprovalCode(projectmaster.getApprovalCode());
			existingProjectMaster.setCustomerName(projectmaster.getCustomerName());
			projectmaster = projectMasterDAO.store(existingProjectMaster);
		} else {
			projectmaster = projectMasterDAO.store(projectmaster);
		}
        projectMasterDAO.flush();
    }

    /**
     * Delete an existing ProjectMaster entity
     * 
     */
    @Transactional
    public void deleteProjectMaster(ProjectMaster projectmaster) {
        projectMasterDAO.remove(projectmaster);
        projectMasterDAO.flush();
    }

    /**
     * Return a count of all ProjectMaster entity
     * 
     */
    @Transactional
    public Integer countProjectMasters() {
        return ((Long) projectMasterDAO
                .createQuerySingleResult("select count(o) from ProjectMaster o")
                .getSingleResult()).intValue();
    }

    /**
     * Return all ProjectMaster entity
     * 
     */
    @Transactional
    public List<ProjectMaster> findAllProjectMasters(Integer startResult,
            Integer maxRows) {
        return new java.util.ArrayList<ProjectMaster>(
                projectMasterDAO.findAllProjectMasters(startResult, maxRows));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ttv.cashflow.service.ProjectMasterService#
     * findProjectMasterByCodeNameContaining(java.lang.String)
     */
    public Set<ProjectMaster> findProjectMasterByCodeNameContaining(
            String codeName) {
        return projectMasterDAO.findProjectMasterByCodeNameContaining(codeName);
    }

    @Transactional
    public String findProjectMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword) {
        Map<String, Object> map = new HashMap<>();
        List<ProjectMaster> listProjectMaster = projectMasterDAO.findProjectMasterPaging(start, end, orderColumn, orderBy, keyword);
        for(ProjectMaster p: listProjectMaster) {
        	p.setProjectAllocations(null);
        	p.setProjectPayrollDetailses(null);
        }
        map.put("data", listProjectMaster);
        map.put("recordsFiltered", projectMasterDAO.countProjectMasterFilter(keyword));

        Gson gson = new Gson();
        return gson.toJson(map);
    }

    @Transactional
    public void deleteProjectMasterByCodes(String[] codes) {
        for (String code : codes) {
            ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(code);
            if (projectMaster != null) {
                deleteProjectMaster(projectMaster);
            }
        }
        
    }

    @Override
    public boolean updateProjectMaster(ProjectMaster projectMaster) {
        ProjectMaster existingProjectMaster = projectMasterDAO.findProjectMasterByCode(projectMaster.getCode());

        if (existingProjectMaster != null) {
            existingProjectMaster.setCode(projectMaster.getCode());
            existingProjectMaster.setName(projectMaster.getName());
            existingProjectMaster.setBusinessTypeName(projectMaster.getBusinessTypeName());
            existingProjectMaster.setApprovalCode(projectMaster.getApprovalCode());
            existingProjectMaster.setCustomerName(projectMaster.getCustomerName());

            ProjectMaster divisionIsReturnedFromDb = projectMasterDAO.store(existingProjectMaster);
            projectMasterDAO.flush();

            return (divisionIsReturnedFromDb != null) ? true : false;
        }
        return false;
    }

    @Override
    public boolean createProjectMaster(ProjectMaster projectMaster) {
        ProjectMaster projectMasterIsReturnedFromDb = projectMasterDAO.store(projectMaster);
        projectMasterDAO.flush();
        return (projectMasterIsReturnedFromDb != null) ? true : false;
    }

    @Override
    public boolean checkExistingProjectMasterCode(String code) {
        return projectMasterDAO.checkExistingProjectMasterCode(code);
    }
}
