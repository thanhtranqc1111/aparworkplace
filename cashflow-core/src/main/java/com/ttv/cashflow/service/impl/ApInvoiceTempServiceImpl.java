package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ApInvoiceTempDAO;

import com.ttv.cashflow.domain.ApInvoiceTemp;
import com.ttv.cashflow.service.ApInvoiceTempService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ApInvoiceTemp entities
 * 
 */

@Service("ApInvoiceTempService")

@Transactional
public class ApInvoiceTempServiceImpl implements ApInvoiceTempService {

	/**
	 * DAO injected by Spring that manages ApInvoiceTemp entities
	 * 
	 */
	@Autowired
	private ApInvoiceTempDAO apInvoiceTempDAO;

	/**
	 * Instantiates a new ApInvoiceTempServiceImpl.
	 *
	 */
	public ApInvoiceTempServiceImpl() {
	}

	/**
	 * Save an existing ApInvoiceTemp entity
	 * 
	 */
	@Transactional
	public void saveApInvoiceTemp(ApInvoiceTemp apinvoicetemp) {
		ApInvoiceTemp existingApInvoiceTemp = apInvoiceTempDAO.findApInvoiceTempByPrimaryKey(apinvoicetemp.getId());

		if (existingApInvoiceTemp != null) {
			if (existingApInvoiceTemp != apinvoicetemp) {
				existingApInvoiceTemp.setId(apinvoicetemp.getId());
				existingApInvoiceTemp.setPayeeCode(apinvoicetemp.getPayeeCode());
				existingApInvoiceTemp.setPayeeName(apinvoicetemp.getPayeeName());
				existingApInvoiceTemp.setPayeeTypeName(apinvoicetemp.getPayeeTypeName());
				existingApInvoiceTemp.setInvoiceNo(apinvoicetemp.getInvoiceNo());
				existingApInvoiceTemp.setInvoiceIssuedDate(apinvoicetemp.getInvoiceIssuedDate());
				existingApInvoiceTemp.setMonth(apinvoicetemp.getMonth());
				existingApInvoiceTemp.setClaimType(apinvoicetemp.getClaimType());
				existingApInvoiceTemp.setGstType(apinvoicetemp.getGstType());
				existingApInvoiceTemp.setGstRate(apinvoicetemp.getGstRate());
				existingApInvoiceTemp.setOriginalCurrency(apinvoicetemp.getOriginalCurrency());
				existingApInvoiceTemp.setFxRate(apinvoicetemp.getFxRate());
				existingApInvoiceTemp.setAccountPayableCode(apinvoicetemp.getAccountPayableCode());
				existingApInvoiceTemp.setAccountPayableName(apinvoicetemp.getAccountPayableName());
				existingApInvoiceTemp.setPaymentTerm(apinvoicetemp.getPaymentTerm());
				existingApInvoiceTemp.setScheduledPaymentDate(apinvoicetemp.getScheduledPaymentDate());
				existingApInvoiceTemp.setDescription(apinvoicetemp.getDescription());
				existingApInvoiceTemp.setAccountCode(apinvoicetemp.getAccountCode());
				existingApInvoiceTemp.setAccountName(apinvoicetemp.getAccountName());
				existingApInvoiceTemp.setAccountType(apinvoicetemp.getAccountType());
				existingApInvoiceTemp.setProjectCode(apinvoicetemp.getProjectCode());
				existingApInvoiceTemp.setProjectName(apinvoicetemp.getProjectName());
				existingApInvoiceTemp.setProjectDescription(apinvoicetemp.getProjectDescription());
				existingApInvoiceTemp.setPoNo(apinvoicetemp.getPoNo());
				existingApInvoiceTemp.setApprovalCode(apinvoicetemp.getApprovalCode());
				existingApInvoiceTemp.setExcludeGstOriginalAmount(apinvoicetemp.getExcludeGstOriginalAmount());
				existingApInvoiceTemp.setIsApproval(apinvoicetemp.getIsApproval());
				existingApInvoiceTemp.setTotalExcludeGstOriginalAmount(apinvoicetemp.getTotalExcludeGstOriginalAmount());
				existingApInvoiceTemp.setTotalIncludedGstConvertedAmount(apinvoicetemp.getTotalIncludedGstConvertedAmount());
				existingApInvoiceTemp.setIncludedGstConvertedAmount(apinvoicetemp.getIncludedGstConvertedAmount());
				
			}
			apinvoicetemp = apInvoiceTempDAO.store(existingApInvoiceTemp);
		} else {
			apinvoicetemp = apInvoiceTempDAO.store(apinvoicetemp);
		}
		apInvoiceTempDAO.flush();
	}

	/**
	 * Return all ApInvoiceTemp entity
	 * 
	 */
	@Transactional
	public List<ApInvoiceTemp> findAllApInvoiceTemps(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoiceTemp>(apInvoiceTempDAO.findAllApInvoiceTemps(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id) {
		return apInvoiceTempDAO.findApInvoiceTempByPrimaryKey(id);
	}

	/**
	 * Return a count of all ApInvoiceTemp entity
	 * 
	 */
	@Transactional
	public Integer countApInvoiceTemps() {
		return ((Long) apInvoiceTempDAO.createQuerySingleResult("select count(o) from ApInvoiceTemp o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing ApInvoiceTemp entity
	 * 
	 */
	@Transactional
	public void deleteApInvoiceTemp(ApInvoiceTemp apinvoicetemp) {
		apInvoiceTempDAO.remove(apinvoicetemp);
		apInvoiceTempDAO.flush();
	}

	/**
	 * Load an existing ApInvoiceTemp entity
	 * 
	 */
	@Transactional
	public Set<ApInvoiceTemp> loadApInvoiceTemps() {
		return apInvoiceTempDAO.findAllApInvoiceTemps();
	}
}
