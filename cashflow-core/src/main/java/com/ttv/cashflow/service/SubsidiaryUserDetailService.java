
package com.ttv.cashflow.service;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.domain.UserAccount;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

/**
 * Spring service that handles CRUD requests for SubsidiaryUserDetail entities
 * 
 */
public interface SubsidiaryUserDetailService {

	/**
	* Delete an existing Subsidiary entity
	* 
	 */
	public SubsidiaryUserDetail deleteSubsidiaryUserDetailSubsidiary(Integer subsidiaryuserdetail_id, Integer related_subsidiary_id);

	/**
	* Save an existing SubsidiaryUserDetail entity
	* 
	 */
	public void saveSubsidiaryUserDetail(SubsidiaryUserDetail subsidiaryuserdetail);

	/**
	* Delete an existing SubsidiaryUserDetail entity
	* 
	 */
	public void deleteSubsidiaryUserDetail(SubsidiaryUserDetail subsidiaryuserdetail_1);

	/**
	* Save an existing Subsidiary entity
	* 
	 */
	public SubsidiaryUserDetail saveSubsidiaryUserDetailSubsidiary(Integer id, Subsidiary related_subsidiary);

	/**
	* Delete an existing UserAccount entity
	* 
	 */
	public SubsidiaryUserDetail deleteSubsidiaryUserDetailUserAccount(Integer subsidiaryuserdetail_id_1, Integer related_useraccount_id);

	/**
	* Load an existing SubsidiaryUserDetail entity
	* 
	 */
	public Set<SubsidiaryUserDetail> loadSubsidiaryUserDetails();

	/**
	 */
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id_1);

	/**
	* Save an existing UserAccount entity
	* 
	 */
	public SubsidiaryUserDetail saveSubsidiaryUserDetailUserAccount(Integer id_2, UserAccount related_useraccount);

	/**
	* Return all SubsidiaryUserDetail entity
	* 
	 */
	public List<SubsidiaryUserDetail> findAllSubsidiaryUserDetails(Integer startResult, Integer maxRows);

	/**
	* Return a count of all SubsidiaryUserDetail entity
	* 
	 */
	public Integer countSubsidiaryUserDetails();
	
	public Map<String, Integer> getSubsidiaryUserMap();
	public int saveSubsidiaryUserDetail(JsonArray jsonArray);
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByUserId(Integer userId);
}