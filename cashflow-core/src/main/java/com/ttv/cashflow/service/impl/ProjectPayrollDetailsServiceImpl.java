package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.ttv.cashflow.dao.PayrollDetailsDAO;
import com.ttv.cashflow.dao.ProjectAllocationDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.dao.ProjectPayrollDetailsDAO;
import com.ttv.cashflow.domain.PayrollDetails;
import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.domain.ProjectPayrollDetails;
import com.ttv.cashflow.dto.PaymentEmployeeDTO;
import com.ttv.cashflow.dto.ProjectPaymentDTO;
import com.ttv.cashflow.dto.ProjectPaymentDetail;
import com.ttv.cashflow.dto.ProjectPaymentCollectorDTO;
import com.ttv.cashflow.service.ProjectPayrollDetailsService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for ProjectPayrollDetails entities
 * 
 */

@Service("ProjectPayrollDetailsService")

@Transactional
public class ProjectPayrollDetailsServiceImpl implements ProjectPayrollDetailsService {

	/**
	 * DAO injected by Spring that manages PayrollDetails entities
	 * 
	 */
	@Autowired
	private PayrollDetailsDAO payrollDetailsDAO;

	/**
	 * DAO injected by Spring that manages ProjectMaster entities
	 * 
	 */
	@Autowired
	private ProjectMasterDAO projectMasterDAO;

	/**
	 * DAO injected by Spring that manages ProjectPayrollDetails entities
	 * 
	 */
	@Autowired
	private ProjectPayrollDetailsDAO projectPayrollDetailsDAO;
	
	@Autowired
	private ProjectAllocationDAO projectAllocationDAO;

	/**
	 * Instantiates a new ProjectPayrollDetailsServiceImpl.
	 *
	 */
	public ProjectPayrollDetailsServiceImpl() {
	}

	/**
	 * Return all ProjectPayrollDetails entity
	 * 
	 */
	@Transactional
	public List<ProjectPayrollDetails> findAllProjectPayrollDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ProjectPayrollDetails>(projectPayrollDetailsDAO.findAllProjectPayrollDetailss(startResult, maxRows));
	}

	/**
	 * Save an existing ProjectPayrollDetails entity
	 * 
	 */
	@Transactional
	public void saveProjectPayrollDetails(ProjectPayrollDetails projectpayrolldetails) {
		ProjectPayrollDetails existingProjectPayrollDetails = projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(projectpayrolldetails.getId());

		if (existingProjectPayrollDetails != null) {
			if (existingProjectPayrollDetails != projectpayrolldetails) {
				existingProjectPayrollDetails.setId(projectpayrolldetails.getId());
				existingProjectPayrollDetails.setProjectAllocationAmount(projectpayrolldetails.getProjectAllocationAmount());
				existingProjectPayrollDetails.setCreatedDate(projectpayrolldetails.getCreatedDate());
				existingProjectPayrollDetails.setModifiedDate(projectpayrolldetails.getModifiedDate());
			}
			projectpayrolldetails = projectPayrollDetailsDAO.store(existingProjectPayrollDetails);
		} else {
			projectpayrolldetails = projectPayrollDetailsDAO.store(projectpayrolldetails);
		}
		projectPayrollDetailsDAO.flush();
	}

	/**
	 * Delete an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public ProjectPayrollDetails deleteProjectPayrollDetailsPayrollDetails(BigInteger projectpayrolldetails_id, BigInteger related_payrolldetails_id) {
		ProjectPayrollDetails projectpayrolldetails = projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(projectpayrolldetails_id, -1, -1);
		PayrollDetails related_payrolldetails = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(related_payrolldetails_id, -1, -1);

		projectpayrolldetails.setPayrollDetails(null);
		related_payrolldetails.getProjectPayrollDetailses().remove(projectpayrolldetails);
		projectpayrolldetails = projectPayrollDetailsDAO.store(projectpayrolldetails);
		projectPayrollDetailsDAO.flush();

		related_payrolldetails = payrollDetailsDAO.store(related_payrolldetails);
		payrollDetailsDAO.flush();

		payrollDetailsDAO.remove(related_payrolldetails);
		payrollDetailsDAO.flush();

		return projectpayrolldetails;
	}

	/**
	 * Delete an existing ProjectPayrollDetails entity
	 * 
	 */
	@Transactional
	public void deleteProjectPayrollDetails(ProjectPayrollDetails projectpayrolldetails) {
		projectPayrollDetailsDAO.remove(projectpayrolldetails);
		projectPayrollDetailsDAO.flush();
	}

	/**
	 * Return a count of all ProjectPayrollDetails entity
	 * 
	 */
	@Transactional
	public Integer countProjectPayrollDetailss() {
		return ((Long) projectPayrollDetailsDAO.createQuerySingleResult("select count(o) from ProjectPayrollDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public ProjectPayrollDetails saveProjectPayrollDetailsPayrollDetails(BigInteger id, PayrollDetails related_payrolldetails) {
		ProjectPayrollDetails projectpayrolldetails = projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(id, -1, -1);
		PayrollDetails existingpayrollDetails = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(related_payrolldetails.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpayrollDetails != null) {
			existingpayrollDetails.setId(related_payrolldetails.getId());
//			existingpayrollDetails.setPayrollId(related_payrolldetails.getPayrollId());
			existingpayrollDetails.setEmployeeCode(related_payrolldetails.getEmployeeCode());
			existingpayrollDetails.setRoleTitle(related_payrolldetails.getRoleTitle());
			existingpayrollDetails.setDivision(related_payrolldetails.getDivision());
			existingpayrollDetails.setTeam(related_payrolldetails.getTeam());
			existingpayrollDetails.setLaborBaseSalaryPayment(related_payrolldetails.getLaborBaseSalaryPayment());
			existingpayrollDetails.setLaborAdditionalPayment1(related_payrolldetails.getLaborAdditionalPayment1());
			existingpayrollDetails.setLaborAdditionalPayment2(related_payrolldetails.getLaborAdditionalPayment2());
			existingpayrollDetails.setLaborAdditionalPayment3(related_payrolldetails.getLaborAdditionalPayment3());
			existingpayrollDetails.setLaborSocialInsuranceEmployer1(related_payrolldetails.getLaborSocialInsuranceEmployer1());
			existingpayrollDetails.setLaborSocialInsuranceEmployer2(related_payrolldetails.getLaborSocialInsuranceEmployer2());
			existingpayrollDetails.setLaborSocialInsuranceEmployer3(related_payrolldetails.getLaborSocialInsuranceEmployer3());
			existingpayrollDetails.setLaborOtherPayment1(related_payrolldetails.getLaborOtherPayment1());
			existingpayrollDetails.setLaborOtherPayment2(related_payrolldetails.getLaborOtherPayment2());
			existingpayrollDetails.setLaborOtherPayment3(related_payrolldetails.getLaborOtherPayment3());
			existingpayrollDetails.setTotalLaborCostOriginal(related_payrolldetails.getTotalLaborCostOriginal());
			existingpayrollDetails.setDeductionSocialInsuranceEmployer1(related_payrolldetails.getDeductionSocialInsuranceEmployer1());
			existingpayrollDetails.setDeductionSocialInsuranceEmployer2(related_payrolldetails.getDeductionSocialInsuranceEmployer2());
			existingpayrollDetails.setDeductionSocialInsuranceEmployer3(related_payrolldetails.getDeductionSocialInsuranceEmployer3());
			existingpayrollDetails.setDeductionSocialInsuranceEmployee1(related_payrolldetails.getDeductionSocialInsuranceEmployee1());
			existingpayrollDetails.setDeductionSocialInsuranceEmployee2(related_payrolldetails.getDeductionSocialInsuranceEmployee2());
			existingpayrollDetails.setDeductionSocialInsuranceEmployee3(related_payrolldetails.getDeductionSocialInsuranceEmployee3());
			existingpayrollDetails.setDeductionWht(related_payrolldetails.getDeductionWht());
			existingpayrollDetails.setDeductionOther1(related_payrolldetails.getDeductionOther1());
			existingpayrollDetails.setDeductionOther2(related_payrolldetails.getDeductionOther2());
			existingpayrollDetails.setDeductionOther3(related_payrolldetails.getDeductionOther3());
			existingpayrollDetails.setTotalDeductionOriginal(related_payrolldetails.getTotalDeductionOriginal());
			existingpayrollDetails.setNetPaymentOriginal(related_payrolldetails.getNetPaymentOriginal());
			existingpayrollDetails.setNetPaymentConverted(related_payrolldetails.getNetPaymentConverted());
			existingpayrollDetails.setApprovalCode(related_payrolldetails.getApprovalCode());
			existingpayrollDetails.setIsApproval(related_payrolldetails.getIsApproval());
			existingpayrollDetails.setCreatedDate(related_payrolldetails.getCreatedDate());
			existingpayrollDetails.setModifiedDate(related_payrolldetails.getModifiedDate());
			related_payrolldetails = existingpayrollDetails;
		} else {
			related_payrolldetails = payrollDetailsDAO.store(related_payrolldetails);
			payrollDetailsDAO.flush();
		}

		projectpayrolldetails.setPayrollDetails(related_payrolldetails);
		related_payrolldetails.getProjectPayrollDetailses().add(projectpayrolldetails);
		projectpayrolldetails = projectPayrollDetailsDAO.store(projectpayrolldetails);
		projectPayrollDetailsDAO.flush();

		related_payrolldetails = payrollDetailsDAO.store(related_payrolldetails);
		payrollDetailsDAO.flush();

		return projectpayrolldetails;
	}

	/**
	 */
	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id) {
		return projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(id);
	}

	/**
	 * Load an existing ProjectPayrollDetails entity
	 * 
	 */
	@Transactional
	public Set<ProjectPayrollDetails> loadProjectPayrollDetailss() {
		return projectPayrollDetailsDAO.findAllProjectPayrollDetailss();
	}

	/**
	 * Save an existing ProjectMaster entity
	 * 
	 */
	@Transactional
	public ProjectPayrollDetails saveProjectPayrollDetailsProjectMaster(BigInteger id, ProjectMaster related_projectmaster) {
		ProjectPayrollDetails projectpayrolldetails = projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(id, -1, -1);
		ProjectMaster existingprojectMaster = projectMasterDAO.findProjectMasterByPrimaryKey(related_projectmaster.getCode());

		// copy into the existing record to preserve existing relationships
		if (existingprojectMaster != null) {
			existingprojectMaster.setCode(related_projectmaster.getCode());
			existingprojectMaster.setName(related_projectmaster.getName());
			existingprojectMaster.setBusinessTypeName(related_projectmaster.getBusinessTypeName());
			existingprojectMaster.setApprovalCode(related_projectmaster.getApprovalCode());
			existingprojectMaster.setCustomerName(related_projectmaster.getCustomerName());
			existingprojectMaster.setIsActive(related_projectmaster.getIsActive());
			related_projectmaster = existingprojectMaster;
		} else {
			related_projectmaster = projectMasterDAO.store(related_projectmaster);
			projectMasterDAO.flush();
		}

		projectpayrolldetails.setProjectMaster(related_projectmaster);
		related_projectmaster.getProjectPayrollDetailses().add(projectpayrolldetails);
		projectpayrolldetails = projectPayrollDetailsDAO.store(projectpayrolldetails);
		projectPayrollDetailsDAO.flush();

		related_projectmaster = projectMasterDAO.store(related_projectmaster);
		projectMasterDAO.flush();

		return projectpayrolldetails;
	}

	/**
	 * Delete an existing ProjectMaster entity
	 * 
	 */
	@Transactional
	public ProjectPayrollDetails deleteProjectPayrollDetailsProjectMaster(BigInteger projectpayrolldetails_id, String related_projectmaster_code) {
		ProjectPayrollDetails projectpayrolldetails = projectPayrollDetailsDAO.findProjectPayrollDetailsByPrimaryKey(projectpayrolldetails_id, -1, -1);
		ProjectMaster related_projectmaster = projectMasterDAO.findProjectMasterByPrimaryKey(related_projectmaster_code, -1, -1);

		projectpayrolldetails.setProjectMaster(null);
		related_projectmaster.getProjectPayrollDetailses().remove(projectpayrolldetails);
		projectpayrolldetails = projectPayrollDetailsDAO.store(projectpayrolldetails);
		projectPayrollDetailsDAO.flush();

		related_projectmaster = projectMasterDAO.store(related_projectmaster);
		projectMasterDAO.flush();

		projectMasterDAO.remove(related_projectmaster);
		projectMasterDAO.flush();

		return projectpayrolldetails;
	}

	@Override
	public List<ProjectPaymentDTO> getProjectPaymentDTOLst(java.util.Calendar monthFrom, java.util.Calendar monthTo) {
		// initialize
		List<ProjectPaymentDTO> projectPaymentLst = new ArrayList<ProjectPaymentDTO>();
		ProjectPaymentDTO projectPaymentInfor;
		
		// Loop from monthFrom to monthTo
		while (monthFrom.compareTo(monthTo) <= 0) {
	
			// get Project Payment Entity
			projectPaymentInfor = new ProjectPaymentDTO();
			projectPaymentInfor = getProjectPaymentEntity(monthFrom);
			
			// if Project Payment Entity is null then continue loop
			if(projectPaymentInfor == null) {
				monthFrom.add(Calendar.MONTH, 01);
				continue;
			}

			// add entity to list and continue loop
			projectPaymentLst.add(projectPaymentInfor);
			monthFrom.add(Calendar.MONTH, 01);
		}

		return projectPaymentLst;
	}
	
	/**
	 * get project payment entity
	 * 
	 */
	private ProjectPaymentDTO getProjectPaymentEntity(java.util.Calendar month) {
		// initialize
		ProjectPaymentDTO projectPaymentInfor = new ProjectPaymentDTO();

		Map<String, String> mapProjectCodeAndProjectName = new LinkedHashMap<>();
		List<PaymentEmployeeDTO> paymentEmployeeDTOLst = new ArrayList<PaymentEmployeeDTO>();
		PaymentEmployeeDTO paymentEmployee;
		String employeeCodeOld ="";

		// get List project Payment Detail from DB
		Iterator<ProjectPaymentDetail> projectPaymentDetailIterator = projectAllocationDAO.getListPaymentProjectByMonth(new SimpleDateFormat("MM/yyyy").format(month.getTime())).iterator();
		Map<String, BigDecimal> mapProjectAssignedSalary = new LinkedHashMap<>();

		while(projectPaymentDetailIterator.hasNext()) {
			ProjectPaymentDetail projectPaymentDetai = projectPaymentDetailIterator.next();

			//other employee Code
			if (!employeeCodeOld.equals(projectPaymentDetai.getEmployeeCode())) {
				// init entity
				employeeCodeOld = projectPaymentDetai.getEmployeeCode();
				paymentEmployee = new PaymentEmployeeDTO();
				mapProjectAssignedSalary = new LinkedHashMap<>();

				// add Project Payment Detail to Payment Employee Entity
				paymentEmployee.setEmployeeCode(projectPaymentDetai.getEmployeeCode());
				paymentEmployee.setEmployeeName(projectPaymentDetai.getEmployeeName());
				paymentEmployee.setEmployeeSalary(projectPaymentDetai.getEmployeeSalary());
				mapProjectAssignedSalary.put(projectPaymentDetai.getProjectCode(), projectPaymentDetai.getProjectAssignedSalary());
				paymentEmployee.setMapProjectAssignedSalary(mapProjectAssignedSalary);

				// add Payment Employee Entity to List
				paymentEmployeeDTOLst.add(paymentEmployee);
			} else {
				mapProjectAssignedSalary.put(projectPaymentDetai.getProjectCode(), projectPaymentDetai.getProjectAssignedSalary());
			}
			
			// put code and name of project Payment in month into map
			if (!mapProjectCodeAndProjectName.containsKey(projectPaymentDetai.getProjectCode())) {
				mapProjectCodeAndProjectName.put(projectPaymentDetai.getProjectCode(), projectPaymentDetai.getProjectName());
			}
		}

		// add information into entity
		if (!CollectionUtils.isEmpty(mapProjectCodeAndProjectName) && !CollectionUtils.isEmpty(paymentEmployeeDTOLst)) {
			projectPaymentInfor.setMonth(new SimpleDateFormat(Constant.MMM_YYYY).format(month.getTime()));
			projectPaymentInfor.setMapProjectCodeAndProjectName(mapProjectCodeAndProjectName);
			projectPaymentInfor.setListPaymentEmployeeDTO(paymentEmployeeDTOLst);
		} else {
			return null;
		}

		return projectPaymentInfor;
	}

	@Override
	public void saveDataProjectPayroll(java.util.Calendar monthFrom, java.util.Calendar monthTo) throws Exception {
		// initialize
		ProjectPayrollDetails projectPayrollDetails;

		// get list project allocation
		Set<ProjectAllocation> setAllocation = projectAllocationDAO.findProjectAllocationByRangeMonth(monthFrom, monthTo);

		//get set employee codes
		Set<String> empCodes = setAllocation.stream().map(e -> e.getEmployeeMaster().getCode()).collect(Collectors.toSet());

		//get all payroll details from employees code, month from, month to
		if (CollectionUtils.isEmpty(empCodes)){
			return;
		}
		Set<PayrollDetails> payrollDetailAll = payrollDetailsDAO.findPayrollDetailsByEmployeeCodeAndMonth(empCodes, monthFrom, monthTo);

		//convert set to map
		Map<ProjectPaymentCollectorDTO, Set<PayrollDetails>> mapEmpPayrollDetails = payrollDetailAll.stream()
																							.collect(Collectors.groupingBy(
																									 	p -> new ProjectPaymentCollectorDTO(p.getEmployeeCode(), p.getPayroll().getMonth()),
																									 	Collectors.toSet()));
		// loop list project allocation
		Iterator<ProjectAllocation> projectAllocationIterator = setAllocation.iterator();
		while(projectAllocationIterator.hasNext()) {
			ProjectAllocation projectAllocation = projectAllocationIterator.next();
			
			// get payroll details
			if (mapEmpPayrollDetails.get(new ProjectPaymentCollectorDTO(projectAllocation.getEmployeeMaster().getCode(), 
																		projectAllocation.getMonth())) == null) {
				continue;
			}

			// loop list payroll details
			Iterator<PayrollDetails> payrollDetailsIterator = mapEmpPayrollDetails.get(new ProjectPaymentCollectorDTO(projectAllocation.getEmployeeMaster().getCode(),
																													  projectAllocation.getMonth())).iterator();
			while(payrollDetailsIterator.hasNext()) {
				PayrollDetails payrollDetails = payrollDetailsIterator.next();
				
				// check exist project_code and payroll_details_id on projectPayrollDetails
				ProjectPayrollDetails projectPayrollDetailsOnDB = projectPayrollDetailsDAO
						.findProjectPayrollDetailsByProjectCodeAndPayrollDetailId(projectAllocation.getProjectMaster().getCode(), payrollDetails.getId());
				
				// if exist
				if(projectPayrollDetailsOnDB != null) {
					// save projectPayrollDetails when projectAllocation was change or projectPayrollDetails was change
					if (projectAllocation.getModifiedDate() != null && payrollDetails.getModifiedDate() != null && projectPayrollDetailsOnDB.getModifiedDate() != null
							&& projectPayrollDetailsOnDB.getModifiedDate().compareTo(projectAllocation.getModifiedDate()) > 0
							&& projectPayrollDetailsOnDB.getModifiedDate().compareTo(payrollDetails.getModifiedDate()) > 0) {
						continue;
					}
					projectPayrollDetails = projectPayrollDetailsOnDB;

				} else {
					projectPayrollDetails = new ProjectPayrollDetails();
					projectPayrollDetails.setCreatedDate(Calendar.getInstance());
					projectPayrollDetails.setPayrollDetails(payrollDetails);
					projectPayrollDetails.setProjectMaster(projectAllocation.getProjectMaster());
				}

				// calculate Salary for Project Assigned
				BigDecimal paymentAmount = payrollDetails.getNetPaymentConverted(); 
				if (paymentAmount == null || projectAllocation.getPercentAllocation() == null) {
					continue;
				}
				BigDecimal projectAllocationAmount = paymentAmount.multiply(projectAllocation.getPercentAllocation()).divide(BigDecimal.valueOf(100));
				projectPayrollDetails.setProjectAllocationAmount(projectAllocationAmount);
				projectPayrollDetails.setModifiedDate(Calendar.getInstance());

				// save Project payroll details
				projectPayrollDetails = projectPayrollDetailsDAO.store(projectPayrollDetails);
				projectPayrollDetailsDAO.flush();
			}
		}
	}
}
