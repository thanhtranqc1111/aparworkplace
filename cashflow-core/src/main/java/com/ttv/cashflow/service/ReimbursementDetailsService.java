
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementDetails;

/**
 * Spring service that handles CRUD requests for ReimbursementDetails entities
 * 
 */
public interface ReimbursementDetailsService {

	/**
	* Load an existing ReimbursementDetails entity
	* 
	 */
	public Set<ReimbursementDetails> loadReimbursementDetailss();

	/**
	* Return all ReimbursementDetails entity
	* 
	 */
	public List<ReimbursementDetails> findAllReimbursementDetailss(Integer startResult, Integer maxRows);

	/**
	 */
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id);

	/**
	* Delete an existing Reimbursement entity
	* 
	 */
	public ReimbursementDetails deleteReimbursementDetailsReimbursement(BigInteger reimbursementdetails_id, BigInteger related_reimbursement_id);

	/**
	* Return a count of all ReimbursementDetails entity
	* 
	 */
	public Integer countReimbursementDetailss();

	/**
	* Save an existing Reimbursement entity
	* 
	 */
	public ReimbursementDetails saveReimbursementDetailsReimbursement(BigInteger id_1, Reimbursement related_reimbursement);

	/**
	* Save an existing ReimbursementDetails entity
	* 
	 */
	public void saveReimbursementDetails(ReimbursementDetails reimbursementdetails);

	/**
	* Delete an existing ReimbursementDetails entity
	* 
	 */
	public void deleteReimbursementDetails(ReimbursementDetails reimbursementdetails_1);
}