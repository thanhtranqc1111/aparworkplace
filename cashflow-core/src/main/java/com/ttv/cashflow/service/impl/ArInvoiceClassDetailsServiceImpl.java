package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ArInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ArInvoiceClassDetailsDAO;

import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;
import com.ttv.cashflow.service.ArInvoiceClassDetailsService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ArInvoiceClassDetails entities
 * 
 */

@Service("ArInvoiceClassDetailsService")

@Transactional
public class ArInvoiceClassDetailsServiceImpl implements ArInvoiceClassDetailsService {

	/**
	 * DAO injected by Spring that manages ArInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ArInvoiceAccountDetailsDAO arInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ArInvoiceClassDetails entities
	 * 
	 */
	@Autowired
	private ArInvoiceClassDetailsDAO arInvoiceClassDetailsDAO;

	/**
	 * Instantiates a new ArInvoiceClassDetailsServiceImpl.
	 *
	 */
	public ArInvoiceClassDetailsServiceImpl() {
	}

	/**
	 */
	@Transactional
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id) {
		return arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(id);
	}

	/**
	 * Delete an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ArInvoiceClassDetails deleteArInvoiceClassDetailsArInvoiceAccountDetails(BigInteger arinvoiceclassdetails_id, BigInteger related_arinvoiceaccountdetails_id) {
		ArInvoiceClassDetails arinvoiceclassdetails = arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(arinvoiceclassdetails_id, -1, -1);
		ArInvoiceAccountDetails related_arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(related_arinvoiceaccountdetails_id, -1, -1);

		arinvoiceclassdetails.setArInvoiceAccountDetails(null);
		related_arinvoiceaccountdetails.getArInvoiceClassDetailses().remove(arinvoiceclassdetails);
		arinvoiceclassdetails = arInvoiceClassDetailsDAO.store(arinvoiceclassdetails);
		arInvoiceClassDetailsDAO.flush();

		related_arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(related_arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		arInvoiceAccountDetailsDAO.remove(related_arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		return arinvoiceclassdetails;
	}

	/**
	 * Return a count of all ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public Integer countArInvoiceClassDetailss() {
		return ((Long) arInvoiceClassDetailsDAO.createQuerySingleResult("select count(o) from ArInvoiceClassDetails o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> loadArInvoiceClassDetailss() {
		return arInvoiceClassDetailsDAO.findAllArInvoiceClassDetailss();
	}

	/**
	 * Return all ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public List<ArInvoiceClassDetails> findAllArInvoiceClassDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArInvoiceClassDetails>(arInvoiceClassDetailsDAO.findAllArInvoiceClassDetailss(startResult, maxRows));
	}

	/**
	 * Save an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ArInvoiceClassDetails saveArInvoiceClassDetailsArInvoiceAccountDetails(BigInteger id, ArInvoiceAccountDetails related_arinvoiceaccountdetails) {
		ArInvoiceClassDetails arinvoiceclassdetails = arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(id, -1, -1);
		ArInvoiceAccountDetails existingarInvoiceAccountDetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(related_arinvoiceaccountdetails.getId());

		// copy into the existing record to preserve existing relationships
		if (existingarInvoiceAccountDetails != null) {
			existingarInvoiceAccountDetails.setId(related_arinvoiceaccountdetails.getId());
			existingarInvoiceAccountDetails.setAccountCode(related_arinvoiceaccountdetails.getAccountCode());
			existingarInvoiceAccountDetails.setAccountName(related_arinvoiceaccountdetails.getAccountName());
			existingarInvoiceAccountDetails.setExcludeGstOriginalAmount(related_arinvoiceaccountdetails.getExcludeGstOriginalAmount());
			existingarInvoiceAccountDetails.setExcludeGstConvertedAmount(related_arinvoiceaccountdetails.getExcludeGstConvertedAmount());
			existingarInvoiceAccountDetails.setGstOriginalAmount(related_arinvoiceaccountdetails.getGstOriginalAmount());
			existingarInvoiceAccountDetails.setGstConvertedAmount(related_arinvoiceaccountdetails.getGstConvertedAmount());
			existingarInvoiceAccountDetails.setIncludeGstOriginalAmount(related_arinvoiceaccountdetails.getIncludeGstOriginalAmount());
			existingarInvoiceAccountDetails.setIncludeGstConvertedAmount(related_arinvoiceaccountdetails.getIncludeGstConvertedAmount());
			existingarInvoiceAccountDetails.setCreatedDate(related_arinvoiceaccountdetails.getCreatedDate());
			existingarInvoiceAccountDetails.setModifiedDate(related_arinvoiceaccountdetails.getModifiedDate());
			related_arinvoiceaccountdetails = existingarInvoiceAccountDetails;
		} else {
			related_arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(related_arinvoiceaccountdetails);
			arInvoiceAccountDetailsDAO.flush();
		}

		arinvoiceclassdetails.setArInvoiceAccountDetails(related_arinvoiceaccountdetails);
		related_arinvoiceaccountdetails.getArInvoiceClassDetailses().add(arinvoiceclassdetails);
		arinvoiceclassdetails = arInvoiceClassDetailsDAO.store(arinvoiceclassdetails);
		arInvoiceClassDetailsDAO.flush();

		related_arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(related_arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		return arinvoiceclassdetails;
	}

	/**
	 * Delete an existing ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public void deleteArInvoiceClassDetails(ArInvoiceClassDetails arinvoiceclassdetails) {
		arInvoiceClassDetailsDAO.remove(arinvoiceclassdetails);
		arInvoiceClassDetailsDAO.flush();
	}

	/**
	 * Save an existing ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public void saveArInvoiceClassDetails(ArInvoiceClassDetails arinvoiceclassdetails) {
		ArInvoiceClassDetails existingArInvoiceClassDetails = arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(arinvoiceclassdetails.getId());

		if (existingArInvoiceClassDetails != null) {
			if (existingArInvoiceClassDetails != arinvoiceclassdetails) {
				existingArInvoiceClassDetails.setId(arinvoiceclassdetails.getId());
				existingArInvoiceClassDetails.setClassCode(arinvoiceclassdetails.getClassCode());
				existingArInvoiceClassDetails.setClassName(arinvoiceclassdetails.getClassName());
				existingArInvoiceClassDetails.setApprovalCode(arinvoiceclassdetails.getApprovalCode());
				existingArInvoiceClassDetails.setDescription(arinvoiceclassdetails.getDescription());
				existingArInvoiceClassDetails.setExcludeGstOriginalAmount(arinvoiceclassdetails.getExcludeGstOriginalAmount());
				existingArInvoiceClassDetails.setExcludeGstConvertedAmount(arinvoiceclassdetails.getExcludeGstConvertedAmount());
				existingArInvoiceClassDetails.setFxRate(arinvoiceclassdetails.getFxRate());
				existingArInvoiceClassDetails.setGstType(arinvoiceclassdetails.getGstType());
				existingArInvoiceClassDetails.setGstRate(arinvoiceclassdetails.getGstRate());
				existingArInvoiceClassDetails.setGstOriginalAmount(arinvoiceclassdetails.getGstOriginalAmount());
				existingArInvoiceClassDetails.setGstConvertedAmount(arinvoiceclassdetails.getGstConvertedAmount());
				existingArInvoiceClassDetails.setIncludeGstOriginalAmount(arinvoiceclassdetails.getIncludeGstOriginalAmount());
				existingArInvoiceClassDetails.setIncludeGstConvertedAmount(arinvoiceclassdetails.getIncludeGstConvertedAmount());
				existingArInvoiceClassDetails.setIsApproval(arinvoiceclassdetails.getIsApproval());
				existingArInvoiceClassDetails.setCreatedDate(arinvoiceclassdetails.getCreatedDate());
				existingArInvoiceClassDetails.setModifiedDate(arinvoiceclassdetails.getModifiedDate());
			}
			arinvoiceclassdetails = arInvoiceClassDetailsDAO.store(existingArInvoiceClassDetails);
		} else {
			arinvoiceclassdetails = arInvoiceClassDetailsDAO.store(arinvoiceclassdetails);
		}
		arInvoiceClassDetailsDAO.flush();
	}
}
