package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.CurrencyMasterDAO;
import com.ttv.cashflow.domain.CurrencyMaster;
import com.ttv.cashflow.service.CurrencyMasterService;

/**
 * Spring service that handles CRUD requests for CurrencyMaster entities
 * 
 */

@Service("CurrencyMasterService")

@Transactional
public class CurrencyMasterServiceImpl implements CurrencyMasterService {

	/**
	 * DAO injected by Spring that manages CurrencyMaster entities
	 * 
	 */
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;

	/**
	 * Instantiates a new CurrencyMasterServiceImpl.
	 *
	 */
	public CurrencyMasterServiceImpl() {
	}

	/**
	 */
	@Transactional
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code) {
		return currencyMasterDAO.findCurrencyMasterByPrimaryKey(code);
	}

	/**
	 * Save an existing CurrencyMaster entity
	 * 
	 */
	@Transactional
	public void saveCurrencyMaster(CurrencyMaster currencymaster) {
		CurrencyMaster existingCurrencyMaster = currencyMasterDAO.findCurrencyMasterByPrimaryKey(currencymaster.getCode());

		if (existingCurrencyMaster != null) {
			if (existingCurrencyMaster != currencymaster) {
				existingCurrencyMaster.setCode(currencymaster.getCode());
				existingCurrencyMaster.setName(currencymaster.getName());
			}
			currencymaster = currencyMasterDAO.store(existingCurrencyMaster);
		} else {
			currencymaster = currencyMasterDAO.store(currencymaster);
		}
		currencyMasterDAO.flush();
	}

	/**
	 * Return a count of all CurrencyMaster entity
	 * 
	 */
	@Transactional
	public Integer countCurrencyMasters() {
		return ((Long) currencyMasterDAO.createQuerySingleResult("select count(o) from CurrencyMaster o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing CurrencyMaster entity
	 * 
	 */
	@Transactional
	public Set<CurrencyMaster> loadCurrencyMasters() {
		return currencyMasterDAO.findAllCurrencyMasters();
	}

	/**
	 * Return all CurrencyMaster entity
	 * 
	 */
	@Transactional
	public List<CurrencyMaster> findAllCurrencyMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<CurrencyMaster>(currencyMasterDAO.findAllCurrencyMasters(startResult, maxRows));
	}

	/**
	 * Delete an existing CurrencyMaster entity
	 * 
	 */
	@Transactional
	public void deleteCurrencyMaster(CurrencyMaster currencymaster) {
		currencyMasterDAO.remove(currencymaster);
		currencyMasterDAO.flush();
	}

	public void deleteCurrencyMasterByCodes(String[] codes) {
		// TODO Auto-generated method stub
	    for (String code : codes) {
	        CurrencyMaster currencymaster = currencyMasterDAO.findCurrencyMasterByCode(code);
	        if (currencymaster != null) {
	            deleteCurrencyMaster(currencymaster);
	        }
	    }
	}

	public String findCurrencyMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Integer recordsTotal = currencyMasterDAO.countCurrencyMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", currencyMasterDAO.findCurrencyMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", currencyMasterDAO.countCurrencyMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
