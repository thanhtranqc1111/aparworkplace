
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ReimbursementPaymentStatus;

/**
 * Spring service that handles CRUD requests for ReimbursementPaymentStatus entities
 * 
 */
public interface ReimbursementPaymentStatusService {

	/**
	* Load an existing ReimbursementPaymentStatus entity
	* 
	 */
	public Set<ReimbursementPaymentStatus> loadReimbursementPaymentStatuss();

	/**
	 */
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id);

	/**
	* Delete an existing ReimbursementPaymentStatus entity
	* 
	 */
	public void deleteReimbursementPaymentStatus(ReimbursementPaymentStatus reimbursementpaymentstatus);

	/**
	* Save an existing ReimbursementPaymentStatus entity
	* 
	 */
	public void saveReimbursementPaymentStatus(ReimbursementPaymentStatus reimbursementpaymentstatus_1);

	/**
	* Return a count of all ReimbursementPaymentStatus entity
	* 
	 */
	public Integer countReimbursementPaymentStatuss();

	/**
	* Return all ReimbursementPaymentStatus entity
	* 
	 */
	public List<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss(Integer startResult, Integer maxRows);
}