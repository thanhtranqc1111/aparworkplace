
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.PayrollPaymentStatus;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for PayrollPaymentStatus entities
 * 
 */
public interface PayrollPaymentStatusService {

	/**
	* Return a count of all PayrollPaymentStatus entity
	* 
	 */
	public Integer countPayrollPaymentStatuss();

	/**
	* Return all PayrollPaymentStatus entity
	* 
	 */
	public List<PayrollPaymentStatus> findAllPayrollPaymentStatuss(Integer startResult, Integer maxRows);

	/**
	 */
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id);

	/**
	* Save an existing PayrollPaymentStatus entity
	* 
	 */
	public void savePayrollPaymentStatus(PayrollPaymentStatus payrollpaymentstatus);

	/**
	* Delete an existing PayrollPaymentStatus entity
	* 
	 */
	public void deletePayrollPaymentStatus(PayrollPaymentStatus payrollpaymentstatus_1);

	/**
	* Load an existing PayrollPaymentStatus entity
	* 
	 */
	public Set<PayrollPaymentStatus> loadPayrollPaymentStatuss();
}