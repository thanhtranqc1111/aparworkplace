package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.dao.SubsidiaryUserDetailDAO;
import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.service.SubsidiaryUserDetailService;

/**
 * Spring service that handles CRUD requests for SubsidiaryUserDetail entities
 * 
 */

@Service("SubsidiaryUserDetailService")

@Transactional
public class SubsidiaryUserDetailServiceImpl implements SubsidiaryUserDetailService {

	/**
	 * DAO injected by Spring that manages Subsidiary entities
	 * 
	 */
	@Autowired
	private SubsidiaryDAO subsidiaryDAO;

	/**
	 * DAO injected by Spring that manages SubsidiaryUserDetail entities
	 * 
	 */
	@Autowired
	private SubsidiaryUserDetailDAO subsidiaryUserDetailDAO;

	/**
	 * DAO injected by Spring that manages UserAccount entities
	 * 
	 */
	@Autowired
	private UserAccountDAO userAccountDAO;

	/**
	 * Instantiates a new SubsidiaryUserDetailServiceImpl.
	 *
	 */
	public SubsidiaryUserDetailServiceImpl() {
	}

	/**
	 * Delete an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public SubsidiaryUserDetail deleteSubsidiaryUserDetailSubsidiary(Integer subsidiaryuserdetail_id, Integer related_subsidiary_id) {
		SubsidiaryUserDetail subsidiaryuserdetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(subsidiaryuserdetail_id, -1, -1);
		Subsidiary related_subsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(related_subsidiary_id, -1, -1);

		subsidiaryuserdetail.setSubsidiary(null);
		related_subsidiary.getSubsidiaryUserDetails().remove(subsidiaryuserdetail);
		subsidiaryuserdetail = subsidiaryUserDetailDAO.store(subsidiaryuserdetail);
		subsidiaryUserDetailDAO.flush();

		related_subsidiary = subsidiaryDAO.store(related_subsidiary);
		subsidiaryDAO.flush();

		subsidiaryDAO.remove(related_subsidiary);
		subsidiaryDAO.flush();

		return subsidiaryuserdetail;
	}

	/**
	 * Save an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public void saveSubsidiaryUserDetail(SubsidiaryUserDetail subsidiaryuserdetail) {
		SubsidiaryUserDetail existingSubsidiaryUserDetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(subsidiaryuserdetail.getId());

		if (existingSubsidiaryUserDetail != null) {
			if (existingSubsidiaryUserDetail != subsidiaryuserdetail) {
				existingSubsidiaryUserDetail.setId(subsidiaryuserdetail.getId());
				existingSubsidiaryUserDetail.setIsDefault(subsidiaryuserdetail.getIsDefault());
				existingSubsidiaryUserDetail.setCreatedDate(subsidiaryuserdetail.getCreatedDate());
				existingSubsidiaryUserDetail.setModifiedDate(subsidiaryuserdetail.getModifiedDate());
			}
			subsidiaryuserdetail = subsidiaryUserDetailDAO.store(existingSubsidiaryUserDetail);
		} else {
			subsidiaryuserdetail = subsidiaryUserDetailDAO.store(subsidiaryuserdetail);
		}
		subsidiaryUserDetailDAO.flush();
	}

	/**
	 * Delete an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public void deleteSubsidiaryUserDetail(SubsidiaryUserDetail subsidiaryuserdetail) {
		subsidiaryUserDetailDAO.remove(subsidiaryuserdetail);
		subsidiaryUserDetailDAO.flush();
	}

	/**
	 * Save an existing Subsidiary entity
	 * 
	 */
	@Transactional
	public SubsidiaryUserDetail saveSubsidiaryUserDetailSubsidiary(Integer id, Subsidiary related_subsidiary) {
		SubsidiaryUserDetail subsidiaryuserdetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(id, -1, -1);
		Subsidiary existingsubsidiary = subsidiaryDAO.findSubsidiaryByPrimaryKey(related_subsidiary.getId());

		// copy into the existing record to preserve existing relationships
		if (existingsubsidiary != null) {
			existingsubsidiary.setId(related_subsidiary.getId());
			existingsubsidiary.setCode(related_subsidiary.getCode());
			existingsubsidiary.setName(related_subsidiary.getName());
			existingsubsidiary.setAddress(related_subsidiary.getAddress());
			existingsubsidiary.setPhone(related_subsidiary.getPhone());
			existingsubsidiary.setWebsite(related_subsidiary.getWebsite());
			existingsubsidiary.setDescription(related_subsidiary.getDescription());
			existingsubsidiary.setIsActive(related_subsidiary.getIsActive());
			existingsubsidiary.setCreatedDate(related_subsidiary.getCreatedDate());
			existingsubsidiary.setModifiedDate(related_subsidiary.getModifiedDate());
			related_subsidiary = existingsubsidiary;
		}

		subsidiaryuserdetail.setSubsidiary(related_subsidiary);
		related_subsidiary.getSubsidiaryUserDetails().add(subsidiaryuserdetail);
		subsidiaryuserdetail = subsidiaryUserDetailDAO.store(subsidiaryuserdetail);
		subsidiaryUserDetailDAO.flush();

		related_subsidiary = subsidiaryDAO.store(related_subsidiary);
		subsidiaryDAO.flush();

		return subsidiaryuserdetail;
	}

	/**
	 * Delete an existing UserAccount entity
	 * 
	 */
	@Transactional
	public SubsidiaryUserDetail deleteSubsidiaryUserDetailUserAccount(Integer subsidiaryuserdetail_id, Integer related_useraccount_id) {
		SubsidiaryUserDetail subsidiaryuserdetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(subsidiaryuserdetail_id, -1, -1);
		UserAccount related_useraccount = userAccountDAO.findUserAccountByPrimaryKey(related_useraccount_id, -1, -1);

		subsidiaryuserdetail.setUserAccount(null);
		related_useraccount.getSubsidiaryUserDetails().remove(subsidiaryuserdetail);
		subsidiaryuserdetail = subsidiaryUserDetailDAO.store(subsidiaryuserdetail);
		subsidiaryUserDetailDAO.flush();

		related_useraccount = userAccountDAO.store(related_useraccount);
		userAccountDAO.flush();

		userAccountDAO.remove(related_useraccount);
		userAccountDAO.flush();

		return subsidiaryuserdetail;
	}

	/**
	 * Load an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public Set<SubsidiaryUserDetail> loadSubsidiaryUserDetails() {
		return subsidiaryUserDetailDAO.findAllSubsidiaryUserDetails();
	}

	/**
	 */
	@Transactional
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id) {
		return subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(id);
	}

	/**
	 * Save an existing UserAccount entity
	 * 
	 */
	@Transactional
	public SubsidiaryUserDetail saveSubsidiaryUserDetailUserAccount(Integer id, UserAccount related_useraccount) {
		SubsidiaryUserDetail subsidiaryuserdetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(id, -1, -1);
		UserAccount existinguserAccount = userAccountDAO.findUserAccountByPrimaryKey(related_useraccount.getId());

		// copy into the existing record to preserve existing relationships
		if (existinguserAccount != null) {
			existinguserAccount.setId(related_useraccount.getId());
			existinguserAccount.setEmail(related_useraccount.getEmail());
			existinguserAccount.setPassword(related_useraccount.getPassword());
			existinguserAccount.setFirstName(related_useraccount.getFirstName());
			existinguserAccount.setLastName(related_useraccount.getLastName());
			existinguserAccount.setPhone(related_useraccount.getPhone());
			existinguserAccount.setIsActive(related_useraccount.getIsActive());
			existinguserAccount.setCreatedDate(related_useraccount.getCreatedDate());
			existinguserAccount.setModifiedDate(related_useraccount.getModifiedDate());
			existinguserAccount.setModifiedDate(related_useraccount.getModifiedDate());
			existinguserAccount.setIsUsing2fa(related_useraccount.getIsUsing2fa());
			related_useraccount = existinguserAccount;
		}

		subsidiaryuserdetail.setUserAccount(related_useraccount);
		related_useraccount.getSubsidiaryUserDetails().add(subsidiaryuserdetail);
		subsidiaryuserdetail = subsidiaryUserDetailDAO.store(subsidiaryuserdetail);
		subsidiaryUserDetailDAO.flush();

		related_useraccount = userAccountDAO.store(related_useraccount);
		userAccountDAO.flush();

		return subsidiaryuserdetail;
	}

	/**
	 * Return all SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public List<SubsidiaryUserDetail> findAllSubsidiaryUserDetails(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<SubsidiaryUserDetail>(subsidiaryUserDetailDAO.findAllSubsidiaryUserDetails(startResult, maxRows));
	}

	/**
	 * Return a count of all SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public Integer countSubsidiaryUserDetails() {
		return ((Long) subsidiaryUserDetailDAO.createQuerySingleResult("select count(o) from SubsidiaryUserDetail o").getSingleResult()).intValue();
	}
	
	/**
	 * Get Subsidiary User detail 
	 * @return map "subsidiary Id, userAccountId"
	 */
	public Map<String, Integer> getSubsidiaryUserMap() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		Set<SubsidiaryUserDetail> set = subsidiaryUserDetailDAO.findAllSubsidiaryUserDetails();
		Iterator<SubsidiaryUserDetail> iterator = set.iterator();
		while(iterator.hasNext())
		{
			SubsidiaryUserDetail subsidiaryUser = iterator.next();
			map.put(subsidiaryUser.getSubsidiary().getId() + "," + subsidiaryUser.getUserAccount().getId(), subsidiaryUser.getId());
		}
		return map;
	}
	
	/**
	 * Save subsidiary user
	 * @param jsonArray
	 * @return
	 */
	public int saveSubsidiaryUserDetail(JsonArray jsonArray) {
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Integer subsidiaryId = Integer.parseInt(jsonObject.get("subsidiaryId").getAsString());
				Integer userAccountId = Integer.parseInt(jsonObject.get("userAccountId").getAsString());
				int value = Integer.parseInt(jsonObject.get("value").getAsString());//0: unchecked, 1: checked
				Set<SubsidiaryUserDetail> subsidiaryUserDetails = subsidiaryUserDetailDAO.findSubsidiaryUserBySubsidiaryIdAndUserId(subsidiaryId, userAccountId);
				if(subsidiaryUserDetails.size() > 0)
				{
					if(value == 1)
					{
						continue;
					}
					else {
						for(SubsidiaryUserDetail subsidiaryUserDetail: subsidiaryUserDetails)
						{
							subsidiaryUserDetailDAO.remove(subsidiaryUserDetail);
						}
					}
				}
				else {
					if(value == 0)
					{
						continue;
					}
					else 
					{
						SubsidiaryUserDetail subsidiaryUserDetail = new SubsidiaryUserDetail();
						Subsidiary subsidiary = new Subsidiary();
						UserAccount userAccount = new UserAccount();
						subsidiary.setId(subsidiaryId);
						userAccount.setId(userAccountId);
						subsidiaryUserDetail.setSubsidiary(subsidiary);
						subsidiaryUserDetail.setUserAccount(userAccount);
						subsidiaryUserDetailDAO.store(subsidiaryUserDetail);
						subsidiaryUserDetailDAO.flush();
					}
				}
			}
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByUserId(Integer userId) {
		return subsidiaryUserDetailDAO.findSubsidiaryUserDetailByUserId(userId);
	}
	
}
