
package com.ttv.cashflow.service;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.ReceiptOrder;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ReceiptOrder entities
 * 
 */
public interface ReceiptOrderService {

	/**
	* Delete an existing ReceiptOrder entity
	* 
	 */
	public void deleteReceiptOrder(ReceiptOrder receiptorder);

	/**
	* Load an existing ReceiptOrder entity
	* 
	 */
	public Set<ReceiptOrder> loadReceiptOrders();

	/**
	* Return a count of all ReceiptOrder entity
	* 
	 */
	public Integer countReceiptOrders();

	/**
	 */
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id);

	/**
	* Return all ReceiptOrder entity
	* 
	 */
	public List<ReceiptOrder> findAllReceiptOrders(Integer startResult, Integer maxRows);

	/**
	* Save an existing ReceiptOrder entity
	*/
	public void saveReceiptOrder(ReceiptOrder receiptorder_1);
	
	public String findReceiptOrderJSON(Integer start, Integer end, String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo,
									   String receiptOrderNo, BigDecimal credit, Integer option, Integer orderColumn, String orderBy, int status);
	
	public String findArInvoiceToMatchRecepitOrderJSON(Integer start, Integer end, String fromApNo, String toApNo,
													   String fromMonth, String toMonth, String payerName, boolean isApproved, BigDecimal credit, Integer option, boolean isNotReceived);
	
	public List<ArInvoiceReceiptOrderDTO> parseArInvoiceJSON(JsonArray jsonArray);
	
	public int appyMatch(BigInteger bankStatementId, List<ArInvoiceReceiptOrderDTO> listArInvoiceReDTO);
	
	public int resetRecepitOrder(BigInteger bankStatementId);
}