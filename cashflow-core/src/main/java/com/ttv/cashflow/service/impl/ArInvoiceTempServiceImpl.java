package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ArInvoiceTempDAO;

import com.ttv.cashflow.domain.ArInvoiceTemp;
import com.ttv.cashflow.service.ArInvoiceTempService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ArInvoiceTemp entities
 * 
 */

@Service("ArInvoiceTempService")

@Transactional
public class ArInvoiceTempServiceImpl implements ArInvoiceTempService {

	/**
	 * DAO injected by Spring that manages ArInvoiceTemp entities
	 * 
	 */
	@Autowired
	private ArInvoiceTempDAO arInvoiceTempDAO;

	/**
	 * Instantiates a new ArInvoiceTempServiceImpl.
	 *
	 */
	public ArInvoiceTempServiceImpl() {
	}

	/**
	 */
	@Transactional
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id) {
		return arInvoiceTempDAO.findArInvoiceTempByPrimaryKey(id);
	}

	/**
	 * Return a count of all ArInvoiceTemp entity
	 * 
	 */
	@Transactional
	public Integer countArInvoiceTemps() {
		return ((Long) arInvoiceTempDAO.createQuerySingleResult("select count(o) from ArInvoiceTemp o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing ArInvoiceTemp entity
	 * 
	 */
	@Transactional
	public void deleteArInvoiceTemp(ArInvoiceTemp arinvoicetemp) {
		arInvoiceTempDAO.remove(arinvoicetemp);
		arInvoiceTempDAO.flush();
	}

	/**
	 * Save an existing ArInvoiceTemp entity
	 * 
	 */
	@Transactional
	public void saveArInvoiceTemp(ArInvoiceTemp arinvoicetemp) {
		ArInvoiceTemp existingArInvoiceTemp = arInvoiceTempDAO.findArInvoiceTempByPrimaryKey(arinvoicetemp.getId());

		if (existingArInvoiceTemp != null) {
			if (existingArInvoiceTemp != arinvoicetemp) {
				existingArInvoiceTemp.setId(arinvoicetemp.getId());
				existingArInvoiceTemp.setPayerName(arinvoicetemp.getPayerName());
				existingArInvoiceTemp.setInvoiceNo(arinvoicetemp.getInvoiceNo());
				existingArInvoiceTemp.setInvoiceIssuedDate(arinvoicetemp.getInvoiceIssuedDate());
				existingArInvoiceTemp.setMonth(arinvoicetemp.getMonth());
				existingArInvoiceTemp.setClaimType(arinvoicetemp.getClaimType());
				existingArInvoiceTemp.setOriginalCurrency(arinvoicetemp.getOriginalCurrency());
				existingArInvoiceTemp.setFxRate(arinvoicetemp.getFxRate());
				existingArInvoiceTemp.setAccountReceivableCode(arinvoicetemp.getAccountReceivableCode());
				existingArInvoiceTemp.setAccountReceivableName(arinvoicetemp.getAccountReceivableName());
				existingArInvoiceTemp.setDescription(arinvoicetemp.getDescription());
				existingArInvoiceTemp.setAccountCode(arinvoicetemp.getAccountCode());
				existingArInvoiceTemp.setAccountName(arinvoicetemp.getAccountName());
				existingArInvoiceTemp.setAccountType(arinvoicetemp.getAccountType());
				existingArInvoiceTemp.setProjectCode(arinvoicetemp.getProjectCode());
				existingArInvoiceTemp.setProjectName(arinvoicetemp.getProjectName());
				existingArInvoiceTemp.setProjectDescription(arinvoicetemp.getProjectDescription());
				existingArInvoiceTemp.setApprovalCode(arinvoicetemp.getApprovalCode());
				existingArInvoiceTemp.setExcludeGstOriginalAmount(arinvoicetemp.getExcludeGstOriginalAmount());
				existingArInvoiceTemp.setGstType(arinvoicetemp.getGstType());
				existingArInvoiceTemp.setGstRate(arinvoicetemp.getGstRate());
				existingArInvoiceTemp.setIsApproval(arinvoicetemp.getIsApproval());
			}
			arinvoicetemp = arInvoiceTempDAO.store(existingArInvoiceTemp);
		} else {
			arinvoicetemp = arInvoiceTempDAO.store(arinvoicetemp);
		}
		arInvoiceTempDAO.flush();
	}

	/**
	 * Load an existing ArInvoiceTemp entity
	 * 
	 */
	@Transactional
	public Set<ArInvoiceTemp> loadArInvoiceTemps() {
		return arInvoiceTempDAO.findAllArInvoiceTemps();
	}

	/**
	 * Return all ArInvoiceTemp entity
	 * 
	 */
	@Transactional
	public List<ArInvoiceTemp> findAllArInvoiceTemps(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArInvoiceTemp>(arInvoiceTempDAO.findAllArInvoiceTemps(startResult, maxRows));
	}
}
