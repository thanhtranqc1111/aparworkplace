package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.service.ApInvoiceClassDetailsService;

/**
 * Spring service that handles CRUD requests for ApInvoiceClassDetails entities
 * 
 */

@Service("ApInvoiceClassDetailsService")

@Transactional
public class ApInvoiceClassDetailsServiceImpl implements ApInvoiceClassDetailsService {

	/**
	 * DAO injected by Spring that manages ApInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ApInvoiceAccountDetailsDAO apInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ApInvoiceClassDetails entities
	 * 
	 */
	@Autowired
	private ApInvoiceClassDetailsDAO apInvoiceClassDetailsDAO;

	/**
	 * Instantiates a new ApInvoiceClassDetailsServiceImpl.
	 *
	 */
	public ApInvoiceClassDetailsServiceImpl() {
	}

	/**
	 * Load an existing ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> loadApInvoiceClassDetailss() {
		return apInvoiceClassDetailsDAO.findAllApInvoiceClassDetailss();
	}

	/**
	 */
	@Transactional
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id) {
		return apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(id);
	}

	/**
	 * Delete an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ApInvoiceClassDetails deleteApInvoiceClassDetailsApInvoiceAccountDetails(BigInteger apinvoiceclassdetails_id, BigInteger related_apinvoiceaccountdetails_id) {
		ApInvoiceClassDetails apinvoiceclassdetails = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(apinvoiceclassdetails_id, -1, -1);
		ApInvoiceAccountDetails related_apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(related_apinvoiceaccountdetails_id, -1, -1);

		apinvoiceclassdetails.setApInvoiceAccountDetails(null);
		related_apinvoiceaccountdetails.getApInvoiceClassDetailses().remove(apinvoiceclassdetails);
		apinvoiceclassdetails = apInvoiceClassDetailsDAO.store(apinvoiceclassdetails);
		apInvoiceClassDetailsDAO.flush();

		related_apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(related_apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		apInvoiceAccountDetailsDAO.remove(related_apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		return apinvoiceclassdetails;
	}

	/**
	 * Delete an existing ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public void deleteApInvoiceClassDetails(ApInvoiceClassDetails apinvoiceclassdetails) {
		apInvoiceClassDetailsDAO.remove(apinvoiceclassdetails);
		apInvoiceClassDetailsDAO.flush();
	}

	/**
	 * Return a count of all ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public Integer countApInvoiceClassDetailss() {
		return ((Long) apInvoiceClassDetailsDAO.createQuerySingleResult("select count(o) from ApInvoiceClassDetails o").getSingleResult()).intValue();
	}

	/**
	 * Return all ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public List<ApInvoiceClassDetails> findAllApInvoiceClassDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoiceClassDetails>(apInvoiceClassDetailsDAO.findAllApInvoiceClassDetailss(startResult, maxRows));
	}

	/**
	 * Save an existing ApInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public ApInvoiceClassDetails saveApInvoiceClassDetailsApInvoiceAccountDetails(BigInteger id, ApInvoiceAccountDetails related_apinvoiceaccountdetails) {
		ApInvoiceClassDetails apinvoiceclassdetails = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(id, -1, -1);
		ApInvoiceAccountDetails existingapInvoiceAccountDetails = apInvoiceAccountDetailsDAO.findApInvoiceAccountDetailsByPrimaryKey(related_apinvoiceaccountdetails.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoiceAccountDetails != null) {
			existingapInvoiceAccountDetails.setId(related_apinvoiceaccountdetails.getId());
			existingapInvoiceAccountDetails.setAccountCode(related_apinvoiceaccountdetails.getAccountCode());
			existingapInvoiceAccountDetails.setAccountName(related_apinvoiceaccountdetails.getAccountName());
			existingapInvoiceAccountDetails.setExcludeGstOriginalAmount(related_apinvoiceaccountdetails.getExcludeGstOriginalAmount());
			existingapInvoiceAccountDetails.setExcludeGstConvertedAmount(related_apinvoiceaccountdetails.getExcludeGstConvertedAmount());
			existingapInvoiceAccountDetails.setGstConvertedAmount(related_apinvoiceaccountdetails.getGstConvertedAmount());
			existingapInvoiceAccountDetails.setIncludeGstConvertedAmount(related_apinvoiceaccountdetails.getIncludeGstConvertedAmount());
			existingapInvoiceAccountDetails.setCreatedDate(related_apinvoiceaccountdetails.getCreatedDate());
			existingapInvoiceAccountDetails.setModifiedDate(related_apinvoiceaccountdetails.getModifiedDate());
			related_apinvoiceaccountdetails = existingapInvoiceAccountDetails;
		} else {
			related_apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(related_apinvoiceaccountdetails);
			apInvoiceAccountDetailsDAO.flush();
		}

		apinvoiceclassdetails.setApInvoiceAccountDetails(related_apinvoiceaccountdetails);
		related_apinvoiceaccountdetails.getApInvoiceClassDetailses().add(apinvoiceclassdetails);
		apinvoiceclassdetails = apInvoiceClassDetailsDAO.store(apinvoiceclassdetails);
		apInvoiceClassDetailsDAO.flush();

		related_apinvoiceaccountdetails = apInvoiceAccountDetailsDAO.store(related_apinvoiceaccountdetails);
		apInvoiceAccountDetailsDAO.flush();

		return apinvoiceclassdetails;
	}

	/**
	 * Save an existing ApInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public void saveApInvoiceClassDetails(ApInvoiceClassDetails apinvoiceclassdetails) {
		ApInvoiceClassDetails existingApInvoiceClassDetails = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByPrimaryKey(apinvoiceclassdetails.getId());

		if (existingApInvoiceClassDetails != null) {
			if (existingApInvoiceClassDetails != apinvoiceclassdetails) {
				existingApInvoiceClassDetails.setId(apinvoiceclassdetails.getId());
				existingApInvoiceClassDetails.setClassCode(apinvoiceclassdetails.getClassCode());
				existingApInvoiceClassDetails.setClassName(apinvoiceclassdetails.getClassName());
				existingApInvoiceClassDetails.setDescription(apinvoiceclassdetails.getDescription());
				existingApInvoiceClassDetails.setPoNo(apinvoiceclassdetails.getPoNo());
				existingApInvoiceClassDetails.setApprovalCode(apinvoiceclassdetails.getApprovalCode());
				existingApInvoiceClassDetails.setExcludeGstOriginalAmount(apinvoiceclassdetails.getExcludeGstOriginalAmount());
				existingApInvoiceClassDetails.setExcludeGstConvertedAmount(apinvoiceclassdetails.getExcludeGstConvertedAmount());
				existingApInvoiceClassDetails.setGstOriginalAmount(apinvoiceclassdetails.getGstOriginalAmount());
				existingApInvoiceClassDetails.setGstConvertedAmount(apinvoiceclassdetails.getGstConvertedAmount());
				existingApInvoiceClassDetails.setIncludeGstOriginalAmount(apinvoiceclassdetails.getIncludeGstOriginalAmount());
				existingApInvoiceClassDetails.setIncludeGstConvertedAmount(apinvoiceclassdetails.getIncludeGstConvertedAmount());
				//existingApInvoiceClassDetails.setCreatedDate(apinvoiceclassdetails.getCreatedDate());
				existingApInvoiceClassDetails.setModifiedDate(apinvoiceclassdetails.getModifiedDate());
				existingApInvoiceClassDetails.setIsApproval(apinvoiceclassdetails.getIsApproval());
			}
			apinvoiceclassdetails = apInvoiceClassDetailsDAO.store(existingApInvoiceClassDetails);
		} else {
			apinvoiceclassdetails = apInvoiceClassDetailsDAO.store(apinvoiceclassdetails);
		}
		apInvoiceClassDetailsDAO.flush();
	}

	public String findByApprovalCodeNotApprovedJSON(String approvalCode) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", apInvoiceClassDetailsDAO.findByApprovalCodeNotApproved(approvalCode));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	@Override
	public String findDetailByApprovalCodeJSON(String approvalCode) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", apInvoiceClassDetailsDAO.findDetailByApprovalCode(approvalCode));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
