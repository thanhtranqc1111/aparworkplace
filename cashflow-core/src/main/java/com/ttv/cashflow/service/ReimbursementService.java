
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.google.gson.JsonObject;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementDetails;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.dto.ReimbursementDTO;

/**
 * Spring service that handles CRUD requests for Reimbursement entities
 * 
 */
public interface ReimbursementService {

	/**
	* Return all Reimbursement entity
	* 
	 */
	public List<Reimbursement> findAllReimbursements(Integer startResult, Integer maxRows);

	/**
	 */
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id);

	/**
	* Save an existing ReimbursementPaymentDetails entity
	* 
	 */
	public Reimbursement saveReimbursementReimbursementPaymentDetailses(BigInteger id_1, ReimbursementPaymentDetails related_reimbursementpaymentdetailses);

	/**
	* Load an existing Reimbursement entity
	* 
	 */
	public Set<Reimbursement> loadReimbursements();

	/**
	* Delete an existing ReimbursementDetails entity
	* 
	 */
	public Reimbursement deleteReimbursementReimbursementDetailses(BigInteger reimbursement_id, BigInteger related_reimbursementdetailses_id);

	/**
	* Delete an existing ReimbursementPaymentDetails entity
	* 
	 */
	public Reimbursement deleteReimbursementReimbursementPaymentDetailses(BigInteger reimbursement_id_1, BigInteger related_reimbursementpaymentdetailses_id);

	/**
	* Save an existing Reimbursement entity
	* 
	 */
	public Reimbursement saveReimbursement(Reimbursement reimbursement);

	/**
	* Save an existing ReimbursementDetails entity
	* 
	 */
	public Reimbursement saveReimbursementReimbursementDetailses(BigInteger id_2, ReimbursementDetails related_reimbursementdetailses);

	/**
	* Delete an existing Reimbursement entity
	* 
	 */
	public void deleteReimbursement(Reimbursement reimbursement_1);

	/**
	* Return a count of all Reimbursement entity
	* 
	 */
	public Integer countReimbursements();
	
	public Reimbursement findReimbursementByReimbursementNo(String reimbursementNo);
	
	public String getReimbursementNo();
	
	public void copyDomainToDTO(Reimbursement reimbursement, ReimbursementDTO reimbursementDTO);
	
	public ReimbursementDTO parseReimbursementJSON(JsonObject jsonObject);
	
	public BigInteger saveReimbursement(ReimbursementDTO reimbursementDTO, boolean isSaveFromImport);
	
	public Set<Reimbursement> searchReimDetails(Object... parameters);

	public ReimbursementDTO clone(Reimbursement reimbursement);
	
	public String findReimbursementPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth,
									      String fromReimbursementNo, String toReimbursementNo, String employeeCode, Integer orderColumn, String orderBy, int status);
	public Set<Reimbursement> findReimbursementByImportKey(String importKey) throws DataAccessException;
	
	public int deleteReimbursementByImportKey(String importKey);
	
	public String getReimbursementNo(Calendar cal);
}