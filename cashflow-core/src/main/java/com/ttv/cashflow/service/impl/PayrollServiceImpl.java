package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dao.PayrollDetailsDAO;
import com.ttv.cashflow.dao.PayrollPaymentDetailsDAO;
import com.ttv.cashflow.dao.PayrollPaymentStatusDAO;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollDetails;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.domain.PayrollPaymentStatus;
import com.ttv.cashflow.dto.PayrollDTO;
import com.ttv.cashflow.dto.PayrollDetailsDTO;
import com.ttv.cashflow.dto.PayrollPaymentDTO;
import com.ttv.cashflow.service.PayrollService;
import com.ttv.cashflow.service.SystemValueService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;

/**
 * Spring service that handles CRUD requests for Payroll entities
 * 
 */

@Service("PayrollService")

@Transactional
public class PayrollServiceImpl implements PayrollService {
	public static final BigInteger SAVE_ERROR_INVAILD_HEADER = new BigInteger("-1");
	public static final BigInteger SAVE_ERROR_INVAILD_DETAILS = new BigInteger("-2");
	public static final BigInteger SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_ORIGINAL = new BigInteger("-3");
	public static final BigInteger SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_CONVERTED = new BigInteger("-4");
	public static final BigInteger SAVE_ERROR_PAYROLL_NO_IS_EXISTS = new BigInteger("-5");
	/**
	 * DAO injected by Spring that manages Payroll entities
	 * 
	 */
	@Autowired
	private PayrollDAO payrollDAO;

	/**
	 * DAO injected by Spring that manages PayrollDetails entities
	 * 
	 */
	@Autowired
	private PayrollDetailsDAO payrollDetailsDAO;

	/**
	 * DAO injected by Spring that manages PayrollPaymentDetails entities
	 * 
	 */
	@Autowired
	private PayrollPaymentDetailsDAO payrollPaymentDetailsDAO;
	
	@Autowired
	private PayrollPaymentStatusDAO paymentStatusDAO;
	
	@Autowired
    private SystemValueService systemValueService;
	
	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
	
	@Autowired
	private BankStatementDAO bankStatementDAO;
	/**
	 * Instantiates a new PayrollServiceImpl.
	 *
	 */
	public PayrollServiceImpl() {
	}

	/**
	 * Load an existing Payroll entity
	 * 
	 */
	@Transactional
	public Set<Payroll> loadPayrolls() {
		return payrollDAO.findAllPayrolls();
	}

	/**
	 * Delete an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public Payroll deletePayrollPayrollDetailses(BigInteger payroll_id, BigInteger related_payrolldetailses_id) {
		PayrollDetails related_payrolldetailses = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(related_payrolldetailses_id, -1, -1);

		Payroll payroll = payrollDAO.findPayrollByPrimaryKey(payroll_id, -1, -1);

		related_payrolldetailses.setPayroll(null);
		payroll.getPayrollDetailses().remove(related_payrolldetailses);

		payrollDetailsDAO.remove(related_payrolldetailses);
		payrollDetailsDAO.flush();

		return payroll;
	}

	/**
	 * Return all Payroll entity
	 * 
	 */
	@Transactional
	public List<Payroll> findAllPayrolls(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Payroll>(payrollDAO.findAllPayrolls(startResult, maxRows));
	}

	/**
	 * Return a count of all Payroll entity
	 * 
	 */
	@Transactional
	public Integer countPayrolls() {
		return ((Long) payrollDAO.createQuerySingleResult("select count(o) from Payroll o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public Payroll savePayrollPayrollPaymentDetailses(BigInteger id, PayrollPaymentDetails related_payrollpaymentdetailses) {
		Payroll payroll = payrollDAO.findPayrollByPrimaryKey(id, -1, -1);
		PayrollPaymentDetails existingpayrollPaymentDetailses = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(related_payrollpaymentdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpayrollPaymentDetailses != null) {
			existingpayrollPaymentDetailses.setId(related_payrollpaymentdetailses.getId());
			existingpayrollPaymentDetailses.setPaymentRate(related_payrollpaymentdetailses.getPaymentRate());
			existingpayrollPaymentDetailses.setNetPaymentAmountConverted(related_payrollpaymentdetailses.getNetPaymentAmountConverted());
			existingpayrollPaymentDetailses.setNetPaymentAmountOriginal(related_payrollpaymentdetailses.getNetPaymentAmountOriginal());
			existingpayrollPaymentDetailses.setCreatedDate(related_payrollpaymentdetailses.getCreatedDate());
			existingpayrollPaymentDetailses.setModifiedDate(related_payrollpaymentdetailses.getModifiedDate());
			related_payrollpaymentdetailses = existingpayrollPaymentDetailses;
		}

		related_payrollpaymentdetailses.setPayroll(payroll);
		payroll.getPayrollPaymentDetailses().add(related_payrollpaymentdetailses);
		related_payrollpaymentdetailses = payrollPaymentDetailsDAO.store(related_payrollpaymentdetailses);
		payrollPaymentDetailsDAO.flush();

		payroll = payrollDAO.store(payroll);
		payrollDAO.flush();

		return payroll;
	}

	/**
	 * Save an existing Payroll entity
	 * 
	 */
	@Transactional
	public Payroll savePayroll(Payroll payroll) {
		Payroll existingPayroll = payrollDAO.findPayrollByPrimaryKey(payroll.getId());

		if (existingPayroll != null) {
			existingPayroll.setId(payroll.getId());
			existingPayroll.setMonth(payroll.getMonth());
			existingPayroll.setPayrollNo(payroll.getPayrollNo());
			existingPayroll.setBookingDate(payroll.getBookingDate());
			existingPayroll.setClaimType(payroll.getClaimType());
			existingPayroll.setAccountPayableCode(payroll.getAccountPayableCode());
			existingPayroll.setAccountPayableName(payroll.getAccountPayableName());
			existingPayroll.setPaymentScheduleDate(payroll.getPaymentScheduleDate());
			existingPayroll.setTotalLaborCostOriginal(payroll.getTotalLaborCostOriginal());
			existingPayroll.setTotalDeductionOriginal(payroll.getTotalDeductionOriginal());
			existingPayroll.setStatus(payroll.getStatus());
			existingPayroll.setTotalNetPaymentOriginal(payroll.getTotalNetPaymentOriginal());
			existingPayroll.setTotalNetPaymentConverted(payroll.getTotalNetPaymentConverted());
			existingPayroll.setDescription(payroll.getDescription());
			existingPayroll.setModifiedDate(payroll.getModifiedDate());
			existingPayroll.setOriginalCurrencyCode(payroll.getOriginalCurrencyCode());
			existingPayroll.setFxRate(payroll.getFxRate());
			existingPayroll.setInvoiceNo(payroll.getInvoiceNo());
			payroll = payrollDAO.store(existingPayroll);
		} else {
			payroll = payrollDAO.store(payroll);
		}
		payrollDAO.flush();
		return payroll;
	}

	/**
	 * Delete an existing Payroll entity
	 * 
	 */
	@Transactional
	public void deletePayroll(Payroll payroll) {
		payrollDAO.remove(payroll);
		payrollDAO.flush();
	}

	/**
	 */
	@Transactional
	public Payroll findPayrollByPrimaryKey(BigInteger id) {
		return payrollDAO.findPayrollByPrimaryKey(id);
	}

	/**
	 * Save an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public PayrollDetails savePayrollPayrollDetailses(Payroll payroll, PayrollDetails payrolldetail) {
		PayrollDetails existingpayrollDetailses = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(payrolldetail.getId());
		// copy into the existing record to preserve existing relationships
		if (existingpayrollDetailses != null) {
			existingpayrollDetailses.setId(payrolldetail.getId());
			existingpayrollDetailses.setEmployeeCode(payrolldetail.getEmployeeCode());
			existingpayrollDetailses.setRoleTitle(payrolldetail.getRoleTitle());
			existingpayrollDetailses.setDivision(payrolldetail.getDivision());
			existingpayrollDetailses.setTeam(payrolldetail.getTeam());
			existingpayrollDetailses.setLaborBaseSalaryPayment(payrolldetail.getLaborBaseSalaryPayment());
			existingpayrollDetailses.setLaborAdditionalPayment1(payrolldetail.getLaborAdditionalPayment1());
			existingpayrollDetailses.setLaborAdditionalPayment2(payrolldetail.getLaborAdditionalPayment2());
			existingpayrollDetailses.setLaborAdditionalPayment3(payrolldetail.getLaborAdditionalPayment3());
			existingpayrollDetailses.setLaborSocialInsuranceEmployer1(payrolldetail.getLaborSocialInsuranceEmployer1());
			existingpayrollDetailses.setLaborSocialInsuranceEmployer2(payrolldetail.getLaborSocialInsuranceEmployer2());
			existingpayrollDetailses.setLaborSocialInsuranceEmployer3(payrolldetail.getLaborSocialInsuranceEmployer3());
			existingpayrollDetailses.setLaborOtherPayment1(payrolldetail.getLaborOtherPayment1());
			existingpayrollDetailses.setLaborOtherPayment2(payrolldetail.getLaborOtherPayment2());
			existingpayrollDetailses.setLaborOtherPayment3(payrolldetail.getLaborOtherPayment3());
			existingpayrollDetailses.setTotalLaborCostOriginal(payrolldetail.getTotalLaborCostOriginal());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployer1(payrolldetail.getDeductionSocialInsuranceEmployer1());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployer2(payrolldetail.getDeductionSocialInsuranceEmployer2());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployer3(payrolldetail.getDeductionSocialInsuranceEmployer3());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployee1(payrolldetail.getDeductionSocialInsuranceEmployee1());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployee2(payrolldetail.getDeductionSocialInsuranceEmployee2());
			existingpayrollDetailses.setDeductionSocialInsuranceEmployee3(payrolldetail.getDeductionSocialInsuranceEmployee3());
			existingpayrollDetailses.setDeductionWht(payrolldetail.getDeductionWht());
			existingpayrollDetailses.setDeductionOther1(payrolldetail.getDeductionOther1());
			existingpayrollDetailses.setDeductionOther2(payrolldetail.getDeductionOther2());
			existingpayrollDetailses.setDeductionOther3(payrolldetail.getDeductionOther3());
			existingpayrollDetailses.setTotalDeductionOriginal(payrolldetail.getTotalDeductionOriginal());
			existingpayrollDetailses.setNetPaymentOriginal(payrolldetail.getNetPaymentOriginal());
			existingpayrollDetailses.setNetPaymentConverted(payrolldetail.getNetPaymentConverted());
			existingpayrollDetailses.setApprovalCode(payrolldetail.getApprovalCode());
			existingpayrollDetailses.setIsApproval(payrolldetail.getIsApproval());
			existingpayrollDetailses.setModifiedDate(payrolldetail.getModifiedDate());
			payrolldetail = existingpayrollDetailses;
		} else {
			payrolldetail.setPayroll(payroll);
		}
		payrollDetailsDAO.store(payrolldetail);
		payrollDetailsDAO.flush();
		return payrolldetail;
	}

	/**
	 * Delete an existing PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public Payroll deletePayrollPayrollPaymentDetailses(BigInteger payroll_id, BigInteger related_payrollpaymentdetailses_id) {
		PayrollPaymentDetails related_payrollpaymentdetailses = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(related_payrollpaymentdetailses_id, -1, -1);

		Payroll payroll = payrollDAO.findPayrollByPrimaryKey(payroll_id, -1, -1);

		related_payrollpaymentdetailses.setPayroll(null);
		payroll.getPayrollPaymentDetailses().remove(related_payrollpaymentdetailses);

		payrollPaymentDetailsDAO.remove(related_payrollpaymentdetailses);
		payrollPaymentDetailsDAO.flush();

		return payroll;
	}
	
	@Transactional
    public Set<Payroll> searchPayroll(Object... parameters) {
		return payrollDAO.searchPayroll(parameters);
    }
	
	@Transactional
    public Set<Payroll> searchPayrollDetails(Object... parameters) {
		return payrollDAO.searchPayrollDetails(parameters);
    }

	@Override
	public PayrollDTO parsePayrollJSON(JsonObject jsonObject) {
		PayrollDTO payrollDTO = new PayrollDTO();
		payrollDTO.setId(new BigInteger(jsonObject.get("id").getAsString()));
		payrollDTO.setMonth(CalendarUtil.getCalendar(jsonObject.get("month").getAsString(), "MM/yyyy"));
		payrollDTO.setPayrollNo(jsonObject.get("payrollNo").getAsString());
		payrollDTO.setBookingDate(CalendarUtil.getCalendar(jsonObject.get("bookingDate").getAsString()));
		payrollDTO.setClaimType(jsonObject.get("claimType").getAsString());
		payrollDTO.setAccountPayableCode(jsonObject.get("accountPayableCode").getAsString());
		payrollDTO.setAccountPayableName(jsonObject.get("accountPayableName").getAsString());
		payrollDTO.setOriginalCurrencyCode(jsonObject.get("originalCurrencyCode").getAsString());
		payrollDTO.setPaymentScheduleDate(CalendarUtil.getCalendar(jsonObject.get("paymentScheduleDate").getAsString()));
		payrollDTO.setTotalLaborCostOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("totalLaborCostOriginal").getAsString().replaceAll(",", "")));
		payrollDTO.setTotalDeductionOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("totalDeductionOriginal").getAsString().replaceAll(",", "")));
		payrollDTO.setTotalNetPaymentOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("totalNetPaymentOriginal").getAsString().replaceAll(",", "")));
		payrollDTO.setTotalNetPaymentConverted(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("totalNetPaymentConverted").getAsString().replaceAll(",", "")));
		payrollDTO.setFxRate(NumberUtil.getBigDecimalScaleDown(-1, jsonObject.get("fxRate").getAsString().replaceAll(",", "")));
		payrollDTO.setDescription(jsonObject.get("description").getAsString());
		payrollDTO.setStatus(jsonObject.get("status").getAsString());
		payrollDTO.setInvoiceNo(jsonObject.get("invoiceNo").getAsString());
		if(jsonObject.has("payrollDetailses")) {
			Set<PayrollDetailsDTO> setPayrollDetailsDTO = new HashSet<>();
			JsonArray jsonArrayPayrollDetails = jsonObject.getAsJsonArray("payrollDetailses");
			for(int i = 0; i < jsonArrayPayrollDetails.size(); i++) {
				JsonObject jsonObjectPayrollDetails = jsonArrayPayrollDetails.get(i).getAsJsonObject();
				PayrollDetailsDTO payrollDetailsDTO = new PayrollDetailsDTO();
				payrollDetailsDTO.setId(new BigInteger(jsonObjectPayrollDetails.get("id").getAsString()));
				if(jsonObjectPayrollDetails.has("removed")) {
					PayrollDetails payrollDetails = payrollDetailsDAO.findPayrollDetailsById(payrollDetailsDTO.getId());
			        if (payrollDetails != null) {
			           payrollDAO.remove(payrollDetails);
			        }
					continue;
				}
				if(jsonObjectPayrollDetails.has("edited") && jsonObjectPayrollDetails.get("edited").getAsString().equals("true")) {
					payrollDetailsDTO.setEdited(true);
				}
				payrollDetailsDTO.setEmployeeCode(jsonObjectPayrollDetails.get("employeeCode").getAsString());
				payrollDetailsDTO.setRoleTitle(jsonObjectPayrollDetails.get("roleTitle").getAsString());
				payrollDetailsDTO.setDivision(jsonObjectPayrollDetails.get("division").getAsString());
				payrollDetailsDTO.setTeam(jsonObjectPayrollDetails.get("team").getAsString());
				if(jsonObjectPayrollDetails.has("approvalCode") && !jsonObjectPayrollDetails.get("approvalCode").getAsString().isEmpty())
				{
					payrollDetailsDTO.setApprovalCode(jsonObjectPayrollDetails.get("approvalCode").getAsString());
				}
				else {
					payrollDetailsDTO.setApprovalCode(null);
				}
				
				if(jsonObjectPayrollDetails.has("laborBaseSalaryPayment"))
				{
					payrollDetailsDTO.setLaborBaseSalaryPayment(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborBaseSalaryPayment").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborBaseSalaryPayment(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborAdditionalPayment1")) {
					payrollDetailsDTO.setLaborAdditionalPayment1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborAdditionalPayment1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborAdditionalPayment1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborAdditionalPayment2"))
				{
					payrollDetailsDTO.setLaborAdditionalPayment2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborAdditionalPayment2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborAdditionalPayment2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborAdditionalPayment3")) {
					payrollDetailsDTO.setLaborAdditionalPayment3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborAdditionalPayment3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborAdditionalPayment3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborSocialInsuranceEmployer1")) {
					payrollDetailsDTO.setLaborSocialInsuranceEmployer1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborSocialInsuranceEmployer1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborSocialInsuranceEmployer1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborSocialInsuranceEmployer2")) {
					payrollDetailsDTO.setLaborSocialInsuranceEmployer2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborSocialInsuranceEmployer2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborSocialInsuranceEmployer2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborSocialInsuranceEmployer3"))
				{
					payrollDetailsDTO.setLaborSocialInsuranceEmployer3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborSocialInsuranceEmployer3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborSocialInsuranceEmployer3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborOtherPayment1"))
				{
					payrollDetailsDTO.setLaborOtherPayment1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborOtherPayment1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborOtherPayment1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborOtherPayment2"))
				{
					payrollDetailsDTO.setLaborOtherPayment2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborOtherPayment2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborOtherPayment2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("laborOtherPayment3"))
				{
					payrollDetailsDTO.setLaborOtherPayment3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("laborOtherPayment3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setLaborOtherPayment3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("totalLaborCostOriginal"))
				{
					payrollDetailsDTO.setTotalLaborCostOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("totalLaborCostOriginal").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setTotalLaborCostOriginal(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployer1"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployer1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployer2"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployer2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployer3"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployer3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployer3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployee1"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployee1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployee2"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployee2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionSocialInsuranceEmployee3"))
				{
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionSocialInsuranceEmployee3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionSocialInsuranceEmployee3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionWht"))
				{
					payrollDetailsDTO.setDeductionWht(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionWht").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionWht(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionOther1"))
				{
					payrollDetailsDTO.setDeductionOther1(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionOther1").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionOther1(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionOther2"))
				{
					payrollDetailsDTO.setDeductionOther2(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionOther2").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionOther2(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("deductionOther3"))
				{
					payrollDetailsDTO.setDeductionOther3(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("deductionOther3").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setDeductionOther3(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("totalDeductionOriginal"))
				{
					payrollDetailsDTO.setTotalDeductionOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("totalDeductionOriginal").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setTotalDeductionOriginal(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("netPaymentOriginal"))
				{
					payrollDetailsDTO.setNetPaymentOriginal(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("netPaymentOriginal").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setNetPaymentOriginal(BigDecimal.ZERO);
				}
				
				if(jsonObjectPayrollDetails.has("netPaymentConverted"))
				{
					payrollDetailsDTO.setNetPaymentConverted(NumberUtil.getBigDecimalScaleDown(2, jsonObjectPayrollDetails.get("netPaymentConverted").getAsString().replaceAll(",", "")));
				}
				else {
					payrollDetailsDTO.setNetPaymentConverted(BigDecimal.ZERO);
				}
				
				setPayrollDetailsDTO.add(payrollDetailsDTO);
			}
			payrollDTO.setPayrollDetailses(setPayrollDetailsDTO);
		}
		return payrollDTO;
	}

	@Override
	public BigInteger savePayroll(PayrollDTO payrollDTO, boolean isSaveFromImport) {
		/*
		 * validate before save
		 */
		if(payrollDTO.getMonth() == null || payrollDTO.getBookingDate() == null || 
		   payrollDTO.getAccountPayableCode() == null || payrollDTO.getAccountPayableName() == null || payrollDTO.getPayrollDetailses().isEmpty()) {
			return SAVE_ERROR_INVAILD_HEADER; //payroll header is invalid
		}
		else if(payrollDTO.getId().compareTo(BigInteger.ZERO) == 0 && payrollDAO.findPayrollObjByPayrollNo(payrollDTO.getPayrollNo()) != null) 
		{
			return SAVE_ERROR_PAYROLL_NO_IS_EXISTS;
		}
		else {
			if(payrollDTO.getPayrollDetailses().stream().anyMatch(p -> StringUtils.isEmpty(p.getEmployeeCode())))
			{
				return SAVE_ERROR_INVAILD_DETAILS;//payroll detail is invalid
			}
			BigDecimal totalLaborAmountOriginal = new BigDecimal("0");
			BigDecimal totalDeductionAmountOriginal = new BigDecimal("0");
			BigDecimal totalNetPaymentOriginal = new BigDecimal("0");
			BigDecimal totalNetPaymentConverted = new BigDecimal("0");
			for(PayrollDetailsDTO payrollDetailsDTO: payrollDTO.getPayrollDetailses()) {
				totalLaborAmountOriginal = totalLaborAmountOriginal.add(payrollDetailsDTO.getTotalLaborCostOriginal());
				totalDeductionAmountOriginal= totalDeductionAmountOriginal.add(payrollDetailsDTO.getTotalDeductionOriginal());
				totalNetPaymentOriginal = totalNetPaymentOriginal.add(payrollDetailsDTO.getNetPaymentOriginal());
				totalNetPaymentConverted = totalNetPaymentConverted.add(payrollDetailsDTO.getNetPaymentConverted());
			}
			//validate when import from excel
			if(isSaveFromImport){
				if(payrollDTO.getTotalNetPaymentConverted().compareTo(totalNetPaymentConverted) != 0)
				{
					return SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_CONVERTED; //Total Payroll Amount (Converted)" column: is not equal sum of "Payroll Amount (Converted)" of parent
				}
				else if(payrollDTO.getTotalNetPaymentOriginal().compareTo(totalNetPaymentOriginal) != 0) {
					return SAVE_ERROR_INVAILD_TOTAL_PAYROLL_AMOUNT_ORIGINAL; //Total Payroll Amount (Original)" column: is not equal sum of "Payroll Amount (Original)" of parent
				}
			}
			/*
			 * prepare data for save
			 */
			Payroll payroll = new Payroll();
			BeanUtils.copyProperties(payrollDTO, payroll);
			payroll.setPayrollDetailses(null);
			payroll.setCreatedDate(Calendar.getInstance());
			payroll.setModifiedDate(Calendar.getInstance());
			payroll.setTotalLaborCostOriginal(totalLaborAmountOriginal);
			payroll.setTotalDeductionOriginal(totalDeductionAmountOriginal);
			payroll.setTotalNetPaymentOriginal(totalNetPaymentOriginal);
			payroll.setTotalNetPaymentConverted(totalNetPaymentConverted);
			Payroll payrollSaved = savePayroll(payroll);
			/*
			 * save details
			 */
			//only allow to update payroll detail when status is not paid
			if(payrollSaved != null && Constant.API_NOT_PAID.equals(payrollSaved.getStatus())) {
				//insert payroll details
				for(PayrollDetailsDTO payrollDetailsDTO: payrollDTO.getPayrollDetailses()) {
					//only save new and edited item
					if(payrollDetailsDTO.getId().compareTo(BigInteger.ZERO) == 0 || payrollDetailsDTO.isEdited()){
						PayrollDetails payrollDetails = new PayrollDetails();
						BeanUtils.copyProperties(payrollDetailsDTO, payrollDetails);
						payrollDetails.setCreatedDate(Calendar.getInstance());
						payrollDetails.setModifiedDate(Calendar.getInstance());
						payrollDetails.setIsApproval(StringUtils.isEmpty(payrollDetails.getApprovalCode())? false: true);
						savePayrollPayrollDetailses(payrollSaved, payrollDetails);
					}
				}
				//insert payroll status
				PayrollPaymentStatus paymentStatus = null;
				if(payrollDTO.getId() != null && BigInteger.ZERO.compareTo(payrollDTO.getId()) < 0)
				{
					Set<PayrollPaymentStatus> set = paymentStatusDAO.findPayrollPaymentStatusByPayrollId(payrollDTO.getId());
					if(!set.isEmpty())
					{
						Iterator<PayrollPaymentStatus> iterator = set.iterator();
						paymentStatus = iterator.next();
					}
				}
				if(paymentStatus == null) {
					paymentStatus = new PayrollPaymentStatus();
					paymentStatus.setPayrollId(payrollSaved.getId());
					paymentStatus.setPaidNetPayment(BigDecimal.ZERO);
					paymentStatus.setUnpaidNetPayment(payrollSaved.getTotalNetPaymentOriginal());
				}
				else {
					paymentStatus.setUnpaidNetPayment(payrollSaved.getTotalNetPaymentOriginal());
				}
				paymentStatusDAO.store(paymentStatus);
			}
			return payrollSaved != null ? payrollSaved.getId() : BigInteger.ZERO;
		}
	}

	@Override
	public String getPayrollNo() {
		return payrollDAO.getPayrollNo();
	}

	@Override
	public Payroll findPayrollByPayrollNo(String payrollNo) {
		Set<Payroll> set = payrollDAO.findPayrollByPayrollNo(payrollNo);
		if(!set.isEmpty())
		{
			return set.stream().collect(Collectors.toList()).get(0);
		}
		return null;
	}

	@Override
	public void copyDomainToDTO(Payroll payroll, PayrollDTO payrollDTO) {
		BeanUtils.copyProperties(payroll, payrollDTO);
		if(!payroll.getPayrollDetailses().isEmpty()) {
			Set<PayrollDetailsDTO> set = new HashSet<>();
			for(PayrollDetails payrollDetails: payroll.getPayrollDetailses()) {
				PayrollDetailsDTO payrollDetailsDTO = new PayrollDetailsDTO();
				BeanUtils.copyProperties(payrollDetails, payrollDetailsDTO);
				set.add(payrollDetailsDTO);
			}
			payrollDTO.setPayrollDetailses(set);
		}
	}

	@Override
	public String findPayrollPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth,
								        String fromPayrollNo, String toPayrollNo, Integer orderColumn, String orderBy, int status) {
		List<PayrollPaymentDTO> list = payrollDAO.findPayrollPaging(start, end, fromMonth, toMonth, fromPayrollNo, toPayrollNo, orderColumn, orderBy, status);
		Map<String, String> invStatus = systemValueService.findSystemValueAsMapByType(Constant.PREFIX_API);
		Map<String, String> payStatus = systemValueService.findSystemValueAsMapByType(Constant.PREFIX_PAY);
		list.stream().forEach(p -> {
			List<BankStatement> bankStatements = new ArrayList<>(bankStatementDAO.findBankStatementByPaymentOrderNo(p.getPaymentOrderNo()));
			if(!bankStatements.isEmpty()) {
				p.setBankStatementIds(bankStatements.stream().map(b -> b.getId()).collect(Collectors.toList()));
			}
			//status to render class CSS
			p.setPaymentOrderStatusCode(p.getPaymentOrderStatus());
			p.setPayrollStatusCode(p.getPayrollStatus());
			//set status (code -> value)
			p.setPaymentOrderStatus(payStatus.get(p.getPaymentOrderStatus()));
			p.setPayrollStatus(invStatus.get(p.getPayrollStatus()));
			// set value for claim type 
			ClaimTypeMaster claimTypeMaster = claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(p.getClaimType());
			if(null != claimTypeMaster){
			    p.setClaimTypeVal(claimTypeMaster.getName());
			}
		});
		Map<String, Object> map = new HashMap<>();
        map.put("data", list);
        map.put("recordsFiltered", payrollDAO.countPayrollFilter(fromMonth, toMonth, fromPayrollNo, toPayrollNo, status));
        Gson gson = new Gson();
        return gson.toJson(map);
	}
	
	@Override
	public PayrollDTO clone(Payroll payrollOriginal) {
		if(payrollOriginal != null) {
			PayrollDTO payrollClone = new PayrollDTO();
			copyDomainToDTO(payrollOriginal, payrollClone);
			payrollClone.setId(BigInteger.ZERO);
			payrollClone.setPayrollNo(getPayrollNo());
			payrollClone.setStatus(Constant.API_NOT_PAID);
			payrollClone.getPayrollDetailses().stream().forEach(p -> {
				p.setId(BigInteger.ZERO);
				p.setIsApproval(false);
			});
			return payrollClone;
		}
		return null; 
	}

	@Override
	public Set<Payroll> findPayrollByImportKey(String importKey) throws DataAccessException {
		return payrollDAO.findPayrollByImportKey(importKey);
	}

	@Override
	public int deletePayrollByImportKey(String importKey) {
		Set<Payroll> set = findPayrollByImportKey(importKey);
		if(set.isEmpty()) {
			return 0;
		}
		else {
			set.stream().forEach(p -> {
				/*
				 * delete payroll payment status
				 */
				if(p.getId().compareTo(BigInteger.ZERO) > 0)
				{
					Set<PayrollPaymentStatus> setPayrollPaymentStatus = paymentStatusDAO.findPayrollPaymentStatusByPayrollId(p.getId());
					if(!setPayrollPaymentStatus.isEmpty())
					{
						Iterator<PayrollPaymentStatus> iterator = setPayrollPaymentStatus.iterator();
						PayrollPaymentStatus paymentStatus = iterator.next();
						paymentStatusDAO.remove(paymentStatus);
					}
				}
				/*
				 * delete payroll
				 */
				deletePayroll(p);
			});
			return 1;
		}
	}

	@Override
	public String getPayrollNo(Calendar cal) {
		return payrollDAO.getPayrollNo(cal);
	}
}
