
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;

/**
 * Spring service that handles CRUD requests for ArInvoiceReceiptStatus entities
 * 
 */
public interface ArInvoiceReceiptStatusService {

	/**
	* Return a count of all ArInvoiceReceiptStatus entity
	* 
	 */
	public Integer countArInvoiceReceiptStatuss();

	/**
	* Delete an existing ArInvoiceReceiptStatus entity
	* 
	 */
	public void deleteArInvoiceReceiptStatus(ArInvoiceReceiptStatus arinvoicereceiptstatus);

	/**
	* Save an existing ArInvoiceReceiptStatus entity
	* 
	 */
	public void saveArInvoiceReceiptStatus(ArInvoiceReceiptStatus arinvoicereceiptstatus_1);

	/**
	* Return all ArInvoiceReceiptStatus entity
	* 
	 */
	public List<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss(Integer startResult, Integer maxRows);

	/**
	* Load an existing ArInvoiceReceiptStatus entity
	* 
	 */
	public Set<ArInvoiceReceiptStatus> loadArInvoiceReceiptStatuss();

	/**
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id_1);
	
	public ArInvoiceReceiptStatus exstingArInvoiceReceiptStatus(BigInteger arInvoiceId);
}