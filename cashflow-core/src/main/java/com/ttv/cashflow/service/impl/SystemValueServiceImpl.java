package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.SystemValueDAO;

import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.service.SystemValueService;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for SystemValue entities
 * 
 */

@Service("SystemValueService")

@Transactional
public class SystemValueServiceImpl implements SystemValueService {

	/**
	 * DAO injected by Spring that manages SystemValue entities
	 * 
	 */
	@Autowired
	private SystemValueDAO systemValueDAO;

	/**
	 * Instantiates a new SystemValueServiceImpl.
	 *
	 */
	public SystemValueServiceImpl() {
	}

	/**
	 * Return a count of all SystemValue entity
	 * 
	 */
	@Transactional
	public Integer countSystemValues() {
		return ((Long) systemValueDAO.createQuerySingleResult("select count(o) from SystemValue o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public SystemValue findSystemValueByPrimaryKey(Integer id) {
		return systemValueDAO.findSystemValueByPrimaryKey(id);
	}

	/**
	 * Save an existing SystemValue entity
	 * 
	 */
	@Transactional
	public void saveSystemValue(SystemValue systemvalue) {
		SystemValue existingSystemValue = systemValueDAO.findSystemValueByPrimaryKey(systemvalue.getId());

		if (existingSystemValue != null) {
			if (existingSystemValue != systemvalue) {
				existingSystemValue.setId(systemvalue.getId());
				existingSystemValue.setType(systemvalue.getType());
				existingSystemValue.setCode(systemvalue.getCode());
				existingSystemValue.setValue(systemvalue.getValue());
				existingSystemValue.setDescription(systemvalue.getDescription());
			}
			systemvalue = systemValueDAO.store(existingSystemValue);
		} else {
			systemvalue = systemValueDAO.store(systemvalue);
		}
		systemValueDAO.flush();
	}

	/**
	 * Load an existing SystemValue entity
	 * 
	 */
	@Transactional
	public Set<SystemValue> loadSystemValues() {
		return systemValueDAO.findAllSystemValues();
	}

	/**
	 * Return all SystemValue entity
	 * 
	 */
	@Transactional
	public List<SystemValue> findAllSystemValues(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<SystemValue>(systemValueDAO.findAllSystemValues(startResult, maxRows));
	}

	/**
	 * Delete an existing SystemValue entity
	 * 
	 */
	@Transactional
	public void deleteSystemValue(SystemValue systemvalue) {
		systemValueDAO.remove(systemvalue);
		systemValueDAO.flush();
	}

	@Override
	public Map<String, String> findSystemValueAsMapByType(String type) {
		Map<String, String> ret = new LinkedHashMap<>();
        
        Iterator<SystemValue> iter = systemValueDAO.findSystemValueByType(type).iterator();
        while (iter.hasNext()) {
            SystemValue sysVal = iter.next();
            ret.put(sysVal.getCode(), sysVal.getValue());
        }

        return ret;
	}
}
