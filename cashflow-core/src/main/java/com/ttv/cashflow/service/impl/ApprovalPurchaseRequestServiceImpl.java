package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.ObjectError;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;
import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.service.ApprovalPurchaseRequestService;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.ResponseUtil;

/**
 * Spring service that handles CRUD requests for ApprovalPurchaseRequest entities
 * 
 */

@Service("ApprovalPurchaseRequestService")

@Transactional
public class ApprovalPurchaseRequestServiceImpl implements ApprovalPurchaseRequestService {

	/**
	 * DAO injected by Spring that manages ApprovalPurchaseRequest entities
	 * 
	 */
	@Autowired
	private ApprovalPurchaseRequestDAO approvalPurchaseRequestDAO;
	@Autowired
	private ApInvoiceClassDetailsDAO apInvoiceClassDetailsDAO;
	@Autowired
	private SystemValueDAO systemValueDAO;
	/**
	 * Instantiates a new ApprovalPurchaseRequestServiceImpl.
	 *
	 */
	public ApprovalPurchaseRequestServiceImpl() {
	}

	/**
	 * Return a count of all ApprovalPurchaseRequest entity
	 * 
	 */
	@Transactional
	public Integer countApprovalPurchaseRequests() {
		return ((Long) approvalPurchaseRequestDAO.createQuerySingleResult("select count(o) from ApprovalPurchaseRequest o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ApprovalPurchaseRequest entity
	 * 
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> loadApprovalPurchaseRequests() {
		return approvalPurchaseRequestDAO.findAllApprovalPurchaseRequests();
	}

	/**
	 * Return all ApprovalPurchaseRequest entity
	 * 
	 */
	@Transactional
	public List<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApprovalPurchaseRequest>(approvalPurchaseRequestDAO.findAllApprovalPurchaseRequests(startResult, maxRows));
	}

	/**
	 * Save an existing ApprovalPurchaseRequest entity
	 * 
	 */
	@Transactional
	public void saveApprovalPurchaseRequest(ApprovalPurchaseRequest approvalpurchaserequest) {
		ApprovalPurchaseRequest existingApprovalPurchaseRequest = approvalPurchaseRequestDAO.findApprovalPurchaseRequestByPrimaryKey(approvalpurchaserequest.getId());
		if (existingApprovalPurchaseRequest != null) {
			existingApprovalPurchaseRequest.setId(approvalpurchaserequest.getId());
			existingApprovalPurchaseRequest.setApprovalCode(approvalpurchaserequest.getApprovalCode());
			existingApprovalPurchaseRequest.setApprovalType(approvalpurchaserequest.getApprovalType());
			existingApprovalPurchaseRequest.setApplicationDate(approvalpurchaserequest.getApplicationDate());
			existingApprovalPurchaseRequest.setValidityFrom(approvalpurchaserequest.getValidityFrom());
			existingApprovalPurchaseRequest.setValidityTo(approvalpurchaserequest.getValidityTo());
			existingApprovalPurchaseRequest.setCurrencyCode(approvalpurchaserequest.getCurrencyCode());
			existingApprovalPurchaseRequest.setIncludeGstOriginalAmount(approvalpurchaserequest.getIncludeGstOriginalAmount());
			existingApprovalPurchaseRequest.setPurpose(approvalpurchaserequest.getPurpose());
			existingApprovalPurchaseRequest.setApplicant(approvalpurchaserequest.getApplicant());
			existingApprovalPurchaseRequest.setApprovedDate(approvalpurchaserequest.getApprovedDate());
			existingApprovalPurchaseRequest.setStatus(approvalpurchaserequest.getStatus());
			existingApprovalPurchaseRequest.setModifiedDate(Calendar.getInstance());
			approvalpurchaserequest = approvalPurchaseRequestDAO.store(existingApprovalPurchaseRequest);
		} else {
			approvalpurchaserequest.setCreatedDate(Calendar.getInstance());
			approvalpurchaserequest.setStatus(Constant.APPROVAL_STT_DRAFT);
			approvalpurchaserequest = approvalPurchaseRequestDAO.store(approvalpurchaserequest);
		}
		approvalPurchaseRequestDAO.flush();
	}

	/**
	 */
	@Transactional
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id) {
		return approvalPurchaseRequestDAO.findApprovalPurchaseRequestByPrimaryKey(id);
	}

	/**
	 * Delete an existing ApprovalPurchaseRequest entity
	 * 
	 */
	@Transactional
	public void deleteApprovalPurchaseRequest(ApprovalPurchaseRequest approvalpurchaserequest) {
		Set<ApInvoiceClassDetails> set = apInvoiceClassDetailsDAO.findApInvoiceClassDetailsByApprovalCode(approvalpurchaserequest.getApprovalCode());
		//don't remove approval assigned for invoice
		if(set.isEmpty())
		{
			approvalPurchaseRequestDAO.remove(approvalpurchaserequest);
		}
	}

	@Override
	public void deleteApprovalByIds(BigInteger[] ids) {
		for (BigInteger id: ids) {
			ApprovalPurchaseRequest approval = approvalPurchaseRequestDAO.findApprovalPurchaseRequestById(id);
	        if (approval != null && Constant.APPROVAL_STT_DRAFT.equals(approval.getStatus())) {
	           deleteApprovalPurchaseRequest(approval);
	        }
	    }
		approvalPurchaseRequestDAO.flush();
	}

	@Override
	public String findApprovalJson(Integer start, Integer end, String approvalType, String approvalCode,
								   Calendar validityFrom, Calendar validityTo, String status, Integer orderColumn, String orderBy) {
		Integer recordsTotal = approvalPurchaseRequestDAO.countApprovalAll();
        List<ApprovalPurchaseRequest> approvals = new java.util.ArrayList<>(approvalPurchaseRequestDAO.findApprovalPaging(start, end, approvalType, approvalCode, validityFrom, validityTo, status, orderColumn, orderBy));
        for(ApprovalPurchaseRequest approval: approvals) {
        	if(!StringUtils.isEmpty(approval.getStatus())) {
        		SystemValue systemValue = systemValueDAO.findSystemValueByTypeAndCode(Constant.APPROVAL_STT_TYPE, approval.getStatus());
        		if(systemValue != null)
        		{
        			approval.setStatusValue(systemValue.getValue());
        		}
        	}
        }
        Map<String, Object> map = new HashMap<>();
        map.put("data", approvals);
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", approvalPurchaseRequestDAO.countApprovalFilter(start, end, (approvalType != null && approvalType.isEmpty()) ? null : approvalType, "%" + (approvalCode == null ? "" : approvalCode) + "%", validityFrom, validityTo, status));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	@Override
	public Set<ApprovalPurchaseRequest> findApprovalByApprovalCode(String approvalCode) {
		return approvalPurchaseRequestDAO.findApprovalPurchaseRequestByApprovalCode(approvalCode.trim());
	}

	@Override
	public int checkExists(ApprovalPurchaseRequest approval) {
		return approvalPurchaseRequestDAO.checkExists(approval);
	}

	@Override
	public String saveApprovalPurchaseRequestForRestAPI(ApprovalPurchaseRequest approvalpurchaserequest) {
		List<ObjectError> list = new ArrayList<>();
		//validate some field
		if(approvalpurchaserequest.getValidityFrom().compareTo(approvalpurchaserequest.getValidityTo()) >= 0)
		{
			list.add(new ObjectError("Validity Date Error", "Validity Date To must greater than Validity Date From"));
			return ResponseUtil.createAjaxError(list);
		}
		else if(approvalpurchaserequest.getId().compareTo(BigInteger.ZERO) == 0 && !findApprovalByApprovalCode(approvalpurchaserequest.getApprovalCode()).isEmpty()) {
			list.add(new ObjectError("Approval No Exists", "Your Approval No is already exists."));
			return ResponseUtil.createAjaxError(list);
		}
		else 
		{
			//when update
			if (approvalpurchaserequest.getId().compareTo(BigInteger.ZERO) > 0) {
				ApprovalPurchaseRequest existApproval = findApprovalPurchaseRequestByPrimaryKey(approvalpurchaserequest.getId());
				if (existApproval != null)
				{
					//validate approval code
					Set<ApprovalPurchaseRequest> set = findApprovalByApprovalCode(approvalpurchaserequest.getApprovalCode());
					if(!set.isEmpty()){
						Iterator<ApprovalPurchaseRequest> iterator = set.iterator();
						while(iterator.hasNext()) {
							ApprovalPurchaseRequest appTemp = iterator.next();
							if(appTemp.getId().compareTo(existApproval.getId()) != 0) {
								list.add(new ObjectError("Approval No Exists", "Your Approval No is already exists."));
								return ResponseUtil.createAjaxError(list);
							}
						}
					}
					//only allow update when status is draft
					if(!Constant.APPROVAL_STT_DRAFT.equals(existApproval.getStatus())){
						list.add(new ObjectError("Not Allow to Update", "This Approval No is being proccessed or approved"));
						return ResponseUtil.createAjaxError(list);
					}
					else {
						saveApprovalPurchaseRequest(approvalpurchaserequest);
						return ResponseUtil.createAjaxSuccess(0);
					}
				}
				//no data found when update
				else {
					list.add(new ObjectError("Id Not Exists", "Your Approval Id is not exists to update."));
					return ResponseUtil.createAjaxError(list);
				}
			}
			//when insert
			saveApprovalPurchaseRequest(approvalpurchaserequest);
			return ResponseUtil.createAjaxSuccess(0);
		}
	}

	@Override
	public String updateStatusForRestAPI(String approvalCode, String status, Calendar approvedDate, String taskId) {
		List<ObjectError> list = new ArrayList<>();
		if(systemValueDAO.findSystemValueByTypeAndCode(Constant.APPROVAL_STT_TYPE, status) != null) {
			Set<ApprovalPurchaseRequest> set = findApprovalByApprovalCode(approvalCode);
			if(!set.isEmpty()){
				Iterator<ApprovalPurchaseRequest> iterator = set.iterator();
				ApprovalPurchaseRequest approvalPurchaseRequest = iterator.next();
				approvalPurchaseRequest.setStatus(status);
				approvalPurchaseRequest.setApprovedDate(approvedDate);
				approvalPurchaseRequest.setTaskId(taskId);
				saveApprovalPurchaseRequest(approvalPurchaseRequest);
				return ResponseUtil.createAjaxSuccess(0);
			}
			else {
				list.add(new ObjectError("Id Not Exists", "Your Approval Id is not exists to update."));
				return ResponseUtil.createAjaxError(list);
			}
		}
		else {
			list.add(new ObjectError("Id Not Exists", "Status code must be 001, 002 or 003"));
			return ResponseUtil.createAjaxError(list);
		}
	}
}
