
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.Tenant;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Tenant entities
 * 
 */
public interface TenantService {

	/**
	* Return a count of all Tenant entity
	* 
	 */
	public Integer countTenants();

	/**
	* Load an existing Tenant entity
	* 
	 */
	public Set<Tenant> loadTenants();

	/**
	 */
	public Tenant findTenantByPrimaryKey(Integer id);

	/**
	* Save an existing Tenant entity
	* 
	 */
	public void saveTenant(Tenant Tenant);

	/**
	* Return all Tenant entity
	* 
	 */
	public List<Tenant> findAllTenants(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Tenant entity
	* 
	 */
	public void deleteTenant(Tenant Tenant_1);
}