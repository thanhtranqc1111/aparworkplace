package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.PayeePayerMaster;

/**
 * Spring service that handles CRUD requests for PayeePayerMaster entities
 * 
 */
public interface PayeePayerMasterService {

    /**
     *
     * @param keyword
     *            the keyword
     * @return the sets the
     */
    public Set<PayeePayerMaster> findPayeePayerMasterByAccountAndName(
            String keyword);

    /**
     * Return a count of all PayeePayerMaster entity
     * 
     */
    public Integer countPayeePayerMasters();

    /**
     * Save an existing PayeePayerMaster entity
     * 
     */
    public void savePayeePayerMaster(PayeePayerMaster payeepayermaster);

    /**
     */
    public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code);

    /**
     * Delete an existing PayeePayerMaster entity
     * 
     */
    public void deletePayeePayerMaster(PayeePayerMaster payeepayermaster_1);

    /**
     * Load an existing PayeePayerMaster entity
     * 
     */
    public Set<PayeePayerMaster> loadPayeePayerMasters();

    /**
     * Return all PayeePayerMaster entity
     * 
     */
    public String findPayeeMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword);

    /**
     * Delete list existing PayeePayerMaster entity by codes
     * 
     */
    public void deletePayeePayerMasterByCodes(String codes[]);
    
    /**
     * @param code
     * @return
     */
    public String findPayeePayerMasterByPrimaryKeyJson(String code);

    public List<PayeePayerMaster> findTransactionPartyMastersByTransactionType(String transactionType);

    boolean checkExistingCustomerName(String customerName);
}