package com.ttv.cashflow.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.PermissionDAO;
import com.ttv.cashflow.dao.RolePermissionDAO;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.RolePermission;
import com.ttv.cashflow.dto.PermissionDTO;
import com.ttv.cashflow.service.PermissionService;

/**
 * Spring service that handles CRUD requests for Permission entities
 * 
 */

@Service("PermissionService")

@Transactional
public class PermissionServiceImpl implements PermissionService {

	/**
	 * DAO injected by Spring that manages Permission entities
	 * 
	 */
	@Autowired
	private PermissionDAO permissionDAO;

	/**
	 * DAO injected by Spring that manages RolePermission entities
	 * 
	 */
	@Autowired
	private RolePermissionDAO rolePermissionDAO;

	/**
	 * Instantiates a new PermissionServiceImpl.
	 *
	 */
	public PermissionServiceImpl() {
	}

	/**
	 * Save an existing Permission entity
	 * 
	 */
	@Transactional
	public void savePermission(Permission permission) {
		Permission existingPermission = permissionDAO.findPermissionByPrimaryKey(permission.getId());

		if (existingPermission != null) {
			if (existingPermission != permission) {
				existingPermission.setId(permission.getId());
				existingPermission.setCode(permission.getCode());
				existingPermission.setName(permission.getName());
				existingPermission.setDescription(permission.getDescription());
				existingPermission.setModifiedDate(Calendar.getInstance());
			}
			permission = permissionDAO.store(existingPermission);
		} else {
			permission.setCreatedDate(Calendar.getInstance());
			permission = permissionDAO.store(permission);
		}
		permissionDAO.flush();
	}

	/**
	 * Delete an existing Permission entity
	 * 
	 */
	@Transactional
	public void deletePermission(Permission permission) {
		permissionDAO.remove(permission);
		permissionDAO.flush();
	}

	/**
	 * Save an existing RolePermission entity
	 * 
	 */
	@Transactional
	public Permission savePermissionRolePermissions(Integer id, RolePermission related_rolepermissions) {
		Permission permission = permissionDAO.findPermissionByPrimaryKey(id, -1, -1);
		RolePermission existingrolePermissions = rolePermissionDAO.findRolePermissionByPrimaryKey(related_rolepermissions.getId());

		// copy into the existing record to preserve existing relationships
		if (existingrolePermissions != null) {
			existingrolePermissions.setId(related_rolepermissions.getId());
			existingrolePermissions.setCreatedDate(related_rolepermissions.getCreatedDate());
			existingrolePermissions.setModifiedDate(related_rolepermissions.getModifiedDate());
			related_rolepermissions = existingrolePermissions;
		}

		related_rolepermissions.setPermission(permission);
		permission.getRolePermissions().add(related_rolepermissions);
		related_rolepermissions = rolePermissionDAO.store(related_rolepermissions);
		rolePermissionDAO.flush();

		permission = permissionDAO.store(permission);
		permissionDAO.flush();

		return permission;
	}

	/**
	 */
	@Transactional
	public Permission findPermissionByPrimaryKey(Integer id) {
		return permissionDAO.findPermissionByPrimaryKey(id);
	}

	/**
	 * Return all Permission entity
	 * 
	 */
	@Transactional
	public List<Permission> findAllPermissions(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Permission>(permissionDAO.findAllPermissions(startResult, maxRows));
	}

	/**
	 * Load an existing Permission entity
	 * 
	 */
	@Transactional
	public Set<Permission> loadPermissions() {
		return permissionDAO.findAllPermissions();
	}

	/**
	 * Return a count of all Permission entity
	 * 
	 */
	@Transactional
	public Integer countPermissions() {
		return ((Long) permissionDAO.createQuerySingleResult("select count(o) from Permission o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing RolePermission entity
	 * 
	 */
	@Transactional
	public Permission deletePermissionRolePermissions(Integer permission_id, Integer related_rolepermissions_id) {
		RolePermission related_rolepermissions = rolePermissionDAO.findRolePermissionByPrimaryKey(related_rolepermissions_id, -1, -1);

		Permission permission = permissionDAO.findPermissionByPrimaryKey(permission_id, -1, -1);

		related_rolepermissions.setPermission(null);
		permission.getRolePermissions().remove(related_rolepermissions);

		rolePermissionDAO.remove(related_rolepermissions);
		rolePermissionDAO.flush();

		return permission;
	}

	public String findPermissionJson(String keyword) {
		// TODO Auto-generated method stub
		List<Permission> list = new java.util.ArrayList<Permission>(permissionDAO.findPermissionPaging(keyword, -1, -1));
        List<PermissionDTO> listDTO = new ArrayList<PermissionDTO>();
        for(Permission permission: list)
        {
        	PermissionDTO permissionDTO = new PermissionDTO();
        	BeanUtils.copyProperties(permission, permissionDTO);
        	listDTO.add(permissionDTO);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listDTO);
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	public List<Permission> parsePermissionJSON(JsonArray jsonArray) {
		// TODO Auto-generated method stub
		List<Permission> list = new ArrayList<Permission>();
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Permission permission = new Permission();
				permission.setId(jsonObject.get("id").getAsInt());
				if(jsonObject.has("name"))
					permission.setName(jsonObject.get("name").getAsString());
				if(jsonObject.has("description"))
					permission.setDescription(jsonObject.get("description").getAsString());
				if(jsonObject.has("code"))
					permission.setCode(jsonObject.get("code").getAsString());
				list.add(permission);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public void deletePermissionByIds(Integer[] ids) {
		// TODO Auto-generated method stub
		for (Integer id: ids) {
			Permission permission = permissionDAO.findPermissionById(id);
	        if (permission != null) {
	           deletePermission(permission);
	        }
	    }
	}
}
