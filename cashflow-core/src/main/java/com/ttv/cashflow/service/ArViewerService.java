
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ArViewer;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ArViewer entities
 * 
 */
public interface ArViewerService {

	/**
	* Load an existing ArViewer entity
	* 
	 */
	public Set<ArViewer> loadArViewers();

	/**
	* Delete an existing ArViewer entity
	* 
	 */
	public void deleteArViewer(ArViewer arViewer);

	/**
	* Save an existing ArViewer entity
	* 
	 */
	public void saveArViewer(ArViewer arViewer);

	/**
	* Return a count of all ArViewer entity
	* 
	 */
	public Integer countArViewers();

	/**
	 */
	public ArViewer findArViewerByPrimaryKey(BigInteger id);

	/**
	* Return all ArViewer entity
	* 
	 */
	public List<ArViewer> findAllArViewers(Integer startResult, Integer maxRows);
}