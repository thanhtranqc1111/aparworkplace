package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApprovalTypePurchaseMasterDAO;
import com.ttv.cashflow.domain.ApprovalTypePurchaseMaster;
import com.ttv.cashflow.service.ApprovalTypePurchaseMasterService;

/**
 * Spring service that handles CRUD requests for ApprovalTypePurchaseMaster entities
 * 
 */

@Service("ApprovalTypePurchaseMasterService")

@Transactional
public class ApprovalTypePurchaseMasterServiceImpl implements ApprovalTypePurchaseMasterService {

	/**
	 * DAO injected by Spring that manages ApprovalTypePurchaseMaster entities
	 * 
	 */
	@Autowired
	private ApprovalTypePurchaseMasterDAO approvalTypePurchaseMasterDAO;

	/**
	 * Instantiates a new ApprovalTypePurchaseMasterServiceImpl.
	 *
	 */
	public ApprovalTypePurchaseMasterServiceImpl() {
	}

	/**
	 * Return all ApprovalTypePurchaseMaster entity
	 * 
	 */
	@Transactional
	public List<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApprovalTypePurchaseMaster>(approvalTypePurchaseMasterDAO.findAllApprovalTypePurchaseMasters(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code) {
		return approvalTypePurchaseMasterDAO.findApprovalTypePurchaseMasterByPrimaryKey(code);
	}

	/**
	 * Load an existing ApprovalTypePurchaseMaster entity
	 * 
	 */
	@Transactional
	public Set<ApprovalTypePurchaseMaster> loadApprovalTypePurchaseMasters() {
		return approvalTypePurchaseMasterDAO.findAllApprovalTypePurchaseMasters();
	}

	/**
	 * Save an existing ApprovalTypePurchaseMaster entity
	 * 
	 */
	@Transactional
	public void saveApprovalTypePurchaseMaster(ApprovalTypePurchaseMaster approvaltypepurchasemaster) {
		ApprovalTypePurchaseMaster existingApprovalTypePurchaseMaster = approvalTypePurchaseMasterDAO.findApprovalTypePurchaseMasterByPrimaryKey(approvaltypepurchasemaster.getCode());

		if (existingApprovalTypePurchaseMaster != null) {
			if (existingApprovalTypePurchaseMaster != approvaltypepurchasemaster) {
				existingApprovalTypePurchaseMaster.setCode(approvaltypepurchasemaster.getCode());
				existingApprovalTypePurchaseMaster.setName(approvaltypepurchasemaster.getName());
			}
			approvaltypepurchasemaster = approvalTypePurchaseMasterDAO.store(existingApprovalTypePurchaseMaster);
		} else {
			approvaltypepurchasemaster = approvalTypePurchaseMasterDAO.store(approvaltypepurchasemaster);
		}
		approvalTypePurchaseMasterDAO.flush();
	}

	/**
	 * Delete an existing ApprovalTypePurchaseMaster entity
	 * 
	 */
	@Transactional
	public void deleteApprovalTypePurchaseMaster(ApprovalTypePurchaseMaster approvaltypepurchasemaster) {
		approvalTypePurchaseMasterDAO.remove(approvaltypepurchasemaster);
		approvalTypePurchaseMasterDAO.flush();
	}

	/**
	 * Return a count of all ApprovalTypePurchaseMaster entity
	 * 
	 */
	@Transactional
	public Integer countApprovalTypePurchaseMasters() {
		return ((Long) approvalTypePurchaseMasterDAO.createQuerySingleResult("select count(o) from ApprovalTypePurchaseMaster o").getSingleResult()).intValue();
	}

	@Override
	public void deleteApprovalTypeMasterByCodes(String[] codes) {
		for (String code : codes) {
			ApprovalTypePurchaseMaster approvalTypeMaster = approvalTypePurchaseMasterDAO.findApprovalTypePurchaseMasterByCode(code);
	        if (approvalTypeMaster != null) {
	            deleteApprovalTypePurchaseMaster(approvalTypeMaster);
	        }
	    }
	}

	@Override
	public String findApprovalTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		Integer recordsTotal = approvalTypePurchaseMasterDAO.countApprovalTypeAccountMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", approvalTypePurchaseMasterDAO.findApprovalTypeAccountMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", approvalTypePurchaseMasterDAO.countApprovalTypeAccountMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
