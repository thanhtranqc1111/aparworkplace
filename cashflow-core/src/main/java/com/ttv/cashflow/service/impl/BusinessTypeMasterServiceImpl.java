package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.BusinessTypeMasterDAO;
import com.ttv.cashflow.domain.BusinessTypeMaster;
import com.ttv.cashflow.service.BusinessTypeMasterService;

/**
 * Spring service that handles CRUD requests for BusinessTypeMaster entities
 * 
 */

@Service("BusinessTypeMasterService")

@Transactional
public class BusinessTypeMasterServiceImpl implements BusinessTypeMasterService {

	/**
	 * DAO injected by Spring that manages BusinessTypeMaster entities
	 * 
	 */
	@Autowired
	private BusinessTypeMasterDAO businessTypeMasterDAO;

	/**
	 * Instantiates a new BusinessTypeMasterServiceImpl.
	 *
	 */
	public BusinessTypeMasterServiceImpl() {
	}

	/**
	 */
	@Transactional
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code) {
		return businessTypeMasterDAO.findBusinessTypeMasterByPrimaryKey(code);
	}

	/**
	 * Return all BusinessTypeMaster entity
	 * 
	 */
	@Transactional
	public List<BusinessTypeMaster> findAllBusinessTypeMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<BusinessTypeMaster>(businessTypeMasterDAO.findAllBusinessTypeMasters(startResult, maxRows));
	}

	/**
	 * Return a count of all BusinessTypeMaster entity
	 * 
	 */
	@Transactional
	public Integer countBusinessTypeMasters() {
		return ((Long) businessTypeMasterDAO.createQuerySingleResult("select count(o) from BusinessTypeMaster o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing BusinessTypeMaster entity
	 * 
	 */
	@Transactional
	public Set<BusinessTypeMaster> loadBusinessTypeMasters() {
		return businessTypeMasterDAO.findAllBusinessTypeMasters();
	}

	/**
	 * Save an existing BusinessTypeMaster entity
	 * 
	 */
	@Transactional
	public void saveBusinessTypeMaster(BusinessTypeMaster businesstypemaster) {
		BusinessTypeMaster existingBusinessTypeMaster = businessTypeMasterDAO.findBusinessTypeMasterByPrimaryKey(businesstypemaster.getCode());

		if (existingBusinessTypeMaster != null) {
			if (existingBusinessTypeMaster != businesstypemaster) {
				existingBusinessTypeMaster.setCode(businesstypemaster.getCode());
				existingBusinessTypeMaster.setName(businesstypemaster.getName());
			}
			businesstypemaster = businessTypeMasterDAO.store(existingBusinessTypeMaster);
		} else {
			businesstypemaster = businessTypeMasterDAO.store(businesstypemaster);
		}
		businessTypeMasterDAO.flush();
	}

	/**
	 * Delete an existing BusinessTypeMaster entity
	 * 
	 */
	@Transactional
	public void deleteBusinessTypeMaster(BusinessTypeMaster businesstypemaster) {
		businessTypeMasterDAO.remove(businesstypemaster);
		businessTypeMasterDAO.flush();
	}
	
	public void deleteBusinessTypeMasterByCodes(String[] codes) {
	    for (String code : codes) {
	        BusinessTypeMaster businessTypemaster = businessTypeMasterDAO.findBusinessTypeMasterByCode(code);
	        if (businessTypemaster != null) {
	            deleteBusinessTypeMaster(businessTypemaster);
	        }
	    }
	}

	public String findBusinessTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		Integer recordsTotal = businessTypeMasterDAO.countBusinessTypeMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", businessTypeMasterDAO.findBusinessTypeMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", businessTypeMasterDAO.countBusinessTypeMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

    @Override
    public boolean checkExistingBusinessTypeCode(String businessTypeName) {
        return businessTypeMasterDAO.checkExistingBusinessTypeCode(businessTypeName);
    }
}
