package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ReimbursementPaymentStatusDAO;
import com.ttv.cashflow.domain.ReimbursementPaymentStatus;
import com.ttv.cashflow.service.ReimbursementPaymentStatusService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ReimbursementPaymentStatus entities
 * 
 */

@Service("ReimbursementPaymentStatusService")

@Transactional
public class ReimbursementPaymentStatusServiceImpl implements ReimbursementPaymentStatusService {

	/**
	 * DAO injected by Spring that manages ReimbursementPaymentStatus entities
	 * 
	 */
	@Autowired
	private ReimbursementPaymentStatusDAO reimbursementPaymentStatusDAO;

	/**
	 * Instantiates a new ReimbursementPaymentStatusServiceImpl.
	 *
	 */
	public ReimbursementPaymentStatusServiceImpl() {
	}

	/**
	 * Load an existing ReimbursementPaymentStatus entity
	 * 
	 */
	@Transactional
	public Set<ReimbursementPaymentStatus> loadReimbursementPaymentStatuss() {
		return reimbursementPaymentStatusDAO.findAllReimbursementPaymentStatuss();
	}

	/**
	 */
	@Transactional
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id) {
		return reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByPrimaryKey(id);
	}

	/**
	 * Delete an existing ReimbursementPaymentStatus entity
	 * 
	 */
	@Transactional
	public void deleteReimbursementPaymentStatus(ReimbursementPaymentStatus reimbursementpaymentstatus) {
		reimbursementPaymentStatusDAO.remove(reimbursementpaymentstatus);
		reimbursementPaymentStatusDAO.flush();
	}

	/**
	 * Save an existing ReimbursementPaymentStatus entity
	 * 
	 */
	@Transactional
	public void saveReimbursementPaymentStatus(ReimbursementPaymentStatus reimbursementpaymentstatus) {
		ReimbursementPaymentStatus existingReimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByPrimaryKey(reimbursementpaymentstatus.getId());

		if (existingReimbursementPaymentStatus != null) {
			if (existingReimbursementPaymentStatus != reimbursementpaymentstatus) {
				existingReimbursementPaymentStatus.setId(reimbursementpaymentstatus.getId());
				existingReimbursementPaymentStatus.setReimbursementId(reimbursementpaymentstatus.getReimbursementId());
				existingReimbursementPaymentStatus.setPaidReimbursementOriginalPayment(reimbursementpaymentstatus.getPaidReimbursementOriginalPayment());
				existingReimbursementPaymentStatus.setUnpaidReimbursementOriginalPayment(reimbursementpaymentstatus.getUnpaidReimbursementOriginalPayment());
			}
			reimbursementpaymentstatus = reimbursementPaymentStatusDAO.store(existingReimbursementPaymentStatus);
		} else {
			reimbursementpaymentstatus = reimbursementPaymentStatusDAO.store(reimbursementpaymentstatus);
		}
		reimbursementPaymentStatusDAO.flush();
	}

	/**
	 * Return a count of all ReimbursementPaymentStatus entity
	 * 
	 */
	@Transactional
	public Integer countReimbursementPaymentStatuss() {
		return ((Long) reimbursementPaymentStatusDAO.createQuerySingleResult("select count(o) from ReimbursementPaymentStatus o").getSingleResult()).intValue();
	}

	/**
	 * Return all ReimbursementPaymentStatus entity
	 * 
	 */
	@Transactional
	public List<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ReimbursementPaymentStatus>(reimbursementPaymentStatusDAO.findAllReimbursementPaymentStatuss(startResult, maxRows));
	}
}
