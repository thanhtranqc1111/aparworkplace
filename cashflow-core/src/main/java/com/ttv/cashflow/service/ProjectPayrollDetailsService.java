
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.PayrollDetails;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.domain.ProjectPayrollDetails;
import com.ttv.cashflow.dto.ProjectPaymentDTO;

/**
 * Spring service that handles CRUD requests for ProjectPayrollDetails entities
 * 
 */
public interface ProjectPayrollDetailsService {

	/**
	* Return all ProjectPayrollDetails entity
	* 
	 */
	public List<ProjectPayrollDetails> findAllProjectPayrollDetailss(Integer startResult, Integer maxRows);

	/**
	* Save an existing ProjectPayrollDetails entity
	* 
	 */
	public void saveProjectPayrollDetails(ProjectPayrollDetails projectpayrolldetails);

	/**
	* Delete an existing PayrollDetails entity
	* 
	 */
	public ProjectPayrollDetails deleteProjectPayrollDetailsPayrollDetails(BigInteger projectpayrolldetails_id, BigInteger related_payrolldetails_id);

	/**
	* Delete an existing ProjectPayrollDetails entity
	* 
	 */
	public void deleteProjectPayrollDetails(ProjectPayrollDetails projectpayrolldetails_1);

	/**
	* Return a count of all ProjectPayrollDetails entity
	* 
	 */
	public Integer countProjectPayrollDetailss();

	/**
	* Save an existing PayrollDetails entity
	* 
	 */
	public ProjectPayrollDetails saveProjectPayrollDetailsPayrollDetails(BigInteger id, PayrollDetails related_payrolldetails);

	/**
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id_1);

	/**
	* Load an existing ProjectPayrollDetails entity
	* 
	 */
	public Set<ProjectPayrollDetails> loadProjectPayrollDetailss();

	/**
	* Save an existing ProjectMaster entity
	* 
	 */
	public ProjectPayrollDetails saveProjectPayrollDetailsProjectMaster(BigInteger id_2, ProjectMaster related_projectmaster);

	/**
	* Delete an existing ProjectMaster entity
	* 
	 */
	public ProjectPayrollDetails deleteProjectPayrollDetailsProjectMaster(BigInteger projectpayrolldetails_id_1, String related_projectmaster_code);
	
	 /**
     * Return list ProjectPaymentDTO entity 
     */
    
    public List<ProjectPaymentDTO> getProjectPaymentDTOLst(java.util.Calendar monthFrom, java.util.Calendar monthTo);
    
	 /**
     * Save ProjectPayrollDetails entity
     */
    
    public void saveDataProjectPayroll(java.util.Calendar monthFrom, java.util.Calendar monthTo) throws Exception;

}