
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ApInvoicePaymentStatus entities
 * 
 */
public interface ApInvoicePaymentStatusService {

	/**
	 */
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id);

	/**
	* Load an existing ApInvoicePaymentStatus entity
	* 
	 */
	public Set<ApInvoicePaymentStatus> loadApInvoicePaymentStatuss();

	/**
	* Delete an existing ApInvoice entity
	* 
	 */
	public ApInvoicePaymentStatus deleteApInvoicePaymentStatusApInvoice(BigInteger apinvoicepaymentstatus_id, BigInteger related_apinvoice_id);

	/**
	* Save an existing ApInvoicePaymentStatus entity
	* 
	 */
	public void saveApInvoicePaymentStatus(ApInvoicePaymentStatus apinvoicepaymentstatus);

	/**
	* Delete an existing ApInvoicePaymentStatus entity
	* 
	 */
	public void deleteApInvoicePaymentStatus(ApInvoicePaymentStatus apinvoicepaymentstatus_1);

	/**
	* Return a count of all ApInvoicePaymentStatus entity
	* 
	 */
	public Integer countApInvoicePaymentStatuss();

	/**
	* Return all ApInvoicePaymentStatus entity
	* 
	 */
	public List<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss(Integer startResult, Integer maxRows);

}