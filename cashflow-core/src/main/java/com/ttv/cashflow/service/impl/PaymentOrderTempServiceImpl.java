package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.PaymentOrderTempDAO;
import com.ttv.cashflow.domain.PaymentOrderTemp;
import com.ttv.cashflow.service.PaymentOrderTempService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for PaymentOrderTemp entities
 * 
 */

@Service("PaymentOrderTempService")

@Transactional
public class PaymentOrderTempServiceImpl implements PaymentOrderTempService {

	/**
	 * DAO injected by Spring that manages PaymentOrderTemp entities
	 * 
	 */
	@Autowired
	private PaymentOrderTempDAO paymentOrderTempDAO;

	/**
	 * Instantiates a new PaymentOrderTempServiceImpl.
	 *
	 */
	public PaymentOrderTempServiceImpl() {
	}

	/**
	 * Load an existing PaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public Set<PaymentOrderTemp> loadPaymentOrderTemps() {
		return paymentOrderTempDAO.findAllPaymentOrderTemps();
	}

	/**
	 */
	@Transactional
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id) {
		return paymentOrderTempDAO.findPaymentOrderTempByPrimaryKey(id);
	}

	/**
	 * Save an existing PaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public void savePaymentOrderTemp(PaymentOrderTemp paymentordertemp) {
		PaymentOrderTemp existingPaymentOrderTemp = paymentOrderTempDAO.findPaymentOrderTempByPrimaryKey(paymentordertemp.getId());

		if (existingPaymentOrderTemp != null) {
			if (existingPaymentOrderTemp != paymentordertemp) {
				existingPaymentOrderTemp.setId(paymentordertemp.getId());
				existingPaymentOrderTemp.setBankName(paymentordertemp.getBankName());
				existingPaymentOrderTemp.setBankAccount(paymentordertemp.getBankAccount());
				existingPaymentOrderTemp.setValueDate(paymentordertemp.getValueDate());
				existingPaymentOrderTemp.setTotalAmountConvertedAmount(paymentordertemp.getTotalAmountConvertedAmount());
				existingPaymentOrderTemp.setBankRefNo(paymentordertemp.getBankRefNo());
				existingPaymentOrderTemp.setDescription(paymentordertemp.getDescription());
				existingPaymentOrderTemp.setPaymentApprovalNo(paymentordertemp.getPaymentApprovalNo());
				existingPaymentOrderTemp.setAccountPayableCode(paymentordertemp.getAccountPayableCode());
				existingPaymentOrderTemp.setAccountPayableName(paymentordertemp.getAccountPayableName());
				existingPaymentOrderTemp.setApInvoiceNo(paymentordertemp.getApInvoiceNo());
				existingPaymentOrderTemp.setApInvoiceOriginalCurrencyCode(paymentordertemp.getApInvoiceOriginalCurrencyCode());
				existingPaymentOrderTemp.setApInvoicePaymentOriginalAmount(paymentordertemp.getApInvoicePaymentOriginalAmount());
				existingPaymentOrderTemp.setApInvoicePaymentRate(paymentordertemp.getApInvoicePaymentRate());
				existingPaymentOrderTemp.setApInvoicePaymentConvertedAmount(paymentordertemp.getApInvoicePaymentConvertedAmount());
				existingPaymentOrderTemp.setApInvoicePayeeBankName(paymentordertemp.getApInvoicePayeeBankName());
				existingPaymentOrderTemp.setApInvoicePayeeBankAccNo(paymentordertemp.getApInvoicePayeeBankAccNo());
				existingPaymentOrderTemp.setPayrollNo(paymentordertemp.getPayrollNo());
				existingPaymentOrderTemp.setPayrollOriginalCurrencyCode(paymentordertemp.getPayrollOriginalCurrencyCode());
				existingPaymentOrderTemp.setPayrollPaymentOriginalAmount(paymentordertemp.getPayrollPaymentOriginalAmount());
				existingPaymentOrderTemp.setPayrollPaymentRate(paymentordertemp.getPayrollPaymentRate());
				existingPaymentOrderTemp.setPayrollPaymentConvertedAmount(paymentordertemp.getPayrollPaymentConvertedAmount());
				existingPaymentOrderTemp.setReimbursementNo(paymentordertemp.getReimbursementNo());
				existingPaymentOrderTemp.setReimbursementOriginalCurrencyCode(paymentordertemp.getReimbursementOriginalCurrencyCode());
				existingPaymentOrderTemp.setReimbursementPaymentOriginalAmount(paymentordertemp.getReimbursementPaymentOriginalAmount());
				existingPaymentOrderTemp.setReimbursementPaymentRate(paymentordertemp.getReimbursementPaymentRate());
				existingPaymentOrderTemp.setReimbursementPaymentConvertedAmount(paymentordertemp.getReimbursementPaymentConvertedAmount());
			}
			paymentordertemp = paymentOrderTempDAO.store(existingPaymentOrderTemp);
		} else {
			paymentordertemp = paymentOrderTempDAO.store(paymentordertemp);
		}
		paymentOrderTempDAO.flush();
	}

	/**
	 * Delete an existing PaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public void deletePaymentOrderTemp(PaymentOrderTemp paymentordertemp) {
		paymentOrderTempDAO.remove(paymentordertemp);
		paymentOrderTempDAO.flush();
	}

	/**
	 * Return all PaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public List<PaymentOrderTemp> findAllPaymentOrderTemps(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PaymentOrderTemp>(paymentOrderTempDAO.findAllPaymentOrderTemps(startResult, maxRows));
	}

	/**
	 * Return a count of all PaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public Integer countPaymentOrderTemps() {
		return ((Long) paymentOrderTempDAO.createQuerySingleResult("select count(o) from PaymentOrderTemp o").getSingleResult()).intValue();
	}
}
