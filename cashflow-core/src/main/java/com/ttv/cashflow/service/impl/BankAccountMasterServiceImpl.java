package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.service.BankAccountMasterService;

/**
 * Spring service that handles CRUD requests for BankAccountMaster entities
 * 
 */

@Service("BankAccountMasterService")

@Transactional
public class BankAccountMasterServiceImpl implements BankAccountMasterService {

	/**
	 * DAO injected by Spring that manages BankAccountMaster entities
	 * 
	 */
	@Autowired
	private BankAccountMasterDAO bankAccountMasterDAO;

	/**
	 * Instantiates a new BankAccountMasterServiceImpl.
	 *
	 */
	public BankAccountMasterServiceImpl() {
	}

	/**
	 * Return all BankAccountMaster entity
	 * 
	 */
	@Transactional
	public List<BankAccountMaster> findAllBankAccountMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<BankAccountMaster>(bankAccountMasterDAO.findAllBankAccountMasters(startResult, maxRows));
	}

	/**
	 * Save an existing BankAccountMaster entity
	 * 
	 */
	@Transactional
	public void saveBankAccountMaster(BankAccountMaster bankaccountmaster) {
		//init ending balance by beginning balance
		bankaccountmaster.setEndingBalance(bankaccountmaster.getBeginningBalance());
		BankAccountMaster existingBankAccountMaster = bankAccountMasterDAO.findBankAccountMasterByPrimaryKey(bankaccountmaster.getCode());
		if (existingBankAccountMaster != null) {
			if (existingBankAccountMaster != bankaccountmaster) {
				existingBankAccountMaster.setCode(bankaccountmaster.getCode());
				existingBankAccountMaster.setType(bankaccountmaster.getType());
				existingBankAccountMaster.setName(bankaccountmaster.getName());
				existingBankAccountMaster.setBankAccountNo(bankaccountmaster.getBankAccountNo());
				existingBankAccountMaster.setBankCode(bankaccountmaster.getBankCode());
				existingBankAccountMaster.setCurrencyCode(bankaccountmaster.getCurrencyCode());
				existingBankAccountMaster.setBeginningBalance(bankaccountmaster.getBeginningBalance());
				existingBankAccountMaster.setEndingBalance(bankaccountmaster.getEndingBalance());
			}
			bankaccountmaster = bankAccountMasterDAO.store(existingBankAccountMaster);
		} else {
			bankaccountmaster = bankAccountMasterDAO.store(bankaccountmaster);
		}
		bankAccountMasterDAO.flush();
	}

	/**
	 * Load an existing BankAccountMaster entity
	 * 
	 */
	@Transactional
	public Set<BankAccountMaster> loadBankAccountMasters() {
		return bankAccountMasterDAO.findAllBankAccountMasters();
	}

	/**
	 * Return a count of all BankAccountMaster entity
	 * 
	 */
	@Transactional
	public Integer countBankAccountMasters() {
		return ((Long) bankAccountMasterDAO.createQuerySingleResult("select count(o) from BankAccountMaster o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing BankAccountMaster entity
	 * 
	 */
	@Transactional
	public void deleteBankAccountMaster(BankAccountMaster bankaccountmaster) {
		bankAccountMasterDAO.remove(bankaccountmaster);
		bankAccountMasterDAO.flush();
	}

	/**
	 */
	@Transactional
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code) {
		return bankAccountMasterDAO.findBankAccountMasterByPrimaryKey(code);
	}

	public void deleteBankAccountMasterByCodes(String[] codes) {
		for (String code : codes) {
	        BankAccountMaster bankAccountMaster = bankAccountMasterDAO.findBankAccountMasterByCode(code);
	        if (bankAccountMaster != null) {
	            deleteBankAccountMaster(bankAccountMaster);
	        }
	    }
	}

	public String findBankAccountMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		Integer recordsTotal = bankAccountMasterDAO.countBankAccountMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", bankAccountMasterDAO.findBankAccountMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", bankAccountMasterDAO.countBankAccountMasterFilter(-1, -1, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode_1) throws DataAccessException {
		return bankAccountMasterDAO.findBankAccountMasterByBankCode("".equals(bankCode_1)?null:bankCode_1);
	}
}
