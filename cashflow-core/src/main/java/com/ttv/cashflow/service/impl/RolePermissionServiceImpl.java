package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.PermissionDAO;
import com.ttv.cashflow.dao.RoleDAO;
import com.ttv.cashflow.dao.RolePermissionDAO;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.RolePermission;
import com.ttv.cashflow.service.RolePermissionService;

/**
 * Spring service that handles CRUD requests for RolePermission entities
 * 
 */

@Service("RolePermissionService")

@Transactional
public class RolePermissionServiceImpl implements RolePermissionService {

	/**
	 * DAO injected by Spring that manages Permission entities
	 * 
	 */
	@Autowired
	private PermissionDAO permissionDAO;

	/**
	 * DAO injected by Spring that manages Role entities
	 * 
	 */
	@Autowired
	private RoleDAO roleDAO;

	/**
	 * DAO injected by Spring that manages RolePermission entities
	 * 
	 */
	@Autowired
	private RolePermissionDAO rolePermissionDAO;

	/**
	 * Instantiates a new RolePermissionServiceImpl.
	 *
	 */
	public RolePermissionServiceImpl() {
	}

	/**
	 * Return all RolePermission entity
	 * 
	 */
	@Transactional
	public List<RolePermission> findAllRolePermissions(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<RolePermission>(rolePermissionDAO.findAllRolePermissions(startResult, maxRows));
	}

	/**
	 * Save an existing RolePermission entity
	 * 
	 */
	@Transactional
	public void saveRolePermission(RolePermission rolepermission) {
		RolePermission existingRolePermission = rolePermissionDAO.findRolePermissionByPrimaryKey(rolepermission.getId());

		if (existingRolePermission != null) {
			if (existingRolePermission != rolepermission) {
				existingRolePermission.setId(rolepermission.getId());
				existingRolePermission.setCreatedDate(rolepermission.getCreatedDate());
				existingRolePermission.setModifiedDate(rolepermission.getModifiedDate());
			}
			rolepermission = rolePermissionDAO.store(existingRolePermission);
		} else {
			rolepermission = rolePermissionDAO.store(rolepermission);
		}
		rolePermissionDAO.flush();
	}

	/**
	 * Load an existing RolePermission entity
	 * 
	 */
	@Transactional
	public Set<RolePermission> loadRolePermissions() {
		return rolePermissionDAO.findAllRolePermissions();
	}

	/**
	 * Delete an existing Permission entity
	 * 
	 */
	@Transactional
	public RolePermission deleteRolePermissionPermission(Integer rolepermission_id, Integer related_permission_id) {
		RolePermission rolepermission = rolePermissionDAO.findRolePermissionByPrimaryKey(rolepermission_id, -1, -1);
		Permission related_permission = permissionDAO.findPermissionByPrimaryKey(related_permission_id, -1, -1);

		rolepermission.setPermission(null);
		related_permission.getRolePermissions().remove(rolepermission);
		rolepermission = rolePermissionDAO.store(rolepermission);
		rolePermissionDAO.flush();

		related_permission = permissionDAO.store(related_permission);
		permissionDAO.flush();

		permissionDAO.remove(related_permission);
		permissionDAO.flush();

		return rolepermission;
	}

	/**
	 * Delete an existing RolePermission entity
	 * 
	 */
	@Transactional
	public void deleteRolePermission(RolePermission rolepermission) {
		rolePermissionDAO.remove(rolepermission);
		rolePermissionDAO.flush();
	}

	/**
	 * Save an existing Role entity
	 * 
	 */
	@Transactional
	public RolePermission saveRolePermissionRole(Integer id, Role related_role) {
		RolePermission rolepermission = rolePermissionDAO.findRolePermissionByPrimaryKey(id, -1, -1);
		Role existingrole = roleDAO.findRoleByPrimaryKey(related_role.getId());

		// copy into the existing record to preserve existing relationships
		if (existingrole != null) {
			existingrole.setId(related_role.getId());
			existingrole.setName(related_role.getName());
			existingrole.setDescription(related_role.getDescription());
			existingrole.setCreatedDate(related_role.getCreatedDate());
			existingrole.setModifiedDate(related_role.getModifiedDate());
			related_role = existingrole;
		}

		rolepermission.setRole(related_role);
		related_role.getRolePermissions().add(rolepermission);
		rolepermission = rolePermissionDAO.store(rolepermission);
		rolePermissionDAO.flush();

		related_role = roleDAO.store(related_role);
		roleDAO.flush();

		return rolepermission;
	}

	/**
	 * Return a count of all RolePermission entity
	 * 
	 */
	@Transactional
	public Integer countRolePermissions() {
		return ((Long) rolePermissionDAO.createQuerySingleResult("select count(o) from RolePermission o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing Role entity
	 * 
	 */
	@Transactional
	public RolePermission deleteRolePermissionRole(Integer rolepermission_id, Integer related_role_id) {
		RolePermission rolepermission = rolePermissionDAO.findRolePermissionByPrimaryKey(rolepermission_id, -1, -1);
		Role related_role = roleDAO.findRoleByPrimaryKey(related_role_id, -1, -1);

		rolepermission.setRole(null);
		related_role.getRolePermissions().remove(rolepermission);
		rolepermission = rolePermissionDAO.store(rolepermission);
		rolePermissionDAO.flush();

		related_role = roleDAO.store(related_role);
		roleDAO.flush();

		roleDAO.remove(related_role);
		roleDAO.flush();

		return rolepermission;
	}

	/**
	 * Save an existing Permission entity
	 * 
	 */
	@Transactional
	public RolePermission saveRolePermissionPermission(Integer id, Permission related_permission) {
		RolePermission rolepermission = rolePermissionDAO.findRolePermissionByPrimaryKey(id, -1, -1);
		Permission existingpermission = permissionDAO.findPermissionByPrimaryKey(related_permission.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpermission != null) {
			existingpermission.setId(related_permission.getId());
			existingpermission.setName(related_permission.getName());
			existingpermission.setDescription(related_permission.getDescription());
			existingpermission.setCreatedDate(related_permission.getCreatedDate());
			existingpermission.setModifiedDate(related_permission.getModifiedDate());
			related_permission = existingpermission;
		}

		rolepermission.setPermission(related_permission);
		related_permission.getRolePermissions().add(rolepermission);
		rolepermission = rolePermissionDAO.store(rolepermission);
		rolePermissionDAO.flush();

		related_permission = permissionDAO.store(related_permission);
		permissionDAO.flush();

		return rolepermission;
	}

	/**
	 */
	@Transactional
	public RolePermission findRolePermissionByPrimaryKey(Integer id) {
		return rolePermissionDAO.findRolePermissionByPrimaryKey(id);
	}

	public Map<String, Integer> getMapRolePermission() {
		// TODO Auto-generated method stub
		Map<String, Integer> map = new HashMap<String, Integer>();
		Set<RolePermission> set = rolePermissionDAO.findAllRolePermissions();
		Iterator<RolePermission> iterator = set.iterator();
		while(iterator.hasNext())
		{
			RolePermission rolePermission = iterator.next();
			map.put(rolePermission.getRole().getId() + "," + rolePermission.getPermission().getId(), rolePermission.getId());
		}
		return map;
	}

	public int save(JsonArray jsonArray) {
		// TODO Auto-generated method stub
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Integer roleId = Integer.parseInt(jsonObject.get("roleId").getAsString());
				Integer permissionId = Integer.parseInt(jsonObject.get("permissionId").getAsString());
				int value = Integer.parseInt(jsonObject.get("value").getAsString());//0: unchecked, 1: checked
				Set<RolePermission> setRolePermission = rolePermissionDAO.findRolePermissionByRoleIdAndPermissionId(roleId, permissionId);
				if(setRolePermission.size() > 0)
				{
					if(value == 1)
					{
						continue;
					}
					else {
						for(RolePermission rolePermission: setRolePermission)
						{
							rolePermissionDAO.remove(rolePermission);
						}
					}
				}
				else {
					if(value == 0)
					{
						continue;
					}
					else 
					{
						RolePermission rolePermission = new RolePermission();
						rolePermission.setId(0);
						Role role = new Role();
						Permission permission = new Permission();
						role.setId(roleId);
						permission.setId(permissionId);
						rolePermission.setRole(role);
						rolePermission.setPermission(permission);
						rolePermissionDAO.store(rolePermission);
					}
				}
			}
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Set<RolePermission> findRolePermissionByRoleId(Integer roleId) {
		return rolePermissionDAO.findRolePermissionByRoleId(roleId);
	}
}
