
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.RolePermission;

/**
 * Spring service that handles CRUD requests for RolePermission entities
 * 
 */
public interface RolePermissionService {

	/**
	* Return all RolePermission entity
	* 
	 */
	public List<RolePermission> findAllRolePermissions(Integer startResult, Integer maxRows);

	/**
	* Save an existing RolePermission entity
	* 
	 */
	public void saveRolePermission(RolePermission rolepermission);

	/**
	* Load an existing RolePermission entity
	* 
	 */
	public Set<RolePermission> loadRolePermissions();

	/**
	* Delete an existing Permission entity
	* 
	 */
	public RolePermission deleteRolePermissionPermission(Integer rolepermission_id, Integer related_permission_id);

	/**
	* Delete an existing RolePermission entity
	* 
	 */
	public void deleteRolePermission(RolePermission rolepermission_1);

	/**
	* Save an existing Role entity
	* 
	 */
	public RolePermission saveRolePermissionRole(Integer id, Role related_role);

	/**
	* Return a count of all RolePermission entity
	* 
	 */
	public Integer countRolePermissions();

	/**
	* Delete an existing Role entity
	* 
	 */
	public RolePermission deleteRolePermissionRole(Integer rolepermission_id_1, Integer related_role_id);

	/**
	* Save an existing Permission entity
	* 
	 */
	public RolePermission saveRolePermissionPermission(Integer id_1, Permission related_permission);

	/**
	 */
	public RolePermission findRolePermissionByPrimaryKey(Integer id_2);
	public Map<String, Integer> getMapRolePermission();
	public int save(JsonArray jsonArray);
	public Set<RolePermission> findRolePermissionByRoleId(Integer roleId);
}