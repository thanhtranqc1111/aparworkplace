package com.ttv.cashflow.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.dto.PaymentOrderDTO;

/**
 * Spring service that handles CRUD requests for PaymentOrder entities
 * 
 */
public interface PaymentOrderService {

	/**
	* Load an existing PaymentOrder entity
	* 
	 */
	public Set<PaymentOrder> loadPaymentOrders();

	/**
     * Save an existing PaymentOrder entity
     * 
     * @return
     * 
     */
    public PaymentOrder savePaymentOrder(PaymentOrder paymentorder);

	/**
	* Save an existing ApInvoicePaymentDetails entity
	* 
	 */
	public PaymentOrder savePaymentOrderApInvoicePaymentDetailses(BigInteger id, ApInvoicePaymentDetails related_apinvoicepaymentdetailses);

	/**
	* Delete an existing ApInvoicePaymentDetails entity
	* 
	 */
	public PaymentOrder deletePaymentOrderApInvoicePaymentDetailses(BigInteger paymentorder_id, BigInteger related_apinvoicepaymentdetailses_id);

	/**
	 */
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id_1);

	/**
	* Return all PaymentOrder entity
	* 
	 */
	public List<PaymentOrder> findAllPaymentOrders(Integer startResult, Integer maxRows);

	/**
	* Return a count of all PaymentOrder entity
	* 
	 */
	public Integer countPaymentOrders();

	/**
	* Delete an existing PaymentOrder entity
	* 
	 */
	public void deletePaymentOrder(PaymentOrder paymentorder_1);
	
	/**

	 * 	
	 * @param searchPayment
	 */
	public List<PaymentInvoice> searchPayment(Integer startResult, Integer maxRow, String fromDate, String toDate,
            String bankRef, String invoiceNo, String status);
	
	/**
	 * 	
	 * @param searchPaymentCount
	 */
	public BigInteger searchPaymentCount(String fromDate, String toDate, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status);
	
	/**
	 * Search ApInvoice
	 */
	public Set<ApInvoice> searchApInvoice(Object... parameters);
	
	/**
	 * 
	 * @param paymentOrderNo
	 * @return
	 * @throws DataAccessException
	 */
	public String findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo, String status) throws DataAccessException;
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType) throws DataAccessException;
	
	/**
	 * check Duplicate PaymentOrder
	 * @param bankName
	 * @param bankAccount
	 * @param originalAmount
	 * @param rate
	 * @param convertedAmount
	 * @param valueDate
	 * @return
	 * @throws DataAccessException
	 */
    public boolean checkDuplicatePaymentOrder(String bankName, String bankAccount, BigDecimal convertedAmount, Calendar valueDate) throws DataAccessException;
    
    public PaymentOrder checkDuplicateByConvertedAmountAndValueDate(String bankName, String bankAccount, BigDecimal convertedAmount, Calendar valueDate);
	
	public String findPaymentOrderJSON(Integer startpage, Integer endPage, String fromDate, String toDate,
			String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status, Integer orderColumn, String orderBy);
	
	public String findPaymentOrderPayrollJSON(Integer startpage, Integer endPage, String fromDate, String toDate,
			String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn, String orderBy);
	
	public List<String> searchPayementOrderPayroll(Integer startResult, Integer maxRow, String fromDate, String toDate,
			String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn,
			String orderBy);
	
	public List<PaymentOrderDTO> getPaymentOrderNoBasedOnBankRef(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter);
	public PaymentOrder findPaymentOrderByPaymentOrderNoOnly(String paymentOrderNo);
	
	/**
	 * search ap invoice details
	 * @param parameters
	 * @return
	 */
	public Set<ApInvoice> searchApInvoiceDetails(Object... parameters);

    String getPaymentOrderNo(Calendar valueDate);
}