package com.ttv.cashflow.service.impl;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;

import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.service.PayeePayerTypeMasterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for PayeePayerTypeMaster entities
 * 
 */

@Service("PayeePayerTypeMasterService")

@Transactional
public class PayeePayerTypeMasterServiceImpl
        implements PayeePayerTypeMasterService {

    /**
     * DAO injected by Spring that manages PayeePayerTypeMaster entities
     * 
     */
    @Autowired
    private PayeePayerTypeMasterDAO payeePayerTypeMasterDAO;

    /**
     * Instantiates a new PayeePayerTypeMasterServiceImpl.
     *
     */
    public PayeePayerTypeMasterServiceImpl() {
    }

    /**
     * Return a count of all PayeePayerTypeMaster entity
     * 
     */
    @Transactional
    public Integer countPayeePayerTypeMasters() {
        return ((Long) payeePayerTypeMasterDAO
                .createQuerySingleResult(
                        "select count(o) from PayeePayerTypeMaster o")
                .getSingleResult()).intValue();
    }

    /**
     */
    @Transactional
    public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(
            String code) {
        return payeePayerTypeMasterDAO
                .findPayeePayerTypeMasterByPrimaryKey(code);
    }

    /**
     * Load an existing PayeePayerTypeMaster entity
     * 
     */
    @Transactional
    public Set<PayeePayerTypeMaster> loadPayeePayerTypeMasters() {
        return payeePayerTypeMasterDAO.findAllPayeePayerTypeMasters();
    }

    /**
     * Delete an existing PayeePayerTypeMaster entity
     * 
     */
    @Transactional
    public void deletePayeePayerTypeMaster(
            PayeePayerTypeMaster payeepayertypemaster) {
        payeePayerTypeMasterDAO.remove(payeepayertypemaster);
        payeePayerTypeMasterDAO.flush();
    }

    /**
     * Delete list existing PayeePayerTypeMaster entity by codes
     * 
     */
    @Transactional
    public void deletePayeePayerTypeMasterByCodes(String[] codes) {
        for (String code : codes) {
            PayeePayerTypeMaster payeepayertypemaster = payeePayerTypeMasterDAO
                    .findPayeePayerTypeMasterByCode(code);
            if (payeepayertypemaster != null) {
                deletePayeePayerTypeMaster(payeepayertypemaster);
            }
        }
    }

    /**
     * Save an existing PayeePayerTypeMaster entity
     * 
     */
    @Transactional
    public void savePayeePayerTypeMaster(
            PayeePayerTypeMaster payeepayertypemaster) {
        PayeePayerTypeMaster existingPayeePayerTypeMaster = payeePayerTypeMasterDAO
                .findPayeePayerTypeMasterByPrimaryKey(
                        payeepayertypemaster.getCode());

        if (existingPayeePayerTypeMaster != null) {
            if (existingPayeePayerTypeMaster != payeepayertypemaster) {
                existingPayeePayerTypeMaster
                        .setCode(payeepayertypemaster.getCode());
                existingPayeePayerTypeMaster
                        .setName(payeepayertypemaster.getName());
            }
            payeepayertypemaster = payeePayerTypeMasterDAO
                    .store(existingPayeePayerTypeMaster);
        } else {
            payeepayertypemaster = payeePayerTypeMasterDAO
                    .store(payeepayertypemaster);
        }
        payeePayerTypeMasterDAO.flush();
    }

    /**
     * Return all PayeePayerTypeMaster entity
     * 
     */
    @Transactional
    public List<PayeePayerTypeMaster> findAllPayeePayerTypeMasters(
            Integer startResult, Integer maxRows) {
        return new java.util.ArrayList<PayeePayerTypeMaster>(
                payeePayerTypeMasterDAO
                        .findAllPayeePayerTypeMasters(startResult, maxRows));
    }

    @Transactional
    public String findPayeeTypeMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword) {
        Integer recordsTotal = payeePayerTypeMasterDAO.findAllPayeePayerTypeMasters().size();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", payeePayerTypeMasterDAO.findPayeeTypeMasterPaging(start, end, orderColumn, orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", payeePayerTypeMasterDAO.countFilter(start, end, orderColumn, orderBy, keyword));
        Gson gson = new Gson();
        String jPayeeTypeMaster = gson.toJson(map);
        return jPayeeTypeMaster;
    }
}
