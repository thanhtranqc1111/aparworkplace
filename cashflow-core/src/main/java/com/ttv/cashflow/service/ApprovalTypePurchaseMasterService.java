
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ApprovalTypePurchaseMaster;

/**
 * Spring service that handles CRUD requests for ApprovalTypePurchaseMaster entities
 * 
 */
public interface ApprovalTypePurchaseMasterService {

	/**
	* Return all ApprovalTypePurchaseMaster entity
	* 
	 */
	public List<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters(Integer startResult, Integer maxRows);

	/**
	 */
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code);

	/**
	* Load an existing ApprovalTypePurchaseMaster entity
	* 
	 */
	public Set<ApprovalTypePurchaseMaster> loadApprovalTypePurchaseMasters();

	/**
	* Save an existing ApprovalTypePurchaseMaster entity
	* 
	 */
	public void saveApprovalTypePurchaseMaster(ApprovalTypePurchaseMaster approvaltypepurchasemaster);

	/**
	* Delete an existing ApprovalTypePurchaseMaster entity
	* 
	 */
	public void deleteApprovalTypePurchaseMaster(ApprovalTypePurchaseMaster approvaltypepurchasemaster_1);

	/**
	* Return a count of all ApprovalTypePurchaseMaster entity
	* 
	 */
	public Integer countApprovalTypePurchaseMasters();
	/**
	 */
	public void deleteApprovalTypeMasterByCodes(String codes[]);
	public String findApprovalTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}