package com.ttv.cashflow.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApprovalTypeSalesMasterDAO;
import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;
import com.ttv.cashflow.service.ApprovalTypeSalesMasterService;

/**
 * Spring service that handles CRUD requests for ApprovalTypeSalesMaster entities
 * 
 */

@Service("ApprovalTypeSalesMasterService")

@Transactional
public class ApprovalTypeSalesMasterServiceImpl implements ApprovalTypeSalesMasterService {

	/**
	 * DAO injected by Spring that manages ApprovalTypeSalesMaster entities
	 * 
	 */
	@Autowired
	private ApprovalTypeSalesMasterDAO approvalTypeSalesMasterDAO;

	/**
	 * Instantiates a new ApprovalTypeSalesMasterServiceImpl.
	 *
	 */
	public ApprovalTypeSalesMasterServiceImpl() {
	}

	/**
	 * Return all ApprovalTypeSalesMaster entity
	 * 
	 */
	@Transactional
	public List<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApprovalTypeSalesMaster>(approvalTypeSalesMasterDAO.findAllApprovalTypeSalesMasters(startResult, maxRows));
	}

	/**
	 * Return a count of all ApprovalTypeSalesMaster entity
	 * 
	 */
	@Transactional
	public Integer countApprovalTypeSalesMasters() {
		return ((Long) approvalTypeSalesMasterDAO.createQuerySingleResult("select count(o) from ApprovalTypeSalesMaster o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing ApprovalTypeSalesMaster entity
	 * 
	 */
	@Transactional
	public void saveApprovalTypeSalesMaster(ApprovalTypeSalesMaster approvaltypesalesmaster) {
		ApprovalTypeSalesMaster existingApprovalTypeSalesMaster = approvalTypeSalesMasterDAO.findApprovalTypeSalesMasterByPrimaryKey(approvaltypesalesmaster.getCode());

		if (existingApprovalTypeSalesMaster != null) {
			if (existingApprovalTypeSalesMaster != approvaltypesalesmaster) {
				existingApprovalTypeSalesMaster.setCode(approvaltypesalesmaster.getCode());
				existingApprovalTypeSalesMaster.setName(approvaltypesalesmaster.getName());
			}
			approvaltypesalesmaster = approvalTypeSalesMasterDAO.store(existingApprovalTypeSalesMaster);
		} else {
			approvaltypesalesmaster = approvalTypeSalesMasterDAO.store(approvaltypesalesmaster);
		}
		approvalTypeSalesMasterDAO.flush();
	}

	/**
	 * Delete an existing ApprovalTypeSalesMaster entity
	 * 
	 */
	@Transactional
	public void deleteApprovalTypeSalesMaster(ApprovalTypeSalesMaster approvaltypesalesmaster) {
		approvalTypeSalesMasterDAO.remove(approvaltypesalesmaster);
		approvalTypeSalesMasterDAO.flush();
	}

	/**
	 */
	@Transactional
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByPrimaryKey(String code) {
		return approvalTypeSalesMasterDAO.findApprovalTypeSalesMasterByPrimaryKey(code);
	}

	/**
	 * Load an existing ApprovalTypeSalesMaster entity
	 * 
	 */
	@Transactional
	public Set<ApprovalTypeSalesMaster> loadApprovalTypeSalesMasters() {
		return approvalTypeSalesMasterDAO.findAllApprovalTypeSalesMasters();
	}

	@Override
	public void deleteApprovalTypeMasterByCodes(String[] codes) {
		for (String code : codes) {
			ApprovalTypeSalesMaster approvalTypeMaster = approvalTypeSalesMasterDAO.findApprovalTypeSalesMasterByCode(code);
	        if (approvalTypeMaster != null) {
	            deleteApprovalTypeSalesMaster(approvalTypeMaster);
	        }
	    }
	}

	@Override
	public String findApprovalTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
		Integer recordsTotal = approvalTypeSalesMasterDAO.countApprovalTypeAccountMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", approvalTypeSalesMasterDAO.findApprovalTypeAccountMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", approvalTypeSalesMasterDAO.countApprovalTypeAccountMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
