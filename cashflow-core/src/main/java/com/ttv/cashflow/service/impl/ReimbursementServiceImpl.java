package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dao.ReimbursementDetailsDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentDetailsDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentStatusDAO;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementDetails;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.domain.ReimbursementPaymentStatus;
import com.ttv.cashflow.dto.ReimbursementDTO;
import com.ttv.cashflow.dto.ReimbursementDetailsDTO;
import com.ttv.cashflow.dto.ReimbursementPaymentDTO;
import com.ttv.cashflow.service.ReimbursementService;
import com.ttv.cashflow.service.SystemValueService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;

/**
 * Spring service that handles CRUD requests for Reimbursement entities
 * 
 */

@Service("ReimbursementService")

@Transactional
public class ReimbursementServiceImpl implements ReimbursementService {
	public static final BigInteger SAVE_ERROR_INVAILD_HEADER = new BigInteger("-1");
	public static final BigInteger SAVE_ERROR_INVAILD_DETAILS = new BigInteger("-2");
	public static final BigInteger SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_ORIGINAL = new BigInteger("-3");
	public static final BigInteger SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_CONVERTED = new BigInteger("-4");
	public static final BigInteger SAVE_ERROR_REIMBURSEMENT_NO_IS_EXISTS = new BigInteger("-5");
	/**
	 * DAO injected by Spring that manages Reimbursement entities
	 * 
	 */
	@Autowired
	private ReimbursementDAO reimbursementDAO;

	/**
	 * DAO injected by Spring that manages ReimbursementDetails entities
	 * 
	 */
	@Autowired
	private ReimbursementDetailsDAO reimbursementDetailsDAO;

	/**
	 * DAO injected by Spring that manages ReimbursementPaymentDetails entities
	 * 
	 */
	@Autowired
	private ReimbursementPaymentDetailsDAO reimbursementPaymentDetailsDAO;
	
	@Autowired
	private ReimbursementPaymentStatusDAO reimbursementPaymentStatusDAO;
	
	@Autowired
    private SystemValueService systemValueService;

	@Autowired
    private ClaimTypeMasterDAO claimTypeMasterDAO;
	
	@Autowired
	private BankStatementDAO bankStatementDAO;
	/**
	 * Instantiates a new ReimbursementServiceImpl.
	 *
	 */
	public ReimbursementServiceImpl() {
	}

	/**
	 * Return all Reimbursement entity
	 * 
	 */
	@Transactional
	public List<Reimbursement> findAllReimbursements(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Reimbursement>(reimbursementDAO.findAllReimbursements(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id) {
		return reimbursementDAO.findReimbursementByPrimaryKey(id);
	}

	/**
	 * Save an existing ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public Reimbursement saveReimbursementReimbursementPaymentDetailses(BigInteger id, ReimbursementPaymentDetails related_reimbursementpaymentdetailses) {
		Reimbursement reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(id, -1, -1);
		ReimbursementPaymentDetails existingreimbursementPaymentDetailses = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(related_reimbursementpaymentdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingreimbursementPaymentDetailses != null) {
			existingreimbursementPaymentDetailses.setId(related_reimbursementpaymentdetailses.getId());
			existingreimbursementPaymentDetailses.setPaymentOriginalAmount(related_reimbursementpaymentdetailses.getPaymentOriginalAmount());
			existingreimbursementPaymentDetailses.setPaymentRate(related_reimbursementpaymentdetailses.getPaymentRate());
			existingreimbursementPaymentDetailses.setPaymentConvertedAmount(related_reimbursementpaymentdetailses.getPaymentConvertedAmount());
			existingreimbursementPaymentDetailses.setVarianceAmount(related_reimbursementpaymentdetailses.getVarianceAmount());
			existingreimbursementPaymentDetailses.setVarianceAccountCode(related_reimbursementpaymentdetailses.getVarianceAccountCode());
			existingreimbursementPaymentDetailses.setCreatedDate(related_reimbursementpaymentdetailses.getCreatedDate());
			existingreimbursementPaymentDetailses.setModifiedDate(related_reimbursementpaymentdetailses.getModifiedDate());
			related_reimbursementpaymentdetailses = existingreimbursementPaymentDetailses;
		} else {
			related_reimbursementpaymentdetailses = reimbursementPaymentDetailsDAO.store(related_reimbursementpaymentdetailses);
			reimbursementPaymentDetailsDAO.flush();
		}

		related_reimbursementpaymentdetailses.setReimbursement(reimbursement);
		reimbursement.getReimbursementPaymentDetailses().add(related_reimbursementpaymentdetailses);
		related_reimbursementpaymentdetailses = reimbursementPaymentDetailsDAO.store(related_reimbursementpaymentdetailses);
		reimbursementPaymentDetailsDAO.flush();

		reimbursement = reimbursementDAO.store(reimbursement);
		reimbursementDAO.flush();

		return reimbursement;
	}

	/**
	 * Load an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public Set<Reimbursement> loadReimbursements() {
		return reimbursementDAO.findAllReimbursements();
	}

	/**
	 * Delete an existing ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public Reimbursement deleteReimbursementReimbursementDetailses(BigInteger reimbursement_id, BigInteger related_reimbursementdetailses_id) {
		ReimbursementDetails related_reimbursementdetailses = reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(related_reimbursementdetailses_id, -1, -1);

		Reimbursement reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(reimbursement_id, -1, -1);

		related_reimbursementdetailses.setReimbursement(null);
		reimbursement.getReimbursementDetailses().remove(related_reimbursementdetailses);

		reimbursementDetailsDAO.remove(related_reimbursementdetailses);
		reimbursementDetailsDAO.flush();

		return reimbursement;
	}

	/**
	 * Delete an existing ReimbursementPaymentDetails entity
	 * 
	 */
	@Transactional
	public Reimbursement deleteReimbursementReimbursementPaymentDetailses(BigInteger reimbursement_id, BigInteger related_reimbursementpaymentdetailses_id) {
		ReimbursementPaymentDetails related_reimbursementpaymentdetailses = reimbursementPaymentDetailsDAO.findReimbursementPaymentDetailsByPrimaryKey(related_reimbursementpaymentdetailses_id, -1, -1);

		Reimbursement reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(reimbursement_id, -1, -1);

		related_reimbursementpaymentdetailses.setReimbursement(null);
		reimbursement.getReimbursementPaymentDetailses().remove(related_reimbursementpaymentdetailses);

		reimbursementPaymentDetailsDAO.remove(related_reimbursementpaymentdetailses);
		reimbursementPaymentDetailsDAO.flush();

		return reimbursement;
	}

	/**
	 * Save an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public Reimbursement saveReimbursement(Reimbursement reimbursement) {
		Reimbursement existingReimbursement = reimbursementDAO.findReimbursementByPrimaryKey(reimbursement.getId());

		if (existingReimbursement != null) {
			if (existingReimbursement != reimbursement) {
				existingReimbursement.setId(reimbursement.getId());
				existingReimbursement.setEmployeeCode(reimbursement.getEmployeeCode());
				existingReimbursement.setEmployeeName(reimbursement.getEmployeeName());
				existingReimbursement.setReimbursementNo(reimbursement.getReimbursementNo());
				existingReimbursement.setOriginalCurrencyCode(reimbursement.getOriginalCurrencyCode());
				existingReimbursement.setFxRate(reimbursement.getFxRate());
				existingReimbursement.setBookingDate(reimbursement.getBookingDate());
				existingReimbursement.setMonth(reimbursement.getMonth());
				existingReimbursement.setClaimType(reimbursement.getClaimType());
				existingReimbursement.setAccountPayableCode(reimbursement.getAccountPayableCode());
				existingReimbursement.setAccountPayableName(reimbursement.getAccountPayableName());
				existingReimbursement.setDescription(reimbursement.getDescription());
				existingReimbursement.setIncludeGstTotalOriginalAmount(reimbursement.getIncludeGstTotalOriginalAmount());
				existingReimbursement.setIncludeGstTotalConvertedAmount(reimbursement.getIncludeGstTotalConvertedAmount());
				existingReimbursement.setScheduledPaymentDate(reimbursement.getScheduledPaymentDate());
				existingReimbursement.setStatus(reimbursement.getStatus());
				existingReimbursement.setInvoiceNo(reimbursement.getInvoiceNo());
				existingReimbursement.setExcludeGstTotalConvertedAmount(reimbursement.getExcludeGstTotalConvertedAmount());
				existingReimbursement.setExcludeGstTotalOriginalAmount(reimbursement.getExcludeGstTotalOriginalAmount());
				existingReimbursement.setGstType(reimbursement.getGstType());
				existingReimbursement.setGstRate(reimbursement.getGstRate());
				existingReimbursement.setGstConvertedAmount(reimbursement.getGstConvertedAmount());
				existingReimbursement.setGstOriginalAmount(reimbursement.getGstOriginalAmount());
				existingReimbursement.setCreatedDate(reimbursement.getCreatedDate());
				existingReimbursement.setModifiedDate(reimbursement.getModifiedDate());
			}
			reimbursement = reimbursementDAO.store(existingReimbursement);
		} else {
			reimbursement = reimbursementDAO.store(reimbursement);
		}
		reimbursementDAO.flush();
		return reimbursement;
	}

	/**
	 * Save an existing ReimbursementDetails entity
	 * 
	 */
	@Transactional
	public Reimbursement saveReimbursementReimbursementDetailses(BigInteger id, ReimbursementDetails related_reimbursementdetailses) {
		Reimbursement reimbursement = reimbursementDAO.findReimbursementByPrimaryKey(id, -1, -1);
		ReimbursementDetails existingreimbursementDetailses = reimbursementDetailsDAO.findReimbursementDetailsByPrimaryKey(related_reimbursementdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingreimbursementDetailses != null) {
			existingreimbursementDetailses.setId(related_reimbursementdetailses.getId());
			existingreimbursementDetailses.setAccountCode(related_reimbursementdetailses.getAccountCode());
			existingreimbursementDetailses.setAccountName(related_reimbursementdetailses.getAccountName());
			existingreimbursementDetailses.setProjectCode(related_reimbursementdetailses.getProjectCode());
			existingreimbursementDetailses.setProjectName(related_reimbursementdetailses.getProjectName());
			existingreimbursementDetailses.setApprovalCode(related_reimbursementdetailses.getApprovalCode());
			existingreimbursementDetailses.setIsApproval(related_reimbursementdetailses.getIsApproval());
			existingreimbursementDetailses.setDescription(related_reimbursementdetailses.getDescription());
			existingreimbursementDetailses.setIncludeGstOriginalAmount(related_reimbursementdetailses.getIncludeGstOriginalAmount());
			existingreimbursementDetailses.setIncludeGstConvertedAmount(related_reimbursementdetailses.getIncludeGstConvertedAmount());
			existingreimbursementDetailses.setCreatedDate(related_reimbursementdetailses.getCreatedDate());
			existingreimbursementDetailses.setModifiedDate(related_reimbursementdetailses.getModifiedDate());
			existingreimbursementDetailses.setExcludeGstConvertedAmount(related_reimbursementdetailses.getExcludeGstConvertedAmount());
			existingreimbursementDetailses.setExcludeGstOriginalAmount(related_reimbursementdetailses.getExcludeGstOriginalAmount());
			existingreimbursementDetailses.setGstConvertedAmount(related_reimbursementdetailses.getGstConvertedAmount());
			existingreimbursementDetailses.setGstOriginalAmount(related_reimbursementdetailses.getGstOriginalAmount());
			related_reimbursementdetailses = existingreimbursementDetailses;
		} else {
			related_reimbursementdetailses = reimbursementDetailsDAO.store(related_reimbursementdetailses);
			reimbursementDetailsDAO.flush();
		}

		related_reimbursementdetailses.setReimbursement(reimbursement);
		reimbursement.getReimbursementDetailses().add(related_reimbursementdetailses);
		related_reimbursementdetailses = reimbursementDetailsDAO.store(related_reimbursementdetailses);
		reimbursementDetailsDAO.flush();

		reimbursement = reimbursementDAO.store(reimbursement);
		reimbursementDAO.flush();

		return reimbursement;
	}

	/**
	 * Delete an existing Reimbursement entity
	 * 
	 */
	@Transactional
	public void deleteReimbursement(Reimbursement reimbursement) {
		reimbursementDAO.remove(reimbursement);
		reimbursementDAO.flush();
	}

	/**
	 * Return a count of all Reimbursement entity
	 * 
	 */
	@Transactional
	public Integer countReimbursements() {
		return ((Long) reimbursementDAO.createQuerySingleResult("select count(o) from Reimbursement o").getSingleResult()).intValue();
	}

	@Override
	public Reimbursement findReimbursementByReimbursementNo(String reimbursementNo) {
		Set<Reimbursement> setReimbursement = reimbursementDAO.findReimbursementByReimbursementNo(reimbursementNo);
		if(!setReimbursement.isEmpty()) {
			Iterator<Reimbursement> iterator = setReimbursement.iterator();
			return iterator.next();
		}
		return null;
	}

	@Override
	public String getReimbursementNo() {
		return reimbursementDAO.getReimbursementNo();
	}

	@Override
	public void copyDomainToDTO(Reimbursement reimbursement, ReimbursementDTO reimbursementDTO) {
		BeanUtils.copyProperties(reimbursement, reimbursementDTO);
		if(!reimbursement.getReimbursementDetailses().isEmpty()) {
			Set<ReimbursementDetailsDTO> set = new HashSet<>();
			for(ReimbursementDetails re: reimbursement.getReimbursementDetailses()) {
				ReimbursementDetailsDTO reimbursementDetailsDTO = new ReimbursementDetailsDTO();
				BeanUtils.copyProperties(re, reimbursementDetailsDTO);
				set.add(reimbursementDetailsDTO);
			}
			reimbursementDTO.setReimbursementDetailses(set);
		}
	}

	@Override
	public ReimbursementDTO parseReimbursementJSON(JsonObject jsonObject) {
		try {
			ReimbursementDTO reimbursementDTO = new ReimbursementDTO();
			reimbursementDTO.setId(new BigInteger(jsonObject.get("id").getAsString()));
			reimbursementDTO.setReimbursementNo(jsonObject.get("reimbursementNo").getAsString());
			reimbursementDTO.setBookingDate(CalendarUtil.getCalendar(jsonObject.get("bookingDate").getAsString()));
			reimbursementDTO.setClaimType(jsonObject.get("claimType").getAsString());
			reimbursementDTO.setAccountPayableCode(jsonObject.get("accountPayableCode").getAsString());
			reimbursementDTO.setAccountPayableName(jsonObject.get("accountPayableName").getAsString());
			reimbursementDTO.setStatus(jsonObject.get("status").getAsString());
			reimbursementDTO.setDescription(jsonObject.get("description").getAsString());
			reimbursementDTO.setIncludeGstTotalConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("includeGstTotalConvertedAmount").getAsString().replaceAll(",", "")));
			reimbursementDTO.setIncludeGstTotalOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("includeGstTotalOriginalAmount").getAsString().replaceAll(",", "")));
			reimbursementDTO.setFxRate(NumberUtil.getBigDecimalScaleDown(-1, jsonObject.get("fxRate").getAsString().replaceAll(",", "")));
			reimbursementDTO.setOriginalCurrencyCode(jsonObject.get("originalCurrencyCode").getAsString());
			reimbursementDTO.setEmployeeName(jsonObject.get("employeeName").getAsString());
			reimbursementDTO.setEmployeeCode(jsonObject.get("employeeCode").getAsString());
			reimbursementDTO.setScheduledPaymentDate(CalendarUtil.getCalendar(jsonObject.get("paymentScheduleDate").getAsString()));
			reimbursementDTO.setMonth(CalendarUtil.getCalendar(jsonObject.get("month").getAsString(), "MM/yyyy"));
			reimbursementDTO.setInvoiceNo(jsonObject.get("invoiceNo").getAsString());
			reimbursementDTO.setGstType(jsonObject.get("gstType").getAsString());
			reimbursementDTO.setGstRate(NumberUtil.getBigDecimalScaleDown(-1, jsonObject.get("gstRate").getAsString().replaceAll(",", "")));
			reimbursementDTO.setExcludeGstTotalConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("excludeGstTotalConvertedAmount").getAsString().replaceAll(",", "")));
			reimbursementDTO.setExcludeGstTotalOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("excludeGstTotalOriginalAmount").getAsString().replaceAll(",", "")));
			reimbursementDTO.setGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("gstConvertedAmount").getAsString().replaceAll(",", "")));
			reimbursementDTO.setGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("gstOriginalAmount").getAsString().replaceAll(",", "")));
			if(jsonObject.has("reimbursementDetailses")) {
				Set<ReimbursementDetailsDTO> setReimbursementDetailsDTO = new HashSet<>();
				JsonArray jsonArrayReimbursementDetails = jsonObject.getAsJsonArray("reimbursementDetailses");
				for(int i = 0; i < jsonArrayReimbursementDetails.size(); i++) {
					JsonObject jsonObjectReimbursementDetails = jsonArrayReimbursementDetails.get(i).getAsJsonObject();
					ReimbursementDetailsDTO reimbursementDetailsDTO = new ReimbursementDetailsDTO();
					reimbursementDetailsDTO.setId(new BigInteger(jsonObjectReimbursementDetails.get("id").getAsString()));
					if(jsonObjectReimbursementDetails.has("removed")) {
						ReimbursementDetails reimbursementDetails = reimbursementDetailsDAO.findReimbursementDetailsById(reimbursementDetailsDTO.getId());
				        if (reimbursementDetails != null) {
				           reimbursementDAO.remove(reimbursementDetails);
				        }
						continue;
					}
					reimbursementDetailsDTO.setAccountCode(jsonObjectReimbursementDetails.get("accountCode").getAsString());
					reimbursementDetailsDTO.setAccountName(jsonObjectReimbursementDetails.get("accountName").getAsString());
					reimbursementDetailsDTO.setProjectCode(jsonObjectReimbursementDetails.get("projectCode").getAsString());
					reimbursementDetailsDTO.setProjectName(jsonObjectReimbursementDetails.get("projectName").getAsString());
					reimbursementDetailsDTO.setDescription(jsonObjectReimbursementDetails.get("description").getAsString());
					if(jsonObjectReimbursementDetails.has("approvalCode") && !jsonObjectReimbursementDetails.get("approvalCode").getAsString().isEmpty())
					{
						reimbursementDetailsDTO.setApprovalCode(jsonObjectReimbursementDetails.get("approvalCode").getAsString());
					}
					else {
						reimbursementDetailsDTO.setApprovalCode(null);
					}
					
					if(jsonObjectReimbursementDetails.has("includeGstOriginalAmount"))
					{
						reimbursementDetailsDTO.setIncludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("includeGstOriginalAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setIncludeGstOriginalAmount(BigDecimal.ZERO);
					}
					
					if(jsonObjectReimbursementDetails.has("includeGstConvertedAmount"))
					{
						reimbursementDetailsDTO.setIncludeGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("includeGstConvertedAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setIncludeGstConvertedAmount(BigDecimal.ZERO);
					}
					
					if(jsonObjectReimbursementDetails.has("gstOriginalAmount"))
					{
						reimbursementDetailsDTO.setGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("gstOriginalAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setGstOriginalAmount(BigDecimal.ZERO);
					}
					
					if(jsonObjectReimbursementDetails.has("gstConvertedAmount"))
					{
						reimbursementDetailsDTO.setGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("gstConvertedAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setGstConvertedAmount(BigDecimal.ZERO);
					}
					
					if(jsonObjectReimbursementDetails.has("excludeGstOriginalAmount"))
					{
						reimbursementDetailsDTO.setExcludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("excludeGstOriginalAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setExcludeGstOriginalAmount(BigDecimal.ZERO);
					}
					
					if(jsonObjectReimbursementDetails.has("excludeGstConvertedAmount"))
					{
						reimbursementDetailsDTO.setExcludeGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObjectReimbursementDetails.get("excludeGstConvertedAmount").getAsString().replaceAll(",", "")));
					}
					else {
						reimbursementDetailsDTO.setExcludeGstConvertedAmount(BigDecimal.ZERO);
					}
					setReimbursementDetailsDTO.add(reimbursementDetailsDTO);
				}
				reimbursementDTO.setReimbursementDetailses(setReimbursementDetailsDTO);
			}
			return reimbursementDTO;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public BigInteger saveReimbursement(ReimbursementDTO reimbursementDTO, boolean isSaveFromImport) {
		/*
		 * validate before save
		 */
		if(reimbursementDTO.getBookingDate() == null || reimbursementDTO.getFxRate() == null ||
		   reimbursementDTO.getAccountPayableCode() == null || reimbursementDTO.getAccountPayableName() == null || reimbursementDTO.getReimbursementDetailses().isEmpty()) {
			return SAVE_ERROR_INVAILD_HEADER; //Reimbursement header is invalid
		}
		else if(reimbursementDTO.getId().compareTo(BigInteger.ZERO) == 0 && findReimbursementByReimbursementNo(reimbursementDTO.getReimbursementNo()) != null) {
			return SAVE_ERROR_REIMBURSEMENT_NO_IS_EXISTS;
		}
		else if(reimbursementDTO.getReimbursementDetailses().stream().anyMatch(re -> StringUtils.isEmpty(re.getProjectName()) || StringUtils.isEmpty(re.getAccountCode())))
		{
			return SAVE_ERROR_INVAILD_DETAILS; //Reimbursement detail is invalid
		}
		BigDecimal excludeGstTotalAmountOriginal = new BigDecimal("0");
		BigDecimal includeGstTotalAmountConverted = new BigDecimal("0");
		for(ReimbursementDetailsDTO reimbursementDetailsDTO: reimbursementDTO.getReimbursementDetailses()) {
			excludeGstTotalAmountOriginal = excludeGstTotalAmountOriginal.add(reimbursementDetailsDTO.getExcludeGstOriginalAmount());
			includeGstTotalAmountConverted = includeGstTotalAmountConverted.add(reimbursementDetailsDTO.getIncludeGstConvertedAmount());
		}
		//validate when import from excel
		if(isSaveFromImport) {
			if(reimbursementDTO.getIncludeGstTotalConvertedAmount().compareTo(includeGstTotalAmountConverted) != 0)
			{
				return SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_CONVERTED; //Total Reimbursement Amount (Converted)" column: is not equal sum of "Reimbursement Amount (Converted)" of parent
			}
			else if(reimbursementDTO.getExcludeGstTotalOriginalAmount().compareTo(excludeGstTotalAmountOriginal) != 0) {
				return SAVE_ERROR_INVAILD_TOTAL_REIMBURSEMENT_AMOUNT_ORIGINAL; //Total Reimbursement Amount (Original)" column: is not equal sum of "Reimbursement Amount (Original)" of parent
			}
		}
		/*
		 * prepare data for save
		 */
		Reimbursement reimbursement = new Reimbursement();
		BeanUtils.copyProperties(reimbursementDTO, reimbursement);
		reimbursement.setReimbursementDetailses(null);
		reimbursement.setCreatedDate(Calendar.getInstance());
		reimbursement.setModifiedDate(Calendar.getInstance());
		reimbursement.setIncludeGstTotalConvertedAmount(includeGstTotalAmountConverted);
		reimbursement.setIncludeGstTotalOriginalAmount(excludeGstTotalAmountOriginal);
		Reimbursement reimbursementSaved = saveReimbursement(reimbursement);
		/*
		 * save details
		 */
		if(reimbursementSaved != null) {
			//only allow to update reimbursement detail when status is not paid
			if(Constant.API_NOT_PAID.equals(reimbursementSaved.getStatus())) {
				for(ReimbursementDetailsDTO reimbursementDetailsDTO: reimbursementDTO.getReimbursementDetailses()) {
					ReimbursementDetails reimbursementDetails = new ReimbursementDetails();
					BeanUtils.copyProperties(reimbursementDetailsDTO, reimbursementDetails);
					reimbursementDetails.setCreatedDate(Calendar.getInstance());
					reimbursementDetails.setModifiedDate(Calendar.getInstance());
					reimbursementDetails.setIsApproval(StringUtils.isEmpty(reimbursementDetailsDTO.getApprovalCode())? false: true);
					saveReimbursementReimbursementDetailses(reimbursementSaved.getId(), reimbursementDetails);
				}
			}
			
			//insert reimbursement status
			ReimbursementPaymentStatus reimbursementPaymentStatus = null;
			if(BigInteger.ZERO.compareTo(reimbursementDTO.getId()) < 0) {
				Set<ReimbursementPaymentStatus> set = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(reimbursementDTO.getId());
				if(!set.isEmpty()) {
					Iterator<ReimbursementPaymentStatus> iterator = set.iterator();
					reimbursementPaymentStatus = iterator.next();
				}
			}
			
			if(reimbursementPaymentStatus == null) {
				reimbursementPaymentStatus = new ReimbursementPaymentStatus();
				reimbursementPaymentStatus.setReimbursementId(reimbursementSaved.getId());
				reimbursementPaymentStatus.setPaidReimbursementOriginalPayment(BigDecimal.ZERO);
				reimbursementPaymentStatus.setUnpaidReimbursementOriginalPayment(reimbursement.getIncludeGstTotalOriginalAmount());
			}
			else {
				reimbursementPaymentStatus.setUnpaidReimbursementOriginalPayment(reimbursement.getIncludeGstTotalOriginalAmount());
			}
			
			reimbursementPaymentStatusDAO.store(reimbursementPaymentStatus);
		}
		return reimbursementSaved != null ? reimbursementSaved.getId() : BigInteger.ZERO;
	}

	@Override
	public ReimbursementDTO clone(Reimbursement reimbursement) {
		if(reimbursement != null) {
			ReimbursementDTO reimbursementDTO = new ReimbursementDTO();
			copyDomainToDTO(reimbursement, reimbursementDTO);
			reimbursementDTO.setId(BigInteger.ZERO);
			reimbursementDTO.setReimbursementNo(getReimbursementNo());
			reimbursementDTO.setStatus(Constant.API_NOT_PAID);
			reimbursementDTO.getReimbursementDetailses().stream().forEach(p -> {
				p.setId(BigInteger.ZERO);
				p.setIsApproval(false);
			});
			return reimbursementDTO;
		}
		return null;
	}
	
	@Transactional
    public Set<Reimbursement> searchReimDetails(Object... parameters) {
		return reimbursementDAO.searchReimbursementDetails(parameters);
    }

	@Override
	public String findReimbursementPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth,
										  String fromReimbursementNo, String toReimbursementNo, String employeeCode, Integer orderColumn, String orderBy, int status) {
		List<ReimbursementPaymentDTO> list = reimbursementDAO.findReimbursementPaging(start, end, fromMonth, toMonth, fromReimbursementNo, 
																					  toReimbursementNo, employeeCode, orderColumn, orderBy, status);
		Map<String, String> invStatus = systemValueService.findSystemValueAsMapByType(Constant.PREFIX_API);
		Map<String, String> payStatus = systemValueService.findSystemValueAsMapByType(Constant.PREFIX_PAY);
		list.stream().forEach(p -> {
			List<BankStatement> bankStatements = new ArrayList<>(bankStatementDAO.findBankStatementByPaymentOrderNo(p.getPaymentOrderNo()));
			if(!bankStatements.isEmpty()) {
				p.setBankStatementIds(bankStatements.stream().map(b -> b.getId()).collect(Collectors.toList()));
			}
			//status to render class CSS
			p.setPaymentOrderStatusCode(p.getPaymentOrderStatus());
			p.setReimbursementStatusCode(p.getReimbursementStatus());
			//set status (code -> value)
			p.setPaymentOrderStatus(payStatus.get(p.getPaymentOrderStatus()));
			p.setReimbursementStatus(invStatus.get(p.getReimbursementStatus()));
			// set value for claim type 
			ClaimTypeMaster claimTypeMaster = claimTypeMasterDAO.findClaimTypeMasterByPrimaryKey(p.getClaimType());
			if(null != claimTypeMaster){
			    p.setClaimTypeVal(claimTypeMaster.getName());
			}
		});
		Map<String, Object> map = new HashMap<>();
        map.put("data", list);
        map.put("recordsFiltered", reimbursementDAO.countReimbursementFilter(fromMonth, toMonth, fromReimbursementNo, toReimbursementNo, employeeCode, status));
        Gson gson = new Gson();
        return gson.toJson(map);
	}
	
	@Override
	public Set<Reimbursement> findReimbursementByImportKey(String importKey) throws DataAccessException{
		return reimbursementDAO.findReimbursementByImportKey(importKey);
	}
	
	@Override
	public int deleteReimbursementByImportKey(String importKey) {
		Set<Reimbursement> set = findReimbursementByImportKey(importKey);
		if(set.isEmpty()) {
			return 0;
		}
		else {
			set.stream().forEach(p -> {
				/*
				 * delete reimbursement payment status
				 */
				if(p.getId().compareTo(BigInteger.ZERO) > 0)
				{
					Set<ReimbursementPaymentStatus> setReimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(p.getId());
					if(!setReimbursementPaymentStatus.isEmpty())
					{
						Iterator<ReimbursementPaymentStatus> iterator = setReimbursementPaymentStatus.iterator();
						ReimbursementPaymentStatus paymentStatus = iterator.next();
						reimbursementPaymentStatusDAO.remove(paymentStatus);
					}
				}
				/*
				 * delete reimbursement
				 */
				deleteReimbursement(p);
			});
			return 1;
		}
	}

	@Override
	public String getReimbursementNo(Calendar cal) {
		return reimbursementDAO.getReimbursementNo(cal);
	}
}
