package com.ttv.cashflow.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.RandomStringUtils;
import org.jboss.aerogear.security.otp.api.Base32;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.RoleDAO;
import com.ttv.cashflow.dao.SubsidiaryUserDetailDAO;
import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;
import com.ttv.cashflow.domain.UserAccount;
import com.ttv.cashflow.dto.UserAccountAdminForm;
import com.ttv.cashflow.dto.UserAccountDTO;
import com.ttv.cashflow.dto.UserAccountForm;
import com.ttv.cashflow.service.UserAccountService;
import com.ttv.cashflow.util.ApplicationUtil;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for UserAccount entities
 * 
 */

@Service("UserAccountService")
@Transactional
public class UserAccountServiceImpl implements UserAccountService {
	public static final int SAVE_ERROR_INVAILD_PASSWORD = -1;
	public static final int SAVE_ERROR_PASSWORD_NOT_IDENTICAL = -2;
	public static final int SAVE_ERROR_EMAIL_OR_PASSWORD_IS_EXISTS = -3;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	/**
	 * DAO injected by Spring that manages Role entities
	 * 
	 */
	@Autowired
	private RoleDAO roleDAO;

	/**
	 * DAO injected by Spring that manages SubsidiaryUserDetail entities
	 * 
	 */
	@Autowired
	private SubsidiaryUserDetailDAO subsidiaryUserDetailDAO;

	/**
	 * DAO injected by Spring that manages UserAccount entities
	 * 
	 */
	@Autowired
	private UserAccountDAO userAccountDAO;

	/**
	 * Instantiates a new UserAccountServiceImpl.
	 *
	 */
	public UserAccountServiceImpl() {
	}

	/**
	 * Save an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public UserAccount saveUserAccountSubsidiaryUserDetails(Integer id, SubsidiaryUserDetail related_subsidiaryuserdetails) {
		UserAccount useraccount = userAccountDAO.findUserAccountByPrimaryKey(id, -1, -1);
		SubsidiaryUserDetail existingsubsidiaryUserDetails = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(related_subsidiaryuserdetails.getId());

		// copy into the existing record to preserve existing relationships
		if (existingsubsidiaryUserDetails != null) {
			existingsubsidiaryUserDetails.setId(related_subsidiaryuserdetails.getId());
			existingsubsidiaryUserDetails.setCreatedDate(related_subsidiaryuserdetails.getCreatedDate());
			existingsubsidiaryUserDetails.setModifiedDate(related_subsidiaryuserdetails.getModifiedDate());
			related_subsidiaryuserdetails = existingsubsidiaryUserDetails;
		}

		related_subsidiaryuserdetails.setUserAccount(useraccount);
		useraccount.getSubsidiaryUserDetails().add(related_subsidiaryuserdetails);
		related_subsidiaryuserdetails = subsidiaryUserDetailDAO.store(related_subsidiaryuserdetails);
		subsidiaryUserDetailDAO.flush();

		useraccount = userAccountDAO.store(useraccount);
		userAccountDAO.flush();

		return useraccount;
	}

	/**
	 * Delete an existing UserAccount entity
	 * 
	 */
	@Transactional
	public void deleteUserAccount(UserAccount useraccount) {
		userAccountDAO.remove(useraccount);
		userAccountDAO.flush();
	}

	/**
	 * Return a count of all UserAccount entity
	 * 
	 */
	@Transactional
	public Integer countUserAccounts() {
		return ((Long) userAccountDAO.createQuerySingleResult("select count(o) from UserAccount o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Role entity
	 * 
	 */
	@Transactional
	public UserAccount saveUserAccountRole(Integer id, Role related_role) {
		UserAccount useraccount = userAccountDAO.findUserAccountByPrimaryKey(id, -1, -1);
		Role existingrole = roleDAO.findRoleByPrimaryKey(related_role.getId());

		// copy into the existing record to preserve existing relationships
		if (existingrole != null) {
			existingrole.setId(related_role.getId());
			existingrole.setName(related_role.getName());
			existingrole.setDescription(related_role.getDescription());
			existingrole.setCreatedDate(related_role.getCreatedDate());
			existingrole.setModifiedDate(related_role.getModifiedDate());
			related_role = existingrole;
		} else {
			related_role = roleDAO.store(related_role);
			roleDAO.flush();
		}

		useraccount.setRole(related_role);
		related_role.getUserAccounts().add(useraccount);
		useraccount = userAccountDAO.store(useraccount);
		userAccountDAO.flush();

		related_role = roleDAO.store(related_role);
		roleDAO.flush();

		return useraccount;
	}

	/**
	 * Return all UserAccount entity
	 * 
	 */
	@Transactional
	public List<UserAccount> findAllUserAccounts(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<UserAccount>(userAccountDAO.findAllUserAccounts(startResult, maxRows));
	}

	/**
	 * Delete an existing SubsidiaryUserDetail entity
	 * 
	 */
	@Transactional
	public UserAccount deleteUserAccountSubsidiaryUserDetails(Integer useraccount_id, Integer related_subsidiaryuserdetails_id) {
		SubsidiaryUserDetail related_subsidiaryuserdetails = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByPrimaryKey(related_subsidiaryuserdetails_id, -1, -1);

		UserAccount useraccount = userAccountDAO.findUserAccountByPrimaryKey(useraccount_id, -1, -1);

		related_subsidiaryuserdetails.setUserAccount(null);
		useraccount.getSubsidiaryUserDetails().remove(related_subsidiaryuserdetails);

		subsidiaryUserDetailDAO.remove(related_subsidiaryuserdetails);
		subsidiaryUserDetailDAO.flush();

		return useraccount;
	}

	/**
	 * Load an existing UserAccount entity
	 * 
	 */
	@Transactional
	public Set<UserAccount> loadUserAccounts() {
		return userAccountDAO.findAllUserAccounts();
	}

	/**
	 */
	@Transactional
	public UserAccount findUserAccountByPrimaryKey(Integer id) {
		return userAccountDAO.findUserAccountByPrimaryKey(id);
	}

	/**
	 * Delete an existing Role entity
	 * 
	 */
	@Transactional
	public UserAccount deleteUserAccountRole(Integer useraccount_id, Integer related_role_id) {
		UserAccount useraccount = userAccountDAO.findUserAccountByPrimaryKey(useraccount_id, -1, -1);
		Role related_role = roleDAO.findRoleByPrimaryKey(related_role_id, -1, -1);

		useraccount.setRole(null);
		related_role.getUserAccounts().remove(useraccount);
		useraccount = userAccountDAO.store(useraccount);
		userAccountDAO.flush();

		related_role = roleDAO.store(related_role);
		roleDAO.flush();

		roleDAO.remove(related_role);
		roleDAO.flush();

		return useraccount;
	}

	/**
	 * Save an existing UserAccount entity
	 * 
	 */
	@Transactional
	public void saveUserAccount(UserAccount useraccount) {
		UserAccount existingUserAccount = userAccountDAO.findUserAccountByPrimaryKey(useraccount.getId());
		if (existingUserAccount != null) {
			if (existingUserAccount != useraccount) {
				existingUserAccount.setId(useraccount.getId());
				existingUserAccount.setEmail(useraccount.getEmail());
				existingUserAccount.setPassword(useraccount.getPassword());
				existingUserAccount.setFirstName(useraccount.getFirstName());
				existingUserAccount.setLastName(useraccount.getLastName());
				existingUserAccount.setPhone(useraccount.getPhone());
				existingUserAccount.setIsActive(useraccount.getIsActive());
				existingUserAccount.setCreatedDate(useraccount.getCreatedDate());
				existingUserAccount.setModifiedDate(useraccount.getModifiedDate());
			}
			useraccount = userAccountDAO.store(existingUserAccount);
		} else {
			useraccount = userAccountDAO.store(useraccount);
		}
		userAccountDAO.flush();
	}

	public Set<UserAccount> findUserAccountByEmail(String email) {
		return userAccountDAO.findUserAccountByEmail(email, -1, -1);
	}

	public int saveUserAccount(UserAccountForm userAccountDTO) {
		try {
			UserAccount existingUserAccount = userAccountDAO.findUserAccountByPrimaryKey(userAccountDTO.getId());
			if (existingUserAccount != null) {
				if(userAccountDTO.getUpdatePasswordStatus() > 0)
				{
					/*
					 * validate password
					 */
					if(!bCryptPasswordEncoder.matches(userAccountDTO.getPassword(), existingUserAccount.getPassword()))
					{
						return SAVE_ERROR_PASSWORD_NOT_IDENTICAL;
					} else if(!userAccountDTO.getNewPassword().trim().equals(userAccountDTO.getReNewPassword().trim())) {
						return SAVE_ERROR_EMAIL_OR_PASSWORD_IS_EXISTS;
					} else if (!ApplicationUtil.validateRegex(userAccountDTO.getNewPassword().trim(), Constant.PASSWORD_PATTERN)) {
						return SAVE_ERROR_INVAILD_PASSWORD;
					} else {
						existingUserAccount.setPassword(ApplicationUtil.convertToBCrypt(userAccountDTO.getNewPassword()));
					}
				}
				existingUserAccount.setFirstName(userAccountDTO.getFirstName());
				existingUserAccount.setLastName(userAccountDTO.getLastName());
				existingUserAccount.setPhone(userAccountDTO.getPhone());
				existingUserAccount.setModifiedDate(Calendar.getInstance());
				userAccountDAO.store(existingUserAccount);
				/*
				 * update default subsidiary
				 */
				Set<SubsidiaryUserDetail> setSubsidiaryUserDetail = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByUserId(userAccountDTO.getId());
				for(SubsidiaryUserDetail subsidiaryUserDetail: setSubsidiaryUserDetail) {
					if(subsidiaryUserDetail.getSubsidiary().getId() == userAccountDTO.getDefaultSubsidiaryId())
					{
						subsidiaryUserDetail.setIsDefault(true);
					}
					else {
						subsidiaryUserDetail.setIsDefault(false);
					}
					subsidiaryUserDetailDAO.store(subsidiaryUserDetail);
				}
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String generatePassword() {
		int length = 8;
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
	    String generatedString = RandomStringUtils.random(length, characters);
	    while (!ApplicationUtil.validateRegex(generatedString, Constant.PASSWORD_PATTERN)) {
	    	generatedString = RandomStringUtils.random(length, characters);
		}
		return generatedString;
	}
	
	public String findUserAccountJson(Integer start, Integer end, Integer tenantId, String keyword) {
        Set<UserAccount> set = tenantId == 0? userAccountDAO.findUserAccountAdminPaging(keyword, start, end): userAccountDAO.findUserAccountManagerPaging(tenantId, keyword, start, end);
        List<UserAccountDTO> listDTO = new ArrayList<UserAccountDTO>();
        for(UserAccount userAccount: set)
        {
        	UserAccountDTO userAccountDTO = new UserAccountDTO();
        	BeanUtils.copyProperties(userAccount, userAccountDTO);
        	listDTO.add(userAccountDTO);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listDTO);
        map.put("recordsFiltered", tenantId == 0? userAccountDAO.countUserAccountAdminPaging(keyword): userAccountDAO.countUserAccountManagerPaging(tenantId, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	public int saveUserAccountAdmin(UserAccountAdminForm userAccountDTO) {
		try {
			UserAccount userAccount = null;
			if(userAccountDTO.getId() > 0)
			{
				userAccount = userAccountDAO.findUserAccountByPrimaryKey(userAccountDTO.getId());
				userAccount.setModifiedDate(Calendar.getInstance());
			}
			else {
				userAccount = new UserAccount();
				userAccount.setCreatedDate(Calendar.getInstance());
				userAccount.setIsUsing2fa(true);
				userAccount.setSecretToken(Base32.random());
			}
			/*
			 * validate password
			 */
			if(userAccountDTO.getUpdatePasswordStatus() > 0) {
				if(!userAccountDTO.getNewPassword().trim().equals(userAccountDTO.getReNewPassword().trim())) {
					return SAVE_ERROR_PASSWORD_NOT_IDENTICAL;
				} else if (!ApplicationUtil.validateRegex(userAccountDTO.getNewPassword().trim(), Constant.PASSWORD_PATTERN)) {
					return SAVE_ERROR_INVAILD_PASSWORD;
				} else {
					userAccount.setPassword(ApplicationUtil.convertToBCrypt(userAccountDTO.getNewPassword()));
				}
			}
			/*
			 * validate email
			 */
			Set<UserAccount> setExistsingUserByEmail = userAccountDAO.findUserAccountByEmail(userAccountDTO.getEmail());
			if(setExistsingUserByEmail.stream().anyMatch(p -> p.getId() != userAccountDTO.getId())) {
				return -3;
			}
			/*
			 * validate phone number
			 */
			userAccount.setEmail(userAccountDTO.getEmail());
			userAccount.setFirstName(userAccountDTO.getFirstName());
			userAccount.setLastName(userAccountDTO.getLastName());
			userAccount.setPhone(userAccountDTO.getPhone());
			userAccount.setIsActive(userAccountDTO.getIsActive() > 0? true: false);
			Role role= new Role();
			role.setId(userAccountDTO.getRoleId());
			userAccount.setRole(role);
			UserAccount savedUserAccount = userAccountDAO.store(userAccount);
			/*
			 * update subsidiary 
			 */
			//when update for old user
			if(userAccountDTO.getId() > 0) {
				List<Integer> currentSubIds = userAccount.getSubsidiaryUserDetails().stream().map(u -> u.getSubsidiary().getId()).collect(Collectors.toList());
				
				if(userAccountDTO.getSettedSubsidiaries().size() != currentSubIds.size())
				{
					removeSubsidiaryUserDetail(userAccountDTO.getId());
				}
				else {
					if(userAccountDTO.getSettedSubsidiaries().stream().anyMatch(u -> !currentSubIds.contains(u))) {
						removeSubsidiaryUserDetail(userAccountDTO.getId());
					}
				}
			}
			if(!userAccountDTO.getSettedSubsidiaries().isEmpty()) {
				for(Integer subId: userAccountDTO.getSettedSubsidiaries()) {
					createSubsidiaryUserDetail(subId, savedUserAccount.getId());
				}
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public void deleteUserAccountByIds(Integer[] ids) {
		for (Integer id: ids) {
			UserAccount userAccount = userAccountDAO.findUserAccountById(id);
	        if (userAccount != null) {
	           deleteUserAccount(userAccount);
	        }
	    }
	}
	
	private void createSubsidiaryUserDetail(Integer subId, Integer userId) {
		if(subsidiaryUserDetailDAO.findSubsidiaryUserBySubsidiaryIdAndUserId(subId, userId).isEmpty()) {
			SubsidiaryUserDetail subsidiaryUserDetail = new SubsidiaryUserDetail();
			Subsidiary subsidiary = new Subsidiary();
			UserAccount userAccountTemp = new UserAccount();
			subsidiary.setId(subId);
			userAccountTemp.setId(userId);
			subsidiaryUserDetail.setSubsidiary(subsidiary);
			subsidiaryUserDetail.setUserAccount(userAccountTemp);
			subsidiaryUserDetailDAO.store(subsidiaryUserDetail);
			subsidiaryUserDetailDAO.flush();
		}
	}
	
	private void removeSubsidiaryUserDetail(Integer userId) {
		Set<SubsidiaryUserDetail> subsidiaryUserDetails = subsidiaryUserDetailDAO.findSubsidiaryUserDetailByUserId(userId);
		if(!subsidiaryUserDetails.isEmpty())
		{
			for(SubsidiaryUserDetail subsidiaryUserDetail: subsidiaryUserDetails)
			{
				subsidiaryUserDetailDAO.remove(subsidiaryUserDetail);
			}
		}
		subsidiaryUserDetailDAO.flush();
	}

	@Override
	public UserAccount preAuthenticateUserAccount(String email, String password) {
		Set<UserAccount> setUserAccount = userAccountDAO.findUserAccountByEmail(email, -1, -1);
		if(!setUserAccount.isEmpty()) {
			Iterator<UserAccount> iterator = setUserAccount.iterator();
			UserAccount userAccount = iterator.next();
			if(BCrypt.checkpw(password, userAccount.getPassword()))
			{
				return userAccount;
			}
		}
		return null;
	}
}
