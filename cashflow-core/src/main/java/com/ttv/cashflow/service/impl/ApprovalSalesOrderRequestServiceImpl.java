package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApprovalSalesOrderRequestDAO;
import com.ttv.cashflow.dao.ArInvoiceClassDetailsDAO;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;
import com.ttv.cashflow.service.ApprovalSalesOrderRequestService;

/**
 * Spring service that handles CRUD requests for ApprovalSalesOrderRequest entities
 * 
 */

@Service("ApprovalSalesOrderRequestService")

@Transactional
public class ApprovalSalesOrderRequestServiceImpl implements ApprovalSalesOrderRequestService {

	/**
	 * DAO injected by Spring that manages ApprovalSalesOrderRequest entities
	 * 
	 */
	@Autowired
	private ApprovalSalesOrderRequestDAO approvalSalesOrderRequestDAO;
	@Autowired
	private ArInvoiceClassDetailsDAO arInvoiceClassDetailsDAO;
	/**
	 * Instantiates a new ApprovalSalesOrderRequestServiceImpl.
	 *
	 */
	public ApprovalSalesOrderRequestServiceImpl() {
	}

	/**
	 * Delete an existing ApprovalSalesOrderRequest entity
	 * 
	 */
	@Transactional
	public void deleteApprovalSalesOrderRequest(ApprovalSalesOrderRequest approvalsalesorderrequest) {
		approvalSalesOrderRequestDAO.remove(approvalsalesorderrequest);
	}

	/**
	 * Save an existing ApprovalSalesOrderRequest entity
	 * 
	 */
	@Transactional
	public void saveApprovalSalesOrderRequest(ApprovalSalesOrderRequest approvalsalesorderrequest) {
		ApprovalSalesOrderRequest existingApprovalSalesOrderRequest = approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestByPrimaryKey(approvalsalesorderrequest.getId());

		if (existingApprovalSalesOrderRequest != null) {
			if (existingApprovalSalesOrderRequest != approvalsalesorderrequest) {
				existingApprovalSalesOrderRequest.setId(approvalsalesorderrequest.getId());
				existingApprovalSalesOrderRequest.setApprovalCode(approvalsalesorderrequest.getApprovalCode());
				existingApprovalSalesOrderRequest.setApprovalType(approvalsalesorderrequest.getApprovalType());
				existingApprovalSalesOrderRequest.setApplicationDate(approvalsalesorderrequest.getApplicationDate());
				existingApprovalSalesOrderRequest.setValidityFrom(approvalsalesorderrequest.getValidityFrom());
				existingApprovalSalesOrderRequest.setValidityTo(approvalsalesorderrequest.getValidityTo());
				existingApprovalSalesOrderRequest.setCurrencyCode(approvalsalesorderrequest.getCurrencyCode());
				existingApprovalSalesOrderRequest.setIncludeGstOriginalAmount(approvalsalesorderrequest.getIncludeGstOriginalAmount());
				existingApprovalSalesOrderRequest.setPurpose(approvalsalesorderrequest.getPurpose());
				existingApprovalSalesOrderRequest.setApplicant(approvalsalesorderrequest.getApplicant());
				existingApprovalSalesOrderRequest.setApprovedDate(approvalsalesorderrequest.getApprovedDate());
				existingApprovalSalesOrderRequest.setCreatedDate(approvalsalesorderrequest.getCreatedDate());
				existingApprovalSalesOrderRequest.setModifiedDate(approvalsalesorderrequest.getModifiedDate());
			}
			approvalsalesorderrequest = approvalSalesOrderRequestDAO.store(existingApprovalSalesOrderRequest);
		} else {
			approvalsalesorderrequest = approvalSalesOrderRequestDAO.store(approvalsalesorderrequest);
		}
		approvalSalesOrderRequestDAO.flush();
	}

	/**
	 * Load an existing ApprovalSalesOrderRequest entity
	 * 
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> loadApprovalSalesOrderRequests() {
		return approvalSalesOrderRequestDAO.findAllApprovalSalesOrderRequests();
	}

	/**
	 * Return a count of all ApprovalSalesOrderRequest entity
	 * 
	 */
	@Transactional
	public Integer countApprovalSalesOrderRequests() {
		return ((Long) approvalSalesOrderRequestDAO.createQuerySingleResult("select count(o) from ApprovalSalesOrderRequest o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id) {
		return approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestByPrimaryKey(id);
	}

	/**
	 * Return all ApprovalSalesOrderRequest entity
	 * 
	 */
	@Transactional
	public List<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApprovalSalesOrderRequest>(approvalSalesOrderRequestDAO.findAllApprovalSalesOrderRequests(startResult, maxRows));
	}

	@Override
	public void deleteApprovalByIds(BigInteger[] ids) {
		for (BigInteger id: ids) {
			ApprovalSalesOrderRequest approval = approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestById(id);
	        if (approval != null) {
	           deleteApprovalSalesOrderRequest(approval);
	        }
	    }
		approvalSalesOrderRequestDAO.flush();
	}

	@Override
	public String findApprovalJson(Integer start, Integer end, String approvalType, String approvalCode,
			Calendar validityFrom, Calendar validityTo, Integer orderColumn, String orderBy) {
		Integer recordsTotal =approvalSalesOrderRequestDAO.countApprovalAll();
        List<ApprovalSalesOrderRequest> approvals = new java.util.ArrayList<ApprovalSalesOrderRequest>(approvalSalesOrderRequestDAO.findApprovalPaging(start, end, approvalType, approvalCode, validityFrom, validityTo, orderColumn, orderBy));
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", approvals);
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", approvalSalesOrderRequestDAO.countApprovalFilter(start, end, (approvalType != null && approvalType.isEmpty()) ? null : approvalType, "%" + (approvalCode == null ? "" : approvalCode) + "%", validityFrom, validityTo));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	@Override
	public Set<ApprovalSalesOrderRequest> findApprovalByApprovalCode(String approvalCode) {
		return approvalSalesOrderRequestDAO.findApprovalSalesOrderRequestByApprovalCode(approvalCode.trim());
	}

	@Override
	public int checkExists(ApprovalSalesOrderRequest approval) {
		return approvalSalesOrderRequestDAO.checkExists(approval);
	}

    @Override
    public List<ApprovalSalesOrderRequest> findApproval(String keyword) {
        return approvalSalesOrderRequestDAO.findApproval(keyword);
    }

	@Override
	public List<ApprovalSalesOrderRequest> findApprovalAvailable(String approvalCode, String issueDate)
			throws DataAccessException {
		return approvalSalesOrderRequestDAO.findApprovalAvailable(approvalCode, issueDate);
	}

    @Override
    public boolean checkExistingApprovalCode(String approvalCode) {
        return approvalSalesOrderRequestDAO.checkExistingApprovalCode(approvalCode);
    }

}
