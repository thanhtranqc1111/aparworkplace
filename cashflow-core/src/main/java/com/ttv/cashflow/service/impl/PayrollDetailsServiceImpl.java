package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dao.PayrollDetailsDAO;

import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollDetails;
import com.ttv.cashflow.service.PayrollDetailsService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for PayrollDetails entities
 * 
 */

@Service("PayrollDetailsService")

@Transactional
public class PayrollDetailsServiceImpl implements PayrollDetailsService {

	/**
	 * DAO injected by Spring that manages Payroll entities
	 * 
	 */
	@Autowired
	private PayrollDAO payrollDAO;

	/**
	 * DAO injected by Spring that manages PayrollDetails entities
	 * 
	 */
	@Autowired
	private PayrollDetailsDAO payrollDetailsDAO;

	/**
	 * Instantiates a new PayrollDetailsServiceImpl.
	 *
	 */
	public PayrollDetailsServiceImpl() {
	}

	/**
	 */
	@Transactional
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id) {
		return payrollDetailsDAO.findPayrollDetailsByPrimaryKey(id);
	}

	/**
	 * Load an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public Set<PayrollDetails> loadPayrollDetailss() {
		return payrollDetailsDAO.findAllPayrollDetailss();
	}

	/**
	 * Delete an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public void deletePayrollDetails(PayrollDetails payrolldetails) {
		payrollDetailsDAO.remove(payrolldetails);
		payrollDetailsDAO.flush();
	}

	/**
	 * Save an existing PayrollDetails entity
	 * 
	 */
	@Transactional
	public void savePayrollDetails(PayrollDetails payrolldetails) {
		PayrollDetails existingPayrollDetails = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(payrolldetails.getId());

		if (existingPayrollDetails != null) {
			if (existingPayrollDetails != payrolldetails) {
				existingPayrollDetails.setId(payrolldetails.getId());
				existingPayrollDetails.setEmployeeCode(payrolldetails.getEmployeeCode());
				existingPayrollDetails.setRoleTitle(payrolldetails.getRoleTitle());
				existingPayrollDetails.setDivision(payrolldetails.getDivision());
				existingPayrollDetails.setTeam(payrolldetails.getTeam());
				existingPayrollDetails.setLaborBaseSalaryPayment(payrolldetails.getLaborBaseSalaryPayment());
				existingPayrollDetails.setLaborAdditionalPayment1(payrolldetails.getLaborAdditionalPayment1());
				existingPayrollDetails.setLaborAdditionalPayment2(payrolldetails.getLaborAdditionalPayment2());
				existingPayrollDetails.setLaborAdditionalPayment3(payrolldetails.getLaborAdditionalPayment3());
				existingPayrollDetails.setLaborSocialInsuranceEmployer1(payrolldetails.getLaborSocialInsuranceEmployer1());
				existingPayrollDetails.setLaborSocialInsuranceEmployer2(payrolldetails.getLaborSocialInsuranceEmployer2());
				existingPayrollDetails.setLaborSocialInsuranceEmployer3(payrolldetails.getLaborSocialInsuranceEmployer3());
				existingPayrollDetails.setLaborOtherPayment1(payrolldetails.getLaborOtherPayment1());
				existingPayrollDetails.setLaborOtherPayment2(payrolldetails.getLaborOtherPayment2());
				existingPayrollDetails.setLaborOtherPayment3(payrolldetails.getLaborOtherPayment3());
				existingPayrollDetails.setTotalLaborCostOriginal(payrolldetails.getTotalLaborCostOriginal());
				existingPayrollDetails.setDeductionSocialInsuranceEmployer1(payrolldetails.getDeductionSocialInsuranceEmployer1());
				existingPayrollDetails.setDeductionSocialInsuranceEmployer2(payrolldetails.getDeductionSocialInsuranceEmployer2());
				existingPayrollDetails.setDeductionSocialInsuranceEmployer3(payrolldetails.getDeductionSocialInsuranceEmployer3());
				existingPayrollDetails.setDeductionSocialInsuranceEmployee1(payrolldetails.getDeductionSocialInsuranceEmployee1());
				existingPayrollDetails.setDeductionSocialInsuranceEmployee2(payrolldetails.getDeductionSocialInsuranceEmployee2());
				existingPayrollDetails.setDeductionSocialInsuranceEmployee3(payrolldetails.getDeductionSocialInsuranceEmployee3());
				existingPayrollDetails.setDeductionWht(payrolldetails.getDeductionWht());
				existingPayrollDetails.setDeductionOther1(payrolldetails.getDeductionOther1());
				existingPayrollDetails.setDeductionOther2(payrolldetails.getDeductionOther2());
				existingPayrollDetails.setDeductionOther3(payrolldetails.getDeductionOther3());
				existingPayrollDetails.setTotalDeductionOriginal(payrolldetails.getTotalDeductionOriginal());
				existingPayrollDetails.setNetPaymentOriginal(payrolldetails.getNetPaymentOriginal());
				existingPayrollDetails.setNetPaymentConverted(payrolldetails.getNetPaymentConverted());
				existingPayrollDetails.setApprovalCode(payrolldetails.getApprovalCode());
				existingPayrollDetails.setIsApproval(payrolldetails.getIsApproval());
				existingPayrollDetails.setCreatedDate(payrolldetails.getCreatedDate());
				existingPayrollDetails.setModifiedDate(payrolldetails.getModifiedDate());
			}
			payrolldetails = payrollDetailsDAO.store(existingPayrollDetails);
		} else {
			payrolldetails = payrollDetailsDAO.store(payrolldetails);
		}
		payrollDetailsDAO.flush();
	}

	/**
	 * Return all PayrollDetails entity
	 * 
	 */
	@Transactional
	public List<PayrollDetails> findAllPayrollDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PayrollDetails>(payrollDetailsDAO.findAllPayrollDetailss(startResult, maxRows));
	}

	/**
	 * Delete an existing Payroll entity
	 * 
	 */
	@Transactional
	public PayrollDetails deletePayrollDetailsPayroll(BigInteger payrolldetails_id, BigInteger related_payroll_id) {
		PayrollDetails payrolldetails = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(payrolldetails_id, -1, -1);
		Payroll related_payroll = payrollDAO.findPayrollByPrimaryKey(related_payroll_id, -1, -1);

		payrolldetails.setPayroll(null);
		related_payroll.getPayrollDetailses().remove(payrolldetails);
		payrolldetails = payrollDetailsDAO.store(payrolldetails);
		payrollDetailsDAO.flush();

		related_payroll = payrollDAO.store(related_payroll);
		payrollDAO.flush();

		payrollDAO.remove(related_payroll);
		payrollDAO.flush();

		return payrolldetails;
	}

	/**
	 * Return a count of all PayrollDetails entity
	 * 
	 */
	@Transactional
	public Integer countPayrollDetailss() {
		return ((Long) payrollDetailsDAO.createQuerySingleResult("select count(o) from PayrollDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Payroll entity
	 * 
	 */
	@Transactional
	public PayrollDetails savePayrollDetailsPayroll(BigInteger id, Payroll related_payroll) {
		PayrollDetails payrolldetails = payrollDetailsDAO.findPayrollDetailsByPrimaryKey(id, -1, -1);
		Payroll existingpayroll = payrollDAO.findPayrollByPrimaryKey(related_payroll.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpayroll != null) {
			existingpayroll.setId(related_payroll.getId());
			existingpayroll.setMonth(related_payroll.getMonth());
			existingpayroll.setPayrollNo(related_payroll.getPayrollNo());
			existingpayroll.setBookingDate(related_payroll.getBookingDate());
			existingpayroll.setClaimType(related_payroll.getClaimType());
			existingpayroll.setAccountPayableCode(related_payroll.getAccountPayableCode());
			existingpayroll.setAccountPayableName(related_payroll.getAccountPayableName());
			existingpayroll.setPaymentScheduleDate(related_payroll.getPaymentScheduleDate());
			existingpayroll.setTotalLaborCostOriginal(related_payroll.getTotalLaborCostOriginal());
			existingpayroll.setTotalDeductionOriginal(related_payroll.getTotalDeductionOriginal());
			existingpayroll.setTotalNetPaymentOriginal(related_payroll.getTotalNetPaymentOriginal());
			existingpayroll.setTotalNetPaymentConverted(related_payroll.getTotalNetPaymentConverted());
			existingpayroll.setCreatedDate(related_payroll.getCreatedDate());
			existingpayroll.setModifiedDate(related_payroll.getModifiedDate());
			related_payroll = existingpayroll;
		} else {
			related_payroll = payrollDAO.store(related_payroll);
			payrollDAO.flush();
		}

		payrolldetails.setPayroll(related_payroll);
		related_payroll.getPayrollDetailses().add(payrolldetails);
		payrolldetails = payrollDetailsDAO.store(payrolldetails);
		payrollDetailsDAO.flush();

		related_payroll = payrollDAO.store(related_payroll);
		payrollDAO.flush();

		return payrolldetails;
	}
}
