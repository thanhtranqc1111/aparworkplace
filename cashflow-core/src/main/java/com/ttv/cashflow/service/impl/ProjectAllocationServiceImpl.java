package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.dao.ProjectAllocationDAO;
import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.dto.PayrollDTO;
import com.ttv.cashflow.dto.ProjectAllocationDTO;
import com.ttv.cashflow.dto.ProjectAllocationPrecentageDTO;
import com.ttv.cashflow.service.ProjectAllocationService;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for ProjectAllocation entities
 * 
 */

@Service("ProjectAllocationService")

@Transactional
public class ProjectAllocationServiceImpl implements ProjectAllocationService {

	/**
	 * DAO injected by Spring that manages EmployeeMaster entities
	 * 
	 */
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	/**
	 * DAO injected by Spring that manages ProjectAllocation entities
	 * 
	 */
	@Autowired
	private ProjectAllocationDAO projectAllocationDAO;

	/**
	 * DAO injected by Spring that manages ProjectMaster entities
	 * 
	 */
	@Autowired
	private ProjectMasterDAO projectMasterDAO;

	/**
	 * Instantiates a new ProjectAllocationServiceImpl.
	 *
	 */
	public ProjectAllocationServiceImpl() {
	}

	/**
	 * Save an existing ProjectAllocation entity
	 * 
	 */
	@Transactional
	public void saveProjectAllocation(ProjectAllocation projectallocation) {
		ProjectAllocation existingProjectAllocation = projectAllocationDAO.findProjectAllocationByPrimaryKey(projectallocation.getId());
		if (existingProjectAllocation != null) {
			existingProjectAllocation.setId(projectallocation.getId());
			existingProjectAllocation.setMonth(projectallocation.getMonth());
			existingProjectAllocation.setPercentAllocation(projectallocation.getPercentAllocation());
			existingProjectAllocation.setEmployeeMaster(projectallocation.getEmployeeMaster());
			existingProjectAllocation.setProjectMaster(projectallocation.getProjectMaster());
			existingProjectAllocation.setModifiedDate(Calendar.getInstance());
			projectallocation = projectAllocationDAO.store(existingProjectAllocation);
		} else {
			projectallocation.setCreatedDate(Calendar.getInstance());
			projectallocation.setModifiedDate(Calendar.getInstance());
			projectallocation = projectAllocationDAO.store(projectallocation);
		}
		projectAllocationDAO.flush();
	}

	/**
	 * Return all ProjectAllocation entity
	 * 
	 */
	@Transactional
	public List<ProjectAllocation> findAllProjectAllocations(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ProjectAllocation>(projectAllocationDAO.findAllProjectAllocations(startResult, maxRows));
	}

	/**
	 * Delete an existing ProjectAllocation entity
	 * 
	 */
	@Transactional
	public void deleteProjectAllocation(ProjectAllocation projectallocation) {
		projectAllocationDAO.remove(projectallocation);
		projectAllocationDAO.flush();
	}

	/**
	 * Load an existing ProjectAllocation entity
	 * 
	 */
	@Transactional
	public Set<ProjectAllocation> loadProjectAllocations() {
		return projectAllocationDAO.findAllProjectAllocations();
	}

	/**
	 * Delete an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public ProjectAllocation deleteProjectAllocationEmployeeMaster(BigInteger projectallocation_id, BigInteger related_employeemaster_id) {
		ProjectAllocation projectallocation = projectAllocationDAO.findProjectAllocationByPrimaryKey(projectallocation_id, -1, -1);
		EmployeeMaster related_employeemaster = employeeMasterDAO.findEmployeeMasterByPrimaryKey(related_employeemaster_id, -1, -1);

		projectallocation.setEmployeeMaster(null);
		related_employeemaster.getProjectAllocations().remove(projectallocation);
		projectallocation = projectAllocationDAO.store(projectallocation);
		projectAllocationDAO.flush();

		related_employeemaster = employeeMasterDAO.store(related_employeemaster);
		employeeMasterDAO.flush();

		employeeMasterDAO.remove(related_employeemaster);
		employeeMasterDAO.flush();

		return projectallocation;
	}

	/**
	 * Save an existing ProjectMaster entity
	 * 
	 */
	@Transactional
	public ProjectAllocation saveProjectAllocationProjectMaster(BigInteger id, ProjectMaster related_projectmaster) {
		ProjectAllocation projectallocation = projectAllocationDAO.findProjectAllocationByPrimaryKey(id, -1, -1);
		ProjectMaster existingprojectMaster = projectMasterDAO.findProjectMasterByPrimaryKey(related_projectmaster.getCode());

		// copy into the existing record to preserve existing relationships
		if (existingprojectMaster != null) {
			existingprojectMaster.setCode(related_projectmaster.getCode());
			existingprojectMaster.setName(related_projectmaster.getName());
			existingprojectMaster.setBusinessTypeName(related_projectmaster.getBusinessTypeName());
			existingprojectMaster.setApprovalCode(related_projectmaster.getApprovalCode());
			existingprojectMaster.setCustomerName(related_projectmaster.getCustomerName());
			existingprojectMaster.setIsActive(related_projectmaster.getIsActive());
			related_projectmaster = existingprojectMaster;
		} else {
			related_projectmaster = projectMasterDAO.store(related_projectmaster);
			projectMasterDAO.flush();
		}

		projectallocation.setProjectMaster(related_projectmaster);
		related_projectmaster.getProjectAllocations().add(projectallocation);
		projectallocation = projectAllocationDAO.store(projectallocation);
		projectAllocationDAO.flush();

		related_projectmaster = projectMasterDAO.store(related_projectmaster);
		projectMasterDAO.flush();

		return projectallocation;
	}

	/**
	 * Return a count of all ProjectAllocation entity
	 * 
	 */
	@Transactional
	public Integer countProjectAllocations() {
		return ((Long) projectAllocationDAO.createQuerySingleResult("select count(o) from ProjectAllocation o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id) {
		return projectAllocationDAO.findProjectAllocationByPrimaryKey(id);
	}

	/**
	 * Delete an existing ProjectMaster entity
	 * 
	 */
	@Transactional
	public ProjectAllocation deleteProjectAllocationProjectMaster(BigInteger projectallocation_id, String related_projectmaster_code) {
		ProjectAllocation projectallocation = projectAllocationDAO.findProjectAllocationByPrimaryKey(projectallocation_id, -1, -1);
		ProjectMaster related_projectmaster = projectMasterDAO.findProjectMasterByPrimaryKey(related_projectmaster_code, -1, -1);

		projectallocation.setProjectMaster(null);
		related_projectmaster.getProjectAllocations().remove(projectallocation);
		projectallocation = projectAllocationDAO.store(projectallocation);
		projectAllocationDAO.flush();

		related_projectmaster = projectMasterDAO.store(related_projectmaster);
		projectMasterDAO.flush();

		projectMasterDAO.remove(related_projectmaster);
		projectMasterDAO.flush();

		return projectallocation;
	}

	/**
	 * Save an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public ProjectAllocation saveProjectAllocationEmployeeMaster(BigInteger id, EmployeeMaster related_employeemaster) {
		ProjectAllocation projectallocation = projectAllocationDAO.findProjectAllocationByPrimaryKey(id, -1, -1);
		EmployeeMaster existingemployeeMaster = employeeMasterDAO.findEmployeeMasterByPrimaryKey(related_employeemaster.getId());

		// copy into the existing record to preserve existing relationships
		if (existingemployeeMaster != null) {
			existingemployeeMaster.setId(related_employeemaster.getId());
			//existingemployeeMaster.setDivisionId(related_employeemaster.getDivisionId());
			existingemployeeMaster.setCode(related_employeemaster.getCode());
			existingemployeeMaster.setName(related_employeemaster.getName());
			existingemployeeMaster.setRoleTitle(related_employeemaster.getRoleTitle());
			existingemployeeMaster.setTeam(related_employeemaster.getTeam());
			existingemployeeMaster.setJoinDate(related_employeemaster.getJoinDate());
			existingemployeeMaster.setResignDate(related_employeemaster.getResignDate());
			related_employeemaster = existingemployeeMaster;
		} else {
			related_employeemaster = employeeMasterDAO.store(related_employeemaster);
			employeeMasterDAO.flush();
		}

		projectallocation.setEmployeeMaster(related_employeemaster);
		related_employeemaster.getProjectAllocations().add(projectallocation);
		projectallocation = projectAllocationDAO.store(projectallocation);
		projectAllocationDAO.flush();

		related_employeemaster = employeeMasterDAO.store(related_employeemaster);
		employeeMasterDAO.flush();

		return projectallocation;
	}

	@Override
	public Set<ProjectAllocationDTO> findProjectAllocation(String employeeCode, Calendar fromMonth, Calendar toMonth) throws Exception{
		if(fromMonth == null || toMonth == null) {
			throw new Exception("fromMonth and toMonth parameters can not be null");
		}
		Set<EmployeeMaster> setEmployeeMaster = null;
		if(!StringUtils.isEmpty(employeeCode)) {
			setEmployeeMaster = employeeMasterDAO.findEmployeeMasterByCode(employeeCode);
		}
		else
		{
			setEmployeeMaster = employeeMasterDAO.findAllEmployeeMasters();
		}
		Set<ProjectAllocationDTO> setProjectAllocationDTO = new TreeSet <>();
		for(EmployeeMaster e: setEmployeeMaster) {
			//filter project
			Comparator<ProjectAllocation> myComparator = new Comparator<ProjectAllocation>() {
			    public int compare(ProjectAllocation o1, ProjectAllocation o2) {
			        return o1.getProjectMaster().getCode().compareTo(o2.getProjectMaster().getCode());

			    }
			};
			Set<ProjectAllocation> setProjectAllocation = e.getProjectAllocations().stream()
																				   .filter(p -> (p.getMonth().equals(toMonth) || p.getMonth().before(toMonth)) && 
																						   		(p.getMonth().equals(fromMonth) || p.getMonth().after(fromMonth))).sorted(myComparator).collect(Collectors.toSet());
			//when employee does not have any project
			if(setProjectAllocation.isEmpty()) {
				ProjectAllocationDTO projectAllocationDTO = new ProjectAllocationDTO(e.getId(), e.getCode(),
																					 e.getName(), null, 
																					 null, e.getTeam());
				setProjectAllocationDTO.add(projectAllocationDTO);
			}
			else {
				for(ProjectAllocation p: setProjectAllocation) {
					//create DTO
					ProjectAllocationDTO projectAllocationDTO = new ProjectAllocationDTO(e.getId(), e.getCode(),
																						 e.getName(), p.getProjectMaster().getName(), 
																						 p.getProjectMaster().getCode(), e.getTeam());
					//retrieve map month data when DTO added before
					Optional<ProjectAllocationDTO> optional = setProjectAllocationDTO.stream().filter(pp -> pp.equals(projectAllocationDTO)).findFirst();
					if(optional.isPresent())
					{
						projectAllocationDTO.setMapMonthPercent(optional.get().getMapMonthPercent());
					}
					else {
						//add to result list if DTO is not added before
						setProjectAllocationDTO.add(projectAllocationDTO);
					}
					//insert allocation data to DTO
					projectAllocationDTO.getMapMonthPercent().put(CalendarUtil.toString(p.getMonth()), new ProjectAllocationPrecentageDTO(p.getId(), p.getPercentAllocation()));
				}
			}
		}
		return setProjectAllocationDTO;
	}

	@Override
	public int saveListProjectAllocation(JsonArray jsonArray) throws Exception{
		int totalSaveSuccessRecords = 0;
		for(int i = 0; i < jsonArray.size(); i++) {
			JsonObject projectAllocationObject = jsonArray.get(i).getAsJsonObject();
			BigDecimal percentAllocation = null;
			if(projectAllocationObject.has("percent")) {
				percentAllocation= projectAllocationObject.get("percent").getAsBigDecimal();
			}
			if(projectAllocationObject.has("removed") || (percentAllocation != null && percentAllocation.equals(BigDecimal.ZERO))) {
				ProjectAllocation p = projectAllocationDAO.findProjectAllocationById(projectAllocationObject.get("id").getAsBigInteger());
				if(p != null) {
					deleteProjectAllocation(p);
				}
				totalSaveSuccessRecords ++;
				continue;
			}
			ProjectAllocation projectAllocation = new ProjectAllocation();
			projectAllocation.setId(projectAllocationObject.get("id").getAsBigInteger());
			
			String projectCode = projectAllocationObject.get("projectCode").getAsString();
			ProjectMaster projectMaster = projectMasterDAO.findProjectMasterByCode(projectCode);
			if(projectMaster == null)
			{
				throw new Exception("Project code is not exists in master data");
			}
			else {
				projectAllocation.setProjectMaster(projectMaster);
			}
			
			BigInteger empployeeId = projectAllocationObject.get("empId").getAsBigInteger();
			EmployeeMaster employeeMaster = employeeMasterDAO.findEmployeeMasterById(empployeeId);
			if(employeeMaster == null)
			{
				throw new Exception("Employee id is not exists in master data");
			}
			else {
				projectAllocation.setEmployeeMaster(employeeMaster);
			}
			
			if(percentAllocation.compareTo(BigDecimal.ZERO) > 0 && percentAllocation.compareTo(new BigDecimal(100)) <= 0) {
				projectAllocation.setPercentAllocation(percentAllocation);
			}
			else {
				throw new Exception("Percent Allocation must be between 0 and 100");
			}
			projectAllocation.setMonth(CalendarUtil.getCalendar(projectAllocationObject.get("month").getAsString(), Constant.DDMMYYYY));
			saveProjectAllocation(projectAllocation);
			totalSaveSuccessRecords ++;
		}
		return totalSaveSuccessRecords;
	}
}
