
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;

/**
 * Spring service that handles CRUD requests for DivisionMaster entities
 * 
 */
public interface DivisionMasterService {

	/**
	* Return all DivisionMaster entity
	* 
	 */
	public List<DivisionMaster> findAllDivisionMasters(Integer startResult, Integer maxRows);

	/**
	* Return a count of all DivisionMaster entity
	* 
	 */
	public Integer countDivisionMasters();

	/**
	* Delete an existing EmployeeMaster entity
	* 
	 */
	public DivisionMaster deleteDivisionMasterEmployeeMasters(BigInteger divisionmaster_id, BigInteger related_employeemasters_id);

	/**
	* Delete an existing DivisionMaster entity
	* 
	 */
	public void deleteDivisionMaster(DivisionMaster divisionmaster);

	/**
	 */
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id);

	/**
	* Save an existing EmployeeMaster entity
	* 
	 */
	public DivisionMaster saveDivisionMasterEmployeeMasters(BigInteger id_1, EmployeeMaster related_employeemasters);

	/**
	* Save an existing DivisionMaster entity
	* 
	 */
	public void saveDivisionMaster(DivisionMaster divisionmaster_1);

	/**
	* Load an existing DivisionMaster entity
	* 
	 */
	public Set<DivisionMaster> loadDivisionMasters();

    String findDivisionMasterJson(Integer startpage, Integer numOfRecord, Integer orderColumn, String orderBy, String keyword);

    void deleteDivisionMasterByCodes(String[] codes);

    DivisionMaster findDivisionMasterByCode(String code);

    boolean createDivisionMaster(DivisionMaster divisionmaster);

    boolean updateDivisionMaster(DivisionMaster divisionmaster);

}