package com.ttv.cashflow.service.impl;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.AccountMasterDAO;

import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.BusinessTypeMaster;
import com.ttv.cashflow.service.AccountMasterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for AccountMaster entities
 * 
 */

@Service("AccountMasterService")

@Transactional
public class AccountMasterServiceImpl implements AccountMasterService {

	/**
	 * DAO injected by Spring that manages AccountMaster entities
	 * 
	 */
	@Autowired
	private AccountMasterDAO accountMasterDAO;

	/**
	 * Instantiates a new AccountMasterServiceImpl.
	 *
	 */
	public AccountMasterServiceImpl() {
	}

	/**
	 * Save an existing AccountMaster entity
	 * 
	 */
	@Transactional
	public void saveAccountMaster(AccountMaster accountmaster) {
		AccountMaster existingAccountMaster = accountMasterDAO.findAccountMasterByPrimaryKey(accountmaster.getCode());

		if (existingAccountMaster != null) {
			if (existingAccountMaster != accountmaster) {
				existingAccountMaster.setCode(accountmaster.getCode());
				existingAccountMaster.setParentCode(accountmaster.getParentCode());
				existingAccountMaster.setName(accountmaster.getName());
				existingAccountMaster.setType(accountmaster.getType());
			}
			accountmaster = accountMasterDAO.store(existingAccountMaster);
		} else {
			accountmaster = accountMasterDAO.store(accountmaster);
		}
		accountMasterDAO.flush();
	}

	/**
	 */
	@Transactional
	public AccountMaster findAccountMasterByPrimaryKey(String code) {
		return accountMasterDAO.findAccountMasterByPrimaryKey(code);
	}

	/**
	 * Return all AccountMaster entity
	 * 
	 */
	@Transactional
	public List<AccountMaster> findAllAccountMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<AccountMaster>(accountMasterDAO.findAllAccountMasters(startResult, maxRows));
	}

	/**
	 * Return a count of all AccountMaster entity
	 * 
	 */
	@Transactional
	public Integer countAccountMasters() {
		return ((Long) accountMasterDAO.createQuerySingleResult("select count(o) from AccountMaster o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing AccountMaster entity
	 * 
	 */
	@Transactional
	public void deleteAccountMaster(AccountMaster accountmaster) {
		accountMasterDAO.remove(accountmaster);
		accountMasterDAO.flush();
	}

	/**
	 * Load an existing AccountMaster entity
	 * 
	 */
	@Transactional
	public Set<AccountMaster> loadAccountMasters() {
		return accountMasterDAO.findAllAccountMasters();
	}

	public Set<AccountMaster> findAccountMasterByNameCodeContaining(String nameCode) {
		return accountMasterDAO.findAccountMasterByNameCodeContaining(nameCode);
	}
	
	public String findAccountNameByCode(String code) {
        return accountMasterDAO.findAccountMasterByCode(code).getName();
    }

	public void deleteAccountMasterByCodes(String[] codes) {
		for (String code : codes) {
	        AccountMaster accountMaster = accountMasterDAO.findAccountMasterByCode(code);
	        if (accountMaster != null) {
	            deleteAccountMaster(accountMaster);
	        }
	    }
	}

	public String findAccountMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
		Integer recordsTotal = accountMasterDAO.countAccountMasterAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", accountMasterDAO.findAccountMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", accountMasterDAO.countAccountMasterFilter(start, end, keyword));
        Gson gson = new Gson();
        return  gson.toJson(map);
	}
}
