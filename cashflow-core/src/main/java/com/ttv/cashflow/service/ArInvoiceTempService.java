
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ArInvoiceTemp;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ArInvoiceTemp entities
 * 
 */
public interface ArInvoiceTempService {

	/**
	 */
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id);

	/**
	* Return a count of all ArInvoiceTemp entity
	* 
	 */
	public Integer countArInvoiceTemps();

	/**
	* Delete an existing ArInvoiceTemp entity
	* 
	 */
	public void deleteArInvoiceTemp(ArInvoiceTemp arinvoicetemp);

	/**
	* Save an existing ArInvoiceTemp entity
	* 
	 */
	public void saveArInvoiceTemp(ArInvoiceTemp arinvoicetemp_1);

	/**
	* Load an existing ArInvoiceTemp entity
	* 
	 */
	public Set<ArInvoiceTemp> loadArInvoiceTemps();

	/**
	* Return all ArInvoiceTemp entity
	* 
	 */
	public List<ArInvoiceTemp> findAllArInvoiceTemps(Integer startResult, Integer maxRows);
}