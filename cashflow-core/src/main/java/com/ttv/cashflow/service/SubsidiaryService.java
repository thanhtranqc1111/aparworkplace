
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;

/**
 * Spring service that handles CRUD requests for Subsidiary entities
 * 
 */
public interface SubsidiaryService {

	/**
	* Return all Subsidiary entity
	* 
	 */
	public List<Subsidiary> findAllSubsidiarys(Integer startResult, Integer maxRows);

	/**
	* Return a count of all Subsidiary entity
	* 
	 */
	public Integer countSubsidiarys();

	/**
	* Save an existing Subsidiary entity
	* 
	 */
	public void saveSubsidiary(Subsidiary subsidiary);

	/**
	 */
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id);

	/**
	* Load an existing Subsidiary entity
	* 
	 */
	public Set<Subsidiary> loadSubsidiarys();

	/**
	* Delete an existing Subsidiary entity
	* 
	 */
	public void deleteSubsidiary(Subsidiary subsidiary_1);

	/**
	* Save an existing SubsidiaryUserDetail entity
	* 
	 */
	public Subsidiary saveSubsidiarySubsidiaryUserDetails(Integer id_1, SubsidiaryUserDetail related_subsidiaryuserdetails);

	/**
	* Delete an existing SubsidiaryUserDetail entity
	* 
	 */
	public Subsidiary deleteSubsidiarySubsidiaryUserDetails(Integer subsidiary_id, Integer related_subsidiaryuserdetails_id);
	
	/**
	 * find Subsidiary Json
	 * @param name
	 * @return
	 */
	public String findSubsidiaryJson(String name);
	
	public void deleteSubsidiaryByIds(Integer[] ids);

    List<Subsidiary> findListSubsidiaryByUserId(Integer userId);
}