
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.TenantScriptInitialization;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for TenantScriptInitialization entities
 * 
 */
public interface TenantScriptInitializationService {

	/**
	* Save an existing TenantScriptInitialization entity
	* 
	 */
	public void saveTenantScriptInitialization(TenantScriptInitialization tenantscriptinitialization);

	/**
	* Delete an existing TenantScriptInitialization entity
	* 
	 */
	public void deleteTenantScriptInitialization(TenantScriptInitialization tenantscriptinitialization_1);

	/**
	* Load an existing TenantScriptInitialization entity
	* 
	 */
	public Set<TenantScriptInitialization> loadTenantScriptInitializations();

	/**
	* Return a count of all TenantScriptInitialization entity
	* 
	 */
	public Integer countTenantScriptInitializations();

	/**
	* Return all TenantScriptInitialization entity
	* 
	 */
	public List<TenantScriptInitialization> findAllTenantScriptInitializations(Integer startResult, Integer maxRows);

	/**
	 */
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id);
}