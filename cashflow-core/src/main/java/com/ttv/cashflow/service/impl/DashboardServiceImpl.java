package com.ttv.cashflow.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.DashboardDAO;
import com.ttv.cashflow.dto.DashboardChartOneDTO;
import com.ttv.cashflow.service.DashboardService;

@Service("DashboardService")
@Transactional
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private DashboardDAO dashboardDAO;

	@Override
	public List<DashboardChartOneDTO> getListChartOne(Calendar fromMonth, Calendar toMonth) {
		return dashboardDAO.getListChartOne(fromMonth, toMonth);
	}
	
}
