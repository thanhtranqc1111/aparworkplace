
package com.ttv.cashflow.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.Subsidiary;
import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.dto.InvoicePayment;
import com.ttv.cashflow.dto.PaymentInvoice;

/**
 * Spring service that handles CRUD requests for ApInvoice entities
 * 
 */
public interface ApInvoiceService {

	/**
	* Save an existing ApInvoice entity
	* 
	 */
	public void saveApInvoice(ApInvoice apinvoice);

	/**
	* Delete an existing ApInvoicePaymentStatus entity
	* 
	 */
	public ApInvoice deleteApInvoiceApInvoicePaymentStatuses(BigInteger apinvoice_id, BigInteger related_apinvoicepaymentstatuses_id);

	/**
	* Save an existing Subsidiary entity
	* 
	 */
	public ApInvoice saveApInvoiceSubsidiary(BigInteger id, Subsidiary related_subsidiary);

	/**
	* Load an existing ApInvoice entity
	* 
	 */
	public Set<ApInvoice> loadApInvoices();

	/**
	* Delete an existing ApInvoice entity
	* 
	 */
	public void deleteApInvoice(ApInvoice apinvoice_1);

	/**
	* Delete an existing Subsidiary entity
	* 
	 */
	public ApInvoice deleteApInvoiceSubsidiary(BigInteger apinvoice_id_1, Integer related_subsidiary_id);

	/**
	* Save an existing ApInvoicePaymentDetails entity
	* 
	 */
	public ApInvoice saveApInvoiceApInvoicePaymentDetailses(BigInteger id_2, ApInvoicePaymentDetails related_apinvoicepaymentdetailses);

	/**
	* Delete an existing ApInvoicePaymentDetails entity
	* 
	 */
	public ApInvoice deleteApInvoiceApInvoicePaymentDetailses(BigInteger apinvoice_id_2, BigInteger related_apinvoicepaymentdetailses_id);

	/**
	* Return a count of all ApInvoice entity
	* 
	 */
	public Integer countApInvoices();

	/**
	* Return all ApInvoice entity
	* 
	 */
	public List<ApInvoice> findAllApInvoices(Integer startResult, Integer maxRows);

	/**
	 */
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id_3);

	/**
	* Save an existing ApInvoiceAccountDetails entity
	* 
	 */
	public ApInvoice saveApInvoiceApInvoiceAccountDetailses(BigInteger id_4, ApInvoiceAccountDetails related_apinvoiceaccountdetailses);

	/**
	* Delete an existing ApInvoiceAccountDetails entity
	* 
	 */
	public ApInvoice deleteApInvoiceApInvoiceAccountDetailses(BigInteger apinvoice_id_3, BigInteger related_apinvoiceaccountdetailses_id);

	/**
	 * Get ap invoice detail
	 */
	public List<InvoiceDetail> getInvoiceDetail(BigInteger apInvoiceId);
	
	/**
	 * Get last Id
	 * @return
	 */
	public BigInteger getLastId();

    public List<String> searchApInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
            								  String invoice, String payeeName, String status, String fromApNo, String toApNo, Integer orderColumn, String orderBy);
    
    public List<PaymentInvoice> searchInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
			  String invoice, String payeeName, String status, String fromApNo, String toApNo);
    
    public BigInteger countSearch(String fromMonth, String toMonth, String invoice, String payeeName,
            					  String fromApNo, String toApNo, String status);
    
    public List<InvoicePayment> searchInvoiceDetail(List<String> apInvoiceNoLst, Integer orderColumn, String orderBy);
    
    public String findAPInvoiceJSON(Integer startpage, Integer endPage, String calendarMonthFrom, String calendarMonthTo,
			String fromApNo, String toApNo, String payeeName, String invoiceNo, String status, Integer orderColumn, String orderBy);
    

    ApInvoice findApInvoiceByApInvoiceNo(String apInvoiceNo);
}