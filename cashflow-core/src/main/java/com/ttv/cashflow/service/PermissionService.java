
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.RolePermission;

/**
 * Spring service that handles CRUD requests for Permission entities
 * 
 */
public interface PermissionService {

	/**
	* Save an existing Permission entity
	* 
	 */
	public void savePermission(Permission permission);

	/**
	* Delete an existing Permission entity
	* 
	 */
	public void deletePermission(Permission permission_1);

	/**
	* Save an existing RolePermission entity
	* 
	 */
	public Permission savePermissionRolePermissions(Integer id, RolePermission related_rolepermissions);

	/**
	 */
	public Permission findPermissionByPrimaryKey(Integer id_1);

	/**
	* Return all Permission entity
	* 
	 */
	public List<Permission> findAllPermissions(Integer startResult, Integer maxRows);

	/**
	* Load an existing Permission entity
	* 
	 */
	public Set<Permission> loadPermissions();

	/**
	* Return a count of all Permission entity
	* 
	 */
	public Integer countPermissions();

	/**
	* Delete an existing RolePermission entity
	* 
	 */
	public Permission deletePermissionRolePermissions(Integer permission_id, Integer related_rolepermissions_id);
	/**
	 */
	public String findPermissionJson(String keyword);
	public List<Permission> parsePermissionJSON(JsonArray jsonArray);
	public void deletePermissionByIds(Integer ids[]);
}