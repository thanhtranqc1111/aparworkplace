
package com.ttv.cashflow.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.PaymentOrder;

/**
 * Spring service that handles CRUD requests for ApInvoicePaymentDetails entities
 * 
 */
public interface ApInvoicePaymentDetailsService {

	/**
     * Save an existing ApInvoicePaymentDetails entity
     * 
     * @return
     * 
     */
    public ApInvoicePaymentDetails saveApInvoicePaymentDetails(ApInvoicePaymentDetails apinvoicepaymentdetails);

	/**
	* Save an existing PaymentOrder entity
	* 
	 */
	public ApInvoicePaymentDetails saveApInvoicePaymentDetailsPaymentOrder(BigInteger id, PaymentOrder related_paymentorder);

	/**
	* Return a count of all ApInvoicePaymentDetails entity
	* 
	 */
	public Integer countApInvoicePaymentDetailss();

	/**
	* Delete an existing PaymentOrder entity
	* 
	 */
	public ApInvoicePaymentDetails deleteApInvoicePaymentDetailsPaymentOrder(BigInteger apinvoicepaymentdetails_id, BigInteger related_paymentorder_id);

	/**
	* Load an existing ApInvoicePaymentDetails entity
	* 
	 */
	public Set<ApInvoicePaymentDetails> loadApInvoicePaymentDetailss();

	/**
	* Delete an existing ApInvoicePaymentDetails entity
	* 
	 */
	public void deleteApInvoicePaymentDetails(ApInvoicePaymentDetails apinvoicepaymentdetails_1);

	/**
	* Save an existing ApInvoice entity
	* 
	 */
	public ApInvoicePaymentDetails saveApInvoicePaymentDetailsApInvoice(BigInteger id_1, ApInvoice related_apinvoice);

	/**
	 */
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id_2);

	/**
	* Delete an existing ApInvoice entity
	* 
	 */
	public ApInvoicePaymentDetails deleteApInvoicePaymentDetailsApInvoice(BigInteger apinvoicepaymentdetails_id_1, BigInteger related_apinvoice_id);

	/**
	* Return all ApInvoicePaymentDetails entity
	* 
	 */
	public List<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss(Integer startResult, Integer maxRows);
	
	/**
	 * get total original amount by ap invoice id without payment amount (total original amount - this payment original amount)
	 * @param paymentId
	 * @param apInvoiceId
	 * @return
	 */
	public BigDecimal getTotalOriginalAmountByApInvoiceId(BigInteger paymentId, BigInteger apInvoiceId);

}