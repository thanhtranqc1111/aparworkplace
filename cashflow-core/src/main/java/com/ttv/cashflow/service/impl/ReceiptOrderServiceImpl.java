package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.dao.ArInvoiceReceiptStatusDAO;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.ReceiptOrderDAO;
import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.ReceiptOrder;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;
import com.ttv.cashflow.dto.ReceiptOrderDTO;
import com.ttv.cashflow.service.ReceiptOrderService;
import com.ttv.cashflow.util.NumberUtil;

/**
 * Spring service that handles CRUD requests for ReceiptOrder entities
 * 
 */

@Service("ReceiptOrderService")

@Transactional
public class ReceiptOrderServiceImpl implements ReceiptOrderService {
	public static final String STATUS_RECEIVED = "001";
	public static final String STATUS_NOT_RECEIVED = "002";
	public static final String STATUS_PARTIAL_RECEIVED = "003";
	/**
	 * DAO injected by Spring that manages ReceiptOrder entities
	 * 
	 */
	@Autowired
	private ReceiptOrderDAO receiptOrderDAO;
	
	@Autowired
	private ArInvoiceDAO arInvoiceDAO;
	
	@Autowired
	private ArInvoiceReceiptStatusDAO arInvoiceReceiptStatusDAO;
	
	@Autowired
	private BankStatementDAO bankStatementDAO;
	
	/**
	 * Instantiates a new ReceiptOrderServiceImpl.
	 *
	 */
	public ReceiptOrderServiceImpl() {
	}

	/**
	 * Return a count of all ReceiptOrder entity
	 * 
	 */
	@Transactional
	public Integer countReceiptOrders() {
		return ((Long) receiptOrderDAO.createQuerySingleResult("select count(o) from ReceiptOrder o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ReceiptOrder entity
	 * 
	 */
	@Transactional
	public Set<ReceiptOrder> loadReceiptOrders() {
		return receiptOrderDAO.findAllReceiptOrders();
	}

	/**
	 * Save an existing ReceiptOrder entity
	 * 
	 */
	@Transactional
	public void saveReceiptOrder(ReceiptOrder receiptorder) {
		ReceiptOrder existingReceiptOrder = receiptOrderDAO.findReceiptOrderByPrimaryKey(receiptorder.getId());

		if (existingReceiptOrder != null) {
			if (existingReceiptOrder != receiptorder) {
				existingReceiptOrder.setId(receiptorder.getId());
				existingReceiptOrder.setTransactionId(receiptorder.getTransactionId());
				existingReceiptOrder.setArInvoiceId(receiptorder.getArInvoiceId());
				existingReceiptOrder.setSettedIncludeGstOriginalAmount(receiptorder.getSettedIncludeGstOriginalAmount());
				existingReceiptOrder.setReceiptRate(receiptorder.getReceiptRate());
				existingReceiptOrder.setSettedIncludeGstConvertedAmount(receiptorder.getSettedIncludeGstConvertedAmount());
				existingReceiptOrder.setVarianceAmount(receiptorder.getVarianceAmount());
				existingReceiptOrder.setVarianceAccountName(receiptorder.getVarianceAccountName());
				existingReceiptOrder.setCreatedDate(receiptorder.getCreatedDate());
				existingReceiptOrder.setModifedDate(receiptorder.getModifedDate());
			}
			receiptorder = receiptOrderDAO.store(existingReceiptOrder);
		} else {
			receiptorder = receiptOrderDAO.store(receiptorder);
		}
		receiptOrderDAO.flush();
	}

	/**
	 */
	@Transactional
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id) {
		return receiptOrderDAO.findReceiptOrderByPrimaryKey(id);
	}

	/**
	 * Return all ReceiptOrder entity
	 * 
	 */
	@Transactional
	public List<ReceiptOrder> findAllReceiptOrders(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ReceiptOrder>(receiptOrderDAO.findAllReceiptOrders(startResult, maxRows));
	}

	/**
	 * Delete an existing ReceiptOrder entity
	 * 
	 */
	@Transactional
	public void deleteReceiptOrder(ReceiptOrder receiptorder) {
		receiptOrderDAO.remove(receiptorder);
		receiptOrderDAO.flush();
	}

	@Override
	public String findReceiptOrderJSON(Integer start, Integer end, String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, 
									   String receiptOrderNo, BigDecimal credit, Integer option, Integer orderColumn, String orderBy, int status)  {
		List<ReceiptOrderDTO> listReceiptOrder = new java.util.ArrayList<ReceiptOrderDTO>();
		int recordsFiltered = 0;
			
		List<BigInteger> listBankStatementIds = new ArrayList<BigInteger>();
		if(!StringUtils.isEmpty(receiptOrderNo)) {
			System.out.println(receiptOrderNo);
			listBankStatementIds = bankStatementDAO.getBankStatementIds(receiptOrderNo, orderColumn, orderBy);
			recordsFiltered = listBankStatementIds.size();
		}
		else {
			//show Receipt Order data based on bank statement id
			listBankStatementIds = bankStatementDAO.getBankStatementIds(start, end, bankName, bankAccNo, transactionFrom, transactionTo, credit, option, orderColumn, orderBy, status);
		}
		if(listBankStatementIds.size() > 0)
		{
			listReceiptOrder = new java.util.ArrayList<ReceiptOrderDTO>(receiptOrderDAO.findReceiptOrderPaging(listBankStatementIds, orderColumn, orderBy));
			recordsFiltered = bankStatementDAO.countBankStatementFilter(bankName, bankAccNo, transactionFrom, transactionTo, credit, option, status);
		}
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listReceiptOrder);
        map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
        return jsonData;
	}

	@Override
	public String findArInvoiceToMatchRecepitOrderJSON(Integer start, Integer end, String fromApNo, String toApNo,
													   String fromMonth, String toMonth, String payerName, boolean isApproved, BigDecimal credit, Integer option, boolean isNotReceived) {
		List<ArInvoiceReceiptOrderDTO> listArInvoiceDTO = new java.util.ArrayList<ArInvoiceReceiptOrderDTO>();
		//int recordsFiltered = 0;
		listArInvoiceDTO = arInvoiceDAO.findArInvoiceToMatchRecepitOrder(start, end, fromApNo, toApNo, fromMonth, toMonth, payerName, isApproved, credit, option, isNotReceived);
		//recordsFiltered = arInvoiceDAO.countArInvoiceToMatchRecepitOrder(fromApNo, toApNo, fromMonth, toMonth, payerName, isApproved, credit, option);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listArInvoiceDTO);
        //map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
        return jsonData;
	}

	@Override
	public List<ArInvoiceReceiptOrderDTO> parseArInvoiceJSON(JsonArray jsonArray) {
		List<ArInvoiceReceiptOrderDTO> listArInvoiceReDTO = new ArrayList<ArInvoiceReceiptOrderDTO>();
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				ArInvoiceReceiptOrderDTO arInvoiceReDTO = new ArInvoiceReceiptOrderDTO();
				arInvoiceReDTO.setId(jsonObject.get("id").getAsBigInteger());
				arInvoiceReDTO.setVarianceAccountName(jsonObject.get("varianceAccountName").getAsString());
				arInvoiceReDTO.setSettedIncludeGstConvertedAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("settedAmountConverted").getAsString().replaceAll(",", "")));
				arInvoiceReDTO.setSettedIncludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("settedAmountOriginal").getAsString().replaceAll(",", "")));
				arInvoiceReDTO.setVarianceAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("varianceAmount").getAsString().replaceAll(",", "")));
				arInvoiceReDTO.setReceiptRate(NumberUtil.getBigDecimalScaleDown(10, jsonObject.get("receiptRate").getAsString().replaceAll(",", "")));
				arInvoiceReDTO.setIncludeGstOriginalAmount(NumberUtil.getBigDecimalScaleDown(2, jsonObject.get("includeGstOriginalAmount").getAsString().replaceAll(",", "")));
				listArInvoiceReDTO.add(arInvoiceReDTO);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listArInvoiceReDTO;
	}

	@Override
	public int appyMatch(BigInteger bankStatementId, List<ArInvoiceReceiptOrderDTO> listArInvoiceReDTO) {
		try {
			BankStatement bankStatement = bankStatementDAO.findBankStatementById(bankStatementId);
			if(bankStatement == null) {
				return -1;
			}
			else {
				/*
				 * settedIncludeGstOriginalAmount must not be greater than remainIncludeGstOriginalAmount
				 */
				for(ArInvoiceReceiptOrderDTO arInvoiceReDTO: listArInvoiceReDTO) {
					ArInvoiceReceiptStatus arInvoiceReceiptStatus = arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByArInvoiceId(arInvoiceReDTO.getId());
					if(arInvoiceReceiptStatus != null && arInvoiceReceiptStatus.getRemainIncludeGstOriginalAmount() != null
					   && arInvoiceReceiptStatus.getRemainIncludeGstOriginalAmount().compareTo(arInvoiceReDTO.getSettedIncludeGstOriginalAmount()) < 0)
					{
						return -2;
					}
				}
				
				/*
				 * calculate total setted amount
				 */
				BigDecimal totalSettedIncludeGstConvertedAmount = new BigDecimal("0");
				for(ArInvoiceReceiptOrderDTO arInvoiceReDTO: listArInvoiceReDTO) 
				{
					totalSettedIncludeGstConvertedAmount = totalSettedIncludeGstConvertedAmount.add(arInvoiceReDTO.getSettedIncludeGstOriginalAmount());
				}
				
				/*
				 * total amount must be equal with credit
				 */
				if(totalSettedIncludeGstConvertedAmount.compareTo(bankStatement.getCredit()) == 0) {
					List<String> receiptOrderNos = new ArrayList<String>();
					for(ArInvoiceReceiptOrderDTO arInvoiceRe: listArInvoiceReDTO) {
						String receiptOrderNo = appyMatch(bankStatementId, arInvoiceRe);
						receiptOrderNos.add(receiptOrderNo);
					}
					bankStatement.setReceiptOrderNo(String.join(", ", receiptOrderNos));
					bankStatement.setModifiedDate(Calendar.getInstance());
					bankStatementDAO.store(bankStatement);
				}
				else {
					return -3;
				}
				return 1;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}
	
	private String appyMatch(BigInteger bankStatementId, ArInvoiceReceiptOrderDTO arInvoiceReDTO) {
		ReceiptOrder receiptOrder = new ReceiptOrder();
		receiptOrder.setReceiptOrderNo(receiptOrderDAO.getReceiptOrderNo());
		receiptOrder.setTransactionId(bankStatementId);
		receiptOrder.setArInvoiceId(arInvoiceReDTO.getId());
		receiptOrder.setSettedIncludeGstConvertedAmount(arInvoiceReDTO.getSettedIncludeGstConvertedAmount());
		receiptOrder.setReceiptRate(arInvoiceReDTO.getReceiptRate());
		receiptOrder.setSettedIncludeGstOriginalAmount(arInvoiceReDTO.getSettedIncludeGstOriginalAmount());
		receiptOrder.setVarianceAmount(arInvoiceReDTO.getVarianceAmount());
		receiptOrder.setVarianceAccountName(arInvoiceReDTO.getVarianceAccountName());
		receiptOrder.setCreatedDate(Calendar.getInstance());
		receiptOrder.setModifedDate(Calendar.getInstance());
		receiptOrderDAO.store(receiptOrder);
		/*
		 * update receipt status
		 */
		
		//get current ArInvoiceReceiptStatus
		ArInvoiceReceiptStatus arInvoiceReceiptStatus = arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByArInvoiceId(arInvoiceReDTO.getId());
		
		if(arInvoiceReceiptStatus != null && arInvoiceReceiptStatus.getId() != null) {
			arInvoiceReceiptStatus.setSettedIncludeGstOriginalAmount(arInvoiceReceiptStatus.getSettedIncludeGstOriginalAmount().add(receiptOrder.getSettedIncludeGstOriginalAmount()));
		}
		else
		{
			arInvoiceReceiptStatus = new ArInvoiceReceiptStatus();
			arInvoiceReceiptStatus.setArInvoiceId(arInvoiceReDTO.getId());
			arInvoiceReceiptStatus.setSettedIncludeGstOriginalAmount(receiptOrder.getSettedIncludeGstOriginalAmount());
		}
		arInvoiceReceiptStatus.setRemainIncludeGstOriginalAmount(arInvoiceReDTO.getIncludeGstOriginalAmount().subtract(arInvoiceReceiptStatus.getSettedIncludeGstOriginalAmount()));
		arInvoiceReceiptStatusDAO.store(arInvoiceReceiptStatus);
		
		/*
		 * update ArInvoice status
		 */
		ArInvoice arInvoice = arInvoiceDAO.findArInvoiceById(arInvoiceReDTO.getId());
		if(arInvoice != null) {
			if(arInvoiceReceiptStatus.getRemainIncludeGstOriginalAmount().compareTo(BigDecimal.ZERO) == 0)
			{
				arInvoice.setStatus(STATUS_RECEIVED);
			}
			else {
				arInvoice.setStatus(STATUS_PARTIAL_RECEIVED);
			}
			arInvoiceDAO.store(arInvoice);
		}
		return receiptOrder.getReceiptOrderNo();
	}

	@Override
	public int resetRecepitOrder(BigInteger bankStatementId) {
		try {
			BankStatement bankStatement = bankStatementDAO.findBankStatementById(bankStatementId);
			if(bankStatement != null) {
				/*
				 * reset bank statement 
				 */
				bankStatement.setReceiptOrderNo(null);
				bankStatementDAO.store(bankStatement);
				/*
				 * reset receipt order
				 */
				Set<ReceiptOrder> set = receiptOrderDAO.findReceiptOrderByTransactionId(bankStatementId);
				for(ReceiptOrder receiptOrder: set) {
					/*
					 * reset ArInvoiceReceiptStatus
					 */
					ArInvoiceReceiptStatus arInvoiceReceiptStatus = arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByArInvoiceId(receiptOrder.getArInvoiceId());	
					if(arInvoiceReceiptStatus != null && arInvoiceReceiptStatus.getId() != null) {
						arInvoiceReceiptStatus.setSettedIncludeGstOriginalAmount(arInvoiceReceiptStatus.getSettedIncludeGstOriginalAmount().subtract(receiptOrder.getSettedIncludeGstOriginalAmount()));
						arInvoiceReceiptStatus.setRemainIncludeGstOriginalAmount(arInvoiceReceiptStatus.getRemainIncludeGstOriginalAmount().add(receiptOrder.getSettedIncludeGstOriginalAmount()));
						arInvoiceReceiptStatusDAO.store(arInvoiceReceiptStatus);
						/*
						 * update ArInvoice status
						 */
						ArInvoice arInvoice = arInvoiceDAO.findArInvoiceById(receiptOrder.getArInvoiceId());
						if(arInvoice != null) {
							if(arInvoiceReceiptStatus.getRemainIncludeGstOriginalAmount().compareTo(BigDecimal.ZERO) == 0)
							{
								arInvoice.setStatus(STATUS_RECEIVED);
							}
							else if(arInvoiceReceiptStatus.getSettedIncludeGstOriginalAmount().compareTo(BigDecimal.ZERO) > 0){
								arInvoice.setStatus(STATUS_PARTIAL_RECEIVED);
							}
							else {
								arInvoice.setStatus(STATUS_NOT_RECEIVED);
							}
							arInvoiceDAO.store(arInvoice);
						}
					}	
					/*
					 * remove receipt order
					 */
					receiptOrderDAO.remove(receiptOrder);
				}
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}