package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.service.ApInvoicePaymentDetailsService;

/**
 * Spring service that handles CRUD requests for ApInvoicePaymentDetails entities
 * 
 */

@Service("ApInvoicePaymentDetailsService")

@Transactional
public class ApInvoicePaymentDetailsServiceImpl implements ApInvoicePaymentDetailsService {

	/**
	 * DAO injected by Spring that manages ApInvoice entities
	 * 
	 */
	@Autowired
	private ApInvoiceDAO apInvoiceDAO;

	/**
	 * DAO injected by Spring that manages ApInvoicePaymentDetails entities
	 * 
	 */
	@Autowired
	private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;

	/**
	 * DAO injected by Spring that manages PaymentOrder entities
	 * 
	 */
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;

	/**
	 * Instantiates a new ApInvoicePaymentDetailsServiceImpl.
	 *
	 */
	public ApInvoicePaymentDetailsServiceImpl() {
	}

	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
	/**
	 * Save an existing ApInvoicePaymentDetails entity
	 * 
	 */
    @Transactional
    public ApInvoicePaymentDetails saveApInvoicePaymentDetails(ApInvoicePaymentDetails apinvoicepaymentdetails) {
        ApInvoicePaymentDetails existingApInvoicePaymentDetails = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(apinvoicepaymentdetails.getId());
        ApInvoicePaymentDetails resultApInvoicePaymentDetails = null;

        if (existingApInvoicePaymentDetails != null) {
            existingApInvoicePaymentDetails.setPaymentIncludeGstConvertedAmount(apinvoicepaymentdetails.getPaymentIncludeGstConvertedAmount());
            existingApInvoicePaymentDetails.setPaymentIncludeGstOriginalAmount(apinvoicepaymentdetails.getPaymentIncludeGstOriginalAmount());
            existingApInvoicePaymentDetails.setPaymentRate(apinvoicepaymentdetails.getPaymentRate());
            existingApInvoicePaymentDetails.setPayeeBankName(apinvoicepaymentdetails.getPayeeBankName());
            existingApInvoicePaymentDetails.setPayeeBankAccNo(apinvoicepaymentdetails.getPayeeBankAccNo());
            existingApInvoicePaymentDetails.setVarianceAmount(apinvoicepaymentdetails.getVarianceAmount());
            existingApInvoicePaymentDetails.setVarianceAccountCode(apinvoicepaymentdetails.getVarianceAccountCode());
            existingApInvoicePaymentDetails.setCreatedDate(apinvoicepaymentdetails.getCreatedDate());
            existingApInvoicePaymentDetails.setModifiedDate(apinvoicepaymentdetails.getModifiedDate());
            resultApInvoicePaymentDetails = apInvoicePaymentDetailsDAO.store(existingApInvoicePaymentDetails);
        } else {
            resultApInvoicePaymentDetails = apInvoicePaymentDetailsDAO.store(apinvoicepaymentdetails);
        }
        apInvoicePaymentDetailsDAO.flush();
        return resultApInvoicePaymentDetails;
    }

	/**
	 * Save an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public ApInvoicePaymentDetails saveApInvoicePaymentDetailsPaymentOrder(BigInteger id, PaymentOrder related_paymentorder) {
		ApInvoicePaymentDetails apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(id, -1, -1);
		PaymentOrder existingpaymentOrder = paymentOrderDAO.findPaymentOrderByPrimaryKey(related_paymentorder.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpaymentOrder != null) {
			existingpaymentOrder.setId(related_paymentorder.getId());
			existingpaymentOrder.setPaymentOrderNo(related_paymentorder.getPaymentOrderNo());
			existingpaymentOrder.setBankName(related_paymentorder.getBankName());
			existingpaymentOrder.setBankAccount(related_paymentorder.getBankAccount());
			existingpaymentOrder.setIncludeGstConvertedAmount(related_paymentorder.getIncludeGstConvertedAmount());
			existingpaymentOrder.setIncludeGstOriginalAmount(related_paymentorder.getIncludeGstOriginalAmount());
			existingpaymentOrder.setValueDate(related_paymentorder.getValueDate());
			existingpaymentOrder.setBankRefNo(related_paymentorder.getBankRefNo());
			existingpaymentOrder.setDescription(related_paymentorder.getDescription());
			existingpaymentOrder.setPaymentApprovalNo(related_paymentorder.getPaymentApprovalNo());
			existingpaymentOrder.setAccountPayableCode(related_paymentorder.getAccountPayableCode());
			existingpaymentOrder.setAccountPayableName(related_paymentorder.getAccountPayableName());
			existingpaymentOrder.setStatus(related_paymentorder.getStatus());
			existingpaymentOrder.setCreatedDate(related_paymentorder.getCreatedDate());
			existingpaymentOrder.setModifiedDate(related_paymentorder.getModifiedDate());
			related_paymentorder = existingpaymentOrder;
		} else {
			related_paymentorder = paymentOrderDAO.store(related_paymentorder);
			paymentOrderDAO.flush();
		}

		apinvoicepaymentdetails.setPaymentOrder(related_paymentorder);
		related_paymentorder.getApInvoicePaymentDetailses().add(apinvoicepaymentdetails);
		apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.store(apinvoicepaymentdetails);
		apInvoicePaymentDetailsDAO.flush();

		related_paymentorder = paymentOrderDAO.store(related_paymentorder);
		paymentOrderDAO.flush();

		return apinvoicepaymentdetails;
	}

	/**
	 * Return a count of all ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public Integer countApInvoicePaymentDetailss() {
		return ((Long) apInvoicePaymentDetailsDAO.createQuerySingleResult("select count(o) from ApInvoicePaymentDetails o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public ApInvoicePaymentDetails deleteApInvoicePaymentDetailsPaymentOrder(BigInteger apinvoicepaymentdetails_id, BigInteger related_paymentorder_id) {
		ApInvoicePaymentDetails apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(apinvoicepaymentdetails_id, -1, -1);
		PaymentOrder related_paymentorder = paymentOrderDAO.findPaymentOrderByPrimaryKey(related_paymentorder_id, -1, -1);

		apinvoicepaymentdetails.setPaymentOrder(null);
		related_paymentorder.getApInvoicePaymentDetailses().remove(apinvoicepaymentdetails);
		apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.store(apinvoicepaymentdetails);
		apInvoicePaymentDetailsDAO.flush();

		related_paymentorder = paymentOrderDAO.store(related_paymentorder);
		paymentOrderDAO.flush();

		paymentOrderDAO.remove(related_paymentorder);
		paymentOrderDAO.flush();

		return apinvoicepaymentdetails;
	}

	/**
	 * Load an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> loadApInvoicePaymentDetailss() {
		return apInvoicePaymentDetailsDAO.findAllApInvoicePaymentDetailss();
	}

	/**
	 * Delete an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public void deleteApInvoicePaymentDetails(ApInvoicePaymentDetails apinvoicepaymentdetails) {
		apInvoicePaymentDetailsDAO.remove(apinvoicepaymentdetails);
		apInvoicePaymentDetailsDAO.flush();
	}

	/**
	 * Save an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public ApInvoicePaymentDetails saveApInvoicePaymentDetailsApInvoice(BigInteger id, ApInvoice related_apinvoice) {
		ApInvoicePaymentDetails apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(id, -1, -1);
		ApInvoice existingapInvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(related_apinvoice.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoice != null) {
			existingapInvoice.setId(related_apinvoice.getId());
			existingapInvoice.setPayeeCode(related_apinvoice.getPayeeCode());
			existingapInvoice.setPayeeName(related_apinvoice.getPayeeName());
			existingapInvoice.setPayeeTypeCode(related_apinvoice.getPayeeTypeCode());
			existingapInvoice.setPayeeTypeName(related_apinvoice.getPayeeTypeName());
			existingapInvoice.setApInvoiceNo(related_apinvoice.getApInvoiceNo());
			existingapInvoice.setInvoiceNo(related_apinvoice.getInvoiceNo());
			existingapInvoice.setOriginalCurrencyCode(related_apinvoice.getOriginalCurrencyCode());
			existingapInvoice.setFxRate(related_apinvoice.getFxRate());
			existingapInvoice.setGstRate(related_apinvoice.getGstRate());
			existingapInvoice.setDescription(related_apinvoice.getDescription());
			existingapInvoice.setInvoiceIssuedDate(related_apinvoice.getInvoiceIssuedDate());
			existingapInvoice.setClaimType(related_apinvoice.getClaimType());
			existingapInvoice.setGstType(related_apinvoice.getGstType());
			existingapInvoice.setAccountPayableCode(related_apinvoice.getAccountPayableCode());
			existingapInvoice.setAccountPayableName(related_apinvoice.getAccountPayableName());
			existingapInvoice.setExcludeGstOriginalAmount(related_apinvoice.getExcludeGstOriginalAmount());
			existingapInvoice.setExcludeGstConvertedAmount(related_apinvoice.getExcludeGstConvertedAmount());
			existingapInvoice.setGstOriginalAmount(related_apinvoice.getGstOriginalAmount());
			existingapInvoice.setGstConvertedAmount(related_apinvoice.getGstConvertedAmount());
			existingapInvoice.setIncludeGstOriginalAmount(related_apinvoice.getIncludeGstOriginalAmount());
			existingapInvoice.setIncludeGstConvertedAmount(related_apinvoice.getIncludeGstConvertedAmount());
			existingapInvoice.setPaymentTerm(related_apinvoice.getPaymentTerm());
			existingapInvoice.setScheduledPaymentDate(related_apinvoice.getScheduledPaymentDate());
			existingapInvoice.setStatus(related_apinvoice.getStatus());
			existingapInvoice.setCreatedDate(related_apinvoice.getCreatedDate());
			existingapInvoice.setModifiedDate(related_apinvoice.getModifiedDate());
			related_apinvoice = existingapInvoice;
		} else {
			related_apinvoice = apInvoiceDAO.store(related_apinvoice);
			apInvoiceDAO.flush();
		}

		apinvoicepaymentdetails.setApInvoice(related_apinvoice);
		related_apinvoice.getApInvoicePaymentDetailses().add(apinvoicepaymentdetails);
		apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.store(apinvoicepaymentdetails);
		apInvoicePaymentDetailsDAO.flush();

		related_apinvoice = apInvoiceDAO.store(related_apinvoice);
		apInvoiceDAO.flush();

		return apinvoicepaymentdetails;
	}

	/**
	 */
	@Transactional
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id) {
		return apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(id);
	}

	/**
	 * Delete an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public ApInvoicePaymentDetails deleteApInvoicePaymentDetailsApInvoice(BigInteger apinvoicepaymentdetails_id, BigInteger related_apinvoice_id) {
		ApInvoicePaymentDetails apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(apinvoicepaymentdetails_id, -1, -1);
		ApInvoice related_apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(related_apinvoice_id, -1, -1);

		apinvoicepaymentdetails.setApInvoice(null);
		related_apinvoice.getApInvoicePaymentDetailses().remove(apinvoicepaymentdetails);
		apinvoicepaymentdetails = apInvoicePaymentDetailsDAO.store(apinvoicepaymentdetails);
		apInvoicePaymentDetailsDAO.flush();

		related_apinvoice = apInvoiceDAO.store(related_apinvoice);
		apInvoiceDAO.flush();

		apInvoiceDAO.remove(related_apinvoice);
		apInvoiceDAO.flush();

		return apinvoicepaymentdetails;
	}

	/**
	 * Return all ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public List<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoicePaymentDetails>(apInvoicePaymentDetailsDAO.findAllApInvoicePaymentDetailss(startResult, maxRows));
	}

	public BigDecimal getTotalOriginalAmountByApInvoiceId(BigInteger paymentId, BigInteger apInvoiceId) {
		return apInvoicePaymentDetailsDAO.getTotalOriginalAmountByApInvoiceId(paymentId, apInvoiceId);
	}
	
}
