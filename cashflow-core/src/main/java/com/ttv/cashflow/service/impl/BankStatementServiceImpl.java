package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.BankAccountMaster;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.service.BankStatementUtilService;
import com.ttv.cashflow.service.BankStatementService;
import com.ttv.cashflow.util.BankAccInfo;
import com.ttv.cashflow.util.CalendarUtil;
import com.ttv.cashflow.util.Constant;
import com.ttv.cashflow.util.NumberUtil;

/**
 * Spring service that handles CRUD requests for BankStatement entities
 * 
 */

@Service("BankStatementService")
@Transactional
public class BankStatementServiceImpl implements BankStatementService {
	@Autowired
	private BankStatementDAO bankStatementDAO;
	@Autowired
	private BankAccountMasterDAO bankAccountMasterDAO;
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;	
	@Autowired
	private SystemValueDAO systemValueDAO;
	@Autowired
	@Qualifier("BankStatementForApInvoiceServiceImpl")
	private BankStatementUtilService bankStatementForAPInvoiceService;
	@Autowired
	@Qualifier("BankStatementForPayrollServiceImpl")
	private BankStatementUtilService bankStatementForPayrollService;
	@Autowired
	@Qualifier("BankStatementForReimbursementServiceImpl")
	private BankStatementUtilService bankStatementForReimbursementService;
	
	/**
	 * Instantiates a new BankStatementServiceImpl.
	 *
	 */
	public BankStatementServiceImpl() {
	}

	/**
	 * Return a count of all BankStatement entity
	 * 
	 */
	@Transactional
	public Integer countBankStatements() {
		return ((Long) bankStatementDAO.createQuerySingleResult("select count(o) from BankStatement o").getSingleResult()).intValue();
	}

	/**
	 * Return all BankStatement entity
	 * 
	 */
	@Transactional
	public List<BankStatement> findAllBankStatements(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<BankStatement>(bankStatementDAO.findAllBankStatements(startResult, maxRows));
	}

	/**
	 * Load an existing BankStatement entity
	 * 
	 */
	@Transactional
	public Set<BankStatement> loadBankStatements() {
		return bankStatementDAO.findAllBankStatements();
	}

	/**
	 * Save an existing BankStatement entity
	 * 
	 */
	@Transactional
	public void saveBankStatement(BankStatement bankstatement) {
		BankStatement existingBankStatement = bankStatementDAO.findBankStatementByPrimaryKey(bankstatement.getId());
		if (existingBankStatement != null) {
			if (existingBankStatement != bankstatement) {
				existingBankStatement.setId(bankstatement.getId());
				existingBankStatement.setBankName(bankstatement.getBankName());
				existingBankStatement.setBankAccountNo(bankstatement.getBankAccountNo());
				existingBankStatement.setTransactionDate(bankstatement.getTransactionDate());
				existingBankStatement.setValueDate(bankstatement.getValueDate());
				existingBankStatement.setDescription1(bankstatement.getDescription1());
				existingBankStatement.setDescription2(bankstatement.getDescription2());
				existingBankStatement.setReference1(bankstatement.getReference1());
				existingBankStatement.setReference2(bankstatement.getReference2());
				existingBankStatement.setDebit(bankstatement.getDebit());
				existingBankStatement.setCredit(bankstatement.getCredit());
				existingBankStatement.setPaymentOrderNo(bankstatement.getPaymentOrderNo());
				existingBankStatement.setBalance(bankstatement.getBalance());
				existingBankStatement.setBeginningBalance(bankstatement.getBeginningBalance());
				existingBankStatement.setModifiedDate(Calendar.getInstance());
			}
			bankstatement = bankStatementDAO.store(existingBankStatement);
		} else {
			bankstatement.setCreatedDate(Calendar.getInstance());
			bankstatement = bankStatementDAO.store(bankstatement);
		}
		bankStatementDAO.flush();
	}

	/**
	 */
	@Transactional
	public BankStatement findBankStatementByPrimaryKey(BigInteger id) {
		return bankStatementDAO.findBankStatementByPrimaryKey(id);
	}

	/**
	 * Delete an existing BankStatement entity
	 * 
	 */
	@Transactional
	public void deleteBankStatement(BankStatement bankstatement) {
		/*
		 * prevent remove statement set for payment order or receipt order
		 * update ending balance in bank account master data
		 */
		if(bankstatement.getBankAccountNo() != null && bankstatement.getPaymentOrderNo() == null && bankstatement.getReceiptOrderNo() == null) {
			Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterDAO.findBankAccountMasterByBankAccountNo(bankstatement.getBankAccountNo());
			if(!setBankAccountMaster.isEmpty()) {
				Iterator<BankAccountMaster> bankAccIterator = setBankAccountMaster.iterator();
				while(bankAccIterator.hasNext()) {
					BankAccountMaster bankAccountMaster = bankAccIterator.next();
					bankAccountMaster.setEndingBalance(bankAccountMaster.getEndingBalance().subtract(bankstatement.getCredit()).add(bankstatement.getDebit()));
					bankAccountMasterDAO.store(bankAccountMaster);
				}
			}
		}
		bankStatementDAO.remove(bankstatement);
		bankStatementDAO.flush();
	}

	public void deleteBankStatementByIds(BigInteger[] ids) {
		for (BigInteger id: ids) {
			BankStatement bankStatement = bankStatementDAO.findBankStatementById(id);
	        if (bankStatement != null) {
	           deleteBankStatement(bankStatement);
	        }
	    }
	}

	public String findBankStatementJson(String bankName, String bankAccNo,
										Calendar transactionFrom, Calendar transactionTo, BigInteger transactionNumber, Integer transDateFilter, Integer subsidiaryId) {
		List<BankStatement> bankStatements = new java.util.ArrayList<BankStatement>();
		int recordsFiltered = 0;
		Map<String, Object> map = new HashMap<String, Object>();
		if(transactionNumber != null && transactionNumber.doubleValue() > 0) {
			BankStatement bankStatement = bankStatementDAO.findBankStatementById(transactionNumber);
			if(bankStatement != null) {
				PaymentOrder paymentOrder = paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(bankStatement.getPaymentOrderNo());
				if(paymentOrder != null) {
					bankStatement.setPaymentOrderId(paymentOrder.getId());
				}
				bankStatements.add(bankStatement);
			}
			recordsFiltered = bankStatements.size();
		}
		else {
			bankStatements = new java.util.ArrayList<BankStatement>(bankStatementDAO.findBankStatementPaging(bankName, bankAccNo, transactionFrom, transactionTo, transDateFilter));
			recordsFiltered = bankStatementDAO.countBankStatementFilter(bankName, bankAccNo);
		}
		Calendar maxTransactionDate = getMaxTransactionDate(bankName, bankAccNo);
		if(maxTransactionDate != null)
		{
			 map.put("maxTransactionDate", CalendarUtil.toString(maxTransactionDate));
		}
        map.put("data", bankStatements);
        map.put("recordsFiltered", recordsFiltered);
        //check is there any someone is editing the same bank account
        BankAccInfo bankAccInfo = new BankAccInfo(subsidiaryId, bankName, bankAccNo);
		int roleEdit = 1;//allow edit
		String info = BankAccInfo.MAP_EDITING_USER.get(bankAccInfo);
		if(info != null) {
			roleEdit = 0;//prevent edit
			String tokens[] = info.split(Constant.TOKEN_SEPERATOR);
			if(tokens.length == 2)
			{
				map.put("userEditing", tokens[1]);
			}
		}
		map.put("roleEdit", roleEdit);
        Gson gson = new Gson();
        String jBankMaster = gson.toJson(map);
        return jBankMaster;
	}

	public List<BankStatement> parseBankStatementJSON(JsonArray jsonArray) {
		List<BankStatement> listBankStatements = new ArrayList<>();
		try {
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonObject bankStatementObject = jsonArray.get(i).getAsJsonObject();
				if(bankStatementObject.has("removed")) {
					BankStatement bankStatement = bankStatementDAO.findBankStatementById(bankStatementObject.get("id").getAsBigInteger());
			        if (bankStatement != null) {
			           deleteBankStatement(bankStatement);
			        }
					continue;
				}
				BankStatement bankStatement = new BankStatement();
				bankStatement.setId(bankStatementObject.get("id").getAsBigInteger());
				
				if(bankStatementObject.has("bankAccountNo"))
				{
					bankStatement.setBankAccountNo(bankStatementObject.get("bankAccountNo").getAsString());
				}
				
				if(bankStatementObject.has("bankName"))
				{
					bankStatement.setBankName(bankStatementObject.get("bankName").getAsString());
				}
				
				if(bankStatementObject.has("credit"))
				{
					bankStatement.setCredit(NumberUtil.getBigDecimalScaleDown(2, bankStatementObject.get("credit").getAsString().replaceAll(",", "")));
				}
				
				if(bankStatementObject.has("debit"))
				{
					bankStatement.setDebit(NumberUtil.getBigDecimalScaleDown(2, bankStatementObject.get("debit").getAsString().replaceAll(",", "")));
				}
				
				if(bankStatementObject.has("balance"))
				{
					bankStatement.setBalance(NumberUtil.getBigDecimalScaleDown(2, bankStatementObject.get("balance").getAsString().replaceAll(",", "")));
				}
				
				if(bankStatementObject.has("beginningBalance"))
				{
					bankStatement.setBeginningBalance(NumberUtil.getBigDecimalScaleDown(2, bankStatementObject.get("beginningBalance").getAsString().replaceAll(",", "")));
				}
				
				if(bankStatementObject.has("paymentOrderNo"))
				{
					bankStatement.setPaymentOrderNo(bankStatementObject.get("paymentOrderNo").getAsString());
				}
				
				if(bankStatementObject.has("transactionDate"))
					bankStatement.setTransactionDate(CalendarUtil.getCalendar(bankStatementObject.get("transactionDate").getAsString(), Constant.DDMMYYYY));
				
				if(bankStatementObject.has("valueDate"))
				{
					bankStatement.setValueDate(CalendarUtil.getCalendar(bankStatementObject.get("valueDate").getAsString(), Constant.DDMMYYYY));
				}
				
				if(bankStatementObject.has("description1"))
				{
					bankStatement.setDescription1(bankStatementObject.get("description1").getAsString());
				}
				
				if(bankStatementObject.has("description2"))
				{
					bankStatement.setDescription2(bankStatementObject.get("description2").getAsString());
				}
				
				if(bankStatementObject.has("reference1"))
				{
					bankStatement.setReference1(bankStatementObject.get("reference1").getAsString());
				}
				
				if(bankStatementObject.has("reference2"))
				{
					bankStatement.setReference2(bankStatementObject.get("reference2").getAsString());
				}
				
				if(bankStatementObject.has("currencyCode"))
				{
					bankStatement.setCurrencyCode(bankStatementObject.get("currencyCode").getAsString());
				}
				listBankStatements.add(bankStatement);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listBankStatements;
	}
	
	public int updatePaymentOrderStatus(Set<String> setPaymentOrderNo, Map<String, String> mapPaymentOrderNoRef) {
		try {
			//find all payment order relate with bank statement
			List<PaymentOrderDTO> listPaymentHasPaid = bankStatementDAO.getPaymentOrderPaid(setPaymentOrderNo);
			for(PaymentOrderDTO paymentOrderDTO: listPaymentHasPaid)
			{
				PaymentOrder paymentOrder = paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(paymentOrderDTO.getPaymentOrderNo());
				if(paymentOrder != null)
				{
					/*
					 * update bank status in paymentOrder if paid enough
					 */
					if(paymentOrder.getIncludeGstConvertedAmount() != null && paymentOrderDTO.getPaymentTotalAmount().compareTo(paymentOrder.getIncludeGstConvertedAmount()) == 0)
					{
						paymentOrder.setStatus(Constant.PAY_PROCESSED);
					}
					paymentOrderDAO.store(paymentOrder);
					/*
					 * find all ap invoice relate with payment order
					 */
					if(paymentOrder.isPaymentOrderApInvoice())
					{
						bankStatementForAPInvoiceService.updatePaymentStatus(paymentOrder);
					}
					if(paymentOrder.isPaymentOrderPayroll()) {
						bankStatementForPayrollService.updatePaymentStatus(paymentOrder);
					}
					if(paymentOrder.isPaymentOrderReimbursement()) {
						bankStatementForReimbursementService.updatePaymentStatus(paymentOrder);
					}
				}
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int checkExists(BankStatement bankStatement) {
		return bankStatementDAO.checkExists(bankStatement);
	}

	@Override
	public Calendar getMaxTransactionDate(String bankName, String bankAccNo) {
		return bankStatementDAO.getMaxTransactionDate(bankName, bankAccNo);
	}

	@Override
	public int saveListBankStatement(List<BankStatement> listBankStatements) {
		try {
			Set<String> setPaymentOrderNo = new HashSet<String>();
			//map store payment order No - Bank Reference
			Map<String, String> mapPaymentOrderNoRef = new HashMap<String, String>();
			for(BankStatement bankStatement: listBankStatements)
			{
				saveBankStatement(bankStatement);
				//update bank reference in payment order table
				if(!StringUtils.isEmpty(bankStatement.getPaymentOrderNo()))
				{
					setPaymentOrderNo.add(bankStatement.getPaymentOrderNo());
				}
			}
			//update some info in payment order and ap invoice
			if(!setPaymentOrderNo.isEmpty())
			{
				updatePaymentOrderStatus(setPaymentOrderNo, mapPaymentOrderNoRef);
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int updateEndingBalance(String bankAccNo, BigDecimal endingBalance) {
		try {
			Set<BankAccountMaster> setBankAccountMaster = bankAccountMasterDAO.findBankAccountMasterByBankAccountNo(bankAccNo);
			if(!setBankAccountMaster.isEmpty()) {
				Iterator<BankAccountMaster> bankAccIterator = setBankAccountMaster.iterator();
				while(bankAccIterator.hasNext()) {
					BankAccountMaster bankAccountMaster = bankAccIterator.next();
					bankAccountMaster.setEndingBalance(endingBalance);
					bankAccountMasterDAO.store(bankAccountMaster);
				}
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int resetPaymentOrderStatus(BigInteger bankStatementId) {
		BankStatement bankStatement = bankStatementDAO.findBankStatementById(bankStatementId);
		if(bankStatement != null && !StringUtils.isEmpty(bankStatement.getPaymentOrderNo())) {
			/*
			 * reset payment order status
			 */
			PaymentOrder paymentOrder = paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(bankStatement.getPaymentOrderNo());
			paymentOrder.setStatus(Constant.PAY_PROCESSING);
			paymentOrderDAO.store(paymentOrder);
			/*
			 * remove payment order No. from bank statement
			 */
			bankStatement.setPaymentOrderNo(null);
			bankStatementDAO.store(bankStatement);
			/*
			 * find all ap invoice relate with payment order
			 */
			try {
				if(paymentOrder.isPaymentOrderApInvoice()) {
					bankStatementForAPInvoiceService.resetPaymentOrderStatus(paymentOrder);
				}
				if(paymentOrder.isPaymentOrderPayroll()){
					bankStatementForPayrollService.resetPaymentOrderStatus(paymentOrder);
				}
				if(paymentOrder.isPaymentOrderReimbursement()){
					bankStatementForReimbursementService.resetPaymentOrderStatus(paymentOrder);
				}
				return 1;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	@Override
	public boolean validateTransactionDateBeforeEdit(BigInteger bankStatementId) {
		try {
			SystemValue systemValue = systemValueDAO.findSystemValueByTypeAndCode("BSM_DAYS", "BSM_DAYS");
			if(systemValue != null) {
				BankStatement bankStatement = bankStatementDAO.findBankStatementById(bankStatementId);
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, new Integer("-" + systemValue.getValue()));
				if(bankStatement.getTransactionDate().compareTo(cal) <= 0)
				{
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
