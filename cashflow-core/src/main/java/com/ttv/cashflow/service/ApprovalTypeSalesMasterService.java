
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;

/**
 * Spring service that handles CRUD requests for ApprovalTypeSalesMaster entities
 * 
 */
public interface ApprovalTypeSalesMasterService {

	/**
	* Return all ApprovalTypeSalesMaster entity
	* 
	 */
	public List<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters(Integer startResult, Integer maxRows);

	/**
	* Return a count of all ApprovalTypeSalesMaster entity
	* 
	 */
	public Integer countApprovalTypeSalesMasters();

	/**
	* Save an existing ApprovalTypeSalesMaster entity
	* 
	 */
	public void saveApprovalTypeSalesMaster(ApprovalTypeSalesMaster approvaltypesalesmaster);

	/**
	* Delete an existing ApprovalTypeSalesMaster entity
	* 
	 */
	public void deleteApprovalTypeSalesMaster(ApprovalTypeSalesMaster approvaltypesalesmaster_1);

	/**
	 */
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByPrimaryKey(String code);

	/**
	* Load an existing ApprovalTypeSalesMaster entity
	* 
	 */
	public Set<ApprovalTypeSalesMaster> loadApprovalTypeSalesMasters();
	public void deleteApprovalTypeMasterByCodes(String codes[]);
	public String findApprovalTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}