package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.DivisionMasterDAO;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.dto.EmployeeMasterDTO;
import com.ttv.cashflow.service.EmployeeMasterService;
/**
 * Spring service that handles CRUD requests for EmployeeMaster entities
 * 
 */

@Service("EmployeeMasterService")

@Transactional
public class EmployeeMasterServiceImpl implements EmployeeMasterService {

	/**
	 * DAO injected by Spring that manages DivisionMaster entities
	 * 
	 */
	@Autowired
    private DivisionMasterDAO divisionMasterDAO;

	/**
	 * DAO injected by Spring that manages EmployeeMaster entities
	 * 
	 */
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	/**
	 * Instantiates a new EmployeeMasterServiceImpl.
	 *
	 */
	public EmployeeMasterServiceImpl() {
	}

	/**
	 * Return all EmployeeMaster entity
	 * 
	 */
	@Transactional
	public List<EmployeeMaster> findAllEmployeeMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<EmployeeMaster>(employeeMasterDAO.findAllEmployeeMasters(startResult, maxRows));
	}

	/**
	 * Delete an existing DivisionMaster entity
	 * 
	 */
	@Transactional
    public EmployeeMaster deleteEmployeeMasterDivisionMaster(BigInteger employeemaster_id, BigInteger related_divisionmaster_id) {
		EmployeeMaster employeemaster = employeeMasterDAO.findEmployeeMasterByPrimaryKey(employeemaster_id, -1, -1);
        DivisionMaster related_divisionmaster = divisionMasterDAO.findDivisionMasterByPrimaryKey(related_divisionmaster_id, -1, -1);

		employeemaster.setDivisionMaster(null);
        related_divisionmaster.getEmployeeMasters().remove(employeemaster);
		employeemaster = employeeMasterDAO.store(employeemaster);
		employeeMasterDAO.flush();

        related_divisionmaster = divisionMasterDAO.store(related_divisionmaster);
        divisionMasterDAO.flush();

        divisionMasterDAO.remove(related_divisionmaster);
        divisionMasterDAO.flush();

		return employeemaster;
	}

	/**
	 * Load an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public Set<EmployeeMaster> loadEmployeeMasters() {
		return employeeMasterDAO.findAllEmployeeMasters();
	}

	/**
	 * Delete an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public void deleteEmployeeMaster(EmployeeMaster employeemaster) {
		employeeMasterDAO.remove(employeemaster);
		employeeMasterDAO.flush();
	}

	/**
	 * Save an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public void saveEmployeeMaster(EmployeeMaster employeemaster) {
        EmployeeMaster existingEmployeeMaster = findEmployeeMasterByCode(employeemaster.getCode());

		if (existingEmployeeMaster != null) {
				existingEmployeeMaster.setDivisionMaster(employeemaster.getDivisionMaster());
				existingEmployeeMaster.setCode(employeemaster.getCode());
				existingEmployeeMaster.setName(employeemaster.getName());
				existingEmployeeMaster.setRoleTitle(employeemaster.getRoleTitle());
				existingEmployeeMaster.setTeam(employeemaster.getTeam());
				existingEmployeeMaster.setJoinDate(employeemaster.getJoinDate());
				existingEmployeeMaster.setResignDate(employeemaster.getResignDate());
            employeeMasterDAO.store(existingEmployeeMaster);
		} else {
            employeeMasterDAO.store(employeemaster);
		}
		employeeMasterDAO.flush();
	}

	/**
	 * Save an existing DivisionMaster entity
	 * 
	 */
	@Transactional
    public EmployeeMaster saveEmployeeMasterDivisionMaster(BigInteger id, DivisionMaster related_divisionmaster) {
		EmployeeMaster employeemaster = employeeMasterDAO.findEmployeeMasterByPrimaryKey(id, -1, -1);
        DivisionMaster existingdivisionMaster = divisionMasterDAO.findDivisionMasterByPrimaryKey(related_divisionmaster.getId());

		// copy into the existing record to preserve existing relationships
        if (existingdivisionMaster != null) {
            existingdivisionMaster.setId(related_divisionmaster.getId());
            existingdivisionMaster.setCode(related_divisionmaster.getCode());
            existingdivisionMaster.setName(related_divisionmaster.getName());
            related_divisionmaster = existingdivisionMaster;
		} else {
            related_divisionmaster = divisionMasterDAO.store(related_divisionmaster);
            divisionMasterDAO.flush();
		}

        employeemaster.setDivisionMaster(related_divisionmaster);
        related_divisionmaster.getEmployeeMasters().add(employeemaster);
		employeemaster = employeeMasterDAO.store(employeemaster);
		employeeMasterDAO.flush();

        related_divisionmaster = divisionMasterDAO.store(related_divisionmaster);
        divisionMasterDAO.flush();

		return employeemaster;
	}

	/**
	 * Return a count of all EmployeeMaster entity
	 * 
	 */
	@Transactional
	public Integer countEmployeeMasters() {
		return ((Long) employeeMasterDAO.createQuerySingleResult("select count(o) from EmployeeMaster o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id) {
		return employeeMasterDAO.findEmployeeMasterByPrimaryKey(id);
	}

    @Override
    public String findEmployeeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
        List<EmployeeMasterDTO> employeeMasterDTOs = employeeMasterDAO.findEmployeeMasterPaging(start, end, orderColumn, orderBy, keyword);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", employeeMasterDTOs);
        map.put("recordsFiltered", employeeMasterDAO.countEmployeeMasterFilter(keyword));

        Gson gson = new Gson();
        return gson.toJson(map);
    }

    @Override
    public void deleteEmployeeMasterByCodes(String[] codes) {
        for (String code : codes) {
            Set<EmployeeMaster> employeeMasters = employeeMasterDAO.findEmployeeMasterByCode(code);

            if (!org.springframework.util.CollectionUtils.isEmpty(employeeMasters)) {
                // get only first element in set which is returned from db because code is unique
                EmployeeMaster employeeMaster = employeeMasters.iterator().next();

                deleteEmployeeMaster(employeeMaster);
            }
        }
    }

    @Override
    public EmployeeMaster findEmployeeMasterByCode(String code) {
        Set<EmployeeMaster> employeeMasters = employeeMasterDAO.findEmployeeMasterByCode(code);

        // get only first element in set which is returned from db because code is unique
        return !org.springframework.util.CollectionUtils.isEmpty(employeeMasters) ? employeeMasters.iterator().next() : null;
    }

    @Override
    public boolean updateEmployeeMaster(EmployeeMaster employeenmaster) {
        EmployeeMaster existingEmployeeMaster = findEmployeeMasterByCode(employeenmaster.getCode());

        if (existingEmployeeMaster != null) {
            existingEmployeeMaster.setCode(employeenmaster.getCode());
            existingEmployeeMaster.setName(employeenmaster.getName());
            existingEmployeeMaster.setRoleTitle(employeenmaster.getRoleTitle());
            existingEmployeeMaster.setTeam(employeenmaster.getTeam());
            existingEmployeeMaster.setDivisionMaster(employeenmaster.getDivisionMaster());
            existingEmployeeMaster.setJoinDate(employeenmaster.getJoinDate());
            existingEmployeeMaster.setResignDate(employeenmaster.getResignDate());
            EmployeeMaster divisionIsReturnedFromDb = employeeMasterDAO.store(existingEmployeeMaster);
            employeeMasterDAO.flush();

            return (divisionIsReturnedFromDb != null) ? true : false;
        }
        return false;
    }

    @Override
    public boolean createEmployeeMaster(EmployeeMaster divisionmaster) {
        EmployeeMaster divisionIsReturnedFromDb = employeeMasterDAO.store(divisionmaster);
        employeeMasterDAO.flush();
        return (divisionIsReturnedFromDb != null) ? true : false;
    }

    @Override
    public void deleteEmployeeMasterByCode(String code) {
        Set<EmployeeMaster> employeeMasters = employeeMasterDAO.findEmployeeMasterByCode(code);

        if (!org.springframework.util.CollectionUtils.isEmpty(employeeMasters)) {
            // get only first element in set which is returned from db because code is unique
            EmployeeMaster employeeMaster = employeeMasters.iterator().next();

            deleteEmployeeMaster(employeeMaster);
        }
    }
}
