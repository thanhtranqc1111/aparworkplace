
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.BusinessTypeMaster;

/**
 * Spring service that handles CRUD requests for BusinessTypeMaster entities
 * 
 */
public interface BusinessTypeMasterService {

	/**
	 */
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code);

	/**
	* Return all BusinessTypeMaster entity
	* 
	 */
	public List<BusinessTypeMaster> findAllBusinessTypeMasters(Integer startResult, Integer maxRows);

	/**
	* Return a count of all BusinessTypeMaster entity
	* 
	 */
	public Integer countBusinessTypeMasters();

	/**
	* Load an existing BusinessTypeMaster entity
	* 
	 */
	public Set<BusinessTypeMaster> loadBusinessTypeMasters();

	/**
	* Save an existing BusinessTypeMaster entity
	* 
	 */
	public void saveBusinessTypeMaster(BusinessTypeMaster businesstypemaster);

	/**
	* Delete an existing BusinessTypeMaster entity
	* 
	 */
	public void deleteBusinessTypeMaster(BusinessTypeMaster businesstypemaster_1);
	public void deleteBusinessTypeMasterByCodes(String codes[]);
	public String findBusinessTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    boolean checkExistingBusinessTypeCode(String businessTypeName);
}