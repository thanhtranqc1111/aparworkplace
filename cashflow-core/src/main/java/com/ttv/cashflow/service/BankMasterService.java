
package com.ttv.cashflow.service;

import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.BankMaster;

/**
 * Spring service that handles CRUD requests for BankMaster entities
 * 
 */
public interface BankMasterService {

	/**
	 * Return a count of all BankMaster entity
	 * 
	 */
	public Integer countBankMasters();

	/**
	 * Return all BankMaster entity
	 * 
	 */
	public List<BankMaster> findAllBankMasters(Integer startResult, Integer maxRows);

	/**
	 * Save an existing BankMaster entity
	 * 
	 */
	public void saveBankMaster(BankMaster bankmaster);

	/**
	 * Delete an existing BankMaster entity
	 * 
	 */
	public void deleteBankMaster(BankMaster bankmaster_1);

	/**
	 * Load an existing BankMaster entity
	 * 
	 */
	public Set<BankMaster> loadBankMasters();
	public BankMaster findBankMasterByPrimaryKey(String code);
	public String findBankMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
	public void deleteBankMasterByCodes(String[] codes);
}