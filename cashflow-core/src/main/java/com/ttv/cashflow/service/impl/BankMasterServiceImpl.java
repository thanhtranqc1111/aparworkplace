package com.ttv.cashflow.service.impl;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.BankMasterDAO;

import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.BusinessTypeMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;
import com.ttv.cashflow.service.BankMasterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for BankMaster entities
 * 
 */

@Service("BankMasterService")

@Transactional
public class BankMasterServiceImpl implements BankMasterService {

    /**
     * DAO injected by Spring that manages BankMaster entities
     * 
     */
    @Autowired
    private BankMasterDAO bankMasterDAO;

    /**
     * Instantiates a new BankMasterServiceImpl.
     *
     */
    public BankMasterServiceImpl() {
    }

    /**
     * Return a count of all BankMaster entity
     * 
     */
    @Transactional
    public Integer countBankMasters() {
        return ((Long) bankMasterDAO
                .createQuerySingleResult("select count(o) from BankMaster o")
                .getSingleResult()).intValue();
    }

    /**
     * Return all BankMaster entity
     * 
     */
    @Transactional
    public List<BankMaster> findAllBankMasters(Integer startResult,
            Integer maxRows) {
        return new java.util.ArrayList<BankMaster>(
                bankMasterDAO.findAllBankMasters(startResult, maxRows));
    }

    /**
     * Save an existing BankMaster entity
     * 
     */
    @Transactional
    public void saveBankMaster(BankMaster bankmaster) {
        BankMaster existingBankMaster = bankMasterDAO.findBankMasterByPrimaryKey(bankmaster.getCode());
        if (existingBankMaster != null) {
            if (existingBankMaster != bankmaster) {
                existingBankMaster.setCode(bankmaster.getCode());
                existingBankMaster.setName(bankmaster.getName());
            }
            bankmaster = bankMasterDAO.store(existingBankMaster);
        } else {
            bankmaster = bankMasterDAO.store(bankmaster);
        }
        bankMasterDAO.flush();
    }

    /**
     * Delete an existing BankMaster entity
     * 
     */
    @Transactional
    public void deleteBankMaster(BankMaster bankmaster) {
        bankMasterDAO.remove(bankmaster);
        bankMasterDAO.flush();
    }

    /**
     * Load an existing BankMaster entity
     * 
     */
    @Transactional
    public Set<BankMaster> loadBankMasters() {
        return bankMasterDAO.findAllBankMasters();
    }

    /**
     */
    @Transactional
    public BankMaster findBankMasterByPrimaryKey(String code) {
        return bankMasterDAO.findBankMasterByPrimaryKey(code);
    }

    public String findBankMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword) {
        Integer recordsTotal = bankMasterDAO.findAllBankMasters().size();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", bankMasterDAO.findBankMasterPaging(start, end, orderColumn,orderBy, keyword));
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", bankMasterDAO.countBankMasterFilter(start, end, orderColumn, orderBy, keyword));
        Gson gson = new Gson();
        String jBankMaster = gson.toJson(map);
        return jBankMaster;
    }

    /**
     * Delete list existing PayeePayerTypeMaster entity by codes
     * 
     */
    @Transactional
    public void deleteBankMasterByCodes(String[] codes) {
        for (String code : codes) {
            BankMaster bankMaster = bankMasterDAO.findBankMasterByCode(code);
            if (bankMaster != null) {
                deleteBankMaster(bankMaster);
            }
        }
    }
}
