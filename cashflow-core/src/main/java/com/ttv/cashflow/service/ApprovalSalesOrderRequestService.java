
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;

/**
 * Spring service that handles CRUD requests for ApprovalSalesOrderRequest entities
 * 
 */
public interface ApprovalSalesOrderRequestService {

	/**
	* Delete an existing ApprovalSalesOrderRequest entity
	* 
	 */
	public void deleteApprovalSalesOrderRequest(ApprovalSalesOrderRequest approvalsalesorderrequest);

	/**
	* Save an existing ApprovalSalesOrderRequest entity
	* 
	 */
	public void saveApprovalSalesOrderRequest(ApprovalSalesOrderRequest approvalsalesorderrequest_1);

	/**
	* Load an existing ApprovalSalesOrderRequest entity
	* 
	 */
	public Set<ApprovalSalesOrderRequest> loadApprovalSalesOrderRequests();

	/**
	* Return a count of all ApprovalSalesOrderRequest entity
	* 
	 */
	public Integer countApprovalSalesOrderRequests();

	/**
	 */
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id);

	/**
	* Return all ApprovalSalesOrderRequest entity
	* 
	 */
	public List<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests(Integer startResult, Integer maxRows);
	public void deleteApprovalByIds(BigInteger ids[]);
	public String findApprovalJson(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo, Integer orderColumn, String orderBy);
	public Set<ApprovalSalesOrderRequest> findApprovalByApprovalCode(String approvalCode);
	public int checkExists(ApprovalSalesOrderRequest approval);
	public List<ApprovalSalesOrderRequest> findApprovalAvailable(String approvalCode, String issueDate) throws DataAccessException;
    public List<ApprovalSalesOrderRequest> findApproval(String keyword);

    boolean checkExistingApprovalCode(String approvalCode);
}