
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.PayrollViewer;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for PayrollViewer entities
 * 
 */
public interface PayrollViewerService {

	/**
	* Return a count of all PayrollViewer entity
	* 
	 */
	public Integer countPayrollViewers();

	/**
	 */
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id);

	/**
	* Return all PayrollViewer entity
	* 
	 */
	public List<PayrollViewer> findAllPayrollViewers(Integer startResult, Integer maxRows);

	/**
	* Delete an existing PayrollViewer entity
	* 
	 */
	public void deletePayrollViewer(PayrollViewer payrollviewer);

	/**
	* Save an existing PayrollViewer entity
	* 
	 */
	public void savePayrollViewer(PayrollViewer payrollviewer_1);

	/**
	* Load an existing PayrollViewer entity
	* 
	 */
	public Set<PayrollViewer> loadPayrollViewers();
}