package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ArInvoiceAccountDetailsDAO;
import com.ttv.cashflow.dao.ArInvoiceClassDetailsDAO;
import com.ttv.cashflow.dao.ArInvoiceDAO;

import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;
import com.ttv.cashflow.service.ArInvoiceAccountDetailsService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ArInvoiceAccountDetails entities
 * 
 */

@Service("ArInvoiceAccountDetailsService")

@Transactional
public class ArInvoiceAccountDetailsServiceImpl implements ArInvoiceAccountDetailsService {

	/**
	 * DAO injected by Spring that manages ArInvoiceAccountDetails entities
	 * 
	 */
	@Autowired
	private ArInvoiceAccountDetailsDAO arInvoiceAccountDetailsDAO;

	/**
	 * DAO injected by Spring that manages ArInvoiceClassDetails entities
	 * 
	 */
	@Autowired
	private ArInvoiceClassDetailsDAO arInvoiceClassDetailsDAO;

	/**
	 * DAO injected by Spring that manages ArInvoice entities
	 * 
	 */
	@Autowired
	private ArInvoiceDAO arInvoiceDAO;

	/**
	 * Instantiates a new ArInvoiceAccountDetailsServiceImpl.
	 *
	 */
	public ArInvoiceAccountDetailsServiceImpl() {
	}

	/**
	 * Save an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public void saveArInvoiceAccountDetails(ArInvoiceAccountDetails arinvoiceaccountdetails) {
		ArInvoiceAccountDetails existingArInvoiceAccountDetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(arinvoiceaccountdetails.getId());

		if (existingArInvoiceAccountDetails != null) {
			if (existingArInvoiceAccountDetails != arinvoiceaccountdetails) {
				existingArInvoiceAccountDetails.setId(arinvoiceaccountdetails.getId());
				existingArInvoiceAccountDetails.setAccountCode(arinvoiceaccountdetails.getAccountCode());
				existingArInvoiceAccountDetails.setAccountName(arinvoiceaccountdetails.getAccountName());
				existingArInvoiceAccountDetails.setExcludeGstOriginalAmount(arinvoiceaccountdetails.getExcludeGstOriginalAmount());
				existingArInvoiceAccountDetails.setExcludeGstConvertedAmount(arinvoiceaccountdetails.getExcludeGstConvertedAmount());
				existingArInvoiceAccountDetails.setGstOriginalAmount(arinvoiceaccountdetails.getGstOriginalAmount());
				existingArInvoiceAccountDetails.setGstConvertedAmount(arinvoiceaccountdetails.getGstConvertedAmount());
				existingArInvoiceAccountDetails.setIncludeGstOriginalAmount(arinvoiceaccountdetails.getIncludeGstOriginalAmount());
				existingArInvoiceAccountDetails.setIncludeGstConvertedAmount(arinvoiceaccountdetails.getIncludeGstConvertedAmount());
				existingArInvoiceAccountDetails.setCreatedDate(arinvoiceaccountdetails.getCreatedDate());
				existingArInvoiceAccountDetails.setModifiedDate(arinvoiceaccountdetails.getModifiedDate());
			}
			arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(existingArInvoiceAccountDetails);
		} else {
			ArInvoiceAccountDetails ret = arInvoiceAccountDetailsDAO.store(arinvoiceaccountdetails);
			arinvoiceaccountdetails.copy(ret);
		}
		arInvoiceAccountDetailsDAO.flush();
	}

	/**
	 * Delete an existing ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public ArInvoiceAccountDetails deleteArInvoiceAccountDetailsArInvoiceClassDetailses(BigInteger arinvoiceaccountdetails_id, BigInteger related_arinvoiceclassdetailses_id) {
		ArInvoiceClassDetails related_arinvoiceclassdetailses = arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(related_arinvoiceclassdetailses_id, -1, -1);

		ArInvoiceAccountDetails arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(arinvoiceaccountdetails_id, -1, -1);

		related_arinvoiceclassdetailses.setArInvoiceAccountDetails(null);
		arinvoiceaccountdetails.getArInvoiceClassDetailses().remove(related_arinvoiceclassdetailses);

		arInvoiceClassDetailsDAO.remove(related_arinvoiceclassdetailses);
		arInvoiceClassDetailsDAO.flush();

		return arinvoiceaccountdetails;
	}

	/**
	 * Load an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> loadArInvoiceAccountDetailss() {
		return arInvoiceAccountDetailsDAO.findAllArInvoiceAccountDetailss();
	}

	/**
	 * Return a count of all ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public Integer countArInvoiceAccountDetailss() {
		return ((Long) arInvoiceAccountDetailsDAO.createQuerySingleResult("select count(o) from ArInvoiceAccountDetails o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing ArInvoiceClassDetails entity
	 * 
	 */
	@Transactional
	public ArInvoiceAccountDetails saveArInvoiceAccountDetailsArInvoiceClassDetailses(BigInteger id, ArInvoiceClassDetails related_arinvoiceclassdetailses) {
		ArInvoiceAccountDetails arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
		ArInvoiceClassDetails existingarInvoiceClassDetailses = arInvoiceClassDetailsDAO.findArInvoiceClassDetailsByPrimaryKey(related_arinvoiceclassdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingarInvoiceClassDetailses != null) {
			existingarInvoiceClassDetailses.setId(related_arinvoiceclassdetailses.getId());
			existingarInvoiceClassDetailses.setClassCode(related_arinvoiceclassdetailses.getClassCode());
			existingarInvoiceClassDetailses.setClassName(related_arinvoiceclassdetailses.getClassName());
			existingarInvoiceClassDetailses.setApprovalCode(related_arinvoiceclassdetailses.getApprovalCode());
			existingarInvoiceClassDetailses.setDescription(related_arinvoiceclassdetailses.getDescription());
			existingarInvoiceClassDetailses.setExcludeGstOriginalAmount(related_arinvoiceclassdetailses.getExcludeGstOriginalAmount());
			existingarInvoiceClassDetailses.setExcludeGstConvertedAmount(related_arinvoiceclassdetailses.getExcludeGstConvertedAmount());
			existingarInvoiceClassDetailses.setFxRate(related_arinvoiceclassdetailses.getFxRate());
			existingarInvoiceClassDetailses.setGstType(related_arinvoiceclassdetailses.getGstType());
			existingarInvoiceClassDetailses.setGstRate(related_arinvoiceclassdetailses.getGstRate());
			existingarInvoiceClassDetailses.setGstOriginalAmount(related_arinvoiceclassdetailses.getGstOriginalAmount());
			existingarInvoiceClassDetailses.setGstConvertedAmount(related_arinvoiceclassdetailses.getGstConvertedAmount());
			existingarInvoiceClassDetailses.setIncludeGstOriginalAmount(related_arinvoiceclassdetailses.getIncludeGstOriginalAmount());
			existingarInvoiceClassDetailses.setIncludeGstConvertedAmount(related_arinvoiceclassdetailses.getIncludeGstConvertedAmount());
			existingarInvoiceClassDetailses.setIsApproval(related_arinvoiceclassdetailses.getIsApproval());
			existingarInvoiceClassDetailses.setCreatedDate(related_arinvoiceclassdetailses.getCreatedDate());
			existingarInvoiceClassDetailses.setModifiedDate(related_arinvoiceclassdetailses.getModifiedDate());
			related_arinvoiceclassdetailses = existingarInvoiceClassDetailses;
		} else {
			related_arinvoiceclassdetailses = arInvoiceClassDetailsDAO.store(related_arinvoiceclassdetailses);
			arInvoiceClassDetailsDAO.flush();
		}

		related_arinvoiceclassdetailses.setArInvoiceAccountDetails(arinvoiceaccountdetails);
		arinvoiceaccountdetails.getArInvoiceClassDetailses().add(related_arinvoiceclassdetailses);
		related_arinvoiceclassdetailses = arInvoiceClassDetailsDAO.store(related_arinvoiceclassdetailses);
		arInvoiceClassDetailsDAO.flush();

		arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		return arinvoiceaccountdetails;
	}

	/**
	 * Save an existing ArInvoice entity
	 * 
	 */
	@Transactional
	public ArInvoiceAccountDetails saveArInvoiceAccountDetailsArInvoice(BigInteger id, ArInvoice related_arinvoice) {
		ArInvoiceAccountDetails arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
		ArInvoice existingarInvoice = arInvoiceDAO.findArInvoiceByPrimaryKey(related_arinvoice.getId());

		// copy into the existing record to preserve existing relationships
		if (existingarInvoice != null) {
			existingarInvoice.setId(related_arinvoice.getId());
			existingarInvoice.setPayerName(related_arinvoice.getPayerName());
			existingarInvoice.setArInvoiceNo(related_arinvoice.getArInvoiceNo());
			existingarInvoice.setOriginalCurrencyCode(related_arinvoice.getOriginalCurrencyCode());
			existingarInvoice.setFxRate(related_arinvoice.getFxRate());
			existingarInvoice.setGstRate(related_arinvoice.getGstRate());
			existingarInvoice.setInvoiceIssuedDate(related_arinvoice.getInvoiceIssuedDate());
			existingarInvoice.setMonth(related_arinvoice.getMonth());
			existingarInvoice.setClaimType(related_arinvoice.getClaimType());
			existingarInvoice.setGstType(related_arinvoice.getGstType());
			existingarInvoice.setApprovalCode(related_arinvoice.getApprovalCode());
			existingarInvoice.setAccountReceivableCode(related_arinvoice.getAccountReceivableCode());
			existingarInvoice.setAccountReceivableName(related_arinvoice.getAccountReceivableName());
			existingarInvoice.setDescription(related_arinvoice.getDescription());
			existingarInvoice.setExcludeGstOriginalAmount(related_arinvoice.getExcludeGstOriginalAmount());
			existingarInvoice.setExcludeGstConvertedAmount(related_arinvoice.getExcludeGstConvertedAmount());
			existingarInvoice.setGstOriginalAmount(related_arinvoice.getGstOriginalAmount());
			existingarInvoice.setGstConvertedAmount(related_arinvoice.getGstConvertedAmount());
			existingarInvoice.setIncludeGstOriginalAmount(related_arinvoice.getIncludeGstOriginalAmount());
			existingarInvoice.setIncludeGstConvertedAmount(related_arinvoice.getIncludeGstConvertedAmount());
			existingarInvoice.setCreatedDate(related_arinvoice.getCreatedDate());
			existingarInvoice.setModifiedDate(related_arinvoice.getModifiedDate());
			related_arinvoice = existingarInvoice;
		} else {
			related_arinvoice = arInvoiceDAO.store(related_arinvoice);
			arInvoiceDAO.flush();
		}

		arinvoiceaccountdetails.setArInvoice(related_arinvoice);
		related_arinvoice.getArInvoiceAccountDetailses().add(arinvoiceaccountdetails);
		arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		related_arinvoice = arInvoiceDAO.store(related_arinvoice);
		arInvoiceDAO.flush();

		return arinvoiceaccountdetails;
	}

	/**
	 * Delete an existing ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public void deleteArInvoiceAccountDetails(ArInvoiceAccountDetails arinvoiceaccountdetails) {
		arInvoiceAccountDetailsDAO.remove(arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();
	}

	/**
	 */
	@Transactional
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id) {
		return arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(id);
	}

	/**
	 * Delete an existing ArInvoice entity
	 * 
	 */
	@Transactional
	public ArInvoiceAccountDetails deleteArInvoiceAccountDetailsArInvoice(BigInteger arinvoiceaccountdetails_id, BigInteger related_arinvoice_id) {
		ArInvoiceAccountDetails arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.findArInvoiceAccountDetailsByPrimaryKey(arinvoiceaccountdetails_id, -1, -1);
		ArInvoice related_arinvoice = arInvoiceDAO.findArInvoiceByPrimaryKey(related_arinvoice_id, -1, -1);

		arinvoiceaccountdetails.setArInvoice(null);
		related_arinvoice.getArInvoiceAccountDetailses().remove(arinvoiceaccountdetails);
		arinvoiceaccountdetails = arInvoiceAccountDetailsDAO.store(arinvoiceaccountdetails);
		arInvoiceAccountDetailsDAO.flush();

		related_arinvoice = arInvoiceDAO.store(related_arinvoice);
		arInvoiceDAO.flush();

		arInvoiceDAO.remove(related_arinvoice);
		arInvoiceDAO.flush();

		return arinvoiceaccountdetails;
	}

	/**
	 * Return all ArInvoiceAccountDetails entity
	 * 
	 */
	@Transactional
	public List<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArInvoiceAccountDetails>(arInvoiceAccountDetailsDAO.findAllArInvoiceAccountDetailss(startResult, maxRows));
	}
	
	@Transactional
	public BigInteger getLastId(){
	    return arInvoiceAccountDetailsDAO.getLastId();
	}
}
