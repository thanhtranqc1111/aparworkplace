package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.SystemValue;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.dto.PaymentPayroll;
import com.ttv.cashflow.service.PaymentOrderService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for PaymentOrder entities
 * 
 */

@Service("PaymentOrderService")

@Transactional
public class PaymentOrderServiceImpl implements PaymentOrderService {
	
	SimpleDateFormat sdf = new SimpleDateFormat(Constant.DDMMYYYY, Locale.ENGLISH);
	/**
	 * DAO injected by Spring that manages ApInvoicePaymentDetails entities
	 * 
	 */
	@Autowired
	private ApInvoicePaymentDetailsDAO apInvoicePaymentDetailsDAO;

	/**
	 * DAO injected by Spring that manages PaymentOrder entities
	 * 
	 */
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;
	
	@Autowired
	private BankStatementDAO bankStatementDAO;
	
	@Autowired
    private SystemValueDAO systemValueDAO;

	/**
	 * Instantiates a new PaymentOrderServiceImpl.
	 *
	 */
	public PaymentOrderServiceImpl() {
	}

	/**
	 * Load an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public Set<PaymentOrder> loadPaymentOrders() {
		return paymentOrderDAO.findAllPaymentOrders();
	}

	/**
	 * Save an existing PaymentOrder entity
	 * 
	 */
    @Transactional
    public PaymentOrder savePaymentOrder(PaymentOrder paymentorder) {
        PaymentOrder existingPaymentOrder = paymentOrderDAO.findPaymentOrderByPrimaryKey(paymentorder.getId());
        PaymentOrder resultPaymentOrder = null;

        if (existingPaymentOrder != null) {
            existingPaymentOrder.setPaymentOrderNo(paymentorder.getPaymentOrderNo());
            existingPaymentOrder.setBankName(paymentorder.getBankName());
            existingPaymentOrder.setBankAccount(paymentorder.getBankAccount());
            existingPaymentOrder.setOriginalCurrencyCode(paymentorder.getOriginalCurrencyCode());
            existingPaymentOrder.setPaymentRate(paymentorder.getPaymentRate());
            existingPaymentOrder.setIncludeGstOriginalAmount(paymentorder.getIncludeGstOriginalAmount());
            existingPaymentOrder.setIncludeGstConvertedAmount(paymentorder.getIncludeGstConvertedAmount());
            existingPaymentOrder.setValueDate(paymentorder.getValueDate());
            existingPaymentOrder.setBankRefNo(paymentorder.getBankRefNo());
            existingPaymentOrder.setDescription(paymentorder.getDescription());
            existingPaymentOrder.setPaymentApprovalNo(paymentorder.getPaymentApprovalNo());
            existingPaymentOrder.setAccountPayableCode(paymentorder.getAccountPayableCode());
            existingPaymentOrder.setAccountPayableName(paymentorder.getAccountPayableName());
            existingPaymentOrder.setPaymentType(paymentorder.getPaymentType());
            existingPaymentOrder.setModifiedDate(paymentorder.getModifiedDate());

            if (StringUtils.isNotEmpty(paymentorder.getStatus())) {
                existingPaymentOrder.setStatus(paymentorder.getStatus());
            }

            resultPaymentOrder = paymentOrderDAO.store(existingPaymentOrder);
        } else {
            resultPaymentOrder = paymentOrderDAO.store(paymentorder);
        }

        paymentOrderDAO.flush();
        return resultPaymentOrder;
    }
	
	/**
	 * Save an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public PaymentOrder savePaymentOrderApInvoicePaymentDetailses(BigInteger id, ApInvoicePaymentDetails related_apinvoicepaymentdetailses) {
		PaymentOrder paymentorder = paymentOrderDAO.findPaymentOrderByPrimaryKey(id, -1, -1);
		ApInvoicePaymentDetails existingapInvoicePaymentDetailses = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(related_apinvoicepaymentdetailses.getId());

		// copy into the existing record to preserve existing relationships
		if (existingapInvoicePaymentDetailses != null) {
			existingapInvoicePaymentDetailses.setId(related_apinvoicepaymentdetailses.getId());
			existingapInvoicePaymentDetailses.setPaymentIncludeGstConvertedAmount(related_apinvoicepaymentdetailses.getPaymentIncludeGstConvertedAmount());
			existingapInvoicePaymentDetailses.setPaymentIncludeGstOriginalAmount(related_apinvoicepaymentdetailses.getPaymentIncludeGstOriginalAmount());
			existingapInvoicePaymentDetailses.setPaymentRate(related_apinvoicepaymentdetailses.getPaymentRate());
			existingapInvoicePaymentDetailses.setPayeeBankName(related_apinvoicepaymentdetailses.getPayeeBankName());
			existingapInvoicePaymentDetailses.setPayeeBankAccNo(related_apinvoicepaymentdetailses.getPayeeBankAccNo());
			existingapInvoicePaymentDetailses.setVarianceAmount(related_apinvoicepaymentdetailses.getVarianceAmount());
			existingapInvoicePaymentDetailses.setVarianceAccountCode(related_apinvoicepaymentdetailses.getVarianceAccountCode());
			existingapInvoicePaymentDetailses.setCreatedDate(related_apinvoicepaymentdetailses.getCreatedDate());
			existingapInvoicePaymentDetailses.setModifiedDate(related_apinvoicepaymentdetailses.getModifiedDate());
			related_apinvoicepaymentdetailses = existingapInvoicePaymentDetailses;
		} else {
			related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.store(related_apinvoicepaymentdetailses);
			apInvoicePaymentDetailsDAO.flush();
		}

		related_apinvoicepaymentdetailses.setPaymentOrder(paymentorder);
		paymentorder.getApInvoicePaymentDetailses().add(related_apinvoicepaymentdetailses);
		related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.store(related_apinvoicepaymentdetailses);
		apInvoicePaymentDetailsDAO.flush();

		paymentorder = paymentOrderDAO.store(paymentorder);
		paymentOrderDAO.flush();

		return paymentorder;
	}

	/**
	 * Delete an existing ApInvoicePaymentDetails entity
	 * 
	 */
	@Transactional
	public PaymentOrder deletePaymentOrderApInvoicePaymentDetailses(BigInteger paymentorder_id, BigInteger related_apinvoicepaymentdetailses_id) {
		ApInvoicePaymentDetails related_apinvoicepaymentdetailses = apInvoicePaymentDetailsDAO.findApInvoicePaymentDetailsByPrimaryKey(related_apinvoicepaymentdetailses_id, -1, -1);

		PaymentOrder paymentorder = paymentOrderDAO.findPaymentOrderByPrimaryKey(paymentorder_id, -1, -1);

		related_apinvoicepaymentdetailses.setPaymentOrder(null);
		paymentorder.getApInvoicePaymentDetailses().remove(related_apinvoicepaymentdetailses);

		apInvoicePaymentDetailsDAO.remove(related_apinvoicepaymentdetailses);
		apInvoicePaymentDetailsDAO.flush();

		return paymentorder;
	}

	/**
	 */
	@Transactional
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id) {
		return paymentOrderDAO.findPaymentOrderByPrimaryKey(id);
	}

	/**
	 * Return all PaymentOrder entity
	 * 
	 */
	@Transactional
	public List<PaymentOrder> findAllPaymentOrders(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PaymentOrder>(paymentOrderDAO.findAllPaymentOrders(startResult, maxRows));
	}

	/**
	 * Return a count of all PaymentOrder entity
	 * 
	 */
	@Transactional
	public Integer countPaymentOrders() {
		return ((Long) paymentOrderDAO.createQuerySingleResult("select count(o) from PaymentOrder o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing PaymentOrder entity
	 * 
	 */
	@Transactional
	public void deletePaymentOrder(PaymentOrder paymentorder) {
		paymentOrderDAO.remove(paymentorder);
		paymentOrderDAO.flush();
	}
	
	public List<PaymentInvoice> searchPayment(Integer startResult, Integer maxRow, String fromDate, String toDate,
            String bankRef, String invoiceNo, String status) {
        List<PaymentInvoice> lstPayment = paymentOrderDAO.searchPayment(startResult, maxRow, fromDate, toDate, bankRef, invoiceNo, status, -1, null);
        return lstPayment;
    }
	
	public BigInteger searchPaymentCount(String fromDate, String toDate, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status){
		return paymentOrderDAO.searchPaymentCount(fromDate, toDate, bankRef, paymentOrderNoFrom, paymentOrderNoTo, status);
	}
	
	public BigInteger searchPaymentPayrollCount(String fromDate, String toDate, String bankRef, String fromPayrollNo, String toPayrollNo, String status){
		return paymentOrderDAO.searchPaymentPayrollCount(fromDate, toDate, bankRef, fromPayrollNo, toPayrollNo, status);
	}
	
	@Transactional
    public Set<ApInvoice> searchApInvoice(Object... parameters) {
	    return paymentOrderDAO.searchApInvoice(parameters);
    }
	
	@Transactional
    public Set<ApInvoice> searchApInvoiceDetails(Object... parameters) {
	    return paymentOrderDAO.searchApInvoiceDetails(parameters);
    }

	public String findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo, String status) throws DataAccessException {
		// TODO Auto-generated method stubv
		Map<String, Object> map = new HashMap<String, Object>();
		Set<PaymentOrder> setOrg = paymentOrderDAO.findPaymentOrderByPaymentOrderNoContainingAndStatus(paymentOrderNo,status,-1,-1);
		//must create DTO because of serialize of HIBERNATE proxy relational mapping error
		Set<PaymentOrderDTO> setDTO = new LinkedHashSet<PaymentOrderDTO>();
		Iterator<PaymentOrder> iterator = setOrg.iterator();
		while(iterator.hasNext()) {
			PaymentOrder paymentOrder = iterator.next();
			PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
			BeanUtils.copyProperties(paymentOrder, paymentOrderDTO);
			paymentOrderDTO.setPaymentTotalAmount(paymentOrder.getIncludeGstConvertedAmount());
			setDTO.add(paymentOrderDTO);
		}
		map.put("data", setDTO);
        Gson gson = new Gson();
        return  gson.toJson(map);
	}

	@Override
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType)
			throws DataAccessException {
		return paymentOrderDAO.findPaymentOrderByPaymentOrderNo(paymentOrderNo, paymentType, -1, -1);
	}
	
    public boolean checkDuplicatePaymentOrder(String bankName, String bankAccount, BigDecimal convertedAmount, Calendar valueDate) {
        return paymentOrderDAO.checkDuplicatePaymentOrder(bankName, bankAccount, convertedAmount, valueDate);
	}
    
    //String bankName, String bankAccount, BigDecimal convertedAmount, Calendar valueDate
    public PaymentOrder checkDuplicateByConvertedAmountAndValueDate(String bankName, String bankAccount, BigDecimal convertedAmount, Calendar valueDate) {
        return paymentOrderDAO.checkDuplicateByConvertedAmountAndValueDate(bankName, bankAccount, convertedAmount, valueDate);
	}
	
	public String findPaymentOrderJSON(Integer startpage, Integer endPage, String fromDate, String toDate,
			String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status, Integer orderColumn, String orderBy){
		
		// list payment order

		List<PaymentInvoice> listPaymentOrder = paymentOrderDAO.searchPaymentOrder(startpage, endPage, fromDate, toDate, bankRef, paymentOrderNoFrom, paymentOrderNoTo, status, orderColumn, orderBy);
		
		//binh.lv
		List<String> listPaymentNoApInvoice = new ArrayList<String>();
		List<String> listPaymentNoPayroll = new ArrayList<String>();
		List<String> listReimbursement = new ArrayList<String>();
		for (PaymentInvoice data : listPaymentOrder) {
			String[] paymentType = data.getPaymentType().split(Constant.STRING_COMMA);
			if(paymentType == null) continue;
			for (String type : paymentType) {
				switch(type) {
				case Constant.PAYMENT_ORDER_APINVOICE:
					listPaymentNoApInvoice.add(data.getPaymentOrderNo());
					break;
				case Constant.PAYMENT_ORDER_PAYROLL:
					listPaymentNoPayroll.add(data.getPaymentOrderNo());
					break;
				case Constant.PAYMENT_ORDER_REIMBURSEMENT:
					listReimbursement.add(data.getPaymentOrderNo());
					break;
				default: break;
				}
			}
		
		}
		//
		List<PaymentInvoice> listPayment = new ArrayList<PaymentInvoice>();
		listPayment = searchPaymentOrderDetail(listPaymentNoApInvoice, listPaymentNoPayroll, listReimbursement, orderColumn, orderBy);
		
		
		Map<String, String> invStatus = toMap(Constant.PREFIX_API);
		Map<String, String> payStatus = toMap(Constant.PREFIX_PAY);
		
		for (PaymentInvoice invoicePayment : listPayment) {

			List<BankStatement> bankStatements = new ArrayList<BankStatement>(bankStatementDAO.findBankStatementByPaymentOrderNo(invoicePayment.getPaymentOrderNo()));
			invoicePayment.setBankStatement(bankStatements);
			//thoai.nh status to render class css
			invoicePayment.setPaymentStatusCode(invoicePayment.getPaymentStatus());
			invoicePayment.setInvoiceStatusCode(invoicePayment.getInvoiceStatus());
			//set status (code -> value)
			invoicePayment.setPaymentStatus(payStatus.get(invoicePayment.getPaymentStatus()));
			invoicePayment.setInvoiceStatus(invStatus.get(invoicePayment.getInvoiceStatus()));
		}
		
		// ar invoice count
		BigInteger recordsFiltered = BigInteger.ZERO;
		recordsFiltered = searchPaymentCount(fromDate, toDate, bankRef, paymentOrderNoFrom, paymentOrderNoTo, status);
        
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listPayment);
        map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
		
		return jsonData;
	}
	
	public String findPaymentOrderPayrollJSON(Integer startpage, Integer endPage, String fromDate, String toDate,
			String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn, String orderBy){
		
		// list payment order
		List<String> listPaymentNo = searchPayementOrderPayroll(startpage, endPage, fromDate, toDate, bankRef, fromPayrollNo, toPayrollNo, status, orderColumn, orderBy);
		
		//
		List<PaymentPayroll> listPaymentPayroll = new ArrayList<PaymentPayroll>();
		listPaymentPayroll = searchPaymentOrderPayrollDetail(listPaymentNo, orderColumn, orderBy);
		
		Map<String, String> payStatus = toMap(Constant.PREFIX_PAY);
		Map<String, String> prStatus = toMap(Constant.PREFIX_PR);
		
		for (PaymentPayroll paymentPayroll : listPaymentPayroll) {

			List<BankStatement> bankStatements = new ArrayList<BankStatement>(bankStatementDAO.findBankStatementByPaymentOrderNo(paymentPayroll.getPaymentOrderNo()));
			paymentPayroll.setBankStatement(bankStatements);
			//thoai.nh status to render class css
			paymentPayroll.setPaymentStatusCode(paymentPayroll.getPaymentStatus());
			paymentPayroll.setPayrollStatusCode(paymentPayroll.getPayrollStatus());
			//set status (code -> value)
			paymentPayroll.setPaymentStatus(payStatus.get(paymentPayroll.getPaymentStatus()));
			paymentPayroll.setPayrollStatus(prStatus.get(paymentPayroll.getPayrollStatus()));
		}
		
		// ar invoice count
		BigInteger recordsFiltered = BigInteger.ZERO;
		recordsFiltered = searchPaymentPayrollCount(fromDate, toDate, bankRef, fromPayrollNo, toPayrollNo, status);
        
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", listPaymentPayroll);
        map.put("recordsFiltered", recordsFiltered);
        Gson gson = new Gson();
        String jsonData = gson.toJson(map);
		
		return jsonData;
	}

	
	public List<PaymentInvoice> searchPaymentOrderDetail(List<String> listPaymentNoApInvoice, List<String> listPaymentNoPayroll, List<String> listReimbursement, Integer orderColumn, String orderBy){
		List<PaymentInvoice> lstApInvoiceReceipt = paymentOrderDAO.searchPaymentDetail(listPaymentNoApInvoice, listPaymentNoPayroll, listReimbursement, orderColumn, orderBy);
        return lstApInvoiceReceipt;
	}
	
	public List<PaymentPayroll> searchPaymentOrderPayrollDetail(List<String> paymentOrderNoLst, Integer orderColumn, String orderBy){
		List<PaymentPayroll> lstPayment = paymentOrderDAO.searchPaymentDetailPayroll(paymentOrderNoLst, orderColumn, orderBy);
        return lstPayment;
	}
	
	private Set<SystemValue> getStatus(String type) {
        return systemValueDAO.findSystemValueByType(type);
    }
    
    private Map<String, String> toMap(String type) {
        Map<String, String> map = new LinkedHashMap<String, String>();

        Iterator<SystemValue> iter = getStatus(type).iterator();
        while (iter.hasNext()) {
            SystemValue master = iter.next();
            map.put(master.getCode(), master.getValue());
        }

        return map;
    }

	@Override
	public List<PaymentOrderDTO> getPaymentOrderNoBasedOnBankRef(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter) {
		return paymentOrderDAO.getPaymentOrderNoBasedOnBankRef(bankName, bankAccNo, transactionFrom, transactionTo, transDateFilter);
	}

	@Override
	public PaymentOrder findPaymentOrderByPaymentOrderNoOnly(String paymentOrderNo) {
		return paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(paymentOrderNo);
	}

    @Override
    public String getPaymentOrderNo(Calendar valueDate) {
        return paymentOrderDAO.getPaymentOrderNo(valueDate);
    }

	@Override
	public List<String> searchPayementOrderPayroll(Integer startResult, Integer maxRow, String fromDate, String toDate,
			String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn,
			String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}
}
