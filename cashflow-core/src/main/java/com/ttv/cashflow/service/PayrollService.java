
package com.ttv.cashflow.service;

import com.google.gson.JsonObject;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollDetails;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.dto.PayrollDTO;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

/**
 * Spring service that handles CRUD requests for Payroll entities
 * 
 */
public interface PayrollService {

	/**
	* Load an existing Payroll entity
	* 
	 */
	public Set<Payroll> loadPayrolls();

	/**
	* Delete an existing PayrollDetails entity
	* 
	 */
	public Payroll deletePayrollPayrollDetailses(BigInteger payroll_id, BigInteger related_payrolldetailses_id);

	/**
	* Return all Payroll entity
	* 
	 */
	public List<Payroll> findAllPayrolls(Integer startResult, Integer maxRows);

	/**
	* Return a count of all Payroll entity
	* 
	 */
	public Integer countPayrolls();

	/**
	* Save an existing PayrollPaymentDetails entity
	* 
	 */
	public Payroll savePayrollPayrollPaymentDetailses(BigInteger id, PayrollPaymentDetails related_payrollpaymentdetailses);

	/**
	* Save an existing Payroll entity
	* 
	 */
	public Payroll savePayroll(Payroll payroll);

	/**
	* Delete an existing Payroll entity
	* 
	 */
	public void deletePayroll(Payroll payroll_1);

	/**
	 */
	public Payroll findPayrollByPrimaryKey(BigInteger id_1);

	/**
	* Save an existing PayrollDetails entity
	* 
	 */
	public PayrollDetails savePayrollPayrollDetailses(Payroll payroll, PayrollDetails payrolldetail);

	/**
	* Delete an existing PayrollPaymentDetails entity
	* 
	 */
	public Payroll deletePayrollPayrollPaymentDetailses(BigInteger payroll_id_1, BigInteger related_payrollpaymentdetailses_id);
	
	/**
	 * Search payroll
	 * @param parameters
	 * @return
	 */
	public Set<Payroll> searchPayroll(Object... parameters);
	public PayrollDTO parsePayrollJSON(JsonObject jsonObject);
	public BigInteger savePayroll(PayrollDTO payrollDTO, boolean isSaveFromImport);
	public String getPayrollNo();
	public String getPayrollNo(Calendar cal);
	public Payroll findPayrollByPayrollNo(String payrollNo);
	public void copyDomainToDTO(Payroll payroll, PayrollDTO payrollDTO);
	public String findPayrollPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth,
		     							String fromPayrollNo, String toPayrollNo, Integer orderColumn, String orderBy, int status);
	public PayrollDTO clone(Payroll payrollOriginal);
	public Set<Payroll> findPayrollByImportKey(String importKey) throws DataAccessException;
	public int deletePayrollByImportKey(String importKey);
	
	/**
	 * search payroll details
	 * @param parameters
	 * @return
	 */
	public Set<Payroll> searchPayrollDetails(Object... parameters);
}