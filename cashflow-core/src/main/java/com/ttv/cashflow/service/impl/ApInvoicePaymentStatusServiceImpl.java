package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;
import com.ttv.cashflow.service.ApInvoicePaymentStatusService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ApInvoicePaymentStatus entities
 * 
 */

@Service("ApInvoicePaymentStatusService")

@Transactional
public class ApInvoicePaymentStatusServiceImpl implements ApInvoicePaymentStatusService {

	/**
	 * DAO injected by Spring that manages ApInvoice entities
	 * 
	 */
	@Autowired
	private ApInvoiceDAO apInvoiceDAO;

	/**
	 * DAO injected by Spring that manages ApInvoicePaymentStatus entities
	 * 
	 */
	@Autowired
	private ApInvoicePaymentStatusDAO apInvoicePaymentStatusDAO;

	/**
	 * Instantiates a new ApInvoicePaymentStatusServiceImpl.
	 *
	 */
	public ApInvoicePaymentStatusServiceImpl() {
	}

	/**
	 */
	@Transactional
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id) {
		return apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByPrimaryKey(id);
	}

	/**
	 * Load an existing ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public Set<ApInvoicePaymentStatus> loadApInvoicePaymentStatuss() {
		return apInvoicePaymentStatusDAO.findAllApInvoicePaymentStatuss();
	}

	/**
	 * Delete an existing ApInvoice entity
	 * 
	 */
	@Transactional
	public ApInvoicePaymentStatus deleteApInvoicePaymentStatusApInvoice(BigInteger apinvoicepaymentstatus_id, BigInteger related_apinvoice_id) {
		ApInvoicePaymentStatus apinvoicepaymentstatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByPrimaryKey(apinvoicepaymentstatus_id, -1, -1);
		ApInvoice related_apinvoice = apInvoiceDAO.findApInvoiceByPrimaryKey(related_apinvoice_id, -1, -1);

		apinvoicepaymentstatus = apInvoicePaymentStatusDAO.store(apinvoicepaymentstatus);
		apInvoicePaymentStatusDAO.flush();

		related_apinvoice = apInvoiceDAO.store(related_apinvoice);
		apInvoiceDAO.flush();

		apInvoiceDAO.remove(related_apinvoice);
		apInvoiceDAO.flush();

		return apinvoicepaymentstatus;
	}

	/**
	 * Save an existing ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public void saveApInvoicePaymentStatus(ApInvoicePaymentStatus apinvoicepaymentstatus) {
		ApInvoicePaymentStatus existingApInvoicePaymentStatus = apInvoicePaymentStatusDAO.findApInvoicePaymentStatusByPrimaryKey(apinvoicepaymentstatus.getId());

		if (existingApInvoicePaymentStatus != null) {
			if (existingApInvoicePaymentStatus != apinvoicepaymentstatus) {
				existingApInvoicePaymentStatus.setId(apinvoicepaymentstatus.getId());
				existingApInvoicePaymentStatus.setPaidIncludeGstOriginalAmount(apinvoicepaymentstatus.getPaidIncludeGstOriginalAmount());
				existingApInvoicePaymentStatus.setUnpaidIncludeGstOriginalAmount(apinvoicepaymentstatus.getUnpaidIncludeGstOriginalAmount());
			}
			apinvoicepaymentstatus = apInvoicePaymentStatusDAO.store(existingApInvoicePaymentStatus);
		} else {
			apinvoicepaymentstatus = apInvoicePaymentStatusDAO.store(apinvoicepaymentstatus);
		}
		apInvoicePaymentStatusDAO.flush();
	}

	/**
	 * Delete an existing ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public void deleteApInvoicePaymentStatus(ApInvoicePaymentStatus apinvoicepaymentstatus) {
		apInvoicePaymentStatusDAO.remove(apinvoicepaymentstatus);
		apInvoicePaymentStatusDAO.flush();
	}

	/**
	 * Return a count of all ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public Integer countApInvoicePaymentStatuss() {
		return ((Long) apInvoicePaymentStatusDAO.createQuerySingleResult("select count(o) from ApInvoicePaymentStatus o").getSingleResult()).intValue();
	}

	/**
	 * Return all ApInvoicePaymentStatus entity
	 * 
	 */
	@Transactional
	public List<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApInvoicePaymentStatus>(apInvoicePaymentStatusDAO.findAllApInvoicePaymentStatuss(startResult, maxRows));
	}
	
}
