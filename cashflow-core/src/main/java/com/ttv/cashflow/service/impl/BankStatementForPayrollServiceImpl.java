package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dao.PayrollPaymentDetailsDAO;
import com.ttv.cashflow.dao.PayrollPaymentStatusDAO;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.domain.PayrollPaymentStatus;
import com.ttv.cashflow.service.BankStatementUtilService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for BankStatement entities
 * 
 */

@Service("BankStatementForPayrollServiceImpl")
@Transactional
public class BankStatementForPayrollServiceImpl implements BankStatementUtilService {
	@Autowired
	private PayrollPaymentStatusDAO payrollPaymentStatusDAO;
	
	@Autowired
	private PayrollDAO payrollDAO;
	
	@Autowired
	private PayrollPaymentDetailsDAO payrollPaymentDetailsDAO;
	
	@Override
	public int updatePaymentStatus(PaymentOrder paymentOrder) {
		try {
			Set<PayrollPaymentDetails> setPayrollPaymentDetailss = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPaymentId(paymentOrder.getId());
			setPayrollPaymentDetailss.forEach(payrollPaymentDetails -> {
				//update payroll status
				Payroll payroll = payrollPaymentDetails.getPayroll();
				BigDecimal totalAmount = payroll.getTotalNetPaymentOriginal();
				BigDecimal totalPaid = payrollDAO.getTotalNetPaymentAmountByPayrollId(payroll.getId());
				if(totalPaid.doubleValue() > 0) {
					if(totalPaid.compareTo(totalAmount) >= 0)
					{
						payroll.setStatus(Constant.API_PAID);//PAID
					}
					else 
					{
						payroll.setStatus(Constant.API_PARTIAL_PAID);//PARTIAL PAID
					}
					payrollDAO.store(payroll);
					
					//update payroll invoice paid status
					PayrollPaymentStatus payrollPaymentStatus = null;
					Set<PayrollPaymentStatus> setPayrollPaymentStatus = payrollPaymentStatusDAO.findPayrollPaymentStatusByPayrollId(payroll.getId());
					if(!setPayrollPaymentStatus.isEmpty()) {
						Iterator<PayrollPaymentStatus> iterator = setPayrollPaymentStatus.iterator();
						payrollPaymentStatus = iterator.next();
					}
					else {
						payrollPaymentStatus = new PayrollPaymentStatus();
						payrollPaymentStatus.setPayrollId(payroll.getId());
					}
					payrollPaymentStatus.setPaidNetPayment(totalPaid);
					payrollPaymentStatus.setUnpaidNetPayment(totalAmount.subtract(totalPaid));
					payrollPaymentStatusDAO.store(payrollPaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int resetPaymentOrderStatus(PaymentOrder paymentOrder) {
		try {
			Set<PayrollPaymentDetails> setPayrollPaymentDetails = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPaymentId(paymentOrder.getId());
			setPayrollPaymentDetails.forEach(payrollPaymentDetails -> {
				//reset Payroll status
				Payroll payroll = payrollPaymentDetails.getPayroll();
				BigDecimal totalAmount = payroll.getTotalNetPaymentOriginal();
				BigDecimal totalPaid = payrollDAO.getTotalNetPaymentAmountByPayrollId(payroll.getId());
				if(totalPaid.compareTo(totalAmount) >= 0)
				{
					payroll.setStatus(Constant.API_PAID);
				}
				else 
				{
					if(totalPaid.compareTo(BigDecimal.ZERO) > 0){
						payroll.setStatus(Constant.API_PARTIAL_PAID);
					}
					else {
						payroll.setStatus(Constant.API_NOT_PAID);
					}
				}
				payrollDAO.store(payroll);
				
				/*
				 * reset Payroll paid status
				 */
				PayrollPaymentStatus payrollPaymentStatus = null;
				Set<PayrollPaymentStatus> setPayrollPaymentStatus = payrollPaymentStatusDAO.findPayrollPaymentStatusByPayrollId(payroll.getId());
				if(!setPayrollPaymentStatus.isEmpty()) {
					Iterator<PayrollPaymentStatus> iterator = setPayrollPaymentStatus.iterator();
					payrollPaymentStatus = iterator.next();
				}
				if(payrollPaymentStatus != null && payrollPaymentStatus.getId() != null) {
					payrollPaymentStatus.setPaidNetPayment(totalPaid);
					payrollPaymentStatus.setUnpaidNetPayment(totalAmount.subtract(totalPaid));
					payrollPaymentStatusDAO.store(payrollPaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
