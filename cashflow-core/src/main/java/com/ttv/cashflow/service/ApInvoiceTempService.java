
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ApInvoiceTemp;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ApInvoiceTemp entities
 * 
 */
public interface ApInvoiceTempService {

	/**
	* Save an existing ApInvoiceTemp entity
	* 
	 */
	public void saveApInvoiceTemp(ApInvoiceTemp apinvoicetemp);

	/**
	* Return all ApInvoiceTemp entity
	* 
	 */
	public List<ApInvoiceTemp> findAllApInvoiceTemps(Integer startResult, Integer maxRows);

	/**
	 */
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id);

	/**
	* Return a count of all ApInvoiceTemp entity
	* 
	 */
	public Integer countApInvoiceTemps();

	/**
	* Delete an existing ApInvoiceTemp entity
	* 
	 */
	public void deleteApInvoiceTemp(ApInvoiceTemp apinvoicetemp_1);

	/**
	* Load an existing ApInvoiceTemp entity
	* 
	 */
	public Set<ApInvoiceTemp> loadApInvoiceTemps();
}