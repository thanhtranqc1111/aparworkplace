
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.AccountMaster;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for AccountMaster entities
 * 
 */
public interface AccountMasterService {

	/**
	* Save an existing AccountMaster entity
	* 
	 */
	public void saveAccountMaster(AccountMaster accountmaster);

	/**
	 */
	public AccountMaster findAccountMasterByPrimaryKey(String code);

	/**
	* Return all AccountMaster entity
	* 
	 */
	public List<AccountMaster> findAllAccountMasters(Integer startResult, Integer maxRows);

	/**
	* Return a count of all AccountMaster entity
	* 
	 */
	public Integer countAccountMasters();

	/**
	* Delete an existing AccountMaster entity
	* 
	 */
	public void deleteAccountMaster(AccountMaster accountmaster_1);

	/**
	* Load an existing AccountMaster entity
	* 
	 */
	public Set<AccountMaster> loadAccountMasters();
	
	
	/**
	 * Find account master by name code containing.
	 *
	 * @param nameCode the name code
	 * @return the account master
	 */
	public Set<AccountMaster> findAccountMasterByNameCodeContaining(String nameCode);
	
	/**
	 * Find account name by account code
	 */
	public String findAccountNameByCode(String code);
	public void deleteAccountMasterByCodes(String codes[]);
	public String findAccountMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}