
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;

/**
 * Spring service that handles CRUD requests for EmployeeMaster entities
 * 
 */
public interface EmployeeMasterService {

	/**
	* Return all EmployeeMaster entity
	* 
	 */
	public List<EmployeeMaster> findAllEmployeeMasters(Integer startResult, Integer maxRows);

	/**
	* Delete an existing DivisionMaster entity
	* 
	 */
	public EmployeeMaster deleteEmployeeMasterDivisionMaster(BigInteger employeemaster_id, BigInteger related_divisionmaster_id);

	/**
	* Load an existing EmployeeMaster entity
	* 
	 */
	public Set<EmployeeMaster> loadEmployeeMasters();

	/**
	* Delete an existing EmployeeMaster entity
	* 
	 */
	public void deleteEmployeeMaster(EmployeeMaster employeemaster);

	/**
	* Save an existing EmployeeMaster entity
	* 
	 */
	public void saveEmployeeMaster(EmployeeMaster employeemaster_1);

	/**
	* Save an existing DivisionMaster entity
	* 
	 */
	public EmployeeMaster saveEmployeeMasterDivisionMaster(BigInteger id, DivisionMaster related_divisionmaster);

	/**
	* Return a count of all EmployeeMaster entity
	* 
	 */
	public Integer countEmployeeMasters();

	/**
	 */
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id_1);

    String findEmployeeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    void deleteEmployeeMasterByCodes(String[] codes);

    EmployeeMaster findEmployeeMasterByCode(String code);

    boolean updateEmployeeMaster(EmployeeMaster employeeMaster);

    boolean createEmployeeMaster(EmployeeMaster employeeMaster);

    void deleteEmployeeMasterByCode(String code);
}