package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.ApViewerDAO;

import com.ttv.cashflow.domain.ApViewer;
import com.ttv.cashflow.service.ApViewerService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ApViewer entities
 * 
 */

@Service("ApViewerService")

@Transactional
public class ApViewerServiceImpl implements ApViewerService {

	/**
	 * DAO injected by Spring that manages ApViewer entities
	 * 
	 */
	@Autowired
	private ApViewerDAO apViewerDAO;

	/**
	 * Instantiates a new ApViewerServiceImpl.
	 *
	 */
	public ApViewerServiceImpl() {
	}

	/**
	 * Load an existing ApViewer entity
	 * 
	 */
	@Transactional
	public Set<ApViewer> loadApViewers() {
		return apViewerDAO.findAllApViewers();
	}

	/**
	 * Return a count of all ApViewer entity
	 * 
	 */
	@Transactional
	public Integer countApViewers() {
		return ((Long) apViewerDAO.createQuerySingleResult("select count(o) from ApViewer o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public ApViewer findApViewerByPrimaryKey(BigInteger id) {
		return apViewerDAO.findApViewerByPrimaryKey(id);
	}

	/**
	 * Return all ApViewer entity
	 * 
	 */
	@Transactional
	public List<ApViewer> findAllApViewers(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ApViewer>(apViewerDAO.findAllApViewers(startResult, maxRows));
	}

	/**
	 * Delete an existing ApViewer entity
	 * 
	 */
	@Transactional
	public void deleteApViewer(ApViewer apViewer) {
		apViewerDAO.remove(apViewer);
		apViewerDAO.flush();
	}

	/**
	 * Save an existing ApViewer entity
	 * 
	 */
	@Transactional
	public void saveApViewer(ApViewer apViewer) {
		ApViewer existingApViewer = apViewerDAO.findApViewerByPrimaryKey(apViewer.getId());

		if (existingApViewer != null) {
			if (existingApViewer != apViewer) {
				existingApViewer.setId(apViewer.getId());
				existingApViewer.setApInvoiceNo(apViewer.getApInvoiceNo());
				existingApViewer.setMonth(apViewer.getMonth());
				existingApViewer.setPayeeName(apViewer.getPayeeName());
				existingApViewer.setInvoiceNo(apViewer.getInvoiceNo());
				existingApViewer.setApprovalCode(apViewer.getApprovalCode());
				existingApViewer.setOriginalCurrencyCode(apViewer.getOriginalCurrencyCode());
				existingApViewer.setExcludeGstOriginalAmount(apViewer.getExcludeGstOriginalAmount());
				existingApViewer.setGstType(apViewer.getGstType());
				existingApViewer.setGstRate(apViewer.getGstRate());
				existingApViewer.setIncludeGstOriginalAmount(apViewer.getIncludeGstOriginalAmount());
				existingApViewer.setFxRate(apViewer.getFxRate());
				existingApViewer.setIncludeGstConvertedAmount(apViewer.getIncludeGstConvertedAmount());
				existingApViewer.setAccountName(apViewer.getAccountName());
				existingApViewer.setClassName(apViewer.getClassName());
				existingApViewer.setDescription(apViewer.getDescription());
				existingApViewer.setPaymentOrderNo(apViewer.getPaymentOrderNo());
				existingApViewer.setValueDate(apViewer.getValueDate());
				existingApViewer.setPaymentIncludeGstOriginalAmount(apViewer.getPaymentIncludeGstOriginalAmount());
				existingApViewer.setPaymentRate(apViewer.getPaymentRate());
				existingApViewer.setPaymentIncludeGstConvertedAmount(apViewer.getPaymentIncludeGstConvertedAmount());
				existingApViewer.setVarianceAmount(apViewer.getVarianceAmount());
				existingApViewer.setUnpaidIncludeGstOriginalAmount(apViewer.getUnpaidIncludeGstOriginalAmount());
			}
			apViewer = apViewerDAO.store(existingApViewer);
		} else {
			apViewer = apViewerDAO.store(apViewer);
		}
		apViewerDAO.flush();
	}
}
