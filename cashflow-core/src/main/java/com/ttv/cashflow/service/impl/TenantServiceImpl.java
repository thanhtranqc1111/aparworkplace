package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.TenantDAO;

import com.ttv.cashflow.domain.Tenant;
import com.ttv.cashflow.service.TenantService;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Tenant entities
 * 
 */

@Service("TenantService")

@Transactional
public class TenantServiceImpl implements TenantService {

	/**
	 * DAO injected by Spring that manages Tenant entities
	 * 
	 */
	@Autowired
	private TenantDAO tenantDAO;

	/**
	 * Instantiates a new TenantServiceImpl.
	 *
	 */
	public TenantServiceImpl() {
	}

	/**
	 * Return a count of all Tenant entity
	 * 
	 */
	@Transactional
	public Integer countTenants() {
		return ((Long) tenantDAO.createQuerySingleResult("select count(o) from Tenant o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing Tenant entity
	 * 
	 */
	@Transactional
	public Set<Tenant> loadTenants() {
		return tenantDAO.findAllTenants();
	}

	/**
	 */
	@Transactional
	public Tenant findTenantByPrimaryKey(Integer id) {
		return tenantDAO.findTenantByPrimaryKey(id);
	}

	/**
	 * Save an existing Tenant entity
	 * 
	 */
	@Transactional
	public void saveTenant(Tenant Tenant) {
		Tenant existingTenant = tenantDAO.findTenantByPrimaryKey(Tenant.getId());

		if (existingTenant != null) {
			if (existingTenant != Tenant) {
				existingTenant.setId(Tenant.getId());
				existingTenant.setCode(Tenant.getCode());
				existingTenant.setDbName(Tenant.getDbName());
				existingTenant.setDbUrl(Tenant.getDbUrl());
				existingTenant.setDbUsername(Tenant.getDbUsername());
				existingTenant.setDbPassword(Tenant.getDbPassword());
				existingTenant.setDbDriverClassName(Tenant.getDbDriverClassName());
			}
			Tenant = tenantDAO.store(existingTenant);
		} else {
			Tenant = tenantDAO.store(Tenant);
		}
		tenantDAO.flush();
	}

	/**
	 * Return all Tenant entity
	 * 
	 */
	@Transactional
	public List<Tenant> findAllTenants(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Tenant>(tenantDAO.findAllTenants(startResult, maxRows));
	}

	/**
	 * Delete an existing Tenant entity
	 * 
	 */
	@Transactional
	public void deleteTenant(Tenant Tenant) {
		tenantDAO.remove(Tenant);
		tenantDAO.flush();
	}
}
