
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoiceAccountDetails;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;

/**
 * Spring service that handles CRUD requests for ApInvoiceClassDetails entities
 * 
 */
public interface ApInvoiceClassDetailsService {

	/**
	* Load an existing ApInvoiceClassDetails entity
	* 
	 */
	public Set<ApInvoiceClassDetails> loadApInvoiceClassDetailss();

	/**
	 */
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id);

	/**
	* Delete an existing ApInvoiceAccountDetails entity
	* 
	 */
	public ApInvoiceClassDetails deleteApInvoiceClassDetailsApInvoiceAccountDetails(BigInteger apinvoiceclassdetails_id, BigInteger related_apinvoiceaccountdetails_id);

	/**
	* Delete an existing ApInvoiceClassDetails entity
	* 
	 */
	public void deleteApInvoiceClassDetails(ApInvoiceClassDetails apinvoiceclassdetails);

	/**
	* Return a count of all ApInvoiceClassDetails entity
	* 
	 */
	public Integer countApInvoiceClassDetailss();

	/**
	* Return all ApInvoiceClassDetails entity
	* 
	 */
	public List<ApInvoiceClassDetails> findAllApInvoiceClassDetailss(Integer startResult, Integer maxRows);

	/**
	* Save an existing ApInvoiceAccountDetails entity
	* 
	 */
	public ApInvoiceClassDetails saveApInvoiceClassDetailsApInvoiceAccountDetails(BigInteger id_1, ApInvoiceAccountDetails related_apinvoiceaccountdetails);

	/**
	* Save an existing ApInvoiceClassDetails entity
	* 
	 */
	public void saveApInvoiceClassDetails(ApInvoiceClassDetails apinvoiceclassdetails_1);
	public String findByApprovalCodeNotApprovedJSON(String approvalCode) throws DataAccessException;
	public String findDetailByApprovalCodeJSON(String approvalCode);
}