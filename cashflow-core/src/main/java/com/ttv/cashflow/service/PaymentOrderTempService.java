
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.PaymentOrderTemp;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for PaymentOrderTemp entities
 * 
 */
public interface PaymentOrderTempService {

	/**
	* Load an existing PaymentOrderTemp entity
	* 
	 */
	public Set<PaymentOrderTemp> loadPaymentOrderTemps();

	/**
	 */
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id);

	/**
	* Save an existing PaymentOrderTemp entity
	* 
	 */
	public void savePaymentOrderTemp(PaymentOrderTemp paymentordertemp);

	/**
	* Delete an existing PaymentOrderTemp entity
	* 
	 */
	public void deletePaymentOrderTemp(PaymentOrderTemp paymentordertemp_1);

	/**
	* Return all PaymentOrderTemp entity
	* 
	 */
	public List<PaymentOrderTemp> findAllPaymentOrderTemps(Integer startResult, Integer maxRows);

	/**
	* Return a count of all PaymentOrderTemp entity
	* 
	 */
	public Integer countPaymentOrderTemps();
}