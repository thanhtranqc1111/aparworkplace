
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.CurrencyMaster;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for CurrencyMaster entities
 * 
 */
public interface CurrencyMasterService {

	/**
	 */
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code);

	/**
	* Save an existing CurrencyMaster entity
	* 
	 */
	public void saveCurrencyMaster(CurrencyMaster currencymaster);

	/**
	* Return a count of all CurrencyMaster entity
	* 
	 */
	public Integer countCurrencyMasters();

	/**
	* Load an existing CurrencyMaster entity
	* 
	 */
	public Set<CurrencyMaster> loadCurrencyMasters();

	/**
	* Return all CurrencyMaster entity
	* 
	 */
	public List<CurrencyMaster> findAllCurrencyMasters(Integer startResult, Integer maxRows);

	/**
	* Delete an existing CurrencyMaster entity
	* 
	 */
	public void deleteCurrencyMaster(CurrencyMaster currencymaster_1);
	public void deleteCurrencyMasterByCodes(String codes[]);
	public String findCurrencyMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}