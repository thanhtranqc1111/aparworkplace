
package com.ttv.cashflow.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;

/**
 * Spring service that handles CRUD requests for PayrollPaymentDetails entities
 * 
 */
public interface PayrollPaymentDetailsService {

	/**
	* Return all PayrollPaymentDetails entity
	* 
	 */
	public List<PayrollPaymentDetails> findAllPayrollPaymentDetailss(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Payroll entity
	* 
	 */
	public PayrollPaymentDetails deletePayrollPaymentDetailsPayroll(BigInteger payrollpaymentdetails_id, BigInteger related_payroll_id);

	/**
	* Load an existing PayrollPaymentDetails entity
	* 
	 */
	public Set<PayrollPaymentDetails> loadPayrollPaymentDetailss();

	/**
     * Save an existing PayrollPaymentDetails entity
     * 
     * @return
     * 
     */
    public PayrollPaymentDetails savePayrollPaymentDetails(PayrollPaymentDetails payrollpaymentdetails);

	/**
	* Delete an existing PayrollPaymentDetails entity
	* 
	 */
	public void deletePayrollPaymentDetails(PayrollPaymentDetails payrollpaymentdetails_1);

	/**
	* Save an existing Payroll entity
	* 
	 */
	public PayrollPaymentDetails savePayrollPaymentDetailsPayroll(BigInteger id, Payroll related_payroll);

	/**
	* Return a count of all PayrollPaymentDetails entity
	* 
	 */
	public Integer countPayrollPaymentDetailss();

	/**
	 */
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id_1);
	
	/**
	 * 
	 * @param paymentId
	 * @param payrollId
	 * @return
	 */
	public BigDecimal getTotalNetPaymentAmountOriginalByPayrollId(BigInteger paymentId, BigInteger payrollId);
}