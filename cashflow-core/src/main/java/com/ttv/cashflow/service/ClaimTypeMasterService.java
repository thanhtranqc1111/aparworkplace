
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ClaimTypeMaster;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ClaimTypeMaster entities
 * 
 */
public interface ClaimTypeMasterService {

	/**
	 */
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code);

	/**
	* Return all ClaimTypeMaster entity
	* 
	 */
	public List<ClaimTypeMaster> findAllClaimTypeMasters(Integer startResult, Integer maxRows);

	/**
	* Return a count of all ClaimTypeMaster entity
	* 
	 */
	public Integer countClaimTypeMasters();

	/**
	* Load an existing ClaimTypeMaster entity
	* 
	 */
	public Set<ClaimTypeMaster> loadClaimTypeMasters();

	/**
	* Delete an existing ClaimTypeMaster entity
	* 
	 */
	public void deleteClaimTypeMaster(ClaimTypeMaster claimtypemaster);

	/**
	* Save an existing ClaimTypeMaster entity
	* 
	 */
	public void saveClaimTypeMaster(ClaimTypeMaster claimtypemaster_1);
	public void deleteClaimTypeMasterByCodes(String codes[]);
	public String findClaimTypeMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}