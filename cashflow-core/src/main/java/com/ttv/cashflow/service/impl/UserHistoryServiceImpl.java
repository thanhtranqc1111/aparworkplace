package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.UserHistoryDAO;
import com.ttv.cashflow.domain.UserHistory;
import com.ttv.cashflow.service.UserHistoryService;

/**
 * Spring service that handles CRUD requests for UserHistory entities
 * 
 */

@Service("UserHistoryService")

@Transactional
public class UserHistoryServiceImpl implements UserHistoryService {

	/**
	 * DAO injected by Spring that manages UserHistory entities
	 * 
	 */
	@Autowired
	private UserHistoryDAO userHistoryDAO;

	/**
	 * Instantiates a new UserHistoryServiceImpl.
	 *
	 */
	public UserHistoryServiceImpl() {
	}

	/**
	 */
	@Transactional
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id) {
		return userHistoryDAO.findUserHistoryByPrimaryKey(id);
	}

	/**
	 * Delete an existing UserHistory entity
	 * 
	 */
	@Transactional
	public void deleteUserHistory(UserHistory userhistory) {
		userHistoryDAO.remove(userhistory);
		userHistoryDAO.flush();
	}

	/**
	 * Load an existing UserHistory entity
	 * 
	 */
	@Transactional
	public Set<UserHistory> loadUserHistorys() {
		return userHistoryDAO.findAllUserHistorys();
	}

	/**
	 * Save an existing UserHistory entity
	 * 
	 */
	@Transactional
	public void saveUserHistory(UserHistory userhistory) {
		UserHistory existingUserHistory = userHistoryDAO.findUserHistoryByPrimaryKey(userhistory.getId());

		if (existingUserHistory != null) {
			if (existingUserHistory != userhistory) {
				existingUserHistory.setId(userhistory.getId());
				existingUserHistory.setFirstName(userhistory.getFirstName());
				existingUserHistory.setLastName(userhistory.getLastName());
				existingUserHistory.setEmail(userhistory.getEmail());
				existingUserHistory.setModule(userhistory.getModule());
				existingUserHistory.setAction(userhistory.getAction());
				existingUserHistory.setTenantCode(userhistory.getTenantCode());
				existingUserHistory.setTenantName(userhistory.getTenantName());
				existingUserHistory.setActionDate(userhistory.getActionDate());
			}
			userhistory = userHistoryDAO.store(existingUserHistory);
		} else {
			userhistory = userHistoryDAO.store(userhistory);
		}
		userHistoryDAO.flush();
	}

	/**
	 * Return all UserHistory entity
	 * 
	 */
	@Transactional
	public List<UserHistory> findAllUserHistorys(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<UserHistory>(userHistoryDAO.findAllUserHistorys(startResult, maxRows));
	}

	/**
	 * Return a count of all UserHistory entity
	 * 
	 */
	@Transactional
	public Integer countUserHistorys() {
		return ((Long) userHistoryDAO.createQuerySingleResult("select count(o) from UserHistory o").getSingleResult()).intValue();
	}

    @Override
    public String findUserHistoryJson(UserHistory userHistory, String fromMonth, String toMonth, Integer startpage, Integer lengthOfpage,
            Integer orderColumn, String orderBy) {
        List<UserHistory> userHistories = userHistoryDAO.findPagingUserHistories(userHistory, fromMonth, toMonth, startpage, lengthOfpage,
                orderColumn, orderBy);

        Map<String, Object> map = new HashMap<>();
        map.put("data", userHistories);
        map.put("recordsFiltered", userHistoryDAO.countFilteredUserHistory(userHistory, fromMonth, toMonth));

        Gson gson = new Gson();
        return gson.toJson(map);
    }

}
