package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ArInvoiceReceiptStatusDAO;
import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;
import com.ttv.cashflow.service.ArInvoiceReceiptStatusService;

/**
 * Spring service that handles CRUD requests for ArInvoiceReceiptStatus entities
 * 
 */

@Service("ArInvoiceReceiptStatusService")

@Transactional
public class ArInvoiceReceiptStatusServiceImpl implements ArInvoiceReceiptStatusService {

	/**
	 * DAO injected by Spring that manages ArInvoiceReceiptStatus entities
	 * 
	 */
	@Autowired
	private ArInvoiceReceiptStatusDAO arInvoiceReceiptStatusDAO;

	/**
	 * Instantiates a new ArInvoiceReceiptStatusServiceImpl.
	 *
	 */
	public ArInvoiceReceiptStatusServiceImpl() {
	}

	/**
	 * Return a count of all ArInvoiceReceiptStatus entity
	 * 
	 */
	@Transactional
	public Integer countArInvoiceReceiptStatuss() {
		return ((Long) arInvoiceReceiptStatusDAO.createQuerySingleResult("select count(o) from ArInvoiceReceiptStatus o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing ArInvoiceReceiptStatus entity
	 * 
	 */
	@Transactional
	public void deleteArInvoiceReceiptStatus(ArInvoiceReceiptStatus arinvoicereceiptstatus) {
		arInvoiceReceiptStatusDAO.remove(arinvoicereceiptstatus);
		arInvoiceReceiptStatusDAO.flush();
	}

	/**
	 * Save an existing ArInvoiceReceiptStatus entity
	 * 
	 */
	@Transactional
	public void saveArInvoiceReceiptStatus(ArInvoiceReceiptStatus arinvoicereceiptstatus) {
		ArInvoiceReceiptStatus existingArInvoiceReceiptStatus = arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByPrimaryKey(arinvoicereceiptstatus.getId());

		if (existingArInvoiceReceiptStatus != null) {
			if (existingArInvoiceReceiptStatus != arinvoicereceiptstatus) {
				existingArInvoiceReceiptStatus.setId(arinvoicereceiptstatus.getId());
				existingArInvoiceReceiptStatus.setSettedIncludeGstOriginalAmount(arinvoicereceiptstatus.getSettedIncludeGstOriginalAmount());
				existingArInvoiceReceiptStatus.setRemainIncludeGstOriginalAmount(arinvoicereceiptstatus.getRemainIncludeGstOriginalAmount());
			}
			arinvoicereceiptstatus = arInvoiceReceiptStatusDAO.store(existingArInvoiceReceiptStatus);
		} else {
			arinvoicereceiptstatus = arInvoiceReceiptStatusDAO.store(arinvoicereceiptstatus);
		}
		arInvoiceReceiptStatusDAO.flush();
	}

	/**
	 * Return all ArInvoiceReceiptStatus entity
	 * 
	 */
	@Transactional
	public List<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ArInvoiceReceiptStatus>(arInvoiceReceiptStatusDAO.findAllArInvoiceReceiptStatuss(startResult, maxRows));
	}

	/**
	 * Load an existing ArInvoiceReceiptStatus entity
	 * 
	 */
	@Transactional
	public Set<ArInvoiceReceiptStatus> loadArInvoiceReceiptStatuss() {
		return arInvoiceReceiptStatusDAO.findAllArInvoiceReceiptStatuss();
	}

	/**
	 */
	@Transactional
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id) {
		return arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByPrimaryKey(id);
	}
	
	public ArInvoiceReceiptStatus exstingArInvoiceReceiptStatus(BigInteger arInvoiceId) {
		ArInvoiceReceiptStatus existingArInvoiceReceiptStatus = arInvoiceReceiptStatusDAO.findArInvoiceReceiptStatusByArInvoiceId(arInvoiceId);
		if (existingArInvoiceReceiptStatus != null && existingArInvoiceReceiptStatus.getId() != null) {
			return existingArInvoiceReceiptStatus;
		}
		return new ArInvoiceReceiptStatus();
	}
}
