package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.dao.PayrollPaymentDetailsDAO;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.PayrollPaymentDetails;
import com.ttv.cashflow.service.PayrollPaymentDetailsService;

/**
 * Spring service that handles CRUD requests for PayrollPaymentDetails entities
 * 
 */

@Service("PayrollPaymentDetailsService")

@Transactional
public class PayrollPaymentDetailsServiceImpl implements PayrollPaymentDetailsService {

	/**
	 * DAO injected by Spring that manages Payroll entities
	 * 
	 */
	@Autowired
	private PayrollDAO payrollDAO;

	/**
	 * DAO injected by Spring that manages PayrollPaymentDetails entities
	 * 
	 */
	@Autowired
	private PayrollPaymentDetailsDAO payrollPaymentDetailsDAO;

	/**
	 * Instantiates a new PayrollPaymentDetailsServiceImpl.
	 *
	 */
	public PayrollPaymentDetailsServiceImpl() {
	}

	/**
	 * Return all PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public List<PayrollPaymentDetails> findAllPayrollPaymentDetailss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<PayrollPaymentDetails>(payrollPaymentDetailsDAO.findAllPayrollPaymentDetailss(startResult, maxRows));
	}

	/**
	 * Delete an existing Payroll entity
	 * 
	 */
	@Transactional
	public PayrollPaymentDetails deletePayrollPaymentDetailsPayroll(BigInteger payrollpaymentdetails_id, BigInteger related_payroll_id) {
		PayrollPaymentDetails payrollpaymentdetails = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(payrollpaymentdetails_id, -1, -1);
		Payroll related_payroll = payrollDAO.findPayrollByPrimaryKey(related_payroll_id, -1, -1);

		payrollpaymentdetails.setPayroll(null);
		related_payroll.getPayrollPaymentDetailses().remove(payrollpaymentdetails);
		payrollpaymentdetails = payrollPaymentDetailsDAO.store(payrollpaymentdetails);
		payrollPaymentDetailsDAO.flush();

		related_payroll = payrollDAO.store(related_payroll);
		payrollDAO.flush();

		payrollDAO.remove(related_payroll);
		payrollDAO.flush();

		return payrollpaymentdetails;
	}

	/**
	 * Load an existing PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public Set<PayrollPaymentDetails> loadPayrollPaymentDetailss() {
		return payrollPaymentDetailsDAO.findAllPayrollPaymentDetailss();
	}

	/**
	 * Save an existing PayrollPaymentDetails entity
	 * 
	 */
    @Transactional
    public PayrollPaymentDetails savePayrollPaymentDetails(PayrollPaymentDetails payrollpaymentdetails) {
        PayrollPaymentDetails existingPayrollPaymentDetails = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(payrollpaymentdetails.getId());
        PayrollPaymentDetails resultPayrollPaymentDetails = null;

        if (existingPayrollPaymentDetails != null) {
            // existingPayrollPaymentDetails.setPaymentId(payrollpaymentdetails.getPaymentId());
            existingPayrollPaymentDetails.setPaymentRate(payrollpaymentdetails.getPaymentRate());
            existingPayrollPaymentDetails.setNetPaymentAmountOriginal(payrollpaymentdetails.getNetPaymentAmountOriginal());
            existingPayrollPaymentDetails.setNetPaymentAmountConverted(payrollpaymentdetails.getNetPaymentAmountConverted());
            existingPayrollPaymentDetails.setCreatedDate(payrollpaymentdetails.getCreatedDate());
            existingPayrollPaymentDetails.setModifiedDate(payrollpaymentdetails.getModifiedDate());
            resultPayrollPaymentDetails = payrollPaymentDetailsDAO.store(existingPayrollPaymentDetails);
        } else {
            resultPayrollPaymentDetails = payrollPaymentDetailsDAO.store(payrollpaymentdetails);
        }
        payrollPaymentDetailsDAO.flush();
        return resultPayrollPaymentDetails;
    }

	/**
	 * Delete an existing PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public void deletePayrollPaymentDetails(PayrollPaymentDetails payrollpaymentdetails) {
		payrollPaymentDetailsDAO.remove(payrollpaymentdetails);
		payrollPaymentDetailsDAO.flush();
	}

	/**
	 * Save an existing Payroll entity
	 * 
	 */
	@Transactional
	public PayrollPaymentDetails savePayrollPaymentDetailsPayroll(BigInteger id, Payroll related_payroll) {
		PayrollPaymentDetails payrollpaymentdetails = payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(id, -1, -1);
		Payroll existingpayroll = payrollDAO.findPayrollByPrimaryKey(related_payroll.getId());

		// copy into the existing record to preserve existing relationships
		if (existingpayroll != null) {
			existingpayroll.setId(related_payroll.getId());
			existingpayroll.setMonth(related_payroll.getMonth());
			existingpayroll.setPayrollNo(related_payroll.getPayrollNo());
			existingpayroll.setBookingDate(related_payroll.getBookingDate());
			existingpayroll.setClaimType(related_payroll.getClaimType());
			existingpayroll.setAccountPayableCode(related_payroll.getAccountPayableCode());
			existingpayroll.setAccountPayableName(related_payroll.getAccountPayableName());
			existingpayroll.setPaymentScheduleDate(related_payroll.getPaymentScheduleDate());
			existingpayroll.setTotalLaborCostOriginal(related_payroll.getTotalLaborCostOriginal());
			existingpayroll.setTotalDeductionOriginal(related_payroll.getTotalDeductionOriginal());
			existingpayroll.setTotalNetPaymentOriginal(related_payroll.getTotalNetPaymentOriginal());
			existingpayroll.setTotalNetPaymentConverted(related_payroll.getTotalNetPaymentConverted());
			existingpayroll.setCreatedDate(related_payroll.getCreatedDate());
			existingpayroll.setModifiedDate(related_payroll.getModifiedDate());
			related_payroll = existingpayroll;
		}

		payrollpaymentdetails.setPayroll(related_payroll);
		related_payroll.getPayrollPaymentDetailses().add(payrollpaymentdetails);
		payrollpaymentdetails = payrollPaymentDetailsDAO.store(payrollpaymentdetails);
		payrollPaymentDetailsDAO.flush();

		related_payroll = payrollDAO.store(related_payroll);
		payrollDAO.flush();

		return payrollpaymentdetails;
	}

	/**
	 * Return a count of all PayrollPaymentDetails entity
	 * 
	 */
	@Transactional
	public Integer countPayrollPaymentDetailss() {
		return ((Long) payrollPaymentDetailsDAO.createQuerySingleResult("select count(o) from PayrollPaymentDetails o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id) {
		return payrollPaymentDetailsDAO.findPayrollPaymentDetailsByPrimaryKey(id);
	}
	
	public BigDecimal getTotalNetPaymentAmountOriginalByPayrollId(BigInteger paymentId, BigInteger payrollId) {
		return payrollPaymentDetailsDAO.getTotalNetPaymentAmountOriginalByPayrollId(paymentId, payrollId);
	}
}
