
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.domain.ProjectMaster;
import com.ttv.cashflow.dto.ProjectAllocationDTO;

/**
 * Spring service that handles CRUD requests for ProjectAllocation entities
 * 
 */
public interface ProjectAllocationService {

	/**
	* Save an existing ProjectAllocation entity
	* 
	 */
	public void saveProjectAllocation(ProjectAllocation projectallocation);

	/**
	* Return all ProjectAllocation entity
	* 
	 */
	public List<ProjectAllocation> findAllProjectAllocations(Integer startResult, Integer maxRows);

	/**
	* Delete an existing ProjectAllocation entity
	* 
	 */
	public void deleteProjectAllocation(ProjectAllocation projectallocation_1);

	/**
	* Load an existing ProjectAllocation entity
	* 
	 */
	public Set<ProjectAllocation> loadProjectAllocations();

	/**
	* Delete an existing EmployeeMaster entity
	* 
	 */
	public ProjectAllocation deleteProjectAllocationEmployeeMaster(BigInteger projectallocation_id, BigInteger related_employeemaster_id);

	/**
	* Save an existing ProjectMaster entity
	* 
	 */
	public ProjectAllocation saveProjectAllocationProjectMaster(BigInteger id, ProjectMaster related_projectmaster);

	/**
	* Return a count of all ProjectAllocation entity
	* 
	 */
	public Integer countProjectAllocations();

	/**
	 */
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id_1);

	/**
	* Delete an existing ProjectMaster entity
	* 
	 */
	public ProjectAllocation deleteProjectAllocationProjectMaster(BigInteger projectallocation_id_1, String related_projectmaster_code);

	/**
	* Save an existing EmployeeMaster entity
	* 
	 */
	public ProjectAllocation saveProjectAllocationEmployeeMaster(BigInteger id_2, EmployeeMaster related_employeemaster);
	
	public Set<ProjectAllocationDTO> findProjectAllocation(String employeeCode, Calendar fromMonth, Calendar toMonth) throws Exception;
	
	public int saveListProjectAllocation(JsonArray jsonArray) throws Exception;
}