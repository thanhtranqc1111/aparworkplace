
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.PayeePayerTypeMaster;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for PayeePayerTypeMaster entities
 * 
 */
public interface PayeePayerTypeMasterService {

    /**
     * Return a count of all PayeePayerTypeMaster entity
     * 
     */
    public Integer countPayeePayerTypeMasters();

    /**
     */
    public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(
            String code);

    /**
     * Load an existing PayeePayerTypeMaster entity
     * 
     */
    public Set<PayeePayerTypeMaster> loadPayeePayerTypeMasters();

    /**
     * Delete an existing PayeePayerTypeMaster entity
     * 
     */
    public void deletePayeePayerTypeMaster(
            PayeePayerTypeMaster payeepayertypemaster);

    /**
     * Delete list existing PayeePayerTypeMaster entity by codes
     * 
     */
    public void deletePayeePayerTypeMasterByCodes(String codes[]);

    /**
     * Save an existing PayeePayerTypeMaster entity
     * 
     */
    public void savePayeePayerTypeMaster(
            PayeePayerTypeMaster payeepayertypemaster_1);

    /**
     * Return all PayeePayerTypeMaster entity
     * 
     */
    public List<PayeePayerTypeMaster> findAllPayeePayerTypeMasters(
            Integer startResult, Integer maxRows);

    public String findPayeeTypeMasterJson(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword);

}