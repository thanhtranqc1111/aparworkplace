package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.InvoicePaymentOrderTempDAO;

import com.ttv.cashflow.domain.InvoicePaymentOrderTemp;
import com.ttv.cashflow.service.InvoicePaymentOrderTempService;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for InvoicePaymentOrderTemp entities
 * 
 */

@Service("InvoicePaymentOrderTempService")

@Transactional
public class InvoicePaymentOrderTempServiceImpl implements InvoicePaymentOrderTempService {

	/**
	 * DAO injected by Spring that manages InvoicePaymentOrderTemp entities
	 * 
	 */
	@Autowired
	private InvoicePaymentOrderTempDAO invoicePaymentOrderTempDAO;

	/**
	 * Instantiates a new InvoicePaymentOrderTempServiceImpl.
	 *
	 */
	public InvoicePaymentOrderTempServiceImpl() {
	}

	/**
	 * Return all InvoicePaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public List<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<InvoicePaymentOrderTemp>(invoicePaymentOrderTempDAO.findAllInvoicePaymentOrderTemps(startResult, maxRows));
	}

	/**
	 * Return a count of all InvoicePaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public Integer countInvoicePaymentOrderTemps() {
		return ((Long) invoicePaymentOrderTempDAO.createQuerySingleResult("select count(o) from InvoicePaymentOrderTemp o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id) {
		return invoicePaymentOrderTempDAO.findInvoicePaymentOrderTempByPrimaryKey(id);
	}

	/**
	 * Delete an existing InvoicePaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public void deleteInvoicePaymentOrderTemp(InvoicePaymentOrderTemp invoicepaymentordertemp) {
		invoicePaymentOrderTempDAO.remove(invoicepaymentordertemp);
		invoicePaymentOrderTempDAO.flush();
	}

	/**
	 * Load an existing InvoicePaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public Set<InvoicePaymentOrderTemp> loadInvoicePaymentOrderTemps() {
		return invoicePaymentOrderTempDAO.findAllInvoicePaymentOrderTemps();
	}

	/**
	 * Save an existing InvoicePaymentOrderTemp entity
	 * 
	 */
	@Transactional
	public void saveInvoicePaymentOrderTemp(InvoicePaymentOrderTemp invoicepaymentordertemp) {
		InvoicePaymentOrderTemp existingInvoicePaymentOrderTemp = invoicePaymentOrderTempDAO.findInvoicePaymentOrderTempByPrimaryKey(invoicepaymentordertemp.getId());

		if (existingInvoicePaymentOrderTemp != null) {
			if (existingInvoicePaymentOrderTemp != invoicepaymentordertemp) {
				existingInvoicePaymentOrderTemp.setId(invoicepaymentordertemp.getId());
				existingInvoicePaymentOrderTemp.setApInvoiceId(invoicepaymentordertemp.getApInvoiceId());
				existingInvoicePaymentOrderTemp.setPaymentOrderNo(invoicepaymentordertemp.getPaymentOrderNo());
			}
			invoicepaymentordertemp = invoicePaymentOrderTempDAO.store(existingInvoicePaymentOrderTemp);
		} else {
			invoicepaymentordertemp = invoicePaymentOrderTempDAO.store(invoicepaymentordertemp);
		}
		invoicePaymentOrderTempDAO.flush();
	}
}
