package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.PaymentOrder;

/**
 * @author thoai.nh
 * created date Apr 16, 2018
 */
public interface BankStatementUtilService {
	public int updatePaymentStatus(PaymentOrder paymentOrder);
	public int resetPaymentOrderStatus(PaymentOrder paymentOrder);
}
