
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.GstMaster;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for GstMaster entities
 * 
 */
public interface GstMasterService {

	/**
	* Return a count of all GstMaster entity
	* 
	 */
	public Integer countGstMasters();

	/**
	* Return all GstMaster entity
	* 
	 */
	public List<GstMaster> findAllGstMasters(Integer startResult, Integer maxRows);

	/**
	* Delete an existing GstMaster entity
	* 
	 */
	public void deleteGstMaster(GstMaster GstMaster);

	/**
	* Load an existing GstMaster entity
	* 
	 */
	public Set<GstMaster> loadGstMasters();

	/**
	 */
	public GstMaster findGstMasterByPrimaryKey(String code);

	/**
	* Save an existing GstMaster entity
	* 
	*/
	
	public void saveGstMaster(GstMaster GstMaster_1);
    public void deleteGstMasterByCodes(String codes[]);
	public String findGstMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
	
}