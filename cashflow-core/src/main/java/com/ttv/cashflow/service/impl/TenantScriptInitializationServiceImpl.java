package com.ttv.cashflow.service.impl;

import com.ttv.cashflow.dao.TenantScriptInitializationDAO;

import com.ttv.cashflow.domain.TenantScriptInitialization;
import com.ttv.cashflow.service.TenantScriptInitializationService;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for TenantScriptInitialization entities
 * 
 */

@Service("TenantScriptInitializationService")

@Transactional
public class TenantScriptInitializationServiceImpl implements TenantScriptInitializationService {

	/**
	 * DAO injected by Spring that manages TenantScriptInitialization entities
	 * 
	 */
	@Autowired
	private TenantScriptInitializationDAO tenantScriptInitializationDAO;

	/**
	 * Instantiates a new TenantScriptInitializationServiceImpl.
	 *
	 */
	public TenantScriptInitializationServiceImpl() {
	}

	/**
	 * Save an existing TenantScriptInitialization entity
	 * 
	 */
	@Transactional
	public void saveTenantScriptInitialization(TenantScriptInitialization tenantscriptinitialization) {
		TenantScriptInitialization existingTenantScriptInitialization = tenantScriptInitializationDAO.findTenantScriptInitializationByPrimaryKey(tenantscriptinitialization.getId());

		if (existingTenantScriptInitialization != null) {
			if (existingTenantScriptInitialization != tenantscriptinitialization) {
				existingTenantScriptInitialization.setId(tenantscriptinitialization.getId());
				existingTenantScriptInitialization.setCode(tenantscriptinitialization.getCode());
				existingTenantScriptInitialization.setName(tenantscriptinitialization.getName());
				existingTenantScriptInitialization.setValue(tenantscriptinitialization.getValue());
			}
			tenantscriptinitialization = tenantScriptInitializationDAO.store(existingTenantScriptInitialization);
		} else {
			tenantscriptinitialization = tenantScriptInitializationDAO.store(tenantscriptinitialization);
		}
		tenantScriptInitializationDAO.flush();
	}

	/**
	 * Delete an existing TenantScriptInitialization entity
	 * 
	 */
	@Transactional
	public void deleteTenantScriptInitialization(TenantScriptInitialization tenantscriptinitialization) {
		tenantScriptInitializationDAO.remove(tenantscriptinitialization);
		tenantScriptInitializationDAO.flush();
	}

	/**
	 * Load an existing TenantScriptInitialization entity
	 * 
	 */
	@Transactional
	public Set<TenantScriptInitialization> loadTenantScriptInitializations() {
		return tenantScriptInitializationDAO.findAllTenantScriptInitializations();
	}

	/**
	 * Return a count of all TenantScriptInitialization entity
	 * 
	 */
	@Transactional
	public Integer countTenantScriptInitializations() {
		return ((Long) tenantScriptInitializationDAO.createQuerySingleResult("select count(o) from TenantScriptInitialization o").getSingleResult()).intValue();
	}

	/**
	 * Return all TenantScriptInitialization entity
	 * 
	 */
	@Transactional
	public List<TenantScriptInitialization> findAllTenantScriptInitializations(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<TenantScriptInitialization>(tenantScriptInitializationDAO.findAllTenantScriptInitializations(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id) {
		return tenantScriptInitializationDAO.findTenantScriptInitializationByPrimaryKey(id);
	}
}
