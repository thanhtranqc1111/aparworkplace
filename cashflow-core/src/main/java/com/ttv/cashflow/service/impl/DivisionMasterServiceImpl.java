package com.ttv.cashflow.service.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.ttv.cashflow.dao.DivisionMasterDAO;
import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.dto.DivisionMasterDTO;
import com.ttv.cashflow.service.DivisionMasterService;

/**
 * Spring service that handles CRUD requests for DivisionMaster entities
 * 
 */

@Service("DivisionMasterService")

@Transactional
public class DivisionMasterServiceImpl implements DivisionMasterService {

	/**
	 * DAO injected by Spring that manages DivisionMaster entities
	 * 
	 */
	@Autowired
	private DivisionMasterDAO divisionMasterDAO;

	/**
	 * DAO injected by Spring that manages EmployeeMaster entities
	 * 
	 */
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	/**
	 * Instantiates a new DivisionMasterServiceImpl.
	 *
	 */
	public DivisionMasterServiceImpl() {
	}

	/**
	 * Return all DivisionMaster entity
	 * 
	 */
	@Transactional
	public List<DivisionMaster> findAllDivisionMasters(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<DivisionMaster>(divisionMasterDAO.findAllDivisionMasters(startResult, maxRows));
	}

	/**
	 * Return a count of all DivisionMaster entity
	 * 
	 */
	@Transactional
	public Integer countDivisionMasters() {
		return ((Long) divisionMasterDAO.createQuerySingleResult("select count(o) from DivisionMaster o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public DivisionMaster deleteDivisionMasterEmployeeMasters(BigInteger divisionmaster_id, BigInteger related_employeemasters_id) {
		EmployeeMaster related_employeemasters = employeeMasterDAO.findEmployeeMasterByPrimaryKey(related_employeemasters_id, -1, -1);

		DivisionMaster divisionmaster = divisionMasterDAO.findDivisionMasterByPrimaryKey(divisionmaster_id, -1, -1);

		related_employeemasters.setDivisionMaster(null);
		divisionmaster.getEmployeeMasters().remove(related_employeemasters);

		employeeMasterDAO.remove(related_employeemasters);
		employeeMasterDAO.flush();

		return divisionmaster;
	}

	/**
	 * Delete an existing DivisionMaster entity
	 * 
	 */
	@Transactional
	public void deleteDivisionMaster(DivisionMaster divisionmaster) {
		divisionMasterDAO.remove(divisionmaster);
		divisionMasterDAO.flush();
	}

	/**
	 */
	@Transactional
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id) {
		return divisionMasterDAO.findDivisionMasterByPrimaryKey(id);
	}

	/**
	 * Save an existing EmployeeMaster entity
	 * 
	 */
	@Transactional
	public DivisionMaster saveDivisionMasterEmployeeMasters(BigInteger id, EmployeeMaster related_employeemasters) {
		DivisionMaster divisionmaster = divisionMasterDAO.findDivisionMasterByPrimaryKey(id, -1, -1);
		EmployeeMaster existingemployeeMasters = employeeMasterDAO.findEmployeeMasterByPrimaryKey(related_employeemasters.getId());

		// copy into the existing record to preserve existing relationships
		if (existingemployeeMasters != null) {
			existingemployeeMasters.setId(related_employeemasters.getId());
			existingemployeeMasters.setCode(related_employeemasters.getCode());
			existingemployeeMasters.setName(related_employeemasters.getName());
			existingemployeeMasters.setRoleTitle(related_employeemasters.getRoleTitle());
			existingemployeeMasters.setTeam(related_employeemasters.getTeam());
			related_employeemasters = existingemployeeMasters;
		} else {
			related_employeemasters = employeeMasterDAO.store(related_employeemasters);
			employeeMasterDAO.flush();
		}

		related_employeemasters.setDivisionMaster(divisionmaster);
		divisionmaster.getEmployeeMasters().add(related_employeemasters);
		related_employeemasters = employeeMasterDAO.store(related_employeemasters);
		employeeMasterDAO.flush();

		divisionmaster = divisionMasterDAO.store(divisionmaster);
		divisionMasterDAO.flush();

		return divisionmaster;
	}

	/**
	 * Save an existing DivisionMaster entity
	 * 
	 */
    @Transactional
    public void saveDivisionMaster(DivisionMaster divisionmaster) {
        DivisionMaster existingDivisionMaster = findDivisionMasterByCode(divisionmaster.getCode());

        if (existingDivisionMaster != null) {
            existingDivisionMaster.setCode(divisionmaster.getCode());
            existingDivisionMaster.setName(divisionmaster.getName());
            divisionMasterDAO.store(existingDivisionMaster);
        } else {
            divisionMasterDAO.store(divisionmaster);
        }
        divisionMasterDAO.flush();
    }

	/**
	 * Load an existing DivisionMaster entity
	 * 
	 */
	@Transactional
	public Set<DivisionMaster> loadDivisionMasters() {
		return divisionMasterDAO.findAllDivisionMasters();
	}

    @Override
    public String findDivisionMasterJson(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
        List<DivisionMasterDTO> divisionMasterDTOs = divisionMasterDAO.findDivisionMasterPaging(start, end, orderColumn, orderBy, keyword);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", divisionMasterDTOs);
        map.put("recordsFiltered", divisionMasterDAO.countFilteredDivisionMaster(keyword));

        Gson gson = new Gson();
        return gson.toJson(map);
    }

    @Override
    public void deleteDivisionMasterByCodes(String[] codes) {
        for (String code : codes) {
            Set<DivisionMaster> divisionMasters = divisionMasterDAO.findDivisionMasterByCode(code);

            if (!org.springframework.util.CollectionUtils.isEmpty(divisionMasters)) {
                // get only first element in set which is returned from db because code is unique
                DivisionMaster divisionMaster = divisionMasters.iterator().next();

                deleteDivisionMaster(divisionMaster);
            }
        }
    }

    @Override
    public DivisionMaster findDivisionMasterByCode(String code) {
        Set<DivisionMaster> divisionMasters = divisionMasterDAO.findDivisionMasterByCode(code);

        // get only first element in set which is returned from db because code is unique
        return !org.springframework.util.CollectionUtils.isEmpty(divisionMasters) ? divisionMasters.iterator().next() : null;
    }

    @Override
    public boolean updateDivisionMaster(DivisionMaster divisionmaster) {
        DivisionMaster existingDivisionMaster = findDivisionMasterByCode(divisionmaster.getCode());

        if (existingDivisionMaster != null) {
            existingDivisionMaster.setCode(divisionmaster.getCode());
            existingDivisionMaster.setName(divisionmaster.getName());

            DivisionMaster divisionIsReturnedFromDb = divisionMasterDAO.store(existingDivisionMaster);
            divisionMasterDAO.flush();

            return (divisionIsReturnedFromDb != null) ? true : false;
        }
        return false;
    }

    @Override
    public boolean createDivisionMaster(DivisionMaster divisionmaster) {
        DivisionMaster divisionIsReturnedFromDb = divisionMasterDAO.store(divisionmaster);
        divisionMasterDAO.flush();
        return (divisionIsReturnedFromDb != null) ? true : false;
    }
}
