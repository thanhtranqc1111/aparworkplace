
package com.ttv.cashflow.service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import com.ttv.cashflow.domain.ApprovalPurchaseRequest;

/**
 * Spring service that handles CRUD requests for ApprovalPurchaseRequest entities
 * 
 */
public interface ApprovalPurchaseRequestService {

	/**
	* Return a count of all ApprovalPurchaseRequest entity
	* 
	 */
	public Integer countApprovalPurchaseRequests();

	/**
	* Load an existing ApprovalPurchaseRequest entity
	* 
	 */
	public Set<ApprovalPurchaseRequest> loadApprovalPurchaseRequests();

	/**
	* Return all ApprovalPurchaseRequest entity
	* 
	 */
	public List<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests(Integer startResult, Integer maxRows);

	/**
	* Save an existing ApprovalPurchaseRequest entity
	* 
	 */
	public void saveApprovalPurchaseRequest(ApprovalPurchaseRequest approvalpurchaserequest);

	/**
	 */
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id);

	/**
	* Delete an existing ApprovalPurchaseRequest entity
	* 
	 */
	public void deleteApprovalPurchaseRequest(ApprovalPurchaseRequest approvalpurchaserequest);
	public void deleteApprovalByIds(BigInteger ids[]);
	public String findApprovalJson(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo, String status, Integer orderColumn, String orderBy);
	public Set<ApprovalPurchaseRequest> findApprovalByApprovalCode(String approvalCode);
	public int checkExists(ApprovalPurchaseRequest approval);
	public String saveApprovalPurchaseRequestForRestAPI(ApprovalPurchaseRequest approvalpurchaserequest);
	public String updateStatusForRestAPI(String approvalCode, String status, Calendar approvedDate, String taskId);
}