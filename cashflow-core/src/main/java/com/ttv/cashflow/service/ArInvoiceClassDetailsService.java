
package com.ttv.cashflow.service;

import com.ttv.cashflow.domain.ArInvoiceAccountDetails;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ArInvoiceClassDetails entities
 * 
 */
public interface ArInvoiceClassDetailsService {

	/**
	 */
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id);

	/**
	* Delete an existing ArInvoiceAccountDetails entity
	* 
	 */
	public ArInvoiceClassDetails deleteArInvoiceClassDetailsArInvoiceAccountDetails(BigInteger arinvoiceclassdetails_id, BigInteger related_arinvoiceaccountdetails_id);

	/**
	* Return a count of all ArInvoiceClassDetails entity
	* 
	 */
	public Integer countArInvoiceClassDetailss();

	/**
	* Load an existing ArInvoiceClassDetails entity
	* 
	 */
	public Set<ArInvoiceClassDetails> loadArInvoiceClassDetailss();

	/**
	* Return all ArInvoiceClassDetails entity
	* 
	 */
	public List<ArInvoiceClassDetails> findAllArInvoiceClassDetailss(Integer startResult, Integer maxRows);

	/**
	* Save an existing ArInvoiceAccountDetails entity
	* 
	 */
	public ArInvoiceClassDetails saveArInvoiceClassDetailsArInvoiceAccountDetails(BigInteger id_1, ArInvoiceAccountDetails related_arinvoiceaccountdetails);

	/**
	* Delete an existing ArInvoiceClassDetails entity
	* 
	 */
	public void deleteArInvoiceClassDetails(ArInvoiceClassDetails arinvoiceclassdetails);

	/**
	* Save an existing ArInvoiceClassDetails entity
	* 
	 */
	public void saveArInvoiceClassDetails(ArInvoiceClassDetails arinvoiceclassdetails_1);
}