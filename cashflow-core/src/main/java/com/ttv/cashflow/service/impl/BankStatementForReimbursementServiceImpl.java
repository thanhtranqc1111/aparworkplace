package com.ttv.cashflow.service.impl;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentDetailsDAO;
import com.ttv.cashflow.dao.ReimbursementPaymentStatusDAO;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;
import com.ttv.cashflow.domain.ReimbursementPaymentStatus;
import com.ttv.cashflow.service.BankStatementUtilService;
import com.ttv.cashflow.util.Constant;

/**
 * Spring service that handles CRUD requests for BankStatement entities
 * 
 */

@Service("BankStatementForReimbursementServiceImpl")
@Transactional
public class BankStatementForReimbursementServiceImpl implements BankStatementUtilService {
	@Autowired
	private ReimbursementDAO reimbursementDAO;
	
	@Autowired
	private ReimbursementPaymentDetailsDAO reimbursementPaymentDetailsDAO;
	
	@Autowired
	private ReimbursementPaymentStatusDAO reimbursementPaymentStatusDAO;
	
	@Override
	public int updatePaymentStatus(PaymentOrder paymentOrder) {
		try {
			Set<ReimbursementPaymentDetails> setReimbursementPaymentDetailss = reimbursementPaymentDetailsDAO.findReimbursemenPaymentDetailsByPaymentId(paymentOrder.getId());
			setReimbursementPaymentDetailss.forEach(reimbursementPaymentDetails -> {
				//update Reimbursement status
				Reimbursement reimbursement = reimbursementPaymentDetails.getReimbursement();
				BigDecimal totalAmount = reimbursement.getIncludeGstTotalOriginalAmount();
				BigDecimal totalPaid = reimbursementDAO.getTotalAmountByReimbursementId(reimbursement.getId());
				if(totalPaid.doubleValue() > 0) {
					if(totalPaid.compareTo(totalAmount) >= 0)
					{
						reimbursement.setStatus(Constant.API_PAID);//PAID
					}
					else 
					{
						reimbursement.setStatus(Constant.API_PARTIAL_PAID);//PARTIAL PAID
					}
					reimbursementDAO.store(reimbursement);
					
					//update Reimbursement invoice paid status
					ReimbursementPaymentStatus reimbursementPaymentStatus = null;
					Set<ReimbursementPaymentStatus> setReimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(reimbursement.getId());
					if(!setReimbursementPaymentStatus.isEmpty()) {
						Iterator<ReimbursementPaymentStatus> iterator = setReimbursementPaymentStatus.iterator();
						reimbursementPaymentStatus = iterator.next();
					}
					else {
						reimbursementPaymentStatus = new ReimbursementPaymentStatus();
						reimbursementPaymentStatus.setReimbursementId(reimbursement.getId());
					}
					reimbursementPaymentStatus.setPaidReimbursementOriginalPayment(totalPaid);
					reimbursementPaymentStatus.setUnpaidReimbursementOriginalPayment(totalAmount.subtract(totalPaid));
					reimbursementPaymentStatusDAO.store(reimbursementPaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int resetPaymentOrderStatus(PaymentOrder paymentOrder) {
		try {
			Set<ReimbursementPaymentDetails> setReimbursementPaymentDetails = reimbursementPaymentDetailsDAO.findReimbursemenPaymentDetailsByPaymentId(paymentOrder.getId());
			setReimbursementPaymentDetails.forEach(reimbursementPaymentDetails -> {
				//reset Reimbursement status
				Reimbursement reimbursement = reimbursementPaymentDetails.getReimbursement();
				BigDecimal totalAmount = reimbursement.getIncludeGstTotalOriginalAmount();
				BigDecimal totalPaid = reimbursementDAO.getTotalAmountByReimbursementId(reimbursement.getId());
				if(totalPaid.compareTo(totalAmount) >= 0)
				{
					reimbursement.setStatus(Constant.API_PAID);
				}
				else 
				{
					if(totalPaid.compareTo(BigDecimal.ZERO) > 0){
						reimbursement.setStatus(Constant.API_PARTIAL_PAID);
					}
					else {
						reimbursement.setStatus(Constant.API_NOT_PAID);
					}
				}
				reimbursementDAO.store(reimbursement);
				
				/*
				 * reset Reimbursement paid status
				 */
				ReimbursementPaymentStatus reimbursementPaymentStatus = null;
				Set<ReimbursementPaymentStatus> setReimbursementPaymentStatus = reimbursementPaymentStatusDAO.findReimbursementPaymentStatusByReimbursementId(reimbursement.getId());
				if(!setReimbursementPaymentStatus.isEmpty()) {
					Iterator<ReimbursementPaymentStatus> iterator = setReimbursementPaymentStatus.iterator();
					reimbursementPaymentStatus = iterator.next();
				}
				if(reimbursementPaymentStatus != null && reimbursementPaymentStatus.getId() != null) {
					reimbursementPaymentStatus.setPaidReimbursementOriginalPayment(totalPaid);
					reimbursementPaymentStatus.setUnpaidReimbursementOriginalPayment(totalAmount.subtract(totalPaid));
					reimbursementPaymentStatusDAO.store(reimbursementPaymentStatus);
				}
			});
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
