package com.ttv.cashflow.dao;

import java.util.Calendar;
import java.util.List;

import com.ttv.cashflow.dto.DashboardChartOneDTO;

public interface DashboardDAO {
	public List<DashboardChartOneDTO> getListChartOne(Calendar fromMonth, Calendar toMonth);
}
