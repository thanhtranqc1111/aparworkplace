
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ApInvoiceTemp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ApInvoiceTemp entities.
 * 
 */
public interface ApInvoiceTempDAO extends JpaDao<ApInvoiceTemp> {

	/**
	 * JPQL Query - findApInvoiceTempByPayeeName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeName(String payeeName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeName(String payeeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectFxRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectFxRate(java.math.BigDecimal projectFxRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectFxRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectFxRate(BigDecimal projectFxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPoNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNoContaining(String poNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPoNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrencyContaining(String originalCurrency) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrencyContaining(String originalCurrency, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectName(String projectName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNoContaining(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByClaimType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimType(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByClaimType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCode(String projectCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCode(String projectCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrency
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrency(String projectOriginalCurrency) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrency
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrency(String projectOriginalCurrency, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstType(String projectGstType) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstType(String projectGstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableName(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDate(Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstType(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstType(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNoContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonthBefore
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthBefore(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonthBefore
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthBefore(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceIssuedDate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceIssuedDate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceIssuedDate(Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeNameContaining(String payeeTypeName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeNameContaining(String payeeTypeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTerm
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTerm(String paymentTerm) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTerm
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTerm(String paymentTerm, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByDescription
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByDescription
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateBefore
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateBefore
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateBefore(Calendar scheduledPaymentDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountType(String accountType) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountType
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountType(String accountType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonthAfter
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthAfter(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonthAfter
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthAfter(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrency
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrency(String originalCurrency_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrency
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrency(String originalCurrency_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateAfter
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateAfter
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateAfter(Calendar scheduledPaymentDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCodeContaining(String projectCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCodeContaining(String projectCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByClaimTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimTypeContaining(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByClaimTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimTypeContaining(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountName(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceTemps
	 *
	 */
	public Set<ApInvoiceTemp> findAllApInvoiceTemps() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceTemps
	 *
	 */
	public Set<ApInvoiceTemp> findAllApInvoiceTemps(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPrimaryKey
	 *
	 */
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPrimaryKey
	 *
	 */
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstTypeContaining(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByGstTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByGstTypeContaining(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTermContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTermContaining(String paymentTerm_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTermContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTermContaining(String paymentTerm_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescription
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescription(String projectDescription) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescription
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescription(String projectDescription, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescriptionContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescriptionContaining(String projectDescription_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescriptionContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescriptionContaining(String projectDescription_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCodeContaining(String accountCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNo(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNo(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectNameContaining(String projectName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectNameContaining(String projectName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCode(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCodeContaining(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCodeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCodeContaining(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByDescriptionContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByDescriptionContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAmountExcludeGstUnconverted
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAmountExcludeGstUnconverted(java.math.BigDecimal amountExcludeGstUnconverted) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAmountExcludeGstUnconverted
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAmountExcludeGstUnconverted(BigDecimal amountExcludeGstUnconverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPoNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNo(String poNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPoNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNo(String poNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeNameContaining(String payeeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeNameContaining(String payeeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountTypeContaining(String accountType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountTypeContaining(String accountType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNo(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNo
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNo(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountNameContaining(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountNameContaining(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstRate(java.math.BigDecimal projectGstRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstRate(BigDecimal projectGstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrencyContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrencyContaining(String projectOriginalCurrency_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrencyContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrencyContaining(String projectOriginalCurrency_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempById
	 *
	 */
	public ApInvoiceTemp findApInvoiceTempById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempById
	 *
	 */
	public ApInvoiceTemp findApInvoiceTempById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCode(String accountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCode(String accountCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableNameContaining(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableNameContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableNameContaining(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCode
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByFxRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByFxRate
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeName(String payeeTypeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeName
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeName(String payeeTypeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonth
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonth(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByMonth
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByMonth(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstTypeContaining(String projectGstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstTypeContaining
	 *
	 */
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstTypeContaining(String projectGstType_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * Get list ApInvoiceTemp have group by payee_name, invoice_no, month, payment_order_no
	 */
	public List<String> findListIdApInvoiceTempByHavingGroup() throws DataAccessException;
	
	/**
	 * find ApInvoiceTemp By ListId
	 */
	public List<ApInvoiceTemp> findApInvoiceTempByListId(List<BigInteger> listId) throws DataAccessException;
	
	/**
	 * Do truncate
	 */
	public void truncate() throws DataAccessException;

}