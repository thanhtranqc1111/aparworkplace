
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.RoleDAO;
import com.ttv.cashflow.domain.Role;

/**
 * DAO to manage Role entities.
 * 
 */
@Repository("RoleDAO")

@Transactional
public class RoleDAOImpl extends AbstractJpaDao<Role> implements RoleDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Role.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new RoleDAOImpl
	 *
	 */
	public RoleDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findRoleByCreatedDate
	 *
	 */
	@Transactional
	public Set<Role> findRoleByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findRoleByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<Role> findRoleByDescriptionContaining(String description) throws DataAccessException {

		return findRoleByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleByDescription
	 *
	 */
	@Transactional
	public Set<Role> findRoleByDescription(String description) throws DataAccessException {

		return findRoleByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByDescription", startResult, maxRows, description);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleByName
	 *
	 */
	@Transactional
	public Set<Role> findRoleByName(String name) throws DataAccessException {

		return findRoleByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByName", startResult, maxRows, name);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleById
	 *
	 */
	@Transactional
	public Role findRoleById(Integer id) throws DataAccessException {

		return findRoleById(id, -1, -1);
	}

	/**
	 * JPQL Query - findRoleById
	 *
	 */

	@Transactional
	public Role findRoleById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findRoleById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Role) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findRoleByPrimaryKey
	 *
	 */
	@Transactional
	public Role findRoleByPrimaryKey(Integer id) throws DataAccessException {

		return findRoleByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByPrimaryKey
	 *
	 */

	@Transactional
	public Role findRoleByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findRoleByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Role) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllRoles
	 *
	 */
	@Transactional
	public Set<Role> findAllRoles() throws DataAccessException {

		return findAllRoles(-1, -1);
	}

	/**
	 * JPQL Query - findAllRoles
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findAllRoles(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllRoles", startResult, maxRows);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleByModifiedDate
	 *
	 */
	@Transactional
	public Set<Role> findRoleByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findRoleByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * JPQL Query - findRoleByNameContaining
	 *
	 */
	@Transactional
	public Set<Role> findRoleByNameContaining(String name) throws DataAccessException {

		return findRoleByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findRoleByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Role> findRoleByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRoleByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Role>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Role entity) {
		return true;
	}

	public Set<Role> findRolePaging(String keyword, int startResult, int maxRows) throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("findRolePaging", startResult, maxRows, keyword != null? keyword: "");
		return new LinkedHashSet<Role>(query.getResultList());
	}

	public int countRolePaging(String keyword) throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countRolePaging", -1, -1, keyword != null? keyword: "");
		return ((Number)query.getSingleResult()).intValue();
	}
}
