
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ApInvoicePaymentStatus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ApInvoicePaymentStatus entities.
 * 
 */
public interface ApInvoicePaymentStatusDAO extends JpaDao<ApInvoicePaymentStatus> {

	/**
	 * JPQL Query - findApInvoicePaymentStatusByUnpaidUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByUnpaidUnconvertedAmount(java.math.BigDecimal unpaidUnconvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusByUnpaidUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByUnpaidUnconvertedAmount(BigDecimal unpaidUnconvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPrimaryKey
	 *
	 */
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPrimaryKey
	 *
	 */
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoicePaymentStatuss
	 *
	 */
	public Set<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoicePaymentStatuss
	 *
	 */
	public Set<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusById
	 *
	 */
	public ApInvoicePaymentStatus findApInvoicePaymentStatusById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusById
	 *
	 */
	public ApInvoicePaymentStatus findApInvoicePaymentStatusById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPaidUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByPaidUnconvertedAmount(java.math.BigDecimal paidUnconvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPaidUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByPaidUnconvertedAmount(BigDecimal paidUnconvertedAmount, int startResult, int maxRows) throws DataAccessException;
	
	/**
     * JPQL Query - findApInvoicePaymentStatusByApInvoiceId
     *
     */
    public ApInvoicePaymentStatus findApInvoicePaymentStatusByApInvoiceId(BigInteger apInvoiceId) throws DataAccessException;
        

}