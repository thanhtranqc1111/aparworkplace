
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PayrollPaymentStatusDAO;
import com.ttv.cashflow.domain.PayrollPaymentStatus;

/**
 * DAO to manage PayrollPaymentStatus entities.
 * 
 */
@Repository("PayrollPaymentStatusDAO")

@Transactional
public class PayrollPaymentStatusDAOImpl extends AbstractJpaDao<PayrollPaymentStatus>
		implements PayrollPaymentStatusDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PayrollPaymentStatus.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayrollPaymentStatusDAOImpl
	 *
	 */
	public PayrollPaymentStatusDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPrimaryKey
	 *
	 */
	@Transactional
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPayrollPaymentStatusByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPrimaryKey
	 *
	 */

	@Transactional
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollPaymentStatusByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollPaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPayrollId
	 *
	 */
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPayrollId(BigInteger payrollId) throws DataAccessException {

		return findPayrollPaymentStatusByPayrollId(payrollId, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPayrollId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPayrollId(BigInteger payrollId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentStatusByPayrollId", startResult, maxRows, payrollId);
		return new LinkedHashSet<PayrollPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByUnpaidNetPayment
	 *
	 */
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByUnpaidNetPayment(java.math.BigDecimal unpaidNetPayment) throws DataAccessException {

		return findPayrollPaymentStatusByUnpaidNetPayment(unpaidNetPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByUnpaidNetPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByUnpaidNetPayment(java.math.BigDecimal unpaidNetPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentStatusByUnpaidNetPayment", startResult, maxRows, unpaidNetPayment);
		return new LinkedHashSet<PayrollPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPaidNetPayment
	 *
	 */
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPaidNetPayment(java.math.BigDecimal paidNetPayment) throws DataAccessException {

		return findPayrollPaymentStatusByPaidNetPayment(paidNetPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusByPaidNetPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPaidNetPayment(java.math.BigDecimal paidNetPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentStatusByPaidNetPayment", startResult, maxRows, paidNetPayment);
		return new LinkedHashSet<PayrollPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayrollPaymentStatuss
	 *
	 */
	@Transactional
	public Set<PayrollPaymentStatus> findAllPayrollPaymentStatuss() throws DataAccessException {

		return findAllPayrollPaymentStatuss(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayrollPaymentStatuss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentStatus> findAllPayrollPaymentStatuss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayrollPaymentStatuss", startResult, maxRows);
		return new LinkedHashSet<PayrollPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusById
	 *
	 */
	@Transactional
	public PayrollPaymentStatus findPayrollPaymentStatusById(BigInteger id) throws DataAccessException {

		return findPayrollPaymentStatusById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentStatusById
	 *
	 */

	@Transactional
	public PayrollPaymentStatus findPayrollPaymentStatusById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollPaymentStatusById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollPaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PayrollPaymentStatus entity) {
		return true;
	}
	
}
