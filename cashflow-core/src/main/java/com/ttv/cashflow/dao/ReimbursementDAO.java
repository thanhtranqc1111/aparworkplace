
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.dto.ReimbursementPaymentDTO;

/**
 * DAO to manage Reimbursement entities.
 * 
 */
public interface ReimbursementDAO extends JpaDao<Reimbursement> {

	/**
	 * JPQL Query - findReimbursementByDescriptionContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByDescriptionContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByReimbursementNo
	 *
	 */
	public Set<Reimbursement> findReimbursementByReimbursementNo(String reimbursementNo) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByReimbursementNo
	 *
	 */
	public Set<Reimbursement> findReimbursementByReimbursementNo(String reimbursementNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDate(Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByDescription
	 *
	 */
	public Set<Reimbursement> findReimbursementByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByDescription
	 *
	 */
	public Set<Reimbursement> findReimbursementByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByStatus
	 *
	 */
	public Set<Reimbursement> findReimbursementByStatus(String status) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByStatus
	 *
	 */
	public Set<Reimbursement> findReimbursementByStatus(String status, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementById
	 *
	 */
	public Reimbursement findReimbursementById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementById
	 *
	 */
	public Reimbursement findReimbursementById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeCode(String employeeCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeCode(String employeeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDate(java.util.Calendar bookingDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDate(Calendar bookingDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCodeContaining(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCodeContaining(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByPrimaryKey
	 *
	 */
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByPrimaryKey
	 *
	 */
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountConverted
	 *
	 */
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountConverted(java.math.BigDecimal includeGstTotalAmountConverted) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountConverted
	 *
	 */
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountConverted(BigDecimal includeGstTotalAmountConverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeCodeContaining(String employeeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeCodeContaining(String employeeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByClaimType
	 *
	 */
	public Set<Reimbursement> findReimbursementByClaimType(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByClaimType
	 *
	 */
	public Set<Reimbursement> findReimbursementByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonthBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonthBefore(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonthBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonthBefore(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableCodeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByCreatedDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByCreatedDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonth
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonth(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonth
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonth(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonthAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonthAfter(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByMonthAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByMonthAfter(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeName
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeName(String employeeName) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeName
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeName(String employeeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableNameContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableNameContaining(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableNameContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByStatusContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByStatusContaining(String status_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByStatusContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByStatusContaining(String status_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableCode(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableCode
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableCode(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursements
	 *
	 */
	public Set<Reimbursement> findAllReimbursements() throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursements
	 *
	 */
	public Set<Reimbursement> findAllReimbursements(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountOriginal
	 *
	 */
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountOriginal(java.math.BigDecimal includeGstTotalAmountConverted) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountOriginal
	 *
	 */
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountOriginal(BigDecimal includeGstTotalAmountConverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateBefore(Calendar scheduledPaymentDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByFxRate
	 *
	 */
	public Set<Reimbursement> findReimbursementByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByFxRate
	 *
	 */
	public Set<Reimbursement> findReimbursementByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDateAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDateAfter(java.util.Calendar bookingDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDateAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDateAfter(Calendar bookingDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDateBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDateBefore(java.util.Calendar bookingDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByBookingDateBefore
	 *
	 */
	public Set<Reimbursement> findReimbursementByBookingDateBefore(Calendar bookingDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByClaimTypeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByClaimTypeContaining(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByClaimTypeContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByClaimTypeContaining(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeNameContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeNameContaining(String employeeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByEmployeeNameContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByEmployeeNameContaining(String employeeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByModifiedDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByModifiedDate
	 *
	 */
	public Set<Reimbursement> findReimbursementByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableName
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableName(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByAccountPayableName
	 *
	 */
	public Set<Reimbursement> findReimbursementByAccountPayableName(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByReimbursementNoContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByReimbursementNoContaining(String reimbursementNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByReimbursementNoContaining
	 *
	 */
	public Set<Reimbursement> findReimbursementByReimbursementNoContaining(String reimbursementNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateAfter
	 *
	 */
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateAfter(Calendar scheduledPaymentDate_2, int startResult, int maxRows) throws DataAccessException;
	
	public String getReimbursementNo();
	
	public String getReimbursementNo(Calendar cal);
	
	public Set<Reimbursement> searchReimbursementDetails(Object... parameters) throws DataAccessException;
	
	public Boolean checkApprovalReimbursement(String reimNo);
	
	public int countReimbursementFilter(Calendar fromMonth, Calendar toMonth, String fromReimbursementNo, String toReimbursementNo, String employeeCode, int status);
	
	public List<ReimbursementPaymentDTO> findReimbursementPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth, String fromReimbursementNo, 
																String toReimbursementNo, String employeeCode, Integer orderColumn, String orderBy, int status);
	
	public BigInteger countDetail(String reimbursmentNo) throws DataAccessException;
	
	public Set<Reimbursement> findReimbursementByImportKey(String importKey) throws DataAccessException;
	
	public BigDecimal getTotalAmountByReimbursementId(BigInteger reimbursementNoId);
}