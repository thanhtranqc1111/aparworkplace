
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.AccountPayableViewerDAO;
import com.ttv.cashflow.domain.AccountPayableViewer;
import com.ttv.cashflow.domain.PaymentOrder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage AccountPayableViewer entities.
 * 
 */
@Repository("AccountPayableViewerDAO")

@Transactional
public class AccountPayableViewerDAOImpl extends AbstractJpaDao<AccountPayableViewer>
		implements AccountPayableViewerDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			AccountPayableViewer.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new AccountPayableViewerDAOImpl
	 *
	 */
	public AccountPayableViewerDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentConvertedAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentConvertedAmount(BigDecimal paymentConvertedAmount) throws DataAccessException {

		return findAccountPayableViewerByPaymentConvertedAmount(paymentConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentConvertedAmount(BigDecimal paymentConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByPaymentConvertedAmount", startResult, maxRows, paymentConvertedAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerById
	 *
	 */
	@Transactional
	public AccountPayableViewer findAccountPayableViewerById(BigInteger id) throws DataAccessException {

		return findAccountPayableViewerById(id, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerById
	 *
	 */

	@Transactional
	public AccountPayableViewer findAccountPayableViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountPayableViewerById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.AccountPayableViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNo
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findAccountPayableViewerByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByCurrencyContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrencyContaining(String currency) throws DataAccessException {

		return findAccountPayableViewerByCurrencyContaining(currency, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByCurrencyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrencyContaining(String currency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByCurrencyContaining", startResult, maxRows, currency);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOriginalAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOriginalAmount(BigDecimal paymentOriginalAmount) throws DataAccessException {

		return findAccountPayableViewerByPaymentOriginalAmount(paymentOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOriginalAmount(BigDecimal paymentOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByPaymentOriginalAmount", startResult, maxRows, paymentOriginalAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByDescriptionContaining(String description) throws DataAccessException {

		return findAccountPayableViewerByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonthAfter
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findAccountPayableViewerByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findAccountPayableViewerByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByPaymentOrderNoContaining", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByProjectNameContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectNameContaining(String projectName) throws DataAccessException {

		return findAccountPayableViewerByProjectNameContaining(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByProjectNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByProjectNameContaining", startResult, maxRows, projectName);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findAccountPayableViewerByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVarianceAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVarianceAmount(BigDecimal varianceAmount) throws DataAccessException {

		return findAccountPayableViewerByVarianceAmount(varianceAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVarianceAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByVarianceAmount", startResult, maxRows, varianceAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNoContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoContaining(String accountPayableNo) throws DataAccessException {

		return findAccountPayableViewerByAccountPayableNoContaining(accountPayableNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoContaining(String accountPayableNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByAccountPayableNoContaining", startResult, maxRows, accountPayableNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByOriginalAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByOriginalAmount(BigDecimal originalAmount) throws DataAccessException {

		return findAccountPayableViewerByOriginalAmount(originalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByOriginalAmount(BigDecimal originalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByOriginalAmount", startResult, maxRows, originalAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findAccountPayableViewerByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonthBefore
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findAccountPayableViewerByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByDescription
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByDescription(String description) throws DataAccessException {

		return findAccountPayableViewerByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByDescription", startResult, maxRows, description);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllAccountPayableViewers
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAllAccountPayableViewers() throws DataAccessException {

		return findAllAccountPayableViewers(-1, -1);
	}

	/**
	 * JPQL Query - findAllAccountPayableViewers
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAllAccountPayableViewers(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllAccountPayableViewers", startResult, maxRows);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentRate
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentRate(BigDecimal paymentRate) throws DataAccessException {

		return findAccountPayableViewerByPaymentRate(paymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByPaymentRate", startResult, maxRows, paymentRate);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByProjectName
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectName(String projectName) throws DataAccessException {

		return findAccountPayableViewerByProjectName(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByProjectName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByProjectName", startResult, maxRows, projectName);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimTypeContaining(String claimType) throws DataAccessException {

		return findAccountPayableViewerByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByConvertedAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByConvertedAmount(BigDecimal convertedAmount) throws DataAccessException {

		return findAccountPayableViewerByConvertedAmount(convertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByConvertedAmount(BigDecimal convertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByConvertedAmount", startResult, maxRows, convertedAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNo
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNo(String accountPayableNo) throws DataAccessException {

		return findAccountPayableViewerByAccountPayableNo(accountPayableNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNo(String accountPayableNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByAccountPayableNo", startResult, maxRows, accountPayableNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCode
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCode(String approvalCode) throws DataAccessException {

		return findAccountPayableViewerByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVendorContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVendorContaining(String vendor) throws DataAccessException {

		return findAccountPayableViewerByVendorContaining(vendor, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVendorContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVendorContaining(String vendor, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByVendorContaining", startResult, maxRows, vendor);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByValueDate
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByValueDate(java.util.Calendar valueDate) throws DataAccessException {

		return findAccountPayableViewerByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByValueDate(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByClaimType
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimType(String claimType) throws DataAccessException {

		return findAccountPayableViewerByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVendor
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVendor(String vendor) throws DataAccessException {

		return findAccountPayableViewerByVendor(vendor, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByVendor
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByVendor(String vendor, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByVendor", startResult, maxRows, vendor);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByFxRate
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByFxRate(BigDecimal fxRate) throws DataAccessException {

		return findAccountPayableViewerByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonth
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonth(java.util.Calendar month) throws DataAccessException {

		return findAccountPayableViewerByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByMonth", startResult, maxRows, month);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountNameContaining(String accountName) throws DataAccessException {

		return findAccountPayableViewerByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findAccountPayableViewerByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountName
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountName(String accountName) throws DataAccessException {

		return findAccountPayableViewerByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPrimaryKey
	 *
	 */
	@Transactional
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id) throws DataAccessException {

		return findAccountPayableViewerByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByPrimaryKey
	 *
	 */

	@Transactional
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountPayableViewerByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.AccountPayableViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAccountPayableViewerByRemainAmount
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByRemainAmount(BigDecimal remainAmount) throws DataAccessException {

		return findAccountPayableViewerByRemainAmount(remainAmount, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByRemainAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByRemainAmount(BigDecimal remainAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByRemainAmount", startResult, maxRows, remainAmount);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountPayableViewerByCurrency
	 *
	 */
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrency(String currency) throws DataAccessException {

		return findAccountPayableViewerByCurrency(currency, -1, -1);
	}

	/**
	 * JPQL Query - findAccountPayableViewerByCurrency
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrency(String currency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountPayableViewerByCurrency", startResult, maxRows, currency);
		return new LinkedHashSet<AccountPayableViewer>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(AccountPayableViewer entity) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
    public List<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoList(List<String> paymentOrderNoList){
	    Query query = createNamedQuery("findAccountPayableViewerByPaymentOrderNoList", -1, -1, paymentOrderNoList.size() == 0 ? "" : paymentOrderNoList);
        return new ArrayList<AccountPayableViewer>(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
    public List<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoList(List<String> accountPayableNoList){
        Query query = createNamedQuery("findAccountPayableViewerByAccountPayableNoList", -1, -1, accountPayableNoList.size() == 0 ? "" : accountPayableNoList);
        return new ArrayList<AccountPayableViewer>(query.getResultList());
    }
	
	
	
	public void refresh(){
	    getEntityManager().createNativeQuery("REFRESH MATERIALIZED VIEW account_payable_viewer").executeUpdate();
	}
}
