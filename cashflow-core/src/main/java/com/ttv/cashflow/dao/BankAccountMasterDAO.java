
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.BankAccountMaster;

/**
 * DAO to manage BankAccountMaster entities.
 * 
 */
public interface BankAccountMasterDAO extends JpaDao<BankAccountMaster> {

	/**
	 * JPQL Query - findBankAccountMasterByTypeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByTypeContaining(String type) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByTypeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByTypeContaining(String type, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCode
	 *
	 */
	public BankAccountMaster findBankAccountMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCode
	 *
	 */
	public BankAccountMaster findBankAccountMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCode
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCode(String currencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCode
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankCodeContaining(String bankCode) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankCodeContaining(String bankCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNoContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNoContaining(String bankAccountNo) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNoContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNoContaining(String bankAccountNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByType
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByType(String type_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByType
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByType(String type_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByName
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByName
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByNameContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByNameContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllBankAccountMasters
	 *
	 */
	public Set<BankAccountMaster> findAllBankAccountMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllBankAccountMasters
	 *
	 */
	public Set<BankAccountMaster> findAllBankAccountMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankCode
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankCode
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByPrimaryKey
	 *
	 */
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByPrimaryKey
	 *
	 */
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNo
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNo(String bankAccountNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNo
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNo(String bankAccountNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCodeContaining(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCodeContaining(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCodeContaining(String currencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCodeContaining
	 *
	 */
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCodeContaining(String currencyCode_1, int startResult, int maxRows) throws DataAccessException;
	public int countBankAccountMasterFilter(Integer start, Integer end, String keyword);
	public int countBankAccountMasterAll();
	public Set<BankAccountMaster> findBankAccountMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
	public Set<BankAccountMaster> findBankAccountMasterByBankNameAndBankAccountNo(String bankName, String bankAccountNo);
}