
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.DivisionMasterDAO;
import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.dto.DivisionMasterDTO;

/**
 * DAO to manage DivisionMaster entities.
 * 
 */
@Repository("DivisionMasterDAO")

@Transactional
public class DivisionMasterDAOImpl extends AbstractJpaDao<DivisionMaster> implements DivisionMasterDAO {
    
    private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
        mapColumnIndex.put(1, "divisionMaster.code");
        mapColumnIndex.put(2, "divisionMaster.name");
    }

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			DivisionMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new DivisionMasterDAOImpl
	 *
	 */
	public DivisionMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findDivisionMasterById
	 *
	 */
	@Transactional
	public DivisionMaster findDivisionMasterById(BigInteger id) throws DataAccessException {

		return findDivisionMasterById(id, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterById
	 *
	 */

	@Transactional
	public DivisionMaster findDivisionMasterById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findDivisionMasterById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.DivisionMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findDivisionMasterByPrimaryKey
	 *
	 */
	@Transactional
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id) throws DataAccessException {

		return findDivisionMasterByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterByPrimaryKey
	 *
	 */

	@Transactional
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findDivisionMasterByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.DivisionMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findDivisionMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByCodeContaining(String code) throws DataAccessException {

		return findDivisionMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDivisionMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<DivisionMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findDivisionMasterByCode
	 *
	 */
	@Transactional
    public Set<DivisionMaster> findDivisionMasterByCode(String code) throws DataAccessException {
        return findDivisionMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterByCode
	 *
	 */

	@Transactional
    public Set<DivisionMaster> findDivisionMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDivisionMasterByCode", startResult, maxRows, code);
        return new LinkedHashSet<DivisionMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findDivisionMasterByName
	 *
	 */
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByName(String name) throws DataAccessException {

		return findDivisionMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDivisionMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<DivisionMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllDivisionMasters
	 *
	 */
	@Transactional
	public Set<DivisionMaster> findAllDivisionMasters() throws DataAccessException {

		return findAllDivisionMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllDivisionMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<DivisionMaster> findAllDivisionMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllDivisionMasters", startResult, maxRows);
		return new LinkedHashSet<DivisionMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findDivisionMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByNameContaining(String name) throws DataAccessException {

		return findDivisionMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findDivisionMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<DivisionMaster> findDivisionMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDivisionMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<DivisionMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(DivisionMaster entity) {
		return true;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<DivisionMasterDTO> findDivisionMasterPaging(Integer firstResult, Integer maxResults, Integer orderColumn, String orderBy,
            String keyword) {

        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT divisionMaster.id as id, divisionMaster.code as code, divisionMaster.name as name ");
        queryString.append("FROM division_master divisionMaster ");
        queryString.append("WHERE lower(code) like lower(CONCAT('%', ?1, '%')) or lower(name) like lower(CONCAT('%', ?1, '%')) ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ");
        queryString.append(orderBy);

        Query query = getEntityManager().createNativeQuery(queryString.toString(), "DivisionMasterDTO");
        query.setParameter(1, keyword);
        query.setFirstResult(firstResult == null || firstResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : firstResult);

        if (maxResults != null && maxResults > 0) {
            query.setMaxResults(maxResults);
        }
        return (List<DivisionMasterDTO>) query.getResultList();
    }

    @Override
    public Long countAllDivisionMaster() {
        return (Long) executeQuerySingleResult("SELECT count(1) FROM DivisionMaster");
    }

    @Override
    public Long countFilteredDivisionMaster(String keyword) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM DivisionMaster divisionMaster ");
        queryString.append("WHERE lower(code) like lower(CONCAT('%', ?1, '%')) or lower(name) like lower(CONCAT('%', ?1, '%'))");

        Query query = createQuery(queryString.toString(), -1, -1);
        query.setParameter(1, keyword);
        return (Long) query.getSingleResult();
    }
}
