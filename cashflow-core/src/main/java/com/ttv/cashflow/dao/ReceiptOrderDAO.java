
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ReceiptOrder;
import com.ttv.cashflow.dto.ReceiptOrderDTO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ReceiptOrder entities.
 * 
 */
public interface ReceiptOrderDAO extends JpaDao<ReceiptOrder> {

	/**
	 * JPQL Query - findReceiptOrderBySettedIncludeGstOriginalAmount
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderBySettedIncludeGstOriginalAmount(java.math.BigDecimal settedIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderBySettedIncludeGstOriginalAmount
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderBySettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderById
	 *
	 */
	public ReceiptOrder findReceiptOrderById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderById
	 *
	 */
	public ReceiptOrder findReceiptOrderById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByArInvoiceId
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByArInvoiceId(BigInteger arInvoiceId) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByArInvoiceId
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByArInvoiceId(BigInteger arInvoiceId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByTransactionId
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByTransactionId(BigInteger transactionId) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByTransactionId
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByTransactionId(BigInteger transactionId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByCreatedDate
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByCreatedDate
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByModifedDate
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByModifedDate(java.util.Calendar modifedDate) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByModifedDate
	 *
	 */
	public Set<ReceiptOrder> findReceiptOrderByModifedDate(Calendar modifedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllReceiptOrders
	 *
	 */
	public Set<ReceiptOrder> findAllReceiptOrders() throws DataAccessException;

	/**
	 * JPQL Query - findAllReceiptOrders
	 *
	 */
	public Set<ReceiptOrder> findAllReceiptOrders(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByPrimaryKey
	 *
	 */
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findReceiptOrderByPrimaryKey
	 *
	 */
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * JPQL Query - findReceiptOrderPaging
	 *
	 */
	public List<ReceiptOrderDTO> findReceiptOrderPaging(List<BigInteger> listBankStatementIds, Integer orderColumn, String orderBy);
	
	/**
	 * JPQL Query - getReceiptOrderNo
	 *
	 */
	public String getReceiptOrderNo();
}