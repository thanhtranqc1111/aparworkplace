
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.SubsidiaryDAO;
import com.ttv.cashflow.domain.Subsidiary;

/**
 * DAO to manage Subsidiary entities.
 * 
 */
@Repository("SubsidiaryDAO")

@Transactional
public class SubsidiaryDAOImpl extends AbstractJpaDao<Subsidiary> implements SubsidiaryDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Subsidiary.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new SubsidiaryDAOImpl
	 *
	 */
	public SubsidiaryDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findSubsidiaryByWebsite
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByWebsite(String website) throws DataAccessException {

		return findSubsidiaryByWebsite(website, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByWebsite
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByWebsite(String website, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByWebsite", startResult, maxRows, website);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByDescriptionContaining(String description) throws DataAccessException {

		return findSubsidiaryByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByName
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByName(String name) throws DataAccessException {

		return findSubsidiaryByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByName", startResult, maxRows, name);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByDescription
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByDescription(String description) throws DataAccessException {

		return findSubsidiaryByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByDescription", startResult, maxRows, description);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByPrimaryKey
	 *
	 */
	@Transactional
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id) throws DataAccessException {

		return findSubsidiaryByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByPrimaryKey
	 *
	 */

	@Transactional
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSubsidiaryByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Subsidiary) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findSubsidiaryByNameContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByNameContaining(String name) throws DataAccessException {

		return findSubsidiaryByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByPhoneContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByPhoneContaining(String phone) throws DataAccessException {

		return findSubsidiaryByPhoneContaining(phone, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByPhoneContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByPhoneContaining(String phone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByPhoneContaining", startResult, maxRows, phone);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByModifiedDate
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findSubsidiaryByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByCreatedDate
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findSubsidiaryByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByIsActive
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByIsActive(Boolean isActive) throws DataAccessException {

		return findSubsidiaryByIsActive(isActive, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByIsActive
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByIsActive(Boolean isActive, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByIsActive", startResult, maxRows, isActive);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByCode
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCode(String code) throws DataAccessException {

		return findSubsidiaryByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByCode", startResult, maxRows, code);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByCodeContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCodeContaining(String code) throws DataAccessException {

		return findSubsidiaryByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllSubsidiarys
	 *
	 */
	@Transactional
	public Set<Subsidiary> findAllSubsidiarys() throws DataAccessException {

		return findAllSubsidiarys(-1, -1);
	}

	/**
	 * JPQL Query - findAllSubsidiarys
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findAllSubsidiarys(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllSubsidiarys", startResult, maxRows);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryById
	 *
	 */
	@Transactional
	public Subsidiary findSubsidiaryById(Integer id) throws DataAccessException {

		return findSubsidiaryById(id, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryById
	 *
	 */

	@Transactional
	public Subsidiary findSubsidiaryById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSubsidiaryById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Subsidiary) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findSubsidiaryByAddress
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByAddress(String address) throws DataAccessException {

		return findSubsidiaryByAddress(address, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByAddress
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByAddress(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByAddress", startResult, maxRows, address);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByWebsiteContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByWebsiteContaining(String website) throws DataAccessException {

		return findSubsidiaryByWebsiteContaining(website, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByWebsiteContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByWebsiteContaining(String website, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByWebsiteContaining", startResult, maxRows, website);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByAddressContaining
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByAddressContaining(String address) throws DataAccessException {

		return findSubsidiaryByAddressContaining(address, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByAddressContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByAddressContaining", startResult, maxRows, address);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryByPhone
	 *
	 */
	@Transactional
	public Set<Subsidiary> findSubsidiaryByPhone(String phone) throws DataAccessException {

		return findSubsidiaryByPhone(phone, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryByPhone
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Subsidiary> findSubsidiaryByPhone(String phone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryByPhone", startResult, maxRows, phone);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Subsidiary entity) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Set<Subsidiary> findSubsidiaryByNameOrCode(String keywork){
		Query query = createNamedQuery("findSubsidiaryByNameOrCode", -1, -1, keywork);
		return new LinkedHashSet<Subsidiary>(query.getResultList());
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<Subsidiary> findListSubsidiaryByUserId(Integer userId) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT subsidiary.* ");
        queryString.append("FROM subsidiary subsidiary INNER JOIN subsidiary_user_detail subsidiaryUserDetail ");
        queryString.append("ON subsidiary.id = subsidiaryUserDetail.subsidiary_id ");
        queryString.append("AND subsidiaryUserDetail.user_account_id = :userAccountId");

        Query query = getEntityManager().createNativeQuery(queryString.toString(), Subsidiary.class);
        query.setParameter("userAccountId", userId);

        return query.getResultList();
    }

}
