
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ApInvoiceTempDAO;
import com.ttv.cashflow.domain.ApInvoiceTemp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ApInvoiceTemp entities.
 * 
 */
@Repository("ApInvoiceTempDAO")

@Transactional
public class ApInvoiceTempDAOImpl extends AbstractJpaDao<ApInvoiceTemp> implements ApInvoiceTempDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApInvoiceTemp.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlow_Driver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoiceTempDAOImpl
	 *
	 */
	public ApInvoiceTempDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeName
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeName(String payeeName) throws DataAccessException {

		return findApInvoiceTempByPayeeName(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeName(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPayeeName", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectFxRate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectFxRate(java.math.BigDecimal projectFxRate) throws DataAccessException {

		return findApInvoiceTempByProjectFxRate(projectFxRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectFxRate(java.math.BigDecimal projectFxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectFxRate", startResult, maxRows, projectFxRate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPoNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNoContaining(String poNo) throws DataAccessException {

		return findApInvoiceTempByPoNoContaining(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPoNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPoNoContaining", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrencyContaining(String originalCurrency) throws DataAccessException {

		return findApInvoiceTempByOriginalCurrencyContaining(originalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrencyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrencyContaining(String originalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByOriginalCurrencyContaining", startResult, maxRows, originalCurrency);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectName
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectName(String projectName) throws DataAccessException {

		return findApInvoiceTempByProjectName(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectName", startResult, maxRows, projectName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findApInvoiceTempByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByClaimType
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimType(String claimType) throws DataAccessException {

		return findApInvoiceTempByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCode(String projectCode) throws DataAccessException {

		return findApInvoiceTempByProjectCode(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCode(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectCode", startResult, maxRows, projectCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrency
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrency(String projectOriginalCurrency) throws DataAccessException {

		return findApInvoiceTempByProjectOriginalCurrency(projectOriginalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrency
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrency(String projectOriginalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectOriginalCurrency", startResult, maxRows, projectOriginalCurrency);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstType
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstType(String projectGstType) throws DataAccessException {

		return findApInvoiceTempByProjectGstType(projectGstType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstType(String projectGstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectGstType", startResult, maxRows, projectGstType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableName
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findApInvoiceTempByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findApInvoiceTempByScheduledPaymentDate(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByScheduledPaymentDate", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstType
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstType(String gstType) throws DataAccessException {

		return findApInvoiceTempByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findApInvoiceTempByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPaymentOrderNoContaining", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonthBefore
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findApInvoiceTempByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceIssuedDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findApInvoiceTempByInvoiceIssuedDate(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceIssuedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByInvoiceIssuedDate", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findApInvoiceTempByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeNameContaining(String payeeTypeName) throws DataAccessException {

		return findApInvoiceTempByPayeeTypeNameContaining(payeeTypeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeNameContaining(String payeeTypeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPayeeTypeNameContaining", startResult, maxRows, payeeTypeName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTerm
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTerm(String paymentTerm) throws DataAccessException {

		return findApInvoiceTempByPaymentTerm(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTerm
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTerm(String paymentTerm, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPaymentTerm", startResult, maxRows, paymentTerm);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByDescription
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByDescription(String description) throws DataAccessException {

		return findApInvoiceTempByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateBefore
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findApInvoiceTempByScheduledPaymentDateBefore(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByScheduledPaymentDateBefore", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountType
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountType(String accountType) throws DataAccessException {

		return findApInvoiceTempByAccountType(accountType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountType(String accountType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountType", startResult, maxRows, accountType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstRate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findApInvoiceTempByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonthAfter
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findApInvoiceTempByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrency
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrency(String originalCurrency) throws DataAccessException {

		return findApInvoiceTempByOriginalCurrency(originalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByOriginalCurrency
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByOriginalCurrency(String originalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByOriginalCurrency", startResult, maxRows, originalCurrency);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateAfter
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findApInvoiceTempByScheduledPaymentDateAfter(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByScheduledPaymentDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByScheduledPaymentDateAfter", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCodeContaining(String projectCode) throws DataAccessException {

		return findApInvoiceTempByProjectCodeContaining(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectCodeContaining(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectCodeContaining", startResult, maxRows, projectCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimTypeContaining(String claimType) throws DataAccessException {

		return findApInvoiceTempByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountName
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountName(String accountName) throws DataAccessException {

		return findApInvoiceTempByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApInvoiceTemps
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findAllApInvoiceTemps() throws DataAccessException {

		return findAllApInvoiceTemps(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoiceTemps
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findAllApInvoiceTemps(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoiceTemps", startResult, maxRows);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoiceTempByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoiceTemp findApInvoiceTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceTempByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstTypeContaining(String gstType) throws DataAccessException {

		return findApInvoiceTempByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTermContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTermContaining(String paymentTerm) throws DataAccessException {

		return findApInvoiceTempByPaymentTermContaining(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentTermContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentTermContaining(String paymentTerm, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPaymentTermContaining", startResult, maxRows, paymentTerm);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescription
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescription(String projectDescription) throws DataAccessException {

		return findApInvoiceTempByProjectDescription(projectDescription, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescription(String projectDescription, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectDescription", startResult, maxRows, projectDescription);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescriptionContaining(String projectDescription) throws DataAccessException {

		return findApInvoiceTempByProjectDescriptionContaining(projectDescription, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectDescriptionContaining(String projectDescription, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectDescriptionContaining", startResult, maxRows, projectDescription);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCodeContaining(String accountCode) throws DataAccessException {

		return findApInvoiceTempByAccountCodeContaining(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountCodeContaining", startResult, maxRows, accountCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findApInvoiceTempByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectNameContaining(String projectName) throws DataAccessException {

		return findApInvoiceTempByProjectNameContaining(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectNameContaining", startResult, maxRows, projectName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findApInvoiceTempByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findApInvoiceTempByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByDescriptionContaining(String description) throws DataAccessException {

		return findApInvoiceTempByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAmountExcludeGstUnconverted
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAmountExcludeGstUnconverted(java.math.BigDecimal amountExcludeGstUnconverted) throws DataAccessException {

		return findApInvoiceTempByAmountExcludeGstUnconverted(amountExcludeGstUnconverted, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAmountExcludeGstUnconverted
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAmountExcludeGstUnconverted(java.math.BigDecimal amountExcludeGstUnconverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAmountExcludeGstUnconverted", startResult, maxRows, amountExcludeGstUnconverted);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPoNo
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNo(String poNo) throws DataAccessException {

		return findApInvoiceTempByPoNo(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPoNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPoNo(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPoNo", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeNameContaining(String payeeName) throws DataAccessException {

		return findApInvoiceTempByPayeeNameContaining(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeNameContaining(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPayeeNameContaining", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountTypeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountTypeContaining(String accountType) throws DataAccessException {

		return findApInvoiceTempByAccountTypeContaining(accountType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountTypeContaining(String accountType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountTypeContaining", startResult, maxRows, accountType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNo
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findApInvoiceTempByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountNameContaining(String accountName) throws DataAccessException {

		return findApInvoiceTempByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstRate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstRate(java.math.BigDecimal projectGstRate) throws DataAccessException {

		return findApInvoiceTempByProjectGstRate(projectGstRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstRate(java.math.BigDecimal projectGstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectGstRate", startResult, maxRows, projectGstRate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrencyContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrencyContaining(String projectOriginalCurrency) throws DataAccessException {

		return findApInvoiceTempByProjectOriginalCurrencyContaining(projectOriginalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectOriginalCurrencyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectOriginalCurrencyContaining(String projectOriginalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectOriginalCurrencyContaining", startResult, maxRows, projectOriginalCurrency);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempById
	 *
	 */
	@Transactional
	public ApInvoiceTemp findApInvoiceTempById(BigInteger id) throws DataAccessException {

		return findApInvoiceTempById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempById
	 *
	 */

	@Transactional
	public ApInvoiceTemp findApInvoiceTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceTempById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCode(String accountCode) throws DataAccessException {

		return findApInvoiceTempByAccountCode(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountCode(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountCode", startResult, maxRows, accountCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findApInvoiceTempByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCode(String approvalCode) throws DataAccessException {

		return findApInvoiceTempByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByFxRate
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findApInvoiceTempByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeName
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeName(String payeeTypeName) throws DataAccessException {

		return findApInvoiceTempByPayeeTypeName(payeeTypeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByPayeeTypeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByPayeeTypeName(String payeeTypeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByPayeeTypeName", startResult, maxRows, payeeTypeName);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonth
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonth(java.util.Calendar month) throws DataAccessException {

		return findApInvoiceTempByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstTypeContaining(String projectGstType) throws DataAccessException {

		return findApInvoiceTempByProjectGstTypeContaining(projectGstType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceTempByProjectGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceTemp> findApInvoiceTempByProjectGstTypeContaining(String projectGstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceTempByProjectGstTypeContaining", startResult, maxRows, projectGstType);
		return new LinkedHashSet<ApInvoiceTemp>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoiceTemp entity) {
		return true;
	}
	
//	@SuppressWarnings("unchecked")
//    @Transactional
//    public List<ApInvoiceTemp> findApInvoiceTempByHavingGroup() throws DataAccessException {
//	    
//	    String queryString = new StringBuilder()
//        .append("SELECT ")
////        .append("payee_name AS payeeName ")
////        .append(",invoice_no AS invoiceNo ")
////        .append(",month ")
////        .append(",payment_order_no AS paymentOrderNo ")
////        .append(",string_agg(cast(id as text), ',') AS listId ")
//        .append("string_agg(cast(id as text), ',') AS listId ")
//        .append("FROM ap_invoice_temp ")
//        .append("GROUP BY payee_name, invoice_no, month, payment_order_no ")
//        .toString();
//	    
//	    Query query = getEntityManager().createNativeQuery(queryString, "result1");
//
//	    return new ArrayList<ApInvoiceTemp>(query.getResultList());
//    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<String> findListIdApInvoiceTempByHavingGroup() throws DataAccessException {
        
        String queryString = new StringBuilder()
        .append("SELECT ")
        .append("string_agg(cast(id as text), ',') AS listId ")
        .append("FROM ap_invoice_temp ")
        .append("GROUP BY payee_name, invoice_no, month, claim_type ")
        .append("ORDER BY listId ")
        .toString();
        
        Query query = getEntityManager().createNativeQuery(queryString);

        return new ArrayList<String>(query.getResultList());
    }
	
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<ApInvoiceTemp> findApInvoiceTempByListId(List<BigInteger> listId) throws DataAccessException {
	    
	    String queryString = new StringBuilder()
	    .append("select myApInvoiceTemp from ApInvoiceTemp myApInvoiceTemp WHERE id IN (?1)")
        .toString();
        
        Query query = getEntityManager().createQuery(queryString)
                                        .setParameter(1, listId);
        
        return new ArrayList<ApInvoiceTemp>(query.getResultList());
    }
	
    @Transactional
    public void truncate() throws DataAccessException {
        getEntityManager().createNativeQuery("truncate table ap_invoice_temp").executeUpdate();
    }
}
