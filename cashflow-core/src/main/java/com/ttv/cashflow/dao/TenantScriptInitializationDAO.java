
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.TenantScriptInitialization;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage TenantScriptInitialization entities.
 * 
 */
public interface TenantScriptInitializationDAO extends JpaDao<TenantScriptInitialization> {

	/**
	 * JPQL Query - findAllTenantScriptInitializations
	 *
	 */
	public Set<TenantScriptInitialization> findAllTenantScriptInitializations() throws DataAccessException;

	/**
	 * JPQL Query - findAllTenantScriptInitializations
	 *
	 */
	public Set<TenantScriptInitialization> findAllTenantScriptInitializations(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByPrimaryKey
	 *
	 */
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByPrimaryKey
	 *
	 */
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByCode
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByCode
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByValueContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValueContaining(String value) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByValueContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValueContaining(String value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationById
	 *
	 */
	public TenantScriptInitialization findTenantScriptInitializationById(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationById
	 *
	 */
	public TenantScriptInitialization findTenantScriptInitializationById(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByName
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByName
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByValue
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValue(String value_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByValue
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValue(String value_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByCodeContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByCodeContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByNameContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantScriptInitializationByNameContaining
	 *
	 */
	public Set<TenantScriptInitialization> findTenantScriptInitializationByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

}