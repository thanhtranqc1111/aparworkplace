
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.UserAccount;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage UserAccount entities.
 * 
 */
public interface UserAccountDAO extends JpaDao<UserAccount> {

	/**
	 * JPQL Query - findUserAccountByFirstNameContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByFirstNameContaining(String firstName) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByFirstNameContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByFirstNameContaining(String firstName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllUserAccounts
	 *
	 */
	public Set<UserAccount> findAllUserAccounts() throws DataAccessException;

	/**
	 * JPQL Query - findAllUserAccounts
	 *
	 */
	public Set<UserAccount> findAllUserAccounts(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByLastName
	 *
	 */
	public Set<UserAccount> findUserAccountByLastName(String lastName) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByLastName
	 *
	 */
	public Set<UserAccount> findUserAccountByLastName(String lastName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByCreatedDate
	 *
	 */
	public Set<UserAccount> findUserAccountByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByCreatedDate
	 *
	 */
	public Set<UserAccount> findUserAccountByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountById
	 *
	 */
	public UserAccount findUserAccountById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountById
	 *
	 */
	public UserAccount findUserAccountById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPhone
	 *
	 */
	public Set<UserAccount> findUserAccountByPhone(String phone) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPhone
	 *
	 */
	public Set<UserAccount> findUserAccountByPhone(String phone, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPhoneContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByPhoneContaining(String phone_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPhoneContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByPhoneContaining(String phone_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByLastNameContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByLastNameContaining(String lastName_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByLastNameContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByLastNameContaining(String lastName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPasswordContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByPasswordContaining(String password) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPasswordContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByPasswordContaining(String password, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByFirstName
	 *
	 */
	public Set<UserAccount> findUserAccountByFirstName(String firstName_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByFirstName
	 *
	 */
	public Set<UserAccount> findUserAccountByFirstName(String firstName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPrimaryKey
	 *
	 */
	public UserAccount findUserAccountByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPrimaryKey
	 *
	 */
	public UserAccount findUserAccountByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByIsActive
	 *
	 */
	public Set<UserAccount> findUserAccountByIsActive(Boolean isActive) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByIsActive
	 *
	 */
	public Set<UserAccount> findUserAccountByIsActive(Boolean isActive, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByEmailContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByEmailContaining(String email) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByEmailContaining
	 *
	 */
	public Set<UserAccount> findUserAccountByEmailContaining(String email, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByModifiedDate
	 *
	 */
	public Set<UserAccount> findUserAccountByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByModifiedDate
	 *
	 */
	public Set<UserAccount> findUserAccountByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByEmail
	 *
	 */
	public Set<UserAccount> findUserAccountByEmail(String email_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByEmail
	 *
	 */
	public Set<UserAccount> findUserAccountByEmail(String email_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPassword
	 *
	 */
	public Set<UserAccount> findUserAccountByPassword(String password_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserAccountByPassword
	 *
	 */
	public Set<UserAccount> findUserAccountByPassword(String password_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * JPQL Query - findAccountByEmailAndPassword
	 *
	 */
	public Set<UserAccount> findUserAccountManagerPaging(Integer tanentId, String keyword, int startResult, int maxRows) throws DataAccessException;
	public int countUserAccountManagerPaging(Integer tanentId, String keyword) throws DataAccessException;
	public Set<UserAccount> findUserAccountAdminPaging(String keyword, int startResult, int maxRows) throws DataAccessException;
	public int countUserAccountAdminPaging(String keyword) throws DataAccessException;

}