
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.BankMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage BankMaster entities.
 * 
 */
public interface BankMasterDAO extends JpaDao<BankMaster> {

	/**
	 * JPQL Query - findBankMasterByPrimaryKey
	 *
	 */
	public BankMaster findBankMasterByPrimaryKey(String code) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByPrimaryKey
	 *
	 */
	public BankMaster findBankMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByNameContaining
	 *
	 */
	public Set<BankMaster> findBankMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByNameContaining
	 *
	 */
	public Set<BankMaster> findBankMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByName
	 *
	 */
	public Set<BankMaster> findBankMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByName
	 *
	 */
	public Set<BankMaster> findBankMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByCodeContaining
	 *
	 */
	public Set<BankMaster> findBankMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByCodeContaining
	 *
	 */
	public Set<BankMaster> findBankMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByCode
	 *
	 */
	public BankMaster findBankMasterByCode(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findBankMasterByCode
	 *
	 */
	public BankMaster findBankMasterByCode(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllBankMasters
	 *
	 */
	public Set<BankMaster> findAllBankMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllBankMasters
	 *
	 */
	public Set<BankMaster> findAllBankMasters(int startResult, int maxRows) throws DataAccessException;
	
	public Set<BankMaster> findBankMasterPaging(
                Integer start, Integer end, Integer orderColumn, String orderBy,
                String keyword) throws DataAccessException;
	public int countBankMasterFilter(Integer start, Integer length, Integer orderColumn,
            String orderBy, String keyword);

}