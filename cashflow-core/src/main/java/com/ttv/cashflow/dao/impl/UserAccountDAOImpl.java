
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.UserAccountDAO;
import com.ttv.cashflow.domain.UserAccount;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage UserAccount entities.
 * 
 */
@Repository("UserAccountDAO")

@Transactional
public class UserAccountDAOImpl extends AbstractJpaDao<UserAccount> implements UserAccountDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			UserAccount.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new UserAccountDAOImpl
	 *
	 */
	public UserAccountDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findUserAccountByFirstNameContaining
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByFirstNameContaining(String firstName) throws DataAccessException {

		return findUserAccountByFirstNameContaining(firstName, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByFirstNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByFirstNameContaining(String firstName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByFirstNameContaining", startResult, maxRows, firstName);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllUserAccounts
	 *
	 */
	@Transactional
	public Set<UserAccount> findAllUserAccounts() throws DataAccessException {

		return findAllUserAccounts(-1, -1);
	}

	/**
	 * JPQL Query - findAllUserAccounts
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findAllUserAccounts(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllUserAccounts", startResult, maxRows);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByLastName
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByLastName(String lastName) throws DataAccessException {

		return findUserAccountByLastName(lastName, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByLastName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByLastName(String lastName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByLastName", startResult, maxRows, lastName);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByCreatedDate
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findUserAccountByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountById
	 *
	 */
	@Transactional
	public UserAccount findUserAccountById(Integer id) throws DataAccessException {

		return findUserAccountById(id, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountById
	 *
	 */

	@Transactional
	public UserAccount findUserAccountById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserAccountById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.UserAccount) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserAccountByPhone
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByPhone(String phone) throws DataAccessException {

		return findUserAccountByPhone(phone, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByPhone
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByPhone(String phone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByPhone", startResult, maxRows, phone);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByPhoneContaining
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByPhoneContaining(String phone) throws DataAccessException {

		return findUserAccountByPhoneContaining(phone, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByPhoneContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByPhoneContaining(String phone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByPhoneContaining", startResult, maxRows, phone);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByLastNameContaining
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByLastNameContaining(String lastName) throws DataAccessException {

		return findUserAccountByLastNameContaining(lastName, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByLastNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByLastNameContaining(String lastName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByLastNameContaining", startResult, maxRows, lastName);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByPasswordContaining
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByPasswordContaining(String password) throws DataAccessException {

		return findUserAccountByPasswordContaining(password, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByPasswordContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByPasswordContaining(String password, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByPasswordContaining", startResult, maxRows, password);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByFirstName
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByFirstName(String firstName) throws DataAccessException {

		return findUserAccountByFirstName(firstName, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByFirstName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByFirstName(String firstName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByFirstName", startResult, maxRows, firstName);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByPrimaryKey
	 *
	 */
	@Transactional
	public UserAccount findUserAccountByPrimaryKey(Integer id) throws DataAccessException {

		return findUserAccountByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByPrimaryKey
	 *
	 */

	@Transactional
	public UserAccount findUserAccountByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserAccountByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.UserAccount) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserAccountByIsActive
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByIsActive(Boolean isActive) throws DataAccessException {

		return findUserAccountByIsActive(isActive, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByIsActive
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByIsActive(Boolean isActive, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByIsActive", startResult, maxRows, isActive);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByEmailContaining
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByEmailContaining(String email) throws DataAccessException {

		return findUserAccountByEmailContaining(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByEmailContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByEmailContaining(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByEmailContaining", startResult, maxRows, email);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByModifiedDate
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findUserAccountByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByEmail
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByEmail(String email) throws DataAccessException {

		return findUserAccountByEmail(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByEmail
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByEmail(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByEmail", startResult, maxRows, email);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserAccountByPassword
	 *
	 */
	@Transactional
	public Set<UserAccount> findUserAccountByPassword(String password) throws DataAccessException {

		return findUserAccountByPassword(password, -1, -1);
	}

	/**
	 * JPQL Query - findUserAccountByPassword
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserAccount> findUserAccountByPassword(String password, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserAccountByPassword", startResult, maxRows, password);
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(UserAccount entity) {
		return true;
	}

	@Override
	public Set<UserAccount> findUserAccountManagerPaging(Integer tanentId, String keyword, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findUserAccountManagerPaging", startResult, maxRows, tanentId, keyword != null? keyword: "");
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	@Override
	public int countUserAccountManagerPaging(Integer tanentId, String keyword) throws DataAccessException {
		Query query = createNamedQuery("countUserAccountManagerPaging", -1, -1, tanentId, keyword != null? keyword: "");
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public Set<UserAccount> findUserAccountAdminPaging(String keyword, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findUserAccountAdminPaging", startResult, maxRows, keyword != null? keyword: "");
		return new LinkedHashSet<UserAccount>(query.getResultList());
	}

	@Override
	public int countUserAccountAdminPaging(String keyword) throws DataAccessException {
		Query query = createNamedQuery("countUserAccountAdminPaging", -1, -1, keyword != null? keyword: "");
		return ((Number)query.getSingleResult()).intValue();
	}
}
