
package com.ttv.cashflow.dao;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.dto.EmployeeMasterDTO;

/**
 * DAO to manage EmployeeMaster entities.
 * 
 */
public interface EmployeeMasterDAO extends JpaDao<EmployeeMaster> {

	/**
	 * JPQL Query - findEmployeeMasterByCode
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByCode
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterById
	 *
	 */
	public EmployeeMaster findEmployeeMasterById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterById
	 *
	 */
	public EmployeeMaster findEmployeeMasterById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitleContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitleContaining(String roleTitle) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitleContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitleContaining(String roleTitle, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByName
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByName
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByTeam
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByTeam(String team) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByTeam
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByTeam(String team, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByPrimaryKey
	 *
	 */
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByPrimaryKey
	 *
	 */
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByNameContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByNameContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByCodeContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByCodeContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByTeamContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByTeamContaining(String team_1) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByTeamContaining
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByTeamContaining(String team_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllEmployeeMasters
	 *
	 */
	public Set<EmployeeMaster> findAllEmployeeMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllEmployeeMasters
	 *
	 */
	public Set<EmployeeMaster> findAllEmployeeMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitle
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitle(String roleTitle_1) throws DataAccessException;

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitle
	 *
	 */
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitle(String roleTitle_1, int startResult, int maxRows) throws DataAccessException;

    List<EmployeeMasterDTO> findEmployeeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    Long countEmployeeMasterAll();

    Integer countEmployeeMasterFilter(String keyword);
    
    List<EmployeeMasterDTO> getEmployeeMaster(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
    
    List<EmployeeMasterDTO> getEmployeeMasterForPayroll(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword, Calendar paymentScheduleDate);
}