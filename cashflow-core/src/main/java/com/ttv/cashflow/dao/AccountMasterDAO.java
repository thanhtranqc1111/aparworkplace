
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.AccountMaster;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage AccountMaster entities.
 * 
 */
public interface AccountMasterDAO extends JpaDao<AccountMaster> {

	/**
	 * JPQL Query - findAccountMasterByPrimaryKey
	 *
	 */
	public AccountMaster findAccountMasterByPrimaryKey(String code) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByPrimaryKey
	 *
	 */
	public AccountMaster findAccountMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByParentCode
	 *
	 */
	public Set<AccountMaster> findAccountMasterByParentCode(String parentCode) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByParentCode
	 *
	 */
	public Set<AccountMaster> findAccountMasterByParentCode(String parentCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByType
	 *
	 */
	public Set<AccountMaster> findAccountMasterByType(String type) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByType
	 *
	 */
	public Set<AccountMaster> findAccountMasterByType(String type, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByName
	 *
	 */
	public Set<AccountMaster> findAccountMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByName
	 *
	 */
	public Set<AccountMaster> findAccountMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByCodeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByCodeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByTypeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByTypeContaining(String type_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByTypeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByTypeContaining(String type_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByParentCodeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByParentCodeContaining(String parentCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByParentCodeContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByParentCodeContaining(String parentCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByCode
	 *
	 */
	public AccountMaster findAccountMasterByCode(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByCode
	 *
	 */
	public AccountMaster findAccountMasterByCode(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByNameContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByNameContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountMasters
	 *
	 */
	public Set<AccountMaster> findAllAccountMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountMasters
	 *
	 */
	public Set<AccountMaster> findAllAccountMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountMasterByNameContaining
	 *
	 */
	public Set<AccountMaster> findAccountMasterByNameCodeContaining(String nameCode) throws DataAccessException;
	public int countAccountMasterFilter(Integer start, Integer end, String keyword);
	public int countAccountMasterAll();
	public Set<AccountMaster> findAccountMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
	
}