
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.BusinessTypeMaster;

/**
 * DAO to manage BusinessTypeMaster entities.
 * 
 */
public interface BusinessTypeMasterDAO extends JpaDao<BusinessTypeMaster> {

	/**
	 * JPQL Query - findAllBusinessTypeMasters
	 *
	 */
	public Set<BusinessTypeMaster> findAllBusinessTypeMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllBusinessTypeMasters
	 *
	 */
	public Set<BusinessTypeMaster> findAllBusinessTypeMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByCodeContaining
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByCodeContaining(String code) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByCodeContaining
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByCode
	 *
	 */
	public BusinessTypeMaster findBusinessTypeMasterByCode(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByCode
	 *
	 */
	public BusinessTypeMaster findBusinessTypeMasterByCode(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByName
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByName
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByPrimaryKey
	 *
	 */
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByPrimaryKey
	 *
	 */
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByNameContaining
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findBusinessTypeMasterByNameContaining
	 *
	 */
	public Set<BusinessTypeMaster> findBusinessTypeMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;
	public int countBusinessTypeMasterFilter(Integer start, Integer end, String keyword);
	public int countBusinessTypeMasterAll();
	public Set<BusinessTypeMaster> findBusinessTypeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    boolean checkExistingBusinessTypeCode(String businessTypeName);
}