
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.dto.InvoicePayment;
import com.ttv.cashflow.dto.PaymentInvoice;

/**
 * DAO to manage ApInvoice entities.
 * 
 */
public interface ApInvoiceDAO extends JpaDao<ApInvoice> {

	/**
	 * JPQL Query - findApInvoiceByDescriptionContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByDescriptionContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoices
	 *
	 */
	public Set<ApInvoice> findAllApInvoices() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoices
	 *
	 */
	public Set<ApInvoice> findAllApInvoices(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPrimaryKey
	 *
	 */
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPrimaryKey
	 *
	 */
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeCode(String payeeCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeCode(String payeeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByGstAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByGstAmount(java.math.BigDecimal gstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByGstAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByGstAmount(BigDecimal gstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApInvoiceNoContaining(String apInvoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApInvoiceNoContaining(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByClaimType
	 *
	 */
	public Set<ApInvoice> findApInvoiceByClaimType(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByClaimType
	 *
	 */
	public Set<ApInvoice> findApInvoiceByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceNo(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeName(String payeeTypeName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeName(String payeeTypeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeNameContaining(String payeeName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeNameContaining(String payeeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByModifiedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByModifiedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPoNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPoNoContaining(String poNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPoNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByExcludeGstAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByExcludeGstAmount(java.math.BigDecimal excludeGstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByExcludeGstAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByExcludeGstAmount(BigDecimal excludeGstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApInvoiceNo(String apInvoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApInvoiceNo(String apInvoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeCode(String payeeTypeCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeCode(String payeeTypeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeCodeContaining(String payeeTypeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeCodeContaining(String payeeTypeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPoNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPoNo(String poNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPoNo
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPoNo(String poNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceNoContaining(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceNoContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceNoContaining(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByStatus
	 *
	 */
	public Set<ApInvoice> findApInvoiceByStatus(String status) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByStatus
	 *
	 */
	public Set<ApInvoice> findApInvoiceByStatus(String status, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPaymentTerm
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPaymentTerm(String paymentTerm) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPaymentTerm
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPaymentTerm(String paymentTerm, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByDescription
	 *
	 */
	public Set<ApInvoice> findApInvoiceByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByDescription
	 *
	 */
	public Set<ApInvoice> findApInvoiceByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableName(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByFxRate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByFxRate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByStatusContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByStatusContaining(String status_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByStatusContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByStatusContaining(String status_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInpt
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInpt(Integer inpt) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInpt
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInpt(Integer inpt, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceIssuedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvoiceIssuedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvoiceIssuedDate(Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApprovalCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByApprovalCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeNameContaining(String payeeTypeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeTypeNameContaining(String payeeTypeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByVatCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByVatCode(String vatCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByVatCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByVatCode(String vatCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvTotalAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvTotalAmount(java.math.BigDecimal invTotalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByInvTotalAmount
	 *
	 */
	public Set<ApInvoice> findApInvoiceByInvTotalAmount(BigDecimal invTotalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceById
	 *
	 */
	public ApInvoice findApInvoiceById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceById
	 *
	 */
	public ApInvoice findApInvoiceById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByGst
	 *
	 */
	public Set<ApInvoice> findApInvoiceByGst(java.math.BigDecimal gst) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByGst
	 *
	 */
	public Set<ApInvoice> findApInvoiceByGst(BigDecimal gst, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPaymentTermContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPaymentTermContaining(String paymentTerm_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPaymentTermContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPaymentTermContaining(String paymentTerm_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableNameContaining(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableNameContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableNameContaining(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByCreatedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByCreatedDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByVatCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByVatCodeContaining(String vatCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByVatCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByVatCodeContaining(String vatCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByScheduledPaymentDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByScheduledPaymentDate
	 *
	 */
	public Set<ApInvoice> findApInvoiceByScheduledPaymentDate(Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeCodeContaining(String payeeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeCodeContaining(String payeeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableCode(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCode
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeName(String payeeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByPayeeName
	 *
	 */
	public Set<ApInvoice> findApInvoiceByPayeeName(String payeeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByClaimTypeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByClaimTypeContaining(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByClaimTypeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByClaimTypeContaining(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableCodeContaining(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCodeContaining
	 *
	 */
	public Set<ApInvoice> findApInvoiceByAccountPayableCodeContaining(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	public List<InvoiceDetail> getInvoiceDetail(BigInteger apInvoiceId);
	
	
	public BigInteger getLastId();

    public List<PaymentInvoice> searchInvoice(Integer startResult, Integer maxRow, String calFrom, String calTo,
            String invoice, String payeeName, String status, String fromApNo, String toApNo, Integer orderColumn, String orderBy);

    public BigInteger searchCount(String calFrom, String calTo, String invoice, String payeeName, String status, String fromApNo, String toApNo);
    
    public List<InvoicePayment> searchInvoiceDetail(List<String> apInvoiceNoLst, Integer orderColumn, String orderBy);
    
    /**
     * Call function ap_invoice_no()
     */
    public String getApInvoiceNo();
    
    /**
     * Call function check_approval_ap_invoice(apinvoiceno character)
     */
    public Boolean checkApprovalApInvoice(String apInvoiceNo);
    
    /**
     * Call function ap_invoice_no(t timestamp )
     */
    public String getApInvoiceNo(Calendar cal);
    
    /**
     * findApInvoiceByImportKey
     */
    public ApInvoice findApInvoiceByImportKey(String importKey);
    
    public ApInvoice findApInvoiceByApNo(String apInvoiceNo) throws DataAccessException;
    
    /**
     * Count number detail of AP invoice.
     */
    public BigInteger countDetail(String apInvoiceNo) throws DataAccessException;
    
    public boolean checkUniqueInvoiceNo(String invoiceNo, String id) throws DataAccessException;
    
}