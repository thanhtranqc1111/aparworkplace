
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PayrollPaymentStatus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PayrollPaymentStatus entities.
 * 
 */
public interface PayrollPaymentStatusDAO extends JpaDao<PayrollPaymentStatus> {

	/**
	 * JPQL Query - findPayrollPaymentStatusByPrimaryKey
	 *
	 */
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByPrimaryKey
	 *
	 */
	public PayrollPaymentStatus findPayrollPaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByPayrollId
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPayrollId(BigInteger payrollId) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByPayrollId
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPayrollId(BigInteger payrollId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByUnpaidNetPayment
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByUnpaidNetPayment(java.math.BigDecimal unpaidNetPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByUnpaidNetPayment
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByUnpaidNetPayment(BigDecimal unpaidNetPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByPaidNetPayment
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPaidNetPayment(java.math.BigDecimal paidNetPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusByPaidNetPayment
	 *
	 */
	public Set<PayrollPaymentStatus> findPayrollPaymentStatusByPaidNetPayment(BigDecimal paidNetPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollPaymentStatuss
	 *
	 */
	public Set<PayrollPaymentStatus> findAllPayrollPaymentStatuss() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollPaymentStatuss
	 *
	 */
	public Set<PayrollPaymentStatus> findAllPayrollPaymentStatuss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusById
	 *
	 */
	public PayrollPaymentStatus findPayrollPaymentStatusById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentStatusById
	 *
	 */
	public PayrollPaymentStatus findPayrollPaymentStatusById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

}