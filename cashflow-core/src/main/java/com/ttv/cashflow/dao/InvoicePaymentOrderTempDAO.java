
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.InvoicePaymentOrderTemp;

import java.math.BigInteger;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage InvoicePaymentOrderTemp entities.
 * 
 */
public interface InvoicePaymentOrderTempDAO extends JpaDao<InvoicePaymentOrderTemp> {

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNoContaining
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNoContaining
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempById
	 *
	 */
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempById
	 *
	 */
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllInvoicePaymentOrderTemps
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps() throws DataAccessException;

	/**
	 * JPQL Query - findAllInvoicePaymentOrderTemps
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByApInvoiceId
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByApInvoiceId(BigInteger apInvoiceId) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByApInvoiceId
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByApInvoiceId(BigInteger apInvoiceId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNo
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNo(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNo
	 *
	 */
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNo(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPrimaryKey
	 *
	 */
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPrimaryKey
	 *
	 */
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

}