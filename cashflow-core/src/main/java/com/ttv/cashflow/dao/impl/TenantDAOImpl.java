
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.TenantDAO;
import com.ttv.cashflow.domain.Tenant;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Tenant entities.
 * 
 */
@Repository("TenantDAO")

@Transactional
public class TenantDAOImpl extends AbstractJpaDao<Tenant> implements TenantDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Tenant.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new TenantDAOImpl
	 *
	 */
	public TenantDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findTenantByCodeContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByCodeContaining(String code) throws DataAccessException {

		return findTenantByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbUsername
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbUsername(String dbUsername) throws DataAccessException {

		return findTenantByDbUsername(dbUsername, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbUsername
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbUsername(String dbUsername, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbUsername", startResult, maxRows, dbUsername);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbPasswordContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbPasswordContaining(String dbPassword) throws DataAccessException {

		return findTenantByDbPasswordContaining(dbPassword, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbPasswordContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbPasswordContaining(String dbPassword, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbPasswordContaining", startResult, maxRows, dbPassword);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbNameContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbNameContaining(String dbName) throws DataAccessException {

		return findTenantByDbNameContaining(dbName, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbNameContaining(String dbName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbNameContaining", startResult, maxRows, dbName);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbPassword
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbPassword(String dbPassword) throws DataAccessException {

		return findTenantByDbPassword(dbPassword, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbPassword
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbPassword(String dbPassword, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbPassword", startResult, maxRows, dbPassword);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbUrl
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbUrl(String dbUrl) throws DataAccessException {

		return findTenantByDbUrl(dbUrl, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbUrl
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbUrl(String dbUrl, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbUrl", startResult, maxRows, dbUrl);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantById
	 *
	 */
	@Transactional
	public Tenant findTenantById(Integer id) throws DataAccessException {

		return findTenantById(id, -1, -1);
	}

	/**
	 * JPQL Query - findTenantById
	 *
	 */

	@Transactional
	public Tenant findTenantById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTenantById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Tenant) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTenantByCode
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByCode(String code) throws DataAccessException {

		return findTenantByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByCode", startResult, maxRows, code);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbName
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbName(String dbName) throws DataAccessException {

		return findTenantByDbName(dbName, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbName(String dbName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbName", startResult, maxRows, dbName);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbUrlContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbUrlContaining(String dbUrl) throws DataAccessException {

		return findTenantByDbUrlContaining(dbUrl, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbUrlContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbUrlContaining(String dbUrl, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbUrlContaining", startResult, maxRows, dbUrl);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbDriverClassName
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbDriverClassName(String dbDriverClassName) throws DataAccessException {

		return findTenantByDbDriverClassName(dbDriverClassName, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbDriverClassName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbDriverClassName(String dbDriverClassName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbDriverClassName", startResult, maxRows, dbDriverClassName);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByPrimaryKey
	 *
	 */
	@Transactional
	public Tenant findTenantByPrimaryKey(Integer id) throws DataAccessException {

		return findTenantByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByPrimaryKey
	 *
	 */

	@Transactional
	public Tenant findTenantByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTenantByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Tenant) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTenantByDbDriverClassNameContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbDriverClassNameContaining(String dbDriverClassName) throws DataAccessException {

		return findTenantByDbDriverClassNameContaining(dbDriverClassName, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbDriverClassNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbDriverClassNameContaining(String dbDriverClassName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbDriverClassNameContaining", startResult, maxRows, dbDriverClassName);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllTenants
	 *
	 */
	@Transactional
	public Set<Tenant> findAllTenants() throws DataAccessException {

		return findAllTenants(-1, -1);
	}

	/**
	 * JPQL Query - findAllTenants
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findAllTenants(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllTenants", startResult, maxRows);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantByDbUsernameContaining
	 *
	 */
	@Transactional
	public Set<Tenant> findTenantByDbUsernameContaining(String dbUsername) throws DataAccessException {

		return findTenantByDbUsernameContaining(dbUsername, -1, -1);
	}

	/**
	 * JPQL Query - findTenantByDbUsernameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tenant> findTenantByDbUsernameContaining(String dbUsername, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantByDbUsernameContaining", startResult, maxRows, dbUsername);
		return new LinkedHashSet<Tenant>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Tenant entity) {
		return true;
	}
}
