
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.Configuration;

import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Configuration entities.
 * 
 */
public interface ConfigurationDAO extends JpaDao<Configuration> {

	/**
	 * JPQL Query - findConfigurationByKeyContaining
	 *
	 */
	public Set<Configuration> findConfigurationByKeyContaining(String key) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByKeyContaining
	 *
	 */
	public Set<Configuration> findConfigurationByKeyContaining(String key, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByValue
	 *
	 */
	public Set<Configuration> findConfigurationByValue(String value) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByValue
	 *
	 */
	public Set<Configuration> findConfigurationByValue(String value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByValueContaining
	 *
	 */
	public Set<Configuration> findConfigurationByValueContaining(String value_1) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByValueContaining
	 *
	 */
	public Set<Configuration> findConfigurationByValueContaining(String value_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByPrimaryKey
	 *
	 */
	public Configuration findConfigurationByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByPrimaryKey
	 *
	 */
	public Configuration findConfigurationByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByKey
	 *
	 */
	public Set<Configuration> findConfigurationByKey(String key_1) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationByKey
	 *
	 */
	public Set<Configuration> findConfigurationByKey(String key_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllConfigurations
	 *
	 */
	public Set<Configuration> findAllConfigurations() throws DataAccessException;

	/**
	 * JPQL Query - findAllConfigurations
	 *
	 */
	public Set<Configuration> findAllConfigurations(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationById
	 *
	 */
	public Configuration findConfigurationById(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findConfigurationById
	 *
	 */
	public Configuration findConfigurationById(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * Find configuration by ids.
	 *
	 * @param ids the list of id
	 * @return the configuration
	 */
	public List<Configuration> findConfigurationByIds(Integer[] ids);
}