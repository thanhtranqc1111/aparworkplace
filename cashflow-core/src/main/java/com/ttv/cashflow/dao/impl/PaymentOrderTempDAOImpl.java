
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PaymentOrderTempDAO;
import com.ttv.cashflow.domain.PaymentOrderTemp;

/**
 * DAO to manage PaymentOrderTemp entities.
 * 
 */
@Repository("PaymentOrderTempDAO")

@Transactional
public class PaymentOrderTempDAOImpl extends AbstractJpaDao<PaymentOrderTemp> implements PaymentOrderTempDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PaymentOrderTemp.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PaymentOrderTempDAOImpl
	 *
	 */
	public PaymentOrderTempDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableName
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findPaymentOrderTempByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentOriginalAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentOriginalAmount(java.math.BigDecimal apInvoicePaymentOriginalAmount) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePaymentOriginalAmount(apInvoicePaymentOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentOriginalAmount(java.math.BigDecimal apInvoicePaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePaymentOriginalAmount", startResult, maxRows, apInvoicePaymentOriginalAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNoContaining(String apInvoicePayeeBankAccNo) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePayeeBankAccNoContaining(apInvoicePayeeBankAccNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNoContaining(String apInvoicePayeeBankAccNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePayeeBankAccNoContaining", startResult, maxRows, apInvoicePayeeBankAccNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining(String reimbursementOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining(reimbursementOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining(String reimbursementOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining", startResult, maxRows, reimbursementOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNo(String paymentApprovalNo) throws DataAccessException {

		return findPaymentOrderTempByPaymentApprovalNo(paymentApprovalNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNo(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPaymentApprovalNo", startResult, maxRows, paymentApprovalNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccount(String bankAccount) throws DataAccessException {

		return findPaymentOrderTempByBankAccount(bankAccount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccount(String bankAccount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankAccount", startResult, maxRows, bankAccount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentRate
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentRate(java.math.BigDecimal apInvoicePaymentRate) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePaymentRate(apInvoicePaymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentRate(java.math.BigDecimal apInvoicePaymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePaymentRate", startResult, maxRows, apInvoicePaymentRate);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findPaymentOrderTempByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentConvertedAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentConvertedAmount(java.math.BigDecimal payrollPaymentConvertedAmount) throws DataAccessException {

		return findPaymentOrderTempByPayrollPaymentConvertedAmount(payrollPaymentConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentConvertedAmount(java.math.BigDecimal payrollPaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollPaymentConvertedAmount", startResult, maxRows, payrollPaymentConvertedAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankNameContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankNameContaining(String apInvoicePayeeBankName) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePayeeBankNameContaining(apInvoicePayeeBankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankNameContaining(String apInvoicePayeeBankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePayeeBankNameContaining", startResult, maxRows, apInvoicePayeeBankName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccountContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccountContaining(String bankAccount) throws DataAccessException {

		return findPaymentOrderTempByBankAccountContaining(bankAccount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccountContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccountContaining(String bankAccount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankAccountContaining", startResult, maxRows, bankAccount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentConvertedAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentConvertedAmount(java.math.BigDecimal reimbursementPaymentConvertedAmount) throws DataAccessException {

		return findPaymentOrderTempByReimbursementPaymentConvertedAmount(reimbursementPaymentConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentConvertedAmount(java.math.BigDecimal reimbursementPaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementPaymentConvertedAmount", startResult, maxRows, reimbursementPaymentConvertedAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempById
	 *
	 */
	@Transactional
	public PaymentOrderTemp findPaymentOrderTempById(BigInteger id) throws DataAccessException {

		return findPaymentOrderTempById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempById
	 *
	 */

	@Transactional
	public PaymentOrderTemp findPaymentOrderTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPaymentOrderTempById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PaymentOrderTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPaymentOrderTempByDescription
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescription(String description) throws DataAccessException {

		return findPaymentOrderTempByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByDescription", startResult, maxRows, description);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPrimaryKey
	 *
	 */
	@Transactional
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPaymentOrderTempByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPrimaryKey
	 *
	 */

	@Transactional
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPaymentOrderTempByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PaymentOrderTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCode(String apInvoiceOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByApInvoiceOriginalCurrencyCode(apInvoiceOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCode(String apInvoiceOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoiceOriginalCurrencyCode", startResult, maxRows, apInvoiceOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentConvertedAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentConvertedAmount(java.math.BigDecimal apInvoicePaymentConvertedAmount) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePaymentConvertedAmount(apInvoicePaymentConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentConvertedAmount(java.math.BigDecimal apInvoicePaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePaymentConvertedAmount", startResult, maxRows, apInvoicePaymentConvertedAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescriptionContaining(String description) throws DataAccessException {

		return findPaymentOrderTempByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByTotalAmountConvertedAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByTotalAmountConvertedAmount(java.math.BigDecimal totalAmountConvertedAmount) throws DataAccessException {

		return findPaymentOrderTempByTotalAmountConvertedAmount(totalAmountConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByTotalAmountConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByTotalAmountConvertedAmount(java.math.BigDecimal totalAmountConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByTotalAmountConvertedAmount", startResult, maxRows, totalAmountConvertedAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNo(String payrollNo) throws DataAccessException {

		return findPaymentOrderTempByPayrollNo(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNo(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollNo", startResult, maxRows, payrollNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining(String apInvoiceOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining(apInvoiceOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining(String apInvoiceOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining", startResult, maxRows, apInvoiceOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNo(String reimbursementNo) throws DataAccessException {

		return findPaymentOrderTempByReimbursementNo(reimbursementNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNo(String reimbursementNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementNo", startResult, maxRows, reimbursementNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPaymentOrderTemps
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findAllPaymentOrderTemps() throws DataAccessException {

		return findAllPaymentOrderTemps(-1, -1);
	}

	/**
	 * JPQL Query - findAllPaymentOrderTemps
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findAllPaymentOrderTemps(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPaymentOrderTemps", startResult, maxRows);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByValueDate
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByValueDate(java.util.Calendar valueDate) throws DataAccessException {

		return findPaymentOrderTempByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByValueDate(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentRate
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentRate(java.math.BigDecimal payrollPaymentRate) throws DataAccessException {

		return findPaymentOrderTempByPayrollPaymentRate(payrollPaymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentRate(java.math.BigDecimal payrollPaymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollPaymentRate", startResult, maxRows, payrollPaymentRate);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNoContaining(String paymentApprovalNo) throws DataAccessException {

		return findPaymentOrderTempByPaymentApprovalNoContaining(paymentApprovalNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNoContaining(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPaymentApprovalNoContaining", startResult, maxRows, paymentApprovalNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findPaymentOrderTempByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentRate
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentRate(java.math.BigDecimal reimbursementPaymentRate) throws DataAccessException {

		return findPaymentOrderTempByReimbursementPaymentRate(reimbursementPaymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentRate(java.math.BigDecimal reimbursementPaymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementPaymentRate", startResult, maxRows, reimbursementPaymentRate);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNo(String bankRefNo) throws DataAccessException {

		return findPaymentOrderTempByBankRefNo(bankRefNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNo(String bankRefNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankRefNo", startResult, maxRows, bankRefNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankName
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankName(String apInvoicePayeeBankName) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePayeeBankName(apInvoicePayeeBankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankName(String apInvoicePayeeBankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePayeeBankName", startResult, maxRows, apInvoicePayeeBankName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentOriginalAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentOriginalAmount(java.math.BigDecimal reimbursementPaymentOriginalAmount) throws DataAccessException {

		return findPaymentOrderTempByReimbursementPaymentOriginalAmount(reimbursementPaymentOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentOriginalAmount(java.math.BigDecimal reimbursementPaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementPaymentOriginalAmount", startResult, maxRows, reimbursementPaymentOriginalAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNoContaining(String reimbursementNo) throws DataAccessException {

		return findPaymentOrderTempByReimbursementNoContaining(reimbursementNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNoContaining(String reimbursementNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementNoContaining", startResult, maxRows, reimbursementNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCode(String payrollOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByPayrollOriginalCurrencyCode(payrollOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCode(String payrollOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollOriginalCurrencyCode", startResult, maxRows, payrollOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankNameContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankNameContaining(String bankName) throws DataAccessException {

		return findPaymentOrderTempByBankNameContaining(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankNameContaining(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankNameContaining", startResult, maxRows, bankName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCode(String reimbursementOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByReimbursementOriginalCurrencyCode(reimbursementOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCode(String reimbursementOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByReimbursementOriginalCurrencyCode", startResult, maxRows, reimbursementOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankName
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankName(String bankName) throws DataAccessException {

		return findPaymentOrderTempByBankName(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankName(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankName", startResult, maxRows, bankName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining(String payrollOriginalCurrencyCode) throws DataAccessException {

		return findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining(payrollOriginalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining(String payrollOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining", startResult, maxRows, payrollOriginalCurrencyCode);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNoContaining(String bankRefNo) throws DataAccessException {

		return findPaymentOrderTempByBankRefNoContaining(bankRefNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNoContaining(String bankRefNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByBankRefNoContaining", startResult, maxRows, bankRefNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNoContaining(String apInvoiceNo) throws DataAccessException {

		return findPaymentOrderTempByApInvoiceNoContaining(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNoContaining(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoiceNoContaining", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNoContaining(String payrollNo) throws DataAccessException {

		return findPaymentOrderTempByPayrollNoContaining(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNoContaining(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollNoContaining", startResult, maxRows, payrollNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNo(String apInvoiceNo) throws DataAccessException {

		return findPaymentOrderTempByApInvoiceNo(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNo(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoiceNo", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findPaymentOrderTempByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNo
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNo(String apInvoicePayeeBankAccNo) throws DataAccessException {

		return findPaymentOrderTempByApInvoicePayeeBankAccNo(apInvoicePayeeBankAccNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNo(String apInvoicePayeeBankAccNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByApInvoicePayeeBankAccNo", startResult, maxRows, apInvoicePayeeBankAccNo);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentOriginalAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentOriginalAmount(java.math.BigDecimal payrollPaymentOriginalAmount) throws DataAccessException {

		return findPaymentOrderTempByPayrollPaymentOriginalAmount(payrollPaymentOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentOriginalAmount(java.math.BigDecimal payrollPaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderTempByPayrollPaymentOriginalAmount", startResult, maxRows, payrollPaymentOriginalAmount);
		return new LinkedHashSet<PaymentOrderTemp>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PaymentOrderTemp entity) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<String> findListIdPaymentOrderTempByHavingGroup() throws DataAccessException {
        
        String queryString = new StringBuilder()
        .append("SELECT ")
        .append("string_agg(cast(id as text), ',') AS listId ")
        .append("FROM payment_order_temp ")
        .append("GROUP BY bank_name, bank_account, value_date, total_amount_converted_amount ")
        .append("ORDER BY listId ")
        .toString();
        
        Query query = getEntityManager().createNativeQuery(queryString);

        return new ArrayList<String>(query.getResultList());
    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<PaymentOrderTemp> findPaymentOrderTempByListId(List<BigInteger> listId) throws DataAccessException {
	    
	    String queryString = new StringBuilder()
	    .append("select myPaymentOrderTemp from PaymentOrderTemp myPaymentOrderTemp WHERE id IN (?1)")
        .toString();
        
        Query query = getEntityManager().createQuery(queryString).setParameter(1, listId);
        
        return new ArrayList<PaymentOrderTemp>(query.getResultList());
    }
	
	@Transactional
    public void truncate() throws DataAccessException {
        getEntityManager().createNativeQuery("truncate table payment_order_temp").executeUpdate();
    }
	
	public BigDecimal totalApinvoiceConvertedAmount(String apInvoiceNo){
		 String queryString = new StringBuilder()
        .append("SELECT sum(total_amount_converted_amount) ")
        .append("FROM payment_order_temp ")
        .append("WHERE ap_invoice_no = ?1 ")
        .toString();
        Query query = getEntityManager().createNativeQuery(queryString)
        .setParameter(1, apInvoiceNo);
        
        BigDecimal ret = (BigDecimal) query.getSingleResult();
        return ret;
	}
	
	public BigDecimal totalPayrollConvertedAmount(String payrollNo){
		 String queryString = new StringBuilder()
       .append("SELECT sum(total_amount_converted_amount) ")
       .append("FROM payment_order_temp ")
       .append("WHERE payroll_no = ?1 ")
       .toString();
       Query query = getEntityManager().createNativeQuery(queryString)
       .setParameter(1, payrollNo);
       
       BigDecimal ret = (BigDecimal) query.getSingleResult();
       return ret;
	}
	
	public BigDecimal totalReimbursementConvertedAmount(String reimbursementNo){
		String queryString = new StringBuilder()
       .append("SELECT sum(total_amount_converted_amount) ")
       .append("FROM payment_order_temp ")
       .append("WHERE reimbursement_no = ?1 ")
       .toString();
       Query query = getEntityManager().createNativeQuery(queryString)
       .setParameter(1, reimbursementNo);
       
       BigDecimal ret = (BigDecimal) query.getSingleResult();
       return ret;
	}
}
