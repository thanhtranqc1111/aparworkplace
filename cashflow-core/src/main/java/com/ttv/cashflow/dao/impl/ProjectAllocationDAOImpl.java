
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.dao.ProjectAllocationDAO;
import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.dto.ProjectPaymentDetail;

/**
 * DAO to manage ProjectAllocation entities.
 * 
 */
@Repository("ProjectAllocationDAO")

@Transactional
public class ProjectAllocationDAOImpl extends AbstractJpaDao<ProjectAllocation> implements ProjectAllocationDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ProjectAllocation.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ProjectAllocationDAOImpl
	 *
	 */
	public ProjectAllocationDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findProjectAllocationByMonthBefore
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findProjectAllocationByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationByModifiedDate
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findProjectAllocationByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationByMonth
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonth(java.util.Calendar month) throws DataAccessException {

		return findProjectAllocationByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationByPrimaryKey
	 *
	 */
	@Transactional
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id) throws DataAccessException {

		return findProjectAllocationByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByPrimaryKey
	 *
	 */

	@Transactional
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findProjectAllocationByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ProjectAllocation) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findProjectAllocationByMonthAfter
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findProjectAllocationByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationById
	 *
	 */
	@Transactional
	public ProjectAllocation findProjectAllocationById(BigInteger id) throws DataAccessException {

		return findProjectAllocationById(id, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationById
	 *
	 */

	@Transactional
	public ProjectAllocation findProjectAllocationById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findProjectAllocationById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ProjectAllocation) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllProjectAllocations
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findAllProjectAllocations() throws DataAccessException {

		return findAllProjectAllocations(-1, -1);
	}

	/**
	 * JPQL Query - findAllProjectAllocations
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findAllProjectAllocations(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllProjectAllocations", startResult, maxRows);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationByCreatedDate
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findProjectAllocationByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectAllocationByPercentAllocation
	 *
	 */
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByPercentAllocation(java.math.BigDecimal percentAllocation) throws DataAccessException {

		return findProjectAllocationByPercentAllocation(percentAllocation, -1, -1);
	}

	/**
	 * JPQL Query - findProjectAllocationByPercentAllocation
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByPercentAllocation(java.math.BigDecimal percentAllocation, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByPercentAllocation", startResult, maxRows, percentAllocation);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ProjectAllocation entity) {
		return true;
	}

	@Override
	public Set<ProjectAllocation> findProjectAllocation(String employeeCode, Calendar fromMonth, Calendar toMonth) {
		StringBuilder queryString = new StringBuilder("SELECT myProjectAllocation "
													  + "FROM ProjectAllocation myProjectAllocation "
													  + "WHERE myProjectAllocation.month >= ?1 and myProjectAllocation.month <= ?2 ");
		if(!StringUtils.isEmpty(employeeCode)) {
			queryString.append("and myProjectAllocation.employeeMaster.code = ?3");
		}
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter(1, fromMonth);
		query.setParameter(2, toMonth);
		if(!StringUtils.isEmpty(employeeCode)) {
			query.setParameter(3, employeeCode);
		}
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectAllocation> findProjectAllocationByRangeMonth(java.util.Calendar from, java.util.Calendar to) throws DataAccessException {
		Query query = createNamedQuery("findProjectAllocationByRangeMonth", null, null, from, to);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	@Override
	public Set<ProjectAllocation> findProjectAllocationByMonthAndEmployeeId(Calendar month, BigInteger employeeId) {
		Query query = createNamedQuery("findProjectAllocationByMonthAndEmployeeId", null, null, month, employeeId);
		return new LinkedHashSet<ProjectAllocation>(query.getResultList());
	}

	@Override
	public List<ProjectPaymentDetail> getListPaymentProjectByMonth(String month) {

		StoredProcedureQuery query  = getEntityManager().createNamedStoredProcedureQuery("get_list_payment_project");

		query.setParameter(2, month);
		
		query.execute();

		return (List<ProjectPaymentDetail>) query.getResultList();
	}
	

}
