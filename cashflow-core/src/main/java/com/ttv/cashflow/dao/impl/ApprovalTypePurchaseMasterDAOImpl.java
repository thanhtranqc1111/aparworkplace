
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApprovalTypePurchaseMasterDAO;
import com.ttv.cashflow.domain.ApprovalTypePurchaseMaster;

/**
 * DAO to manage ApprovalTypePurchaseMaster entities.
 * 
 */
@Repository("ApprovalTypePurchaseMasterDAO")

@Transactional
public class ApprovalTypePurchaseMasterDAOImpl extends AbstractJpaDao<ApprovalTypePurchaseMaster> implements ApprovalTypePurchaseMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myApprovalTypeMaster.code");
    	mapColumnIndex.put(2, "myApprovalTypeMaster.name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApprovalTypePurchaseMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApprovalTypePurchaseMasterDAOImpl
	 *
	 */
	public ApprovalTypePurchaseMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllApprovalTypePurchaseMasters
	 *
	 */
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters() throws DataAccessException {

		return findAllApprovalTypePurchaseMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllApprovalTypePurchaseMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApprovalTypePurchaseMasters", startResult, maxRows);
		return new LinkedHashSet<ApprovalTypePurchaseMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByPrimaryKey
	 *
	 */
	@Transactional
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code) throws DataAccessException {

		return findApprovalTypePurchaseMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByPrimaryKey
	 *
	 */

	@Transactional
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalTypePurchaseMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ApprovalTypePurchaseMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByName
	 *
	 */
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByName(String name) throws DataAccessException {

		return findApprovalTypePurchaseMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypePurchaseMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<ApprovalTypePurchaseMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByCodeContaining(String code) throws DataAccessException {

		return findApprovalTypePurchaseMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypePurchaseMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<ApprovalTypePurchaseMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByNameContaining(String name) throws DataAccessException {

		return findApprovalTypePurchaseMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypePurchaseMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<ApprovalTypePurchaseMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCode
	 *
	 */
	@Transactional
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByCode(String code) throws DataAccessException {

		return findApprovalTypePurchaseMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCode
	 *
	 */

	@Transactional
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalTypePurchaseMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ApprovalTypePurchaseMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApprovalTypePurchaseMaster entity) {
		return true;
	}

	@Override
	public int countApprovalTypeAccountMasterFilter(Integer start, Integer end, String keyword) {
		Query query = createNamedQuery("countApprovalTypePurchaseMasterPaging", start, end, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public int countApprovalTypeAccountMasterAll() {
		Query query = createNamedQuery("countApprovalTypePurchaseMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public Set<ApprovalTypePurchaseMaster> findApprovalTypeAccountMasterPaging(Integer start, Integer end,
			Integer orderColumn, String orderBy, String keyword) {
		Query query = createQuery("select myApprovalTypeMaster from ApprovalTypePurchaseMaster myApprovalTypeMaster where lower(myApprovalTypeMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   				+ "lower(myApprovalTypeMaster.name) like lower(CONCAT('%',?1, '%'))" + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<ApprovalTypePurchaseMaster>(query.getResultList());
	}
	
	
}
