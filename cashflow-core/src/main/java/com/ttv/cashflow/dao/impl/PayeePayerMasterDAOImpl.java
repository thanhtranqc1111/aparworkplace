
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.dao.PayeePayerMasterDAO;
import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;
import com.ttv.cashflow.domain.PayeePayerMaster;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;

/**
 * DAO to manage PayeePayerMaster entities.
 * 
 */
@Repository("PayeePayerMasterDAO")

@Transactional
public class PayeePayerMasterDAOImpl extends AbstractJpaDao<PayeePayerMaster> implements PayeePayerMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
	static {
		mapColumnIndex.put(1, "myPayeePayerMaster.code");
		mapColumnIndex.put(2, "myPayeePayerMaster.name");
		mapColumnIndex.put(6, "myPayeePayerMaster.bankName");
		mapColumnIndex.put(7, "myPayeePayerMaster.paymentTerm");
		mapColumnIndex.put(8, "myPayeePayerMaster.phone");
		mapColumnIndex.put(9, "myPayeePayerMaster.address");
	}
	@Autowired
	private BankAccountMasterDAO bankAccountMasterDAO;
	@Autowired
	private PayeePayerTypeMasterDAO payeePayerTypeMasterDAO;
	@Autowired
	private AccountMasterDAO accountMasterDAO;
	/**
	 * Set of entity classes managed by this DAO. Typically a DAO manages a single
	 * entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] { PayeePayerMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayeePayerMasterDAOImpl
	 *
	 */
	public PayeePayerMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountAndName(String keyword) {

		Query query = createNamedQuery("findPayeePayerMasterByAccountAndNameContaining", -1, -1, "%" + keyword + "%");
		List<Object[]> resultList = query.getResultList();
		Set<PayeePayerMaster> results = new LinkedHashSet<PayeePayerMaster>();
		PayeePayerMaster payee;
		for (Object[] result : resultList) {
			payee = (PayeePayerMaster) result[0];
			payee.setPayeePayerTypeMaster((PayeePayerTypeMaster) result[1]);
			results.add(payee);
		}
		return results;
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPrimaryKey
	 *
	 */
	@Transactional
	public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code) throws DataAccessException {

		return findPayeePayerMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPrimaryKey
	 *
	 */

	@Transactional
	public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayeePayerMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.PayeePayerMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTerm
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTerm(String paymentTerm) throws DataAccessException {

		return findPayeePayerMasterByPaymentTerm(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTerm
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTerm(String paymentTerm, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByPaymentTerm", startResult, maxRows, paymentTerm);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByCodeContaining(String code) throws DataAccessException {

		return findPayeePayerMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByCodeContaining(String code, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTermContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTermContaining(String paymentTerm)
			throws DataAccessException {

		return findPayeePayerMasterByPaymentTermContaining(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTermContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTermContaining(String paymentTerm, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByPaymentTermContaining", startResult, maxRows,
				paymentTerm);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPhoneContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPhoneContaining(String phone) throws DataAccessException {

		return findPayeePayerMasterByPhoneContaining(phone, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPhoneContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPhoneContaining(String phone, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByPhoneContaining", startResult, maxRows, phone);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByNameContaining(String name) throws DataAccessException {

		return findPayeePayerMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByNameContaining(String name, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCodeContaining(String bankAccountCode)
			throws DataAccessException {

		return findPayeePayerMasterByBankAccountCodeContaining(bankAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCodeContaining(String bankAccountCode,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByBankAccountCodeContaining", startResult, maxRows,
				bankAccountCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayeePayerMasters
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findAllPayeePayerMasters() throws DataAccessException {

		return findAllPayeePayerMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayeePayerMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findAllPayeePayerMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayeePayerMasters", startResult, maxRows);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAddressContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAddressContaining(String address) throws DataAccessException {

		return findPayeePayerMasterByAddressContaining(address, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAddressContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAddressContaining(String address, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByAddressContaining", startResult, maxRows, address);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankName
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankName(String bankName) throws DataAccessException {

		return findPayeePayerMasterByBankName(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankName(String bankName, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByBankName", startResult, maxRows, bankName);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByName
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByName(String name) throws DataAccessException {

		return findPayeePayerMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByName(String name, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCodeContaining(String accountCode)
			throws DataAccessException {

		return findPayeePayerMasterByAccountCodeContaining(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCodeContaining(String accountCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByAccountCodeContaining", startResult, maxRows,
				accountCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCode
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCode(String typeCode) throws DataAccessException {

		return findPayeePayerMasterByTypeCode(typeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCode(String typeCode, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByTypeCode", startResult, maxRows, typeCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPhone
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPhone(String phone) throws DataAccessException {

		return findPayeePayerMasterByPhone(phone, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByPhone
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByPhone(String phone, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByPhone", startResult, maxRows, phone);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCodeContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCodeContaining(String typeCode) throws DataAccessException {

		return findPayeePayerMasterByTypeCodeContaining(typeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCodeContaining(String typeCode, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByTypeCodeContaining", startResult, maxRows, typeCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAddress
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAddress(String address) throws DataAccessException {

		return findPayeePayerMasterByAddress(address, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAddress
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAddress(String address, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByAddress", startResult, maxRows, address);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCode
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCode(String bankAccountCode)
			throws DataAccessException {

		return findPayeePayerMasterByBankAccountCode(bankAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCode(String bankAccountCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByBankAccountCode", startResult, maxRows, bankAccountCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByCode
	 *
	 */
	@Transactional
	public PayeePayerMaster findPayeePayerMasterByCode(String code) throws DataAccessException {

		return findPayeePayerMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByCode
	 *
	 */

	@Transactional
	public PayeePayerMaster findPayeePayerMasterByCode(String code, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayeePayerMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.PayeePayerMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return new PayeePayerMaster();
		}
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankNameContaining
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankNameContaining(String bankName) throws DataAccessException {

		return findPayeePayerMasterByBankNameContaining(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByBankNameContaining(String bankName, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByBankNameContaining", startResult, maxRows, bankName);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCode
	 *
	 */
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCode(String accountCode) throws DataAccessException {

		return findPayeePayerMasterByAccountCode(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCode(String accountCode, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findPayeePayerMasterByAccountCode", startResult, maxRows, accountCode);
		return new LinkedHashSet<PayeePayerMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity
	 * when calling Store
	 * 
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PayeePayerMaster entity) {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayeePayerMaster> findPayeeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) throws DataAccessException {
		Query query = createQuery(
				"select myPayeePayerMaster "
						+ "from PayeePayerMaster myPayeePayerMaster "
						+ "where "
						+ "(lower(myPayeePayerMaster.code) like lower(CONCAT('%', ?1, '%')) or "
						+ "lower(myPayeePayerMaster.name) like lower(CONCAT('%', ?1, '%')) or "
						+ "lower(myPayeePayerMaster.address) like lower(CONCAT('%', ?1, '%')) or "
						+ "lower(myPayeePayerMaster.phone) like lower(CONCAT('%', ?1, '%')))" + " order by "
						+ mapColumnIndex.get(orderColumn) + " " + orderBy,
				start, end);
		query.setParameter(1, keyword);
		Set<PayeePayerMaster> payeePayerMasters = new LinkedHashSet<PayeePayerMaster>(query.getResultList());
		for (PayeePayerMaster item : payeePayerMasters) {
			if(!StringUtils.isEmpty(item.getTypeCode())) {
				item.setPayeePayerTypeMaster(payeePayerTypeMasterDAO.findPayeePayerTypeMasterByPrimaryKey(item.getTypeCode()));
			}
			if (!StringUtils.isEmpty(item.getBankAccountCode())) {
				item.setBankAccountMaster(bankAccountMasterDAO.findBankAccountMasterByCode(item.getBankAccountCode()));
			}
			if(!StringUtils.isEmpty(item.getAccountCode())) {
				item.setAccountMaster(accountMasterDAO.findAccountMasterByPrimaryKey(item.getAccountCode()));
			}
		}
		return payeePayerMasters;
	}

	public int countPayeeMasterFilter(Integer start, Integer length, Integer orderColumn, String orderBy,
			String keyword) {

		return findPayeeMasterPaging(-1, -1, orderColumn, orderBy, keyword).size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PayeePayerMaster> findTransactionPartyMastersByTransactionType(String transactionType) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT payeePayerMaster.* ");
		queryString.append(
				"FROM payee_payer_master payeePayerMaster INNER JOIN payee_payer_type_master payeePayerTypeMaster ");
		queryString.append("ON payeePayerMaster.type_code = payeePayerTypeMaster.code ");
		queryString.append("AND LOWER(payeePayerTypeMaster.name) = LOWER(?1)");

		Query query = getEntityManager().createNativeQuery(queryString.toString(), PayeePayerMaster.class);
		query.setParameter(1, transactionType);

		return (List<PayeePayerMaster>) query.getResultList();
	}

	@Override
	public boolean checkExistingCustomerName(String customerName) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT count(1) ");
		queryString.append(
				"FROM payee_payer_master payeePayerMaster INNER JOIN payee_payer_type_master payeePayerTypeMaster ");
		queryString.append("ON payeePayerMaster.type_code = payeePayerTypeMaster.code ");
		queryString.append("AND LOWER(payeePayerMaster.name) = LOWER(?1)");

		Query query = getEntityManager().createNativeQuery(queryString.toString());
		query.setParameter(1, customerName);

		return ((BigInteger) query.getSingleResult()).intValue() > 0;
	}
}
