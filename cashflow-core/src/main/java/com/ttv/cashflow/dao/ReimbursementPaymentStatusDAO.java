
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ReimbursementPaymentStatus;

/**
 * DAO to manage ReimbursementPaymentStatus entities.
 * 
 */
public interface ReimbursementPaymentStatusDAO extends JpaDao<ReimbursementPaymentStatus> {

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPrimaryKey
	 *
	 */
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPrimaryKey
	 *
	 */
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementPaymentStatuss
	 *
	 */
	public Set<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss() throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementPaymentStatuss
	 *
	 */
	public Set<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByReimbursementId
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByReimbursementId(BigInteger reimbursementId) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByReimbursementId
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByReimbursementId(BigInteger reimbursementId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPaidReimbursementOriginalPayment
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByPaidReimbursementOriginalPayment(java.math.BigDecimal paidReimbursementOriginalPayment) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPaidReimbursementOriginalPayment
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByPaidReimbursementOriginalPayment(BigDecimal paidReimbursementOriginalPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment(java.math.BigDecimal unpaidReimbursementOriginalPayment) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment
	 *
	 */
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment(BigDecimal unpaidReimbursementOriginalPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusById
	 *
	 */
	public ReimbursementPaymentStatus findReimbursementPaymentStatusById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentStatusById
	 *
	 */
	public ReimbursementPaymentStatus findReimbursementPaymentStatusById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

}