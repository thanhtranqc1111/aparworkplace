
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PaymentOrderTemp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PaymentOrderTemp entities.
 * 
 */
public interface PaymentOrderTempDAO extends JpaDao<PaymentOrderTemp> {

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableName(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentOriginalAmount(java.math.BigDecimal apInvoicePaymentOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentOriginalAmount(BigDecimal apInvoicePaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNoContaining(String apInvoicePayeeBankAccNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNoContaining(String apInvoicePayeeBankAccNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining(String reimbursementOriginalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCodeContaining(String reimbursementOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNo(String paymentApprovalNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNo(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccount(String bankAccount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccount(String bankAccount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentRate(java.math.BigDecimal apInvoicePaymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentRate(BigDecimal apInvoicePaymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentConvertedAmount(java.math.BigDecimal payrollPaymentConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentConvertedAmount(BigDecimal payrollPaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankNameContaining(String apInvoicePayeeBankName) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankNameContaining(String apInvoicePayeeBankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccountContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccountContaining(String bankAccount_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankAccountContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankAccountContaining(String bankAccount_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentConvertedAmount(java.math.BigDecimal reimbursementPaymentConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentConvertedAmount(BigDecimal reimbursementPaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempById
	 *
	 */
	public PaymentOrderTemp findPaymentOrderTempById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempById
	 *
	 */
	public PaymentOrderTemp findPaymentOrderTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByDescription
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByDescription
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPrimaryKey
	 *
	 */
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPrimaryKey
	 *
	 */
	public PaymentOrderTemp findPaymentOrderTempByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCode(String apInvoiceOriginalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCode(String apInvoiceOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentConvertedAmount(java.math.BigDecimal apInvoicePaymentConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePaymentConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePaymentConvertedAmount(BigDecimal apInvoicePaymentConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByDescriptionContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByDescriptionContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByTotalAmountConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByTotalAmountConvertedAmount(java.math.BigDecimal totalAmountConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByTotalAmountConvertedAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByTotalAmountConvertedAmount(BigDecimal totalAmountConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNo(String payrollNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNo(String payrollNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining(String apInvoiceOriginalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceOriginalCurrencyCodeContaining(String apInvoiceOriginalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNo(String reimbursementNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNo(String reimbursementNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPaymentOrderTemps
	 *
	 */
	public Set<PaymentOrderTemp> findAllPaymentOrderTemps() throws DataAccessException;

	/**
	 * JPQL Query - findAllPaymentOrderTemps
	 *
	 */
	public Set<PaymentOrderTemp> findAllPaymentOrderTemps(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByValueDate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByValueDate(java.util.Calendar valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByValueDate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByValueDate(Calendar valueDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentRate(java.math.BigDecimal payrollPaymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentRate(BigDecimal payrollPaymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNoContaining(String paymentApprovalNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPaymentApprovalNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPaymentApprovalNoContaining(String paymentApprovalNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCode(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableCode(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentRate(java.math.BigDecimal reimbursementPaymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentRate
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentRate(BigDecimal reimbursementPaymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNo(String bankRefNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNo(String bankRefNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankName(String apInvoicePayeeBankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankName(String apInvoicePayeeBankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentOriginalAmount(java.math.BigDecimal reimbursementPaymentOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementPaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementPaymentOriginalAmount(BigDecimal reimbursementPaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNoContaining(String reimbursementNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementNoContaining(String reimbursementNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCode(String payrollOriginalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCode(String payrollOriginalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankNameContaining(String bankName) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankNameContaining(String bankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCode(String reimbursementOriginalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByReimbursementOriginalCurrencyCode
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByReimbursementOriginalCurrencyCode(String reimbursementOriginalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankName(String bankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankName
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankName(String bankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining(String payrollOriginalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollOriginalCurrencyCodeContaining(String payrollOriginalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNoContaining(String bankRefNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByBankRefNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByBankRefNoContaining(String bankRefNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNoContaining(String apInvoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNoContaining(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNoContaining(String payrollNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollNoContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollNoContaining(String payrollNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNo(String apInvoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoiceNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoiceNo(String apInvoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableNameContaining(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByAccountPayableNameContaining
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByAccountPayableNameContaining(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNo(String apInvoicePayeeBankAccNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByApInvoicePayeeBankAccNo
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByApInvoicePayeeBankAccNo(String apInvoicePayeeBankAccNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentOriginalAmount(java.math.BigDecimal payrollPaymentOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderTempByPayrollPaymentOriginalAmount
	 *
	 */
	public Set<PaymentOrderTemp> findPaymentOrderTempByPayrollPaymentOriginalAmount(BigDecimal payrollPaymentOriginalAmount, int startResult, int maxRows) throws DataAccessException;
	
	public List<String> findListIdPaymentOrderTempByHavingGroup() throws DataAccessException;
	
	public List<PaymentOrderTemp> findPaymentOrderTempByListId(List<BigInteger> listId) throws DataAccessException;

	public void truncate() throws DataAccessException;
	
	public BigDecimal totalReimbursementConvertedAmount(String reimbursementNo);
	
	public BigDecimal totalPayrollConvertedAmount(String payrollNo);
	
	public BigDecimal totalApinvoiceConvertedAmount(String apInvoiceNo);
}