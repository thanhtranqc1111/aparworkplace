
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoiceDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.dto.InvoiceDetail;
import com.ttv.cashflow.dto.InvoicePayment;
import com.ttv.cashflow.dto.PaymentInvoice;

/**
 * DAO to manage ApInvoice entities.
 * 
 */
@Repository("ApInvoiceDAO")

@Transactional
public class ApInvoiceDAOImpl extends AbstractJpaDao<ApInvoice> implements ApInvoiceDAO {
	
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "ap_invoice_no");
    	mapColumnIndex.put(1, "month");
    }
    
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApInvoice.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoiceDAOImpl
	 *
	 */
	public ApInvoiceDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoiceByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByDescriptionContaining(String description) throws DataAccessException {

		return findApInvoiceByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApInvoices
	 *
	 */
	@Transactional
	public Set<ApInvoice> findAllApInvoices() throws DataAccessException {

		return findAllApInvoices(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoices
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findAllApInvoices(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoices", startResult, maxRows);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoiceByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoice findApInvoiceByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoice) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeCode(String payeeCode) throws DataAccessException {

		return findApInvoiceByPayeeCode(payeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeCode(String payeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeCode", startResult, maxRows, payeeCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByGstAmount(java.math.BigDecimal gstAmount) throws DataAccessException {

		return findApInvoiceByGstAmount(gstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByGstAmount(java.math.BigDecimal gstAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByGstAmount", startResult, maxRows, gstAmount);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByApInvoiceNoContaining(String apInvoiceNo) throws DataAccessException {

		return findApInvoiceByApInvoiceNoContaining(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByApInvoiceNoContaining(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByApInvoiceNoContaining", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByClaimType
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByClaimType(String claimType) throws DataAccessException {

		return findApInvoiceByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceNo
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findApInvoiceByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeName
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeName(String payeeTypeName) throws DataAccessException {

		return findApInvoiceByPayeeTypeName(payeeTypeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeName(String payeeTypeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeTypeName", startResult, maxRows, payeeTypeName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeNameContaining(String payeeName) throws DataAccessException {

		return findApInvoiceByPayeeNameContaining(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeNameContaining(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeNameContaining", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findApInvoiceByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPoNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPoNoContaining(String poNo) throws DataAccessException {

		return findApInvoiceByPoNoContaining(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPoNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPoNoContaining", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByExcludeGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByExcludeGstAmount(java.math.BigDecimal excludeGstAmount) throws DataAccessException {

		return findApInvoiceByExcludeGstAmount(excludeGstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByExcludeGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByExcludeGstAmount(java.math.BigDecimal excludeGstAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByExcludeGstAmount", startResult, maxRows, excludeGstAmount);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNo
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByApInvoiceNo(String apInvoiceNo) throws DataAccessException {

		return findApInvoiceByApInvoiceNo(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByApInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByApInvoiceNo(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByApInvoiceNo", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeCode(String payeeTypeCode) throws DataAccessException {

		return findApInvoiceByPayeeTypeCode(payeeTypeCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeCode(String payeeTypeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeTypeCode", startResult, maxRows, payeeTypeCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeCodeContaining(String payeeTypeCode) throws DataAccessException {

		return findApInvoiceByPayeeTypeCodeContaining(payeeTypeCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeCodeContaining(String payeeTypeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeTypeCodeContaining", startResult, maxRows, payeeTypeCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPoNo
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPoNo(String poNo) throws DataAccessException {

		return findApInvoiceByPoNo(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPoNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPoNo(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPoNo", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findApInvoiceByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByStatus
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByStatus(String status) throws DataAccessException {

		return findApInvoiceByStatus(status, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByStatus
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByStatus(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByStatus", startResult, maxRows, status);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findApInvoiceByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPaymentTerm
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPaymentTerm(String paymentTerm) throws DataAccessException {

		return findApInvoiceByPaymentTerm(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPaymentTerm
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPaymentTerm(String paymentTerm, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPaymentTerm", startResult, maxRows, paymentTerm);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findApInvoiceByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByDescription
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByDescription(String description) throws DataAccessException {

		return findApInvoiceByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableName
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findApInvoiceByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByFxRate
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findApInvoiceByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findApInvoiceByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByExcludeGstConvertedAmount", startResult, maxRows, excludeGstConvertedAmount);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByStatusContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByStatusContaining(String status) throws DataAccessException {

		return findApInvoiceByStatusContaining(status, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByStatusContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByStatusContaining(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByStatusContaining", startResult, maxRows, status);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByInpt
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByInpt(Integer inpt) throws DataAccessException {

		return findApInvoiceByInpt(inpt, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByInpt
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByInpt(Integer inpt, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByInpt", startResult, maxRows, inpt);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceIssuedDate
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findApInvoiceByInvoiceIssuedDate(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByInvoiceIssuedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByInvoiceIssuedDate", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByApprovalCode(String approvalCode) throws DataAccessException {

		return findApInvoiceByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeNameContaining(String payeeTypeName) throws DataAccessException {

		return findApInvoiceByPayeeTypeNameContaining(payeeTypeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeTypeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeTypeNameContaining(String payeeTypeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeTypeNameContaining", startResult, maxRows, payeeTypeName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByVatCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByVatCode(String vatCode) throws DataAccessException {

		return findApInvoiceByVatCode(vatCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByVatCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByVatCode(String vatCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByVatCode", startResult, maxRows, vatCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByInvTotalAmount
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvTotalAmount(java.math.BigDecimal invTotalAmount) throws DataAccessException {

		return findApInvoiceByInvTotalAmount(invTotalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByInvTotalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByInvTotalAmount(java.math.BigDecimal invTotalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByInvTotalAmount", startResult, maxRows, invTotalAmount);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceById
	 *
	 */
	@Transactional
	public ApInvoice findApInvoiceById(BigInteger id) throws DataAccessException {

		return findApInvoiceById(id, -1, -1); 
	}

	/**
	 * JPQL Query - findApInvoiceById
	 *
	 */

	@Transactional
	public ApInvoice findApInvoiceById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoice) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceByGst
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByGst(java.math.BigDecimal gst) throws DataAccessException {

		return findApInvoiceByGst(gst, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByGst
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByGst(java.math.BigDecimal gst, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByGst", startResult, maxRows, gst);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findApInvoiceByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPaymentTermContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPaymentTermContaining(String paymentTerm) throws DataAccessException {

		return findApInvoiceByPaymentTermContaining(paymentTerm, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPaymentTermContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPaymentTermContaining(String paymentTerm, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPaymentTermContaining", startResult, maxRows, paymentTerm);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findApInvoiceByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findApInvoiceByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByVatCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByVatCodeContaining(String vatCode) throws DataAccessException {

		return findApInvoiceByVatCodeContaining(vatCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByVatCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByVatCodeContaining(String vatCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByVatCodeContaining", startResult, maxRows, vatCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByScheduledPaymentDate
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findApInvoiceByScheduledPaymentDate(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByScheduledPaymentDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByScheduledPaymentDate", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeCodeContaining(String payeeCode) throws DataAccessException {

		return findApInvoiceByPayeeCodeContaining(payeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeCodeContaining(String payeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeCodeContaining", startResult, maxRows, payeeCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findApInvoiceByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeName
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeName(String payeeName) throws DataAccessException {

		return findApInvoiceByPayeeName(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByPayeeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByPayeeName(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByPayeeName", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByClaimTypeContaining(String claimType) throws DataAccessException {

		return findApInvoiceByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findApInvoiceByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoice> findApInvoiceByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<ApInvoice>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoice entity) {
		return true;
	}

	@SuppressWarnings("unchecked")
    @Transactional
    public List<InvoiceDetail> getInvoiceDetail(BigInteger apInvoiceId) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" aiad.id AS apInvoiceAccountDetailId,");
        builder.append(" aiad.account_code AS accountCode,");
        builder.append(" aiad.account_name AS accountName,");
        builder.append(" aicd.id AS apInvoiceClassDetailId,");
        builder.append(" aicd.class_code AS classCode,");
        builder.append(" aicd.class_name AS className,");
        builder.append(" aicd.description,");
        builder.append(" aicd.po_no AS poNo,");
        builder.append(" aicd.approval_code AS approvalCode,");
        builder.append(" aicd.exclude_gst_original_amount AS excludeGstOriginalAmount,");
        builder.append(" apiv.fx_rate as fxRate,");
        builder.append(" aicd.exclude_gst_converted_amount AS excludeGstConvertedAmount,");
        builder.append(" apiv.gst_type AS gstType,");
        builder.append(" apiv.gst_rate AS gstRate,");
        builder.append(" aicd.gst_original_amount AS gstOriginalAmount,");
        builder.append(" aicd.gst_converted_amount AS gstConvertedAmount,");
        builder.append(" aicd.include_gst_original_amount AS includeGstOriginalAmount,");
        builder.append(" aicd.include_gst_converted_amount AS includeGstConvertedAmount, ");
        builder.append(" aicd.is_approval AS isApproval ");
        //From
        builder.append(" FROM ap_invoice AS apiv");
        builder.append(" JOIN ap_invoice_account_details AS aiad");
        builder.append(" ON apiv.id = aiad.ap_invoice_id");
        builder.append(" JOIN ap_invoice_class_details AS aicd ");
        builder.append(" ON aiad.id = aicd.ap_invoice_account_details_id");
        //Where
        builder.append(" WHERE aiad.ap_invoice_id = :ap_invoice_id");
        //Order by
        builder.append(" ORDER BY aiad.created_date ASC, aicd.created_date ASC");
        
        
        Query query = getEntityManager().createNativeQuery(builder.toString(), "InvoiceDetailResult")
                                        .setParameter("ap_invoice_id", apInvoiceId);
        
        return query.getResultList();
        
    }
	
	
    @Transactional
    public BigInteger getLastId() {
        String sql = "SELECT id FROM ap_invoice ORDER BY id DESC LIMIT 1";

        Query query = getEntityManager().createNativeQuery(sql);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;

    }

    public BigInteger searchCount(String calFrom, String calTo, String invoice, String payeeName, String status, String fromApNo, String toApNo) {
    	String sql = "select count(distinct myApInvoice.ap_invoice_no) " 
    			+ "from ap_invoice myApInvoice "
				+ "left join ap_invoice_payment_details myApInvoicePaymentDetails on myApInvoicePaymentDetails.ap_invoice_id = myApInvoice.id "
				+ "left join payment_order myPaymentOrder on myPaymentOrder.id = myApInvoicePaymentDetails.payment_id "
				+ "where ((?1 = '') or myApInvoice.month >= to_timestamp(?1, 'MM/yyyy')) "
				+ "and ((?2 = '') or myApInvoice.month < to_timestamp(?2, 'MM/yyyy') + interval '1 day') "
            	+ "and ((?3 = '') or lower(myApInvoice.invoice_no) like lower(CONCAT('%', ?3, '%'))) "    
            	+ "and ((?4 = '') or lower(myApInvoice.payee_name) like lower(CONCAT('%', ?4, '%'))) "    
            	+ "and ((?5 = '') or lower(myApInvoice.status) like lower(CONCAT('%', ?5, '%'))) "
            	+ "and (?6 = '' or ?6 <= myApInvoice.ap_invoice_no) and (?7 = '' or ?7 >= myApInvoice.ap_invoice_no)";

    	Query query = getEntityManager().createNativeQuery(sql)
				.setParameter(1, calFrom)
				.setParameter(2, calTo)
				.setParameter(3, invoice)
				.setParameter(4, payeeName)
				.setParameter(5, status)
    			.setParameter(6, fromApNo)
    			.setParameter(7, toApNo);
    	
    	BigInteger count = (BigInteger) query.getSingleResult();
        return count;
    }
    
    @SuppressWarnings("unchecked")
    public List<InvoicePayment> searchInvoiceDetail(List<String> apInvoiceNoLst, Integer orderColumn, String orderBy) {
        
        StringBuilder builder = new StringBuilder()
	 	.append("SELECT ")
        .append("myPaymentOrder.id as paymentId, ")
        .append("myPaymentOrder.payment_order_no as paymentOrderNo, ")
        .append("myPaymentOrder.value_date as valueDate, ")
        .append("myPaymentOrder.bank_ref_no as bankRefNo, ")
        .append("myPaymentOrder.include_gst_original_amount as includeGstOriginalAmount, ")
        .append("myPaymentOrder.include_gst_converted_amount as includeGstConvertedAmount, ")
        .append("myPaymentOrder.status as paymentStatus, ")
		.append("myPaymentOrder.bank_name as bankName, ")
		.append("myPaymentOrder.bank_account as bankAccount, ")
		.append("myPaymentOrder.original_currency_code as paymentCurrency, ")
        .append("myApInvoice.id as apInvoiceId, ")
        .append("myApInvoice.ap_invoice_no as apInvoiceNo, ")
        .append("myApInvoice.payee_code as payeeCode, ")
        .append("myApInvoice.payee_name as payeeName, ")
		.append("myApInvoice.invoice_no as invoiceNo, ")
        .append("myApInvoice.id as apInvoiceId, ")
        .append("myApInvoice.include_gst_original_amount as invIncludeGstOriginalAmount, ")
        .append("myApInvoice.include_gst_converted_amount as invIncludeGstConvertedAmount, ")
        .append("myApInvoice.status as invoiceStatus, ")
		.append("myApInvoice.invoice_issued_date as apInvoiceDate, ")
		.append("accountMaster.name as payeeAccount, ")
		.append("myApInvoice.claim_type as claimType, ")
		.append("myApInvoice.month, ")
        .append("myApInvoice.original_currency_code as apInvoiceCurrency ")
        
        //form
        .append("FROM ap_invoice myApInvoice ")
        .append("left join ap_invoice_payment_details myApInvoicePaymentDetails on myApInvoicePaymentDetails.ap_invoice_id = myApInvoice.id ")
        .append("left join payment_order myPaymentOrder on myPaymentOrder.id = myApInvoicePaymentDetails.payment_id ")
        .append("left join payee_payer_master myPayeeMaster on  myApInvoice.payee_code = myPayeeMaster.code ")
        .append("left join account_master accountMaster on  accountMaster.code = myPayeeMaster.account_code ")
        
        //where
        .append("WHERE myApInvoice.ap_invoice_no IN (:ap_invoice_no) ");

		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myApInvoice.ap_invoice_no desc, myApInvoice.month desc");
		 }
		 
		 Query query = getEntityManager().createNativeQuery(builder.toString(), "findInvoicePaymentDetail")
                .setParameter("ap_invoice_no", apInvoiceNoLst.size() == 0 ? "" : apInvoiceNoLst);

		 return query.getResultList();
    }
    
    @Transactional
    public String getApInvoiceNo() {
        Query query = getEntityManager().createNativeQuery("SELECT ap_invoice_no()");

        String ret = (String) query.getSingleResult();

        return ret;

    }

    @Transactional
    public Boolean checkApprovalApInvoice(String apInvoiceNo) {
        Query query = getEntityManager().createNativeQuery("SELECT check_approval_ap_invoice(:apInvoiceNo)")
                                        .setParameter("apInvoiceNo", apInvoiceNo);

        Boolean ret = (Boolean) query.getSingleResult();

        return ret;

    }
    
    @Transactional
    public String getApInvoiceNo(Calendar cal) {
        Query query = getEntityManager().createNativeQuery("SELECT ap_invoice_no(?1)")
                                        .setParameter(1, cal);

        String ret = (String) query.getSingleResult();

        return ret;

    }
    
    @SuppressWarnings("unchecked")
    @Transactional
    public ApInvoice findApInvoiceByImportKey(String importKey) {
        ApInvoice apInvoice = null;
        
        String queryString = new StringBuilder()
        .append("select myApInvoice from ApInvoice myApInvoice where myApInvoice.importKey = ?1")
        .toString();
        
        Query query = getEntityManager().createQuery(queryString)
                                        .setParameter(1, importKey);
        
        List<ApInvoice> list = new ArrayList<ApInvoice>(query.getResultList());
        
        if(list.size() > 0){
            apInvoice = list.get(0);
        }
        
        return apInvoice;
    }
    
    @Transactional
	public ApInvoice findApInvoiceByApNo(String apInvoiceNo) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceByApInvoiceNo", -1, -1, apInvoiceNo);
			return (com.ttv.cashflow.domain.ApInvoice) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
    
    @Transactional
    public BigInteger countDetail(String apInvoiceNo) throws DataAccessException {
        String sql = new StringBuilder()
                    .append("SELECT count(api_cls.id) ")
                    .append("FROM ap_invoice api ")
                    .append("JOIN ap_invoice_account_details api_acc ON api_acc.ap_invoice_id = api.id ")
                    .append("JOIN ap_invoice_class_details api_cls ON api_cls.ap_invoice_account_details_id = api_acc.id ")
                    .append("WHERE api.ap_invoice_no = :apInvoiceNo ")
                    .toString();
        
        Query query = getEntityManager().createNativeQuery(sql)
                .setParameter("apInvoiceNo", apInvoiceNo);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;
    }
    
    @SuppressWarnings("unchecked")
    public List<PaymentInvoice> searchInvoice(Integer startResult, Integer maxRow, String calFrom, String calTo,
			String invoice, String payeeName, String status, String fromApNo, String toApNo, Integer orderColumn, String orderBy) {
    	StringBuilder builder = new StringBuilder();
    	builder.append("select myApInvoice.ap_invoice_no ")
    	.append("from ap_invoice myApInvoice ")
    	.append("left join ap_invoice_payment_details myApInvoicePaymentDetails on myApInvoicePaymentDetails.ap_invoice_id = myApInvoice.id ")
    	.append("left join payment_order myPaymentOrder on myPaymentOrder.id = myApInvoicePaymentDetails.payment_id ")
    	.append("where ((?1 = '') or myApInvoice.month >= to_timestamp(?1, 'MM/yyyy')) ")
    	.append("and ((?2 = '') or myApInvoice.month < to_timestamp(?2, 'MM/yyyy') + interval '1 day') ")
    	.append("and ((?3 = '') or lower(myApInvoice.invoice_no) like lower(CONCAT('%', ?3, '%'))) ")
    	.append("and ((?4 = '') or lower(myApInvoice.payee_name) like lower(CONCAT('%', ?4, '%'))) ")
    	.append("and ((?5 = '') or lower(myApInvoice.status) like lower(CONCAT('%', ?5, '%'))) ")
    	.append("and (?6 = '' or ?6 <= myApInvoice.ap_invoice_no) and (?7 = '' or ?7 >= myApInvoice.ap_invoice_no) ")
    	.append("group by myApInvoice.month, myApInvoice.ap_invoice_no ");

    	if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myApInvoice.ap_invoice_no desc, myApInvoice.month desc");
		 }
    	
		Query query = getEntityManager().createNativeQuery(builder.toString())
				.setParameter(1, calFrom)
				.setParameter(2, calTo)
				.setParameter(3, invoice)
				.setParameter(4, payeeName)
				.setParameter(5, status)
				.setParameter(6, fromApNo)
    			.setParameter(7, toApNo);
		
        query.setFirstResult(startResult == null || startResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : startResult);
        if (maxRow != null && maxRow > 0) {
            query.setMaxResults(maxRow);
        }
		
		return new ArrayList<PaymentInvoice>(query.getResultList());
	}
    
    @SuppressWarnings("unchecked")
	public boolean checkUniqueInvoiceNo(String invoiceNo, String id){
    	try {
			Query query = createNamedQuery("findApInvoiceByInvoiceNo", -1, -1, invoiceNo);
			List<ApInvoice> list = new ArrayList<ApInvoice>(query.getResultList());
	        
	        if(list.size() <= 0) {
	        	return false;
	        } else if(list.size() == 1){
	        	// check invoice no belong to ap invoice (id)
	        	if(id.equals(list.get(0).getId().toString())) {
					return false;
				} else {
					return true;
				}
	        } else {
	        	return true;
	        }
		} catch (NoResultException nre) {
			return false;
		}
    }
    
}
