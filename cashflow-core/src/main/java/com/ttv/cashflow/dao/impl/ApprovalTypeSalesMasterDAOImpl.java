
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApprovalTypeSalesMasterDAO;
import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;

/**
 * DAO to manage ApprovalTypeSalesMaster entities.
 * 
 */
@Repository("ApprovalTypeSalesMasterDAO")

@Transactional
public class ApprovalTypeSalesMasterDAOImpl extends AbstractJpaDao<ApprovalTypeSalesMaster> implements ApprovalTypeSalesMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myApprovalTypeMaster.code");
    	mapColumnIndex.put(2, "myApprovalTypeMaster.name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApprovalTypeSalesMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApprovalTypeSalesMasterDAOImpl
	 *
	 */
	public ApprovalTypeSalesMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllApprovalTypeSalesMasters
	 *
	 */
	@Transactional
	public Set<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters() throws DataAccessException {

		return findAllApprovalTypeSalesMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllApprovalTypeSalesMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApprovalTypeSalesMasters", startResult, maxRows);
		return new LinkedHashSet<ApprovalTypeSalesMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByName
	 *
	 */
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByName(String name) throws DataAccessException {

		return findApprovalTypeSalesMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypeSalesMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<ApprovalTypeSalesMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByNameContaining(String name) throws DataAccessException {

		return findApprovalTypeSalesMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypeSalesMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<ApprovalTypeSalesMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByCodeContaining(String code) throws DataAccessException {

		return findApprovalTypeSalesMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalTypeSalesMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<ApprovalTypeSalesMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCode
	 *
	 */
	@Transactional
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByCode(String code) throws DataAccessException {

		return findApprovalTypeSalesMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCode
	 *
	 */

	@Transactional
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalTypeSalesMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ApprovalTypeSalesMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByPrimaryKey
	 *
	 */
	@Transactional
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByPrimaryKey(String code) throws DataAccessException {

		return findApprovalTypeSalesMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByPrimaryKey
	 *
	 */

	@Transactional
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalTypeSalesMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ApprovalTypeSalesMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApprovalTypeSalesMaster entity) {
		return true;
	}

	@Override
	public int countApprovalTypeAccountMasterFilter(Integer start, Integer end, String keyword) {
		Query query = createNamedQuery("countApprovalTypeSalesMasterPaging", start, end, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public int countApprovalTypeAccountMasterAll() {
		Query query = createNamedQuery("countApprovalTypeSalesMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public Set<ApprovalTypeSalesMaster> findApprovalTypeAccountMasterPaging(Integer start, Integer end,
			Integer orderColumn, String orderBy, String keyword) {
		Query query = createQuery("select myApprovalTypeMaster from ApprovalTypeSalesMaster myApprovalTypeMaster where lower(myApprovalTypeMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   				+ "lower(myApprovalTypeMaster.name) like lower(CONCAT('%',?1, '%'))" + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<ApprovalTypeSalesMaster>(query.getResultList());
	}
	
}
