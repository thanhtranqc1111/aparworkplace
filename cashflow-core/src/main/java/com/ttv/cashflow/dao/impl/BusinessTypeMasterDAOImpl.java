
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.BusinessTypeMasterDAO;
import com.ttv.cashflow.domain.BusinessTypeMaster;

/**
 * DAO to manage BusinessTypeMaster entities.
 * 
 */
@Repository("BusinessTypeMasterDAO")

@Transactional
public class BusinessTypeMasterDAOImpl extends AbstractJpaDao<BusinessTypeMaster> implements BusinessTypeMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myBusinessTypeMaster.code");
    	mapColumnIndex.put(2, "myBusinessTypeMaster.name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			BusinessTypeMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new BusinessTypeMasterDAOImpl
	 *
	 */
	public BusinessTypeMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllBusinessTypeMasters
	 *
	 */
	@Transactional
	public Set<BusinessTypeMaster> findAllBusinessTypeMasters() throws DataAccessException {

		return findAllBusinessTypeMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllBusinessTypeMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BusinessTypeMaster> findAllBusinessTypeMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllBusinessTypeMasters", startResult, maxRows);
		return new LinkedHashSet<BusinessTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByCodeContaining(String code) throws DataAccessException {

		return findBusinessTypeMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBusinessTypeMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<BusinessTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByCode
	 *
	 */
	@Transactional
	public BusinessTypeMaster findBusinessTypeMasterByCode(String code) throws DataAccessException {

		return findBusinessTypeMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByCode
	 *
	 */

	@Transactional
	public BusinessTypeMaster findBusinessTypeMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBusinessTypeMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.BusinessTypeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByName
	 *
	 */
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByName(String name) throws DataAccessException {

		return findBusinessTypeMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBusinessTypeMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<BusinessTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByPrimaryKey
	 *
	 */
	@Transactional
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code) throws DataAccessException {

		return findBusinessTypeMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByPrimaryKey
	 *
	 */

	@Transactional
	public BusinessTypeMaster findBusinessTypeMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBusinessTypeMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.BusinessTypeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByNameContaining(String name) throws DataAccessException {

		return findBusinessTypeMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findBusinessTypeMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BusinessTypeMaster> findBusinessTypeMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBusinessTypeMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<BusinessTypeMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(BusinessTypeMaster entity) {
		return true;
	}

	public int countBusinessTypeMasterFilter(Integer start, Integer end, String keyword) {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countBusinessTypeMasterPaging", start, end, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countBusinessTypeMasterAll() {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countBusinessTypeMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<BusinessTypeMaster> findBusinessTypeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Query query = createQuery("select myBusinessTypeMaster from BusinessTypeMaster myBusinessTypeMaster where lower(myBusinessTypeMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   + "lower(myBusinessTypeMaster.name) like lower(CONCAT('%',?1, '%')) " + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<BusinessTypeMaster>(query.getResultList());
	}

    @Override
    public boolean checkExistingBusinessTypeCode(String businessTypeName) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM cashflow.business_type_master businessTypeMaster ");
        queryString.append("WHERE businessTypeMaster.name = :businessTypeName");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter("businessTypeName", businessTypeName);

        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
}
