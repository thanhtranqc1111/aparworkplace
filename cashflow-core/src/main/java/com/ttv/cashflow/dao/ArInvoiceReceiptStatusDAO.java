
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ArInvoiceReceiptStatus entities.
 * 
 */
public interface ArInvoiceReceiptStatusDAO extends JpaDao<ArInvoiceReceiptStatus> {

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount(java.math.BigDecimal unreceivedIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount(BigDecimal unreceivedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByPrimaryKey
	 *
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByPrimaryKey
	 *
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount(java.math.BigDecimal receivedIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount(BigDecimal receivedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceReceiptStatuss
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss() throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceReceiptStatuss
	 *
	 */
	public Set<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusById
	 *
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceReceiptStatusById
	 *
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;
	
	
	/**
	 * JPQL Query - findArInvoiceReceiptStatusByArInvoiceId
	 */
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByArInvoiceId(BigInteger arInvoiceId) throws DataAccessException;

}