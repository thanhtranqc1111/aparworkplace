
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ProjectAllocation;
import com.ttv.cashflow.dto.ProjectPaymentDetail;

/**
 * DAO to manage ProjectAllocation entities.
 * 
 */
public interface ProjectAllocationDAO extends JpaDao<ProjectAllocation> {

	/**
	 * JPQL Query - findProjectAllocationByMonthBefore
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonthBefore(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByMonthBefore
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonthBefore(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByModifiedDate
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByModifiedDate
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByMonth
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonth(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByMonth
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonth(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByPrimaryKey
	 *
	 */
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByPrimaryKey
	 *
	 */
	public ProjectAllocation findProjectAllocationByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByMonthAfter
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonthAfter(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByMonthAfter
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByMonthAfter(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationById
	 *
	 */
	public ProjectAllocation findProjectAllocationById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationById
	 *
	 */
	public ProjectAllocation findProjectAllocationById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllProjectAllocations
	 *
	 */
	public Set<ProjectAllocation> findAllProjectAllocations() throws DataAccessException;

	/**
	 * JPQL Query - findAllProjectAllocations
	 *
	 */
	public Set<ProjectAllocation> findAllProjectAllocations(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByCreatedDate
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByCreatedDate
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByPercentAllocation
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByPercentAllocation(java.math.BigDecimal percentAllocation) throws DataAccessException;

	/**
	 * JPQL Query - findProjectAllocationByPercentAllocation
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByPercentAllocation(BigDecimal percentAllocation, int startResult, int maxRows) throws DataAccessException;
	
	public Set<ProjectAllocation> findProjectAllocation(String employeeCode, Calendar fromMonth, Calendar toMonth);
	
	/**
	 * JPQL Query - findProjectAllocationByMonth
	 *
	 */
	public Set<ProjectAllocation> findProjectAllocationByRangeMonth(java.util.Calendar from, java.util.Calendar to) throws DataAccessException;
	
	public Set<ProjectAllocation> findProjectAllocationByMonthAndEmployeeId(Calendar month, BigInteger employeeId);
	
	/**
	 * JPQL Query - findPaymentProjectByMonth
	 *
	 */
	public List<ProjectPaymentDetail> getListPaymentProjectByMonth(String month);
}