
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.dao.ReimbursementDAO;
import com.ttv.cashflow.domain.Reimbursement;
import com.ttv.cashflow.dto.ReimbursementPaymentDTO;

/**
 * DAO to manage Reimbursement entities.
 * 
 */
@Repository("ReimbursementDAO")

@Transactional
public class ReimbursementDAOImpl extends AbstractJpaDao<Reimbursement> implements ReimbursementDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
	static {
    	mapColumnIndex.put(0, "re.reimbursement_no");
    	mapColumnIndex.put(1, "re.month");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Reimbursement.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ReimbursementDAOImpl
	 *
	 */
	public ReimbursementDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findReimbursementByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByDescriptionContaining(String description) throws DataAccessException {

		return findReimbursementByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByReimbursementNo
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByReimbursementNo(String reimbursementNo) throws DataAccessException {

		return findReimbursementByReimbursementNo(reimbursementNo, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByReimbursementNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByReimbursementNo(String reimbursementNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByReimbursementNo", startResult, maxRows, reimbursementNo);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDate
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findReimbursementByScheduledPaymentDate(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDate(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByScheduledPaymentDate", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByDescription
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByDescription(String description) throws DataAccessException {

		return findReimbursementByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByDescription", startResult, maxRows, description);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByStatus
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByStatus(String status) throws DataAccessException {

		return findReimbursementByStatus(status, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByStatus
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByStatus(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByStatus", startResult, maxRows, status);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findReimbursementByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementById
	 *
	 */
	@Transactional
	public Reimbursement findReimbursementById(BigInteger id) throws DataAccessException {

		return findReimbursementById(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementById
	 *
	 */

	@Transactional
	public Reimbursement findReimbursementById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Reimbursement) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeCode
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeCode(String employeeCode) throws DataAccessException {

		return findReimbursementByEmployeeCode(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeCode(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByEmployeeCode", startResult, maxRows, employeeCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByBookingDate
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDate(java.util.Calendar bookingDate) throws DataAccessException {

		return findReimbursementByBookingDate(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByBookingDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDate(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByBookingDate", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findReimbursementByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByPrimaryKey
	 *
	 */
	@Transactional
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id) throws DataAccessException {

		return findReimbursementByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByPrimaryKey
	 *
	 */

	@Transactional
	public Reimbursement findReimbursementByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Reimbursement) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountConverted
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountConverted(java.math.BigDecimal includeGstTotalAmountConverted) throws DataAccessException {

		return findReimbursementByIncludeGstTotalAmountConverted(includeGstTotalAmountConverted, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountConverted
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountConverted(java.math.BigDecimal includeGstTotalAmountConverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByIncludeGstTotalAmountConverted", startResult, maxRows, includeGstTotalAmountConverted);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeCodeContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeCodeContaining(String employeeCode) throws DataAccessException {

		return findReimbursementByEmployeeCodeContaining(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeCodeContaining(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByEmployeeCodeContaining", startResult, maxRows, employeeCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByClaimType
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByClaimType(String claimType) throws DataAccessException {

		return findReimbursementByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByMonthBefore
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findReimbursementByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findReimbursementByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByCreatedDate
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findReimbursementByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByMonth
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByMonth(java.util.Calendar month) throws DataAccessException {

		return findReimbursementByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByMonth", startResult, maxRows, month);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByMonthAfter
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findReimbursementByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeName
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeName(String employeeName) throws DataAccessException {

		return findReimbursementByEmployeeName(employeeName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeName(String employeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByEmployeeName", startResult, maxRows, employeeName);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findReimbursementByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByStatusContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByStatusContaining(String status) throws DataAccessException {

		return findReimbursementByStatusContaining(status, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByStatusContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByStatusContaining(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByStatusContaining", startResult, maxRows, status);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findReimbursementByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllReimbursements
	 *
	 */
	@Transactional
	public Set<Reimbursement> findAllReimbursements() throws DataAccessException {

		return findAllReimbursements(-1, -1);
	}

	/**
	 * JPQL Query - findAllReimbursements
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findAllReimbursements(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllReimbursements", startResult, maxRows);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountOriginal
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountOriginal(java.math.BigDecimal includeGstTotalAmountConverted) throws DataAccessException {

		return findReimbursementByIncludeGstTotalAmountOriginal(includeGstTotalAmountConverted, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByIncludeGstTotalAmountOriginal
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByIncludeGstTotalAmountOriginal(java.math.BigDecimal includeGstTotalAmountConverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByIncludeGstTotalAmountOriginal", startResult, maxRows, includeGstTotalAmountConverted);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateBefore
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findReimbursementByScheduledPaymentDateBefore(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateBefore(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByScheduledPaymentDateBefore", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByFxRate
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findReimbursementByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByBookingDateAfter
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDateAfter(java.util.Calendar bookingDate) throws DataAccessException {

		return findReimbursementByBookingDateAfter(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByBookingDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDateAfter(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByBookingDateAfter", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByBookingDateBefore
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDateBefore(java.util.Calendar bookingDate) throws DataAccessException {

		return findReimbursementByBookingDateBefore(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByBookingDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByBookingDateBefore(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByBookingDateBefore", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByClaimTypeContaining(String claimType) throws DataAccessException {

		return findReimbursementByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeNameContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeNameContaining(String employeeName) throws DataAccessException {

		return findReimbursementByEmployeeNameContaining(employeeName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByEmployeeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByEmployeeNameContaining(String employeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByEmployeeNameContaining", startResult, maxRows, employeeName);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByModifiedDate
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findReimbursementByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableName
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findReimbursementByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByReimbursementNoContaining
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByReimbursementNoContaining(String reimbursementNo) throws DataAccessException {

		return findReimbursementByReimbursementNoContaining(reimbursementNo, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByReimbursementNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByReimbursementNoContaining(String reimbursementNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByReimbursementNoContaining", startResult, maxRows, reimbursementNo);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateAfter
	 *
	 */
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate) throws DataAccessException {

		return findReimbursementByScheduledPaymentDateAfter(scheduledPaymentDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementByScheduledPaymentDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Reimbursement> findReimbursementByScheduledPaymentDateAfter(java.util.Calendar scheduledPaymentDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByScheduledPaymentDateAfter", startResult, maxRows, scheduledPaymentDate);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Reimbursement entity) {
		return true;
	}

	@Override
	public String getReimbursementNo() {
		Query query = getEntityManager().createNativeQuery("SELECT reimbursement_no()");		
		return (String) query.getSingleResult();
	}

	@Override
	public int countReimbursementFilter(Calendar fromMonth, Calendar toMonth, String fromReimbursementNo, String toReimbursementNo, String employeeCode, int status) {
		StringBuilder queryString = new StringBuilder("SELECT count(1) " + 
													  "FROM reimbursement re " + 
													  "WHERE lower(re.status) like lower(CONCAT('%', :status, '%')) " + 
													  "and (:fromReimbursementNo = '' or :fromReimbursementNo <= re.reimbursement_no) and (:toReimbursementNo = '' or :toReimbursementNo >= re.reimbursement_no) ");
		if (fromMonth != null) {
			queryString.append("and re.month >= cast(:fromMonth as date) ");
		}
		if (toMonth != null) {
			queryString.append("and re.month <= cast(:toMonth as date) ");
		}
		if (!StringUtils.isEmpty(employeeCode)) {
			queryString.append("and re.employee_code = :employeeCode ");
		}
		Query query = getEntityManager().createNativeQuery(queryString.toString());
		query.setParameter("status", status);
		query.setParameter("fromReimbursementNo", fromReimbursementNo);
		query.setParameter("toReimbursementNo", toReimbursementNo);
		if (fromMonth != null) {
			query.setParameter("fromMonth", fromMonth);
		}
		if (toMonth != null) {
			query.setParameter("toMonth", toMonth);
		}
		if (!StringUtils.isEmpty(employeeCode)) {
			query.setParameter("employeeCode", employeeCode);
		}
		return ((Number) query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReimbursementPaymentDTO> findReimbursementPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth, 
																String fromReimbursementNo, String toReimbursementNo, String employeeCode, Integer orderColumn, String orderBy, int status) {
		StringBuilder queryString = new StringBuilder("SELECT re.id, " + 
													"		 re.month, " + 
													"        re.reimbursement_no as reimbursementNo, " + 
													"        re.claim_type as claimType, " + 
													"        re.employee_name as employeeName, " +
													"        re.employee_code as employeeCode, " +
													"        re.invoice_no as invoiceNo, " +
													"        re.original_currency_code as originalCurrencyCode, " + 
													"        re.status as reimbursementStatus, " + 
													"        re.include_gst_total_converted_amount as includeGstTotalAmountConverted, " + 
													"        pm.payment_order_no as paymentOrderNo, " + 
													"        pm.value_date as valueDate, " + 
													"        pm.bank_name as bankName, " + 
													"        pm.bank_account as bankAccount, " + 
													"        pm.include_gst_converted_amount as includeGstConvertedAmount, " + 
													"        pm.bank_ref_no as bankRefNo, " + 
													"        pm.status as paymentOrderStatus " + 
													"FROM reimbursement re " + 
													"LEFT outer join reimbursement_payment_details pp on re.id = pp.reimbursement_id " + 
													"LEFT outer join payment_order pm on pm.id = pp.payment_id " +
													"WHERE re.reimbursement_no in (SELECT reimbursement_no FROM reimbursement pa WHERE lower(re.status) like lower(CONCAT('%', :status, '%')) " + 
																				  "and (:fromReimbursementNo = '' or :fromReimbursementNo <= re.reimbursement_no) and (:toReimbursementNo = '' or :toReimbursementNo >= re.reimbursement_no) ");
		if (fromMonth != null) {
			queryString.append("and re.month >= cast(:fromMonth as date) ");
		}
		if (toMonth != null) {
			queryString.append("and re.month <= cast(:toMonth as date) ");
		}
		if (!StringUtils.isEmpty(employeeCode)) {
			queryString.append("and re.employee_code = :employeeCode ");
		}
		if(orderColumn >= 0) {
			queryString.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy + " OFFSET :start LIMIT  " + (end == -1? "ALL" : ":end") +") order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		}
		else {
			queryString.append("order by re.reimbursement_no desc OFFSET :start LIMIT  " + (end == -1? "ALL" : ":end") +") order by re.reimbursement_no desc");
		}
		
		Query query = getEntityManager().createNativeQuery(queryString.toString(), "ReimbursementPaymentDTO");
		query.setParameter("status", status);
		query.setParameter("fromReimbursementNo", fromReimbursementNo);
		query.setParameter("toReimbursementNo", toReimbursementNo);
		if (fromMonth != null) {
			query.setParameter("fromMonth", fromMonth);
		}
		if (toMonth != null) {
			query.setParameter("toMonth", toMonth);
		}
		if (!StringUtils.isEmpty(employeeCode)) {
			query.setParameter("employeeCode", employeeCode);
		}
		if(start != -1 && end != -1) {
			query.setParameter("start", start);
			query.setParameter("end", end);
		}
		else {
			query.setParameter("start", 0);
		}
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
    @Transactional
    public Set<Reimbursement> searchReimbursementDetails(Object... parameters) throws DataAccessException {
        String queryString = new StringBuilder()
            .append("from Reimbursement reim")
            //Where
            .append(" where (?1 = '' or to_timestamp(?1, 'MM/yyyy') <= reim.month)")
            .append(" and (?2 = '' or to_timestamp(?2, 'MM/yyyy') >= reim.month)")
            .append(" and (?3 = '' or to_timestamp(?3, 'dd/MM/yyyy') <= reim.scheduledPaymentDate)")
            .append(" and (?4 = '' or to_timestamp(?4, 'dd/MM/yyyy') >= reim.scheduledPaymentDate)")
            .append(" and (?5 = '' or ?5 = reim.claimType)")
            .append(" and (?6 = '' or ?6 <> reim.status) and reim.status <> ?7 ")
            //Order by
            .append(" order by reim.reimbursementNo asc")
            .toString();
        
        Query query = createQuery(queryString, null, null, parameters);
        
        return new LinkedHashSet<Reimbursement>(query.getResultList());
    }
	
	@Transactional
    public Boolean checkApprovalReimbursement(String reimNo) {
        Query query = getEntityManager().createNativeQuery("SELECT check_approval_reimbursement(:reimNo)")
                                        .setParameter("reimNo", reimNo);

        Boolean ret = (Boolean) query.getSingleResult();
        
        return ret;
    }
	
	@Transactional
    public BigInteger countDetail(String reimbursementNo) throws DataAccessException {
        String sql = new StringBuilder()
            .append("SELECT count(rb.id) ")
            .append("FROM reimbursement rb ")
            .append("JOIN reimbursement_details rb_dt ON rb_dt.reimbursement_id = rb.id ")
            .append("WHERE rb.reimbursement_no = :reimbursementNo ")
            .toString();
        
        Query query = getEntityManager().createNativeQuery(sql)
                .setParameter("reimbursementNo", reimbursementNo);

        BigInteger ret = (BigInteger) query.getSingleResult();
        
        return ret;
	}

	@Override
	public Set<Reimbursement> findReimbursementByImportKey(String importKey) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementByImportKey", -1, -1, importKey);
		return new LinkedHashSet<Reimbursement>(query.getResultList());
	}

	@Override
	public BigDecimal getTotalAmountByReimbursementId(BigInteger reimbursementNoId) {
		try {
			StringBuilder queryString = new StringBuilder("select sum(a.payment_original_amount) as total " + 
														  "from reimbursement_payment_details a, payment_order p " + 
														  "where a.payment_id = p.id and p.status = '001' and a.reimbursement_id = ?1 " + 
														  "group by a.reimbursement_id");
			Query query = getEntityManager().createNativeQuery(queryString.toString()).setParameter(1, reimbursementNoId);
			return (BigDecimal)query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BigDecimal(0);
	}

	@Override
	public String getReimbursementNo(Calendar cal) {
		Query query = getEntityManager().createNativeQuery("SELECT reimbursement_no(?1)")
										.setParameter(1, cal);	
		return (String) query.getSingleResult();
	}
}
