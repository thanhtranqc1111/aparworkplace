
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoicePaymentDetails;

/**
 * DAO to manage ApInvoicePaymentDetails entities.
 * 
 */
public interface ApInvoicePaymentDetailsDAO extends JpaDao<ApInvoicePaymentDetails> {

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankName
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankName(String payeeBankName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankName
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankName(String payeeBankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNo
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNo(String payeeBankAccNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNo
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNo(String payeeBankAccNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsById
	 *
	 */
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsById
	 *
	 */
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentRate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentRate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPrimaryKey
	 *
	 */
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPrimaryKey
	 *
	 */
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankNameContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankNameContaining(String payeeBankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankNameContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankNameContaining(String payeeBankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCode
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCode(String varianceAccountCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCode
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCode(String varianceAccountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderConvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderConvertedAmount(java.math.BigDecimal paymentOrderConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderConvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderConvertedAmount(BigDecimal paymentOrderConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNoContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNoContaining(String payeeBankAccNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNoContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNoContaining(String payeeBankAccNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoicePaymentDetailss
	 *
	 */
	public Set<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoicePaymentDetailss
	 *
	 */
	public Set<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount(java.math.BigDecimal paymentOrderUnconvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount(BigDecimal paymentOrderUnconvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * 
	 * @param paymentId
	 * @return
	 */
	public List<ApInvoicePaymentDetails> findApInvPayDetailIdByPaymentId(BigInteger paymentId);
	public Set<ApInvoicePaymentDetails> findApInvPayDetailIdByPaymentIdInc(List<BigInteger> paymentIds);
	public BigDecimal getTotalPaymentOrderUnconvertedAmountByApInvoiceId(BigInteger apInvoiceId);
	
	/**
	 * get total original amount by ap invoice id without payment amount (total original amount - this payment original amount)
	 * @param paymentId
	 * @param apInvoiceId
	 * @return
	 */
	public BigDecimal getTotalOriginalAmountByApInvoiceId(BigInteger paymentId, BigInteger apInvoiceId);
}