
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.Role;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Permission entities.
 * 
 */
public interface PermissionDAO extends JpaDao<Permission> {

	/**
	 * JPQL Query - findPermissionByPrimaryKey
	 *
	 */
	public Permission findPermissionByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByPrimaryKey
	 *
	 */
	public Permission findPermissionByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByNameContaining
	 *
	 */
	public Set<Permission> findPermissionByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByNameContaining
	 *
	 */
	public Set<Permission> findPermissionByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByName
	 *
	 */
	public Set<Permission> findPermissionByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByName
	 *
	 */
	public Set<Permission> findPermissionByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByDescriptionContaining
	 *
	 */
	public Set<Permission> findPermissionByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByDescriptionContaining
	 *
	 */
	public Set<Permission> findPermissionByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionById
	 *
	 */
	public Permission findPermissionById(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionById
	 *
	 */
	public Permission findPermissionById(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByModifiedDate
	 *
	 */
	public Set<Permission> findPermissionByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByModifiedDate
	 *
	 */
	public Set<Permission> findPermissionByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPermissions
	 *
	 */
	public Set<Permission> findAllPermissions() throws DataAccessException;

	/**
	 * JPQL Query - findAllPermissions
	 *
	 */
	public Set<Permission> findAllPermissions(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByCreatedDate
	 *
	 */
	public Set<Permission> findPermissionByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByCreatedDate
	 *
	 */
	public Set<Permission> findPermissionByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByDescription
	 *
	 */
	public Set<Permission> findPermissionByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findPermissionByDescription
	 *
	 */
	public Set<Permission> findPermissionByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;
	public Set<Permission> findPermissionPaging(String keyword, int startResult, int maxRows) throws DataAccessException;
	public int countPermissionPaging(String keyword) throws DataAccessException;
}