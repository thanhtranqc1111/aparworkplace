
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.PayrollViewerDAO;
import com.ttv.cashflow.domain.PayrollViewer;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage PayrollViewer entities.
 * 
 */
@Repository("PayrollViewerDAO")

@Transactional
public class PayrollViewerDAOImpl extends AbstractJpaDao<PayrollViewer> implements PayrollViewerDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PayrollViewer.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayrollViewerDAOImpl
	 *
	 */
	public PayrollViewerDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2) throws DataAccessException {

		return findPayrollViewerByLaborAdditionalPayment2(laborAdditionalPayment2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborAdditionalPayment2", startResult, maxRows, laborAdditionalPayment2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2) throws DataAccessException {

		return findPayrollViewerByLaborSocialInsuranceEmployer2(laborSocialInsuranceEmployer2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborSocialInsuranceEmployer2", startResult, maxRows, laborSocialInsuranceEmployer2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDescriptionContaining(String description) throws DataAccessException {

		return findPayrollViewerByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployee2(deductionSocialInsuranceEmployee2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployee2", startResult, maxRows, deductionSocialInsuranceEmployee2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployer1(deductionSocialInsuranceEmployer1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployer1", startResult, maxRows, deductionSocialInsuranceEmployer1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByMonth
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonth(java.util.Calendar month) throws DataAccessException {

		return findPayrollViewerByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByMonth", startResult, maxRows, month);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCode
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByEmployeeCode(String employeeCode) throws DataAccessException {

		return findPayrollViewerByEmployeeCode(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByEmployeeCode(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByEmployeeCode", startResult, maxRows, employeeCode);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther3(java.math.BigDecimal deductionOther3) throws DataAccessException {

		return findPayrollViewerByDeductionOther3(deductionOther3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther3(java.math.BigDecimal deductionOther3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionOther3", startResult, maxRows, deductionOther3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findPayrollViewerByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByFxRate
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findPayrollViewerByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPayrollNo
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPayrollNo(String payrollNo) throws DataAccessException {

		return findPayrollViewerByPayrollNo(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPayrollNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPayrollNo(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPayrollNo", startResult, maxRows, payrollNo);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByMonthAfter
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findPayrollViewerByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayrollViewers
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findAllPayrollViewers() throws DataAccessException {

		return findAllPayrollViewers(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayrollViewers
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findAllPayrollViewers(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayrollViewers", startResult, maxRows);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployer2(deductionSocialInsuranceEmployer2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployer2", startResult, maxRows, deductionSocialInsuranceEmployer2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentConverted
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByNetPaymentConverted(java.math.BigDecimal netPaymentConverted) throws DataAccessException {

		return findPayrollViewerByNetPaymentConverted(netPaymentConverted, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentConverted
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByNetPaymentConverted(java.math.BigDecimal netPaymentConverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByNetPaymentConverted", startResult, maxRows, netPaymentConverted);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPrimaryKey
	 *
	 */
	@Transactional
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPayrollViewerByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPrimaryKey
	 *
	 */

	@Transactional
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollViewerByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1) throws DataAccessException {

		return findPayrollViewerByLaborAdditionalPayment1(laborAdditionalPayment1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborAdditionalPayment1", startResult, maxRows, laborAdditionalPayment1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDate
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDate(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollViewerByPaymentScheduleDate(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDate(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentScheduleDate", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalLaborCost
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalLaborCost(java.math.BigDecimal totalLaborCost) throws DataAccessException {

		return findPayrollViewerByTotalLaborCost(totalLaborCost, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalLaborCost
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalLaborCost(java.math.BigDecimal totalLaborCost, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTotalLaborCost", startResult, maxRows, totalLaborCost);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCodeContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByEmployeeCodeContaining(String employeeCode) throws DataAccessException {

		return findPayrollViewerByEmployeeCodeContaining(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByEmployeeCodeContaining(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByEmployeeCodeContaining", startResult, maxRows, employeeCode);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDivisionContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDivisionContaining(String division) throws DataAccessException {

		return findPayrollViewerByDivisionContaining(division, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDivisionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDivisionContaining(String division, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDivisionContaining", startResult, maxRows, division);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findPayrollViewerByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByValueDate
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByValueDate(java.math.BigDecimal valueDate) throws DataAccessException {

		return findPayrollViewerByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByValueDate(java.math.BigDecimal valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther1(java.math.BigDecimal deductionOther1) throws DataAccessException {

		return findPayrollViewerByDeductionOther1(deductionOther1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther1(java.math.BigDecimal deductionOther1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionOther1", startResult, maxRows, deductionOther1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDescription
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDescription(String description) throws DataAccessException {

		return findPayrollViewerByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDescription", startResult, maxRows, description);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3) throws DataAccessException {

		return findPayrollViewerByLaborAdditionalPayment3(laborAdditionalPayment3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborAdditionalPayment3", startResult, maxRows, laborAdditionalPayment3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2) throws DataAccessException {

		return findPayrollViewerByLaborOtherPayment2(laborOtherPayment2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborOtherPayment2", startResult, maxRows, laborOtherPayment2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployee3(deductionSocialInsuranceEmployee3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployee3", startResult, maxRows, deductionSocialInsuranceEmployee3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateAfter
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollViewerByPaymentScheduleDateAfter(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentScheduleDateAfter", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByRoleTitle
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByRoleTitle(String roleTitle) throws DataAccessException {

		return findPayrollViewerByRoleTitle(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByRoleTitle
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByRoleTitle", startResult, maxRows, roleTitle);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerById
	 *
	 */
	@Transactional
	public PayrollViewer findPayrollViewerById(BigInteger id) throws DataAccessException {

		return findPayrollViewerById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerById
	 *
	 */

	@Transactional
	public PayrollViewer findPayrollViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollViewerById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountOriginal
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountOriginal(java.math.BigDecimal paymentAmountOriginal) throws DataAccessException {

		return findPayrollViewerByPaymentAmountOriginal(paymentAmountOriginal, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountOriginal
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountOriginal(java.math.BigDecimal paymentAmountOriginal, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentAmountOriginal", startResult, maxRows, paymentAmountOriginal);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findPayrollViewerByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3) throws DataAccessException {

		return findPayrollViewerByLaborSocialInsuranceEmployer3(laborSocialInsuranceEmployer3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborSocialInsuranceEmployer3", startResult, maxRows, laborSocialInsuranceEmployer3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentOriginal
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByNetPaymentOriginal(java.math.BigDecimal netPaymentOriginal) throws DataAccessException {

		return findPayrollViewerByNetPaymentOriginal(netPaymentOriginal, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentOriginal
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByNetPaymentOriginal(java.math.BigDecimal netPaymentOriginal, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByNetPaymentOriginal", startResult, maxRows, netPaymentOriginal);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployer3(deductionSocialInsuranceEmployer3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployer3", startResult, maxRows, deductionSocialInsuranceEmployer3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findPayrollViewerByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentConverted
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentConverted(java.math.BigDecimal totalNetPaymentConverted) throws DataAccessException {

		return findPayrollViewerByTotalNetPaymentConverted(totalNetPaymentConverted, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentConverted
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentConverted(java.math.BigDecimal totalNetPaymentConverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTotalNetPaymentConverted", startResult, maxRows, totalNetPaymentConverted);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPayrollNoContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPayrollNoContaining(String payrollNo) throws DataAccessException {

		return findPayrollViewerByPayrollNoContaining(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPayrollNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPayrollNoContaining(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPayrollNoContaining", startResult, maxRows, payrollNo);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByRoleTitleContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByRoleTitleContaining(String roleTitle) throws DataAccessException {

		return findPayrollViewerByRoleTitleContaining(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByRoleTitleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByRoleTitleContaining(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByRoleTitleContaining", startResult, maxRows, roleTitle);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1) throws DataAccessException {

		return findPayrollViewerByLaborSocialInsuranceEmployer1(laborSocialInsuranceEmployer1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborSocialInsuranceEmployer1", startResult, maxRows, laborSocialInsuranceEmployer1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateBefore
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollViewerByPaymentScheduleDateBefore(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentScheduleDateBefore", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentOriginal
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentOriginal(java.math.BigDecimal totalNetPaymentOriginal) throws DataAccessException {

		return findPayrollViewerByTotalNetPaymentOriginal(totalNetPaymentOriginal, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentOriginal
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentOriginal(java.math.BigDecimal totalNetPaymentOriginal, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTotalNetPaymentOriginal", startResult, maxRows, totalNetPaymentOriginal);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentRemainAmount
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentRemainAmount(java.math.BigDecimal paymentRemainAmount) throws DataAccessException {

		return findPayrollViewerByPaymentRemainAmount(paymentRemainAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentRemainAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentRemainAmount(java.math.BigDecimal paymentRemainAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentRemainAmount", startResult, maxRows, paymentRemainAmount);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborBaseSalaryPayment
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment) throws DataAccessException {

		return findPayrollViewerByLaborBaseSalaryPayment(laborBaseSalaryPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborBaseSalaryPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborBaseSalaryPayment", startResult, maxRows, laborBaseSalaryPayment);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTeam
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTeam(String team) throws DataAccessException {

		return findPayrollViewerByTeam(team, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTeam
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTeam(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTeam", startResult, maxRows, team);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther2
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther2(java.math.BigDecimal deductionOther2) throws DataAccessException {

		return findPayrollViewerByDeductionOther2(deductionOther2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionOther2(java.math.BigDecimal deductionOther2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionOther2", startResult, maxRows, deductionOther2);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByMonthBefore
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findPayrollViewerByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1) throws DataAccessException {

		return findPayrollViewerByDeductionSocialInsuranceEmployee1(deductionSocialInsuranceEmployee1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionSocialInsuranceEmployee1", startResult, maxRows, deductionSocialInsuranceEmployee1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByClaimTypeContaining(String claimType) throws DataAccessException {

		return findPayrollViewerByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTeamContaining
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTeamContaining(String team) throws DataAccessException {

		return findPayrollViewerByTeamContaining(team, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTeamContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTeamContaining(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTeamContaining", startResult, maxRows, team);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionWht
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionWht(java.math.BigDecimal deductionWht) throws DataAccessException {

		return findPayrollViewerByDeductionWht(deductionWht, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDeductionWht
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDeductionWht(java.math.BigDecimal deductionWht, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDeductionWht", startResult, maxRows, deductionWht);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountConverted
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountConverted(java.math.BigDecimal paymentAmountConverted) throws DataAccessException {

		return findPayrollViewerByPaymentAmountConverted(paymentAmountConverted, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountConverted
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountConverted(java.math.BigDecimal paymentAmountConverted, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentAmountConverted", startResult, maxRows, paymentAmountConverted);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment3
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3) throws DataAccessException {

		return findPayrollViewerByLaborOtherPayment3(laborOtherPayment3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborOtherPayment3", startResult, maxRows, laborOtherPayment3);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentRate
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException {

		return findPayrollViewerByPaymentRate(paymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByPaymentRate(java.math.BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByPaymentRate", startResult, maxRows, paymentRate);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByClaimType
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByClaimType(String claimType) throws DataAccessException {

		return findPayrollViewerByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNo
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findPayrollViewerByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByDivision
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDivision(String division) throws DataAccessException {

		return findPayrollViewerByDivision(division, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByDivision
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByDivision(String division, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByDivision", startResult, maxRows, division);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalDeduction
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalDeduction(java.math.BigDecimal totalDeduction) throws DataAccessException {

		return findPayrollViewerByTotalDeduction(totalDeduction, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByTotalDeduction
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByTotalDeduction(java.math.BigDecimal totalDeduction, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByTotalDeduction", startResult, maxRows, totalDeduction);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment1
	 *
	 */
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1) throws DataAccessException {

		return findPayrollViewerByLaborOtherPayment1(laborOtherPayment1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollViewerByLaborOtherPayment1", startResult, maxRows, laborOtherPayment1);
		return new LinkedHashSet<PayrollViewer>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PayrollViewer entity) {
		return true;
	}
	
	@Transactional
    public void refresh() throws DataAccessException {
        getEntityManager().createNativeQuery("REFRESH MATERIALIZED VIEW payroll_viewer").executeUpdate();
    }
}
