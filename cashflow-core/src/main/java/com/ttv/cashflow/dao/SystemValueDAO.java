
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.SystemValue;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Query;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage SystemValue entities.
 * 
 */
public interface SystemValueDAO extends JpaDao<SystemValue> {

	/**
	 * JPQL Query - findAllSystemValues
	 *
	 */
	public Set<SystemValue> findAllSystemValues() throws DataAccessException;

	/**
	 * JPQL Query - findAllSystemValues
	 *
	 */
	public Set<SystemValue> findAllSystemValues(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByPrimaryKey
	 *
	 */
	public SystemValue findSystemValueByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByPrimaryKey
	 *
	 */
	public SystemValue findSystemValueByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByDescription
	 *
	 */
	public Set<SystemValue> findSystemValueByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByDescription
	 *
	 */
	public Set<SystemValue> findSystemValueByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByDescriptionContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByDescriptionContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByCode
	 *
	 */
	public Set<SystemValue> findSystemValueByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByCode
	 *
	 */
	public Set<SystemValue> findSystemValueByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByValueContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByValueContaining(String value) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByValueContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByValueContaining(String value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByType
	 *
	 */
	public Set<SystemValue> findSystemValueByType(String type) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByType
	 *
	 */
	public Set<SystemValue> findSystemValueByType(String type, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByCodeContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByCodeContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByValue
	 *
	 */
	public Set<SystemValue> findSystemValueByValue(String value_1) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByValue
	 *
	 */
	public Set<SystemValue> findSystemValueByValue(String value_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueById
	 *
	 */
	public SystemValue findSystemValueById(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueById
	 *
	 */
	public SystemValue findSystemValueById(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByTypeContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByTypeContaining(String type_1) throws DataAccessException;

	/**
	 * JPQL Query - findSystemValueByTypeContaining
	 *
	 */
	public Set<SystemValue> findSystemValueByTypeContaining(String type_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * JPQL Query - findSystemValueByTypeAndCode
	 */
	public SystemValue findSystemValueByTypeAndCode(String type, String code) throws DataAccessException;

}