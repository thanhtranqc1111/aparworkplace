
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.InvoicePaymentOrderTempDAO;
import com.ttv.cashflow.domain.InvoicePaymentOrderTemp;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage InvoicePaymentOrderTemp entities.
 * 
 */
@Repository("InvoicePaymentOrderTempDAO")

@Transactional
public class InvoicePaymentOrderTempDAOImpl extends AbstractJpaDao<InvoicePaymentOrderTemp>
		implements InvoicePaymentOrderTempDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			InvoicePaymentOrderTemp.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new InvoicePaymentOrderTempDAOImpl
	 *
	 */
	public InvoicePaymentOrderTempDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findInvoicePaymentOrderTempByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findInvoicePaymentOrderTempByPaymentOrderNoContaining", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<InvoicePaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempById
	 *
	 */
	@Transactional
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempById(BigInteger id) throws DataAccessException {

		return findInvoicePaymentOrderTempById(id, -1, -1);
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempById
	 *
	 */

	@Transactional
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findInvoicePaymentOrderTempById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.InvoicePaymentOrderTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllInvoicePaymentOrderTemps
	 *
	 */
	@Transactional
	public Set<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps() throws DataAccessException {

		return findAllInvoicePaymentOrderTemps(-1, -1);
	}

	/**
	 * JPQL Query - findAllInvoicePaymentOrderTemps
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<InvoicePaymentOrderTemp> findAllInvoicePaymentOrderTemps(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllInvoicePaymentOrderTemps", startResult, maxRows);
		return new LinkedHashSet<InvoicePaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByApInvoiceId
	 *
	 */
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByApInvoiceId(BigInteger apInvoiceId) throws DataAccessException {

		return findInvoicePaymentOrderTempByApInvoiceId(apInvoiceId, -1, -1);
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByApInvoiceId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByApInvoiceId(BigInteger apInvoiceId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findInvoicePaymentOrderTempByApInvoiceId", startResult, maxRows, apInvoiceId);
		return new LinkedHashSet<InvoicePaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findInvoicePaymentOrderTempByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<InvoicePaymentOrderTemp> findInvoicePaymentOrderTempByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findInvoicePaymentOrderTempByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<InvoicePaymentOrderTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPrimaryKey
	 *
	 */
	@Transactional
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id) throws DataAccessException {

		return findInvoicePaymentOrderTempByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findInvoicePaymentOrderTempByPrimaryKey
	 *
	 */

	@Transactional
	public InvoicePaymentOrderTemp findInvoicePaymentOrderTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findInvoicePaymentOrderTempByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.InvoicePaymentOrderTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(InvoicePaymentOrderTemp entity) {
		return true;
	}
}
