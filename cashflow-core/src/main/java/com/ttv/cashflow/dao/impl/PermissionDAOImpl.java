
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.PermissionDAO;
import com.ttv.cashflow.domain.Permission;
import com.ttv.cashflow.domain.Role;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Permission entities.
 * 
 */
@Repository("PermissionDAO")

@Transactional
public class PermissionDAOImpl extends AbstractJpaDao<Permission> implements PermissionDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Permission.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PermissionDAOImpl
	 *
	 */
	public PermissionDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPermissionByPrimaryKey
	 *
	 */
	@Transactional
	public Permission findPermissionByPrimaryKey(Integer id) throws DataAccessException {

		return findPermissionByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByPrimaryKey
	 *
	 */

	@Transactional
	public Permission findPermissionByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPermissionByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Permission) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPermissionByNameContaining
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByNameContaining(String name) throws DataAccessException {

		return findPermissionByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findPermissionByName
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByName(String name) throws DataAccessException {

		return findPermissionByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByName", startResult, maxRows, name);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findPermissionByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByDescriptionContaining(String description) throws DataAccessException {

		return findPermissionByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findPermissionById
	 *
	 */
	@Transactional
	public Permission findPermissionById(Integer id) throws DataAccessException {

		return findPermissionById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionById
	 *
	 */

	@Transactional
	public Permission findPermissionById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPermissionById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Permission) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPermissionByModifiedDate
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findPermissionByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPermissions
	 *
	 */
	@Transactional
	public Set<Permission> findAllPermissions() throws DataAccessException {

		return findAllPermissions(-1, -1);
	}

	/**
	 * JPQL Query - findAllPermissions
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findAllPermissions(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPermissions", startResult, maxRows);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findPermissionByCreatedDate
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findPermissionByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * JPQL Query - findPermissionByDescription
	 *
	 */
	@Transactional
	public Set<Permission> findPermissionByDescription(String description) throws DataAccessException {

		return findPermissionByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findPermissionByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Permission> findPermissionByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPermissionByDescription", startResult, maxRows, description);
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Permission entity) {
		return true;
	}

	public Set<Permission> findPermissionPaging(String keyword, int startResult, int maxRows)
			throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("findPermissionPaging", startResult, maxRows, keyword != null? keyword: "");
		return new LinkedHashSet<Permission>(query.getResultList());
	}

	public int countPermissionPaging(String keyword) throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countPermissionPaging", -1, -1, keyword != null? keyword: "");
		return ((Number)query.getSingleResult()).intValue();
	}
}
