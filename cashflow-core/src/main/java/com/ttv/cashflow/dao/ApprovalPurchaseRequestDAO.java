
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApprovalPurchaseRequest;

/**
 * DAO to manage ApprovalPurchaseRequest entities.
 * 
 */
public interface ApprovalPurchaseRequestDAO extends JpaDao<ApprovalPurchaseRequest> {

	/**
	 * JPQL Query - findApprovalPurchaseRequestById
	 *
	 */
	public ApprovalPurchaseRequest findApprovalPurchaseRequestById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestById
	 *
	 */
	public ApprovalPurchaseRequest findApprovalPurchaseRequestById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurposeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurposeContaining(String purpose) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurposeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurposeContaining(String purpose, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToBefore(java.util.Calendar validityTo) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToBefore(Calendar validityTo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateBefore(java.util.Calendar applicationDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateBefore(Calendar applicationDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToAfter(java.util.Calendar validityTo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToAfter(Calendar validityTo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicant
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicant(String applicant) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicant
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicant(String applicant, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalPurchaseRequests
	 *
	 */
	public Set<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests() throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalPurchaseRequests
	 *
	 */
	public Set<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateBefore(java.util.Calendar approvedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateBefore(Calendar approvedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDate(java.util.Calendar approvedDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDate(Calendar approvedDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByModifiedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByModifiedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalTypeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalTypeContaining(String approvalType) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalTypeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalTypeContaining(String approvalType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurpose
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurpose(String purpose_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurpose
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurpose(String purpose_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCreatedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCreatedDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDate(java.util.Calendar applicationDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDate
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDate(Calendar applicationDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalType
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalType(String approvalType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalType
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalType(String approvalType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCode
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCode
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCode
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCode(String currencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCode
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPrimaryKey
	 *
	 */
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPrimaryKey
	 *
	 */
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicantContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicantContaining(String applicant_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicantContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicantContaining(String applicant_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromBefore(java.util.Calendar validityFrom) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromBefore
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromBefore(Calendar validityFrom, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromAfter(java.util.Calendar validityFrom_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromAfter(Calendar validityFrom_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCodeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCodeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFrom
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFrom(java.util.Calendar validityFrom_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFrom
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFrom(Calendar validityFrom_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateAfter(java.util.Calendar applicationDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateAfter(Calendar applicationDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCodeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCodeContaining(String currencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCodeContaining
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCodeContaining(String currencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateAfter(java.util.Calendar approvedDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateAfter
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateAfter(Calendar approvedDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityTo
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityTo(java.util.Calendar validityTo_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityTo
	 *
	 */
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityTo(Calendar validityTo_2, int startResult, int maxRows) throws DataAccessException;
	public int countApprovalFilter(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo, String status);
	public int countApprovalAll();
	public Set<ApprovalPurchaseRequest> findApprovalPaging(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo,  String status, Integer orderColumn, String orderBy);
	public int checkExists(ApprovalPurchaseRequest approval);
	
	/**
	 * Searching Approval by available for AP Invoice specify.
	 */
	public List<ApprovalPurchaseRequest> findApprovalAvailable(String approvalCode, String bookingDate) throws DataAccessException;

}