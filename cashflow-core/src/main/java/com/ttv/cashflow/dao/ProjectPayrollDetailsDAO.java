
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ProjectPayrollDetails;

/**
 * DAO to manage ProjectPayrollDetails entities.
 * 
 */
public interface ProjectPayrollDetailsDAO extends JpaDao<ProjectPayrollDetails> {

	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectAllocationAmount
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByProjectAllocationAmount(java.math.BigDecimal projectAllocationAmount) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectAllocationAmount
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByProjectAllocationAmount(BigDecimal projectAllocationAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllProjectPayrollDetailss
	 *
	 */
	public Set<ProjectPayrollDetails> findAllProjectPayrollDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllProjectPayrollDetailss
	 *
	 */
	public Set<ProjectPayrollDetails> findAllProjectPayrollDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByPrimaryKey
	 *
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByPrimaryKey
	 *
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByCreatedDate
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByCreatedDate
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsById
	 *
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsById
	 *
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByModifiedDate
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findProjectPayrollDetailsByModifiedDate
	 *
	 */
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectCodeAndPayrollDetailId
	 *
	 */
	public ProjectPayrollDetails findProjectPayrollDetailsByProjectCodeAndPayrollDetailId(String projectCode, BigInteger payrollDetailId) throws DataAccessException;

}