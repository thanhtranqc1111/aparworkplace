
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ReimbursementPaymentDetailsDAO;
import com.ttv.cashflow.domain.ReimbursementPaymentDetails;

/**
 * DAO to manage ReimbursementPaymentDetails entities.
 * 
 */
@Repository("ReimbursementPaymentDetailsDAO")

@Transactional
public class ReimbursementPaymentDetailsDAOImpl extends AbstractJpaDao<ReimbursementPaymentDetails>
		implements ReimbursementPaymentDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ReimbursementPaymentDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ReimbursementPaymentDetailsDAOImpl
	 *
	 */
	public ReimbursementPaymentDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsById
	 *
	 */
	@Transactional
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsById(BigInteger id) throws DataAccessException {

		return findReimbursementPaymentDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsById
	 *
	 */

	@Transactional
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementPaymentDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementPaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findReimbursementPaymentDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAmount
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException {

		return findReimbursementPaymentDetailsByVarianceAmount(varianceAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByVarianceAmount", startResult, maxRows, varianceAmount);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findReimbursementPaymentDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementPaymentDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementPaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentOriginalAmount
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentOriginalAmount(java.math.BigDecimal paymentOriginalAmount) throws DataAccessException {

		return findReimbursementPaymentDetailsByPaymentOriginalAmount(paymentOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentOriginalAmount(java.math.BigDecimal paymentOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByPaymentOriginalAmount", startResult, maxRows, paymentOriginalAmount);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCode
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCode(String varianceAccountCode) throws DataAccessException {

		return findReimbursementPaymentDetailsByVarianceAccountCode(varianceAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCode(String varianceAccountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByVarianceAccountCode", startResult, maxRows, varianceAccountCode);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findReimbursementPaymentDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentRate
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException {

		return findReimbursementPaymentDetailsByPaymentRate(paymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByPaymentRate", startResult, maxRows, paymentRate);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentConvertedAmount
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentConvertedAmount(java.math.BigDecimal paymentConvertedAmount) throws DataAccessException {

		return findReimbursementPaymentDetailsByPaymentConvertedAmount(paymentConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentConvertedAmount(java.math.BigDecimal paymentConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByPaymentConvertedAmount", startResult, maxRows, paymentConvertedAmount);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllReimbursementPaymentDetailss
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss() throws DataAccessException {

		return findAllReimbursementPaymentDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllReimbursementPaymentDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllReimbursementPaymentDetailss", startResult, maxRows);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode) throws DataAccessException {

		return findReimbursementPaymentDetailsByVarianceAccountCodeContaining(varianceAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentDetailsByVarianceAccountCodeContaining", startResult, maxRows, varianceAccountCode);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ReimbursementPaymentDetails entity) {
		return true;
	}
	
	public BigDecimal getTotalReimAmountOriginalByReimId(BigInteger paymentId, BigInteger reimId) {
		Query query = createNamedQuery("findTotalReimAmountOriginalByReimId", -1, -1, paymentId, reimId);
		BigDecimal total = (BigDecimal) query.getSingleResult();
		return total;
	}

	@Override
	public Set<ReimbursementPaymentDetails> findReimbursemenPaymentDetailsByPaymentId(BigInteger paymentId) throws DataAccessException {
		Query query = createNamedQuery("findReimbursemenPaymentDetailsByPaymentId", -1, -1, paymentId);
		return new LinkedHashSet<ReimbursementPaymentDetails>(query.getResultList());
	}
}
