
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ArInvoiceClassDetails;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ArInvoiceClassDetails entities.
 * 
 */
public interface ArInvoiceClassDetailsDAO extends JpaDao<ArInvoiceClassDetails> {

	/**
	 * JPQL Query - findArInvoiceClassDetailsByPrimaryKey
	 *
	 */
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByPrimaryKey
	 *
	 */
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescription
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescription
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstConvertedAmount(BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCode
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCode
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceClassDetailss
	 *
	 */
	public Set<ArInvoiceClassDetails> findAllArInvoiceClassDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceClassDetailss
	 *
	 */
	public Set<ArInvoiceClassDetails> findAllArInvoiceClassDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByFxRate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByFxRate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstTypeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstTypeContaining(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstTypeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIsApproval
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIsApproval(Boolean isApproval) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIsApproval
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIsApproval(Boolean isApproval, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassName
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassName(String className) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassName
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassName(String className, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCodeContaining(String classCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCodeContaining(String classCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassNameContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassNameContaining(String className_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassNameContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassNameContaining(String className_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByModifiedDate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByModifiedDate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstRate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstRate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstType
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstType(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstType
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstType(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCode
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCode(String classCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCode
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCode(String classCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByCreatedDate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByCreatedDate
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsById
	 *
	 */
	public ArInvoiceClassDetails findArInvoiceClassDetailsById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsById
	 *
	 */
	public ArInvoiceClassDetails findArInvoiceClassDetailsById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstOriginalAmount(BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;
	
	public Set<String> findArInvoiceNoByApprovalCode(String approvalCode);
}