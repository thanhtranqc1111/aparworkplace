
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.UserHistoryDAO;
import com.ttv.cashflow.domain.UserHistory;

/**
 * DAO to manage UserHistory entities.
 * 
 */
@Repository("UserHistoryDAO")

@Transactional
public class UserHistoryDAOImpl extends AbstractJpaDao<UserHistory> implements UserHistoryDAO {

    // value for select with 'All' option from dropdowns of module and tenant
    private static final String ALL = "All";

    private static Map<Integer, String> mapColumnIndex = new HashMap<>();

    static {
        mapColumnIndex.put(1, "userHistory.first_name");
        mapColumnIndex.put(2, "userHistory.last_name");
        mapColumnIndex.put(3, "userHistory.email");
        mapColumnIndex.put(4, "userHistory.module");
        mapColumnIndex.put(5, "userHistory.action");
        mapColumnIndex.put(6, "userHistory.tenant_code");
        mapColumnIndex.put(7, "userHistory.tenant_name");
        mapColumnIndex.put(8, "userHistory.action_date");
    }

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			UserHistory.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new UserHistoryDAOImpl
	 *
	 */
	public UserHistoryDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findUserHistoryById
	 *
	 */
	@Transactional
	public UserHistory findUserHistoryById(BigInteger id) throws DataAccessException {

		return findUserHistoryById(id, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryById
	 *
	 */

	@Transactional
	public UserHistory findUserHistoryById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserHistoryById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.UserHistory) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserHistoryByFristNameContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByFristNameContaining(String fristName) throws DataAccessException {

		return findUserHistoryByFristNameContaining(fristName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByFristNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByFristNameContaining(String fristName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByFristNameContaining", startResult, maxRows, fristName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByEmail
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByEmail(String email) throws DataAccessException {

		return findUserHistoryByEmail(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByEmail
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByEmail(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByEmail", startResult, maxRows, email);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByTenantCodeContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantCodeContaining(String tenantCode) throws DataAccessException {

		return findUserHistoryByTenantCodeContaining(tenantCode, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByTenantCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantCodeContaining(String tenantCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByTenantCodeContaining", startResult, maxRows, tenantCode);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByLastName
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByLastName(String lastName) throws DataAccessException {

		return findUserHistoryByLastName(lastName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByLastName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByLastName(String lastName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByLastName", startResult, maxRows, lastName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByModuleContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByModuleContaining(String module) throws DataAccessException {

		return findUserHistoryByModuleContaining(module, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByModuleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByModuleContaining(String module, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByModuleContaining", startResult, maxRows, module);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByActionContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByActionContaining(String action) throws DataAccessException {

		return findUserHistoryByActionContaining(action, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByActionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByActionContaining(String action, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByActionContaining", startResult, maxRows, action);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByTenantName
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantName(String tenantName) throws DataAccessException {

		return findUserHistoryByTenantName(tenantName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByTenantName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantName(String tenantName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByTenantName", startResult, maxRows, tenantName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllUserHistorys
	 *
	 */
	@Transactional
	public Set<UserHistory> findAllUserHistorys() throws DataAccessException {

		return findAllUserHistorys(-1, -1);
	}

	/**
	 * JPQL Query - findAllUserHistorys
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findAllUserHistorys(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllUserHistorys", startResult, maxRows);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByFristName
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByFristName(String fristName) throws DataAccessException {

		return findUserHistoryByFristName(fristName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByFristName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByFristName(String fristName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByFristName", startResult, maxRows, fristName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByTenantNameContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantNameContaining(String tenantName) throws DataAccessException {

		return findUserHistoryByTenantNameContaining(tenantName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByTenantNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantNameContaining(String tenantName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByTenantNameContaining", startResult, maxRows, tenantName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByPrimaryKey
	 *
	 */
	@Transactional
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id) throws DataAccessException {

		return findUserHistoryByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByPrimaryKey
	 *
	 */

	@Transactional
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserHistoryByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.UserHistory) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserHistoryByAction
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByAction(String action) throws DataAccessException {

		return findUserHistoryByAction(action, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByAction
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByAction(String action, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByAction", startResult, maxRows, action);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByTenantCode
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantCode(String tenantCode) throws DataAccessException {

		return findUserHistoryByTenantCode(tenantCode, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByTenantCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByTenantCode(String tenantCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByTenantCode", startResult, maxRows, tenantCode);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByModule
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByModule(String module) throws DataAccessException {

		return findUserHistoryByModule(module, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByModule
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByModule(String module, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByModule", startResult, maxRows, module);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByLastNameContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByLastNameContaining(String lastName) throws DataAccessException {

		return findUserHistoryByLastNameContaining(lastName, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByLastNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByLastNameContaining(String lastName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByLastNameContaining", startResult, maxRows, lastName);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByActionDate
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByActionDate(java.util.Calendar actionDate) throws DataAccessException {

		return findUserHistoryByActionDate(actionDate, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByActionDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByActionDate(java.util.Calendar actionDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByActionDate", startResult, maxRows, actionDate);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserHistoryByEmailContaining
	 *
	 */
	@Transactional
	public Set<UserHistory> findUserHistoryByEmailContaining(String email) throws DataAccessException {

		return findUserHistoryByEmailContaining(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserHistoryByEmailContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserHistory> findUserHistoryByEmailContaining(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserHistoryByEmailContaining", startResult, maxRows, email);
		return new LinkedHashSet<UserHistory>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(UserHistory entity) {
		return true;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<UserHistory> findPagingUserHistories(UserHistory userHistory, String fromMonth, String toMonth, Integer firstResult,
            Integer maxResults, Integer orderColumn, String orderBy) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT userHistory.* ");
        queryString.append("FROM user_history userHistory ");
        queryString.append("WHERE ");

        if (StringUtils.isNotBlank(userHistory.getFirstName())) {
            queryString.append("(LOWER(userHistory.first_name) LIKE LOWER(CONCAT('%', :firstName, '%'))) AND ");
        } else {
            queryString.append("((userHistory.first_name IS NULL) OR (LOWER(userHistory.first_name) LIKE LOWER(CONCAT('%', :firstName, '%')))) AND ");
        }

        if (StringUtils.isNotBlank(userHistory.getLastName())) {
            queryString.append("(LOWER(userHistory.last_name) LIKE LOWER(CONCAT('%', :lastName, '%'))) AND ");
        } else {
            queryString.append("((userHistory.last_name IS NULL) OR (LOWER(userHistory.last_name) LIKE LOWER(CONCAT('%', :lastName, '%')))) AND ");
        }

        if (StringUtils.isNotBlank(userHistory.getEmail())) {
            queryString.append("((LOWER(userHistory.email) LIKE LOWER(CONCAT('%', :email, '%')))) AND ");
        } else {
            queryString.append("((userHistory.email IS NULL) OR (LOWER(userHistory.email) LIKE LOWER(CONCAT('%', :email, '%')))) AND ");
        }

        String tenantCode = userHistory.getTenantCode();

        // if tenant code is an array, then select all tenant which user can access to
        if (tenantCode != null && tenantCode.contains(",")) {
            queryString.append("(userHistory.tenant_code IN :tenantCodes) AND ");
        } else {
            queryString.append("((userHistory.tenant_code IS NULL) OR (LOWER(userHistory.tenant_code) = LOWER(:tenantCode))) AND ");
        }

        if (!ALL.equals(userHistory.getModule())) {
            queryString.append("((userHistory.module IS NULL) OR (LOWER(userHistory.module) = LOWER(:module))) AND ");
        }

        queryString.append("((:fromMonth = '') OR userHistory.action_date >= TO_TIMESTAMP(:fromMonth, 'dd/MM/yyyy')) AND ");
        queryString.append("((:toMonth = '') OR userHistory.action_date < TO_TIMESTAMP(:toMonth, 'dd/MM/yyyy')) ");
        queryString.append("GROUP BY userHistory.id, userHistory.tenant_code ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ").append(orderBy);

        Query query = getEntityManager().createNativeQuery(queryString.toString(), UserHistory.class);
        query.setParameter("firstName", userHistory.getFirstName());
        query.setParameter("lastName", userHistory.getLastName());
        query.setParameter("email", userHistory.getEmail());

        // if tenant code is an array, then select all tenant which user can access to
        if (tenantCode != null && tenantCode.contains(",")) {
            String[] tenantCodeArr = tenantCode.split(",");
            List<String> tenantCodes = Arrays.asList(tenantCodeArr);

            query.setParameter("tenantCodes", tenantCodes);
        } else {
            query.setParameter("tenantCode", userHistory.getTenantCode());
        }

        if (!ALL.equals(userHistory.getModule())) {
            query.setParameter("module", userHistory.getModule());
        }

        query.setParameter("fromMonth", fromMonth);
        query.setParameter("toMonth", toMonth);

        query.setFirstResult(firstResult == null || firstResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : firstResult);

        if (maxResults != null && maxResults > 0) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    @Override
    public Long countAllUserHistory() {
        return (Long) executeQuerySingleResult("SELECT count(1) FROM UserHistory");
    }

    @Override
    public int countFilteredUserHistory(UserHistory userHistory, String fromMonth, String toMonth) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT COUNT(1) ");
        queryString.append("FROM user_history userHistory ");
        queryString.append("WHERE ");

        if (StringUtils.isNotBlank(userHistory.getFirstName())) {
            queryString.append("(LOWER(userHistory.first_name) LIKE LOWER(CONCAT('%', :firstName, '%'))) AND ");
        } else {
            queryString.append("((userHistory.first_name IS NULL) OR (LOWER(userHistory.first_name) LIKE LOWER(CONCAT('%', :firstName, '%')))) AND ");
        }

        if (StringUtils.isNotBlank(userHistory.getLastName())) {
            queryString.append("(LOWER(userHistory.last_name) LIKE LOWER(CONCAT('%', :lastName, '%'))) AND ");
        } else {
            queryString.append("((userHistory.last_name IS NULL) OR (LOWER(userHistory.last_name) LIKE LOWER(CONCAT('%', :lastName, '%')))) AND ");
        }

        if (StringUtils.isNotBlank(userHistory.getEmail())) {
            queryString.append("((LOWER(userHistory.email) LIKE LOWER(CONCAT('%', :email, '%')))) AND ");
        } else {
            queryString.append("((userHistory.email IS NULL) OR (LOWER(userHistory.email) LIKE LOWER(CONCAT('%', :email, '%')))) AND ");
        }

        String tenantCode = userHistory.getTenantCode();

        // if tenant code is an array, then select all tenant which user can access to
        if (tenantCode != null && tenantCode.contains(",")) {
            queryString.append("(userHistory.tenant_code IN :tenantCodes) AND ");
        } else {
            queryString.append("((userHistory.tenant_code IS NULL) OR (LOWER(userHistory.tenant_code) = LOWER(:tenantCode))) AND ");
        }

        if (!ALL.equals(userHistory.getModule())) {
            queryString.append("((userHistory.module IS NULL) OR (LOWER(userHistory.module) = LOWER(:module))) AND ");
        }

        queryString.append("((:fromMonth = '') OR userHistory.action_date >= TO_TIMESTAMP(:fromMonth, 'dd/MM/yyyy')) AND ");
        queryString.append("((:toMonth = '') OR userHistory.action_date < TO_TIMESTAMP(:toMonth, 'dd/MM/yyyy')) ");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter("firstName", userHistory.getFirstName());
        query.setParameter("lastName", userHistory.getLastName());
        query.setParameter("email", userHistory.getEmail());

        // if tenant code is an array, then select all tenant which user can access to
        if (tenantCode != null && tenantCode.contains(",")) {
            String[] tenantCodeArr = tenantCode.split(",");
            List<String> tenantCodes = Arrays.asList(tenantCodeArr);

            query.setParameter("tenantCodes", tenantCodes);
        } else {
            query.setParameter("tenantCode", userHistory.getTenantCode());
        }

        if (!ALL.equals(userHistory.getModule())) {
            query.setParameter("module", userHistory.getModule());
        }

        query.setParameter("fromMonth", fromMonth);
        query.setParameter("toMonth", toMonth);

        return ((BigInteger) query.getSingleResult()).intValue();
    }
}
