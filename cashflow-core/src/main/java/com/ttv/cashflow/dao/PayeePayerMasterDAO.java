
package com.ttv.cashflow.dao;

import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.PayeePayerMaster;

/**
 * DAO to manage PayeePayerMaster entities.
 * 
 */
public interface PayeePayerMasterDAO extends JpaDao<PayeePayerMaster> {

	/**
	 * Find payee payer master by account and name.
	 *
	 * @param keyword the keyword
	 * @return the sets the
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountAndName(String keyword);
	
	/**
	 * JPQL Query - findPayeePayerMasterByPrimaryKey
	 *
	 */
	public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPrimaryKey
	 *
	 */
	public PayeePayerMaster findPayeePayerMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTerm
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTerm(String paymentTerm) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTerm
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTerm(String paymentTerm, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTermContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTermContaining(String paymentTerm_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPaymentTermContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPaymentTermContaining(String paymentTerm_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPhoneContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPhoneContaining(String phone) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPhoneContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPhoneContaining(String phone, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByNameContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByNameContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCodeContaining(String bankAccountCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCodeContaining(String bankAccountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayeePayerMasters
	 *
	 */
	public Set<PayeePayerMaster> findAllPayeePayerMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayeePayerMasters
	 *
	 */
	public Set<PayeePayerMaster> findAllPayeePayerMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAddressContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAddressContaining(String address) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAddressContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankName
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankName(String bankName) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankName
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankName(String bankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByName
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByName
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCodeContaining(String accountCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCode(String typeCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCode(String typeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPhone
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPhone(String phone_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByPhone
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByPhone(String phone_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCodeContaining(String typeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByTypeCodeContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByTypeCodeContaining(String typeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAddress
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAddress(String address_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAddress
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAddress(String address_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCode(String bankAccountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankAccountCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankAccountCode(String bankAccountCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByCode
	 *
	 */
	public PayeePayerMaster findPayeePayerMasterByCode(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByCode
	 *
	 */
	public PayeePayerMaster findPayeePayerMasterByCode(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankNameContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankNameContaining(String bankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByBankNameContaining
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByBankNameContaining(String bankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCode(String accountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerMasterByAccountCode
	 *
	 */
	public Set<PayeePayerMaster> findPayeePayerMasterByAccountCode(String accountCode_1, int startResult, int maxRows) throws DataAccessException;
	
	public Set<PayeePayerMaster> findPayeeMasterPaging(
                Integer start, Integer end, Integer orderColumn, String orderBy,
                String keyword) throws DataAccessException;
	public int countPayeeMasterFilter(Integer start, Integer length, Integer orderColumn,
            String orderBy, String keyword);

    public List<PayeePayerMaster> findTransactionPartyMastersByTransactionType(String transactionType);

    boolean checkExistingCustomerName(String customerName);

}