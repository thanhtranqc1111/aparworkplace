
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PayrollDetails;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PayrollDetails entities.
 * 
 */
public interface PayrollDetailsDAO extends JpaDao<PayrollDetails> {

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther2(java.math.BigDecimal deductionOther2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther2(BigDecimal deductionOther2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCode
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCode
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTotalDeduction
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTotalDeduction(java.math.BigDecimal totalDeduction) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTotalDeduction
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTotalDeduction(BigDecimal totalDeduction, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment3(BigDecimal laborAdditionalPayment3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByPrimaryKey
	 *
	 */
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByPrimaryKey
	 *
	 */
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer2(BigDecimal laborSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCodeContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCodeContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeContaining(String employeeCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeContaining(String employeeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitle
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByRoleTitle(String roleTitle) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitle
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByNetPayment
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByNetPaymentOriginal(java.math.BigDecimal netPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByNetPayment
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByNetPaymentOriginal(BigDecimal netPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByIsApproval
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByIsApproval(Boolean isApproval) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByIsApproval
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByIsApproval(Boolean isApproval, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment1(BigDecimal laborAdditionalPayment1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionWht
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionWht(java.math.BigDecimal deductionWht) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionWht
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionWht(BigDecimal deductionWht, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment2(BigDecimal laborAdditionalPayment2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDivisionContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDivisionContaining(String division) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDivisionContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDivisionContaining(String division, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer1(BigDecimal laborSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer2(BigDecimal deductionSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCode
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCode(String employeeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCode
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCode(String employeeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollDetailss
	 *
	 */
	public Set<PayrollDetails> findAllPayrollDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollDetailss
	 *
	 */
	public Set<PayrollDetails> findAllPayrollDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByModifiedDate
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByModifiedDate
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee1(BigDecimal deductionSocialInsuranceEmployee1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee3(BigDecimal deductionSocialInsuranceEmployee3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer1(BigDecimal deductionSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTotalLaborCost
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTotalLaborCost(java.math.BigDecimal totalLaborCost) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTotalLaborCost
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTotalLaborCost(BigDecimal totalLaborCost, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment2(BigDecimal laborOtherPayment2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByCreatedDate
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByCreatedDate
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment1(BigDecimal laborOtherPayment1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee2
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee2(BigDecimal deductionSocialInsuranceEmployee2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborBaseSalaryPayment
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborBaseSalaryPayment
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborBaseSalaryPayment(BigDecimal laborBaseSalaryPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer3(BigDecimal laborSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDivision
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDivision(String division_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDivision
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDivision(String division_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTeamContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTeamContaining(String team) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTeamContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTeamContaining(String team, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer3(BigDecimal deductionSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther1(java.math.BigDecimal deductionOther1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther1
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther1(BigDecimal deductionOther1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitleContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByRoleTitleContaining(String roleTitle_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitleContaining
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByRoleTitleContaining(String roleTitle_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther3(java.math.BigDecimal deductionOther3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther3(BigDecimal deductionOther3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTeam
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTeam(String team_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByTeam
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByTeam(String team_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsById
	 *
	 */
	public PayrollDetails findPayrollDetailsById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsById
	 *
	 */
	public PayrollDetails findPayrollDetailsById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment3
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment3(BigDecimal laborOtherPayment3, int startResult, int maxRows) throws DataAccessException;
	
	public Set<String> findPayrollNoByApprovalCode(String approvalCode);
	
	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeAndMonth
	 *
	 */
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeAndMonth(Set<String> empCodes, Calendar fromMonth, Calendar toMonth) throws DataAccessException;
}