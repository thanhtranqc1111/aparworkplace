
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ReceiptOrderDAO;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.ReceiptOrder;
import com.ttv.cashflow.dto.ReceiptOrderDTO;
import com.ttv.cashflow.util.Constant;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ReceiptOrder entities.
 * 
 */
@Repository("ReceiptOrderDAO")

@Transactional
public class ReceiptOrderDAOImpl extends AbstractJpaDao<ReceiptOrder> implements ReceiptOrderDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "myBankStatement.id");
    	mapColumnIndex.put(1, "myBankStatement.transaction_date");
    	mapColumnIndex.put(2, "myBankStatement.value_date");
    }
	/**
	 * Set of entity classes managed by this DAO. Typically a DAO manages a single
	 * entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] { ReceiptOrder.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ReceiptOrderDAOImpl
	 *
	 */
	public ReceiptOrderDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findReceiptOrderBySettedIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderBySettedIncludeGstOriginalAmount(
			java.math.BigDecimal settedIncludeGstOriginalAmount) throws DataAccessException {

		return findReceiptOrderBySettedIncludeGstOriginalAmount(settedIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderBySettedIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderBySettedIncludeGstOriginalAmount(
			java.math.BigDecimal settedIncludeGstOriginalAmount, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findReceiptOrderBySettedIncludeGstOriginalAmount", startResult, maxRows,
				settedIncludeGstOriginalAmount);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findReceiptOrderById
	 *
	 */
	@Transactional
	public ReceiptOrder findReceiptOrderById(BigInteger id) throws DataAccessException {

		return findReceiptOrderById(id, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderById
	 *
	 */

	@Transactional
	public ReceiptOrder findReceiptOrderById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReceiptOrderById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReceiptOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReceiptOrderByArInvoiceId
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByArInvoiceId(BigInteger arInvoiceId) throws DataAccessException {

		return findReceiptOrderByArInvoiceId(arInvoiceId, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderByArInvoiceId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByArInvoiceId(BigInteger arInvoiceId, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findReceiptOrderByArInvoiceId", startResult, maxRows, arInvoiceId);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findReceiptOrderByTransactionId
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByTransactionId(BigInteger transactionId) throws DataAccessException {

		return findReceiptOrderByTransactionId(transactionId, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderByTransactionId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByTransactionId(BigInteger transactionId, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findReceiptOrderByTransactionId", startResult, maxRows, transactionId);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findReceiptOrderByCreatedDate
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findReceiptOrderByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findReceiptOrderByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findReceiptOrderByModifedDate
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByModifedDate(java.util.Calendar modifedDate) throws DataAccessException {

		return findReceiptOrderByModifedDate(modifedDate, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderByModifedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findReceiptOrderByModifedDate(java.util.Calendar modifedDate, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findReceiptOrderByModifedDate", startResult, maxRows, modifedDate);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllReceiptOrders
	 *
	 */
	@Transactional
	public Set<ReceiptOrder> findAllReceiptOrders() throws DataAccessException {

		return findAllReceiptOrders(-1, -1);
	}

	/**
	 * JPQL Query - findAllReceiptOrders
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReceiptOrder> findAllReceiptOrders(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllReceiptOrders", startResult, maxRows);
		return new LinkedHashSet<ReceiptOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findReceiptOrderByPrimaryKey
	 *
	 */
	@Transactional
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id) throws DataAccessException {

		return findReceiptOrderByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findReceiptOrderByPrimaryKey
	 *
	 */

	@Transactional
	public ReceiptOrder findReceiptOrderByPrimaryKey(BigInteger id, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findReceiptOrderByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReceiptOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity
	 * when calling Store
	 * 
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ReceiptOrder entity) {
		return true;
	}

	@Override
	public List<ReceiptOrderDTO> findReceiptOrderPaging(List<BigInteger> listBankStatementIds, Integer orderColumn, String orderBy) {
		StringBuilder strBankTransId = new StringBuilder();
		for(int i = 0; i < listBankStatementIds.size(); i++) {
			if(i == 0)
			{
				strBankTransId.append(listBankStatementIds.get(i));
			}
			else
			{
				strBankTransId.append("," + listBankStatementIds.get(i));
			}
		}
		StringBuilder queryString = new StringBuilder("SELECT myBankStatement.id  as bankStatementId, " + 
															 "myBankStatement.transaction_date as transactionDate, " + 
															 "myBankStatement.value_date as valueDate, " + 
															 "myBankStatement.currency_code as currencyCode, " + 
															 "myBankStatement.credit as credit, " + 
															 "myArInvoice.status as arStatus, " + 
															 "systemValue.value as arStatusValue, " + 
															 "myArInvoice.ar_invoice_no as arInvoiceNo," + 
															 "myArInvoice.month as month," + 
															 "myArInvoice.payer_name as payerName," + 
															 "myArInvoice.invoice_no as invoiceNo," + 
															 "myArInvoice.original_currency_code as originalCurrencyCode," + 
															 "myArInvoice.include_gst_original_amount as totalAmount," + 
															 "myArInvoice.include_gst_converted_amount as totalAmountConverted," + 
															 "myArInvoiceStatus.remain_include_gst_original_amount as remainAmount," + 
															 "myReceiptOrder.variance_amount as varianceAmount " + 
												      "FROM   bank_statement myBankStatement " + 
												      "LEFT OUTER JOIN receipt_order myReceiptOrder ON myBankStatement.id = myReceiptOrder.transaction_id " + 
												      "LEFT OUTER JOIN ar_invoice myArInvoice ON myArInvoice.id = myReceiptOrder.ar_invoice_id " + 
												      "LEFT OUTER JOIN ar_invoice_receipt_status myArInvoiceStatus ON myArInvoiceStatus.ar_invoice_id = myArInvoice.id " +
												      "LEFT OUTER JOIN system_value systemValue ON (systemValue.type = '" + Constant.PREFIX_ARI + "' and myArInvoice.status = systemValue.code) " + 
												      "WHERE myBankStatement.id in (" + strBankTransId.toString() +") ");
		queryString.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		Query query = getEntityManager().createNativeQuery(queryString.toString(), "ReceiptOrderDTO");
		return query.getResultList();
	}

	@Override
	public String getReceiptOrderNo() {
		// TODO Auto-generated method stub
		Query query = getEntityManager().createNativeQuery("SELECT receipt_no()");

		String ret = (String) query.getSingleResult();
		
		return ret;
	}
}
