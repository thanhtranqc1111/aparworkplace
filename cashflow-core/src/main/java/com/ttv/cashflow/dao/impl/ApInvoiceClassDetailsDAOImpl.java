
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.dto.ApInvoiceClassDetailDTO;
import com.ttv.cashflow.dto.SummaryApInvoiceClassDetailsDTO;

/**
 * DAO to manage ApInvoiceClassDetails entities.
 * 
 */
@Repository("ApInvoiceClassDetailsDAO")

@Transactional
public class ApInvoiceClassDetailsDAOImpl extends AbstractJpaDao<ApInvoiceClassDetails>
		implements ApInvoiceClassDetailsDAO {
	/**
	 * Set of entity classes managed by this DAO. Typically a DAO manages a single
	 * entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] { ApInvoiceClassDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoiceClassDetailsDAOImpl
	 *
	 */
	public ApInvoiceClassDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGstAmount(java.math.BigDecimal gstAmount)
			throws DataAccessException {

		return findApInvoiceClassDetailsByGstAmount(gstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGstAmount(java.math.BigDecimal gstAmount,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByGstAmount", startResult, maxRows, gstAmount);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstConvertedAmount(
			java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findApInvoiceClassDetailsByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstConvertedAmount(
			java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByExcludeGstConvertedAmount", startResult, maxRows,
				excludeGstConvertedAmount);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount)
			throws DataAccessException {

		return findApInvoiceClassDetailsByExcludeGstAmount(excludeGstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByExcludeGstAmount", startResult, maxRows,
				excludeGstAmount);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate)
			throws DataAccessException {

		return findApInvoiceClassDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCode(String approvalCode)
			throws DataAccessException {

		return findApInvoiceClassDetailsByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCode(String approvalCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGst
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGst(java.math.BigDecimal gst)
			throws DataAccessException {

		return findApInvoiceClassDetailsByGst(gst, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGst
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGst(java.math.BigDecimal gst, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByGst", startResult, maxRows, gst);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByIncludeGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount)
			throws DataAccessException {

		return findApInvoiceClassDetailsByIncludeGstAmount(includeGstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByIncludeGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByIncludeGstAmount", startResult, maxRows,
				includeGstAmount);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescription
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescription(String description)
			throws DataAccessException {

		return findApInvoiceClassDetailsByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescription(String description, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApInvoiceClassDetailss
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findAllApInvoiceClassDetailss() throws DataAccessException {

		return findAllApInvoiceClassDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoiceClassDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findAllApInvoiceClassDetailss(int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoiceClassDetailss", startResult, maxRows);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCodeContaining(String vatCode)
			throws DataAccessException {

		return findApInvoiceClassDetailsByVatCodeContaining(vatCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCodeContaining(String vatCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByVatCodeContaining", startResult, maxRows, vatCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoiceClassDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceClassDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceClassDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassName
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassName(String className)
			throws DataAccessException {

		return findApInvoiceClassDetailsByClassName(className, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassName(String className, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByClassName", startResult, maxRows, className);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate)
			throws DataAccessException {

		return findApInvoiceClassDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCodeContaining(String classCode)
			throws DataAccessException {

		return findApInvoiceClassDetailsByClassCodeContaining(classCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCodeContaining(String classCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByClassCodeContaining", startResult, maxRows,
				classCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassNameContaining(String className)
			throws DataAccessException {

		return findApInvoiceClassDetailsByClassNameContaining(className, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassNameContaining(String className, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByClassNameContaining", startResult, maxRows,
				className);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescriptionContaining(String description)
			throws DataAccessException {

		return findApInvoiceClassDetailsByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescriptionContaining(String description,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByDescriptionContaining", startResult, maxRows,
				description);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCode(String vatCode) throws DataAccessException {

		return findApInvoiceClassDetailsByVatCode(vatCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCode(String vatCode, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByVatCode", startResult, maxRows, vatCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNoContaining(String poNo)
			throws DataAccessException {

		return findApInvoiceClassDetailsByPoNoContaining(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNoContaining(String poNo, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByPoNoContaining", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCodeContaining(String approvalCode)
			throws DataAccessException {

		return findApInvoiceClassDetailsByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCodeContaining(String approvalCode,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByApprovalCodeContaining", startResult, maxRows,
				approvalCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsById
	 *
	 */
	@Transactional
	public ApInvoiceClassDetails findApInvoiceClassDetailsById(BigInteger id) throws DataAccessException {

		return findApInvoiceClassDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsById
	 *
	 */

	@Transactional
	public ApInvoiceClassDetails findApInvoiceClassDetailsById(BigInteger id, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceClassDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceClassDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByFxRate
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate)
			throws DataAccessException {

		return findApInvoiceClassDetailsByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCode(String classCode)
			throws DataAccessException {

		return findApInvoiceClassDetailsByClassCode(classCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCode(String classCode, int startResult,
			int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByClassCode", startResult, maxRows, classCode);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNo
	 *
	 */
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNo(String poNo) throws DataAccessException {

		return findApInvoiceClassDetailsByPoNo(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNo(String poNo, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceClassDetailsByPoNo", startResult, maxRows, poNo);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity
	 * when calling Store
	 * 
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoiceClassDetails entity) {
		return true;
	}

	@Transactional
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIsApproval(Boolean isApproval)
			throws DataAccessException {

		return findApInvoiceClassDetailsByIsApproval(isApproval, -1, -1);
	}

	@SuppressWarnings("unchecked")
	public List<SummaryApInvoiceClassDetailsDTO> findByApprovalCodeNotApproved(String approvalCode)
			throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = entityManager.createNativeQuery(
				"select MIN (id) as id, MIN (ap_invoice_id) as ap_invoice_id, approval_code as approvalCode,"
						+ "MIN(created_date) as applicationDate, MIN(original_currency_code) as currency, sum(include_gst_original_amount) as amount, MIN(description) as description, MIN(ap_invoice_no) "
						+ "from ("
						+ "    select a1.id, a3.id as ap_invoice_id, a1.approval_code, a1.include_gst_original_amount,"
						+ "	 a1.ap_invoice_account_details_id, a1.created_date, a3.original_currency_code, a3.description, a3.ap_invoice_no "
						+ "    from ap_invoice_class_details a1, ap_invoice_account_details a2, ap_invoice a3 "
						+ "    where (a1.is_approval = false or  a1.is_approval is null) "
						+ "    and a1.ap_invoice_account_details_id = a2.id "
						+ "    and a2.ap_invoice_id = a3.id\r\n" + ") data "
						+ "where lower(approval_code) like lower(?1) group by approval_code")
				.setParameter(1, "%" + approvalCode + "%");
		Set<Object[]> objMaster = new LinkedHashSet<Object[]>(query.getResultList());
		List<SummaryApInvoiceClassDetailsDTO> list = new ArrayList<SummaryApInvoiceClassDetailsDTO>();
		for (Object[] objects : objMaster) {
			SummaryApInvoiceClassDetailsDTO obj = new SummaryApInvoiceClassDetailsDTO();
			obj.setId((BigInteger) objects[0]);
			obj.setApInvoiceId((BigInteger) objects[1]);
			obj.setApprovalCode((String) objects[2]);

			java.sql.Timestamp applicationTimeStamp = (Timestamp) objects[3];
			Calendar applicationCelendar = Calendar.getInstance();
			applicationCelendar.setTimeInMillis(applicationTimeStamp.getTime());
			obj.setApplicationDate(applicationCelendar);

			obj.setCurrency((String) objects[4]);
			obj.setAmount((BigDecimal)objects[5]);
			obj.setDescription((String) objects[6]);
			obj.setApInvoiceNo((String) objects[7]);
			list.add(obj);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIsApproval(Boolean isApproval, int startResult,
			int maxRows) throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("findApInvoiceClassDetailsByIsApproval", startResult, maxRows, isApproval);
		return new LinkedHashSet<ApInvoiceClassDetails>(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApInvoiceClassDetailDTO> findDetailByApprovalCode(String approvalCode) throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = entityManager.createNativeQuery("  select a1.class_name, a1.description, a1.include_gst_original_amount as amount " + 
													  "  from ap_invoice_class_details a1 " + 
													  "  where a1.approval_code = ?1")
													.setParameter(1, approvalCode);
		Set<Object[]> objMaster = new LinkedHashSet<Object[]>(query.getResultList());
		List<ApInvoiceClassDetailDTO> list = new ArrayList<ApInvoiceClassDetailDTO>();
		for (Object[] objects : objMaster) {
			ApInvoiceClassDetailDTO obj = new ApInvoiceClassDetailDTO();
			obj.setProjectName((String) objects[0]);
			obj.setDescription((String) objects[1]);
			obj.setAmount((BigDecimal) objects[2]);
			list.add(obj);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> findArInvoiceNoByApprovalCode(String approvalCode) {
		String queryString = "SELECT DISTINCT(ac.apInvoiceAccountDetails.apInvoice.apInvoiceNo) " + 
						 	 "FROM ApInvoiceClassDetails ac " +
						 	 "WHERE ac.approvalCode = ?1";

		Query query = getEntityManager().createQuery(queryString).setParameter(1, approvalCode);
		
		return new LinkedHashSet<String>(query.getResultList());
	}

}
