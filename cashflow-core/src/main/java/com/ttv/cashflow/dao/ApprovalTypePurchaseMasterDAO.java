
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApprovalTypePurchaseMaster;

/**
 * DAO to manage ApprovalTypePurchaseMaster entities.
 * 
 */
public interface ApprovalTypePurchaseMasterDAO extends JpaDao<ApprovalTypePurchaseMaster> {

	/**
	 * JPQL Query - findAllApprovalTypePurchaseMasters
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalTypePurchaseMasters
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findAllApprovalTypePurchaseMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByPrimaryKey
	 *
	 */
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByPrimaryKey
	 *
	 */
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByName
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByName
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCodeContaining
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCodeContaining
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByNameContaining
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByNameContaining
	 *
	 */
	public Set<ApprovalTypePurchaseMaster> findApprovalTypePurchaseMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCode
	 *
	 */
	public ApprovalTypePurchaseMaster findApprovalTypePurchaseMasterByCode(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypePurchaseMasterByCode
	 *
	 */
	public int countApprovalTypeAccountMasterFilter(Integer start, Integer end, String keyword);
	public int countApprovalTypeAccountMasterAll();
	public Set<ApprovalTypePurchaseMaster> findApprovalTypeAccountMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}