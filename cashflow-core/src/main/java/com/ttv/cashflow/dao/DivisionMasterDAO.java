
package com.ttv.cashflow.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.DivisionMaster;
import com.ttv.cashflow.dto.DivisionMasterDTO;

/**
 * DAO to manage DivisionMaster entities.
 * 
 */
public interface DivisionMasterDAO extends JpaDao<DivisionMaster> {

	/**
	 * JPQL Query - findDivisionMasterById
	 *
	 */
	public DivisionMaster findDivisionMasterById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterById
	 *
	 */
	public DivisionMaster findDivisionMasterById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByPrimaryKey
	 *
	 */
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByPrimaryKey
	 *
	 */
	public DivisionMaster findDivisionMasterByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByCodeContaining
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByCodeContaining(String code) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByCodeContaining
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByCode
	 *
	 */
    public Set<DivisionMaster> findDivisionMasterByCode(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByCode
	 *
	 */
    public Set<DivisionMaster> findDivisionMasterByCode(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByName
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByName
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllDivisionMasters
	 *
	 */
	public Set<DivisionMaster> findAllDivisionMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllDivisionMasters
	 *
	 */
	public Set<DivisionMaster> findAllDivisionMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByNameContaining
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findDivisionMasterByNameContaining
	 *
	 */
	public Set<DivisionMaster> findDivisionMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

    List<DivisionMasterDTO> findDivisionMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    Long countAllDivisionMaster();

    Long countFilteredDivisionMaster(String keyword);
}