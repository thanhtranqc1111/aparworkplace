
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.BankMasterDAO;
import com.ttv.cashflow.domain.BankMaster;

/**
 * DAO to manage BankMaster entities.
 * 
 */
@Repository("BankMasterDAO")

@Transactional
public class BankMasterDAOImpl extends AbstractJpaDao<BankMaster>
        implements BankMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myBankMaster.code");
    	mapColumnIndex.put(2, "myBankMaster.name");
    }
    /**
     * Set of entity classes managed by this DAO. Typically a DAO manages a
     * single entity.
     *
     */
    private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
            Arrays.asList(new Class<?>[] { BankMaster.class }));

    /**
     * EntityManager injected by Spring for persistence unit CashFlowDriver
     *
     */
    @PersistenceContext(unitName = "CashFlowDriver")
    private EntityManager entityManager;

    /**
     * Instantiates a new BankMasterDAOImpl
     *
     */
    public BankMasterDAOImpl() {
        super();
    }

    /**
     * Get the entity manager that manages persistence unit
     *
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Returns the set of entity classes managed by this DAO.
     *
     */
    public Set<Class<?>> getTypes() {
        return dataTypes;
    }

    /**
     * JPQL Query - findBankMasterByPrimaryKey
     *
     */
    @Transactional
    public BankMaster findBankMasterByPrimaryKey(String code)
            throws DataAccessException {

        return findBankMasterByPrimaryKey(code, -1, -1);
    }

    /**
     * JPQL Query - findBankMasterByPrimaryKey
     *
     */

    @Transactional
    public BankMaster findBankMasterByPrimaryKey(String code, int startResult,
            int maxRows) throws DataAccessException {
        try {
            Query query = createNamedQuery("findBankMasterByPrimaryKey",
                    startResult, maxRows, code);
            return (com.ttv.cashflow.domain.BankMaster) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findBankMasterByNameContaining
     *
     */
    @Transactional
    public Set<BankMaster> findBankMasterByNameContaining(String name)
            throws DataAccessException {

        return findBankMasterByNameContaining(name, -1, -1);
    }

    /**
     * JPQL Query - findBankMasterByNameContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<BankMaster> findBankMasterByNameContaining(String name,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findBankMasterByNameContaining",
                startResult, maxRows, name);
        return new LinkedHashSet<BankMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findBankMasterByName
     *
     */
    @Transactional
    public Set<BankMaster> findBankMasterByName(String name)
            throws DataAccessException {

        return findBankMasterByName(name, -1, -1);
    }

    /**
     * JPQL Query - findBankMasterByName
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<BankMaster> findBankMasterByName(String name, int startResult,
            int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findBankMasterByName", startResult,
                maxRows, name);
        return new LinkedHashSet<BankMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findBankMasterByCodeContaining
     *
     */
    @Transactional
    public Set<BankMaster> findBankMasterByCodeContaining(String code)
            throws DataAccessException {

        return findBankMasterByCodeContaining(code, -1, -1);
    }

    /**
     * JPQL Query - findBankMasterByCodeContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<BankMaster> findBankMasterByCodeContaining(String code,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findBankMasterByCodeContaining",
                startResult, maxRows, code);
        return new LinkedHashSet<BankMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findBankMasterByCode
     *
     */
    @Transactional
    public BankMaster findBankMasterByCode(String code)
            throws DataAccessException {

        return findBankMasterByCode(code, -1, -1);
    }

    /**
     * JPQL Query - findBankMasterByCode
     *
     */

    @Transactional
    public BankMaster findBankMasterByCode(String code, int startResult,
            int maxRows) throws DataAccessException {
        try {
            Query query = createNamedQuery("findBankMasterByCode", startResult,
                    maxRows, code);
            return (com.ttv.cashflow.domain.BankMaster) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findAllBankMasters
     *
     */
    @Transactional
    public Set<BankMaster> findAllBankMasters() throws DataAccessException {

        return findAllBankMasters(-1, -1);
    }

    /**
     * JPQL Query - findAllBankMasters
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<BankMaster> findAllBankMasters(int startResult, int maxRows)
            throws DataAccessException {
        Query query = createNamedQuery("findAllBankMasters", startResult,
                maxRows);
        return new LinkedHashSet<BankMaster>(query.getResultList());
    }

    /**
     * Used to determine whether or not to merge the entity or persist the
     * entity when calling Store
     * 
     * @see store
     * 
     *
     */
    public boolean canBeMerged(BankMaster entity) {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<BankMaster> findBankMasterPaging(Integer start, Integer end,
            Integer orderColumn, String orderBy, String keyword)
            throws DataAccessException {
    	Query query = createQuery("select myBankMaster from BankMaster myBankMaster where lower(myBankMaster.code) like lower(CONCAT('%',?1, '%')) or "
					+ "lower(myBankMaster.name) like lower(CONCAT('%',?1, '%')) order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
    	query.setParameter(1, keyword);
        return new LinkedHashSet<BankMaster>(query.getResultList());
    }

    @Transactional
    public int countBankMasterFilter(Integer start, Integer length,
            Integer orderColumn, String orderBy, String keyword) {
        Query query = createNamedQuery("findBankMasterPaging", -1, -1,keyword);
        return query.getResultList().size();
    }
}
