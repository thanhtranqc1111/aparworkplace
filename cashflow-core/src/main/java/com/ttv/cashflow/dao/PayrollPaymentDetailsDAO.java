
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PayrollPaymentDetails;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PayrollPaymentDetails entities.
 * 
 */
public interface PayrollPaymentDetailsDAO extends JpaDao<PayrollPaymentDetails> {

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPaymentId
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByPaymentId(BigInteger paymentId) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPaymentId
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByPaymentId(BigInteger paymentId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByModifiedDate
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByModifiedDate
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsById
	 *
	 */
	public PayrollPaymentDetails findPayrollPaymentDetailsById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsById
	 *
	 */
	public PayrollPaymentDetails findPayrollPaymentDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPrimaryKey
	 *
	 */
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPrimaryKey
	 *
	 */
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByCreatedDate
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByCreatedDate
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByNetPaymentAmount
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByNetPaymentAmount(java.math.BigDecimal netPaymentAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollPaymentDetailsByNetPaymentAmount
	 *
	 */
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByNetPaymentAmount(BigDecimal netPaymentAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollPaymentDetailss
	 *
	 */
	public Set<PayrollPaymentDetails> findAllPayrollPaymentDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollPaymentDetailss
	 *
	 */
	public Set<PayrollPaymentDetails> findAllPayrollPaymentDetailss(int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * get total net payment amount by payroll id without payment amount (total net payment amount - payment amount)
	 * @param paymentId
	 * @param payrollId
	 * @return
	 */
	public BigDecimal getTotalNetPaymentAmountOriginalByPayrollId(BigInteger paymentId, BigInteger payrollId);

}