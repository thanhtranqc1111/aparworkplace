
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApInvoicePaymentDetailsDAO;
import com.ttv.cashflow.domain.ApInvoicePaymentDetails;

/**
 * DAO to manage ApInvoicePaymentDetails entities.
 * 
 */
@Repository("ApInvoicePaymentDetailsDAO")

@Transactional
public class ApInvoicePaymentDetailsDAOImpl extends AbstractJpaDao<ApInvoicePaymentDetails>
		implements ApInvoicePaymentDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO. Typically a DAO manages a single
	 * entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] { ApInvoicePaymentDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoicePaymentDetailsDAOImpl
	 *
	 */
	public ApInvoicePaymentDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankName
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankName(String payeeBankName)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByPayeeBankName(payeeBankName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankName(String payeeBankName,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPayeeBankName", startResult, maxRows,
				payeeBankName);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAmount
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByVarianceAmount(varianceAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByVarianceAmount", startResult, maxRows,
				varianceAmount);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNo
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNo(String payeeBankAccNo)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByPayeeBankAccNo(payeeBankAccNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNo(String payeeBankAccNo,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPayeeBankAccNo", startResult, maxRows,
				payeeBankAccNo);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsById
	 *
	 */
	@Transactional
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsById(BigInteger id) throws DataAccessException {

		return findApInvoicePaymentDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsById
	 *
	 */

	@Transactional
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsById(BigInteger id, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoicePaymentDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoicePaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentRate
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByPaymentRate(paymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPaymentRate", startResult, maxRows, paymentRate);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoicePaymentDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoicePaymentDetails findApInvoicePaymentDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows)
			throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoicePaymentDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoicePaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankNameContaining(String payeeBankName)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByPayeeBankNameContaining(payeeBankName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankNameContaining(String payeeBankName,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPayeeBankNameContaining", startResult, maxRows,
				payeeBankName);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByCreatedDate(java.util.Calendar createdDate)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByCreatedDate(java.util.Calendar createdDate,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCode
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCode(String varianceAccountCode)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByVarianceAccountCode(varianceAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCode(String varianceAccountCode,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByVarianceAccountCode", startResult, maxRows,
				varianceAccountCode);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderConvertedAmount(
			java.math.BigDecimal paymentOrderConvertedAmount) throws DataAccessException {

		return findApInvoicePaymentDetailsByPaymentOrderConvertedAmount(paymentOrderConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderConvertedAmount(
			java.math.BigDecimal paymentOrderConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPaymentOrderConvertedAmount", startResult, maxRows,
				paymentOrderConvertedAmount);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByModifiedDate(java.util.Calendar modifiedDate)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByModifiedDate(java.util.Calendar modifiedDate,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNoContaining
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNoContaining(String payeeBankAccNo)
			throws DataAccessException {

		return findApInvoicePaymentDetailsByPayeeBankAccNoContaining(payeeBankAccNo, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPayeeBankAccNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPayeeBankAccNoContaining(String payeeBankAccNo,
			int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPayeeBankAccNoContaining", startResult, maxRows,
				payeeBankAccNo);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApInvoicePaymentDetailss
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss() throws DataAccessException {

		return findAllApInvoicePaymentDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoicePaymentDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findAllApInvoicePaymentDetailss(int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoicePaymentDetailss", startResult, maxRows);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount(
			java.math.BigDecimal paymentOrderUnconvertedAmount) throws DataAccessException {

		return findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount(paymentOrderUnconvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount(
			java.math.BigDecimal paymentOrderUnconvertedAmount, int startResult, int maxRows)
			throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByPaymentOrderUnconvertedAmount", startResult,
				maxRows, paymentOrderUnconvertedAmount);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCodeContaining(
			String varianceAccountCode) throws DataAccessException {

		return findApInvoicePaymentDetailsByVarianceAccountCodeContaining(varianceAccountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentDetailsByVarianceAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentDetails> findApInvoicePaymentDetailsByVarianceAccountCodeContaining(
			String varianceAccountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentDetailsByVarianceAccountCodeContaining", startResult,
				maxRows, varianceAccountCode);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity
	 * when calling Store
	 * 
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoicePaymentDetails entity) {
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<ApInvoicePaymentDetails> findApInvPayDetailIdByPaymentId(BigInteger paymentId) {
		try {
			Query query = createNamedQuery("findApInvoicePaymentDetailsByPaymentId", -1, -1, paymentId);
			return new ArrayList<ApInvoicePaymentDetails>(query.getResultList());
		} catch (NoResultException nre) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<ApInvoicePaymentDetails> findApInvPayDetailIdByPaymentIdInc(List<BigInteger> paymentIds) {
		// TODO Auto-generated method stub
		Query query = createQuery("select * from ap_invoice_payment_details where payment_id in (:ids)", -1, -1);
		query.setParameter("ids", paymentIds);
		return new LinkedHashSet<ApInvoicePaymentDetails>(query.getResultList());
	}

	@Override
	public BigDecimal getTotalPaymentOrderUnconvertedAmountByApInvoiceId(BigInteger apInvoiceId) {
		// TODO Auto-generated method stub
		try {
			StringBuilder queryString = new StringBuilder("select sum(a.payment_include_gst_original_amount) as total " + 
														  "from ap_invoice_payment_details a, payment_order p " + 
														  "where a.payment_id = p.id and p.status = '001' and a.ap_invoice_id = ?1 " + 
														  "group by a.ap_invoice_id");
			Query query = getEntityManager().createNativeQuery(queryString.toString()).setParameter(1, apInvoiceId);
			return (BigDecimal)query.getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return new BigDecimal(0);
	}
	
	public BigDecimal getTotalOriginalAmountByApInvoiceId(BigInteger paymentId, BigInteger apInvoiceId){
		Query query = createNamedQuery("findTotalOriginalAmountByApInvoiceId", -1, -1, paymentId, apInvoiceId);
		BigDecimal total = (BigDecimal) query.getSingleResult();
		return total;
	}
	
}
