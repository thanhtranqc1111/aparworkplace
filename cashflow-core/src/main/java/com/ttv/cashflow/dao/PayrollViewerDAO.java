
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PayrollViewer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PayrollViewer entities.
 * 
 */
public interface PayrollViewerDAO extends JpaDao<PayrollViewer> {

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment2(BigDecimal laborAdditionalPayment2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer2(BigDecimal laborSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDescriptionContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDescriptionContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee2(BigDecimal deductionSocialInsuranceEmployee2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer1(BigDecimal deductionSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonth
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonth(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonth
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonth(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCode
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByEmployeeCode(String employeeCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCode
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByEmployeeCode(String employeeCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther3(java.math.BigDecimal deductionOther3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther3(BigDecimal deductionOther3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCode
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCode
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByFxRate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByFxRate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPayrollNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPayrollNo(String payrollNo) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPayrollNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPayrollNo(String payrollNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonthAfter
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonthAfter(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonthAfter
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonthAfter(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollViewers
	 *
	 */
	public Set<PayrollViewer> findAllPayrollViewers() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrollViewers
	 *
	 */
	public Set<PayrollViewer> findAllPayrollViewers(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer2(BigDecimal deductionSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByNetPaymentConverted(java.math.BigDecimal netPaymentConverted) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByNetPaymentConverted(BigDecimal netPaymentConverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPrimaryKey
	 *
	 */
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPrimaryKey
	 *
	 */
	public PayrollViewer findPayrollViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment1(BigDecimal laborAdditionalPayment1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDate(java.util.Calendar paymentScheduleDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDate(Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalLaborCost
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalLaborCost(java.math.BigDecimal totalLaborCost) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalLaborCost
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalLaborCost(BigDecimal totalLaborCost, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCodeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByEmployeeCodeContaining(String employeeCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByEmployeeCodeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByEmployeeCodeContaining(String employeeCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDivisionContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDivisionContaining(String division) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDivisionContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDivisionContaining(String division, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentOrderNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentOrderNo(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentOrderNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByValueDate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByValueDate(java.math.BigDecimal valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByValueDate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByValueDate(BigDecimal valueDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther1(java.math.BigDecimal deductionOther1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther1(BigDecimal deductionOther1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDescription
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDescription
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborAdditionalPayment3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborAdditionalPayment3(BigDecimal laborAdditionalPayment3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment2(BigDecimal laborOtherPayment2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee3(BigDecimal deductionSocialInsuranceEmployee3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateAfter
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateAfter
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateAfter(Calendar paymentScheduleDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByRoleTitle
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByRoleTitle(String roleTitle) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByRoleTitle
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerById
	 *
	 */
	public PayrollViewer findPayrollViewerById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerById
	 *
	 */
	public PayrollViewer findPayrollViewerById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountOriginal(java.math.BigDecimal paymentAmountOriginal) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountOriginal(BigDecimal paymentAmountOriginal, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer3(BigDecimal laborSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByNetPaymentOriginal(java.math.BigDecimal netPaymentOriginal) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByNetPaymentOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByNetPaymentOriginal(BigDecimal netPaymentOriginal, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployer3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployer3(BigDecimal deductionSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNoContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByInvoiceNoContaining(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNoContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentConverted(java.math.BigDecimal totalNetPaymentConverted) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentConverted(BigDecimal totalNetPaymentConverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPayrollNoContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPayrollNoContaining(String payrollNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPayrollNoContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPayrollNoContaining(String payrollNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByRoleTitleContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByRoleTitleContaining(String roleTitle_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByRoleTitleContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByRoleTitleContaining(String roleTitle_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborSocialInsuranceEmployer1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborSocialInsuranceEmployer1(BigDecimal laborSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateBefore
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentScheduleDateBefore
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentScheduleDateBefore(Calendar paymentScheduleDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentOriginal(java.math.BigDecimal totalNetPaymentOriginal) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalNetPaymentOriginal
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalNetPaymentOriginal(BigDecimal totalNetPaymentOriginal, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentRemainAmount
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentRemainAmount(java.math.BigDecimal paymentRemainAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentRemainAmount
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentRemainAmount(BigDecimal paymentRemainAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborBaseSalaryPayment
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborBaseSalaryPayment
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborBaseSalaryPayment(BigDecimal laborBaseSalaryPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTeam
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTeam(String team) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTeam
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTeam(String team, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther2(java.math.BigDecimal deductionOther2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionOther2
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionOther2(BigDecimal deductionOther2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonthBefore
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonthBefore(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByMonthBefore
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByMonthBefore(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionSocialInsuranceEmployee1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionSocialInsuranceEmployee1(BigDecimal deductionSocialInsuranceEmployee1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByClaimTypeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByClaimTypeContaining(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByClaimTypeContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTeamContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTeamContaining(String team_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTeamContaining
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTeamContaining(String team_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionWht
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionWht(java.math.BigDecimal deductionWht) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDeductionWht
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDeductionWht(BigDecimal deductionWht, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountConverted(java.math.BigDecimal paymentAmountConverted) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentAmountConverted
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentAmountConverted(BigDecimal paymentAmountConverted, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment3
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment3(BigDecimal laborOtherPayment3, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentRate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByPaymentRate
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByClaimType
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByClaimType(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByClaimType
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByClaimType(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByInvoiceNo(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByInvoiceNo
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByInvoiceNo(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDivision
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDivision(String division_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByDivision
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByDivision(String division_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalDeduction
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalDeduction(java.math.BigDecimal totalDeduction) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByTotalDeduction
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByTotalDeduction(BigDecimal totalDeduction, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollViewerByLaborOtherPayment1
	 *
	 */
	public Set<PayrollViewer> findPayrollViewerByLaborOtherPayment1(BigDecimal laborOtherPayment1, int startResult, int maxRows) throws DataAccessException;
	
	/**
    * REFRESH MATERIALIZED VIEW payroll_viewer
    */
   public void refresh() throws DataAccessException;

}