
package com.ttv.cashflow.dao;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.SubsidiaryUserDetail;

/**
 * DAO to manage SubsidiaryUserDetail entities.
 * 
 */
public interface SubsidiaryUserDetailDAO extends JpaDao<SubsidiaryUserDetail> {

	/**
	 * JPQL Query - findSubsidiaryUserDetailByModifiedDate
	 *
	 */
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailByModifiedDate
	 *
	 */
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailById
	 *
	 */
	public SubsidiaryUserDetail findSubsidiaryUserDetailById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailById
	 *
	 */
	public SubsidiaryUserDetail findSubsidiaryUserDetailById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailByCreatedDate
	 *
	 */
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailByCreatedDate
	 *
	 */
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailByPrimaryKey
	 *
	 */
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryUserDetailByPrimaryKey
	 *
	 */
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllSubsidiaryUserDetails
	 *
	 */
	public Set<SubsidiaryUserDetail> findAllSubsidiaryUserDetails() throws DataAccessException;

	/**
	 * JPQL Query - findAllSubsidiaryUserDetails
	 *
	 */
	public Set<SubsidiaryUserDetail> findAllSubsidiaryUserDetails(int startResult, int maxRows) throws DataAccessException;
	
	public Set<SubsidiaryUserDetail> findSubsidiaryUserBySubsidiaryIdAndUserId(Integer subsidiaryId, Integer userId);

	/**
	 * JPQL Query - findSubsidiaryUserDetailByUserId
	 *
	 */
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByUserId(Integer userId) throws DataAccessException;}