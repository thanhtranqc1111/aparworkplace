
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ttv.cashflow.dao.ApInvoiceClassDetailsDAO;
import com.ttv.cashflow.dao.ApprovalPurchaseRequestDAO;
import com.ttv.cashflow.dao.PayrollDetailsDAO;
import com.ttv.cashflow.dao.ReimbursementDetailsDAO;
import com.ttv.cashflow.domain.ApprovalPurchaseRequest;

/**
 * DAO to manage ApprovalPurchaseRequest entities.
 * 
 */
@Repository("ApprovalPurchaseRequestDAO")

@Transactional
public class ApprovalPurchaseRequestDAOImpl extends AbstractJpaDao<ApprovalPurchaseRequest>
		implements ApprovalPurchaseRequestDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(2, "approval_code");
    	mapColumnIndex.put(3, "application_date");
    	mapColumnIndex.put(6, "include_gst_original_amount");
    	mapColumnIndex.put(9, "approved_date");
    }
    @Autowired
    private ApInvoiceClassDetailsDAO apInvoiceClassDetailsDAO;
    @Autowired
    private PayrollDetailsDAO payrollDetailsDAO;
    @Autowired
    private ReimbursementDetailsDAO reimbursementDetailsDAO;
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApprovalPurchaseRequest.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApprovalPurchaseRequestDAOImpl
	 *
	 */
	public ApprovalPurchaseRequestDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestById
	 *
	 */
	@Transactional
	public ApprovalPurchaseRequest findApprovalPurchaseRequestById(BigInteger id) throws DataAccessException {

		return findApprovalPurchaseRequestById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestById
	 *
	 */

	@Transactional
	public ApprovalPurchaseRequest findApprovalPurchaseRequestById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalPurchaseRequestById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApprovalPurchaseRequest) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurposeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurposeContaining(String purpose) throws DataAccessException {

		return findApprovalPurchaseRequestByPurposeContaining(purpose, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurposeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurposeContaining(String purpose, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByPurposeContaining", startResult, maxRows, purpose);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findApprovalPurchaseRequestByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToBefore
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToBefore(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityToBefore(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToBefore(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityToBefore", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateBefore
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateBefore(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApplicationDateBefore(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateBefore(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApplicationDateBefore", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToAfter
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToAfter(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityToAfter(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityToAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityToAfter(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityToAfter", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicant
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicant(String applicant) throws DataAccessException {

		return findApprovalPurchaseRequestByApplicant(applicant, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicant
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicant(String applicant, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApplicant", startResult, maxRows, applicant);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApprovalPurchaseRequests
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests() throws DataAccessException {

		return findAllApprovalPurchaseRequests(-1, -1);
	}

	/**
	 * JPQL Query - findAllApprovalPurchaseRequests
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findAllApprovalPurchaseRequests(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApprovalPurchaseRequests", startResult, maxRows);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateBefore
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateBefore(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovedDateBefore(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateBefore(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovedDateBefore", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDate
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDate(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovedDate(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDate(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovedDate", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findApprovalPurchaseRequestByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalTypeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalTypeContaining(String approvalType) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovalTypeContaining(approvalType, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalTypeContaining(String approvalType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovalTypeContaining", startResult, maxRows, approvalType);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurpose
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurpose(String purpose) throws DataAccessException {

		return findApprovalPurchaseRequestByPurpose(purpose, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPurpose
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByPurpose(String purpose, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByPurpose", startResult, maxRows, purpose);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findApprovalPurchaseRequestByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDate
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDate(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApplicationDate(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDate(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApplicationDate", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalType
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalType(String approvalType) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovalType(approvalType, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalType(String approvalType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovalType", startResult, maxRows, approvalType);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCode(String approvalCode) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCode
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCode(String currencyCode) throws DataAccessException {

		return findApprovalPurchaseRequestByCurrencyCode(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByCurrencyCode", startResult, maxRows, currencyCode);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPrimaryKey
	 *
	 */
	@Transactional
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApprovalPurchaseRequestByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByPrimaryKey
	 *
	 */

	@Transactional
	public ApprovalPurchaseRequest findApprovalPurchaseRequestByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalPurchaseRequestByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApprovalPurchaseRequest) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicantContaining
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicantContaining(String applicant) throws DataAccessException {

		return findApprovalPurchaseRequestByApplicantContaining(applicant, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicantContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicantContaining(String applicant, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApplicantContaining", startResult, maxRows, applicant);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromBefore
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromBefore(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityFromBefore(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromBefore(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityFromBefore", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromAfter
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromAfter(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityFromAfter(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFromAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFromAfter(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityFromAfter", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFrom
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFrom(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityFrom(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityFrom
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityFrom(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityFrom", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateAfter
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateAfter(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApplicationDateAfter(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApplicationDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApplicationDateAfter(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApplicationDateAfter", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCodeContaining(String currencyCode) throws DataAccessException {

		return findApprovalPurchaseRequestByCurrencyCodeContaining(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByCurrencyCodeContaining(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByCurrencyCodeContaining", startResult, maxRows, currencyCode);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateAfter
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateAfter(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalPurchaseRequestByApprovedDateAfter(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByApprovedDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByApprovedDateAfter(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByApprovedDateAfter", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityTo
	 *
	 */
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityTo(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalPurchaseRequestByValidityTo(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalPurchaseRequestByValidityTo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalPurchaseRequest> findApprovalPurchaseRequestByValidityTo(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalPurchaseRequestByValidityTo", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalPurchaseRequest>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApprovalPurchaseRequest entity) {
		return true;
	}

	@Override
	public int countApprovalFilter(Integer start, Integer end, String approvalType, String approvalCode,
									Calendar validityFrom, Calendar validityTo, String status) {
		StringBuilder queryString = new StringBuilder("select count(1) from ApprovalPurchaseRequest myApproval "
				+ "where approval_type = coalesce(cast(:approvalType as text),approval_type) "
				+ "and lower(approval_code) like lower(:approvalCode) ");
		if(validityFrom != null)
		{
			queryString.append("and validity_from >= cast(:validityFrom as date) ");
		}
		if(validityTo != null)
		{
			queryString.append("and validity_to <= cast(:validityTo as date) ");
		}
		if(!StringUtils.isEmpty(status))
		{
			queryString.append("and status = :status ");
		}
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter("approvalType", (approvalType != null && approvalType.isEmpty()) ? null : approvalType);
		query.setParameter("approvalCode", "%" + (approvalCode == null ? "" : approvalCode) + "%");
		if(validityFrom != null)
		{
			query.setParameter("validityFrom", validityFrom);
		}
		if(validityTo != null)
		{
			query.setParameter("validityTo", validityTo);
		}
		if(!StringUtils.isEmpty(status))
		{
			query.setParameter("status", status);
		}
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public int countApprovalAll() {
		Query query = createNamedQuery("countApprovalPurchaseRequestAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<ApprovalPurchaseRequest> findApprovalPaging(Integer start, Integer end, String approvalType,
														   String approvalCode, Calendar validityFrom, Calendar validityTo, String status, Integer orderColumn, String orderBy) {
		StringBuilder queryString = new StringBuilder("select myApprovalPurchaseRequest from ApprovalPurchaseRequest myApprovalPurchaseRequest "
													+ "where approval_type = coalesce(cast(:approvalType as text),approval_type) "
													+ "and lower(approval_code) like lower(:approvalCode) ");
		if(validityFrom != null)
		{
			queryString.append("and approved_date >= cast(:approved_date_from as date) ");
		}
		if(validityTo != null)
		{
			queryString.append("and approved_date <= cast(:approved_date_to as date) ");
		}
		if(!StringUtils.isEmpty(status))
		{
			queryString.append("and status = :status ");
		}
		if(orderColumn == -1)
		{
			queryString.append("order by approved_date asc");
		}
		else {
			queryString.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		}
		
		Query query = createQuery(queryString.toString(), start, end);
		
		query.setParameter("approvalType", (approvalType != null && approvalType.isEmpty()) ? null : approvalType);
		query.setParameter("approvalCode", "%" + (approvalCode == null ? "" : approvalCode) + "%");
		if(validityFrom != null)
		{
			query.setParameter("approved_date_from", validityFrom);
		}
		if(validityTo != null)
		{
			query.setParameter("approved_date_to", validityTo);
		}
		if(!StringUtils.isEmpty(status))
		{
			query.setParameter("status", status);
		}
		Set<ApprovalPurchaseRequest> resultSet = new LinkedHashSet<>(query.getResultList());
		for(ApprovalPurchaseRequest approvalPurchaseRequest: resultSet) {
			approvalPurchaseRequest.setApInVoicesApplied(apInvoiceClassDetailsDAO.findArInvoiceNoByApprovalCode(approvalPurchaseRequest.getApprovalCode()));
			approvalPurchaseRequest.setPayrollsApplied(payrollDetailsDAO.findPayrollNoByApprovalCode(approvalPurchaseRequest.getApprovalCode()));
			approvalPurchaseRequest.setReimbursementsApplied(reimbursementDetailsDAO.findReimbursementNoByApprovalCode(approvalPurchaseRequest.getApprovalCode()));
		}
		return resultSet;
	}

	@Override
	public int checkExists(ApprovalPurchaseRequest approval) {
		Query query = createNamedQuery("checkApprovalPurchaseRequestExists", -1, -1, approval.getApprovalCode(), approval.getIncludeGstOriginalAmount());
		if(!query.getResultList().isEmpty())
		{
			return ((Number)query.getSingleResult()).intValue();
		}
		else
		{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalPurchaseRequest> findApprovalAvailable(String approvalCode, String bookingDate) throws DataAccessException {
		String queryString = new StringBuilder()
			                .append("SELECT myApproval FROM ApprovalPurchaseRequest myApproval WHERE lower(myApproval.approvalCode) like lower(CONCAT('%', ?1, '%')) ")
			                .append("and myApproval.validityFrom <= to_timestamp(?2, 'dd/MM/yyyy')")
			                .append("and myApproval.validityTo  >= to_timestamp(?2, 'dd/MM/yyyy')")
			                .toString();

		Query query = getEntityManager().createQuery(queryString)
		                        .setParameter(1, approvalCode)
		                        .setParameter(2, bookingDate);
		
		return new ArrayList<ApprovalPurchaseRequest>(query.getResultList());
	}
}
