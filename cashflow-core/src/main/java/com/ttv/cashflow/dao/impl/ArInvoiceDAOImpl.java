
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ArInvoiceDAO;
import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.dto.ArInvoiceDTO;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;

/**
 * DAO to manage ArInvoice entities.
 * 
 */
@Repository("ArInvoiceDAO")

@Transactional
public class ArInvoiceDAOImpl extends AbstractJpaDao<ArInvoice> implements ArInvoiceDAO {

	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "myArInvoice.ar_invoice_no");
    	mapColumnIndex.put(1, "myArInvoice.month");
    	mapColumnIndex.put(4, "myArInvoice.payer_name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArInvoice.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArInvoiceDAOImpl
	 *
	 */
	public ArInvoiceDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArInvoiceById
	 *
	 */
	@Transactional
	public ArInvoice findArInvoiceById(BigInteger id) throws DataAccessException {

		return findArInvoiceById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceById
	 *
	 */

	@Transactional
	public ArInvoice findArInvoiceById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoice) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateBefore
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceByInvoiceIssuedDateBefore(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByInvoiceIssuedDateBefore", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByPrimaryKey
	 *
	 */
	@Transactional
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArInvoiceByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByPrimaryKey
	 *
	 */

	@Transactional
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoice) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByExcludeGstConvertedAmount", startResult, maxRows, excludeGstConvertedAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByPayerName
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByPayerName(String payerName) throws DataAccessException {

		return findArInvoiceByPayerName(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByPayerName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByPayerName", startResult, maxRows, payerName);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByPayerNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByPayerNameContaining(String payerName) throws DataAccessException {

		return findArInvoiceByPayerNameContaining(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByPayerNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByPayerNameContaining(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByPayerNameContaining", startResult, maxRows, payerName);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByDescriptionContaining(String description) throws DataAccessException {

		return findArInvoiceByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByGstRate
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findArInvoiceByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableName
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableName(String accountReceivableName) throws DataAccessException {

		return findArInvoiceByAccountReceivableName(accountReceivableName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableName(String accountReceivableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByAccountReceivableName", startResult, maxRows, accountReceivableName);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException {

		return findArInvoiceByGstConvertedAmount(gstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByGstConvertedAmount", startResult, maxRows, gstConvertedAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNo
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByArInvoiceNo(String arInvoiceNo) throws DataAccessException {

		return findArInvoiceByArInvoiceNo(arInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByArInvoiceNo(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByArInvoiceNo", startResult, maxRows, arInvoiceNo);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException {

		return findArInvoiceByGstOriginalAmount(gstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByGstOriginalAmount", startResult, maxRows, gstOriginalAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findArInvoiceByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByApprovalCode
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByApprovalCode(String approvalCode) throws DataAccessException {

		return findArInvoiceByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByClaimTypeContaining(String claimType) throws DataAccessException {

		return findArInvoiceByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByDescription
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByDescription(String description) throws DataAccessException {

		return findArInvoiceByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findArInvoiceByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDate
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceByInvoiceIssuedDate(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByInvoiceIssuedDate", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByArInvoiceNoContaining(String arInvoiceNo) throws DataAccessException {

		return findArInvoiceByArInvoiceNoContaining(arInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByArInvoiceNoContaining(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByArInvoiceNoContaining", startResult, maxRows, arInvoiceNo);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCode
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableCode(String accountReceivableCode) throws DataAccessException {

		return findArInvoiceByAccountReceivableCode(accountReceivableCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableCode(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByAccountReceivableCode", startResult, maxRows, accountReceivableCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByMonth
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonth(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByCreatedDate
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findArInvoiceByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableNameContaining(String accountReceivableName) throws DataAccessException {

		return findArInvoiceByAccountReceivableNameContaining(accountReceivableName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableNameContaining(String accountReceivableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByAccountReceivableNameContaining", startResult, maxRows, accountReceivableName);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllArInvoices
	 *
	 */
	@Transactional
	public Set<ArInvoice> findAllArInvoices() throws DataAccessException {

		return findAllArInvoices(-1, -1);
	}

	/**
	 * JPQL Query - findAllArInvoices
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findAllArInvoices(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArInvoices", startResult, maxRows);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableCodeContaining(String accountReceivableCode) throws DataAccessException {

		return findArInvoiceByAccountReceivableCodeContaining(accountReceivableCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByAccountReceivableCodeContaining(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByAccountReceivableCodeContaining", startResult, maxRows, accountReceivableCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByGstType
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstType(String gstType) throws DataAccessException {

		return findArInvoiceByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByModifiedDate
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findArInvoiceByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByMonthAfter
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByClaimType
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByClaimType(String claimType) throws DataAccessException {

		return findArInvoiceByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateAfter
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceByInvoiceIssuedDateAfter(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByInvoiceIssuedDateAfter", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstTypeContaining(String gstType) throws DataAccessException {

		return findArInvoiceByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByFxRate
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findArInvoiceByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByMonthBefore
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findArInvoiceByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ArInvoice>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArInvoice entity) {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ArInvoiceReceiptOrderDTO> findArInvoiceToMatchRecepitOrder(Integer start, Integer end, String fromApNo, String toApNo,
															   String fromMonth, String toMonth, String payerName, boolean isApproved, BigDecimal credit,
															   Integer option, boolean isNotReceived) throws DataAccessException {
		StringBuilder queryString = new StringBuilder("SELECT myArInvoice.id, " + 
															 "myArInvoice.payer_name as payerName, " + 
															 "myArInvoice.ar_invoice_no as arInvoiceNo, " + 
															 "myArInvoice.original_currency_code as originalCurrencyCode, " + 
															 "myArInvoice.fx_rate as fxRate, gst_rate as gstRate, " + 
															 "myArInvoice.invoice_issued_date as invoiceIssuedDate, " + 
															 "myArInvoice.month as month, claim_type as claimType , " + 
															 "myArInvoice.gst_type as gstType, " + 
															 "myArInvoice.approval_code as approvalCode, " + 
															 "myArInvoice.account_receivable_code as accountReceivableCode, " + 
															 "myArInvoice.account_receivable_name as accountReceivableName, " + 
															 "myArInvoice.description as description, " + 
															 "myArInvoice.exclude_gst_original_amount as excludeGstOriginalAmount, " + 
															 "myArInvoice.exclude_gst_converted_amount as excludeGstConvertedAmount, " + 
															 "myArInvoice.gst_original_amount as gstOriginalAmount, " + 
															 "myArInvoice.gst_converted_amount as gstConvertedAmount, " + 
															 "myArInvoice.include_gst_original_amount as includeGstConvertedAmount, " + 
															 "myArInvoice.include_gst_original_amount as includeGstOriginalAmount, " +
															 "myArInvoiceReceiptStatus.remain_include_gst_original_amount as remainIncludeGstOriginalAmount, " +
															 "CASE WHEN myARInfo.totalRecordNotApproved is null THEN true ELSE false END AS isApproved, " + 
															 "myArInvoice.invoice_no as invoiceNo " +
													  "FROM ar_invoice myArInvoice " +
													  "LEFT OUTER JOIN ar_invoice_receipt_status myArInvoiceReceiptStatus on myArInvoice.id = myArInvoiceReceiptStatus.ar_invoice_id " +
													  "LEFT OUTER JOIN (" + 
														  "SELECT DISTINCT araccount.ar_invoice_id, 1 as totalRecordNotApproved " + 
														  "FROM   ar_invoice_account_details araccount, " + 
														  		 "ar_invoice_class_details arclass  " + 
														  "WHERE  araccount.id = arclass.ar_invoice_account_details_id " + 
																  "AND( " + 
																  "arclass.is_approval = false " + 
																  "OR " + 
																  "arclass.is_approval IS NULL " + 
																  ") " + 
													  ") myARInfo ON myARInfo.ar_invoice_id = myarinvoice.id " + 
												      "WHERE (?1 = '' or ?1 <= myArInvoice.ar_invoice_no) and (?2 = '' or ?2 >= myArInvoice.ar_invoice_no) " +
												      	     "and ((?3 = '') or myArInvoice.month >= to_timestamp(?3, 'MM/yyyy')) " +
												      	     "and ((?4 = '') or myArInvoice.month < to_timestamp(?4, 'MM/yyyy') + interval '1 day') " +
													  		 "and ((?5 = '') or lower(myArInvoice.payer_name) like lower(CONCAT('%', ?5, '%'))) " );
		if (credit != null) {
			switch (option) {
			case 0:
				queryString.append("and myArInvoice.include_gst_original_amount = ?6 ");
				break;
			case 1:
				queryString.append("and myArInvoice.include_gst_original_amount >= ?6 ");
				break;
			case -1:
				queryString.append("and myArInvoice.include_gst_original_amount <= ?6 ");
				break;
			default:
				break;
			}
		}
		if(isApproved) {
			queryString.append("and myARInfo.totalRecordNotApproved is null ");
		}
		if(isNotReceived) {
			queryString.append("and (myArInvoiceReceiptStatus.remain_include_gst_original_amount > 0 or myArInvoiceReceiptStatus.remain_include_gst_original_amount is null) ");
		}
		Query query = getEntityManager().createNativeQuery(queryString.toString(), "ArInvoiceResult");
		query.setParameter(1, fromApNo);
		query.setParameter(2, toApNo);
		query.setParameter(3, fromMonth);
		query.setParameter(4, toMonth);
		query.setParameter(5, payerName);
		if (credit != null) {
			query.setParameter(6, credit);
		}
		return query.getResultList();
	}

	@Override
	public int countArInvoiceToMatchRecepitOrder(String fromApNo, String toApNo,
												 String fromMonth, String toMonth, String payerName, boolean isApproved, BigDecimal credit,
												 Integer option) throws DataAccessException {
		StringBuilder queryString = new StringBuilder("select count(1) from ArInvoice myArInvoice");
		Query query = createQuery(queryString.toString(), -1, -1);
		return ((Number) query.getSingleResult()).intValue();
	}
	
	public BigInteger searchCount(String fromMonth, String toMonth, String invoice, String payerName, String status, String fromArNo, String toArNo){
		StringBuilder builder = new StringBuilder();
		builder.append("select count(distinct myArInvoice.ar_invoice_no) " )
		.append("from ar_invoice myArInvoice ")
		.append("left join receipt_order myReceiptOrder on  myReceiptOrder.ar_invoice_id = myArInvoice.id ")
		.append("left join bank_statement myBankStatement on myBankStatement.id = myReceiptOrder.transaction_id ")
		.append("where ((?1 = '') or myArInvoice.month >= to_timestamp(?1, 'MM/yyyy')) ")
		.append("and ((?2 = '') or myArInvoice.month < to_timestamp(?2, 'MM/yyyy') + interval '1 day') ")
		.append("and ((?3 = '') or lower(myArInvoice.invoice_no) like lower(CONCAT('%', ?3, '%'))) ")
		.append("and ((?4 = '') or lower(myArInvoice.payer_name) like lower(CONCAT('%', ?4, '%'))) ")
		.append("and ((?5 = '') or lower(myArInvoice.status) like lower(CONCAT('%', ?5, '%'))) ")
		.append("and (?6 = '' or ?6 <= myArInvoice.ar_invoice_no) and (?7 = '' or ?7 >= myArInvoice.ar_invoice_no) ");
		
		Query query = getEntityManager().createNativeQuery(builder.toString())
				.setParameter(1, fromMonth)
				.setParameter(2, toMonth)
				.setParameter(3, invoice)
				.setParameter(4, payerName)
				.setParameter(5, status)
				.setParameter(6, fromArNo)
    			.setParameter(7, toArNo);
		
		BigInteger count = (BigInteger) query.getSingleResult();
        return count;
	}
	
	@SuppressWarnings("unchecked")
    public List<ArInvoiceDTO> searchArInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
			String invoiceNo, String payerName, String status, String fromArNo, String toArNo, Integer orderColumn, String orderBy) {
		
		StringBuilder builder = new StringBuilder();
		builder.append("select myArInvoice.ar_invoice_no " )
		.append("from ar_invoice myArInvoice ")
		.append("left join receipt_order myReceiptOrder on  myReceiptOrder.ar_invoice_id = myArInvoice.id ")
		.append("left join bank_statement myBankStatement on myBankStatement.id = myReceiptOrder.transaction_id ")
		.append("where ((?1 = '') or myArInvoice.month >= to_timestamp(?1, 'MM/yyyy')) ")
		.append("and ((?2 = '') or myArInvoice.month < to_timestamp(?2, 'MM/yyyy') + interval '1 day') ")
		.append("and ((?3 = '') or lower(myArInvoice.invoice_no) like lower(CONCAT('%', ?3, '%'))) ")
		.append("and ((?4 = '') or lower(myArInvoice.payer_name) like lower(CONCAT('%', ?4, '%'))) ")
		.append("and ((?5 = '') or lower(myArInvoice.status) like lower(CONCAT('%', ?5, '%'))) ")
		.append("and (?6 = '' or ?6 <= myArInvoice.ar_invoice_no) and (?7 = '' or ?7 >= myArInvoice.ar_invoice_no) ")
		.append("group by myArInvoice.month, myArInvoice.ar_invoice_no, myArInvoice.payer_name ");
		
		if (orderColumn >= 0) {
			builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		} else {
			builder.append("order by myArInvoice.ar_invoice_no desc, myArInvoice.month, myArInvoice.payer_name desc");
		}

		Query query = getEntityManager().createNativeQuery(builder.toString())
				.setParameter(1, fromMonth)
				.setParameter(2, toMonth)
				.setParameter(3, invoiceNo)
				.setParameter(4, payerName)
				.setParameter(5, status)
				.setParameter(6, fromArNo)
				.setParameter(7, toArNo);
		
        query.setFirstResult(startResult == null || startResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : startResult);
        if (maxRow != null && maxRow > 0) {
            query.setMaxResults(maxRow);
        }
		return new ArrayList<ArInvoiceDTO>(query.getResultList());
	}
	
	
	@SuppressWarnings("unchecked")
    public List<ArInvoiceDTO> searchARInvoiceReceiptDetail(List<String> arInvoiceNoLst, Integer orderColumn, String orderBy) {
		StringBuilder builder = new StringBuilder();
		 builder.append("select myArInvoice.id as arId, myArInvoice.ar_invoice_no as arInvoiceNo, myArInvoice.month, ")
		 .append("myAccountMaster.name as payerAccount, ")
		 .append("myArInvoice.claim_type as claimType, ")
		 .append("myArInvoice.payer_name as payerName,  ")
		 .append("myArInvoice.invoice_no as invoiceNo, ")
		 .append("myArInvoice.include_gst_original_amount as totalAmount, ")
		 .append("myArInvoiceReceiptStatus.remain_include_gst_original_amount as remainAmount, ")
		 .append("myArInvoice.status as statusCode, ")
		 .append("(select mySystemValue.value from system_value as mySystemValue where mySystemValue.type ='ARI' and mySystemValue.code = myArInvoice.status) as status, ")
		 .append("myBankStatement.id as bankTrans, ")
		 .append("myBankStatement.bank_name as bankName, ")
		 .append("myBankStatement.bank_account_no as bankAccount, ")
		 .append("myBankStatement.transaction_date as transDate,  ")
		 .append("myArInvoice.invoice_issued_date as issuedDate,")
		 .append("(myBankStatement.reference1 || '</br>' || myBankStatement.reference2 ) as  refNo,")
		 .append("myBankStatement.credit as creditAmount, ")
		 .append("myArInvoice.original_currency_code as originalCurrencyCode, ")
		 .append("myBankStatement.currency_code as bankCurrencyCode ")
		 .append("from ar_invoice myArInvoice ")
		 .append("left join receipt_order myReceiptOrder on  myReceiptOrder.ar_invoice_id = myArInvoice.id ")
		 .append("left join bank_statement myBankStatement on myBankStatement.id = myReceiptOrder.transaction_id ")
		 .append("left join ar_invoice_receipt_status myArInvoiceReceiptStatus on myArInvoiceReceiptStatus.ar_invoice_id = myArInvoice.id ")
		 .append("left join payee_payer_master myPayeeMaster on myPayeeMaster.name = myArInvoice.payer_name ")
		 .append("left join account_master myAccountMaster on myAccountMaster.code = myPayeeMaster.account_code  ")
		 .append("where myArInvoice.ar_invoice_no IN (:ar_invoice_no) ");
		 
		if (orderColumn >= 0) {
			builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		} else {
			builder.append("order by myArInvoice.ar_invoice_no desc, myArInvoice.month, myArInvoice.payer_name desc");
		}
		 
		 Query query = getEntityManager().createNativeQuery(builder.toString(), "ARInvoiceReceiptDetail")
               .setParameter("ar_invoice_no", arInvoiceNoLst.size() == 0 ? "" : arInvoiceNoLst);

		 return query.getResultList();
		
    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public ArInvoice findArInvoiceByImportKey(String importKey) {
        ArInvoice arInvoice = null;
        
        String queryString = new StringBuilder()
        .append("select myArInvoice from ArInvoice myArInvoice where myArInvoice.importKey = ?1")
        .toString();
        
        Query query = getEntityManager().createQuery(queryString)
                                        .setParameter(1, importKey);
        
        List<ArInvoice> list = new ArrayList<ArInvoice>(query.getResultList());
        
        if(list.size() > 0){
            arInvoice = list.get(0);
        }
        
        return arInvoice;
    }
	
	@Transactional
    public String getArInvoiceNo(Calendar cal) {
        Query query = getEntityManager().createNativeQuery("SELECT ar_invoice_no(?1)")
                                        .setParameter(1, cal);

        String ret = (String) query.getSingleResult();

        return ret;

    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<ARInvoiceDetail> getARInvoiceDetail(BigInteger arInvoiceId) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" aiad.id AS arInvoiceAccountDetailId,");
        builder.append(" aiad.account_code AS accountCode,");
        builder.append(" aiad.account_name AS accountName,");
        builder.append(" aicd.id AS arInvoiceClassDetailId,");
        builder.append(" aicd.class_code AS classCode,");
        builder.append(" aicd.class_name AS className,");
        builder.append(" aicd.description,");
        builder.append(" aicd.approval_code AS approvalCode,");
        builder.append(" aicd.exclude_gst_original_amount AS excludeGstOriginalAmount,");
        builder.append(" aicd.fx_rate as fxRate,");
        builder.append(" aicd.exclude_gst_converted_amount AS excludeGstConvertedAmount,");
        builder.append(" aicd.gst_type AS gstType,");
        builder.append(" aicd.gst_rate AS gstRate,");
        builder.append(" aicd.gst_original_amount AS gstOriginalAmount,");
        builder.append(" aicd.gst_converted_amount AS gstConvertedAmount,");
        builder.append(" aicd.include_gst_original_amount AS includeGstOriginalAmount,");
        builder.append(" aicd.include_gst_converted_amount AS includeGstConvertedAmount, ");
        builder.append(" aicd.is_approval AS isApproval ");
        //From
        builder.append(" FROM ar_invoice_account_details AS aiad");
        builder.append(" JOIN ar_invoice_class_details AS aicd ");
        builder.append(" ON aiad.id = aicd.ar_invoice_account_details_id");
        //Where
        builder.append(" WHERE aiad.ar_invoice_id = :ar_invoice_id");
        //Order by
        builder.append(" ORDER BY accountCode ASC");
        
        Query query = getEntityManager().createNativeQuery(builder.toString(), "ARInvoiceDetailResult")
                                        .setParameter("ar_invoice_id", arInvoiceId);
        
        return query.getResultList();
        
    }
	
	@Transactional
    public String getArInvoiceNo() {
        Query query = getEntityManager().createNativeQuery("SELECT ar_invoice_no()");

        String ret = (String) query.getSingleResult();

        return ret;
    }
	
	
	@Transactional
    public BigInteger getLastId() {
        String sql = "SELECT id FROM ar_invoice ORDER BY id DESC LIMIT 1";

        Query query = getEntityManager().createNativeQuery(sql);
        
        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;
    }
	
    @Transactional
    public BigInteger countDetail(String arInvoiceNo) throws DataAccessException {
        String sql = new StringBuilder()
                    .append("SELECT count(ari_cls.id) ")
                    .append("FROM ar_invoice ari ")
                    .append("JOIN ar_invoice_account_details ari_acc ON ari_acc.ar_invoice_id = ari.id ")
                    .append("JOIN ar_invoice_class_details ari_cls ON ari_cls.ar_invoice_account_details_id = ari_acc.id ")
                    .append("WHERE ari.ar_invoice_no = :arInvoiceNo ")
                    .toString();
        
        Query query = getEntityManager().createNativeQuery(sql)
                .setParameter("arInvoiceNo", arInvoiceNo);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;
    }
    
    @SuppressWarnings("unchecked")
	public boolean checkUniqueInvoiceNo(String invoiceNo, String id){
    	try {
			Query query = createNamedQuery("findArInvoiceByInvoiceNo", -1, -1, invoiceNo);
			List<ArInvoice> list = new ArrayList<ArInvoice>(query.getResultList());
	        
	        if(list.size() <= 0) {
	        	return false;
	        } else if(list.size() == 1){
	        	// check invoice no belong to ap invoice (id)
	        	if(id.equals(list.get(0).getId().toString())) {
					return false;
				} else {
					return true;
				}
	        } else {
	        	return true;
	        }
		} catch (NoResultException nre) {
			return false;
		}
    }
}
