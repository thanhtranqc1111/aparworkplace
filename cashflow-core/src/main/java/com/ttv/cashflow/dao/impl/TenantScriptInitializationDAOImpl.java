
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.TenantScriptInitializationDAO;
import com.ttv.cashflow.domain.TenantScriptInitialization;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage TenantScriptInitialization entities.
 * 
 */
@Repository("TenantScriptInitializationDAO")

@Transactional
public class TenantScriptInitializationDAOImpl extends AbstractJpaDao<TenantScriptInitialization>
		implements TenantScriptInitializationDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			TenantScriptInitialization.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new TenantScriptInitializationDAOImpl
	 *
	 */
	public TenantScriptInitializationDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllTenantScriptInitializations
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findAllTenantScriptInitializations() throws DataAccessException {

		return findAllTenantScriptInitializations(-1, -1);
	}

	/**
	 * JPQL Query - findAllTenantScriptInitializations
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findAllTenantScriptInitializations(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllTenantScriptInitializations", startResult, maxRows);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByPrimaryKey
	 *
	 */
	@Transactional
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id) throws DataAccessException {

		return findTenantScriptInitializationByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByPrimaryKey
	 *
	 */

	@Transactional
	public TenantScriptInitialization findTenantScriptInitializationByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTenantScriptInitializationByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.TenantScriptInitialization) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByCode
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCode(String code) throws DataAccessException {

		return findTenantScriptInitializationByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByCode", startResult, maxRows, code);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByValueContaining
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValueContaining(String value) throws DataAccessException {

		return findTenantScriptInitializationByValueContaining(value, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByValueContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValueContaining(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByValueContaining", startResult, maxRows, value);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationById
	 *
	 */
	@Transactional
	public TenantScriptInitialization findTenantScriptInitializationById(Integer id) throws DataAccessException {

		return findTenantScriptInitializationById(id, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationById
	 *
	 */

	@Transactional
	public TenantScriptInitialization findTenantScriptInitializationById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTenantScriptInitializationById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.TenantScriptInitialization) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByName
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByName(String name) throws DataAccessException {

		return findTenantScriptInitializationByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByName", startResult, maxRows, name);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByValue
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValue(String value) throws DataAccessException {

		return findTenantScriptInitializationByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByValue(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByValue", startResult, maxRows, value);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByCodeContaining
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCodeContaining(String code) throws DataAccessException {

		return findTenantScriptInitializationByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByNameContaining
	 *
	 */
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByNameContaining(String name) throws DataAccessException {

		return findTenantScriptInitializationByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findTenantScriptInitializationByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<TenantScriptInitialization> findTenantScriptInitializationByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTenantScriptInitializationByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<TenantScriptInitialization>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(TenantScriptInitialization entity) {
		return true;
	}
}
