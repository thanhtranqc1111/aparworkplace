
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.PayeePayerTypeMasterDAO;
import com.ttv.cashflow.domain.PayeePayerTypeMaster;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage PayeePayerTypeMaster entities.
 * 
 */
@Repository("PayeePayerTypeMasterDAO")

@Transactional
public class PayeePayerTypeMasterDAOImpl
        extends AbstractJpaDao<PayeePayerTypeMaster>
        implements PayeePayerTypeMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myPayeePayerTypeMaster.code");
    	mapColumnIndex.put(2, "myPayeePayerTypeMaster.name");
    }
    /**
     * Set of entity classes managed by this DAO. Typically a DAO manages a
     * single entity.
     *
     */
    private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
            Arrays.asList(new Class<?>[] { PayeePayerTypeMaster.class }));

    /**
     * EntityManager injected by Spring for persistence unit CashFlowDriver
     *
     */
    @PersistenceContext(unitName = "CashFlowDriver")
    private EntityManager entityManager;

    /**
     * Instantiates a new PayeePayerTypeMasterDAOImpl
     *
     */
    public PayeePayerTypeMasterDAOImpl() {
        super();
    }

    /**
     * Get the entity manager that manages persistence unit
     *
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Returns the set of entity classes managed by this DAO.
     *
     */
    public Set<Class<?>> getTypes() {
        return dataTypes;
    }

    /**
     * JPQL Query - findAllPayeePayerTypeMasters
     *
     */
    @Transactional
    public Set<PayeePayerTypeMaster> findAllPayeePayerTypeMasters()
            throws DataAccessException {

        return findAllPayeePayerTypeMasters(-1, -1);
    }

    /**
     * JPQL Query - findAllPayeePayerTypeMasters
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<PayeePayerTypeMaster> findAllPayeePayerTypeMasters(
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findAllPayeePayerTypeMasters",
                startResult, maxRows);
        return new LinkedHashSet<PayeePayerTypeMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByCodeContaining
     *
     */
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByCodeContaining(
            String code) throws DataAccessException {

        return findPayeePayerTypeMasterByCodeContaining(code, -1, -1);
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByCodeContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByCodeContaining(
            String code, int startResult, int maxRows)
            throws DataAccessException {
        Query query = createNamedQuery(
                "findPayeePayerTypeMasterByCodeContaining", startResult,
                maxRows, code);
        return new LinkedHashSet<PayeePayerTypeMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByPrimaryKey
     *
     */
    @Transactional
    public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(
            String code) throws DataAccessException {

        return findPayeePayerTypeMasterByPrimaryKey(code, -1, -1);
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByPrimaryKey
     *
     */

    @Transactional
    public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(
            String code, int startResult, int maxRows)
            throws DataAccessException {
        try {
            Query query = createNamedQuery(
                    "findPayeePayerTypeMasterByPrimaryKey", startResult,
                    maxRows, code);
            return (com.ttv.cashflow.domain.PayeePayerTypeMaster) query
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByNameContaining
     *
     */
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByNameContaining(
            String name) throws DataAccessException {

        return findPayeePayerTypeMasterByNameContaining(name, -1, -1);
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByNameContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByNameContaining(
            String name, int startResult, int maxRows)
            throws DataAccessException {
        Query query = createNamedQuery(
                "findPayeePayerTypeMasterByNameContaining", startResult,
                maxRows, name);
        return new LinkedHashSet<PayeePayerTypeMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByCode
     *
     */
    @Transactional
    public PayeePayerTypeMaster findPayeePayerTypeMasterByCode(String code)
            throws DataAccessException {

        return findPayeePayerTypeMasterByCode(code, -1, -1);
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByCode
     *
     */

    @Transactional
    public PayeePayerTypeMaster findPayeePayerTypeMasterByCode(String code,
            int startResult, int maxRows) throws DataAccessException {
        try {
            Query query = createNamedQuery("findPayeePayerTypeMasterByCode",
                    startResult, maxRows, code);
            return (com.ttv.cashflow.domain.PayeePayerTypeMaster) query
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByName
     *
     */
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByName(String name)
            throws DataAccessException {

        return findPayeePayerTypeMasterByName(name, -1, -1);
    }

    /**
     * JPQL Query - findPayeePayerTypeMasterByName
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByName(String name,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findPayeePayerTypeMasterByName",
                startResult, maxRows, name);
        return new LinkedHashSet<PayeePayerTypeMaster>(query.getResultList());
    }

    /**
     * Used to determine whether or not to merge the entity or persist the
     * entity when calling Store
     * 
     * @see store
     * 
     *
     */
    public boolean canBeMerged(PayeePayerTypeMaster entity) {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<PayeePayerTypeMaster> findPayeeTypeMasterPaging(Integer start,
            Integer end, Integer orderColumn, String orderBy, String keyword)
            throws DataAccessException {
        Query query = createQuery("select myPayeePayerTypeMaster from PayeePayerTypeMaster myPayeePayerTypeMaster where lower(myPayeePayerTypeMaster.code) like lower(CONCAT('%', ?1, '%')) or "
		        + "lower(myPayeePayerTypeMaster.name) like lower(CONCAT('%', ?1, '%')) order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
        query.setParameter(1, keyword);
        return new LinkedHashSet<PayeePayerTypeMaster>(query.getResultList());
    }

    @Transactional
    public int countFilter(Integer start, Integer length, Integer orderColumn,
            String orderBy, String keyword) {
        Query query = createNamedQuery("findPayeeTypeMasterPaging", -1,
                -1, keyword);
        return query.getResultList().size();
    }

    private String builderQueryPayeeTypeMaster(Integer start, Integer length,
            Integer orderColumn, String orderBy, String keyword,
            boolean isCount) {
        Map<Integer, String> mapColumn = new HashMap<Integer, String>();
        mapColumn.put(2, "code");
        mapColumn.put(3, "name");
        StringBuilder sbHqlQuery = new StringBuilder();
        if (isCount) {
            sbHqlQuery.append("select count(*)");
        } else {
            sbHqlQuery.append("select p.code as code, p.name as name");
        }
        sbHqlQuery.append(" from PayeePayerTypeMaster p");
        sbHqlQuery.append(" where 1 = 1 ");
        sbHqlQuery.append(" and ( ");
        sbHqlQuery.append(" code LIKE CONCAT('%', ?1, '%')");
        sbHqlQuery.append(" or name LIKE CONCAT('%', ?1, '%')");
        sbHqlQuery.append(" ) ");
        if (!isCount && orderColumn != null
                && StringUtils.isNotBlank(orderBy)) {
            sbHqlQuery.append(" order by ").append(mapColumn.get(orderColumn));
            sbHqlQuery.append(" ").append(orderBy);
        }
        return sbHqlQuery.toString();
    }

}
