
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ApViewerDAO;
import com.ttv.cashflow.domain.ApViewer;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ApViewer entities.
 * 
 */
@Repository("ApViewerDAO")

@Transactional
public class ApViewerDAOImpl extends AbstractJpaDao<ApViewer> implements ApViewerDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApViewer.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApViewerDAOImpl
	 *
	 */
	public ApViewerDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllApViewers
	 *
	 */
	@Transactional
	public Set<ApViewer> findAllApViewers() throws DataAccessException {

		return findAllApViewers(-1, -1);
	}

	/**
	 * JPQL Query - findAllApViewers
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findAllApViewers(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApViewers", startResult, maxRows);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByClassName
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByClassName(String className) throws DataAccessException {

		return findViewerByClassName(className, -1, -1);
	}

	/**
	 * JPQL Query - findViewerByClassName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findViewerByClassName(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByClassName", startResult, maxRows, className);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByApInvoiceNo
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByApInvoiceNo(String apInvoiceNo) throws DataAccessException {

		return findViewerByApInvoiceNo(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findViewerByApInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findViewerByApInvoiceNo(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByApInvoiceNo", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findViewerByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findApViewerByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPayeeNameContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPayeeNameContaining(String payeeName) throws DataAccessException {

		return findViewerByPayeeNameContaining(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findViewerByPayeeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findViewerByPayeeNameContaining(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPayeeNameContaining", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByApprovalCode(String approvalCode) throws DataAccessException {

		return findApViewerByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByGstTypeContaining(String gstType) throws DataAccessException {

		return findApViewerByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByDescriptionContaining(String description) throws DataAccessException {

		return findApViewerByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPaymentRate
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException {

		return findApViewerByPaymentRate(paymentRate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPaymentRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPaymentRate(java.math.BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPaymentRate", startResult, maxRows, paymentRate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByVarianceAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException {

		return findApViewerByVarianceAmount(varianceAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByVarianceAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByVarianceAmount(java.math.BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByVarianceAmount", startResult, maxRows, varianceAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByFxRate
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findApViewerByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByUnpaidIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByUnpaidIncludeGstOriginalAmount(java.math.BigDecimal unpaidIncludeGstOriginalAmount) throws DataAccessException {

		return findApViewerByUnpaidIncludeGstOriginalAmount(unpaidIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByUnpaidIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByUnpaidIncludeGstOriginalAmount(java.math.BigDecimal unpaidIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByUnpaidIncludeGstOriginalAmount", startResult, maxRows, unpaidIncludeGstOriginalAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByValueDateAfter
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByValueDateAfter(java.util.Calendar valueDate) throws DataAccessException {

		return findApViewerByValueDateAfter(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByValueDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByValueDateAfter(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByValueDateAfter", startResult, maxRows, valueDate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByGstType
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByGstType(String gstType) throws DataAccessException {

		return findApViewerByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByDescription
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByDescription(String description) throws DataAccessException {

		return findApViewerByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByGstRate
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findApViewerByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByAccountName
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByAccountName(String accountName) throws DataAccessException {

		return findApViewerByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByValueDate
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByValueDate(java.util.Calendar valueDate) throws DataAccessException {

		return findApViewerByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByValueDate(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPayeeName
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPayeeName(String payeeName) throws DataAccessException {

		return findApViewerByPayeeName(payeeName, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPayeeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPayeeName(String payeeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPayeeName", startResult, maxRows, payeeName);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findApViewerByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPaymentOrderNoContaining", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByInvoiceNo
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findApViewerByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByClassNameContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByClassNameContaining(String className) throws DataAccessException {

		return findApViewerByClassNameContaining(className, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByClassNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByClassNameContaining(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByClassNameContaining", startResult, maxRows, className);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findApViewerByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findApViewerByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findApViewerByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPaymentIncludeGstConvertedAmount(java.math.BigDecimal paymentIncludeGstConvertedAmount) throws DataAccessException {

		return findApViewerByPaymentIncludeGstConvertedAmount(paymentIncludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPaymentIncludeGstConvertedAmount(java.math.BigDecimal paymentIncludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPaymentIncludeGstConvertedAmount", startResult, maxRows, paymentIncludeGstConvertedAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByMonthAfter
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findApViewerByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByMonth
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByMonth(java.util.Calendar month) throws DataAccessException {

		return findApViewerByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findApViewerByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findApViewerByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByValueDateBefore
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByValueDateBefore(java.util.Calendar valueDate) throws DataAccessException {

		return findApViewerByValueDateBefore(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByValueDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByValueDateBefore(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByValueDateBefore", startResult, maxRows, valueDate);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByMonthBefore
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findApViewerByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerById
	 *
	 */
	@Transactional
	public ApViewer findApViewerById(BigInteger id) throws DataAccessException {

		return findApViewerById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerById
	 *
	 */

	@Transactional
	public ApViewer findApViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApViewerById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApViewerByPrimaryKey
	 *
	 */
	@Transactional
	public ApViewer findApViewerByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApViewerByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPrimaryKey
	 *
	 */

	@Transactional
	public ApViewer findApViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApViewerByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPaymentIncludeGstOriginalAmount(java.math.BigDecimal paymentIncludeGstOriginalAmount) throws DataAccessException {

		return findApViewerByPaymentIncludeGstOriginalAmount(paymentIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPaymentIncludeGstOriginalAmount(java.math.BigDecimal paymentIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPaymentIncludeGstOriginalAmount", startResult, maxRows, paymentIncludeGstOriginalAmount);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findApViewerByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByApInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByApInvoiceNoContaining(String apInvoiceNo) throws DataAccessException {

		return findApViewerByApInvoiceNoContaining(apInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByApInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByApInvoiceNoContaining(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByApInvoiceNoContaining", startResult, maxRows, apInvoiceNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByAccountNameContaining(String accountName) throws DataAccessException {

		return findApViewerByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findApViewerByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<ApViewer> findApViewerByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findApViewerByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findApViewerByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApViewer> findApViewerByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApViewerByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<ApViewer>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApViewer entity) {
		return true;
	}
	
	@Transactional
    public void refresh() throws DataAccessException {
        getEntityManager().createNativeQuery("REFRESH MATERIALIZED VIEW ap_ledger").executeUpdate();
    }
}
