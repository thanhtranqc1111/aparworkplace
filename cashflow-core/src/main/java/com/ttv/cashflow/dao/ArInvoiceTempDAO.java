
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ArInvoiceTemp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ArInvoiceTemp entities.
 * 
 */
public interface ArInvoiceTempDAO extends JpaDao<ArInvoiceTemp> {

	/**
	 * JPQL Query - findArInvoiceTempByGstTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstTypeContaining(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByGstTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPrimaryKey
	 *
	 */
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPrimaryKey
	 *
	 */
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableName(String accountReceivableName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableName(String accountReceivableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountNameContaining(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPayerName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerName(String payerName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPayerName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByDescriptionContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByDescriptionContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountType(String accountType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountType(String accountType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectNameContaining(String projectName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByFxRate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByFxRate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonthAfter
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthAfter(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonthAfter
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthAfter(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateBefore
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateBefore
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateBefore(Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescription
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescription(String projectDescription) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescription
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescription(String projectDescription, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCodeContaining(String accountReceivableCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCodeContaining(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByGstRate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByGstRate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCode(String accountCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCode(String accountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByClaimType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimType(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByClaimType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCodeContaining(String accountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCodeContaining(String accountCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByDescription
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByDescription
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountTypeContaining(String accountType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountTypeContaining(String accountType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonth
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonth(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonth
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonth(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPoNo
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNo(String poNo) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPoNo
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNo(String poNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableNameContaining(String accountReceivableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableNameContaining(String accountReceivableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceTemps
	 *
	 */
	public Set<ArInvoiceTemp> findAllArInvoiceTemps() throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceTemps
	 *
	 */
	public Set<ArInvoiceTemp> findAllArInvoiceTemps(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNoContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNoContaining(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNoContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountName(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountName(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCode(String accountReceivableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCode(String accountReceivableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPoNoContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNoContaining(String poNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPoNoContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNoContaining(String poNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCode(String projectCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectCode
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCode(String projectCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDate
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDate(Calendar invoiceIssuedDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateAfter
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateAfter
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateAfter(Calendar invoiceIssuedDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrencyContaining(String originalCurrency) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrencyContaining(String originalCurrency, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNo
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNo(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNo
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNo(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonthBefore
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthBefore(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByMonthBefore
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthBefore(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrency
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrency(String originalCurrency_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrency
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrency(String originalCurrency_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByGstType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstType(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByGstType
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByGstType(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPayerNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerNameContaining(String payerName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByPayerNameContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerNameContaining(String payerName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectName(String projectName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectName
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectName(String projectName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescriptionContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescriptionContaining(String projectDescription_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescriptionContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescriptionContaining(String projectDescription_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCodeContaining(String projectCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByProjectCodeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCodeContaining(String projectCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByClaimTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimTypeContaining(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempByClaimTypeContaining
	 *
	 */
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimTypeContaining(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempById
	 *
	 */
	public ArInvoiceTemp findArInvoiceTempById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceTempById
	 *
	 */
	public ArInvoiceTemp findArInvoiceTempById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 */
	public List<String> findListIdArInvoiceTempByHavingGroup() throws DataAccessException;
	
	/**
	 */
	public List<ArInvoiceTemp> findArInvoiceTempListId(List<BigInteger> listId) throws DataAccessException;
	
	/**
     * Do truncate
     */
    public void truncate() throws DataAccessException;

}