
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.domain.BankStatement;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage BankStatement entities.
 * 
 */
public interface BankStatementDAO extends JpaDao<BankStatement> {

	/**
	 * JPQL Query - findBankStatementByReference1
	 *
	 */
	public Set<BankStatement> findBankStatementByReference1(String reference1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference1
	 *
	 */
	public Set<BankStatement> findBankStatementByReference1(String reference1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankAccountNo
	 *
	 */
	public Set<BankStatement> findBankStatementByBankAccountNo(String bankAccountNo) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankAccountNo
	 *
	 */
	public Set<BankStatement> findBankStatementByBankAccountNo(String bankAccountNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBalance
	 *
	 */
	public Set<BankStatement> findBankStatementByBalance(java.math.BigDecimal balance) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBalance
	 *
	 */
	public Set<BankStatement> findBankStatementByBalance(BigDecimal balance, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementById
	 *
	 */
	public BankStatement findBankStatementById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementById
	 *
	 */
	public BankStatement findBankStatementById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByModifiedDate
	 *
	 */
	public Set<BankStatement> findBankStatementByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByModifiedDate
	 *
	 */
	public Set<BankStatement> findBankStatementByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPrimaryKey
	 *
	 */
	public BankStatement findBankStatementByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPrimaryKey
	 *
	 */
	public BankStatement findBankStatementByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference2
	 *
	 */
	public Set<BankStatement> findBankStatementByReference2(String reference2) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference2
	 *
	 */
	public Set<BankStatement> findBankStatementByReference2(String reference2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByTransactionDate
	 *
	 */
	public Set<BankStatement> findBankStatementByTransactionDate(java.util.Calendar transactionDate) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByTransactionDate
	 *
	 */
	public Set<BankStatement> findBankStatementByTransactionDate(Calendar transactionDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDebit
	 *
	 */
	public Set<BankStatement> findBankStatementByDebit(java.math.BigDecimal debit) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDebit
	 *
	 */
	public Set<BankStatement> findBankStatementByDebit(BigDecimal debit, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankName
	 *
	 */
	public Set<BankStatement> findBankStatementByBankName(String bankName) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankName
	 *
	 */
	public Set<BankStatement> findBankStatementByBankName(String bankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription2Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription2Containing(String description2) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription2Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription2Containing(String description2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference1Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByReference1Containing(String reference1_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference1Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByReference1Containing(String reference1_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription2
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription2(String description2_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription2
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription2(String description2_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription1Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription1Containing(String description1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription1Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription1Containing(String description1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByCreatedDate
	 *
	 */
	public Set<BankStatement> findBankStatementByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByCreatedDate
	 *
	 */
	public Set<BankStatement> findBankStatementByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNo
	 *
	 */
	public Set<BankStatement> findBankStatementByPaymentOrderNo(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNo
	 *
	 */
	public Set<BankStatement> findBankStatementByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription1
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription1(String description1_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByDescription1
	 *
	 */
	public Set<BankStatement> findBankStatementByDescription1(String description1_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankAccountNoContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByBankAccountNoContaining(String bankAccountNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankAccountNoContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByBankAccountNoContaining(String bankAccountNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference2Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByReference2Containing(String reference2_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByReference2Containing
	 *
	 */
	public Set<BankStatement> findBankStatementByReference2Containing(String reference2_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByCredit
	 *
	 */
	public Set<BankStatement> findBankStatementByCredit(java.math.BigDecimal credit) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByCredit
	 *
	 */
	public Set<BankStatement> findBankStatementByCredit(BigDecimal credit, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllBankStatements
	 *
	 */
	public Set<BankStatement> findAllBankStatements() throws DataAccessException;

	/**
	 * JPQL Query - findAllBankStatements
	 *
	 */
	public Set<BankStatement> findAllBankStatements(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankNameContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByBankNameContaining(String bankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByBankNameContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByBankNameContaining(String bankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNoContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByPaymentOrderNoContaining(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNoContaining
	 *
	 */
	public Set<BankStatement> findBankStatementByPaymentOrderNoContaining(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByValueDate
	 *
	 */
	public Set<BankStatement> findBankStatementByValueDate(java.util.Calendar valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findBankStatementByValueDate
	 *
	 */
	public Set<BankStatement> findBankStatementByValueDate(Calendar valueDate, int startResult, int maxRows) throws DataAccessException;
	public int countBankStatementFilter(String bankName, String bankAccNo);
	public int countBankStatementAll();
	public Set<BankStatement> findBankStatementPaging(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter);
	public Set<BankStatement> findBankStatementPagingForExport(Integer start, Integer end, String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo);
	public List<PaymentOrderDTO> getPaymentOrderPaid(Set<String> setPaymentOrderNo);
	public int checkExists(BankStatement bankStatement);
	public Calendar getMaxTransactionDate(String bankName, String bankAccNo);
	/**
	 * JPQL Query - used in Receipt Order site
	 */
	public int countBankStatementFilter(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, BigDecimal credit, Integer option, int status);
	public List<BigInteger> getBankStatementIds(Integer start, Integer end, String bankName, String bankAccNo,
												Calendar transactionFrom, Calendar transactionTo, BigDecimal credit, Integer option, Integer orderColumn, String orderBy, int status);
	public List<BigInteger> getBankStatementIds(String receiptOrderNo, Integer orderColumn, String orderBy);
	/**
	 * calculate all statement calculated from this statement
	 * @param bankId
	 * @param bankName
	 * @param bankAccountNo
	 * @param newCredit
	 * @param newDebit
	 */
	public void updateBalanceBankStatement(BigInteger bankId, String bankName, String bankAccountNo, BigDecimal newCredit, BigDecimal newDebit);
}