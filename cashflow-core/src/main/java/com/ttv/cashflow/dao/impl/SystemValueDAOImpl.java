
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.SystemValueDAO;
import com.ttv.cashflow.domain.SystemValue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage SystemValue entities.
 * 
 */
@Repository("SystemValueDAO")

@Transactional
public class SystemValueDAOImpl extends AbstractJpaDao<SystemValue> implements SystemValueDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			SystemValue.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new SystemValueDAOImpl
	 *
	 */
	public SystemValueDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllSystemValues
	 *
	 */
	@Transactional
	public Set<SystemValue> findAllSystemValues() throws DataAccessException {

		return findAllSystemValues(-1, -1);
	}

	/**
	 * JPQL Query - findAllSystemValues
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findAllSystemValues(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllSystemValues", startResult, maxRows);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByPrimaryKey
	 *
	 */
	@Transactional
	public SystemValue findSystemValueByPrimaryKey(Integer id) throws DataAccessException {

		return findSystemValueByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByPrimaryKey
	 *
	 */

	@Transactional
	public SystemValue findSystemValueByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSystemValueByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.SystemValue) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findSystemValueByDescription
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByDescription(String description) throws DataAccessException {

		return findSystemValueByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByDescription", startResult, maxRows, description);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByDescriptionContaining(String description) throws DataAccessException {

		return findSystemValueByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByCode
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByCode(String code) throws DataAccessException {

		return findSystemValueByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByCode", startResult, maxRows, code);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByValueContaining
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByValueContaining(String value) throws DataAccessException {

		return findSystemValueByValueContaining(value, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByValueContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByValueContaining(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByValueContaining", startResult, maxRows, value);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByType
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByType(String type) throws DataAccessException {

		return findSystemValueByType(type, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByType(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByType", startResult, maxRows, type);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByCodeContaining
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByCodeContaining(String code) throws DataAccessException {

		return findSystemValueByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueByValue
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByValue(String value) throws DataAccessException {

		return findSystemValueByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByValue(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByValue", startResult, maxRows, value);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * JPQL Query - findSystemValueById
	 *
	 */
	@Transactional
	public SystemValue findSystemValueById(Integer id) throws DataAccessException {

		return findSystemValueById(id, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueById
	 *
	 */

	@Transactional
	public SystemValue findSystemValueById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSystemValueById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.SystemValue) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findSystemValueByTypeContaining
	 *
	 */
	@Transactional
	public Set<SystemValue> findSystemValueByTypeContaining(String type) throws DataAccessException {

		return findSystemValueByTypeContaining(type, -1, -1);
	}

	/**
	 * JPQL Query - findSystemValueByTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SystemValue> findSystemValueByTypeContaining(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSystemValueByTypeContaining", startResult, maxRows, type);
		return new LinkedHashSet<SystemValue>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(SystemValue entity) {
		return true;
	}
	
	/**
     * JPQL Query - findSystemValueByTypeAndCode
     *
     */
    @Transactional
    public SystemValue findSystemValueByTypeAndCode(String type, String code) throws DataAccessException {
        try {
            Query query = createNamedQuery("findSystemValueByTypeAndCode", -1, -1, type, code);
            return (com.ttv.cashflow.domain.SystemValue) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
