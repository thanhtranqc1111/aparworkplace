
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ArInvoiceAccountDetails;

/**
 * DAO to manage ArInvoiceAccountDetails entities.
 * 
 */
public interface ArInvoiceAccountDetailsDAO extends JpaDao<ArInvoiceAccountDetails> {

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByModifiedDate
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByModifiedDate
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsById
	 *
	 */
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsById
	 *
	 */
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCodeContaining(String accountCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceAccountDetailss
	 *
	 */
	public Set<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoiceAccountDetailss
	 *
	 */
	public Set<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountNameContaining(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstConvertedAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstConvertedAmount(BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstOriginalAmount(BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCode
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCode(String accountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCode
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCode(String accountCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByCreatedDate
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByCreatedDate
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountName
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountName(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountName
	 *
	 */
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountName(String accountName_1, int startResult, int maxRows) throws DataAccessException;
	
	public BigInteger getLastId();
}