
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ProjectMasterDAO;
import com.ttv.cashflow.domain.ProjectMaster;

/**
 * DAO to manage ProjectMaster entities.
 * 
 */
@Repository("ProjectMasterDAO")

@Transactional
public class ProjectMasterDAOImpl extends AbstractJpaDao<ProjectMaster>
        implements ProjectMasterDAO {
    private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myProjectMaster.code");
    	mapColumnIndex.put(2, "myProjectMaster.name");
        mapColumnIndex.put(3, "myProjectMaster.businessTypeName");
        mapColumnIndex.put(4, "myProjectMaster.approvalCode");
        mapColumnIndex.put(5, "myProjectMaster.customerName");
    }
    /**
     * Set of entity classes managed by this DAO. Typically a DAO manages a
     * single entity.
     *
     */
    private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
            Arrays.asList(new Class<?>[] { ProjectMaster.class }));

    /**
     * EntityManager injected by Spring for persistence unit CashFlowDriver
     *
     */
    @PersistenceContext(unitName = "CashFlowDriver")
    private EntityManager entityManager;

    /**
     * Instantiates a new ProjectMasterDAOImpl
     *
     */
    public ProjectMasterDAOImpl() {
        super();
    }

    /**
     * Get the entity manager that manages persistence unit
     *
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Returns the set of entity classes managed by this DAO.
     *
     */
    public Set<Class<?>> getTypes() {
        return dataTypes;
    }

    /**
     * JPQL Query - findAllProjectMasters
     *
     */
    @Transactional
    public Set<ProjectMaster> findAllProjectMasters()
            throws DataAccessException {

        return findAllProjectMasters(-1, -1);
    }

    /**
     * JPQL Query - findAllProjectMasters
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<ProjectMaster> findAllProjectMasters(int startResult,
            int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findAllProjectMasters", startResult,
                maxRows);
        return new LinkedHashSet<ProjectMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findProjectMasterByCode
     *
     */
    @Transactional
    public ProjectMaster findProjectMasterByCode(String code) {

        return findProjectMasterByCode(code, -1, -1);
    }

    /**
     * JPQL Query - findProjectMasterByCode
     *
     */

    @Transactional
    public ProjectMaster findProjectMasterByCode(String code, int startResult,
            int maxRows) throws DataAccessException {
        try {
            Query query = createNamedQuery("findProjectMasterByCode",
                    startResult, maxRows, code);
            return (com.ttv.cashflow.domain.ProjectMaster) query
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findProjectMasterByCodeContaining
     *
     */
    @Transactional
    public Set<ProjectMaster> findProjectMasterByCodeContaining(String code)
            throws DataAccessException {

        return findProjectMasterByCodeContaining(code, -1, -1);
    }

    /**
     * JPQL Query - findProjectMasterByCodeContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<ProjectMaster> findProjectMasterByCodeContaining(String code,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findProjectMasterByCodeContaining",
                startResult, maxRows, code);
        return new LinkedHashSet<ProjectMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findProjectMasterByPrimaryKey
     *
     */
    @Transactional
    public ProjectMaster findProjectMasterByPrimaryKey(String code)
            throws DataAccessException {

        return findProjectMasterByPrimaryKey(code, -1, -1);
    }

    /**
     * JPQL Query - findProjectMasterByPrimaryKey
     *
     */

    @Transactional
    public ProjectMaster findProjectMasterByPrimaryKey(String code,
            int startResult, int maxRows) throws DataAccessException {
        try {
            Query query = createNamedQuery("findProjectMasterByPrimaryKey",
                    startResult, maxRows, code);
            return (com.ttv.cashflow.domain.ProjectMaster) query
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * JPQL Query - findProjectMasterByNameContaining
     *
     */
    @Transactional
    public Set<ProjectMaster> findProjectMasterByNameContaining(String name)
            throws DataAccessException {

        return findProjectMasterByNameContaining(name, -1, -1);
    }

    /**
     * JPQL Query - findProjectMasterByNameContaining
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<ProjectMaster> findProjectMasterByNameContaining(String name,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findProjectMasterByNameContaining",
                startResult, maxRows, name);
        return new LinkedHashSet<ProjectMaster>(query.getResultList());
    }

    /**
     * JPQL Query - findProjectMasterByName
     *
     */
    @Transactional
    public Set<ProjectMaster> findProjectMasterByName(String name)
            throws DataAccessException {

        return findProjectMasterByName(name, -1, -1);
    }

    /**
     * JPQL Query - findProjectMasterByName
     *
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public Set<ProjectMaster> findProjectMasterByName(String name,
            int startResult, int maxRows) throws DataAccessException {
        Query query = createNamedQuery("findProjectMasterByName", startResult,
                maxRows, name);
        return new LinkedHashSet<ProjectMaster>(query.getResultList());
    }

    /**
     * Used to determine whether or not to merge the entity or persist the
     * entity when calling Store
     * 
     * @see store
     * 
     *
     */
    public boolean canBeMerged(ProjectMaster entity) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ttv.cashflow.dao.ProjectMasterDAO#
     * findProjectMasterByCodeNameContaining(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Set<ProjectMaster> findProjectMasterByCodeNameContaining(
            String codeName) throws DataAccessException {
        Query query = createNamedQuery("findProjectMasterByCodeNameContaining",
                -1, -1, "%" + codeName + "%");
        return new LinkedHashSet<ProjectMaster>(query.getResultList());
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<ProjectMaster> findProjectMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT myProjectMaster ");
        queryString.append("FROM ProjectMaster myProjectMaster ");
        queryString.append("WHERE ");
        queryString.append("LOWER(myProjectMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.businessTypeName) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.approvalCode) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.customerName) LIKE LOWER(CONCAT('%', ?1, '%')) ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ");
        queryString.append(orderBy);

        Query query = createQuery(queryString.toString(), start, end);
        query.setParameter(1, keyword);

        return (List<ProjectMaster>) query.getResultList();
    }

    @Transactional
    public int countProjectMasterFilter(String keyword) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT COUNT(1) ");
        queryString.append("FROM project_master myProjectMaster ");
        queryString.append("WHERE ");
        queryString.append("LOWER(myProjectMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.business_type_name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.approval_code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(myProjectMaster.customer_name) LIKE LOWER(CONCAT('%', ?1, '%')) ");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter(1, keyword);

        return ((BigInteger) query.getSingleResult()).intValue();
    }

    @Override
    public boolean checkExistingProjectMasterCode(String code) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM cashflow.project_master projectMaster ");
        queryString.append("WHERE projectMaster.code = :code");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter("code", code);

        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
}
