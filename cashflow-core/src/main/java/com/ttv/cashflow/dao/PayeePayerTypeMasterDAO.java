
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.PayeePayerTypeMaster;

import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage PayeePayerTypeMaster entities.
 * 
 */
public interface PayeePayerTypeMasterDAO extends JpaDao<PayeePayerTypeMaster> {

	/**
	 * JPQL Query - findAllPayeePayerTypeMasters
	 *
	 */
	public Set<PayeePayerTypeMaster> findAllPayeePayerTypeMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayeePayerTypeMasters
	 *
	 */
	public Set<PayeePayerTypeMaster> findAllPayeePayerTypeMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByCodeContaining
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByCodeContaining(String code) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByCodeContaining
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByPrimaryKey
	 *
	 */
	public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByPrimaryKey
	 *
	 */
	public PayeePayerTypeMaster findPayeePayerTypeMasterByPrimaryKey(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByNameContaining
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByNameContaining
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByCode
	 *
	 */
	public PayeePayerTypeMaster findPayeePayerTypeMasterByCode(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByCode
	 *
	 */
	public PayeePayerTypeMaster findPayeePayerTypeMasterByCode(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByName
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayeePayerTypeMasterByName
	 *
	 */
	public Set<PayeePayerTypeMaster> findPayeePayerTypeMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;
	
	public Set<PayeePayerTypeMaster> findPayeeTypeMasterPaging(
	                Integer start, Integer end, Integer orderColumn, String orderBy,
	                String keyword) throws DataAccessException;
	public int countFilter(Integer start, Integer length, Integer orderColumn,
	            String orderBy, String keyword);

}