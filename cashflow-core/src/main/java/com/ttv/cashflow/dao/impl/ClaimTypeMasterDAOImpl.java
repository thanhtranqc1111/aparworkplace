
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ClaimTypeMasterDAO;
import com.ttv.cashflow.domain.ClaimTypeMaster;

/**
 * DAO to manage ClaimTypeMaster entities.
 * 
 */
@Repository("ClaimTypeMasterDAO")

@Transactional
public class ClaimTypeMasterDAOImpl extends AbstractJpaDao<ClaimTypeMaster> implements ClaimTypeMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myClaimTypeMaster.code");
    	mapColumnIndex.put(2, "myClaimTypeMaster.name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ClaimTypeMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ClaimTypeMasterDAOImpl
	 *
	 */
	public ClaimTypeMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findClaimTypeMasterByCode
	 *
	 */
	@Transactional
	public ClaimTypeMaster findClaimTypeMasterByCode(String code) throws DataAccessException {

		return findClaimTypeMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findClaimTypeMasterByCode
	 *
	 */

	@Transactional
	public ClaimTypeMaster findClaimTypeMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findClaimTypeMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ClaimTypeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findClaimTypeMasterByPrimaryKey
	 *
	 */
	@Transactional
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code) throws DataAccessException {

		return findClaimTypeMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findClaimTypeMasterByPrimaryKey
	 *
	 */

	@Transactional
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findClaimTypeMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.ClaimTypeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findClaimTypeMasterByName
	 *
	 */
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByName(String name) throws DataAccessException {

		return findClaimTypeMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findClaimTypeMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findClaimTypeMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<ClaimTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findClaimTypeMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByCodeContaining(String code) throws DataAccessException {

		return findClaimTypeMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findClaimTypeMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findClaimTypeMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<ClaimTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findClaimTypeMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByNameContaining(String name) throws DataAccessException {

		return findClaimTypeMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findClaimTypeMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ClaimTypeMaster> findClaimTypeMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findClaimTypeMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<ClaimTypeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllClaimTypeMasters
	 *
	 */
	@Transactional
	public Set<ClaimTypeMaster> findAllClaimTypeMasters() throws DataAccessException {

		return findAllClaimTypeMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllClaimTypeMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ClaimTypeMaster> findAllClaimTypeMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllClaimTypeMasters", startResult, maxRows);
		return new LinkedHashSet<ClaimTypeMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ClaimTypeMaster entity) {
		return true;
	}

	public int countClaimTypeMasterFilter(Integer start, Integer end, String keyword) {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countClaimTypeMasterPaging", -1, -1, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countClaimTypeMasterAll() {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countClaimTypeMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<ClaimTypeMaster> findClaimTypeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Query query = createQuery("select myClaimTypeMaster from ClaimTypeMaster myClaimTypeMaster where lower(myClaimTypeMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   + "lower(myClaimTypeMaster.name) like lower(CONCAT('%',?1, '%')) " + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<ClaimTypeMaster>(query.getResultList());
	}
}
