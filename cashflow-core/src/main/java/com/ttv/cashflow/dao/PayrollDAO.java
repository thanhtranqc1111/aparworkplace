
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.dto.PayrollPaymentDTO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Payroll entities.
 * 
 */
public interface PayrollDAO extends JpaDao<Payroll> {

	/**
	 * JPQL Query - findPayrollByClaimTypeContaining
	 *
	 */
	public Set<Payroll> findPayrollByClaimTypeContaining(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByClaimTypeContaining
	 *
	 */
	public Set<Payroll> findPayrollByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonthBefore
	 *
	 */
	public Set<Payroll> findPayrollByMonthBefore(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonthBefore
	 *
	 */
	public Set<Payroll> findPayrollByMonthBefore(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableNameContaining
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableNameContaining(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableNameContaining
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPrimaryKey
	 *
	 */
	public Payroll findPayrollByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPrimaryKey
	 *
	 */
	public Payroll findPayrollByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalNetPayment
	 *
	 */
	public Set<Payroll> findPayrollByTotalNetPayment(java.math.BigDecimal totalNetPayment) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalNetPayment
	 *
	 */
	public Set<Payroll> findPayrollByTotalNetPayment(BigDecimal totalNetPayment, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrolls
	 *
	 */
	public Set<Payroll> findAllPayrolls() throws DataAccessException;

	/**
	 * JPQL Query - findAllPayrolls
	 *
	 */
	public Set<Payroll> findAllPayrolls(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateAfter
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateAfter
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDateAfter(Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDateBefore
	 *
	 */
	public Set<Payroll> findPayrollByBookingDateBefore(java.util.Calendar bookingDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDateBefore
	 *
	 */
	public Set<Payroll> findPayrollByBookingDateBefore(Calendar bookingDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByClaimType
	 *
	 */
	public Set<Payroll> findPayrollByClaimType(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByClaimType
	 *
	 */
	public Set<Payroll> findPayrollByClaimType(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableCode
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableCode(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableCode
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableName
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableName(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableName
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableName(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPayrollNoContaining
	 *
	 */
	public Set<Payroll> findPayrollByPayrollNoContaining(String payrollNo) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPayrollNoContaining
	 *
	 */
	public Set<Payroll> findPayrollByPayrollNoContaining(String payrollNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDate
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDate(java.util.Calendar paymentScheduleDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDate
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDate(Calendar paymentScheduleDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDate
	 *
	 */
	public Set<Payroll> findPayrollByBookingDate(java.util.Calendar bookingDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDate
	 *
	 */
	public Set<Payroll> findPayrollByBookingDate(Calendar bookingDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByCreatedDate
	 *
	 */
	public Set<Payroll> findPayrollByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByCreatedDate
	 *
	 */
	public Set<Payroll> findPayrollByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByModifiedDate
	 *
	 */
	public Set<Payroll> findPayrollByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByModifiedDate
	 *
	 */
	public Set<Payroll> findPayrollByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonth
	 *
	 */
	public Set<Payroll> findPayrollByMonth(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonth
	 *
	 */
	public Set<Payroll> findPayrollByMonth(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPayrollNo
	 *
	 */
	public Set<Payroll> findPayrollByPayrollNo(String payrollNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPayrollNo
	 *
	 */
	public Set<Payroll> findPayrollByPayrollNo(String payrollNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollById
	 *
	 */
	public Payroll findPayrollById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollById
	 *
	 */
	public Payroll findPayrollById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalLaborAmount
	 *
	 */
	public Set<Payroll> findPayrollByTotalLaborAmount(java.math.BigDecimal totalLaborAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalLaborAmount
	 *
	 */
	public Set<Payroll> findPayrollByTotalLaborAmount(BigDecimal totalLaborAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonthAfter
	 *
	 */
	public Set<Payroll> findPayrollByMonthAfter(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByMonthAfter
	 *
	 */
	public Set<Payroll> findPayrollByMonthAfter(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalDeductionAmount
	 *
	 */
	public Set<Payroll> findPayrollByTotalDeductionAmount(java.math.BigDecimal totalDeductionAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByTotalDeductionAmount
	 *
	 */
	public Set<Payroll> findPayrollByTotalDeductionAmount(BigDecimal totalDeductionAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableCodeContaining
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableCodeContaining(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByAccountPayableCodeContaining
	 *
	 */
	public Set<Payroll> findPayrollByAccountPayableCodeContaining(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateBefore
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateBefore
	 *
	 */
	public Set<Payroll> findPayrollByPaymentScheduleDateBefore(Calendar paymentScheduleDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDateAfter
	 *
	 */
	public Set<Payroll> findPayrollByBookingDateAfter(java.util.Calendar bookingDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findPayrollByBookingDateAfter
	 *
	 */
	public Set<Payroll> findPayrollByBookingDateAfter(Calendar bookingDate_2, int startResult, int maxRows) throws DataAccessException;
	
	public Set<Payroll> searchPayroll(Object... parameters) throws DataAccessException;

	public Boolean checkApprovalPayroll(String payrollNo);
	
	public Payroll findPayrollObjByPayrollNo(String payrollNo) throws DataAccessException;
	
	public String getPayrollNo();
	
	public List<PayrollPaymentDTO> findPayrollPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth, String fromPayrollNo, String toPayrollNo, Integer orderColumn, String orderBy, int status);
	public int countPayrollFilter(Calendar fromMonth, Calendar toMonth, String fromPayrollNo, String toPayrollNo, int status);
	public Set<Payroll> findPayrollByImportKey(String importKey) throws DataAccessException;
	public BigDecimal getTotalNetPaymentAmountByPayrollId(BigInteger payrollId);
	
	/**
	 * Search payment payroll details 
	 * @param parameters
	 * @return
	 * @throws DataAccessException
	 */
	public Set<Payroll> searchPayrollDetails(Object... parameters) throws DataAccessException;
	public String getPayrollNo(Calendar cal);
}