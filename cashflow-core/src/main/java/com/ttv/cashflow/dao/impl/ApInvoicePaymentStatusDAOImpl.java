
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ApInvoicePaymentStatusDAO;
import com.ttv.cashflow.domain.ApInvoicePaymentStatus;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ApInvoicePaymentStatus entities.
 * 
 */
@Repository("ApInvoicePaymentStatusDAO")

@Transactional
public class ApInvoicePaymentStatusDAOImpl extends AbstractJpaDao<ApInvoicePaymentStatus> implements ApInvoicePaymentStatusDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApInvoicePaymentStatus.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoicePaymentStatusDAOImpl
	 *
	 */
	public ApInvoicePaymentStatusDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByUnpaidUnconvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByUnpaidUnconvertedAmount(java.math.BigDecimal unpaidUnconvertedAmount) throws DataAccessException {

		return findApInvoicePaymentStatusByUnpaidUnconvertedAmount(unpaidUnconvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByUnpaidUnconvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByUnpaidUnconvertedAmount(java.math.BigDecimal unpaidUnconvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentStatusByUnpaidUnconvertedAmount", startResult, maxRows, unpaidUnconvertedAmount);
		return new LinkedHashSet<ApInvoicePaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoicePaymentStatusByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoicePaymentStatus findApInvoicePaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoicePaymentStatusByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoicePaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllApInvoicePaymentStatuss
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss() throws DataAccessException {

		return findAllApInvoicePaymentStatuss(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoicePaymentStatuss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentStatus> findAllApInvoicePaymentStatuss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoicePaymentStatuss", startResult, maxRows);
		return new LinkedHashSet<ApInvoicePaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusById
	 *
	 */
	@Transactional
	public ApInvoicePaymentStatus findApInvoicePaymentStatusById(BigInteger id) throws DataAccessException {

		return findApInvoicePaymentStatusById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusById
	 *
	 */

	@Transactional
	public ApInvoicePaymentStatus findApInvoicePaymentStatusById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoicePaymentStatusById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoicePaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPaidUnconvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByPaidUnconvertedAmount(java.math.BigDecimal paidUnconvertedAmount) throws DataAccessException {

		return findApInvoicePaymentStatusByPaidUnconvertedAmount(paidUnconvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoicePaymentStatusByPaidUnconvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoicePaymentStatus> findApInvoicePaymentStatusByPaidUnconvertedAmount(java.math.BigDecimal paidUnconvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoicePaymentStatusByPaidUnconvertedAmount", startResult, maxRows, paidUnconvertedAmount);
		return new LinkedHashSet<ApInvoicePaymentStatus>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoicePaymentStatus entity) {
		return true;
	}
	
	/**
     * JPQL Query - findApInvoicePaymentStatusByPaidUnconvertedAmount
     *
     */

    @Transactional
    public ApInvoicePaymentStatus findApInvoicePaymentStatusByApInvoiceId(BigInteger apInvoiceId) throws DataAccessException {
        try {
            Query query = createNamedQuery("findApInvoicePaymentStatusByApInvoiceId", null, null, apInvoiceId);
            return (ApInvoicePaymentStatus) query.getSingleResult();
        } catch (NoResultException nre) {
            return new ApInvoicePaymentStatus();
        }
    }
}
