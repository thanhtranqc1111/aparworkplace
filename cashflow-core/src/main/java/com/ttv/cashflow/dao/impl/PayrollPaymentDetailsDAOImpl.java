
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.PayrollPaymentDetailsDAO;
import com.ttv.cashflow.domain.PayrollPaymentDetails;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage PayrollPaymentDetails entities.
 * 
 */
@Repository("PayrollPaymentDetailsDAO")

@Transactional
public class PayrollPaymentDetailsDAOImpl extends AbstractJpaDao<PayrollPaymentDetails>
		implements PayrollPaymentDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PayrollPaymentDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayrollPaymentDetailsDAOImpl
	 *
	 */
	public PayrollPaymentDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPaymentId
	 *
	 */
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByPaymentId(BigInteger paymentId) throws DataAccessException {

		return findPayrollPaymentDetailsByPaymentId(paymentId, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPaymentId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByPaymentId(BigInteger paymentId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentDetailsByPaymentId", startResult, maxRows, paymentId);
		return new LinkedHashSet<PayrollPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findPayrollPaymentDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<PayrollPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsById
	 *
	 */
	@Transactional
	public PayrollPaymentDetails findPayrollPaymentDetailsById(BigInteger id) throws DataAccessException {

		return findPayrollPaymentDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsById
	 *
	 */

	@Transactional
	public PayrollPaymentDetails findPayrollPaymentDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollPaymentDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollPaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPayrollPaymentDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public PayrollPaymentDetails findPayrollPaymentDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollPaymentDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollPaymentDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findPayrollPaymentDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<PayrollPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByNetPaymentAmount
	 *
	 */
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByNetPaymentAmount(java.math.BigDecimal netPaymentAmount) throws DataAccessException {

		return findPayrollPaymentDetailsByNetPaymentAmount(netPaymentAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollPaymentDetailsByNetPaymentAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentDetails> findPayrollPaymentDetailsByNetPaymentAmount(java.math.BigDecimal netPaymentAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollPaymentDetailsByNetPaymentAmountOriginal", startResult, maxRows, netPaymentAmount);
		return new LinkedHashSet<PayrollPaymentDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayrollPaymentDetailss
	 *
	 */
	@Transactional
	public Set<PayrollPaymentDetails> findAllPayrollPaymentDetailss() throws DataAccessException {

		return findAllPayrollPaymentDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayrollPaymentDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollPaymentDetails> findAllPayrollPaymentDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayrollPaymentDetailss", startResult, maxRows);
		return new LinkedHashSet<PayrollPaymentDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PayrollPaymentDetails entity) {
		return true;
	}
	
	public BigDecimal getTotalNetPaymentAmountOriginalByPayrollId(BigInteger paymentId, BigInteger payrollId){
		Query query = createNamedQuery("findTotalNetPaymentAmountByPayrollId", -1, -1, paymentId, payrollId);
		BigDecimal total = (BigDecimal) query.getSingleResult();
		return total;
	}
}
