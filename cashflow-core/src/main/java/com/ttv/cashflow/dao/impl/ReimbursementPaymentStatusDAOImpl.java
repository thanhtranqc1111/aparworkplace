
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ReimbursementPaymentStatusDAO;
import com.ttv.cashflow.domain.ReimbursementPaymentStatus;

/**
 * DAO to manage ReimbursementPaymentStatus entities.
 * 
 */
@Repository("ReimbursementPaymentStatusDAO")

@Transactional
public class ReimbursementPaymentStatusDAOImpl extends AbstractJpaDao<ReimbursementPaymentStatus>
		implements ReimbursementPaymentStatusDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ReimbursementPaymentStatus.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ReimbursementPaymentStatusDAOImpl
	 *
	 */
	public ReimbursementPaymentStatusDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPrimaryKey
	 *
	 */
	@Transactional
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id) throws DataAccessException {

		return findReimbursementPaymentStatusByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPrimaryKey
	 *
	 */

	@Transactional
	public ReimbursementPaymentStatus findReimbursementPaymentStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementPaymentStatusByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementPaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllReimbursementPaymentStatuss
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss() throws DataAccessException {

		return findAllReimbursementPaymentStatuss(-1, -1);
	}

	/**
	 * JPQL Query - findAllReimbursementPaymentStatuss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentStatus> findAllReimbursementPaymentStatuss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllReimbursementPaymentStatuss", startResult, maxRows);
		return new LinkedHashSet<ReimbursementPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByReimbursementId
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByReimbursementId(BigInteger reimbursementId) throws DataAccessException {

		return findReimbursementPaymentStatusByReimbursementId(reimbursementId, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByReimbursementId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByReimbursementId(BigInteger reimbursementId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentStatusByReimbursementId", startResult, maxRows, reimbursementId);
		return new LinkedHashSet<ReimbursementPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPaidReimbursementOriginalPayment
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByPaidReimbursementOriginalPayment(java.math.BigDecimal paidReimbursementOriginalPayment) throws DataAccessException {

		return findReimbursementPaymentStatusByPaidReimbursementOriginalPayment(paidReimbursementOriginalPayment, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByPaidReimbursementOriginalPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByPaidReimbursementOriginalPayment(java.math.BigDecimal paidReimbursementOriginalPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentStatusByPaidReimbursementOriginalPayment", startResult, maxRows, paidReimbursementOriginalPayment);
		return new LinkedHashSet<ReimbursementPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment
	 *
	 */
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment(java.math.BigDecimal unpaidReimbursementOriginalPayment) throws DataAccessException {

		return findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment(unpaidReimbursementOriginalPayment, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementPaymentStatus> findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment(java.math.BigDecimal unpaidReimbursementOriginalPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementPaymentStatusByUnpaidReimbursementOriginalPayment", startResult, maxRows, unpaidReimbursementOriginalPayment);
		return new LinkedHashSet<ReimbursementPaymentStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusById
	 *
	 */
	@Transactional
	public ReimbursementPaymentStatus findReimbursementPaymentStatusById(BigInteger id) throws DataAccessException {

		return findReimbursementPaymentStatusById(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementPaymentStatusById
	 *
	 */

	@Transactional
	public ReimbursementPaymentStatus findReimbursementPaymentStatusById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementPaymentStatusById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementPaymentStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ReimbursementPaymentStatus entity) {
		return true;
	}
}
