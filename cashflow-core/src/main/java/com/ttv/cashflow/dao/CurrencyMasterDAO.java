
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.CurrencyMaster;

/**
 * DAO to manage CurrencyMaster entities.
 * 
 */
public interface CurrencyMasterDAO extends JpaDao<CurrencyMaster> {

	/**
	 * JPQL Query - findAllCurrencyMasters
	 *
	 */
	public Set<CurrencyMaster> findAllCurrencyMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllCurrencyMasters
	 *
	 */
	public Set<CurrencyMaster> findAllCurrencyMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByNameContaining
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByNameContaining
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByName
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByName
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByCode
	 *
	 */
	public CurrencyMaster findCurrencyMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByCode
	 *
	 */
	public CurrencyMaster findCurrencyMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByPrimaryKey
	 *
	 */
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByPrimaryKey
	 *
	 */
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByCodeContaining
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByCodeContaining(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findCurrencyMasterByCodeContaining
	 *
	 */
	public Set<CurrencyMaster> findCurrencyMasterByCodeContaining(String code_2, int startResult, int maxRows) throws DataAccessException;
	public int countCurrencyMasterFilter(Integer start, Integer end, String keyword);
	public int countCurrencyMasterAll();
	public Set<CurrencyMaster> findCurrencyMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

}