
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ApInvoiceAccountDetailsDAO;
import com.ttv.cashflow.domain.ApInvoiceAccountDetails;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ApInvoiceAccountDetails entities.
 * 
 */
@Repository("ApInvoiceAccountDetailsDAO")

@Transactional
public class ApInvoiceAccountDetailsDAOImpl extends AbstractJpaDao<ApInvoiceAccountDetails>
		implements ApInvoiceAccountDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApInvoiceAccountDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApInvoiceAccountDetailsDAOImpl
	 *
	 */
	public ApInvoiceAccountDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByIncludeGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount) throws DataAccessException {

		return findApInvoiceAccountDetailsByIncludeGstAmount(includeGstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByIncludeGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByIncludeGstAmount", startResult, maxRows, includeGstAmount);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCodeContaining(String accountCode) throws DataAccessException {

		return findApInvoiceAccountDetailsByAccountCodeContaining(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByAccountCodeContaining", startResult, maxRows, accountCode);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceAccountDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceAccountDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findApInvoiceAccountDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsById
	 *
	 */
	@Transactional
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsById(BigInteger id) throws DataAccessException {

		return findApInvoiceAccountDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsById
	 *
	 */

	@Transactional
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApInvoiceAccountDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApInvoiceAccountDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountName
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountName(String accountName) throws DataAccessException {

		return findApInvoiceAccountDetailsByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findApInvoiceAccountDetailsByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByExcludeGstConvertedAmount", startResult, maxRows, excludeGstConvertedAmount);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByGstAmount(java.math.BigDecimal gstAmount) throws DataAccessException {

		return findApInvoiceAccountDetailsByGstAmount(gstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByGstAmount(java.math.BigDecimal gstAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByGstAmount", startResult, maxRows, gstAmount);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountNameContaining(String accountName) throws DataAccessException {

		return findApInvoiceAccountDetailsByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstAmount
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount) throws DataAccessException {

		return findApInvoiceAccountDetailsByExcludeGstAmount(excludeGstAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByExcludeGstAmount", startResult, maxRows, excludeGstAmount);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findApInvoiceAccountDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApInvoiceAccountDetailss
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss() throws DataAccessException {

		return findAllApInvoiceAccountDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllApInvoiceAccountDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApInvoiceAccountDetailss", startResult, maxRows);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCode
	 *
	 */
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCode(String accountCode) throws DataAccessException {

		return findApInvoiceAccountDetailsByAccountCode(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCode(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApInvoiceAccountDetailsByAccountCode", startResult, maxRows, accountCode);
		return new LinkedHashSet<ApInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApInvoiceAccountDetails entity) {
		return true;
	}
	
	@Transactional
    public BigInteger getLastId() {
        String sql = "SELECT id FROM ap_invoice_account_details ORDER BY id DESC LIMIT 1";

        Query query = getEntityManager().createNativeQuery(sql);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;

    }
}
