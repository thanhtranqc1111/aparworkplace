
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.EmployeeMasterDAO;
import com.ttv.cashflow.domain.EmployeeMaster;
import com.ttv.cashflow.dto.EmployeeMasterDTO;

/**
 * DAO to manage EmployeeMaster entities.
 * 
 */
@Repository("EmployeeMasterDAO")

@Transactional
public class EmployeeMasterDAOImpl extends AbstractJpaDao<EmployeeMaster> implements EmployeeMasterDAO {

    private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
        mapColumnIndex.put(0, "code");
        mapColumnIndex.put(1, "divisionMaster.name");
        mapColumnIndex.put(2, "employeeMaster.name");
        mapColumnIndex.put(3, "role_title");
        mapColumnIndex.put(4, "team");
        mapColumnIndex.put(5, "join_date");
        mapColumnIndex.put(6, "resign_date ");

    }

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			EmployeeMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new EmployeeMasterDAOImpl
	 *
	 */
	public EmployeeMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findEmployeeMasterByCode
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByCode(String code) throws DataAccessException {

		return findEmployeeMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByCode", startResult, maxRows, code);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterById
	 *
	 */
	@Transactional
	public EmployeeMaster findEmployeeMasterById(BigInteger id) throws DataAccessException {

		return findEmployeeMasterById(id, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterById
	 *
	 */

	@Transactional
	public EmployeeMaster findEmployeeMasterById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findEmployeeMasterById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.EmployeeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitleContaining
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitleContaining(String roleTitle) throws DataAccessException {

		return findEmployeeMasterByRoleTitleContaining(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitleContaining(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByRoleTitleContaining", startResult, maxRows, roleTitle);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByName
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByName(String name) throws DataAccessException {

		return findEmployeeMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByTeam
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByTeam(String team) throws DataAccessException {

		return findEmployeeMasterByTeam(team, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByTeam
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByTeam(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByTeam", startResult, maxRows, team);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByPrimaryKey
	 *
	 */
	@Transactional
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id) throws DataAccessException {

		return findEmployeeMasterByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByPrimaryKey
	 *
	 */

	@Transactional
	public EmployeeMaster findEmployeeMasterByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findEmployeeMasterByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.EmployeeMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findEmployeeMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByNameContaining(String name) throws DataAccessException {

		return findEmployeeMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByCodeContaining(String code) throws DataAccessException {

		return findEmployeeMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByTeamContaining
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByTeamContaining(String team) throws DataAccessException {

		return findEmployeeMasterByTeamContaining(team, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByTeamContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByTeamContaining(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByTeamContaining", startResult, maxRows, team);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllEmployeeMasters
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findAllEmployeeMasters() throws DataAccessException {

		return findAllEmployeeMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllEmployeeMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findAllEmployeeMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllEmployeeMasters", startResult, maxRows);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitle
	 *
	 */
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitle(String roleTitle) throws DataAccessException {

		return findEmployeeMasterByRoleTitle(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findEmployeeMasterByRoleTitle
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EmployeeMaster> findEmployeeMasterByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEmployeeMasterByRoleTitle", startResult, maxRows, roleTitle);
		return new LinkedHashSet<EmployeeMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(EmployeeMaster entity) {
		return true;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<EmployeeMasterDTO> findEmployeeMasterPaging(Integer firstResult, Integer maxResults, Integer orderColumn, String orderBy, String keyword) {

        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT employeeMaster.id as id, employeeMaster.code as code, employeeMaster.name as name, employeeMaster.role_title as roleTitle, employeeMaster.team as team, ");
        queryString.append("divisionMaster.name as divisionName, divisionMaster.code as divisionCode ");
        queryString.append(",employeeMaster.join_date as joinDate, employeeMaster.resign_date as resignDate ");
        queryString.append("FROM employee_master employeeMaster INNER JOIN division_master divisionMaster ");
        queryString.append("ON employeeMaster.division_id = divisionMaster.id ");
        queryString.append("AND (");
        queryString.append("LOWER(employeeMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.role_title) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.team) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(divisionMaster.name) LIKE LOWER(CONCAT('%', ?1, '%'))) ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ");
        queryString.append(orderBy);

        Query query = getEntityManager().createNativeQuery(queryString.toString(), "EmployeeMasterDTO");
        query.setParameter(1, keyword);
        query.setFirstResult(firstResult == null || firstResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : firstResult);

        if (maxResults != null && maxResults > 0) {
            query.setMaxResults(maxResults);
        }

        return (List<EmployeeMasterDTO>) query.getResultList();
    }

    @Override
    public Long countEmployeeMasterAll() {
        return (Long) executeQuerySingleResult("select count(1) from EmployeeMaster");
    }

    @Override
    public Integer countEmployeeMasterFilter(String keyword) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM employee_master employeeMaster INNER JOIN division_master divisionMaster ");
        queryString.append("ON employeeMaster.division_id = divisionMaster.id ");
        queryString.append("AND (");
        queryString.append("LOWER(employeeMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.role_title) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.team) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(divisionMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) )");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter(1, keyword);

        return ((BigInteger) query.getSingleResult()).intValue();
    }

	@Override
	public List<EmployeeMasterDTO> getEmployeeMaster(Integer firstResult, Integer maxResults, Integer orderColumn, String orderBy, String keyword) {
		StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT employeeMaster.id as id, employeeMaster.code as code, employeeMaster.name as name, employeeMaster.role_title as roleTitle, employeeMaster.team as team, ");
        queryString.append("divisionMaster.name as divisionName, divisionMaster.code as divisionCode ");
        queryString.append(",employeeMaster.join_date as joinDate, employeeMaster.resign_date as resignDate ");
        queryString.append("FROM employee_master employeeMaster INNER JOIN division_master divisionMaster ");
        queryString.append("ON employeeMaster.division_id = divisionMaster.id ");
        queryString.append("AND (current_date < employeeMaster.resign_date OR resign_date is NULL ) ");
        queryString.append("AND (");
        queryString.append("LOWER(employeeMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.role_title) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.team) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(divisionMaster.name) LIKE LOWER(CONCAT('%', ?1, '%'))) ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ");
        queryString.append(orderBy);

        Query query = getEntityManager().createNativeQuery(queryString.toString(), "EmployeeMasterDTO");
        query.setParameter(1, keyword);
        query.setFirstResult(firstResult == null || firstResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : firstResult);

        if (maxResults != null && maxResults > 0) {
            query.setMaxResults(maxResults);
        }

        return (List<EmployeeMasterDTO>) query.getResultList();
	}

	@Override
	public List<EmployeeMasterDTO> getEmployeeMasterForPayroll(Integer firstResult, Integer maxResults, Integer orderColumn,
														       String orderBy, String keyword, Calendar paymentScheduleDate) {
		StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT employeeMaster.id as id, employeeMaster.code as code, employeeMaster.name as name, employeeMaster.role_title as roleTitle, employeeMaster.team as team, ");
        queryString.append("divisionMaster.name as divisionName, divisionMaster.code as divisionCode ");
        queryString.append(",employeeMaster.join_date as joinDate, employeeMaster.resign_date as resignDate ");
        queryString.append("FROM employee_master employeeMaster INNER JOIN division_master divisionMaster ");
        queryString.append("ON employeeMaster.division_id = divisionMaster.id ");
        if(paymentScheduleDate != null)
        {
        	queryString.append("AND (?2 < employeeMaster.resign_date OR resign_date is NULL ) ");
        }
        queryString.append("AND (");
        queryString.append("LOWER(employeeMaster.code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.name) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.role_title) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(employeeMaster.team) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(divisionMaster.name) LIKE LOWER(CONCAT('%', ?1, '%'))) ");
        queryString.append("ORDER BY ").append(mapColumnIndex.get(orderColumn)).append(" ");
        queryString.append(orderBy);

        Query query = getEntityManager().createNativeQuery(queryString.toString(), "EmployeeMasterDTO");
        query.setParameter(1, keyword);
        if(paymentScheduleDate != null)
        {
        	query.setParameter(2, paymentScheduleDate);
        }
        query.setFirstResult(firstResult == null || firstResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : firstResult);

        if (maxResults != null && maxResults > 0) {
            query.setMaxResults(maxResults);
        }

        return (List<EmployeeMasterDTO>) query.getResultList();
	}
}
