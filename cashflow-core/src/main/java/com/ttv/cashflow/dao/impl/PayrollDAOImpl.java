
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PayrollDAO;
import com.ttv.cashflow.domain.Payroll;
import com.ttv.cashflow.dto.PayrollPaymentDTO;

/**
 * DAO to manage Payroll entities.
 * 
 */
@Repository("PayrollDAO")

@Transactional
public class PayrollDAOImpl extends AbstractJpaDao<Payroll> implements PayrollDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "pa.payroll_no");
    	mapColumnIndex.put(1, "pa.month");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Payroll.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayrollDAOImpl
	 *
	 */
	public PayrollDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPayrollByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByClaimTypeContaining(String claimType) throws DataAccessException {

		return findPayrollByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByMonthBefore
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findPayrollByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findPayrollByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPrimaryKey
	 *
	 */
	@Transactional
	public Payroll findPayrollByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPayrollByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPrimaryKey
	 *
	 */

	@Transactional
	public Payroll findPayrollByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Payroll) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollByTotalNetPayment
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByTotalNetPayment(java.math.BigDecimal totalNetPayment) throws DataAccessException {

		return findPayrollByTotalNetPayment(totalNetPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByTotalNetPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByTotalNetPayment(java.math.BigDecimal totalNetPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByTotalNetPayment", startResult, maxRows, totalNetPayment);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayrolls
	 *
	 */
	@Transactional
	public Set<Payroll> findAllPayrolls() throws DataAccessException {

		return findAllPayrolls(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayrolls
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findAllPayrolls(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayrolls", startResult, maxRows);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateAfter
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollByPaymentScheduleDateAfter(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDateAfter(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByPaymentScheduleDateAfter", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByBookingDateBefore
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByBookingDateBefore(java.util.Calendar bookingDate) throws DataAccessException {

		return findPayrollByBookingDateBefore(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByBookingDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByBookingDateBefore(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByBookingDateBefore", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByClaimType
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByClaimType(String claimType) throws DataAccessException {

		return findPayrollByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findPayrollByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableName
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findPayrollByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPayrollNoContaining
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByPayrollNoContaining(String payrollNo) throws DataAccessException {

		return findPayrollByPayrollNoContaining(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPayrollNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByPayrollNoContaining(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByPayrollNoContaining", startResult, maxRows, payrollNo);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDate
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDate(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollByPaymentScheduleDate(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDate(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByPaymentScheduleDate", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByBookingDate
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByBookingDate(java.util.Calendar bookingDate) throws DataAccessException {

		return findPayrollByBookingDate(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByBookingDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByBookingDate(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByBookingDate", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByCreatedDate
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findPayrollByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByModifiedDate
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findPayrollByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByMonth
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByMonth(java.util.Calendar month) throws DataAccessException {

		return findPayrollByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByMonth", startResult, maxRows, month);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPayrollNo
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByPayrollNo(String payrollNo) throws DataAccessException {

		return findPayrollByPayrollNo(payrollNo, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPayrollNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByPayrollNo(String payrollNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByPayrollNo", startResult, maxRows, payrollNo);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollById
	 *
	 */
	@Transactional
	public Payroll findPayrollById(BigInteger id) throws DataAccessException {

		return findPayrollById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollById
	 *
	 */

	@Transactional
	public Payroll findPayrollById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.Payroll) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollByTotalLaborAmount
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByTotalLaborAmount(java.math.BigDecimal totalLaborAmount) throws DataAccessException {

		return findPayrollByTotalLaborAmount(totalLaborAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByTotalLaborAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByTotalLaborAmount(java.math.BigDecimal totalLaborAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByTotalLaborAmount", startResult, maxRows, totalLaborAmount);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByMonthAfter
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findPayrollByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByTotalDeductionAmount
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByTotalDeductionAmount(java.math.BigDecimal totalDeductionAmount) throws DataAccessException {

		return findPayrollByTotalDeductionAmount(totalDeductionAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByTotalDeductionAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByTotalDeductionAmount(java.math.BigDecimal totalDeductionAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByTotalDeductionAmount", startResult, maxRows, totalDeductionAmount);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findPayrollByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateBefore
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate) throws DataAccessException {

		return findPayrollByPaymentScheduleDateBefore(paymentScheduleDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByPaymentScheduleDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByPaymentScheduleDateBefore(java.util.Calendar paymentScheduleDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByPaymentScheduleDateBefore", startResult, maxRows, paymentScheduleDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollByBookingDateAfter
	 *
	 */
	@Transactional
	public Set<Payroll> findPayrollByBookingDateAfter(java.util.Calendar bookingDate) throws DataAccessException {

		return findPayrollByBookingDateAfter(bookingDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollByBookingDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Payroll> findPayrollByBookingDateAfter(java.util.Calendar bookingDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByBookingDateAfter", startResult, maxRows, bookingDate);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Payroll entity) {
		return true;
	}
	

	@SuppressWarnings("unchecked")
    @Transactional
    public Set<Payroll> searchPayroll(Object... parameters) throws DataAccessException {
        String queryString = new StringBuilder()
            .append("from Payroll pr")
            //Where
            .append(" where (?1 = '' or ?1 <= pr.payrollNo)")
            .append(" and (?2 = '' or ?2 >= pr.payrollNo)")
            .append(" and (?3 = '' or to_timestamp(?3, 'dd/MM/yyyy') <= pr.paymentScheduleDate)")
            .append(" and (?4 = '' or to_timestamp(?4, 'dd/MM/yyyy') >= pr.paymentScheduleDate)")
            .append(" and (?5 = '' or to_timestamp(?5, 'MM/yyyy') <= pr.month)")
            .append(" and (?6 = '' or to_timestamp(?6, 'MM/yyyy') >= pr.month)")
            .append(" and (?7 = '' or ?7 <> pr.status)")
            //Order by
            .append(" order by pr.payrollNo asc")
            .toString();
        
        Query query = createQuery(queryString, null, null, parameters);
        
        return new LinkedHashSet<Payroll>(query.getResultList());
    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public Set<Payroll> searchPayrollDetails(Object... parameters) throws DataAccessException {
        String queryString = new StringBuilder()
            .append("from Payroll pr")
            //Where
            .append(" where (?1 = '' or to_timestamp(?1, 'MM/yyyy') <= pr.month)")
            .append(" and (?2 = '' or to_timestamp(?2, 'MM/yyyy') >= pr.month)")
            .append(" and (?3 = '' or to_timestamp(?3, 'dd/MM/yyyy') <= pr.paymentScheduleDate)")
            .append(" and (?4 = '' or to_timestamp(?4, 'dd/MM/yyyy') >= pr.paymentScheduleDate)")
            .append(" and (?5 = '' or ?5 = pr.claimType)")
            .append(" and (?6 = '' or ?6 <> pr.status) and pr.status <> ?7 ")
            //Order by
            .append(" order by pr.payrollNo asc")
            .toString();
        
        Query query = createQuery(queryString, null, null, parameters);
        
        return new LinkedHashSet<Payroll>(query.getResultList());
    }
	
	
	@Transactional
    public Boolean checkApprovalPayroll(String payrollNo) {
        Query query = getEntityManager().createNativeQuery("SELECT check_approval_payroll(:payrollNo)")
                                        .setParameter("payrollNo", payrollNo);

        Boolean ret = (Boolean) query.getSingleResult();

        return ret;

    }
	
	@Transactional
	public Payroll findPayrollObjByPayrollNo(String payrollNo) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollByPayrollNo", -1, -1, payrollNo);
			return (com.ttv.cashflow.domain.Payroll) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}	
	
	@Override
	public String getPayrollNo() {
		Query query = getEntityManager().createNativeQuery("SELECT payroll_no()");		
		return (String) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PayrollPaymentDTO> findPayrollPaging(Integer start, Integer end, Calendar fromMonth, Calendar toMonth,
													     String fromPayrollNo, String toPayrollNo, Integer orderColumn, String orderBy, int status) {
		StringBuilder queryString = new StringBuilder("SELECT pa.id, " + 
													"		 pa.month, " + 
													"        pa.payroll_no as payrollNo, " + 
													"        pa.claim_type as claimType, " + 
													"        pa.total_net_payment_converted as totalNetPayment, " + 
													"        pa.status as payrollStatus, " + 
													"        pa.invoice_no as invoiceNo, " + 
													"        pm.payment_order_no as paymentOrderNo, " + 
													"        pm.value_date as valueDate, " + 
													"        pm.bank_name as bankName, " + 
													"        pm.bank_account as bankAccount, " + 
													"        pm.include_gst_converted_amount as includeGstConvertedAmount, " + 
													"        pm.bank_ref_no as bankRefNo, " + 
													"        pm.status as paymentOrderStatus " + 
													"FROM payroll pa " + 
													"LEFT outer join payroll_payment_details pp on pa.id = pp.payroll_id " + 
													"LEFT outer join payment_order pm on pm.id = pp.payment_id " +
													"WHERE pa.payroll_no in (SELECT payroll_no FROM payroll pa WHERE lower(pa.status) like lower(CONCAT('%', :status, '%')) " + 
																			"and (:fromPayrollNo = '' or :fromPayrollNo <= pa.payroll_no) and (:toPayrollNo = '' or :toPayrollNo >= pa.payroll_no) ");
		if (fromMonth != null) {
			queryString.append("and pa.month >= cast(:fromMonth as date) ");
		}
		if (toMonth != null) {
			queryString.append("and pa.month <= cast(:toMonth as date) ");
		}
		if(orderColumn >= 0) {
			queryString.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy + " OFFSET :start LIMIT  " + (end == -1? "ALL" : ":end") +") order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		}
		else {
			queryString.append("order by pa.payroll_no desc OFFSET :start LIMIT  " + (end == -1? "ALL" : ":end") +") order by pa.payroll_no desc");
		}
		
		Query query = getEntityManager().createNativeQuery(queryString.toString(), "PayrollPaymentDTO");
		query.setParameter("status", status);
		query.setParameter("fromPayrollNo", fromPayrollNo);
		query.setParameter("toPayrollNo", toPayrollNo);
		if (fromMonth != null) {
			query.setParameter("fromMonth", fromMonth);
		}
		if (toMonth != null) {
			query.setParameter("toMonth", toMonth);
		}
		if(start != -1 && end != -1) {
			query.setParameter("start", start);
			query.setParameter("end", end);
		}
		else {
			query.setParameter("start", 0);
		}
		return query.getResultList();
	}

	@Override
	public int countPayrollFilter(Calendar fromMonth, Calendar toMonth, String fromPayrollNo, String toPayrollNo, int status) {
		StringBuilder queryString = new StringBuilder("SELECT count(1) " + 
													  "FROM payroll pa " + 
													  "WHERE lower(pa.status) like lower(CONCAT('%', :status, '%')) " + 
													  "and (:fromPayrollNo = '' or :fromPayrollNo <= pa.payroll_no) and (:toPayrollNo = '' or :toPayrollNo >= pa.payroll_no) ");
		if (fromMonth != null) {
			queryString.append("and pa.month >= cast(:fromMonth as date) ");
		}
		if (toMonth != null) {
			queryString.append("and pa.month <= cast(:toMonth as date) ");
		}
		Query query = getEntityManager().createNativeQuery(queryString.toString());
		query.setParameter("status", status);
		query.setParameter("fromPayrollNo", fromPayrollNo);
		query.setParameter("toPayrollNo", toPayrollNo);
		if (fromMonth != null) {
			query.setParameter("fromMonth", fromMonth);
		}
		if (toMonth != null) {
			query.setParameter("toMonth", toMonth);
		}
		return ((Number) query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Payroll> findPayrollByImportKey(String importKey) throws DataAccessException {
		Query query = createNamedQuery("findPayrollByImportKey", -1, -1, importKey);
		return new LinkedHashSet<Payroll>(query.getResultList());
	}

	@Override
	public BigDecimal getTotalNetPaymentAmountByPayrollId(BigInteger payrollId) {
		try {
			StringBuilder queryString = new StringBuilder("select sum(a.net_payment_amount_original) as total " + 
														  "from payroll_payment_details a, payment_order p " + 
														  "where a.payment_id = p.id and p.status = '001' and a.payroll_id = ?1 " + 
														  "group by a.payroll_id");
			Query query = getEntityManager().createNativeQuery(queryString.toString()).setParameter(1, payrollId);
			return (BigDecimal)query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BigDecimal(0);
	}

	@Override
	public String getPayrollNo(Calendar cal) {
		Query query = getEntityManager().createNativeQuery("SELECT payroll_no(?1)")
										.setParameter(1, cal);	
		return (String) query.getSingleResult();
	}}
