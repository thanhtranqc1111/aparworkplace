
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ApViewer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ApViewer entities.
 * 
 */
public interface ApViewerDAO extends JpaDao<ApViewer> {

	/**
	 * JPQL Query - findAllApViewers
	 *
	 */
	public Set<ApViewer> findAllApViewers() throws DataAccessException;

	/**
	 * JPQL Query - findAllApViewers
	 *
	 */
	public Set<ApViewer> findAllApViewers(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByClassName
	 *
	 */
	public Set<ApViewer> findApViewerByClassName(String className) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByClassName
	 *
	 */
	public Set<ApViewer> findViewerByClassName(String className, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApInvoiceNo
	 *
	 */
	public Set<ApViewer> findApViewerByApInvoiceNo(String apInvoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApInvoiceNo
	 *
	 */
	public Set<ApViewer> findViewerByApInvoiceNo(String apInvoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByExcludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByExcludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPayeeNameContaining
	 *
	 */
	public Set<ApViewer> findApViewerByPayeeNameContaining(String payeeName) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPayeeNameContaining
	 *
	 */
	public Set<ApViewer> findViewerByPayeeNameContaining(String payeeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApprovalCode
	 *
	 */
	public Set<ApViewer> findApViewerByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApprovalCode
	 *
	 */
	public Set<ApViewer> findApViewerByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstTypeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByGstTypeContaining(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstTypeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByDescriptionContaining
	 *
	 */
	public Set<ApViewer> findApViewerByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByDescriptionContaining
	 *
	 */
	public Set<ApViewer> findApViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentRate
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentRate
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByVarianceAmount
	 *
	 */
	public Set<ApViewer> findApViewerByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByVarianceAmount
	 *
	 */
	public Set<ApViewer> findApViewerByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByFxRate
	 *
	 */
	public Set<ApViewer> findApViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByFxRate
	 *
	 */
	public Set<ApViewer> findApViewerByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByUnpaidIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByUnpaidIncludeGstOriginalAmount(java.math.BigDecimal unpaidIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByUnpaidIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByUnpaidIncludeGstOriginalAmount(BigDecimal unpaidIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDateAfter
	 *
	 */
	public Set<ApViewer> findApViewerByValueDateAfter(java.util.Calendar valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDateAfter
	 *
	 */
	public Set<ApViewer> findApViewerByValueDateAfter(Calendar valueDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstType
	 *
	 */
	public Set<ApViewer> findApViewerByGstType(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstType
	 *
	 */
	public Set<ApViewer> findApViewerByGstType(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByDescription
	 *
	 */
	public Set<ApViewer> findApViewerByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByDescription
	 *
	 */
	public Set<ApViewer> findApViewerByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstRate
	 *
	 */
	public Set<ApViewer> findApViewerByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByGstRate
	 *
	 */
	public Set<ApViewer> findApViewerByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByAccountName
	 *
	 */
	public Set<ApViewer> findApViewerByAccountName(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByAccountName
	 *
	 */
	public Set<ApViewer> findApViewerByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDate
	 *
	 */
	public Set<ApViewer> findApViewerByValueDate(java.util.Calendar valueDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDate
	 *
	 */
	public Set<ApViewer> findApViewerByValueDate(Calendar valueDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPayeeName
	 *
	 */
	public Set<ApViewer> findApViewerByPayeeName(String payeeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPayeeName
	 *
	 */
	public Set<ApViewer> findApViewerByPayeeName(String payeeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentOrderNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentOrderNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByInvoiceNo
	 *
	 */
	public Set<ApViewer> findApViewerByInvoiceNo(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByInvoiceNo
	 *
	 */
	public Set<ApViewer> findApViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByClassNameContaining
	 *
	 */
	public Set<ApViewer> findApViewerByClassNameContaining(String className_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByClassNameContaining
	 *
	 */
	public Set<ApViewer> findApViewerByClassNameContaining(String className_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApprovalCodeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApprovalCodeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByInvoiceNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByInvoiceNoContaining(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByInvoiceNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByInvoiceNoContaining(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstConvertedAmount
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentIncludeGstConvertedAmount(java.math.BigDecimal paymentIncludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstConvertedAmount
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentIncludeGstConvertedAmount(BigDecimal paymentIncludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonthAfter
	 *
	 */
	public Set<ApViewer> findApViewerByMonthAfter(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonthAfter
	 *
	 */
	public Set<ApViewer> findApViewerByMonthAfter(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonth
	 *
	 */
	public Set<ApViewer> findApViewerByMonth(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonth
	 *
	 */
	public Set<ApViewer> findApViewerByMonth(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ApViewer> findApViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByIncludeGstConvertedAmount
	 *
	 */
	public Set<ApViewer> findApViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByIncludeGstConvertedAmount
	 *
	 */
	public Set<ApViewer> findApViewerByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDateBefore
	 *
	 */
	public Set<ApViewer> findApViewerByValueDateBefore(java.util.Calendar valueDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByValueDateBefore
	 *
	 */
	public Set<ApViewer> findApViewerByValueDateBefore(Calendar valueDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonthBefore
	 *
	 */
	public Set<ApViewer> findApViewerByMonthBefore(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByMonthBefore
	 *
	 */
	public Set<ApViewer> findApViewerByMonthBefore(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerById
	 *
	 */
	public ApViewer findApViewerById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerById
	 *
	 */
	public ApViewer findApViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPrimaryKey
	 *
	 */
	public ApViewer findApViewerByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPrimaryKey
	 *
	 */
	public ApViewer findApViewerByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentIncludeGstOriginalAmount(java.math.BigDecimal paymentIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentIncludeGstOriginalAmount
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentIncludeGstOriginalAmount(BigDecimal paymentIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCode
	 *
	 */
	public Set<ApViewer> findApViewerByOriginalCurrencyCode(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByOriginalCurrencyCode
	 *
	 */
	public Set<ApViewer> findApViewerByOriginalCurrencyCode(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApInvoiceNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByApInvoiceNoContaining(String apInvoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByApInvoiceNoContaining
	 *
	 */
	public Set<ApViewer> findApViewerByApInvoiceNoContaining(String apInvoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByAccountNameContaining
	 *
	 */
	public Set<ApViewer> findApViewerByAccountNameContaining(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByAccountNameContaining
	 *
	 */
	public Set<ApViewer> findApViewerByAccountNameContaining(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentOrderNo
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentOrderNo(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApViewerByPaymentOrderNo
	 *
	 */
	public Set<ApViewer> findApViewerByPaymentOrderNo(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;
	
	
	/**
	 * Execute statement: REFRESH MATERIALIZED VIEW ap_ledger;
	 */
	public void refresh() throws DataAccessException;

}