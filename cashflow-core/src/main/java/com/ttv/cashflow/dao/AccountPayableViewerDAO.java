
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.AccountPayableViewer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

/**
 * DAO to manage AccountPayableViewer entities.
 * 
 */
public interface AccountPayableViewerDAO extends JpaDao<AccountPayableViewer> {

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentConvertedAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentConvertedAmount(BigDecimal paymentConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentConvertedAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentConvertedAmount(BigDecimal paymentConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerById
	 *
	 */
	public AccountPayableViewer findAccountPayableViewerById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerById
	 *
	 */
	public AccountPayableViewer findAccountPayableViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNo(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByCurrencyContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrencyContaining(String currency) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByCurrencyContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrencyContaining(String currency, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOriginalAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOriginalAmount(BigDecimal paymentOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOriginalAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOriginalAmount(BigDecimal paymentOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByDescriptionContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByDescriptionContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonthAfter
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthAfter(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonthAfter
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthAfter(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByProjectNameContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectNameContaining(String projectName) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByProjectNameContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNo(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentOrderNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNo(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVarianceAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVarianceAmount(BigDecimal varianceAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVarianceAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoContaining(String accountPayableNo) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoContaining(String accountPayableNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByOriginalAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByOriginalAmount(BigDecimal originalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByOriginalAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByOriginalAmount(BigDecimal originalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCodeContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCodeContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonthBefore
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthBefore(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonthBefore
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonthBefore(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByDescription
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByDescription
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountPayableViewers
	 *
	 */
	public Set<AccountPayableViewer> findAllAccountPayableViewers() throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountPayableViewers
	 *
	 */
	public Set<AccountPayableViewer> findAllAccountPayableViewers(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentRate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentRate(BigDecimal paymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPaymentRate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByProjectName
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectName(String projectName_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByProjectName
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByProjectName(String projectName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByClaimTypeContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimTypeContaining(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByClaimTypeContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByConvertedAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByConvertedAmount(BigDecimal convertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByConvertedAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByConvertedAmount(BigDecimal convertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNo(String accountPayableNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountPayableNo
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountPayableNo(String accountPayableNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCode
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByApprovalCode
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVendorContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVendorContaining(String vendor) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVendorContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVendorContaining(String vendor, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByValueDate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByValueDate(java.util.Calendar valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByValueDate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByValueDate(Calendar valueDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByClaimType
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimType(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByClaimType
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByClaimType(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVendor
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVendor(String vendor_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByVendor
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByVendor(String vendor_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByFxRate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByFxRate(BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByFxRate
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonth
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonth(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByMonth
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByMonth(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountNameContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountNameContaining(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountNameContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNoContaining(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByInvoiceNoContaining
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByInvoiceNoContaining(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountName
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountName(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByAccountName
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByAccountName(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPrimaryKey
	 *
	 */
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByPrimaryKey
	 *
	 */
	public AccountPayableViewer findAccountPayableViewerByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByRemainAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByRemainAmount(BigDecimal remainAmount) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByRemainAmount
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByRemainAmount(BigDecimal remainAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByCurrency
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrency(String currency_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountPayableViewerByCurrency
	 *
	 */
	public Set<AccountPayableViewer> findAccountPayableViewerByCurrency(String currency_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
     * JPQL Query - findAccountPayableViewerByPaymentOrderNoList
     *
     */
	public List<AccountPayableViewer> findAccountPayableViewerByPaymentOrderNoList(List<String> paymentOrderNoList);
	
	/**
     * JPQL Query - findAccountPayableViewerByAccountPayableNoList
     *
     */
    public List<AccountPayableViewer> findAccountPayableViewerByAccountPayableNoList(List<String> accountPayableNoList);
	
	
	/**
	 * Refresh
	 */
	public void refresh();

}