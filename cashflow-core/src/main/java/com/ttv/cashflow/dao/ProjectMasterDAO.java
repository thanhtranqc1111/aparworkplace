
package com.ttv.cashflow.dao;

import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ProjectMaster;

/**
 * DAO to manage ProjectMaster entities.
 * 
 */
public interface ProjectMasterDAO extends JpaDao<ProjectMaster> {

	/**
	 * JPQL Query - findAllProjectMasters
	 *
	 */
	public Set<ProjectMaster> findAllProjectMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllProjectMasters
	 *
	 */
	public Set<ProjectMaster> findAllProjectMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByCode
	 *
	 */
	public ProjectMaster findProjectMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByCode
	 *
	 */
	public ProjectMaster findProjectMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByCodeContaining
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByCodeContaining
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByPrimaryKey
	 *
	 */
	public ProjectMaster findProjectMasterByPrimaryKey(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByPrimaryKey
	 *
	 */
	public ProjectMaster findProjectMasterByPrimaryKey(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByNameContaining
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByNameContaining
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByName
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByName
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findProjectMasterByCodeNameContaining
	 *
	 */
	public Set<ProjectMaster> findProjectMasterByCodeNameContaining(String codeName) throws DataAccessException;
	
	
    public List<ProjectMaster> findProjectMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);

    public int countProjectMasterFilter(String keyword);

    boolean checkExistingProjectMasterCode(String code);

}