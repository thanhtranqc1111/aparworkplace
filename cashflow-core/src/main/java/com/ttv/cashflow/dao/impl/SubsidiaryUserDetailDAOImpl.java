
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.SubsidiaryUserDetailDAO;
import com.ttv.cashflow.domain.SubsidiaryUserDetail;

/**
 * DAO to manage SubsidiaryUserDetail entities.
 * 
 */
@Repository("SubsidiaryUserDetailDAO")

@Transactional
public class SubsidiaryUserDetailDAOImpl extends AbstractJpaDao<SubsidiaryUserDetail>
		implements SubsidiaryUserDetailDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			SubsidiaryUserDetail.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new SubsidiaryUserDetailDAOImpl
	 *
	 */
	public SubsidiaryUserDetailDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByModifiedDate
	 *
	 */
	@Transactional
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findSubsidiaryUserDetailByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryUserDetailByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<SubsidiaryUserDetail>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailById
	 *
	 */
	@Transactional
	public SubsidiaryUserDetail findSubsidiaryUserDetailById(Integer id) throws DataAccessException {

		return findSubsidiaryUserDetailById(id, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailById
	 *
	 */

	@Transactional
	public SubsidiaryUserDetail findSubsidiaryUserDetailById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSubsidiaryUserDetailById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.SubsidiaryUserDetail) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByCreatedDate
	 *
	 */
	@Transactional
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findSubsidiaryUserDetailByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findSubsidiaryUserDetailByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<SubsidiaryUserDetail>(query.getResultList());
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByPrimaryKey
	 *
	 */
	@Transactional
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id) throws DataAccessException {

		return findSubsidiaryUserDetailByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findSubsidiaryUserDetailByPrimaryKey
	 *
	 */

	@Transactional
	public SubsidiaryUserDetail findSubsidiaryUserDetailByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findSubsidiaryUserDetailByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.SubsidiaryUserDetail) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllSubsidiaryUserDetails
	 *
	 */
	@Transactional
	public Set<SubsidiaryUserDetail> findAllSubsidiaryUserDetails() throws DataAccessException {

		return findAllSubsidiaryUserDetails(-1, -1);
	}

	/**
	 * JPQL Query - findAllSubsidiaryUserDetails
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<SubsidiaryUserDetail> findAllSubsidiaryUserDetails(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllSubsidiaryUserDetails", startResult, maxRows);
		return new LinkedHashSet<SubsidiaryUserDetail>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(SubsidiaryUserDetail entity) {
		return true;
	}
	
	/**
	 * find Subsidiary User Account By Subsidiary Id And User Account Id
	 * @param subsidiaryId
	 * @param userId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Set<SubsidiaryUserDetail> findSubsidiaryUserBySubsidiaryIdAndUserId(Integer subsidiaryId, Integer userId) {
		Query query = createNamedQuery("findSubsidiaryUserBySubsidiaryIdAndUserId", -1, -1, subsidiaryId, userId);
		return new LinkedHashSet<SubsidiaryUserDetail>(query.getResultList());
	}

	@Override
	public Set<SubsidiaryUserDetail> findSubsidiaryUserDetailByUserId(Integer userId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("findSubsidiaryUserDetailByUserId", -1, -1, userId);
		return new LinkedHashSet<SubsidiaryUserDetail>(query.getResultList());
	}


}
