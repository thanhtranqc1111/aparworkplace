
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.GstMaster;

/**
 * DAO to manage GstMaster entities.
 * 
 */
public interface GstMasterDAO extends JpaDao<GstMaster> {

	/**
	 * JPQL Query - findGstMasterByCode
	 *
	 */
	public GstMaster findGstMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByCode
	 *
	 */
	public GstMaster findGstMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllGstMasters
	 *
	 */
	public Set<GstMaster> findAllGstMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllGstMasters
	 *
	 */
	public Set<GstMaster> findAllGstMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByCodeContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByCodeContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByNameContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByNameContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByName
	 *
	 */
	public Set<GstMaster> findGstMasterByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByName
	 *
	 */
	public Set<GstMaster> findGstMasterByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByPrimaryKey
	 *
	 */
	public GstMaster findGstMasterByPrimaryKey(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByPrimaryKey
	 *
	 */
	public GstMaster findGstMasterByPrimaryKey(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByRate
	 *
	 */
	public Set<GstMaster> findGstMasterByRate(String rate) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByRate
	 *
	 */
	public Set<GstMaster> findGstMasterByRate(String rate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByRateContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByRateContaining(String rate_1) throws DataAccessException;

	/**
	 * JPQL Query - findGstMasterByRateContaining
	 *
	 */
	public Set<GstMaster> findGstMasterByRateContaining(String rate_1, int startResult, int maxRows) throws DataAccessException;
	public int countGstMasterFilter(Integer start, Integer end, String keyword);
	public int countGstMasterAll();
	public Set<GstMaster> findGstMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}