
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.AccountMasterDAO;
import com.ttv.cashflow.domain.AccountMaster;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage AccountMaster entities.
 * 
 */
@Repository("AccountMasterDAO")

@Transactional
public class AccountMasterDAOImpl extends AbstractJpaDao<AccountMaster> implements AccountMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myAccountMaster.code");
    	mapColumnIndex.put(2, "myAccountMaster.parentCode");
    	mapColumnIndex.put(3, "myAccountMaster.name");
    	mapColumnIndex.put(4, "myAccountMaster.type");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			AccountMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new AccountMasterDAOImpl
	 *
	 */
	public AccountMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAccountMasterByPrimaryKey
	 *
	 */
	@Transactional
	public AccountMaster findAccountMasterByPrimaryKey(String code) throws DataAccessException {

		return findAccountMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByPrimaryKey
	 *
	 */

	@Transactional
	public AccountMaster findAccountMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.AccountMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAccountMasterByParentCode
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByParentCode(String parentCode) throws DataAccessException {

		return findAccountMasterByParentCode(parentCode, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByParentCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByParentCode(String parentCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByParentCode", startResult, maxRows, parentCode);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByType
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByType(String type) throws DataAccessException {

		return findAccountMasterByType(type, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByType(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByType", startResult, maxRows, type);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByName
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByName(String name) throws DataAccessException {

		return findAccountMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByCodeContaining(String code) throws DataAccessException {

		return findAccountMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByTypeContaining
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByTypeContaining(String type) throws DataAccessException {

		return findAccountMasterByTypeContaining(type, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByTypeContaining(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByTypeContaining", startResult, maxRows, type);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByParentCodeContaining
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByParentCodeContaining(String parentCode) throws DataAccessException {

		return findAccountMasterByParentCodeContaining(parentCode, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByParentCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByParentCodeContaining(String parentCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByParentCodeContaining", startResult, maxRows, parentCode);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountMasterByCode
	 *
	 */
	@Transactional
	public AccountMaster findAccountMasterByCode(String code) throws DataAccessException {

		return findAccountMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByCode
	 *
	 */

	@Transactional
	public AccountMaster findAccountMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.AccountMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAccountMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAccountMasterByNameContaining(String name) throws DataAccessException {

		return findAccountMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findAccountMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAccountMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllAccountMasters
	 *
	 */
	@Transactional
	public Set<AccountMaster> findAllAccountMasters() throws DataAccessException {

		return findAllAccountMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllAccountMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountMaster> findAllAccountMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllAccountMasters", startResult, maxRows);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(AccountMaster entity) {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.ttv.cashflow.dao.AccountMasterDAO#findAccountMasterByNameCodeContaining(java.lang.String)
	 */
	public Set<AccountMaster> findAccountMasterByNameCodeContaining(String nameCode) throws DataAccessException {
		Query query = createNamedQuery("findAccountMasterByNameCodeContaining", -1, -1, "%" + nameCode + "%");
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}
	public int countAccountMasterAll() {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countAccountMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<AccountMaster> findAccountMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Query query = createQuery("select myAccountMaster from AccountMaster myAccountMaster where lower(myAccountMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   + "lower(myAccountMaster.name) like lower(CONCAT('%',?1, '%')) or lower(myAccountMaster.parentCode) like lower(CONCAT('%',?1, '%')) or lower(myAccountMaster.type) like lower(CONCAT('%',?1, '%'))" + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<AccountMaster>(query.getResultList());
	}

	public int countAccountMasterFilter(Integer start, Integer end, String keyword) {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countAccountMasterPaging", -1, -1, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}
}
