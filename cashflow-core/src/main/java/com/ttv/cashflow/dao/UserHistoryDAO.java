
package com.ttv.cashflow.dao;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.UserHistory;

/**
 * DAO to manage UserHistory entities.
 * 
 */
public interface UserHistoryDAO extends JpaDao<UserHistory> {

	/**
	 * JPQL Query - findUserHistoryById
	 *
	 */
	public UserHistory findUserHistoryById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryById
	 *
	 */
	public UserHistory findUserHistoryById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByFristNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByFristNameContaining(String fristName) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByFristNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByFristNameContaining(String fristName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByEmail
	 *
	 */
	public Set<UserHistory> findUserHistoryByEmail(String email) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByEmail
	 *
	 */
	public Set<UserHistory> findUserHistoryByEmail(String email, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantCodeContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantCodeContaining(String tenantCode) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantCodeContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantCodeContaining(String tenantCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByLastName
	 *
	 */
	public Set<UserHistory> findUserHistoryByLastName(String lastName) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByLastName
	 *
	 */
	public Set<UserHistory> findUserHistoryByLastName(String lastName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByModuleContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByModuleContaining(String module) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByModuleContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByModuleContaining(String module, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByActionContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByActionContaining(String action) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByActionContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByActionContaining(String action, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantName
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantName(String tenantName) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantName
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantName(String tenantName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllUserHistorys
	 *
	 */
	public Set<UserHistory> findAllUserHistorys() throws DataAccessException;

	/**
	 * JPQL Query - findAllUserHistorys
	 *
	 */
	public Set<UserHistory> findAllUserHistorys(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByFristName
	 *
	 */
	public Set<UserHistory> findUserHistoryByFristName(String fristName_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByFristName
	 *
	 */
	public Set<UserHistory> findUserHistoryByFristName(String fristName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantNameContaining(String tenantName_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantNameContaining(String tenantName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByPrimaryKey
	 *
	 */
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByPrimaryKey
	 *
	 */
	public UserHistory findUserHistoryByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByAction
	 *
	 */
	public Set<UserHistory> findUserHistoryByAction(String action_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByAction
	 *
	 */
	public Set<UserHistory> findUserHistoryByAction(String action_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantCode
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantCode(String tenantCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByTenantCode
	 *
	 */
	public Set<UserHistory> findUserHistoryByTenantCode(String tenantCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByModule
	 *
	 */
	public Set<UserHistory> findUserHistoryByModule(String module_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByModule
	 *
	 */
	public Set<UserHistory> findUserHistoryByModule(String module_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByLastNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByLastNameContaining(String lastName_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByLastNameContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByLastNameContaining(String lastName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByActionDate
	 *
	 */
	public Set<UserHistory> findUserHistoryByActionDate(java.util.Calendar actionDate) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByActionDate
	 *
	 */
	public Set<UserHistory> findUserHistoryByActionDate(Calendar actionDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByEmailContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByEmailContaining(String email_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserHistoryByEmailContaining
	 *
	 */
	public Set<UserHistory> findUserHistoryByEmailContaining(String email_1, int startResult, int maxRows) throws DataAccessException;

    public List<UserHistory> findPagingUserHistories(UserHistory userHistory, String fromMonth, String toMonth, Integer startpage, Integer lengthOfpage,
            Integer orderColumn, String orderBy);

    Long countAllUserHistory();

    int countFilteredUserHistory(UserHistory userHistory, String fromMonth, String toMonth);
}