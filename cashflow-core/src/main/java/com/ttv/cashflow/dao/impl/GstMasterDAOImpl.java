
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.GstMasterDAO;
import com.ttv.cashflow.domain.GstMaster;

/**
 * DAO to manage GstMaster entities.
 * 
 */
@Repository("GstMasterDAO")

@Transactional
public class GstMasterDAOImpl extends AbstractJpaDao<GstMaster> implements GstMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myGstMaster.code");
    	mapColumnIndex.put(2, "myGstMaster.type");
    	mapColumnIndex.put(3, "myGstMaster.rate");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			GstMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new GstMasterDAOImpl
	 *
	 */
	public GstMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findGstMasterByCode
	 *
	 */
	@Transactional
	public GstMaster findGstMasterByCode(String code) throws DataAccessException {

		return findGstMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByCode
	 *
	 */

	@Transactional
	public GstMaster findGstMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findGstMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.GstMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllGstMasters
	 *
	 */
	@Transactional
	public Set<GstMaster> findAllGstMasters() throws DataAccessException {

		return findAllGstMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllGstMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findAllGstMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllGstMasters", startResult, maxRows);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findGstMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<GstMaster> findGstMasterByCodeContaining(String code) throws DataAccessException {

		return findGstMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findGstMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGstMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findGstMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<GstMaster> findGstMasterByNameContaining(String name) throws DataAccessException {

		return findGstMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findGstMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGstMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findGstMasterByName
	 *
	 */
	@Transactional
	public Set<GstMaster> findGstMasterByName(String name) throws DataAccessException {

		return findGstMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findGstMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGstMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findGstMasterByPrimaryKey
	 *
	 */
	@Transactional
	public GstMaster findGstMasterByPrimaryKey(String code) throws DataAccessException {

		return findGstMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByPrimaryKey
	 *
	 */

	@Transactional
	public GstMaster findGstMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findGstMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.GstMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findGstMasterByRate
	 *
	 */
	@Transactional
	public Set<GstMaster> findGstMasterByRate(String rate) throws DataAccessException {

		return findGstMasterByRate(rate, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findGstMasterByRate(String rate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGstMasterByRate", startResult, maxRows, rate);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findGstMasterByRateContaining
	 *
	 */
	@Transactional
	public Set<GstMaster> findGstMasterByRateContaining(String rate) throws DataAccessException {

		return findGstMasterByRateContaining(rate, -1, -1);
	}

	/**
	 * JPQL Query - findGstMasterByRateContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<GstMaster> findGstMasterByRateContaining(String rate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGstMasterByRateContaining", startResult, maxRows, rate);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(GstMaster entity) {
		return true;
	}

	public Set<GstMaster> findGstMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Query query = createQuery("select myGstMaster from GstMaster myGstMaster where lower(myGstMaster.code) like lower(CONCAT('%',?1, '%')) or "
								   + "lower(myGstMaster.type) like lower(CONCAT('%',?1, '%')) " + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<GstMaster>(query.getResultList());
	}

	public int countGstMasterFilter(Integer start, Integer end, String keyword) {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countGstMasterPaging", start, end, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countGstMasterAll() {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countGstMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}
}
