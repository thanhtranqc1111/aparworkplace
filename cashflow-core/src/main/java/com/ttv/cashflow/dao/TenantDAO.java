
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.Tenant;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Tenant entities.
 * 
 */
public interface TenantDAO extends JpaDao<Tenant> {

	/**
	 * JPQL Query - findTenantByCodeContaining
	 *
	 */
	public Set<Tenant> findTenantByCodeContaining(String code) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByCodeContaining
	 *
	 */
	public Set<Tenant> findTenantByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUsername
	 *
	 */
	public Set<Tenant> findTenantByDbUsername(String dbUsername) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUsername
	 *
	 */
	public Set<Tenant> findTenantByDbUsername(String dbUsername, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbPasswordContaining
	 *
	 */
	public Set<Tenant> findTenantByDbPasswordContaining(String dbPassword) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbPasswordContaining
	 *
	 */
	public Set<Tenant> findTenantByDbPasswordContaining(String dbPassword, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbNameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbNameContaining(String dbName) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbNameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbNameContaining(String dbName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbPassword
	 *
	 */
	public Set<Tenant> findTenantByDbPassword(String dbPassword_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbPassword
	 *
	 */
	public Set<Tenant> findTenantByDbPassword(String dbPassword_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUrl
	 *
	 */
	public Set<Tenant> findTenantByDbUrl(String dbUrl) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUrl
	 *
	 */
	public Set<Tenant> findTenantByDbUrl(String dbUrl, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantById
	 *
	 */
	public Tenant findTenantById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findTenantById
	 *
	 */
	public Tenant findTenantById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByCode
	 *
	 */
	public Set<Tenant> findTenantByCode(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByCode
	 *
	 */
	public Set<Tenant> findTenantByCode(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbName
	 *
	 */
	public Set<Tenant> findTenantByDbName(String dbName_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbName
	 *
	 */
	public Set<Tenant> findTenantByDbName(String dbName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUrlContaining
	 *
	 */
	public Set<Tenant> findTenantByDbUrlContaining(String dbUrl_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUrlContaining
	 *
	 */
	public Set<Tenant> findTenantByDbUrlContaining(String dbUrl_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbDriverClassName
	 *
	 */
	public Set<Tenant> findTenantByDbDriverClassName(String dbDriverClassName) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbDriverClassName
	 *
	 */
	public Set<Tenant> findTenantByDbDriverClassName(String dbDriverClassName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByPrimaryKey
	 *
	 */
	public Tenant findTenantByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByPrimaryKey
	 *
	 */
	public Tenant findTenantByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbDriverClassNameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbDriverClassNameContaining(String dbDriverClassName_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbDriverClassNameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbDriverClassNameContaining(String dbDriverClassName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllTenants
	 *
	 */
	public Set<Tenant> findAllTenants() throws DataAccessException;

	/**
	 * JPQL Query - findAllTenants
	 *
	 */
	public Set<Tenant> findAllTenants(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUsernameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbUsernameContaining(String dbUsername_1) throws DataAccessException;

	/**
	 * JPQL Query - findTenantByDbUsernameContaining
	 *
	 */
	public Set<Tenant> findTenantByDbUsernameContaining(String dbUsername_1, int startResult, int maxRows) throws DataAccessException;

}