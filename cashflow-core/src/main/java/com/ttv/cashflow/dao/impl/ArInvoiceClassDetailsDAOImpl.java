
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ArInvoiceClassDetailsDAO;
import com.ttv.cashflow.domain.ArInvoiceClassDetails;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ArInvoiceClassDetails entities.
 * 
 */
@Repository("ArInvoiceClassDetailsDAO")

@Transactional
public class ArInvoiceClassDetailsDAOImpl extends AbstractJpaDao<ArInvoiceClassDetails>
		implements ArInvoiceClassDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArInvoiceClassDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArInvoiceClassDetailsDAOImpl
	 *
	 */
	public ArInvoiceClassDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArInvoiceClassDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ArInvoiceClassDetails findArInvoiceClassDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceClassDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceClassDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findArInvoiceClassDetailsByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescription
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescription(String description) throws DataAccessException {

		return findArInvoiceClassDetailsByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByGstConvertedAmount(gstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByGstConvertedAmount", startResult, maxRows, gstConvertedAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCode(String approvalCode) throws DataAccessException {

		return findArInvoiceClassDetailsByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllArInvoiceClassDetailss
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findAllArInvoiceClassDetailss() throws DataAccessException {

		return findAllArInvoiceClassDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllArInvoiceClassDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findAllArInvoiceClassDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArInvoiceClassDetailss", startResult, maxRows);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByFxRate
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findArInvoiceClassDetailsByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstTypeContaining(String gstType) throws DataAccessException {

		return findArInvoiceClassDetailsByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIsApproval
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIsApproval(Boolean isApproval) throws DataAccessException {

		return findArInvoiceClassDetailsByIsApproval(isApproval, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIsApproval
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIsApproval(Boolean isApproval, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByIsApproval", startResult, maxRows, isApproval);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassName
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassName(String className) throws DataAccessException {

		return findArInvoiceClassDetailsByClassName(className, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassName(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByClassName", startResult, maxRows, className);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCodeContaining(String classCode) throws DataAccessException {

		return findArInvoiceClassDetailsByClassCodeContaining(classCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCodeContaining(String classCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByClassCodeContaining", startResult, maxRows, classCode);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassNameContaining(String className) throws DataAccessException {

		return findArInvoiceClassDetailsByClassNameContaining(className, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassNameContaining(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByClassNameContaining", startResult, maxRows, className);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findArInvoiceClassDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstRate
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findArInvoiceClassDetailsByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstType
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstType(String gstType) throws DataAccessException {

		return findArInvoiceClassDetailsByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCode(String classCode) throws DataAccessException {

		return findArInvoiceClassDetailsByClassCode(classCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByClassCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByClassCode(String classCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByClassCode", startResult, maxRows, classCode);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByExcludeGstConvertedAmount", startResult, maxRows, excludeGstConvertedAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findArInvoiceClassDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsById
	 *
	 */
	@Transactional
	public ArInvoiceClassDetails findArInvoiceClassDetailsById(BigInteger id) throws DataAccessException {

		return findArInvoiceClassDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsById
	 *
	 */

	@Transactional
	public ArInvoiceClassDetails findArInvoiceClassDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceClassDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceClassDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException {

		return findArInvoiceClassDetailsByGstOriginalAmount(gstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByGstOriginalAmount", startResult, maxRows, gstOriginalAmount);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescriptionContaining(String description) throws DataAccessException {

		return findArInvoiceClassDetailsByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceClassDetailsByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceClassDetails> findArInvoiceClassDetailsByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceClassDetailsByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoiceClassDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArInvoiceClassDetails entity) {
		return true;
	}

	@Override
	public Set<String> findArInvoiceNoByApprovalCode(String approvalCode) {
		String queryString = "SELECT DISTINCT(ac.arInvoiceAccountDetails.arInvoice.arInvoiceNo) " + 
						 	  "FROM ArInvoiceClassDetails ac " +
						 	  "WHERE ac.approvalCode = ?1";

		Query query = getEntityManager().createQuery(queryString).setParameter(1, approvalCode);
		
		return new LinkedHashSet<String>(query.getResultList());
	}
}
