package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.dto.PaymentPayroll;
import com.ttv.cashflow.util.Constant;

/**
 * DAO to manage PaymentOrder entities.
 * 
 */
@Repository("PaymentOrderDAO")

@Transactional
public class PaymentOrderDAOImpl extends AbstractJpaDao<PaymentOrder> implements PaymentOrderDAO {

	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "payment_order_no");
    	mapColumnIndex.put(1, "value_date");
    }
    
	private static Map<Integer, String> mapColumnId = new HashMap<Integer, String>();
    static {
    	mapColumnId.put(0, "paymentOrderNo");
    	mapColumnId.put(1, "valueDate");
    }
    
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PaymentOrder.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PaymentOrderDAOImpl
	 *
	 */
	public PaymentOrderDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType) throws DataAccessException {

		return findPaymentOrderByPaymentOrderNo(paymentOrderNo, paymentType, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentOrderNo", startResult, maxRows, paymentOrderNo, paymentType);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCode
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCode(String accountPayableCode) throws DataAccessException {

		return findPaymentOrderByAccountPayableCode(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByAccountPayableCode", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByDescription
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByDescription(String description) throws DataAccessException {

		return findPaymentOrderByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByDescription", startResult, maxRows, description);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByCreatedDate
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findPaymentOrderByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCodeContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCodeContaining(String accountPayableCode) throws DataAccessException {

		return findPaymentOrderByAccountPayableCodeContaining(accountPayableCode, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCodeContaining(String accountPayableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByAccountPayableCodeContaining", startResult, maxRows, accountPayableCode);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByPrimaryKey
	 *
	 */
	@Transactional
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPaymentOrderByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPrimaryKey
	 *
	 */

	@Transactional
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPaymentOrderByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PaymentOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableNameContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableNameContaining(String accountPayableName) throws DataAccessException {

		return findPaymentOrderByAccountPayableNameContaining(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByAccountPayableNameContaining", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPaymentOrders
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findAllPaymentOrders() throws DataAccessException {

		return findAllPaymentOrders(-1, -1);
	}

	/**
	 * JPQL Query - findAllPaymentOrders
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findAllPaymentOrders(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPaymentOrders", startResult, maxRows);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankAccount
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankAccount(String bankAccount) throws DataAccessException {

		return findPaymentOrderByBankAccount(bankAccount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankAccount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankAccount(String bankAccount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankAccount", startResult, maxRows, bankAccount);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankNameContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankNameContaining(String bankName) throws DataAccessException {

		return findPaymentOrderByBankNameContaining(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankNameContaining(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankNameContaining", startResult, maxRows, bankName);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findPaymentOrderByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentOrderNoContaining", startResult, maxRows, "%" + paymentOrderNo +"%");
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNoContaining(String paymentApprovalNo) throws DataAccessException {

		return findPaymentOrderByPaymentApprovalNoContaining(paymentApprovalNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNoContaining(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentApprovalNoContaining", startResult, maxRows, paymentApprovalNo);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankRefNoContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankRefNoContaining(String bankRefNo) throws DataAccessException {

		return findPaymentOrderByBankRefNoContaining(bankRefNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankRefNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankRefNoContaining(String bankRefNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankRefNoContaining", startResult, maxRows, bankRefNo);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByStatusContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByStatusContaining(String status) throws DataAccessException {

		return findPaymentOrderByStatusContaining(status, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByStatusContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByStatusContaining(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByStatusContaining", startResult, maxRows, status);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderById
	 *
	 */
	@Transactional
	public PaymentOrder findPaymentOrderById(BigInteger id) throws DataAccessException {

		return findPaymentOrderById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderById
	 *
	 */

	@Transactional
	public PaymentOrder findPaymentOrderById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPaymentOrderById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PaymentOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPaymentOrderByStatus
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByStatus(String status) throws DataAccessException {

		return findPaymentOrderByStatus(status, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByStatus
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByStatus(String status, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByStatus", startResult, maxRows, status);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByValueDate
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByValueDate(java.util.Calendar valueDate) throws DataAccessException {

		return findPaymentOrderByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByValueDate(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableName
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableName(String accountPayableName) throws DataAccessException {

		return findPaymentOrderByAccountPayableName(accountPayableName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByAccountPayableName(String accountPayableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByAccountPayableName", startResult, maxRows, accountPayableName);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNo
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNo(String paymentApprovalNo) throws DataAccessException {

		return findPaymentOrderByPaymentApprovalNo(paymentApprovalNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNo(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentApprovalNo", startResult, maxRows, paymentApprovalNo);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByModifiedDate
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findPaymentOrderByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankAccountContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankAccountContaining(String bankAccount) throws DataAccessException {

		return findPaymentOrderByBankAccountContaining(bankAccount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankAccountContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankAccountContaining(String bankAccount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankAccountContaining", startResult, maxRows, bankAccount);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankName
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankName(String bankName) throws DataAccessException {

		return findPaymentOrderByBankName(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankName(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankName", startResult, maxRows, bankName);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByDescriptionContaining(String description) throws DataAccessException {

		return findPaymentOrderByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByBankRefNo
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankRefNo(String bankRefNo) throws DataAccessException {

		return findPaymentOrderByBankRefNo(bankRefNo, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByBankRefNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByBankRefNo(String bankRefNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByBankRefNo", startResult, maxRows, bankRefNo);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentTotalAmount
	 *
	 */
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentTotalAmount(java.math.BigDecimal paymentTotalAmount) throws DataAccessException {

		return findPaymentOrderByPaymentTotalAmount(paymentTotalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findPaymentOrderByPaymentTotalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PaymentOrder> findPaymentOrderByPaymentTotalAmount(java.math.BigDecimal paymentTotalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentTotalAmount", startResult, maxRows, paymentTotalAmount);
		return new LinkedHashSet<PaymentOrder>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PaymentOrder entity) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
    public List<PaymentInvoice> searchPaymentDetail(List<String> listPaymentNoApInvoice, List<String> listPaymentNoPayroll, List<String> listReimbursement, Integer orderColumn, String orderBy) {
		 StringBuilder builder = new StringBuilder();
	     builder.append(" select ")
	     .append("   paymentId, paymentOrderNo, valueDate, bankName, bankAccount, bankRefNo, includeGstConvertedAmount, paymentStatus ")
	     .append("   , paymentType, accountPayableId, accountPayableNo, month, claimType, totalAmount, invoiceStatus ")
	     .append(" from ( ")
	     .append("  (select myPaymentOrder.id as paymentId, ")
	     .append("     myPaymentOrder.payment_order_no as paymentOrderNo, ")
	     .append("     myPaymentOrder.value_date as valueDate, ")
	     .append("     myPaymentOrder.bank_name as bankName, ")
	     .append("     myPaymentOrder.bank_account as bankAccount, ")
	     .append("     myPaymentOrder.bank_ref_no as bankRefNo, ")
	     .append("     myPaymentOrder.include_gst_converted_amount as includeGstConvertedAmount, ")
	     .append("     myPaymentOrder.status as paymentStatus, ")
	     .append("     '").append(Constant.PAYMENT_ORDER_APINVOICE).append("' as paymentType, ")
	     .append("     myApInvoice.id as accountPayableId, ")
	     .append("     myApInvoice.ap_invoice_no as accountPayableNo, ")
	     .append("     myApInvoice.month as month, ")
	     .append("     myClaimtype.name as claimType, ")
	     .append("     myApInvoice.include_gst_converted_amount as totalAmount, ")
	     .append("     myApInvoice.status as invoiceStatus ")
	     .append(" from payment_order myPaymentOrder ")
	     .append(" left join ap_invoice_payment_details myApInvoicePaymentDetails on myApInvoicePaymentDetails.payment_id = myPaymentOrder.id ")
	     .append(" left join ap_invoice myApInvoice on myApInvoice.id = myApInvoicePaymentDetails.ap_invoice_id ")
	     .append(" left join claim_type_master myClaimtype on myApInvoice.claim_type = myClaimtype.code")
	     .append(" where myPaymentOrder.payment_order_no IN (:listPaymentNoApInvoice) ")
	     .append("  ) ")
	     .append(" union all ")
	     .append(" ( ")
	     .append("  select myPaymentOrder.id as paymentId, ")
	     .append("     myPaymentOrder.payment_order_no as paymentOrderNo, ")
	     .append("     myPaymentOrder.value_date as valueDate, ")
	     .append("     myPaymentOrder.bank_name as bankName, ")
	     .append("     myPaymentOrder.bank_account as bankAccount, ")
	     .append("     myPaymentOrder.bank_ref_no as bankRefNo, ")
	     .append("     myPaymentOrder.include_gst_converted_amount as includeGstConvertedAmount, ")
	     .append("     myPaymentOrder.status as paymentStatus, ")
	     .append("     '").append(Constant.PAYMENT_ORDER_PAYROLL).append("' as paymentType, ")
	     .append("     myPayroll.id as accountPayableId, ")
	     .append("     myPayroll.payroll_no as accountPayableNo, ")
	     .append("     myPayroll.month as month, ")
	     .append("     myClaimtype.name as claimType, ")
	     .append("     myPayroll.total_net_payment_converted as totalAmount, ")
	     .append("     myPayroll.status as invoiceStatus ")
	     .append(" from payment_order myPaymentOrder ")
	     .append(" left join payroll_payment_details myPayrollPaymentDetails on myPayrollPaymentDetails.payment_id = myPaymentOrder.id ")
	     .append(" left join payroll myPayroll on myPayroll.id = myPayrollPaymentDetails.payroll_id ")
	     .append(" left join claim_type_master myClaimtype on myPayroll.claim_type = myClaimtype.code")
	     .append(" where myPaymentOrder.payment_order_no IN (:listPaymentNoPayroll) ")
	     .append("  ) ")
	     .append(" union all ")
	     .append("  ( ")
	     .append("  select myPaymentOrder.id as paymentId, ")
	     .append("     myPaymentOrder.payment_order_no as paymentOrderNo, ")
	     .append("     myPaymentOrder.value_date as valueDate, ")
	     .append("     myPaymentOrder.bank_name as bankName, ")
	     .append("     myPaymentOrder.bank_account as bankAccount, ")
	     .append("     myPaymentOrder.bank_ref_no as bankRefNo, ")
	     .append("     myPaymentOrder.include_gst_converted_amount as includeGstConvertedAmount, ")
	     .append("     myPaymentOrder.status as paymentStatus, ")
	     .append("     '").append(Constant.PAYMENT_ORDER_REIMBURSEMENT).append("' as paymentType, ")
	     .append("     myReimbursement.id as accountPayableId, ")
	     .append("     myReimbursement.reimbursement_no as accountPayableNo, ")
	     .append("     myReimbursement.month as month, ")
	     .append("     myClaimtype.name as claimType, ")
	     .append("     myReimbursement.include_gst_total_converted_amount as totalAmount, ")
	     .append("     myReimbursement.status as invoiceStatus ")
	     .append(" from payment_order myPaymentOrder ")
	     .append(" left join reimbursement_payment_details myreimbursementPaymentDetails on myreimbursementPaymentDetails.payment_id = myPaymentOrder.id ")
	     .append(" left join reimbursement myReimbursement on myReimbursement.id = myreimbursementPaymentDetails.reimbursement_id ")
	     .append(" left join claim_type_master myClaimtype on myReimbursement.claim_type = myClaimtype.code")
	     .append(" where myPaymentOrder.payment_order_no in (:listReimbursement) ")
	     .append(" ) ")
	     .append(" ) as myAccountPayable ");	

		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnId.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myAccountPayable.paymentOrderNo desc, myAccountPayable.valueDate desc");
		 }
		 
		 Query query = getEntityManager().createNativeQuery(builder.toString(), "findPaymentInvoiceDetail")
                 .setParameter("listPaymentNoApInvoice", listPaymentNoApInvoice.size() == 0 ? "" : listPaymentNoApInvoice)
		 		 .setParameter("listPaymentNoPayroll", listPaymentNoPayroll.size() == 0 ? "" : listPaymentNoPayroll)
 		 		 .setParameter("listReimbursement", listReimbursement.size() == 0 ? "" : listReimbursement);
		 
		 return query.getResultList();
    }
	
	@SuppressWarnings("unchecked")
    public List<PaymentPayroll> searchPaymentDetailPayroll(List<String> paymentOrderNo, Integer orderColumn, String orderBy) {
		 StringBuilder builder = new StringBuilder();
		 builder.append("select myPaymentOrder.id as paymentId, ")
		 .append("myPaymentOrder.payment_order_no as paymentOrderNo, ")
		 .append("myPaymentOrder.value_date as valueDate, ")
		 .append("myPaymentOrder.bank_ref_no as bankRefNo, ")
		 .append("myPaymentOrder.bank_name as bankName, ")
		 .append("myPaymentOrder.bank_account as bankAccount, ")
		 .append("myPaymentOrder.include_gst_original_amount as includeGstOriginalAmount, ")
		 .append("myPaymentOrder.include_gst_converted_amount as includeGstConvertedAmount, ")
		 .append("myPaymentOrder.status as paymentStatus,  ")
		 .append("myPaymentOrder.original_currency_code as paymentCurrency, ")
		 .append("myPayroll.id as payrollId, ")
		 .append("myPayroll.payroll_no as payrollNo,  ")
		 .append("myPayroll.month as month, ")
		 .append("myPayroll.total_net_payment_original as totalNetPaymentAmountOriginal, ")
		 .append("myPayroll.total_net_payment_converted as totalNetPaymentAmountConverted, ")
		 .append("myPayroll.status as payrollStatus ")
		 .append("from payment_order myPaymentOrder ")
		 .append("left join payroll_payment_details myPayrollPaymentDetails on myPayrollPaymentDetails.payment_id = myPaymentOrder.id ")
		 .append("left join payroll myPayroll on myPayroll.id = myPayrollPaymentDetails.payroll_id ")
		 .append("where myPaymentOrder.payment_order_no IN (:paymentOrderNo) ");
		 
		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myPaymentOrder.payment_order_no desc, myPaymentOrder.value_date desc");
		 }
		 
		 Query query = getEntityManager().createNativeQuery(builder.toString(), "findPaymentPayrollDetail")
                 .setParameter("paymentOrderNo", paymentOrderNo.size() == 0 ? "" : paymentOrderNo);

		 return query.getResultList();
    }
	
	@SuppressWarnings("unchecked")
    public List<PaymentOrder> searchPaymentDetailByOrderNo(List<String> paymentOrderNo) {
		Query query = createNamedQuery("findListPaymentOrderByPaymentOrderNo", -1, -1, paymentOrderNo.size() == 0 ? "" : paymentOrderNo);
        return new ArrayList<PaymentOrder>(query.getResultList());
    }
	
	@SuppressWarnings("unchecked")
    public List<PaymentInvoice> searchPayment(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String invoiceNo, String status, Integer orderColumn, String orderBy){
		 StringBuilder builder = new StringBuilder();
		 builder.append("select myPaymentOrder.payment_order_no ")
		 .append("from payment_order myPaymentOrder ")
		 .append("left join ap_invoice_payment_details myApInvoicePaymentDetails on myApInvoicePaymentDetails.payment_id = myPaymentOrder.id ")
		 .append("left join ap_invoice myApInvoice on myApInvoice.id = myApInvoicePaymentDetails.ap_invoice_id ")
		 .append("where ((?1 = '') or myPaymentOrder.value_date >= to_timestamp(?1, 'dd/MM/yyyy')) ")
		 .append("and ((?2 = '') or myPaymentOrder.value_date < to_timestamp(?2, 'dd/MM/yyyy') + interval '1 day') ")
		 .append("and ((?3 = '') or lower(myPaymentOrder.bank_ref_no) like lower(concat('%', ?3, '%'))) ")
		 .append("and ((?4 = '') or lower(myApInvoice.invoice_no) like lower(concat('%', ?4, '%'))) ")
		 .append("and ((?5 = '') or (myPaymentOrder.status = ?5)) ")
		 .append("and myPaymentOrder.payment_type LIKE ?6 ")
		 .append("group by myPaymentOrder.payment_order_no, myPaymentOrder.value_date ");
		 
		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myPaymentOrder.payment_order_no desc, myPaymentOrder.value_date desc");
		 }

		 Query query = getEntityManager().createNativeQuery(builder.toString())
        		.setParameter(1, calFrom)
        		.setParameter(2, calTo)
        		.setParameter(3, bankRef)
        		.setParameter(4, invoiceNo)
        		.setParameter(5, status)
        		.setParameter(6, "%" + Constant.PAYMENT_ORDER_APINVOICE + "%");

		 query.setFirstResult(startResult == null || startResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : startResult);
			if (maxRow != null && maxRow > 0)
				query.setMaxResults(maxRow);
		 
        return new ArrayList<PaymentInvoice>(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
    public List<PaymentPayroll> searchPaymentPayroll(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn, String orderBy){
		 StringBuilder builder = new StringBuilder();
		 builder.append("select myPaymentOrder.payment_order_no ")
		 .append("from payment_order myPaymentOrder ")
		 .append("left join payroll_payment_details myPayrollPaymentDetails on myPayrollPaymentDetails.payment_id = myPaymentOrder.id ")
		 .append("left join payroll myPayroll on myPayroll.id = myPayrollPaymentDetails.payroll_id ")
		 .append("where ((?1 = '') or myPaymentOrder.value_date >= to_timestamp(?1, 'dd/MM/yyyy')) ")
		 .append("and ((?2 = '') or myPaymentOrder.value_date < to_timestamp(?2, 'dd/MM/yyyy') + interval '1 day') ")
		 .append("and ((?3 = '') or lower(myPaymentOrder.bank_ref_no) like lower(concat('%', ?3, '%'))) ")
		 .append("and (?4 = '' or ?4 <= myPayroll.payroll_no) and (?5 = '' or ?5 >= myPayroll.payroll_no) ")
		 .append("and ((?6 = '') or (myPaymentOrder.status = ?6)) ")
		 .append("and myPaymentOrder.payment_type LIKE ?7 ")
		 .append("group by myPaymentOrder.payment_order_no, myPaymentOrder.value_date ");
		 
		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myPaymentOrder.payment_order_no desc, myPaymentOrder.value_date desc");
		 }

		 Query query = getEntityManager().createNativeQuery(builder.toString())
        		.setParameter(1, calFrom)
        		.setParameter(2, calTo)
        		.setParameter(3, bankRef)
        		.setParameter(4, fromPayrollNo)
        		.setParameter(5, toPayrollNo)
        		.setParameter(6, status)
		 		.setParameter(7, "%" + Constant.PAYMENT_ORDER_PAYROLL + "%");

		 query.setFirstResult(startResult == null || startResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : startResult);
			if (maxRow != null && maxRow > 0)
				query.setMaxResults(maxRow);
		 
        return new ArrayList<PaymentPayroll>(query.getResultList());
	}
	
	public BigInteger searchPaymentCount(String calFrom, String calTo, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status){
		 
		StringBuilder builder = new StringBuilder();
		builder.append("select count(distinct myPaymentOrder.payment_order_no) ")
				.append("from payment_order myPaymentOrder ")
				.append("where ((?1 = '') or myPaymentOrder.value_date >= to_timestamp(?1, 'dd/MM/yyyy')) ")
				.append("and ((?2 = '') or myPaymentOrder.value_date <= to_timestamp(?2, 'dd/MM/yyyy') + interval '1 day') ")
				.append("and ((?3 = '') or lower(myPaymentOrder.bank_ref_no) like lower(concat('%', ?3, '%'))) ")
				.append("and ((?4 = '') or (myPaymentOrder.status = ?4)) ")
				.append("and ((?5 = '') or (myPaymentOrder.payment_order_no >= cast( ?5 as text))) ")
				.append("and ((?6 = '') or (myPaymentOrder.payment_order_no <= cast( ?6 as text))) ")
				.append("and (myPaymentOrder.payment_type like ?7 or myPaymentOrder.payment_type like ?8 or myPaymentOrder.payment_type like ?9) ");

		Query query = getEntityManager().createNativeQuery(builder.toString()).setParameter(1, calFrom)
				.setParameter(2, calTo)
				.setParameter(3, bankRef)
				.setParameter(4, status)
				.setParameter(5, paymentOrderNoFrom)
				.setParameter(6, paymentOrderNoTo)
				.setParameter(7, "%" + Constant.PAYMENT_ORDER_APINVOICE + "%")
				.setParameter(8, "%" + Constant.PAYMENT_ORDER_PAYROLL + "%")
				.setParameter(9, "%" + Constant.PAYMENT_ORDER_REIMBURSEMENT + "%");

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;
	}
	
	public BigInteger searchPaymentPayrollCount(String calFrom, String calTo, String bankRef, String fromPayrollNo, String toPayrollNo, String status){
		StringBuilder builder = new StringBuilder();
		builder.append("select count(distinct myPaymentOrder.payment_order_no) ")
				.append("from payment_order myPaymentOrder ")
				.append("left join payroll_payment_details myPayrollPaymentDetails on myPayrollPaymentDetails.payment_id = myPaymentOrder.id ")
				.append("left join payroll myPayroll on myPayroll.id = myPayrollPaymentDetails.payroll_id ")
				.append("where ((?1 = '') or myPaymentOrder.value_date >= to_timestamp(?1, 'dd/MM/yyyy')) ")
				.append("and ((?2 = '') or myPaymentOrder.value_date < to_timestamp(?2, 'dd/MM/yyyy') + interval '1 day') ")
				.append("and ((?3 = '') or lower(myPaymentOrder.bank_ref_no) like lower(concat('%', ?3, '%'))) ")
				.append("and (?4 = '' or ?4 <= myPayroll.payroll_no) and (?5 = '' or ?5 >= myPayroll.payroll_no) ")
				.append("and ((?6 = '') or (myPaymentOrder.status = ?6)) ")
				.append("and myPaymentOrder.payment_type LIKE ?7 ");

        Query query = getEntityManager().createNativeQuery(builder.toString())
        		.setParameter(1, calFrom)
        		.setParameter(2, calTo)
        		.setParameter(3, bankRef)
        		.setParameter(4, fromPayrollNo)
        		.setParameter(5, toPayrollNo)
        		.setParameter(6, status)
        		.setParameter(7, "%" + Constant.PAYMENT_ORDER_PAYROLL + "%");
        
        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;
	}

	@SuppressWarnings("unchecked")
    @Transactional
    public Set<ApInvoice> searchApInvoice(Object... parameters) throws DataAccessException {
        String queryString = new StringBuilder()
            .append("from ApInvoice api")
            //Where
            .append(" where (?1 = '' or ?1 <= api.apInvoiceNo)")
            .append(" and (?2 = '' or ?2 >= api.apInvoiceNo)")
            .append(" and (?3 = '' or to_timestamp(?3, 'dd/MM/yyyy') <= api.scheduledPaymentDate)")
            .append(" and (?4 = '' or to_timestamp(?4, 'dd/MM/yyyy') >= api.scheduledPaymentDate)")
            .append(" and (?5 = '' or ?5 = api.originalCurrencyCode)")
            .append(" and (lower(api.payeeName) like lower(concat('%', ?6,'%')))")
            .append(" and (?7 = '' or ?7 <> api.status)")
            //Order by
            .append(" order by api.apInvoiceNo asc")
            .toString();
        
        Query query = createQuery(queryString, null, null, parameters);
        
        return new LinkedHashSet<ApInvoice>(query.getResultList());
    }
    
	@SuppressWarnings("unchecked")
    @Transactional
    public Set<ApInvoice> searchApInvoiceDetails(Object... parameters) throws DataAccessException {
        String queryString = new StringBuilder()
            .append("from ApInvoice api")
            //Where
            .append(" where (?1 = '' or to_timestamp(?1, 'MM/yyyy') <= api.month)")
            .append(" and (?2 = '' or to_timestamp(?2, 'MM/yyyy') >= api.month)")
            .append(" and (?3 = '' or to_timestamp(?3, 'dd/MM/yyyy') <= api.scheduledPaymentDate)")
            .append(" and (?4 = '' or to_timestamp(?4, 'dd/MM/yyyy') >= api.scheduledPaymentDate)")
            .append(" and (?5 = '' or ?5 = api.claimType)")
            .append(" and (?6 = '' or ?6 <> api.status) and api.status <> ?7 ")
            //Order by
            .append(" order by api.apInvoiceNo asc")
            .toString();
        
        Query query = createQuery(queryString, null, null, parameters);
        
        return new LinkedHashSet<ApInvoice>(query.getResultList());
    }
	
    @Transactional
    public String getPaymentNo() {
        Query query = getEntityManager().createNativeQuery("SELECT payment_no()");

        String ret = (String) query.getSingleResult();

        return ret;

    }
    
    @Transactional
    public BigInteger getLastId() {
        String sql = "SELECT id FROM payment_order ORDER BY id DESC LIMIT 1";

        Query query = getEntityManager().createNativeQuery(sql);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;

    }
    
    public BigInteger findPaymentIdbyPaymentOrderNo(String paymentOrderNo){
    	try {
			Query query = createNamedQuery("findPaymentIdByPaymentOrderNo", -1, -1, paymentOrderNo);
			return (BigInteger) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
    }
    
    public PaymentOrder findPaymentbyPaymentOrderNo(String paymentOrderNo){
    	try {
			Query query = createNamedQuery("findPaymentOrderByPaymentOrderNo", -1, -1, paymentOrderNo, Constant.PAYMENT_ORDER_APINVOICE);
			return  (PaymentOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
    }

	@SuppressWarnings("unchecked")
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContainingAndStatus(String paymentOrderNo, String status, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPaymentOrderByPaymentOrderNoContainingAndStatus", -1, -1, "%" + paymentOrderNo + "%", status);
			return new LinkedHashSet<PaymentOrder>(query.getResultList());
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	@Transactional
    public String getPaymentOrderNo(Calendar cal) {
        Query query = getEntityManager().createNativeQuery("SELECT payment_no(?1)")
                                        .setParameter(1, cal);

        String ret = (String) query.getSingleResult();

        return ret;
    }

    @Transactional
    public boolean checkDuplicatePaymentOrder(String bankName, String bankAccount, BigDecimal convertedAmount, java.util.Calendar valueDate) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM cashflow.payment_order myPaymentOrder ");
        queryString.append("WHERE ");
        queryString.append("myPaymentOrder.bank_name = :bankName AND ");
        queryString.append("myPaymentOrder.bank_account = :bankAccount AND ");
        queryString.append("myPaymentOrder.include_gst_converted_amount = :includeGstConvertedAmount AND ");
        queryString.append("myPaymentOrder.value_date = :valueDate");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter("bankName", bankName);
        query.setParameter("bankAccount", bankAccount);
        query.setParameter("includeGstConvertedAmount", convertedAmount);
        query.setParameter("valueDate", valueDate);

        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
    
    
   public PaymentOrder checkDuplicateByConvertedAmountAndValueDate(String bankName, String bankAccount, BigDecimal convertedAmount, java.util.Calendar valueDate) throws DataAccessException {
//    	StringBuilder queryString = new StringBuilder();
//        queryString.append("SELECT myPaymentOrder ");
//        queryString.append("FROM cashflow.payment_order myPaymentOrder ");
//        queryString.append("WHERE ");
//        queryString.append("myPaymentOrder.bank_name = :bankName AND ");
//        queryString.append("myPaymentOrder.bank_account = :bankAccount AND ");
//        queryString.append("myPaymentOrder.include_gst_converted_amount = :includeGstConvertedAmount AND ");
//        queryString.append("myPaymentOrder.value_date = :valueDate");
//    	
//        Query query = getEntityManager().createNativeQuery(queryString.toString());
//        query.setParameter("bankName", bankName);
//        query.setParameter("bankAccount", bankAccount);
//        query.setParameter("includeGstConvertedAmount", convertedAmount);
//        query.setParameter("valueDate", valueDate);
	   try {
		   Query query = createNamedQuery("findPaymentOrderByConvertedAmountAndValueDate", -1, -1, bankName, bankAccount, convertedAmount, valueDate);
		   return (PaymentOrder) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Transactional
    public BigDecimal getOriginalAmountPayFor(BigInteger apInvoiceId) {
	    
	    String sql = "SELECT sum(payment_include_gst_original_amount) "
	               + "FROM ap_invoice_payment_details "
	               + "WHERE ap_invoice_id = (?1)";
	    
        Query query = getEntityManager().createNativeQuery(sql)
                                        .setParameter(1, apInvoiceId);

        BigDecimal ret = (BigDecimal) query.getSingleResult();

        return ret;
    }
	
	/**
	 * 
	 * @param paryrollId
	 * @return
	 */
	@Transactional
    public BigDecimal getOriginalAmountPayForPR(BigInteger paryrollId) {
	    
	    String sql = "SELECT sum(net_payment_amount_original) "
	               + "FROM payroll_payment_details "
	               + "WHERE payroll_id = (?1)";
	    
        Query query = getEntityManager().createNativeQuery(sql)
                                        .setParameter(1, paryrollId);

        BigDecimal ret = (BigDecimal) query.getSingleResult();

        return ret;
    }
	
	@Transactional
    public BigDecimal getOriginalAmountPayForReim(BigInteger reimId) {
	    
	    String sql = "SELECT sum(payment_original_amount) "
	               + "FROM reimbursement_payment_details "
	               + "WHERE reimbursement_id = (?1)";
	    
        Query query = getEntityManager().createNativeQuery(sql)
                                        .setParameter(1, reimId);

        BigDecimal ret = (BigDecimal) query.getSingleResult();

        return ret;
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentOrderDTO> getPaymentOrderNoBasedOnBankRef(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter) {
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT(p.payment_order_no), p.bank_ref_no, p.include_gst_converted_amount from bank_statement b "
				+ "INNER JOIN payment_order p on reference2 = bank_ref_no "
				+ "WHERE length(p.bank_ref_no) > 0 and p.status = '002' and b.bank_name = cast(:bankName as text) "
				+ "and b.bank_account_no = cast(:bankAccountNo as text) ");
		if(transDateFilter == 0) {
		if(transactionFrom != null)
		{
			queryString.append("and b.transaction_date >= cast(:transactionFrom as date) ");
		}
		if(transactionTo != null)
		{
			queryString.append("and b.transaction_date <= cast(:transactionTo as date) ");
		
		}
		}
		else {
			queryString.append("and b.transaction_date >= current_date - INTERVAL '" + transDateFilter + " month' ");
		}		
		Query query = getEntityManager().createNativeQuery(queryString.toString());
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		if(transDateFilter == 0) {
			if(transactionFrom != null)
			{
				query.setParameter("transactionFrom", transactionFrom);
			}
			if(transactionTo != null)
			{
				query.setParameter("transactionTo", transactionTo);
			}
		}
		List<PaymentOrderDTO> list = new ArrayList<PaymentOrderDTO>();
		Set<Object[]> objects = new LinkedHashSet<Object[]>(query.getResultList());
		for (Object[] object : objects) {
			PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
			paymentOrderDTO.setBankRefNo((String)object[1]);
			paymentOrderDTO.setPaymentOrderNo((String)object[0]);
			paymentOrderDTO.setIncludeGstConvertedAmount((BigDecimal) object[2]);
			list.add(paymentOrderDTO);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PaymentOrder findPaymentOrderByPaymentOrderNoOnly(String paymentOrderNo) throws DataAccessException {
		Query query = createNamedQuery("findPaymentOrderByPaymentOrderNoOnly", -1, -1, paymentOrderNo);
		Set<PaymentOrder> set  = new LinkedHashSet<PaymentOrder>(query.getResultList());
		if(set.isEmpty())
		{
			return null;
		}
		else {
			Iterator<PaymentOrder> iterator = set.iterator();
			return iterator.next();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
    public List<PaymentInvoice> searchPaymentOrder(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status, Integer orderColumn, String orderBy){
		 StringBuilder builder = new StringBuilder();
		 builder.append("select myPaymentOrder.payment_order_no AS paymentOrderNo, myPaymentOrder.payment_type as paymentType ")
		 .append("from payment_order myPaymentOrder ")
		 .append("where ((?1 = '') or myPaymentOrder.value_date >= to_timestamp(?1, 'dd/MM/yyyy')) ")
		 .append("and ((?2 = '') or myPaymentOrder.value_date <= to_timestamp(?2, 'dd/MM/yyyy') + interval '1 day') ")
		 .append("and ((?3 = '') or lower(myPaymentOrder.bank_ref_no) like lower(concat('%', ?3, '%'))) ")
		 .append("and ((?4 = '') or (myPaymentOrder.status = ?4)) ")
		 .append("and ((?5 = '') or (myPaymentOrder.payment_order_no >= cast( ?5 as text))) ")
		 .append("and ((?6 = '') or (myPaymentOrder.payment_order_no <= cast( ?6 as text))) ")
	     .append("and (myPaymentOrder.payment_type like ?7 or myPaymentOrder.payment_type like ?8 or myPaymentOrder.payment_type like ?9) ")
		 .append("group by myPaymentOrder.payment_order_no, myPaymentOrder.value_date, myPaymentOrder.payment_type ");
		 
		 if(orderColumn >= 0){
			 builder.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		 } else {
			 builder.append("order by myPaymentOrder.payment_order_no desc, myPaymentOrder.value_date desc");
		 }

		 Query query = getEntityManager().createNativeQuery(builder.toString(), "findPaymentNo")
        		.setParameter(1, calFrom)
				.setParameter(2, calTo)
				.setParameter(3, bankRef)
				.setParameter(4, status)
				.setParameter(5, paymentOrderNoFrom)
				.setParameter(6, paymentOrderNoTo)
				.setParameter(7, "%" + Constant.PAYMENT_ORDER_APINVOICE + "%")
				.setParameter(8, "%" + Constant.PAYMENT_ORDER_PAYROLL + "%")
				.setParameter(9, "%" + Constant.PAYMENT_ORDER_REIMBURSEMENT + "%");
		 
		 query.setFirstResult(startResult == null || startResult < 0 ? DEFAULT_FIRST_RESULT_INDEX : startResult);
			if (maxRow != null && maxRow > 0)
				query.setMaxResults(maxRow);

		return query.getResultList();
	}
	
	
	
}
