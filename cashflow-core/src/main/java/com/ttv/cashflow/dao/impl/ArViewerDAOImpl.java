
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ArViewerDAO;
import com.ttv.cashflow.domain.ArViewer;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ArViewer entities.
 * 
 */
@Repository("ArViewerDAO")

@Transactional
public class ArViewerDAOImpl extends AbstractJpaDao<ArViewer> implements ArViewerDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArViewer.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArViewerDAOImpl
	 *
	 */
	public ArViewerDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArViewerByArInvoiceNo
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByArInvoiceNo(String arInvoiceNo) throws DataAccessException {

		return findArViewerByArInvoiceNo(arInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByArInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByArInvoiceNo(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByArInvoiceNo", startResult, maxRows, arInvoiceNo);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByAccountNameContaining(String accountName) throws DataAccessException {

		return findArViewerByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findArViewerByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByPayerName
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByPayerName(String payerName) throws DataAccessException {

		return findArViewerByPayerName(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByPayerName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByPayerName", startResult, maxRows, payerName);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByInvoiceNo
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findArViewerByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByTransactionDateBefore
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDateBefore(java.util.Calendar transactionDate) throws DataAccessException {

		return findArViewerByTransactionDateBefore(transactionDate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByTransactionDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDateBefore(java.util.Calendar transactionDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByTransactionDateBefore", startResult, maxRows, transactionDate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByDescriptionContaining(String description) throws DataAccessException {

		return findArViewerByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByGstTypeContaining(String gstType) throws DataAccessException {

		return findArViewerByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByPrimaryKey
	 *
	 */
	@Transactional
	public ArViewer findArViewerByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArViewerByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByPrimaryKey
	 *
	 */

	@Transactional
	public ArViewer findArViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArViewerByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArViewerById
	 *
	 */
	@Transactional
	public ArViewer findArViewerById(BigInteger id) throws DataAccessException {

		return findArViewerById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerById
	 *
	 */

	@Transactional
	public ArViewer findArViewerById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArViewerById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArViewer) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllArViewers
	 *
	 */
	@Transactional
	public Set<ArViewer> findAllArViewers() throws DataAccessException {

		return findAllArViewers(-1, -1);
	}

	/**
	 * JPQL Query - findAllArViewers
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findAllArViewers(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArViewers", startResult, maxRows);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findArViewerByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findArViewerByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findArViewerByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByReceiptOrderId
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByReceiptOrderId(BigInteger receiptOrderId) throws DataAccessException {

		return findArViewerByReceiptOrderId(receiptOrderId, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByReceiptOrderId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByReceiptOrderId(BigInteger receiptOrderId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByReceiptOrderId", startResult, maxRows, receiptOrderId);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByTransactionDate
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDate(java.util.Calendar transactionDate) throws DataAccessException {

		return findArViewerByTransactionDate(transactionDate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByTransactionDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDate(java.util.Calendar transactionDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByTransactionDate", startResult, maxRows, transactionDate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCode
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException {

		return findArViewerByOriginalCurrencyCode(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByOriginalCurrencyCode", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByMonthAfter
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findArViewerByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findArViewerByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByDescription
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByDescription(String description) throws DataAccessException {

		return findArViewerByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByApprovalCode
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByApprovalCode(String approvalCode) throws DataAccessException {

		return findArViewerByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByFxRate
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findArViewerByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByPayerNameContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByPayerNameContaining(String payerName) throws DataAccessException {

		return findArViewerByPayerNameContaining(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByPayerNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByPayerNameContaining(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByPayerNameContaining", startResult, maxRows, payerName);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByReceiptRate
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByReceiptRate(java.math.BigDecimal receiptRate) throws DataAccessException {

		return findArViewerByReceiptRate(receiptRate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByReceiptRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByReceiptRate(java.math.BigDecimal receiptRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByReceiptRate", startResult, maxRows, receiptRate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerBySettedIncludeGstConvertedAmount(java.math.BigDecimal settedIncludeGstConvertedAmount) throws DataAccessException {

		return findArViewerBySettedIncludeGstConvertedAmount(settedIncludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerBySettedIncludeGstConvertedAmount(java.math.BigDecimal settedIncludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerBySettedIncludeGstConvertedAmount", startResult, maxRows, settedIncludeGstConvertedAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByClassName
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByClassName(String className) throws DataAccessException {

		return findArViewerByClassName(className, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByClassName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByClassName(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByClassName", startResult, maxRows, className);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByRemainIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByRemainIncludeGstOriginalAmount(java.math.BigDecimal remainIncludeGstOriginalAmount) throws DataAccessException {

		return findArViewerByRemainIncludeGstOriginalAmount(remainIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByRemainIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByRemainIncludeGstOriginalAmount(java.math.BigDecimal remainIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByRemainIncludeGstOriginalAmount", startResult, maxRows, remainIncludeGstOriginalAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByGstType
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByGstType(String gstType) throws DataAccessException {

		return findArViewerByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByGstRate
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findArViewerByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByVarianceAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException {

		return findArViewerByVarianceAmount(varianceAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByVarianceAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByVarianceAmount(java.math.BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByVarianceAmount", startResult, maxRows, varianceAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerBySettedIncludeGstOriginalAmount(java.math.BigDecimal settedIncludeGstOriginalAmount) throws DataAccessException {

		return findArViewerBySettedIncludeGstOriginalAmount(settedIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerBySettedIncludeGstOriginalAmount(java.math.BigDecimal settedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerBySettedIncludeGstOriginalAmount", startResult, maxRows, settedIncludeGstOriginalAmount);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByMonthBefore
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findArViewerByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByClassNameContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByClassNameContaining(String className) throws DataAccessException {

		return findArViewerByClassNameContaining(className, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByClassNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByClassNameContaining(String className, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByClassNameContaining", startResult, maxRows, className);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByTransactionId
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByTransactionId(BigInteger transactionId) throws DataAccessException {

		return findArViewerByTransactionId(transactionId, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByTransactionId
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByTransactionId(BigInteger transactionId, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByTransactionId", startResult, maxRows, transactionId);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByArInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByArInvoiceNoContaining(String arInvoiceNo) throws DataAccessException {

		return findArViewerByArInvoiceNoContaining(arInvoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByArInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByArInvoiceNoContaining(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByArInvoiceNoContaining", startResult, maxRows, arInvoiceNo);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException {

		return findArViewerByOriginalCurrencyCodeContaining(originalCurrencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByOriginalCurrencyCodeContaining", startResult, maxRows, originalCurrencyCode);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByMonth
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByMonth(java.util.Calendar month) throws DataAccessException {

		return findArViewerByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByAccountName
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByAccountName(String accountName) throws DataAccessException {

		return findArViewerByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * JPQL Query - findArViewerByTransactionDateAfter
	 *
	 */
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDateAfter(java.util.Calendar transactionDate) throws DataAccessException {

		return findArViewerByTransactionDateAfter(transactionDate, -1, -1);
	}

	/**
	 * JPQL Query - findArViewerByTransactionDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArViewer> findArViewerByTransactionDateAfter(java.util.Calendar transactionDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArViewerByTransactionDateAfter", startResult, maxRows, transactionDate);
		return new LinkedHashSet<ArViewer>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArViewer entity) {
		return true;
	}
	
	@Transactional
    public void refresh() throws DataAccessException {
        getEntityManager().createNativeQuery("REFRESH MATERIALIZED VIEW ar_ledger").executeUpdate();
    }
}
