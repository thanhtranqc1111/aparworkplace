
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ArViewer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ArViewer entities.
 * 
 */
public interface ArViewerDAO extends JpaDao<ArViewer> {

	/**
	 * JPQL Query - findArViewerByArInvoiceNo
	 *
	 */
	public Set<ArViewer> findArViewerByArInvoiceNo(String arInvoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByArInvoiceNo
	 *
	 */
	public Set<ArViewer> findArViewerByArInvoiceNo(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByAccountNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByAccountNameContaining(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByAccountNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByApprovalCodeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByApprovalCodeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPayerName
	 *
	 */
	public Set<ArViewer> findArViewerByPayerName(String payerName) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPayerName
	 *
	 */
	public Set<ArViewer> findArViewerByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByInvoiceNo
	 *
	 */
	public Set<ArViewer> findArViewerByInvoiceNo(String invoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByInvoiceNo
	 *
	 */
	public Set<ArViewer> findArViewerByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDateBefore
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDateBefore(java.util.Calendar transactionDate) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDateBefore
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDateBefore(Calendar transactionDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByDescriptionContaining
	 *
	 */
	public Set<ArViewer> findArViewerByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByDescriptionContaining
	 *
	 */
	public Set<ArViewer> findArViewerByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstTypeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByGstTypeContaining(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstTypeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPrimaryKey
	 *
	 */
	public ArViewer findArViewerByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPrimaryKey
	 *
	 */
	public ArViewer findArViewerByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerById
	 *
	 */
	public ArViewer findArViewerById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerById
	 *
	 */
	public ArViewer findArViewerById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArViewers
	 *
	 */
	public Set<ArViewer> findAllArViewers() throws DataAccessException;

	/**
	 * JPQL Query - findAllArViewers
	 *
	 */
	public Set<ArViewer> findAllArViewers(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByInvoiceNoContaining
	 *
	 */
	public Set<ArViewer> findArViewerByInvoiceNoContaining(String invoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByInvoiceNoContaining
	 *
	 */
	public Set<ArViewer> findArViewerByInvoiceNoContaining(String invoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArViewer> findArViewerByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArViewer> findArViewerByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByReceiptOrderId
	 *
	 */
	public Set<ArViewer> findArViewerByReceiptOrderId(BigInteger receiptOrderId) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByReceiptOrderId
	 *
	 */
	public Set<ArViewer> findArViewerByReceiptOrderId(BigInteger receiptOrderId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDate
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDate(java.util.Calendar transactionDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDate
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDate(Calendar transactionDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCode
	 *
	 */
	public Set<ArViewer> findArViewerByOriginalCurrencyCode(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCode
	 *
	 */
	public Set<ArViewer> findArViewerByOriginalCurrencyCode(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonthAfter
	 *
	 */
	public Set<ArViewer> findArViewerByMonthAfter(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonthAfter
	 *
	 */
	public Set<ArViewer> findArViewerByMonthAfter(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByDescription
	 *
	 */
	public Set<ArViewer> findArViewerByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByDescription
	 *
	 */
	public Set<ArViewer> findArViewerByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByApprovalCode
	 *
	 */
	public Set<ArViewer> findArViewerByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByApprovalCode
	 *
	 */
	public Set<ArViewer> findArViewerByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByFxRate
	 *
	 */
	public Set<ArViewer> findArViewerByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByFxRate
	 *
	 */
	public Set<ArViewer> findArViewerByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPayerNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByPayerNameContaining(String payerName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByPayerNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByPayerNameContaining(String payerName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByReceiptRate
	 *
	 */
	public Set<ArViewer> findArViewerByReceiptRate(java.math.BigDecimal receiptRate) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByReceiptRate
	 *
	 */
	public Set<ArViewer> findArViewerByReceiptRate(BigDecimal receiptRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstConvertedAmount
	 *
	 */
	public Set<ArViewer> findArViewerBySettedIncludeGstConvertedAmount(java.math.BigDecimal settedIncludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstConvertedAmount
	 *
	 */
	public Set<ArViewer> findArViewerBySettedIncludeGstConvertedAmount(BigDecimal settedIncludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByClassName
	 *
	 */
	public Set<ArViewer> findArViewerByClassName(String className) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByClassName
	 *
	 */
	public Set<ArViewer> findArViewerByClassName(String className, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByRemainIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByRemainIncludeGstOriginalAmount(java.math.BigDecimal remainIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByRemainIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerByRemainIncludeGstOriginalAmount(BigDecimal remainIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstType
	 *
	 */
	public Set<ArViewer> findArViewerByGstType(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstType
	 *
	 */
	public Set<ArViewer> findArViewerByGstType(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstRate
	 *
	 */
	public Set<ArViewer> findArViewerByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByGstRate
	 *
	 */
	public Set<ArViewer> findArViewerByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByVarianceAmount
	 *
	 */
	public Set<ArViewer> findArViewerByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByVarianceAmount
	 *
	 */
	public Set<ArViewer> findArViewerByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerBySettedIncludeGstOriginalAmount(java.math.BigDecimal settedIncludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerBySettedIncludeGstOriginalAmount
	 *
	 */
	public Set<ArViewer> findArViewerBySettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonthBefore
	 *
	 */
	public Set<ArViewer> findArViewerByMonthBefore(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonthBefore
	 *
	 */
	public Set<ArViewer> findArViewerByMonthBefore(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByClassNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByClassNameContaining(String className_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByClassNameContaining
	 *
	 */
	public Set<ArViewer> findArViewerByClassNameContaining(String className_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionId
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionId(BigInteger transactionId) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionId
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionId(BigInteger transactionId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByArInvoiceNoContaining
	 *
	 */
	public Set<ArViewer> findArViewerByArInvoiceNoContaining(String arInvoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByArInvoiceNoContaining
	 *
	 */
	public Set<ArViewer> findArViewerByArInvoiceNoContaining(String arInvoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ArViewer> findArViewerByOriginalCurrencyCodeContaining(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonth
	 *
	 */
	public Set<ArViewer> findArViewerByMonth(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByMonth
	 *
	 */
	public Set<ArViewer> findArViewerByMonth(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByAccountName
	 *
	 */
	public Set<ArViewer> findArViewerByAccountName(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByAccountName
	 *
	 */
	public Set<ArViewer> findArViewerByAccountName(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDateAfter
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDateAfter(java.util.Calendar transactionDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findArViewerByTransactionDateAfter
	 *
	 */
	public Set<ArViewer> findArViewerByTransactionDateAfter(Calendar transactionDate_2, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * Execute statement: REFRESH MATERIALIZED VIEW ar_ledger;
	 */
	public void refresh() throws DataAccessException;

}