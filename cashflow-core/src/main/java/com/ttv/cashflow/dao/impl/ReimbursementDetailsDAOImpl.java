
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ReimbursementDetailsDAO;
import com.ttv.cashflow.domain.ReimbursementDetails;

/**
 * DAO to manage ReimbursementDetails entities.
 * 
 */
@Repository("ReimbursementDetailsDAO")

@Transactional
public class ReimbursementDetailsDAOImpl extends AbstractJpaDao<ReimbursementDetails>
		implements ReimbursementDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ReimbursementDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ReimbursementDetailsDAOImpl
	 *
	 */
	public ReimbursementDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCodeContaining
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCodeContaining(String projectCode) throws DataAccessException {

		return findReimbursementDetailsByProjectCodeContaining(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCodeContaining(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByProjectCodeContaining", startResult, maxRows, projectCode);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByDescription
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByDescription(String description) throws DataAccessException {

		return findReimbursementDetailsByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCode
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCode(String approvalCode) throws DataAccessException {

		return findReimbursementDetailsByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectName
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectName(String projectName) throws DataAccessException {

		return findReimbursementDetailsByProjectName(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByProjectName", startResult, maxRows, projectName);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findReimbursementDetailsByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findReimbursementDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findReimbursementDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsById
	 *
	 */
	@Transactional
	public ReimbursementDetails findReimbursementDetailsById(BigInteger id) throws DataAccessException {

		return findReimbursementDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsById
	 *
	 */

	@Transactional
	public ReimbursementDetails findReimbursementDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementDetailsByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByDescriptionContaining(String description) throws DataAccessException {

		return findReimbursementDetailsByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCode
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCode(String projectCode) throws DataAccessException {

		return findReimbursementDetailsByProjectCode(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCode(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByProjectCode", startResult, maxRows, projectCode);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findReimbursementDetailsByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllReimbursementDetailss
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findAllReimbursementDetailss() throws DataAccessException {

		return findAllReimbursementDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllReimbursementDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findAllReimbursementDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllReimbursementDetailss", startResult, maxRows);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findReimbursementDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findReimbursementDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ReimbursementDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findReimbursementDetailsByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectNameContaining
	 *
	 */
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectNameContaining(String projectName) throws DataAccessException {

		return findReimbursementDetailsByProjectNameContaining(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findReimbursementDetailsByProjectNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findReimbursementDetailsByProjectNameContaining", startResult, maxRows, projectName);
		return new LinkedHashSet<ReimbursementDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ReimbursementDetails entity) {
		return true;
	}

	@Override
	public Set<String> findReimbursementNoByApprovalCode(String approvalCode) {
		String queryString = "SELECT DISTINCT(ac.reimbursement.reimbursementNo) " + 
			 	 "FROM ReimbursementDetails ac " +
			 	 "WHERE ac.approvalCode = ?1";

		Query query = getEntityManager().createQuery(queryString).setParameter(1, approvalCode);
		
		return new LinkedHashSet<String>(query.getResultList());
	}
}
