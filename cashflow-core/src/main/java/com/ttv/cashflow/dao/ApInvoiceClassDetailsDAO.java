
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoiceClassDetails;
import com.ttv.cashflow.dto.ApInvoiceClassDetailDTO;
import com.ttv.cashflow.dto.SummaryApInvoiceClassDetailsDTO;

/**
 * DAO to manage ApInvoiceClassDetails entities.
 * 
 */
public interface ApInvoiceClassDetailsDAO extends JpaDao<ApInvoiceClassDetails> {

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGstAmount(java.math.BigDecimal gstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGstAmount(BigDecimal gstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByExcludeGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByExcludeGstAmount(BigDecimal excludeGstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGst
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGst(java.math.BigDecimal gst) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByGst
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByGst(BigDecimal gst, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByIncludeGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByIncludeGstAmount
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIncludeGstAmount(BigDecimal includeGstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescription
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescription
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceClassDetailss
	 *
	 */
	public Set<ApInvoiceClassDetails> findAllApInvoiceClassDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceClassDetailss
	 *
	 */
	public Set<ApInvoiceClassDetails> findAllApInvoiceClassDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCodeContaining(String vatCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCodeContaining(String vatCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPrimaryKey
	 *
	 */
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPrimaryKey
	 *
	 */
	public ApInvoiceClassDetails findApInvoiceClassDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassName
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassName(String className) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassName
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassName(String className, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCodeContaining(String classCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCodeContaining(String classCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassNameContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassNameContaining(String className_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassNameContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassNameContaining(String className_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByDescriptionContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCode(String vatCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByVatCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByVatCode(String vatCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNoContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNoContaining(String poNo) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNoContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsById
	 *
	 */
	public ApInvoiceClassDetails findApInvoiceClassDetailsById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsById
	 *
	 */
	public ApInvoiceClassDetails findApInvoiceClassDetailsById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByFxRate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByFxRate
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCode(String classCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByClassCode
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByClassCode(String classCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNo
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNo(String poNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceClassDetailsByPoNo
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByPoNo(String poNo_1, int startResult, int maxRows) throws DataAccessException;
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIsApproval(Boolean isApproval) throws DataAccessException;	
	public List<SummaryApInvoiceClassDetailsDTO> findByApprovalCodeNotApproved(String approvalCode) throws DataAccessException;
	public List<ApInvoiceClassDetailDTO> findDetailByApprovalCode(String approvalCode) throws DataAccessException;
	/**
	 * JPQL Query - findApInvoiceClassDetailsByIsApproval
	 *
	 */
	public Set<ApInvoiceClassDetails> findApInvoiceClassDetailsByIsApproval(Boolean isApproval, int startResult, int maxRows) throws DataAccessException;
	
	public Set<String> findArInvoiceNoByApprovalCode(String approvalCode);
}