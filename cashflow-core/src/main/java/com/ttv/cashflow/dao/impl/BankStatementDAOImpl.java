
package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.BankStatementDAO;
import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.util.Constant;

/**
 * DAO to manage BankStatement entities.
 * 
 */
@Repository("BankStatementDAO")

@Transactional
public class BankStatementDAOImpl extends AbstractJpaDao<BankStatement> implements BankStatementDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(0, "myBankStatement.id");
    	mapColumnIndex.put(1, "myBankStatement.transactionDate");
    	mapColumnIndex.put(2, "myBankStatement.valueDate");
    }
    
    private static Map<Integer, String> mapColumnReceiptOrderIndex = new HashMap<Integer, String>();
    static {
    	mapColumnReceiptOrderIndex.put(0, "myBankStatement.id");
    	mapColumnReceiptOrderIndex.put(1, "myBankStatement.transactionDate");
    	mapColumnReceiptOrderIndex.put(2, "myBankStatement.valueDate");
    }
    
	@Autowired
	private PaymentOrderDAO paymentOrderDAO;
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			BankStatement.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlow_Driver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new BankStatementDAOImpl
	 *
	 */
	public BankStatementDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findBankStatementByReference1
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByReference1(String reference1) throws DataAccessException {

		return findBankStatementByReference1(reference1, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByReference1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByReference1(String reference1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByReference1", startResult, maxRows, reference1);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByBankAccountNo
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByBankAccountNo(String bankAccountNo) throws DataAccessException {

		return findBankStatementByBankAccountNo(bankAccountNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByBankAccountNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByBankAccountNo(String bankAccountNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByBankAccountNo", startResult, maxRows, bankAccountNo);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByBalance
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByBalance(java.math.BigDecimal balance) throws DataAccessException {

		return findBankStatementByBalance(balance, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByBalance
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByBalance(java.math.BigDecimal balance, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByBalance", startResult, maxRows, balance);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementById
	 *
	 */
	@Transactional
	public BankStatement findBankStatementById(BigInteger id) throws DataAccessException {

		return findBankStatementById(id, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementById
	 *
	 */

	@Transactional
	public BankStatement findBankStatementById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBankStatementById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.BankStatement) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBankStatementByModifiedDate
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findBankStatementByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByPrimaryKey
	 *
	 */
	@Transactional
	public BankStatement findBankStatementByPrimaryKey(BigInteger id) throws DataAccessException {

		return findBankStatementByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByPrimaryKey
	 *
	 */

	@Transactional
	public BankStatement findBankStatementByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBankStatementByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.BankStatement) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBankStatementByReference2
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByReference2(String reference2) throws DataAccessException {

		return findBankStatementByReference2(reference2, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByReference2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByReference2(String reference2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByReference2", startResult, maxRows, reference2);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByTransactionDate
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByTransactionDate(java.util.Calendar transactionDate) throws DataAccessException {

		return findBankStatementByTransactionDate(transactionDate, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByTransactionDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByTransactionDate(java.util.Calendar transactionDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByTransactionDate", startResult, maxRows, transactionDate);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByDebit
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByDebit(java.math.BigDecimal debit) throws DataAccessException {

		return findBankStatementByDebit(debit, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByDebit
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByDebit(java.math.BigDecimal debit, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByDebit", startResult, maxRows, debit);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByBankName
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByBankName(String bankName) throws DataAccessException {

		return findBankStatementByBankName(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByBankName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByBankName(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByBankName", startResult, maxRows, bankName);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByDescription2Containing
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByDescription2Containing(String description2) throws DataAccessException {

		return findBankStatementByDescription2Containing(description2, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByDescription2Containing
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByDescription2Containing(String description2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByDescription2Containing", startResult, maxRows, description2);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByReference1Containing
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByReference1Containing(String reference1) throws DataAccessException {

		return findBankStatementByReference1Containing(reference1, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByReference1Containing
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByReference1Containing(String reference1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByReference1Containing", startResult, maxRows, reference1);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByDescription2
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByDescription2(String description2) throws DataAccessException {

		return findBankStatementByDescription2(description2, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByDescription2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByDescription2(String description2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByDescription2", startResult, maxRows, description2);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByDescription1Containing
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByDescription1Containing(String description1) throws DataAccessException {

		return findBankStatementByDescription1Containing(description1, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByDescription1Containing
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByDescription1Containing(String description1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByDescription1Containing", startResult, maxRows, description1);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByCreatedDate
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findBankStatementByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNo
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByPaymentOrderNo(String paymentOrderNo) throws DataAccessException {

		return findBankStatementByPaymentOrderNo(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByPaymentOrderNo(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByPaymentOrderNo", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByDescription1
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByDescription1(String description1) throws DataAccessException {

		return findBankStatementByDescription1(description1, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByDescription1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByDescription1(String description1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByDescription1", startResult, maxRows, description1);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByBankAccountNoContaining
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByBankAccountNoContaining(String bankAccountNo) throws DataAccessException {

		return findBankStatementByBankAccountNoContaining(bankAccountNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByBankAccountNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByBankAccountNoContaining(String bankAccountNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByBankAccountNoContaining", startResult, maxRows, bankAccountNo);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByReference2Containing
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByReference2Containing(String reference2) throws DataAccessException {

		return findBankStatementByReference2Containing(reference2, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByReference2Containing
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByReference2Containing(String reference2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByReference2Containing", startResult, maxRows, reference2);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByCredit
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByCredit(java.math.BigDecimal credit) throws DataAccessException {

		return findBankStatementByCredit(credit, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByCredit
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByCredit(java.math.BigDecimal credit, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByCredit", startResult, maxRows, credit);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllBankStatements
	 *
	 */
	@Transactional
	public Set<BankStatement> findAllBankStatements() throws DataAccessException {

		return findAllBankStatements(-1, -1);
	}

	/**
	 * JPQL Query - findAllBankStatements
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findAllBankStatements(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllBankStatements", startResult, maxRows);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByBankNameContaining
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByBankNameContaining(String bankName) throws DataAccessException {

		return findBankStatementByBankNameContaining(bankName, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByBankNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByBankNameContaining(String bankName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByBankNameContaining", startResult, maxRows, bankName);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNoContaining
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByPaymentOrderNoContaining(String paymentOrderNo) throws DataAccessException {

		return findBankStatementByPaymentOrderNoContaining(paymentOrderNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByPaymentOrderNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByPaymentOrderNoContaining(String paymentOrderNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByPaymentOrderNoContaining", startResult, maxRows, paymentOrderNo);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankStatementByValueDate
	 *
	 */
	@Transactional
	public Set<BankStatement> findBankStatementByValueDate(java.util.Calendar valueDate) throws DataAccessException {

		return findBankStatementByValueDate(valueDate, -1, -1);
	}

	/**
	 * JPQL Query - findBankStatementByValueDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankStatement> findBankStatementByValueDate(java.util.Calendar valueDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankStatementByValueDate", startResult, maxRows, valueDate);
		return new LinkedHashSet<BankStatement>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(BankStatement entity) {
		return true;
	}

	public int countBankStatementFilter(String bankName, String bankAccNo) {
		// TODO Auto-generated method stub
		StringBuilder queryString = new StringBuilder("select count(1) from BankStatement myBankStatement "
				+ "where bankName = :bankName "
				+ "and bankAccountNo = :bankAccountNo ");
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countBankStatementAll() {
		Query query = createNamedQuery("countBankStatementAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<BankStatement> findBankStatementPaging(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter) {
		StringBuilder queryString = new StringBuilder("SELECT * from bank_statement "
													+ "WHERE bank_name = cast(:bankName as text) "
													+ "and bank_account_no = cast(:bankAccountNo as text) ");
		if(transDateFilter == 0) {
			if(transactionFrom != null)
			{
				queryString.append("and transaction_date >= cast(:transactionFrom as date) ");
			}
			if(transactionTo != null)
			{
				queryString.append("and transaction_date <= cast(:transactionTo as date) ");
			
			}
		}
		else {
			queryString.append("and transaction_date >= current_date - INTERVAL '" + transDateFilter + " month' ");
		}
		queryString.append("order by transaction_date asc, id asc ");
		
		Query query = getEntityManager().createNativeQuery(queryString.toString(), BankStatement.class);
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		if(transDateFilter == 0) {
			if(transactionFrom != null)
			{
				query.setParameter("transactionFrom", transactionFrom);
			}
			if(transactionTo != null)
			{
				query.setParameter("transactionTo", transactionTo);
			}
		}
		
		LinkedHashSet<BankStatement> resultSet = new LinkedHashSet<BankStatement>(query.getResultList());
		Iterator<BankStatement> iterator = resultSet.iterator();
		while(iterator.hasNext()) {
			BankStatement bankStatement = iterator.next();
			PaymentOrder paymentOrder = paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(bankStatement.getPaymentOrderNo());
			if(paymentOrder != null && paymentOrder.getId().intValue() > 0) {
				bankStatement.setPaymentOrderId(paymentOrder.getId());
			}
		}
		return resultSet;
	}

	public List<PaymentOrderDTO> getPaymentOrderPaid(Set<String> setPaymentOrderNo) {
		List<PaymentOrderDTO> list = new ArrayList<PaymentOrderDTO>();
		try {
			StringBuilder arrayPaymentOrder = new StringBuilder();
			int i = 0;
			for(String paymentOrderNo: setPaymentOrderNo)
			{
				i ++;
				if(i < setPaymentOrderNo.size())
					arrayPaymentOrder.append("'" + paymentOrderNo + "',");
				else
					arrayPaymentOrder.append("'" + paymentOrderNo + "'");
			}
			Query query = entityManager.createNativeQuery("select payment_order_no, sum(debit) as total_debit " + 
														 "from bank_statement " + 
														 "where payment_order_no in (" + arrayPaymentOrder.toString() +") " + 
														 "group by payment_order_no");
			Set<Object[]> objMaster = new LinkedHashSet<Object[]>(query.getResultList());
			for (Object[] objects : objMaster) {
				PaymentOrderDTO obj = new PaymentOrderDTO();
				obj.setPaymentOrderNo((String)objects[0]);
				obj.setPaymentTotalAmount((BigDecimal) objects[1]);
				list.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Set<BankStatement> findBankStatementPagingForExport(Integer start, Integer end, String bankName,
			String bankAccNo, Calendar transactionFrom, Calendar transactionTo) {
		StringBuilder queryString = new StringBuilder("select myBankStatement from BankStatement myBankStatement "
				+ "where bankName = coalesce(cast(:bankName as text),bankName) "
				+ "and bankAccountNo = coalesce(cast(:bankAccountNo as text), bankAccountNo) ");
		if(transactionFrom != null)
			queryString.append("and transactionDate >= cast(:transactionFrom as date) ");
		if(transactionTo != null)
			queryString.append("and transactionDate <= cast(:transactionTo as date) ");
		queryString.append("order by bankAccountNo desc, transactionDate asc");
		
		Query query = createQuery(queryString.toString(), start, end);
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		if(transactionFrom != null)
			query.setParameter("transactionFrom", transactionFrom);
		if(transactionTo != null)
			query.setParameter("transactionTo", transactionTo);
		LinkedHashSet<BankStatement> resultSet = new LinkedHashSet<BankStatement>(query.getResultList());
		Iterator<BankStatement> iterator = resultSet.iterator();
		while(iterator.hasNext()) {
			BankStatement bankStatement = iterator.next();
			Set<PaymentOrder> setPaymentOrder = paymentOrderDAO.findPaymentOrderByPaymentOrderNo(bankStatement.getPaymentOrderNo(), Constant.PAYMENT_ORDER_APINVOICE);
			if(setPaymentOrder.size() > 0) {
				Iterator<PaymentOrder> iteratorPaymentOrder = setPaymentOrder.iterator();
				PaymentOrder paymentOrder = iteratorPaymentOrder.next();
				bankStatement.setPaymentOrderId(paymentOrder.getId());
			}
		}
		return resultSet;
	}

	@Override
	public int checkExists(BankStatement bankStatement) {
		Query query = createNamedQuery("checkBankStatementExists", -1, -1, bankStatement.getTransactionDate(), bankStatement.getDebit(), bankStatement.getCredit(), bankStatement.getBankName(), bankStatement.getBankAccountNo());
		if(query.getResultList().size() > 0)
		{
			return ((Number)query.getSingleResult()).intValue();
		}
		else
		{
			return 0;
		}
	}

	@Override
	public Calendar getMaxTransactionDate(String bankName, String bankAccNo) {
		Query query = createNamedQuery("getMaxTransactionDate", -1, -1);
		query.setParameter(1, bankName);
		query.setParameter(2, bankAccNo);
		if(query.getResultList().size() > 0)
		{
			return ((Calendar)query.getSingleResult());
		}
		else
		{
			return null;
		}
	}

	@Override
	public int countBankStatementFilter(String bankName, String bankAccNo, Calendar transactionFrom,
										Calendar transactionTo, BigDecimal credit, Integer option, int status) {
		StringBuilder queryString = new StringBuilder("SELECT count(1) "
													+ "FROM BankStatement myBankStatement "
													+ "WHERE bankName = coalesce(cast(:bankName as text),bankName) "
														  + "and bankAccountNo = coalesce(cast(:bankAccountNo as text), bankAccountNo) ");
		if (transactionFrom != null) {
			queryString.append("and transactionDate >= cast(:transactionFrom as date) ");
		}
		if (transactionTo != null) {
			queryString.append("and transactionDate <= cast(:transactionTo as date) ");
		}
		if (credit != null && credit.compareTo(BigDecimal.ZERO) == 1) {
			switch (option) {
			case 0:
				queryString.append("and credit = :credit ");
				break;
			case 1:
				queryString.append("and credit >= :credit ");
				break;
			case -1:
				queryString.append("and credit <= :credit ");
				break;
			default:
				break;
			}
		}
		else
		{
			queryString.append("and credit > 0 ");
		}
		switch (status) {
			case 1:
				queryString.append("and receiptOrderNo is not null ");
				break;
			case 2:
				queryString.append("and receiptOrderNo is null ");
				break;
			default:
				break;
		}
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		if (transactionFrom != null) {
			query.setParameter("transactionFrom", transactionFrom);
		}
		if (transactionTo != null) {
			query.setParameter("transactionTo", transactionTo);
		}
		if (credit != null && credit.compareTo(BigDecimal.ZERO) == 1) {
			query.setParameter("credit", credit);
		}
		return ((Number) query.getSingleResult()).intValue();
	}

	@Override
	public List<BigInteger> getBankStatementIds(Integer start, Integer end, String bankName, String bankAccNo,
												Calendar transactionFrom, Calendar transactionTo, BigDecimal credit, Integer option, Integer orderColumn, String orderBy, int status) {
		StringBuilder queryString = new StringBuilder("SELECT myBankStatement.id "
													+ "FROM BankStatement myBankStatement "
													+ "WHERE bankName = coalesce(cast(:bankName as text),bankName) "
														  + "and bankAccountNo = coalesce(cast(:bankAccountNo as text), bankAccountNo) ");
		if (transactionFrom != null) {
			queryString.append("and transactionDate >= cast(:transactionFrom as date) ");
		}
		if (transactionTo != null) {
			queryString.append("and transactionDate <= cast(:transactionTo as date) ");
		}
		if (credit != null && credit.compareTo(BigDecimal.ZERO) == 1) {
			switch (option) {
			case 0:
				queryString.append("and credit = :credit ");
				break;
			case 1:
				queryString.append("and credit >= :credit ");
				break;
			case -1:
				queryString.append("and credit <= :credit ");
				break;
			default:
				break;
			}
		}
		else
		{
			queryString.append("and credit > 0 ");
		}
		switch (status) {
			case 1:
				queryString.append("and receiptOrderNo is not null ");
				break;
			case 2:
				queryString.append("and receiptOrderNo is null ");
				break;
			default:
				break;
		}
		queryString.append("order by " + mapColumnReceiptOrderIndex.get(orderColumn) + " " + orderBy);
		Query query = createQuery(queryString.toString(), start, end);
		query.setParameter("bankName", (bankName != null && bankName.isEmpty()) ? null : bankName);
		query.setParameter("bankAccountNo", (bankAccNo != null && bankAccNo.isEmpty()) ? null : bankAccNo);
		if (transactionFrom != null) {
			query.setParameter("transactionFrom", transactionFrom);
		}
		if (transactionTo != null) {
			query.setParameter("transactionTo", transactionTo);
		}
		if (credit != null && credit.compareTo(BigDecimal.ZERO) == 1) {
			query.setParameter("credit", credit);
		}
		return query.getResultList();
	}

	@Override
	public List<BigInteger> getBankStatementIds(String receiptOrderNo, Integer orderColumn, String orderBy) {
		StringBuilder queryString = new StringBuilder("SELECT myBankStatement.id "
													+ "FROM BankStatement myBankStatement "
													+ "WHERE receiptOrderNo like :receiptOrderNo ");

		queryString.append("order by " + mapColumnReceiptOrderIndex.get(orderColumn) + " " + orderBy);
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter("receiptOrderNo", "%" + receiptOrderNo + "%");
		return query.getResultList();
	}

	@Override
	public void updateBalanceBankStatement(BigInteger bankId, String bankName, String bankAccountNo,
										   BigDecimal newCredit, BigDecimal newDebit) {
		Query query = getEntityManager().createNativeQuery("select update_balance_bank_statement(:bankId, :bankName , :bankAccountNo, :newCredit , :newDebit)")
                						.setParameter("bankId", bankId)
                						.setParameter("bankName", bankName)
										.setParameter("bankAccountNo", bankAccountNo)
										.setParameter("newCredit", newCredit)
										.setParameter("newDebit", newDebit);
		query.getSingleResult();
	}
}
