
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ArInvoiceReceiptStatusDAO;
import com.ttv.cashflow.domain.ArInvoiceReceiptStatus;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ArInvoiceReceiptStatus entities.
 * 
 */
@Repository("ArInvoiceReceiptStatusDAO")

@Transactional
public class ArInvoiceReceiptStatusDAOImpl extends AbstractJpaDao<ArInvoiceReceiptStatus>
		implements ArInvoiceReceiptStatusDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArInvoiceReceiptStatus.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArInvoiceReceiptStatusDAOImpl
	 *
	 */
	public ArInvoiceReceiptStatusDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount(java.math.BigDecimal unreceivedIncludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount(unreceivedIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount(java.math.BigDecimal unreceivedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceReceiptStatusByUnreceivedIncludeGstOriginalAmount", startResult, maxRows, unreceivedIncludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceReceiptStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByPrimaryKey
	 *
	 */
	@Transactional
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArInvoiceReceiptStatusByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByPrimaryKey
	 *
	 */

	@Transactional
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceReceiptStatusByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceReceiptStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount(java.math.BigDecimal receivedIncludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount(receivedIncludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceReceiptStatus> findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount(java.math.BigDecimal receivedIncludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceReceiptStatusByReceivedIncludeGstOriginalAmount", startResult, maxRows, receivedIncludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceReceiptStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllArInvoiceReceiptStatuss
	 *
	 */
	@Transactional
	public Set<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss() throws DataAccessException {

		return findAllArInvoiceReceiptStatuss(-1, -1);
	}

	/**
	 * JPQL Query - findAllArInvoiceReceiptStatuss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceReceiptStatus> findAllArInvoiceReceiptStatuss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArInvoiceReceiptStatuss", startResult, maxRows);
		return new LinkedHashSet<ArInvoiceReceiptStatus>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusById
	 *
	 */
	@Transactional
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusById(BigInteger id) throws DataAccessException {

		return findArInvoiceReceiptStatusById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceReceiptStatusById
	 *
	 */

	@Transactional
	public ArInvoiceReceiptStatus findArInvoiceReceiptStatusById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceReceiptStatusById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceReceiptStatus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArInvoiceReceiptStatus entity) {
		return true;
	}
	
	/**
     * JPQL Query - findArInvoiceReceiptStatusByArInvoiceId
     *
     */
    @Transactional
    public ArInvoiceReceiptStatus findArInvoiceReceiptStatusByArInvoiceId(BigInteger arInvoiceId) throws DataAccessException {
        try {
            Query query = createNamedQuery("findArInvoiceReceiptStatusByArInvoiceId", null, null, arInvoiceId);
            return (ArInvoiceReceiptStatus) query.getSingleResult();
        } catch (NoResultException nre) {
        	return new ArInvoiceReceiptStatus();
        }
    }
}
