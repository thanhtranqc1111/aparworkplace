
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.BankAccountMasterDAO;
import com.ttv.cashflow.domain.BankAccountMaster;

/**
 * DAO to manage BankAccountMaster entities.
 * 
 */
@Repository("BankAccountMasterDAO")

@Transactional
public class BankAccountMasterDAOImpl extends AbstractJpaDao<BankAccountMaster> implements BankAccountMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myBankAccountMaster.code");
    	mapColumnIndex.put(2, "myBankAccountMaster.type");
    	mapColumnIndex.put(3, "myBankAccountMaster.name");
    	mapColumnIndex.put(4, "myBankAccountMaster.bankAccountNo");
    	mapColumnIndex.put(5, "myBankAccountMaster.bankCode");
    	mapColumnIndex.put(6, "myBankAccountMaster.currencyCode");
    	mapColumnIndex.put(7, "myBankAccountMaster.beginningBalance");
    	mapColumnIndex.put(8, "myBankAccountMaster.endingBalance");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			BankAccountMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new BankAccountMasterDAOImpl
	 *
	 */
	public BankAccountMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findBankAccountMasterByTypeContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByTypeContaining(String type) throws DataAccessException {

		return findBankAccountMasterByTypeContaining(type, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByTypeContaining(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByTypeContaining", startResult, maxRows, type);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByCode
	 *
	 */
	@Transactional
	public BankAccountMaster findBankAccountMasterByCode(String code) throws DataAccessException {

		return findBankAccountMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByCode
	 *
	 */

	@Transactional
	public BankAccountMaster findBankAccountMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBankAccountMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.BankAccountMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCode
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCode(String currencyCode) throws DataAccessException {

		return findBankAccountMasterByCurrencyCode(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByCurrencyCode", startResult, maxRows, currencyCode);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankCodeContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankCodeContaining(String bankCode) throws DataAccessException {

		return findBankAccountMasterByBankCodeContaining(bankCode, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankCodeContaining(String bankCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByBankCodeContaining", startResult, maxRows, bankCode);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNoContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNoContaining(String bankAccountNo) throws DataAccessException {

		return findBankAccountMasterByBankAccountNoContaining(bankAccountNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNoContaining(String bankAccountNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByBankAccountNoContaining", startResult, maxRows, bankAccountNo);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByType
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByType(String type) throws DataAccessException {

		return findBankAccountMasterByType(type, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByType(String type, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByType", startResult, maxRows, type);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByName
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByName(String name) throws DataAccessException {

		return findBankAccountMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByNameContaining(String name) throws DataAccessException {

		return findBankAccountMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllBankAccountMasters
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findAllBankAccountMasters() throws DataAccessException {

		return findAllBankAccountMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllBankAccountMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findAllBankAccountMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllBankAccountMasters", startResult, maxRows);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankCode
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode) throws DataAccessException {

		return findBankAccountMasterByBankCode(bankCode, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankCode(String bankCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByBankCode", startResult, maxRows, bankCode);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByPrimaryKey
	 *
	 */
	@Transactional
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code) throws DataAccessException {

		return findBankAccountMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByPrimaryKey
	 *
	 */

	@Transactional
	public BankAccountMaster findBankAccountMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBankAccountMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.BankAccountMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNo
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNo(String bankAccountNo) throws DataAccessException {

		return findBankAccountMasterByBankAccountNo(bankAccountNo, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByBankAccountNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByBankAccountNo(String bankAccountNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByBankAccountNo", startResult, maxRows, bankAccountNo);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCodeContaining(String code) throws DataAccessException {

		return findBankAccountMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCodeContaining(String currencyCode) throws DataAccessException {

		return findBankAccountMasterByCurrencyCodeContaining(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findBankAccountMasterByCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<BankAccountMaster> findBankAccountMasterByCurrencyCodeContaining(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBankAccountMasterByCurrencyCodeContaining", startResult, maxRows, currencyCode);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(BankAccountMaster entity) {
		return true;
	}

	public int countBankAccountMasterFilter(Integer start, Integer end, String keyword) {
		Query query = createNamedQuery("countBankAccountMasterPaging", start, end, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countBankAccountMasterAll() {
		Query query = createNamedQuery("countBankAccountMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<BankAccountMaster> findBankAccountMasterPaging(Integer start, Integer end, Integer orderColumn,
			String orderBy, String keyword) {
		Query query = createQuery("select myBankAccountMaster from BankAccountMaster myBankAccountMaster where lower(code) like lower(CONCAT('%', ?1, '%')) or "
							    + "lower(type) like lower(CONCAT('%', ?1, '%')) or "
							    + "lower(name) like lower(CONCAT('%', ?1, '%')) or "
							    + "lower(bankAccountNo) like lower(CONCAT('%', ?1, '%')) or "
							    + "lower(bankCode) like lower(CONCAT('%', ?1, '%')) or "
							    + "lower(currencyCode) like lower(CONCAT('%', ?1, '%'))" + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}

	@Override
	public Set<BankAccountMaster> findBankAccountMasterByBankNameAndBankAccountNo(String bankName, String bankAccountNo) {
		Query query = createNamedQuery("findBankAccountMasterByBankNameAndBankAccountNo", -1, -1, bankName, bankAccountNo);
		return new LinkedHashSet<BankAccountMaster>(query.getResultList());
	}
}
