
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ArInvoiceAccountDetailsDAO;
import com.ttv.cashflow.domain.ArInvoiceAccountDetails;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ArInvoiceAccountDetails entities.
 * 
 */
@Repository("ArInvoiceAccountDetailsDAO")

@Transactional
public class ArInvoiceAccountDetailsDAOImpl extends AbstractJpaDao<ArInvoiceAccountDetails>
		implements ArInvoiceAccountDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArInvoiceAccountDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArInvoiceAccountDetailsDAOImpl
	 *
	 */
	public ArInvoiceAccountDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findArInvoiceAccountDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsById
	 *
	 */
	@Transactional
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsById(BigInteger id) throws DataAccessException {

		return findArInvoiceAccountDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsById
	 *
	 */

	@Transactional
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceAccountDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceAccountDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCodeContaining(String accountCode) throws DataAccessException {

		return findArInvoiceAccountDetailsByAccountCodeContaining(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByAccountCodeContaining", startResult, maxRows, accountCode);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllArInvoiceAccountDetailss
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss() throws DataAccessException {

		return findAllArInvoiceAccountDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllArInvoiceAccountDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findAllArInvoiceAccountDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArInvoiceAccountDetailss", startResult, maxRows);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArInvoiceAccountDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ArInvoiceAccountDetails findArInvoiceAccountDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceAccountDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceAccountDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountNameContaining(String accountName) throws DataAccessException {

		return findArInvoiceAccountDetailsByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByExcludeGstConvertedAmount(excludeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByExcludeGstConvertedAmount", startResult, maxRows, excludeGstConvertedAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByIncludeGstConvertedAmount(includeGstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByIncludeGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByIncludeGstConvertedAmount", startResult, maxRows, includeGstConvertedAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstConvertedAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByGstConvertedAmount(gstConvertedAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstConvertedAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByGstConvertedAmount", startResult, maxRows, gstConvertedAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByGstOriginalAmount(gstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByGstOriginalAmount", startResult, maxRows, gstOriginalAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCode(String accountCode) throws DataAccessException {

		return findArInvoiceAccountDetailsByAccountCode(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountCode(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByAccountCode", startResult, maxRows, accountCode);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findArInvoiceAccountDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceAccountDetailsByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountName
	 *
	 */
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountName(String accountName) throws DataAccessException {

		return findArInvoiceAccountDetailsByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceAccountDetailsByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceAccountDetails> findArInvoiceAccountDetailsByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceAccountDetailsByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ArInvoiceAccountDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArInvoiceAccountDetails entity) {
		return true;
	}
	
	@Transactional
    public BigInteger getLastId() {
        String sql = "SELECT id FROM ar_invoice_account_details ORDER BY id DESC LIMIT 1";

        Query query = getEntityManager().createNativeQuery(sql);

        BigInteger ret = (BigInteger) query.getSingleResult();

        return ret;

    }
}
