
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;

/**
 * DAO to manage ApprovalSalesOrderRequest entities.
 * 
 */
public interface ApprovalSalesOrderRequestDAO extends JpaDao<ApprovalSalesOrderRequest> {

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateBefore(java.util.Calendar approvedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateBefore(Calendar approvedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityTo
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityTo(java.util.Calendar validityTo) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityTo
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityTo(Calendar validityTo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateAfter(java.util.Calendar applicationDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateAfter(Calendar applicationDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurpose
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurpose(String purpose) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurpose
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurpose(String purpose, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDate(java.util.Calendar approvedDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDate(Calendar approvedDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByModifiedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByModifiedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurposeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurposeContaining(String purpose_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurposeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurposeContaining(String purpose_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromBefore(java.util.Calendar validityFrom) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromBefore(Calendar validityFrom, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFrom
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFrom(java.util.Calendar validityFrom_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFrom
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFrom(Calendar validityFrom_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByIncludeGstOriginalAmount
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateBefore(java.util.Calendar applicationDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateBefore(Calendar applicationDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToBefore(java.util.Calendar validityTo_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToBefore
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToBefore(Calendar validityTo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicant
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicant(String applicant) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicant
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicant(String applicant, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestById
	 *
	 */
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestById
	 *
	 */
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCode
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCode(String currencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCode
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCodeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCodeContaining(String currencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCodeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCodeContaining(String currencyCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCodeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCodeContaining(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCodeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCode
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCode(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCode
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCode(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToAfter(java.util.Calendar validityTo_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToAfter(Calendar validityTo_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPrimaryKey
	 *
	 */
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPrimaryKey
	 *
	 */
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDate(java.util.Calendar applicationDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDate(Calendar applicationDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCreatedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCreatedDate
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalTypeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalTypeContaining(String approvalType) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalTypeContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalTypeContaining(String approvalType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalType
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalType(String approvalType_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalType
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalType(String approvalType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromAfter(java.util.Calendar validityFrom_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromAfter(Calendar validityFrom_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalSalesOrderRequests
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests() throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalSalesOrderRequests
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicantContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicantContaining(String applicant_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicantContaining
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicantContaining(String applicant_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateAfter(java.util.Calendar approvedDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateAfter
	 *
	 */
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateAfter(Calendar approvedDate_2, int startResult, int maxRows) throws DataAccessException;
	public int countApprovalFilter(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo);
	public int countApprovalAll();
	public Set<ApprovalSalesOrderRequest> findApprovalPaging(Integer start, Integer end, String approvalType, String approvalCode, Calendar validityFrom, Calendar validityTo, Integer orderColumn, String orderBy);
	public int checkExists(ApprovalSalesOrderRequest approval);
	
	/**
	 * Searching Approval by available for AP Invoice specify.
	 */
	public List<ApprovalSalesOrderRequest> findApprovalAvailable(String approvalCode, String issueDate) throws DataAccessException;

    public List<ApprovalSalesOrderRequest> findApproval(String keyword);

    boolean checkExistingApprovalCode(String approvalCode);
}