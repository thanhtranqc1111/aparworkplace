
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.RolePermissionDAO;
import com.ttv.cashflow.domain.RolePermission;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage RolePermission entities.
 * 
 */
@Repository("RolePermissionDAO")

@Transactional
public class RolePermissionDAOImpl extends AbstractJpaDao<RolePermission> implements RolePermissionDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			RolePermission.class }));

	/**
	 * EntityManager injected by Spring for persistence unit TestDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new RolePermissionDAOImpl
	 *
	 */
	public RolePermissionDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllRolePermissions
	 *
	 */
	@Transactional
	public Set<RolePermission> findAllRolePermissions() throws DataAccessException {

		return findAllRolePermissions(-1, -1);
	}

	/**
	 * JPQL Query - findAllRolePermissions
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<RolePermission> findAllRolePermissions(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllRolePermissions", startResult, maxRows);
		return new LinkedHashSet<RolePermission>(query.getResultList());
	}

	/**
	 * JPQL Query - findRolePermissionByModifiedDate
	 *
	 */
	@Transactional
	public Set<RolePermission> findRolePermissionByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findRolePermissionByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findRolePermissionByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<RolePermission> findRolePermissionByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRolePermissionByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<RolePermission>(query.getResultList());
	}

	/**
	 * JPQL Query - findRolePermissionById
	 *
	 */
	@Transactional
	public RolePermission findRolePermissionById(Integer id) throws DataAccessException {

		return findRolePermissionById(id, -1, -1);
	}

	/**
	 * JPQL Query - findRolePermissionById
	 *
	 */

	@Transactional
	public RolePermission findRolePermissionById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findRolePermissionById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.RolePermission) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findRolePermissionByPrimaryKey
	 *
	 */
	@Transactional
	public RolePermission findRolePermissionByPrimaryKey(Integer id) throws DataAccessException {

		return findRolePermissionByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findRolePermissionByPrimaryKey
	 *
	 */

	@Transactional
	public RolePermission findRolePermissionByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findRolePermissionByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.RolePermission) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findRolePermissionByCreatedDate
	 *
	 */
	@Transactional
	public Set<RolePermission> findRolePermissionByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findRolePermissionByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findRolePermissionByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<RolePermission> findRolePermissionByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findRolePermissionByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<RolePermission>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(RolePermission entity) {
		return true;
	}

	public Set<RolePermission> findRolePermissionByRoleIdAndPermissionId(Integer roleId, Integer permissionId) {
		Query query = createNamedQuery("findRolePermissionByRoleIdAndPermissionId", -1, -1, roleId, permissionId);
		return new LinkedHashSet<RolePermission>(query.getResultList());
	}

	@Override
	public Set<RolePermission> findRolePermissionByRoleId(Integer roleId) {
		Query query = createNamedQuery("findRolePermissionByRoleId", -1, -1, roleId);
		return new LinkedHashSet<RolePermission>(query.getResultList());
	}
}
