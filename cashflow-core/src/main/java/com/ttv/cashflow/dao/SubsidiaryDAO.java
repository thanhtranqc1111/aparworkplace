
package com.ttv.cashflow.dao;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.Subsidiary;

/**
 * DAO to manage Subsidiary entities.
 * 
 */
public interface SubsidiaryDAO extends JpaDao<Subsidiary> {

	/**
	 * JPQL Query - findSubsidiaryByWebsite
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByWebsite(String website) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByWebsite
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByWebsite(String website, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByDescriptionContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByDescriptionContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByName
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByName
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByDescription
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByDescription
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPrimaryKey
	 *
	 */
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPrimaryKey
	 *
	 */
	public Subsidiary findSubsidiaryByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByNameContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByNameContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPhoneContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByPhoneContaining(String phone) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPhoneContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByPhoneContaining(String phone, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByModifiedDate
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByModifiedDate
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCreatedDate
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCreatedDate
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByIsActive
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByIsActive(Boolean isActive) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByIsActive
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByIsActive(Boolean isActive, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCode
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCode
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCodeContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCodeContaining(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByCodeContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByCodeContaining(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllSubsidiarys
	 *
	 */
	public Set<Subsidiary> findAllSubsidiarys() throws DataAccessException;

	/**
	 * JPQL Query - findAllSubsidiarys
	 *
	 */
	public Set<Subsidiary> findAllSubsidiarys(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryById
	 *
	 */
	public Subsidiary findSubsidiaryById(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryById
	 *
	 */
	public Subsidiary findSubsidiaryById(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByAddress
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByAddress(String address) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByAddress
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByAddress(String address, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByWebsiteContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByWebsiteContaining(String website_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByWebsiteContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByWebsiteContaining(String website_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByAddressContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByAddressContaining(String address_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByAddressContaining
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByAddressContaining(String address_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPhone
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByPhone(String phone_1) throws DataAccessException;

	/**
	 * JPQL Query - findSubsidiaryByPhone
	 *
	 */
	public Set<Subsidiary> findSubsidiaryByPhone(String phone_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * find Subsidiary By Name/Code
	 * @param keywork
	 * @param startResult
	 * @param maxRows
	 * @return
	 * @throws DataAccessException
	 */
	public Set<Subsidiary> findSubsidiaryByNameOrCode(String keywork) throws DataAccessException;

    List<Subsidiary> findListSubsidiaryByUserId(Integer userId);

}