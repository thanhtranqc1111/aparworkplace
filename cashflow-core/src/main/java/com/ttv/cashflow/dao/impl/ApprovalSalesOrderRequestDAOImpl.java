
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ApprovalSalesOrderRequestDAO;
import com.ttv.cashflow.dao.ArInvoiceClassDetailsDAO;
import com.ttv.cashflow.dao.ReimbursementDetailsDAO;
import com.ttv.cashflow.domain.ApprovalSalesOrderRequest;

/**
 * DAO to manage ApprovalSalesOrderRequest entities.
 * 
 */
@Repository("ApprovalSalesOrderRequestDAO")

@Transactional
public class ApprovalSalesOrderRequestDAOImpl extends AbstractJpaDao<ApprovalSalesOrderRequest>
		implements ApprovalSalesOrderRequestDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(2, "approval_code");
    	mapColumnIndex.put(3, "application_date");
    	mapColumnIndex.put(6, "include_gst_original_amount");
    	mapColumnIndex.put(9, "approved_date");
    }
    @Autowired
    private ArInvoiceClassDetailsDAO arInvoiceClassDetailsDAO;
 
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ApprovalSalesOrderRequest.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ApprovalSalesOrderRequestDAOImpl
	 *
	 */
	public ApprovalSalesOrderRequestDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateBefore
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateBefore(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovedDateBefore(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateBefore(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovedDateBefore", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityTo
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityTo(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityTo(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityTo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityTo(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityTo", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateAfter
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateAfter(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApplicationDateAfter(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateAfter(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApplicationDateAfter", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurpose
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurpose(String purpose) throws DataAccessException {

		return findApprovalSalesOrderRequestByPurpose(purpose, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurpose
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurpose(String purpose, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByPurpose", startResult, maxRows, purpose);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDate
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDate(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovedDate(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDate(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovedDate", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByModifiedDate
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurposeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurposeContaining(String purpose) throws DataAccessException {

		return findApprovalSalesOrderRequestByPurposeContaining(purpose, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPurposeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByPurposeContaining(String purpose, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByPurposeContaining", startResult, maxRows, purpose);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromBefore
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromBefore(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityFromBefore(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromBefore(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityFromBefore", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFrom
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFrom(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityFrom(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFrom
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFrom(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityFrom", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByIncludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException {

		return findApprovalSalesOrderRequestByIncludeGstOriginalAmount(includeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByIncludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByIncludeGstOriginalAmount", startResult, maxRows, includeGstOriginalAmount);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateBefore
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateBefore(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApplicationDateBefore(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDateBefore(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApplicationDateBefore", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToBefore
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToBefore(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityToBefore(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToBefore(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityToBefore", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicant
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicant(String applicant) throws DataAccessException {

		return findApprovalSalesOrderRequestByApplicant(applicant, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicant
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicant(String applicant, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApplicant", startResult, maxRows, applicant);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestById
	 *
	 */
	@Transactional
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestById(BigInteger id) throws DataAccessException {

		return findApprovalSalesOrderRequestById(id, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestById
	 *
	 */

	@Transactional
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalSalesOrderRequestById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApprovalSalesOrderRequest) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCode
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCode(String currencyCode) throws DataAccessException {

		return findApprovalSalesOrderRequestByCurrencyCode(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCode(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByCurrencyCode", startResult, maxRows, currencyCode);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCodeContaining(String currencyCode) throws DataAccessException {

		return findApprovalSalesOrderRequestByCurrencyCodeContaining(currencyCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCurrencyCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCurrencyCodeContaining(String currencyCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByCurrencyCodeContaining", startResult, maxRows, currencyCode);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCode
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCode(String approvalCode) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToAfter
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToAfter(java.util.Calendar validityTo) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityToAfter(validityTo, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityToAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityToAfter(java.util.Calendar validityTo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityToAfter", startResult, maxRows, validityTo);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPrimaryKey
	 *
	 */
	@Transactional
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id) throws DataAccessException {

		return findApprovalSalesOrderRequestByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByPrimaryKey
	 *
	 */

	@Transactional
	public ApprovalSalesOrderRequest findApprovalSalesOrderRequestByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findApprovalSalesOrderRequestByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ApprovalSalesOrderRequest) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDate
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDate(java.util.Calendar applicationDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApplicationDate(applicationDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicationDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicationDate(java.util.Calendar applicationDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApplicationDate", startResult, maxRows, applicationDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCreatedDate
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalTypeContaining
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalTypeContaining(String approvalType) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovalTypeContaining(approvalType, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalTypeContaining(String approvalType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovalTypeContaining", startResult, maxRows, approvalType);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalType
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalType(String approvalType) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovalType(approvalType, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovalType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovalType(String approvalType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovalType", startResult, maxRows, approvalType);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromAfter
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromAfter(java.util.Calendar validityFrom) throws DataAccessException {

		return findApprovalSalesOrderRequestByValidityFromAfter(validityFrom, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByValidityFromAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByValidityFromAfter(java.util.Calendar validityFrom, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByValidityFromAfter", startResult, maxRows, validityFrom);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllApprovalSalesOrderRequests
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests() throws DataAccessException {

		return findAllApprovalSalesOrderRequests(-1, -1);
	}

	/**
	 * JPQL Query - findAllApprovalSalesOrderRequests
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findAllApprovalSalesOrderRequests(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllApprovalSalesOrderRequests", startResult, maxRows);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicantContaining
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicantContaining(String applicant) throws DataAccessException {

		return findApprovalSalesOrderRequestByApplicantContaining(applicant, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApplicantContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApplicantContaining(String applicant, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApplicantContaining", startResult, maxRows, applicant);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateAfter
	 *
	 */
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateAfter(java.util.Calendar approvedDate) throws DataAccessException {

		return findApprovalSalesOrderRequestByApprovedDateAfter(approvedDate, -1, -1);
	}

	/**
	 * JPQL Query - findApprovalSalesOrderRequestByApprovedDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ApprovalSalesOrderRequest> findApprovalSalesOrderRequestByApprovedDateAfter(java.util.Calendar approvedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findApprovalSalesOrderRequestByApprovedDateAfter", startResult, maxRows, approvedDate);
		return new LinkedHashSet<ApprovalSalesOrderRequest>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ApprovalSalesOrderRequest entity) {
		return true;
	}

	@Override
	public int countApprovalFilter(Integer start, Integer end, String approvalType, String approvalCode,
			Calendar validityFrom, Calendar validityTo) {
		StringBuilder queryString = new StringBuilder("select count(1) from ApprovalSalesOrderRequest myApproval "
				+ "where approval_type = coalesce(cast(:approvalType as text),approval_type) "
				+ "and lower(approval_code) like lower(:approvalCode) ");
		if(validityFrom != null)
		{
			queryString.append("and validity_from >= cast(:validityFrom as date) ");
		}
		if(validityTo != null)
		{
			queryString.append("and validity_to <= cast(:validityTo as date) ");
		}
	
		Query query = createQuery(queryString.toString(), -1, -1);
		query.setParameter("approvalType", (approvalType != null && approvalType.isEmpty()) ? null : approvalType);
		query.setParameter("approvalCode", "%" + (approvalCode == null ? "" : approvalCode) + "%");
		if(validityFrom != null)
			query.setParameter("validityFrom", validityFrom);
		if(validityTo != null)
			query.setParameter("validityTo", validityTo);
		
		return ((Number)query.getSingleResult()).intValue();
	}

	@Override
	public int countApprovalAll() {
		Query query = createNamedQuery("countApprovalSalesOrderRequestAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<ApprovalSalesOrderRequest> findApprovalPaging(Integer start, Integer end, String approvalType,
			String approvalCode, Calendar validityFrom, Calendar validityTo, Integer orderColumn, String orderBy) {
		StringBuilder queryString = new StringBuilder("select myApprovalPurchaseRequest from ApprovalSalesOrderRequest myApprovalPurchaseRequest "
				+ "where approval_type = coalesce(cast(:approvalType as text),approval_type) "
				+ "and lower(approval_code) like lower(:approvalCode) ");
		if(validityFrom != null)
		{
			queryString.append("and approved_date >= cast(:approved_date_from as date) ");
		}
		if(validityTo != null)
		{
			queryString.append("and approved_date <= cast(:approved_date_to as date) ");
		}
		if(orderColumn == -1)
		{
			queryString.append("order by approved_date asc");
		}
		else {
			queryString.append("order by " + mapColumnIndex.get(orderColumn) + " " + orderBy);
		}
		
		Query query = createQuery(queryString.toString(), start, end);
		
		query.setParameter("approvalType", (approvalType != null && approvalType.isEmpty()) ? null : approvalType);
		query.setParameter("approvalCode", "%" + (approvalCode == null ? "" : approvalCode) + "%");
		if(validityFrom != null)
		{
			query.setParameter("approved_date_from", validityFrom);
		}
		if(validityTo != null)
		{
			query.setParameter("approved_date_to", validityTo);
		}
		Set<ApprovalSalesOrderRequest> resultSet = new LinkedHashSet<>(query.getResultList());
		for(ApprovalSalesOrderRequest approvalSalesOrderRequest: resultSet) {
			approvalSalesOrderRequest.setArInVoicesApplied(arInvoiceClassDetailsDAO.findArInvoiceNoByApprovalCode(approvalSalesOrderRequest.getApprovalCode()));
		}
		return resultSet;
	}

	@Override
	public int checkExists(ApprovalSalesOrderRequest approval) {
		Query query = createNamedQuery("checkApprovalSalesOrderRequestExists", -1, -1, approval.getApprovalCode(), approval.getIncludeGstOriginalAmount());
		if(!query.getResultList().isEmpty())
		{
			return ((Number)query.getSingleResult()).intValue();
		}
		else
		{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalSalesOrderRequest> findApprovalAvailable(String approvalCode, String issueDate)
			throws DataAccessException {
		String queryString = new StringBuilder()
                .append("SELECT myApproval FROM ApprovalSalesOrderRequest myApproval WHERE lower(myApproval.approvalCode) like lower(CONCAT('%', ?1, '%')) ")
                .append("and myApproval.validityFrom <= to_timestamp(?2, 'dd/MM/yyyy')")
                .append("and myApproval.validityTo  >= to_timestamp(?2, 'dd/MM/yyyy')")
                .toString();

		Query query = getEntityManager().createQuery(queryString)
				.setParameter(1, approvalCode)
                .setParameter(2, issueDate);
	
		return new ArrayList<ApprovalSalesOrderRequest>(query.getResultList());
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<ApprovalSalesOrderRequest> findApproval(String keyword) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT approvalSalesOrderRequest.* ");
        queryString.append("FROM approval_sales_order_request approvalSalesOrderRequest ");
        queryString.append("WHERE ");
        queryString.append("LOWER(approvalSalesOrderRequest.approval_code) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(approvalSalesOrderRequest.approval_type) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(approvalSalesOrderRequest.applicant) LIKE LOWER(CONCAT('%', ?1, '%')) OR ");
        queryString.append("LOWER(approvalSalesOrderRequest.purpose) LIKE LOWER(CONCAT('%', ?1, '%')) ");

        Query query = getEntityManager().createNativeQuery(queryString.toString(), ApprovalSalesOrderRequest.class);
        query.setParameter(1, keyword);

        return (List<ApprovalSalesOrderRequest>) query.getResultList();
    }

    @Override
    public boolean checkExistingApprovalCode(String approvalCode) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT count(1) ");
        queryString.append("FROM cashflow.approval_sales_order_request approvalSalesOrderRequest ");
        queryString.append("WHERE approvalSalesOrderRequest.approval_code = :approvalCode");

        Query query = getEntityManager().createNativeQuery(queryString.toString());
        query.setParameter("approvalCode", approvalCode);

        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
}
