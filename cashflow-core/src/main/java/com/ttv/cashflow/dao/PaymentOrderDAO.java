package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.dto.PaymentInvoice;
import com.ttv.cashflow.dto.PaymentOrderDTO;
import com.ttv.cashflow.dto.PaymentPayroll;



/**
 * DAO to manage PaymentOrder entities.
 * 
 */
public interface PaymentOrderDAO extends JpaDao<PaymentOrder> {

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNo(String paymentOrderNo, String paymentType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCode
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCode(String accountPayableCode) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCode
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCode(String accountPayableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByDescription
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByDescription
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByCreatedDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByCreatedDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCodeContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCodeContaining(String accountPayableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableCodeContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableCodeContaining(String accountPayableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPrimaryKey
	 *
	 */
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPrimaryKey
	 *
	 */
	public PaymentOrder findPaymentOrderByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableNameContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableNameContaining(String accountPayableName) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableNameContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableNameContaining(String accountPayableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllPaymentOrders
	 *
	 */
	public Set<PaymentOrder> findAllPaymentOrders() throws DataAccessException;

	/**
	 * JPQL Query - findAllPaymentOrders
	 *
	 */
	public Set<PaymentOrder> findAllPaymentOrders(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankAccount
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankAccount(String bankAccount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankAccount
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankAccount(String bankAccount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankNameContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankNameContaining(String bankName) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankNameContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankNameContaining(String bankName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentOrderNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContaining(String paymentOrderNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNoContaining(String paymentApprovalNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNoContaining(String paymentApprovalNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankRefNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankRefNoContaining(String bankRefNo) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankRefNoContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankRefNoContaining(String bankRefNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByStatusContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByStatusContaining(String status) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByStatusContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByStatusContaining(String status, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderById
	 *
	 */
	public PaymentOrder findPaymentOrderById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderById
	 *
	 */
	public PaymentOrder findPaymentOrderById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByStatus
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByStatus(String status_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByStatus
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByStatus(String status_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByValueDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByValueDate(java.util.Calendar valueDate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByValueDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByValueDate(Calendar valueDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableName
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableName(String accountPayableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByAccountPayableName
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByAccountPayableName(String accountPayableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNo(String paymentApprovalNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentApprovalNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentApprovalNo(String paymentApprovalNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByModifiedDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByModifiedDate
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankAccountContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankAccountContaining(String bankAccount_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankAccountContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankAccountContaining(String bankAccount_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankName
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankName(String bankName_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankName
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankName(String bankName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByDescriptionContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByDescriptionContaining
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankRefNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankRefNo(String bankRefNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByBankRefNo
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByBankRefNo(String bankRefNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentTotalAmount
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentTotalAmount(java.math.BigDecimal paymentTotalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findPaymentOrderByPaymentTotalAmount
	 *
	 */
	public Set<PaymentOrder> findPaymentOrderByPaymentTotalAmount(BigDecimal paymentTotalAmount, int startResult, int maxRows) throws DataAccessException;
	
	/**

	 * Search Payment Order - ap invoice
	 * @param startResult
	 * @param maxRow
	 * @param calFrom
	 * @param calTo
	 * @param bankRef
	 * @param invoiceNo
	 * @return
	 */
	public List<PaymentInvoice> searchPayment(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String invoiceNo, String status, Integer orderColumn, String orderBy);

	/**

	 * Search Payment Order
	 * @param startResult
	 * @param maxRow
	 * @param calFrom
	 * @param calTo
	 * @param bankRef
	 * @param paymentOrderNoFrom
	 * @param paymentOrderNoTo
	 * @return
	 */
	public List<PaymentInvoice> searchPaymentOrder(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status, Integer orderColumn, String orderBy);

	
	public List<PaymentInvoice> searchPaymentDetail(List<String> listPaymentNoApInvoice, List<String> listPaymentNoPayroll, List<String> listReimbursement, Integer orderColumn, String orderBy);

	/**
	 * Search payment order - payroll
	 * @param startResult
	 * @param maxRow
	 * @param calFrom
	 * @param calTo
	 * @param bankRef
	 * @param fromPayrollNo
	 * @param toPayrollNo
	 * @param status
	 * @param orderColumn
	 * @param orderBy
	 * @return
	 */
	public List<PaymentPayroll> searchPaymentPayroll(Integer startResult, Integer maxRow, String calFrom, String calTo, String bankRef, String fromPayrollNo, String toPayrollNo, String status, Integer orderColumn, String orderBy);

	public List<PaymentPayroll> searchPaymentDetailPayroll(List<String> paymentOrderNo, Integer orderColumn, String orderBy);
	
	/**
	 * Search Ap Invoice for create payment
	 */
    public Set<ApInvoice> searchApInvoice(Object... parameters);
    
    /**
     * Get payment no auto generator.
     */
    public String getPaymentNo();
    
    /**
     * Get last id
     */
    public BigInteger getLastId();

	public BigInteger searchPaymentCount(String calFrom, String calTo, String bankRef, String paymentOrderNoFrom, String paymentOrderNoTo, String status);
	
	public BigInteger searchPaymentPayrollCount(String calFrom, String calTo, String bankRef, String fromPayrollNo, String toPayrollNo, String status);
	
	/**
	 * find payment id
	 * @param paymentOrderNo
	 * @return
	 */
	public BigInteger findPaymentIdbyPaymentOrderNo(String paymentOrderNo);
	
	public PaymentOrder findPaymentbyPaymentOrderNo(String paymentOrderNo);
	
	public Set<PaymentOrder> findPaymentOrderByPaymentOrderNoContainingAndStatus(String paymentOrderNo, String status, int startResult, int maxRows) throws DataAccessException;
	
	public List<PaymentOrder> searchPaymentDetailByOrderNo(List<String> paymentOrderNo);
	
	 /**
     * Call function payment_no(t timestamp )
     */
    public String getPaymentOrderNo(Calendar cal);
    
    public boolean checkDuplicatePaymentOrder(String bankName, String bankAccount, BigDecimal convertedAmount, java.util.Calendar valueDate);
    
    /**
     * Get original amount is paid for AP Invoice.
     */
    public BigDecimal getOriginalAmountPayFor(BigInteger apInvoiceId);
    
    public List<PaymentOrderDTO> getPaymentOrderNoBasedOnBankRef(String bankName, String bankAccNo, Calendar transactionFrom, Calendar transactionTo, Integer transDateFilter);
    
    public PaymentOrder findPaymentOrderByPaymentOrderNoOnly(String paymentOrderNo) throws DataAccessException;
    
    /**
	 * Search Ap Invoice for create payment
	 */
    public Set<ApInvoice> searchApInvoiceDetails(Object... parameters);
    
    public BigDecimal getOriginalAmountPayForPR(BigInteger paryrollId);
    
    public BigDecimal getOriginalAmountPayForReim(BigInteger reimId);
    
    public PaymentOrder checkDuplicateByConvertedAmountAndValueDate(String bankName, String bankAccount, BigDecimal convertedAmount, java.util.Calendar valueDate) throws DataAccessException;
}