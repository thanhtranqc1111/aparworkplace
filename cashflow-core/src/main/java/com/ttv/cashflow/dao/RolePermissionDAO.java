
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.RolePermission;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage RolePermission entities.
 * 
 */
public interface RolePermissionDAO extends JpaDao<RolePermission> {

	/**
	 * JPQL Query - findAllRolePermissions
	 *
	 */
	public Set<RolePermission> findAllRolePermissions() throws DataAccessException;

	/**
	 * JPQL Query - findAllRolePermissions
	 *
	 */
	public Set<RolePermission> findAllRolePermissions(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByModifiedDate
	 *
	 */
	public Set<RolePermission> findRolePermissionByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByModifiedDate
	 *
	 */
	public Set<RolePermission> findRolePermissionByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionById
	 *
	 */
	public RolePermission findRolePermissionById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionById
	 *
	 */
	public RolePermission findRolePermissionById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByPrimaryKey
	 *
	 */
	public RolePermission findRolePermissionByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByPrimaryKey
	 *
	 */
	public RolePermission findRolePermissionByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByCreatedDate
	 *
	 */
	public Set<RolePermission> findRolePermissionByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findRolePermissionByCreatedDate
	 *
	 */
	public Set<RolePermission> findRolePermissionByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;
	public Set<RolePermission> findRolePermissionByRoleIdAndPermissionId(Integer roleId, Integer permissionId);
	public Set<RolePermission> findRolePermissionByRoleId(Integer roleId);
}