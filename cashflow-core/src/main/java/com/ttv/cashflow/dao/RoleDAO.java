
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.Role;
import com.ttv.cashflow.domain.UserAccount;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Role entities.
 * 
 */
public interface RoleDAO extends JpaDao<Role> {

	/**
	 * JPQL Query - findRoleByCreatedDate
	 *
	 */
	public Set<Role> findRoleByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByCreatedDate
	 *
	 */
	public Set<Role> findRoleByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByDescriptionContaining
	 *
	 */
	public Set<Role> findRoleByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByDescriptionContaining
	 *
	 */
	public Set<Role> findRoleByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByDescription
	 *
	 */
	public Set<Role> findRoleByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByDescription
	 *
	 */
	public Set<Role> findRoleByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByName
	 *
	 */
	public Set<Role> findRoleByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByName
	 *
	 */
	public Set<Role> findRoleByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleById
	 *
	 */
	public Role findRoleById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findRoleById
	 *
	 */
	public Role findRoleById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByPrimaryKey
	 *
	 */
	public Role findRoleByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByPrimaryKey
	 *
	 */
	public Role findRoleByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllRoles
	 *
	 */
	public Set<Role> findAllRoles() throws DataAccessException;

	/**
	 * JPQL Query - findAllRoles
	 *
	 */
	public Set<Role> findAllRoles(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByModifiedDate
	 *
	 */
	public Set<Role> findRoleByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByModifiedDate
	 *
	 */
	public Set<Role> findRoleByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByNameContaining
	 *
	 */
	public Set<Role> findRoleByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findRoleByNameContaining
	 *
	 */
	public Set<Role> findRoleByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;
	public Set<Role> findRolePaging(String keyword, int startResult, int maxRows) throws DataAccessException;
	public int countRolePaging(String keyword) throws DataAccessException;

}