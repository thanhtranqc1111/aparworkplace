package com.ttv.cashflow.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.DashboardDAO;
import com.ttv.cashflow.domain.ApInvoice;
import com.ttv.cashflow.domain.BankStatement;
import com.ttv.cashflow.dto.DashboardChartOneDTO;
import com.ttv.cashflow.util.CalendarUtil;

@Repository("DashboardDAO")
@Transactional
public class DashboardDAOImpl extends AbstractJpaDao<ApInvoice> implements DashboardDAO {
	private static final String STACK_NAME_APR = "AP Invoice";
	private static final String STACK_NAME_REI = "Reimbursement";
	private static final String STACK_NAME_PRO = "Payroll";
	private static final String STACK_NAME_ARR = "AR Invoice";
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] { BankStatement.class }));

	@Override
	public List<DashboardChartOneDTO> getListChartOne(Calendar fromMonth, Calendar toMonth) {
		String queryString = "SELECT extract(month from month) as MM, extract(year from month) as yyyy, sum(include_gst_converted_amount) as amount, text('AP Invoice') as stack, text('AP') as type "
				+ "FROM ap_invoice  "
				+ "WHERE month <= :1 and month >= :2 and include_gst_converted_amount > 0 "
				+ "GROUP by 1,2 " + "UNION "
				+ "SELECT extract(month from month) as MM, extract(year from month) as yyyy, sum(include_gst_total_converted_amount) as amount, text('Reimbursement') as stack, text('AP') as type "
				+ "FROM reimbursement  "
				+ "WHERE month <= :1 and month >= :2 and include_gst_total_converted_amount > 0 "
				+ "GROUP by 1,2 " + "UNION "
				+ "SELECT extract(month from month) as MM, extract(year from month) as yyyy, sum(total_net_payment_converted) as amount, text('Payroll') as stack, text('AP') as type "
				+ "FROM payroll  "
				+ "WHERE month <= :1 and month >= :2 and total_net_payment_converted > 0 "
				+ "GROUP by 1,2 " + "UNION "
				+ "SELECT extract(month from month) as MM, extract(year from month) as yyyy, sum(include_gst_converted_amount) as amount, text('AR Invoice') as stack, text('AR') as type "
				+ "FROM ar_invoice  "
				+ "WHERE month <= :1 and month >= :2 and include_gst_converted_amount > 0 "
				+ "GROUP by 1,2";
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("1", toMonth);
		query.setParameter("2", fromMonth);
		Set<Object[]> objMaster = new LinkedHashSet<Object[]>(query.getResultList());
		//map store each stack data
		Map<String, DashboardChartOneDTO> map = new HashMap<>();
		
		map.put(STACK_NAME_REI, new DashboardChartOneDTO(new ArrayList<>(), "Reimbursement", "AP", 1));
		map.put(STACK_NAME_PRO, new DashboardChartOneDTO(new ArrayList<>(), "Payroll", "AP",2 ));
		map.put(STACK_NAME_APR, new DashboardChartOneDTO(new ArrayList<>(), "AP Invoice", "AP", 3));
		map.put(STACK_NAME_ARR, new DashboardChartOneDTO(new ArrayList<>(), "AR Invoice", "AR", 4));
		getEntityManager().close();
		// loop month between fromMonth and toMonth
		while (!fromMonth.after(toMonth)) {
			//variable check data of a AP/AR type on this month is exists or not
			boolean aprHasData = false;
			boolean reiHasData = false;
			boolean proHasData = false;
			boolean arrHasData = false;
			for (Object[] objects : objMaster) {
				int month = ((Double) objects[0]).intValue();
				int year = ((Double)objects[1]).intValue();
				String monthString = (month > 9 ? month : "0" + month) + "/" + year;
				String stack = (String) objects[3];	
				//add data to stack
				if (CalendarUtil.toString(fromMonth, "MM/yyyy").equals(monthString)) {
					BigDecimal amount = (BigDecimal) objects[2];
					map.get(stack).getData().add(amount);
					switch (stack) {
					case STACK_NAME_APR:
						aprHasData = true;
						break;
					case STACK_NAME_REI:
						reiHasData = true;
						break;
					case STACK_NAME_PRO:
						proHasData = true;
						break;
					case STACK_NAME_ARR:
						arrHasData = true;
						break;
					default:
						break;
					}
				}
			}
			//when data not found
			if(!aprHasData) {
				map.get(STACK_NAME_APR).getData().add(BigDecimal.ZERO);
			}
			if(!reiHasData) {
				map.get(STACK_NAME_REI).getData().add(BigDecimal.ZERO);
			}
			if(!proHasData) {
				map.get(STACK_NAME_PRO).getData().add(BigDecimal.ZERO);
			}
			if(!arrHasData) {
				map.get(STACK_NAME_ARR).getData().add(BigDecimal.ZERO);
			}
			fromMonth.add(Calendar.MONTH, 1);
		}
		List<DashboardChartOneDTO> resultList = new ArrayList<DashboardChartOneDTO>(map.values());
		Collections.sort(resultList);
		return resultList;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	@Override
	public boolean canBeMerged(ApInvoice o) {
		return true;
	}

}
