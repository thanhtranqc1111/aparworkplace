
package com.ttv.cashflow.dao.impl;

import com.ttv.cashflow.dao.ArInvoiceTempDAO;
import com.ttv.cashflow.domain.ArInvoiceTemp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ArInvoiceTemp entities.
 * 
 */
@Repository("ArInvoiceTempDAO")

@Transactional
public class ArInvoiceTempDAOImpl extends AbstractJpaDao<ArInvoiceTemp> implements ArInvoiceTempDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ArInvoiceTemp.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ArInvoiceTempDAOImpl
	 *
	 */
	public ArInvoiceTempDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstTypeContaining(String gstType) throws DataAccessException {

		return findArInvoiceTempByGstTypeContaining(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstTypeContaining(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByGstTypeContaining", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByPrimaryKey
	 *
	 */
	@Transactional
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id) throws DataAccessException {

		return findArInvoiceTempByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByPrimaryKey
	 *
	 */

	@Transactional
	public ArInvoiceTemp findArInvoiceTempByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceTempByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableName
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableName(String accountReceivableName) throws DataAccessException {

		return findArInvoiceTempByAccountReceivableName(accountReceivableName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableName(String accountReceivableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountReceivableName", startResult, maxRows, accountReceivableName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountNameContaining(String accountName) throws DataAccessException {

		return findArInvoiceTempByAccountNameContaining(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountNameContaining(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountNameContaining", startResult, maxRows, accountName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByPayerName
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerName(String payerName) throws DataAccessException {

		return findArInvoiceTempByPayerName(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByPayerName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByPayerName", startResult, maxRows, payerName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByDescriptionContaining(String description) throws DataAccessException {

		return findArInvoiceTempByDescriptionContaining(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByDescriptionContaining", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountType
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountType(String accountType) throws DataAccessException {

		return findArInvoiceTempByAccountType(accountType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountType(String accountType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountType", startResult, maxRows, accountType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectNameContaining(String projectName) throws DataAccessException {

		return findArInvoiceTempByProjectNameContaining(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectNameContaining(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectNameContaining", startResult, maxRows, projectName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByFxRate
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByFxRate(java.math.BigDecimal fxRate) throws DataAccessException {

		return findArInvoiceTempByFxRate(fxRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByFxRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByFxRate(java.math.BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByFxRate", startResult, maxRows, fxRate);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonthAfter
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthAfter(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceTempByMonthAfter(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonthAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthAfter(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByMonthAfter", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateBefore
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceTempByInvoiceIssuedDateBefore(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByInvoiceIssuedDateBefore", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescription
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescription(String projectDescription) throws DataAccessException {

		return findArInvoiceTempByProjectDescription(projectDescription, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescription(String projectDescription, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectDescription", startResult, maxRows, projectDescription);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCodeContaining(String accountReceivableCode) throws DataAccessException {

		return findArInvoiceTempByAccountReceivableCodeContaining(accountReceivableCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCodeContaining(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountReceivableCodeContaining", startResult, maxRows, accountReceivableCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstRate
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstRate(java.math.BigDecimal gstRate) throws DataAccessException {

		return findArInvoiceTempByGstRate(gstRate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstRate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstRate(java.math.BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByGstRate", startResult, maxRows, gstRate);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCode(String approvalCode) throws DataAccessException {

		return findArInvoiceTempByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCode(String accountCode) throws DataAccessException {

		return findArInvoiceTempByAccountCode(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCode(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountCode", startResult, maxRows, accountCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByClaimType
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimType(String claimType) throws DataAccessException {

		return findArInvoiceTempByClaimType(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByClaimType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimType(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByClaimType", startResult, maxRows, claimType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCodeContaining(String accountCode) throws DataAccessException {

		return findArInvoiceTempByAccountCodeContaining(accountCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountCodeContaining", startResult, maxRows, accountCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByDescription
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByDescription(String description) throws DataAccessException {

		return findArInvoiceTempByDescription(description, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByDescription
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByDescription(String description, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByDescription", startResult, maxRows, description);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountTypeContaining(String accountType) throws DataAccessException {

		return findArInvoiceTempByAccountTypeContaining(accountType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountTypeContaining(String accountType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountTypeContaining", startResult, maxRows, accountType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonth
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonth(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceTempByMonth(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonth
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonth(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByMonth", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByPoNo
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNo(String poNo) throws DataAccessException {

		return findArInvoiceTempByPoNo(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByPoNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNo(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByPoNo", startResult, maxRows, poNo);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableNameContaining(String accountReceivableName) throws DataAccessException {

		return findArInvoiceTempByAccountReceivableNameContaining(accountReceivableName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableNameContaining(String accountReceivableName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountReceivableNameContaining", startResult, maxRows, accountReceivableName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllArInvoiceTemps
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findAllArInvoiceTemps() throws DataAccessException {

		return findAllArInvoiceTemps(-1, -1);
	}

	/**
	 * JPQL Query - findAllArInvoiceTemps
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findAllArInvoiceTemps(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllArInvoiceTemps", startResult, maxRows);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNoContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNoContaining(String invoiceNo) throws DataAccessException {

		return findArInvoiceTempByInvoiceNoContaining(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNoContaining(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByInvoiceNoContaining", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountName
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountName(String accountName) throws DataAccessException {

		return findArInvoiceTempByAccountName(accountName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountName", startResult, maxRows, accountName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCode(String accountReceivableCode) throws DataAccessException {

		return findArInvoiceTempByAccountReceivableCode(accountReceivableCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByAccountReceivableCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByAccountReceivableCode(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByAccountReceivableCode", startResult, maxRows, accountReceivableCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByPoNoContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNoContaining(String poNo) throws DataAccessException {

		return findArInvoiceTempByPoNoContaining(poNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByPoNoContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPoNoContaining(String poNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByPoNoContaining", startResult, maxRows, poNo);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findArInvoiceTempByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectCode
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCode(String projectCode) throws DataAccessException {

		return findArInvoiceTempByProjectCode(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCode(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectCode", startResult, maxRows, projectCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDate
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceTempByInvoiceIssuedDate(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByInvoiceIssuedDate", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateAfter
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate) throws DataAccessException {

		return findArInvoiceTempByInvoiceIssuedDateAfter(invoiceIssuedDate, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceIssuedDateAfter
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByInvoiceIssuedDateAfter", startResult, maxRows, invoiceIssuedDate);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrencyContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrencyContaining(String originalCurrency) throws DataAccessException {

		return findArInvoiceTempByOriginalCurrencyContaining(originalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrencyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrencyContaining(String originalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByOriginalCurrencyContaining", startResult, maxRows, originalCurrency);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNo
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNo(String invoiceNo) throws DataAccessException {

		return findArInvoiceTempByInvoiceNo(invoiceNo, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByInvoiceNo
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByInvoiceNo(String invoiceNo, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByInvoiceNo", startResult, maxRows, invoiceNo);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonthBefore
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthBefore(java.util.Calendar month) throws DataAccessException {

		return findArInvoiceTempByMonthBefore(month, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByMonthBefore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByMonthBefore(java.util.Calendar month, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByMonthBefore", startResult, maxRows, month);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrency
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrency(String originalCurrency) throws DataAccessException {

		return findArInvoiceTempByOriginalCurrency(originalCurrency, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByOriginalCurrency
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByOriginalCurrency(String originalCurrency, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByOriginalCurrency", startResult, maxRows, originalCurrency);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstType
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstType(String gstType) throws DataAccessException {

		return findArInvoiceTempByGstType(gstType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByGstType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByGstType(String gstType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByGstType", startResult, maxRows, gstType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByPayerNameContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerNameContaining(String payerName) throws DataAccessException {

		return findArInvoiceTempByPayerNameContaining(payerName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByPayerNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByPayerNameContaining(String payerName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByPayerNameContaining", startResult, maxRows, payerName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectName
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectName(String projectName) throws DataAccessException {

		return findArInvoiceTempByProjectName(projectName, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectName", startResult, maxRows, projectName);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescriptionContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescriptionContaining(String projectDescription) throws DataAccessException {

		return findArInvoiceTempByProjectDescriptionContaining(projectDescription, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectDescriptionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectDescriptionContaining(String projectDescription, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectDescriptionContaining", startResult, maxRows, projectDescription);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByExcludeGstOriginalAmount
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException {

		return findArInvoiceTempByExcludeGstOriginalAmount(excludeGstOriginalAmount, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByExcludeGstOriginalAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByExcludeGstOriginalAmount", startResult, maxRows, excludeGstOriginalAmount);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectCodeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCodeContaining(String projectCode) throws DataAccessException {

		return findArInvoiceTempByProjectCodeContaining(projectCode, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByProjectCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByProjectCodeContaining(String projectCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByProjectCodeContaining", startResult, maxRows, projectCode);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempByClaimTypeContaining
	 *
	 */
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimTypeContaining(String claimType) throws DataAccessException {

		return findArInvoiceTempByClaimTypeContaining(claimType, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempByClaimTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ArInvoiceTemp> findArInvoiceTempByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findArInvoiceTempByClaimTypeContaining", startResult, maxRows, claimType);
		return new LinkedHashSet<ArInvoiceTemp>(query.getResultList());
	}

	/**
	 * JPQL Query - findArInvoiceTempById
	 *
	 */
	@Transactional
	public ArInvoiceTemp findArInvoiceTempById(BigInteger id) throws DataAccessException {

		return findArInvoiceTempById(id, -1, -1);
	}

	/**
	 * JPQL Query - findArInvoiceTempById
	 *
	 */

	@Transactional
	public ArInvoiceTemp findArInvoiceTempById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findArInvoiceTempById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ArInvoiceTemp) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ArInvoiceTemp entity) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<String> findListIdArInvoiceTempByHavingGroup() throws DataAccessException {
        
        String queryString = new StringBuilder()
        .append("SELECT ")
        .append("string_agg(cast(id as text), ',') AS listId ")
        .append("FROM ar_invoice_temp ")
        .append("GROUP BY payer_name, invoice_no, month, claim_type ")
        .append("ORDER BY listId ")
        .toString();
        
        Query query = getEntityManager().createNativeQuery(queryString);

        return new ArrayList<String>(query.getResultList());
    }
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<ArInvoiceTemp> findArInvoiceTempListId(List<BigInteger> listId) throws DataAccessException {
        
        String queryString = new StringBuilder()
        .append("select myArInvoiceTemp from ArInvoiceTemp myArInvoiceTemp WHERE id IN (?1)")
        .toString();
        
        Query query = getEntityManager().createQuery(queryString)
                                        .setParameter(1, listId);
        
        return new ArrayList<ArInvoiceTemp>(query.getResultList());
    }
	
	@Transactional
    public void truncate() throws DataAccessException {
        getEntityManager().createNativeQuery("truncate table ar_invoice_temp").executeUpdate();
    }
}
