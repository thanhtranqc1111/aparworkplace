
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ReimbursementDetails;

/**
 * DAO to manage ReimbursementDetails entities.
 * 
 */
public interface ReimbursementDetailsDAO extends JpaDao<ReimbursementDetails> {

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCodeContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCodeContaining(String projectCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCodeContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCodeContaining(String projectCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByDescription
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByDescription(String description) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByDescription
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByDescription(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCode
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCode
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectName
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectName(String projectName) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectName
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectName(String projectName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstConvertedAmount
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByFxRate
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByCreatedDate
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByCreatedDate
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByModifiedDate
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByModifiedDate
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsById
	 *
	 */
	public ReimbursementDetails findReimbursementDetailsById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsById
	 *
	 */
	public ReimbursementDetails findReimbursementDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByDescriptionContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByDescriptionContaining(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByDescriptionContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByDescriptionContaining(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCode
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCode(String projectCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectCode
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectCode(String projectCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByApprovalCodeContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementDetailss
	 *
	 */
	public Set<ReimbursementDetails> findAllReimbursementDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementDetailss
	 *
	 */
	public Set<ReimbursementDetails> findAllReimbursementDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByPrimaryKey
	 *
	 */
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByPrimaryKey
	 *
	 */
	public ReimbursementDetails findReimbursementDetailsByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByIncludeGstOriginalAmount
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectNameContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectNameContaining(String projectName_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementDetailsByProjectNameContaining
	 *
	 */
	public Set<ReimbursementDetails> findReimbursementDetailsByProjectNameContaining(String projectName_1, int startResult, int maxRows) throws DataAccessException;
	
	public Set<String> findReimbursementNoByApprovalCode(String approvalCode);

}