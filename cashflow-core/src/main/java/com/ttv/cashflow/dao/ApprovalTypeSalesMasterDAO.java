
package com.ttv.cashflow.dao;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ApprovalTypeSalesMaster;

/**
 * DAO to manage ApprovalTypeSalesMaster entities.
 * 
 */
public interface ApprovalTypeSalesMasterDAO extends JpaDao<ApprovalTypeSalesMaster> {

	/**
	 * JPQL Query - findAllApprovalTypeSalesMasters
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllApprovalTypeSalesMasters
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findAllApprovalTypeSalesMasters(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByName
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByName
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByNameContaining
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByNameContaining
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCodeContaining
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByCodeContaining(String code) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCodeContaining
	 *
	 */
	public Set<ApprovalTypeSalesMaster> findApprovalTypeSalesMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCode
	 *
	 */
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByCode(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByCode
	 *
	 */
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByCode(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByPrimaryKey
	 *
	 */
	public ApprovalTypeSalesMaster findApprovalTypeSalesMasterByPrimaryKey(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findApprovalTypeSalesMasterByPrimaryKey
	 *
	 */
	public int countApprovalTypeAccountMasterFilter(Integer start, Integer end, String keyword);
	public int countApprovalTypeAccountMasterAll();
	public Set<ApprovalTypeSalesMaster> findApprovalTypeAccountMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}