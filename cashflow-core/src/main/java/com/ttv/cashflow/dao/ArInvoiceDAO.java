
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ArInvoice;
import com.ttv.cashflow.dto.ARInvoiceDetail;
import com.ttv.cashflow.dto.ArInvoiceDTO;
import com.ttv.cashflow.dto.ArInvoiceReceiptOrderDTO;


/**
 * DAO to manage ArInvoice entities.
 * 
 */
public interface ArInvoiceDAO extends JpaDao<ArInvoice> {

	/**
	 * JPQL Query - findArInvoiceById
	 *
	 */
	public ArInvoice findArInvoiceById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceById
	 *
	 */
	public ArInvoice findArInvoiceById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateBefore
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateBefore(java.util.Calendar invoiceIssuedDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateBefore
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateBefore(Calendar invoiceIssuedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPrimaryKey
	 *
	 */
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPrimaryKey
	 *
	 */
	public ArInvoice findArInvoiceByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByExcludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPayerName
	 *
	 */
	public Set<ArInvoice> findArInvoiceByPayerName(String payerName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPayerName
	 *
	 */
	public Set<ArInvoice> findArInvoiceByPayerName(String payerName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPayerNameContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByPayerNameContaining(String payerName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByPayerNameContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByPayerNameContaining(String payerName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByIncludeGstConvertedAmount(java.math.BigDecimal includeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByIncludeGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByDescriptionContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByDescriptionContaining(String description) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByDescriptionContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByDescriptionContaining(String description, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstRate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstRate(java.math.BigDecimal gstRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstRate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstRate(BigDecimal gstRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableName
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableName(String accountReceivableName) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableName
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableName(String accountReceivableName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstConvertedAmount(java.math.BigDecimal gstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstConvertedAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstConvertedAmount(BigDecimal gstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNo
	 *
	 */
	public Set<ArInvoice> findArInvoiceByArInvoiceNo(String arInvoiceNo) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNo
	 *
	 */
	public Set<ArInvoice> findArInvoiceByArInvoiceNo(String arInvoiceNo, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstOriginalAmount(java.math.BigDecimal gstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstOriginalAmount(BigDecimal gstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCodeContaining(String originalCurrencyCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByApprovalCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByApprovalCode(String approvalCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByApprovalCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByClaimTypeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByClaimTypeContaining(String claimType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByClaimTypeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByClaimTypeContaining(String claimType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByDescription
	 *
	 */
	public Set<ArInvoice> findArInvoiceByDescription(String description_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByDescription
	 *
	 */
	public Set<ArInvoice> findArInvoiceByDescription(String description_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByApprovalCodeContaining(String approvalCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByApprovalCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByApprovalCodeContaining(String approvalCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDate(java.util.Calendar invoiceIssuedDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDate(Calendar invoiceIssuedDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNoContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByArInvoiceNoContaining(String arInvoiceNo_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByArInvoiceNoContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByArInvoiceNoContaining(String arInvoiceNo_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableCode(String accountReceivableCode) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableCode(String accountReceivableCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonth
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonth(java.util.Calendar month) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonth
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonth(Calendar month, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByExcludeGstOriginalAmount(java.math.BigDecimal excludeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByExcludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByCreatedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByCreatedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableNameContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableNameContaining(String accountReceivableName_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableNameContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableNameContaining(String accountReceivableName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoices
	 *
	 */
	public Set<ArInvoice> findAllArInvoices() throws DataAccessException;

	/**
	 * JPQL Query - findAllArInvoices
	 *
	 */
	public Set<ArInvoice> findAllArInvoices(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableCodeContaining(String accountReceivableCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByAccountReceivableCodeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByAccountReceivableCodeContaining(String accountReceivableCode_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstType
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstType(String gstType) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstType
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstType(String gstType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByModifiedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByModifiedDate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonthAfter
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonthAfter(java.util.Calendar month_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonthAfter
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonthAfter(Calendar month_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByClaimType
	 *
	 */
	public Set<ArInvoice> findArInvoiceByClaimType(String claimType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByClaimType
	 *
	 */
	public Set<ArInvoice> findArInvoiceByClaimType(String claimType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateAfter
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateAfter(java.util.Calendar invoiceIssuedDate_2) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByInvoiceIssuedDateAfter
	 *
	 */
	public Set<ArInvoice> findArInvoiceByInvoiceIssuedDateAfter(Calendar invoiceIssuedDate_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByIncludeGstOriginalAmount(java.math.BigDecimal includeGstOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByIncludeGstOriginalAmount
	 *
	 */
	public Set<ArInvoice> findArInvoiceByIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstTypeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstTypeContaining(String gstType_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByGstTypeContaining
	 *
	 */
	public Set<ArInvoice> findArInvoiceByGstTypeContaining(String gstType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByFxRate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByFxRate(java.math.BigDecimal fxRate) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByFxRate
	 *
	 */
	public Set<ArInvoice> findArInvoiceByFxRate(BigDecimal fxRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonthBefore
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonthBefore(java.util.Calendar month_2) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByMonthBefore
	 *
	 */
	public Set<ArInvoice> findArInvoiceByMonthBefore(Calendar month_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCode(String originalCurrencyCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findArInvoiceByOriginalCurrencyCode
	 *
	 */
	public Set<ArInvoice> findArInvoiceByOriginalCurrencyCode(String originalCurrencyCode_1, int startResult, int maxRows) throws DataAccessException;
	
	/**
	 * JPQL Query - findArInvoiceToMatchRecepitOrder use in Receipt Order site
	 */
	public List<ArInvoiceReceiptOrderDTO> findArInvoiceToMatchRecepitOrder(Integer start, Integer end, String fromApNo, String toApNo, String fromMonth, String toMonth, 
														   String payerName, boolean isApproved, BigDecimal credit, Integer option, boolean isNotReceived) throws DataAccessException;
	
	/**
	 * JPQL Query - coundArInvoiceToMatchRecepitOrder use in Receipt Order site
	 */
	public int countArInvoiceToMatchRecepitOrder(String fromApNo, String toApNo, String fromMonth, String toMonth, 
			   									 String payerName, boolean isApproved, BigDecimal credit, Integer option) throws DataAccessException;
	
	public BigInteger searchCount(String fromMonth, String toMonth, String invoice, String payerName, String status, String fromArNo, String toArNo);

	public List<ArInvoiceDTO> searchArInvoice(Integer startResult, Integer maxRow, String fromMonth, String toMonth,
			String invoice, String payerName, String status, String fromArNo, String toArNo, Integer orderColumn, String orderBy);
	
	public List<ArInvoiceDTO> searchARInvoiceReceiptDetail(List<String> aprInvoiceNoLst, Integer orderColumn, String orderBy);
	
	/**
	 */
	public ArInvoice findArInvoiceByImportKey(String importKey);
	
	/**
	 */
	public String getArInvoiceNo(Calendar cal);
	
	public List<ARInvoiceDetail> getARInvoiceDetail(BigInteger arInvoiceId);
	
	/**
	 * get ar invoice no auto increase
	 * @return 
	 */
	public String getArInvoiceNo();
	
	public BigInteger getLastId();
	
	/**
	 */
	public BigInteger countDetail(String arInvoiceNo) throws DataAccessException;
	
	public boolean checkUniqueInvoiceNo(String invoiceNo, String id) throws DataAccessException;
}