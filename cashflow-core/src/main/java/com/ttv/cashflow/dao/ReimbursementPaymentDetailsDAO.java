
package com.ttv.cashflow.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;
import org.springframework.dao.DataAccessException;

import com.ttv.cashflow.domain.ReimbursementPaymentDetails;

/**
 * DAO to manage ReimbursementPaymentDetails entities.
 * 
 */
public interface ReimbursementPaymentDetailsDAO extends JpaDao<ReimbursementPaymentDetails> {

	/**
	 * JPQL Query - findReimbursementPaymentDetailsById
	 *
	 */
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsById(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsById
	 *
	 */
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByModifiedDate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByModifiedDate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAmount(java.math.BigDecimal varianceAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAmount(BigDecimal varianceAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPrimaryKey
	 *
	 */
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPrimaryKey
	 *
	 */
	public ReimbursementPaymentDetails findReimbursementPaymentDetailsByPrimaryKey(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentOriginalAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentOriginalAmount(java.math.BigDecimal paymentOriginalAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentOriginalAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentOriginalAmount(BigDecimal paymentOriginalAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCode
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCode(String varianceAccountCode) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCode
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCode(String varianceAccountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByCreatedDate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByCreatedDate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentRate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentRate(java.math.BigDecimal paymentRate) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentRate
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentRate(BigDecimal paymentRate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentConvertedAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentConvertedAmount(java.math.BigDecimal paymentConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByPaymentConvertedAmount
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByPaymentConvertedAmount(BigDecimal paymentConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementPaymentDetailss
	 *
	 */
	public Set<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllReimbursementPaymentDetailss
	 *
	 */
	public Set<ReimbursementPaymentDetails> findAllReimbursementPaymentDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findReimbursementPaymentDetailsByVarianceAccountCodeContaining
	 *
	 */
	public Set<ReimbursementPaymentDetails> findReimbursementPaymentDetailsByVarianceAccountCodeContaining(String varianceAccountCode_1, int startResult, int maxRows) throws DataAccessException;

	public BigDecimal getTotalReimAmountOriginalByReimId(BigInteger paymentId, BigInteger reimId);
	
	public Set<ReimbursementPaymentDetails> findReimbursemenPaymentDetailsByPaymentId(BigInteger paymentId) throws DataAccessException;
}