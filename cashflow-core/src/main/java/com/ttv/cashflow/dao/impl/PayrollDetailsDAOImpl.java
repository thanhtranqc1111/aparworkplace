
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.PayrollDetailsDAO;
import com.ttv.cashflow.domain.PayrollDetails;

/**
 * DAO to manage PayrollDetails entities.
 * 
 */
@Repository("PayrollDetailsDAO")

@Transactional
public class PayrollDetailsDAOImpl extends AbstractJpaDao<PayrollDetails> implements PayrollDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			PayrollDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new PayrollDetailsDAOImpl
	 *
	 */
	public PayrollDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther2(java.math.BigDecimal deductionOther2) throws DataAccessException {

		return findPayrollDetailsByDeductionOther2(deductionOther2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther2(java.math.BigDecimal deductionOther2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionOther2", startResult, maxRows, deductionOther2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCode
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByApprovalCode(String approvalCode) throws DataAccessException {

		return findPayrollDetailsByApprovalCode(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByApprovalCode(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByApprovalCode", startResult, maxRows, approvalCode);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByTotalDeduction
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTotalDeduction(java.math.BigDecimal totalDeduction) throws DataAccessException {

		return findPayrollDetailsByTotalDeduction(totalDeduction, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByTotalDeduction
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTotalDeduction(java.math.BigDecimal totalDeduction, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByTotalDeduction", startResult, maxRows, totalDeduction);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3) throws DataAccessException {

		return findPayrollDetailsByLaborAdditionalPayment3(laborAdditionalPayment3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment3(java.math.BigDecimal laborAdditionalPayment3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborAdditionalPayment3", startResult, maxRows, laborAdditionalPayment3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findPayrollDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public PayrollDetails findPayrollDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2) throws DataAccessException {

		return findPayrollDetailsByLaborSocialInsuranceEmployer2(laborSocialInsuranceEmployer2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer2(java.math.BigDecimal laborSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborSocialInsuranceEmployer2", startResult, maxRows, laborSocialInsuranceEmployer2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCodeContaining
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByApprovalCodeContaining(String approvalCode) throws DataAccessException {

		return findPayrollDetailsByApprovalCodeContaining(approvalCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByApprovalCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByApprovalCodeContaining(String approvalCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByApprovalCodeContaining", startResult, maxRows, approvalCode);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeContaining
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeContaining(String employeeCode) throws DataAccessException {

		return findPayrollDetailsByEmployeeCodeContaining(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeContaining(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByEmployeeCodeContaining", startResult, maxRows, employeeCode);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitle
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByRoleTitle(String roleTitle) throws DataAccessException {

		return findPayrollDetailsByRoleTitle(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitle
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByRoleTitle", startResult, maxRows, roleTitle);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByNetPayment
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByNetPaymentOriginal(java.math.BigDecimal netPayment) throws DataAccessException {

		return findPayrollDetailsByNetPaymentOriginal(netPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByNetPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByNetPaymentOriginal(java.math.BigDecimal netPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByNetPaymentOriginal", startResult, maxRows, netPayment);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByIsApproval
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByIsApproval(Boolean isApproval) throws DataAccessException {

		return findPayrollDetailsByIsApproval(isApproval, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByIsApproval
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByIsApproval(Boolean isApproval, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByIsApproval", startResult, maxRows, isApproval);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1) throws DataAccessException {

		return findPayrollDetailsByLaborAdditionalPayment1(laborAdditionalPayment1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment1(java.math.BigDecimal laborAdditionalPayment1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborAdditionalPayment1", startResult, maxRows, laborAdditionalPayment1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionWht
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionWht(java.math.BigDecimal deductionWht) throws DataAccessException {

		return findPayrollDetailsByDeductionWht(deductionWht, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionWht
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionWht(java.math.BigDecimal deductionWht, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionWht", startResult, maxRows, deductionWht);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2) throws DataAccessException {

		return findPayrollDetailsByLaborAdditionalPayment2(laborAdditionalPayment2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborAdditionalPayment2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborAdditionalPayment2(java.math.BigDecimal laborAdditionalPayment2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborAdditionalPayment2", startResult, maxRows, laborAdditionalPayment2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDivisionContaining
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDivisionContaining(String division) throws DataAccessException {

		return findPayrollDetailsByDivisionContaining(division, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDivisionContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDivisionContaining(String division, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDivisionContaining", startResult, maxRows, division);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1) throws DataAccessException {

		return findPayrollDetailsByLaborSocialInsuranceEmployer1(laborSocialInsuranceEmployer1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer1(java.math.BigDecimal laborSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborSocialInsuranceEmployer1", startResult, maxRows, laborSocialInsuranceEmployer1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployer2(deductionSocialInsuranceEmployer2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer2(java.math.BigDecimal deductionSocialInsuranceEmployer2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployer2", startResult, maxRows, deductionSocialInsuranceEmployer2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCode
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCode(String employeeCode) throws DataAccessException {

		return findPayrollDetailsByEmployeeCode(employeeCode, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCode
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCode(String employeeCode, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByEmployeeCode", startResult, maxRows, employeeCode);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllPayrollDetailss
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findAllPayrollDetailss() throws DataAccessException {

		return findAllPayrollDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllPayrollDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findAllPayrollDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllPayrollDetailss", startResult, maxRows);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findPayrollDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployee1(deductionSocialInsuranceEmployee1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee1(java.math.BigDecimal deductionSocialInsuranceEmployee1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployee1", startResult, maxRows, deductionSocialInsuranceEmployee1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployee3(deductionSocialInsuranceEmployee3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee3(java.math.BigDecimal deductionSocialInsuranceEmployee3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployee3", startResult, maxRows, deductionSocialInsuranceEmployee3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployer1(deductionSocialInsuranceEmployer1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer1(java.math.BigDecimal deductionSocialInsuranceEmployer1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployer1", startResult, maxRows, deductionSocialInsuranceEmployer1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByTotalLaborCost
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTotalLaborCost(java.math.BigDecimal totalLaborCost) throws DataAccessException {

		return findPayrollDetailsByTotalLaborCost(totalLaborCost, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByTotalLaborCost
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTotalLaborCost(java.math.BigDecimal totalLaborCost, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByTotalLaborCost", startResult, maxRows, totalLaborCost);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2) throws DataAccessException {

		return findPayrollDetailsByLaborOtherPayment2(laborOtherPayment2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment2(java.math.BigDecimal laborOtherPayment2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborOtherPayment2", startResult, maxRows, laborOtherPayment2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findPayrollDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1) throws DataAccessException {

		return findPayrollDetailsByLaborOtherPayment1(laborOtherPayment1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment1(java.math.BigDecimal laborOtherPayment1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborOtherPayment1", startResult, maxRows, laborOtherPayment1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee2
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployee2(deductionSocialInsuranceEmployee2, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployee2
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployee2(java.math.BigDecimal deductionSocialInsuranceEmployee2, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployee2", startResult, maxRows, deductionSocialInsuranceEmployee2);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborBaseSalaryPayment
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment) throws DataAccessException {

		return findPayrollDetailsByLaborBaseSalaryPayment(laborBaseSalaryPayment, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborBaseSalaryPayment
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborBaseSalaryPayment(java.math.BigDecimal laborBaseSalaryPayment, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborBaseSalaryPayment", startResult, maxRows, laborBaseSalaryPayment);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3) throws DataAccessException {

		return findPayrollDetailsByLaborSocialInsuranceEmployer3(laborSocialInsuranceEmployer3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborSocialInsuranceEmployer3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborSocialInsuranceEmployer3(java.math.BigDecimal laborSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborSocialInsuranceEmployer3", startResult, maxRows, laborSocialInsuranceEmployer3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDivision
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDivision(String division) throws DataAccessException {

		return findPayrollDetailsByDivision(division, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDivision
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDivision(String division, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDivision", startResult, maxRows, division);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByTeamContaining
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTeamContaining(String team) throws DataAccessException {

		return findPayrollDetailsByTeamContaining(team, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByTeamContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTeamContaining(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByTeamContaining", startResult, maxRows, team);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3) throws DataAccessException {

		return findPayrollDetailsByDeductionSocialInsuranceEmployer3(deductionSocialInsuranceEmployer3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionSocialInsuranceEmployer3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionSocialInsuranceEmployer3(java.math.BigDecimal deductionSocialInsuranceEmployer3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionSocialInsuranceEmployer3", startResult, maxRows, deductionSocialInsuranceEmployer3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther1
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther1(java.math.BigDecimal deductionOther1) throws DataAccessException {

		return findPayrollDetailsByDeductionOther1(deductionOther1, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther1
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther1(java.math.BigDecimal deductionOther1, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionOther1", startResult, maxRows, deductionOther1);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitleContaining
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByRoleTitleContaining(String roleTitle) throws DataAccessException {

		return findPayrollDetailsByRoleTitleContaining(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByRoleTitleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByRoleTitleContaining(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByRoleTitleContaining", startResult, maxRows, roleTitle);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther3(java.math.BigDecimal deductionOther3) throws DataAccessException {

		return findPayrollDetailsByDeductionOther3(deductionOther3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByDeductionOther3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByDeductionOther3(java.math.BigDecimal deductionOther3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByDeductionOther3", startResult, maxRows, deductionOther3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsByTeam
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTeam(String team) throws DataAccessException {

		return findPayrollDetailsByTeam(team, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByTeam
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByTeam(String team, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByTeam", startResult, maxRows, team);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findPayrollDetailsById
	 *
	 */
	@Transactional
	public PayrollDetails findPayrollDetailsById(BigInteger id) throws DataAccessException {

		return findPayrollDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsById
	 *
	 */

	@Transactional
	public PayrollDetails findPayrollDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findPayrollDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.PayrollDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment3
	 *
	 */
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3) throws DataAccessException {

		return findPayrollDetailsByLaborOtherPayment3(laborOtherPayment3, -1, -1);
	}

	/**
	 * JPQL Query - findPayrollDetailsByLaborOtherPayment3
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<PayrollDetails> findPayrollDetailsByLaborOtherPayment3(java.math.BigDecimal laborOtherPayment3, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByLaborOtherPayment3", startResult, maxRows, laborOtherPayment3);
		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(PayrollDetails entity) {
		return true;
	}

	@Override
	public Set<String> findPayrollNoByApprovalCode(String approvalCode) {
		String queryString = "SELECT DISTINCT(ac.payroll.payrollNo) " + 
			 	 "FROM PayrollDetails ac " +
			 	 "WHERE ac.approvalCode = ?1";

		Query query = getEntityManager().createQuery(queryString).setParameter(1, approvalCode);
		
		return new LinkedHashSet<String>(query.getResultList());
	}
	
	/**
	 * JPQL Query - findPayrollDetailsByEmployeeCodeAndMonth
	 *
	 */
	@Override
	public Set<PayrollDetails> findPayrollDetailsByEmployeeCodeAndMonth(Set<String> empCodes, Calendar fromMonth,
			Calendar toMonth) throws DataAccessException {
		Query query = createNamedQuery("findPayrollDetailsByEmployeeCodeAndMonth", null, null, empCodes, fromMonth, toMonth);

		return new LinkedHashSet<PayrollDetails>(query.getResultList());
	}
}
