
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ApInvoiceAccountDetails;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ApInvoiceAccountDetails entities.
 * 
 */
public interface ApInvoiceAccountDetailsDAO extends JpaDao<ApInvoiceAccountDetails> {

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByIncludeGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByIncludeGstAmount(java.math.BigDecimal includeGstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByIncludeGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByIncludeGstAmount(BigDecimal includeGstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCodeContaining(String accountCode) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCodeContaining
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCodeContaining(String accountCode, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByPrimaryKey
	 *
	 */
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByCreatedDate
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByCreatedDate(Calendar createdDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsById
	 *
	 */
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsById(BigInteger id_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsById
	 *
	 */
	public ApInvoiceAccountDetails findApInvoiceAccountDetailsById(BigInteger id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountName
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountName(String accountName) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountName
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountName(String accountName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstConvertedAmount(java.math.BigDecimal excludeGstConvertedAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstConvertedAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByGstAmount(java.math.BigDecimal gstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByGstAmount(BigDecimal gstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountNameContaining(String accountName_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountNameContaining
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountNameContaining(String accountName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstAmount(java.math.BigDecimal excludeGstAmount) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByExcludeGstAmount
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByExcludeGstAmount(BigDecimal excludeGstAmount, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByModifiedDate
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByModifiedDate(Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceAccountDetailss
	 *
	 */
	public Set<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss() throws DataAccessException;

	/**
	 * JPQL Query - findAllApInvoiceAccountDetailss
	 *
	 */
	public Set<ApInvoiceAccountDetails> findAllApInvoiceAccountDetailss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCode
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCode(String accountCode_1) throws DataAccessException;

	/**
	 * JPQL Query - findApInvoiceAccountDetailsByAccountCode
	 *
	 */
	public Set<ApInvoiceAccountDetails> findApInvoiceAccountDetailsByAccountCode(String accountCode_1, int startResult, int maxRows) throws DataAccessException;

    /**
	 * Get last id.
	 */
	public BigInteger getLastId();

}