
package com.ttv.cashflow.dao;

import com.ttv.cashflow.domain.ClaimTypeMaster;
import com.ttv.cashflow.domain.ClaimTypeMaster;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ClaimTypeMaster entities.
 * 
 */
public interface ClaimTypeMasterDAO extends JpaDao<ClaimTypeMaster> {

	/**
	 * JPQL Query - findClaimTypeMasterByCode
	 *
	 */
	public ClaimTypeMaster findClaimTypeMasterByCode(String code) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByCode
	 *
	 */
	public ClaimTypeMaster findClaimTypeMasterByCode(String code, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByPrimaryKey
	 *
	 */
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code_1) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByPrimaryKey
	 *
	 */
	public ClaimTypeMaster findClaimTypeMasterByPrimaryKey(String code_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByName
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByName
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByCodeContaining
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByCodeContaining(String code_2) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByCodeContaining
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByCodeContaining(String code_2, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByNameContaining
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findClaimTypeMasterByNameContaining
	 *
	 */
	public Set<ClaimTypeMaster> findClaimTypeMasterByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllClaimTypeMasters
	 *
	 */
	public Set<ClaimTypeMaster> findAllClaimTypeMasters() throws DataAccessException;

	/**
	 * JPQL Query - findAllClaimTypeMasters
	 *
	 */
	public Set<ClaimTypeMaster> findAllClaimTypeMasters(int startResult, int maxRows) throws DataAccessException;
	public int countClaimTypeMasterFilter(Integer start, Integer end, String keyword);
	public int countClaimTypeMasterAll();
	public Set<ClaimTypeMaster> findClaimTypeMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy, String keyword);
}