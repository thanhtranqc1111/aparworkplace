
package com.ttv.cashflow.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.CurrencyMasterDAO;
import com.ttv.cashflow.domain.CurrencyMaster;

/**
 * DAO to manage CurrencyMaster entities.
 * 
 */
@Repository("CurrencyMasterDAO")
@Transactional
public class CurrencyMasterDAOImpl extends AbstractJpaDao<CurrencyMaster> implements CurrencyMasterDAO {
	private static Map<Integer, String> mapColumnIndex = new HashMap<Integer, String>();
    static {
    	mapColumnIndex.put(1, "myCurrencyMaster.code");
    	mapColumnIndex.put(2, "myCurrencyMaster.name");
    }
	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			CurrencyMaster.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new CurrencyMasterDAOImpl
	 *
	 */
	public CurrencyMasterDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllCurrencyMasters
	 *
	 */
	@Transactional
	public Set<CurrencyMaster> findAllCurrencyMasters() throws DataAccessException {

		return findAllCurrencyMasters(-1, -1);
	}

	/**
	 * JPQL Query - findAllCurrencyMasters
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<CurrencyMaster> findAllCurrencyMasters(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllCurrencyMasters", startResult, maxRows);
		return new LinkedHashSet<CurrencyMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findCurrencyMasterByNameContaining
	 *
	 */
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByNameContaining(String name) throws DataAccessException {

		return findCurrencyMasterByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findCurrencyMasterByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCurrencyMasterByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<CurrencyMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findCurrencyMasterByName
	 *
	 */
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByName(String name) throws DataAccessException {

		return findCurrencyMasterByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findCurrencyMasterByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCurrencyMasterByName", startResult, maxRows, name);
		return new LinkedHashSet<CurrencyMaster>(query.getResultList());
	}

	/**
	 * JPQL Query - findCurrencyMasterByCode
	 *
	 */
	@Transactional
	public CurrencyMaster findCurrencyMasterByCode(String code) throws DataAccessException {

		return findCurrencyMasterByCode(code, -1, -1);
	}

	/**
	 * JPQL Query - findCurrencyMasterByCode
	 *
	 */

	@Transactional
	public CurrencyMaster findCurrencyMasterByCode(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCurrencyMasterByCode", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.CurrencyMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findCurrencyMasterByPrimaryKey
	 *
	 */
	@Transactional
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code) throws DataAccessException {

		return findCurrencyMasterByPrimaryKey(code, -1, -1);
	}

	/**
	 * JPQL Query - findCurrencyMasterByPrimaryKey
	 *
	 */

	@Transactional
	public CurrencyMaster findCurrencyMasterByPrimaryKey(String code, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCurrencyMasterByPrimaryKey", startResult, maxRows, code);
			return (com.ttv.cashflow.domain.CurrencyMaster) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findCurrencyMasterByCodeContaining
	 *
	 */
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByCodeContaining(String code) throws DataAccessException {

		return findCurrencyMasterByCodeContaining(code, -1, -1);
	}

	/**
	 * JPQL Query - findCurrencyMasterByCodeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<CurrencyMaster> findCurrencyMasterByCodeContaining(String code, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCurrencyMasterByCodeContaining", startResult, maxRows, code);
		return new LinkedHashSet<CurrencyMaster>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(CurrencyMaster entity) {
		return true;
	}

	public int countCurrencyMasterFilter(Integer start, Integer end, String keyword) {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countCurrencyMasterPaging", -1, -1, keyword);
		return ((Number)query.getSingleResult()).intValue();
	}

	public int countCurrencyMasterAll() {
		// TODO Auto-generated method stub
		Query query = createNamedQuery("countCurrencyMasterAll", -1, -1);
		return ((Number)query.getSingleResult()).intValue();
	}

	public Set<CurrencyMaster> findCurrencyMasterPaging(Integer start, Integer end, Integer orderColumn, String orderBy,
			String keyword) {
		// TODO Auto-generated method stub
		Query query = createQuery("select myCurrencyMaster from CurrencyMaster myCurrencyMaster where lower(myCurrencyMaster.code) like lower(CONCAT('%',?1, '%')) or "
				   + "lower(myCurrencyMaster.name) like lower(CONCAT('%',?1, '%')) " + " order by " + mapColumnIndex.get(orderColumn) + " " + orderBy, start, end);
		query.setParameter(1, keyword);
		return new LinkedHashSet<CurrencyMaster>(query.getResultList());
	}
}
