
package com.ttv.cashflow.dao.impl;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttv.cashflow.dao.ProjectPayrollDetailsDAO;
import com.ttv.cashflow.domain.ProjectPayrollDetails;

/**
 * DAO to manage ProjectPayrollDetails entities.
 * 
 */
@Repository("ProjectPayrollDetailsDAO")

@Transactional
public class ProjectPayrollDetailsDAOImpl extends AbstractJpaDao<ProjectPayrollDetails>
		implements ProjectPayrollDetailsDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ProjectPayrollDetails.class }));

	/**
	 * EntityManager injected by Spring for persistence unit CashFlowDriver
	 *
	 */
	@PersistenceContext(unitName = "CashFlowDriver")
	private EntityManager entityManager;

	/**
	 * Instantiates a new ProjectPayrollDetailsDAOImpl
	 *
	 */
	public ProjectPayrollDetailsDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectAllocationAmount
	 *
	 */
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByProjectAllocationAmount(java.math.BigDecimal projectAllocationAmount) throws DataAccessException {

		return findProjectPayrollDetailsByProjectAllocationAmount(projectAllocationAmount, -1, -1);
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectAllocationAmount
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByProjectAllocationAmount(java.math.BigDecimal projectAllocationAmount, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectPayrollDetailsByProjectAllocationAmount", startResult, maxRows, projectAllocationAmount);
		return new LinkedHashSet<ProjectPayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllProjectPayrollDetailss
	 *
	 */
	@Transactional
	public Set<ProjectPayrollDetails> findAllProjectPayrollDetailss() throws DataAccessException {

		return findAllProjectPayrollDetailss(-1, -1);
	}

	/**
	 * JPQL Query - findAllProjectPayrollDetailss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectPayrollDetails> findAllProjectPayrollDetailss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllProjectPayrollDetailss", startResult, maxRows);
		return new LinkedHashSet<ProjectPayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByPrimaryKey
	 *
	 */
	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id) throws DataAccessException {

		return findProjectPayrollDetailsByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByPrimaryKey
	 *
	 */

	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsByPrimaryKey(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findProjectPayrollDetailsByPrimaryKey", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ProjectPayrollDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByCreatedDate
	 *
	 */
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByCreatedDate(java.util.Calendar createdDate) throws DataAccessException {

		return findProjectPayrollDetailsByCreatedDate(createdDate, -1, -1);
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByCreatedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByCreatedDate(java.util.Calendar createdDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectPayrollDetailsByCreatedDate", startResult, maxRows, createdDate);
		return new LinkedHashSet<ProjectPayrollDetails>(query.getResultList());
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsById
	 *
	 */
	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsById(BigInteger id) throws DataAccessException {

		return findProjectPayrollDetailsById(id, -1, -1);
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsById
	 *
	 */

	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsById(BigInteger id, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findProjectPayrollDetailsById", startResult, maxRows, id);
			return (com.ttv.cashflow.domain.ProjectPayrollDetails) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByModifiedDate
	 *
	 */
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate) throws DataAccessException {

		return findProjectPayrollDetailsByModifiedDate(modifiedDate, -1, -1);
	}

	/**
	 * JPQL Query - findProjectPayrollDetailsByModifiedDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ProjectPayrollDetails> findProjectPayrollDetailsByModifiedDate(java.util.Calendar modifiedDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findProjectPayrollDetailsByModifiedDate", startResult, maxRows, modifiedDate);
		return new LinkedHashSet<ProjectPayrollDetails>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ProjectPayrollDetails entity) {
		return true;
	}
	
	/**
	 * JPQL Query - findProjectPayrollDetailsByProjectCodeAndPayrollDetailId
	 *
	 */
	@Transactional
	public ProjectPayrollDetails findProjectPayrollDetailsByProjectCodeAndPayrollDetailId(String projectCode, BigInteger payrollDetailId) throws DataAccessException {
		Query query = createNamedQuery("findProjectPayrollDetailsByProjectCodeAndPayrollDetailId", null, null, projectCode, payrollDetailId);
		try {
			return (ProjectPayrollDetails) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
