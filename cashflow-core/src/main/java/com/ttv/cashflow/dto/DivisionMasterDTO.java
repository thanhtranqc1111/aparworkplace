package com.ttv.cashflow.dto;

import java.math.BigInteger;

public class DivisionMasterDTO {

    private BigInteger id;
    private String name;
    private String code;

    public DivisionMasterDTO() {
    }

    public DivisionMasterDTO(BigInteger id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
