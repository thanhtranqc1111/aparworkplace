package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

/**
 * @author thoai.nh
 * created date Mar 20, 2018
 */
public class PayrollPaymentDTO {
	private BigInteger id;
	private Calendar month;
	private String payrollNo;
	private String claimType;
	private BigDecimal totalNetPayment;
	private String paymentOrderNo;
	private Calendar valueDate;
	private String bankName;
	private String bankAccount;
	private BigDecimal includeGstConvertedAmount;
	private String bankRefNo;
	private String paymentOrderStatus;
	private String payrollStatus;
	private String paymentOrderStatusCode;
	private String payrollStatusCode;
	private String claimTypeVal;
	List<BigInteger> bankStatementIds;
	// tien.tm
	private String invoiceNo;
	
	public PayrollPaymentDTO(BigInteger id, Calendar month, String payrollNo, String claimType,
			BigDecimal totalNetPayment, String payrollStatus, String paymentOrderNo, Calendar valueDate,
			String bankName, String bankAccount, BigDecimal includeGstConvertedAmount, String bankRefNo,
			String paymentOrderStatus, String invoiceNo) {
		super();
		this.id = id;
		this.month = month;
		this.payrollNo = payrollNo;
		this.claimType = claimType;
		this.totalNetPayment = totalNetPayment;
		this.payrollStatus = payrollStatus;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.bankRefNo = bankRefNo;
		this.paymentOrderStatus = paymentOrderStatus;
		this.invoiceNo = invoiceNo;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getPayrollNo() {
		return payrollNo;
	}
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public BigDecimal getTotalNetPayment() {
		return totalNetPayment;
	}
	public void setTotalNetPayment(BigDecimal totalNetPayment) {
		this.totalNetPayment = totalNetPayment;
	}
	public String getPayrollStatus() {
		return payrollStatus;
	}
	public void setPayrollStatus(String payrollStatus) {
		this.payrollStatus = payrollStatus;
	}
	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}
	public String getPaymentOrderStatus() {
		return paymentOrderStatus;
	}
	public void setPaymentOrderStatus(String paymentOrderStatus) {
		this.paymentOrderStatus = paymentOrderStatus;
	}
	public String getPaymentOrderStatusCode() {
		return paymentOrderStatusCode;
	}
	public void setPaymentOrderStatusCode(String paymentOrderStatusCode) {
		this.paymentOrderStatusCode = paymentOrderStatusCode;
	}
	public String getPayrollStatusCode() {
		return payrollStatusCode;
	}
	public void setPayrollStatusCode(String payrollStatusCode) {
		this.payrollStatusCode = payrollStatusCode;
	}
	public String getClaimTypeVal() {
		return claimTypeVal;
	}
	public void setClaimTypeVal(String claimTypeVal) {
		this.claimTypeVal = claimTypeVal;
	}
	public List<BigInteger> getBankStatementIds() {
		return bankStatementIds;
	}
	public void setBankStatementIds(List<BigInteger> bankStatementIds) {
		this.bankStatementIds = bankStatementIds;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
}	
