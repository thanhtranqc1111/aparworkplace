package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author thoai.nh
 * created date Apr 6, 2018
 */
public class ReimbursementDetailsDTO {
	private BigInteger id;
	private String accountCode;
	private String accountName;
	private String projectCode;
	private String projectName;
	private String approvalCode;
	private String description;
	private BigDecimal includeGstOriginalAmount;
	private BigDecimal includeGstConvertedAmount;
	private Boolean isApproval;
	private String apprStatusCode;
    private String apprStatusVal;
    private BigDecimal approvalIncludeGstOriginalAmount;
    private BigDecimal gstOriginalAmount;
	private BigDecimal gstConvertedAmount;
	private BigDecimal excludeGstOriginalAmount;
	private BigDecimal excludeGstConvertedAmount;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}
	public String getApprStatusCode() {
		return apprStatusCode;
	}
	public void setApprStatusCode(String apprStatusCode) {
		this.apprStatusCode = apprStatusCode;
	}
	public String getApprStatusVal() {
		return apprStatusVal;
	}
	public void setApprStatusVal(String apprStatusVal) {
		this.apprStatusVal = apprStatusVal;
	}
	public BigDecimal getApprovalIncludeGstOriginalAmount() {
		return approvalIncludeGstOriginalAmount;
	}
	public void setApprovalIncludeGstOriginalAmount(BigDecimal approvalIncludeGstOriginalAmount) {
		this.approvalIncludeGstOriginalAmount = approvalIncludeGstOriginalAmount;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}
	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}
	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}
	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}
}
