package com.ttv.cashflow.dto;

import java.util.Calendar;

import com.ttv.cashflow.domain.EmployeeMaster;

/**
 * @author thoai.nh
 * created date May 4, 2018
 */
public class ProjectAllocationCollectorDTO{
	EmployeeMaster employeeMaster;
	Calendar month;
	public ProjectAllocationCollectorDTO(EmployeeMaster employeeMaster, Calendar month) {
		super();
		this.employeeMaster = employeeMaster;
		this.month = month;
	}
	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}
	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employeeMaster == null) ? 0 : employeeMaster.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectAllocationCollectorDTO other = (ProjectAllocationCollectorDTO) obj;
		if (employeeMaster == null) {
			if (other.employeeMaster != null)
				return false;
		} else if (!employeeMaster.equals(other.employeeMaster))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		return true;
	}
}