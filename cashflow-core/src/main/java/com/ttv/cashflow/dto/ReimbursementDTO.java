package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;

/**
 * @author thoai.nh
 * created date Apr 6, 2018
 */
public class ReimbursementDTO {
	private BigInteger id;
	private String reimbursementNo;
	private String invoiceNo;
	private Calendar bookingDate;
	private String claimType;
	private String accountPayableCode;
	private String accountPayableName;
	private Calendar scheduledPaymentDate;
	private String status;
	private Calendar createdDate;
	private String description;
	private Set<ReimbursementDetailsDTO> reimbursementDetailses;
	private String statusVal;
	private BigDecimal fxRate;
	private BigDecimal includeGstTotalOriginalAmount;
	private BigDecimal includeGstTotalConvertedAmount;
	private String originalCurrencyCode;
	private String employeeName;
	private String employeeCode;
	private Calendar month;
	private String importKey;
	private BigDecimal gstRate;
	private String gstType;
	private BigDecimal excludeGstTotalOriginalAmount;
	private BigDecimal excludeGstTotalConvertedAmount;
	private BigDecimal gstConvertedAmount;
	private BigDecimal gstOriginalAmount;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getReimbursementNo() {
		return reimbursementNo;
	}
	public void setReimbursementNo(String reimbursementNo) {
		this.reimbursementNo = reimbursementNo;
	}
	public Calendar getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getAccountPayableCode() {
		return accountPayableCode;
	}
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}
	public String getAccountPayableName() {
		return accountPayableName;
	}
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}
	public Calendar getScheduledPaymentDate() {
		return scheduledPaymentDate;
	}
	public void setScheduledPaymentDate(Calendar scheduledPaymentDate) {
		this.scheduledPaymentDate = scheduledPaymentDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Calendar getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<ReimbursementDetailsDTO> getReimbursementDetailses() {
		return reimbursementDetailses;
	}
	public void setReimbursementDetailses(Set<ReimbursementDetailsDTO> reimbursementDetailses) {
		this.reimbursementDetailses = reimbursementDetailses;
	}
	public String getStatusVal() {
		return statusVal;
	}
	public void setStatusVal(String statusVal) {
		this.statusVal = statusVal;
	}
	public BigDecimal getFxRate() {
		return fxRate;
	}
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}
	public BigDecimal getIncludeGstTotalOriginalAmount() {
		return includeGstTotalOriginalAmount;
	}
	public void setIncludeGstTotalOriginalAmount(BigDecimal includeGstTotalAmountOriginal) {
		this.includeGstTotalOriginalAmount = includeGstTotalAmountOriginal;
	}
	public BigDecimal getIncludeGstTotalConvertedAmount() {
		return includeGstTotalConvertedAmount;
	}
	public void setIncludeGstTotalConvertedAmount(BigDecimal includeGstTotalAmountConverted) {
		this.includeGstTotalConvertedAmount = includeGstTotalAmountConverted;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String toJSON() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getImportKey() {
		return importKey;
	}
	public void setImportKey(String importKey) {
		this.importKey = importKey;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public BigDecimal getGstRate() {
		return gstRate;
	}
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}
	public String getGstType() {
		return gstType;
	}
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}
	public BigDecimal getExcludeGstTotalOriginalAmount() {
		return excludeGstTotalOriginalAmount;
	}
	public void setExcludeGstTotalOriginalAmount(BigDecimal excludeGstTotalOriginalAmount) {
		this.excludeGstTotalOriginalAmount = excludeGstTotalOriginalAmount;
	}
	public BigDecimal getExcludeGstTotalConvertedAmount() {
		return excludeGstTotalConvertedAmount;
	}
	public void setExcludeGstTotalConvertedAmount(BigDecimal excludeGstTotalConvertedAmount) {
		this.excludeGstTotalConvertedAmount = excludeGstTotalConvertedAmount;
	}
	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}
	public List<ReimbursementDetailsDTO> getReimbursementDetailsesList() {
		List<ReimbursementDetailsDTO> list = reimbursementDetailses.stream().collect(Collectors.toList());
		list.sort((ReimbursementDetailsDTO o1, ReimbursementDetailsDTO o2) -> o1.getId().compareTo(o2.getId()));
		return list;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountPayableCode == null) ? 0 : accountPayableCode.hashCode());
		result = prime * result + ((accountPayableName == null) ? 0 : accountPayableName.hashCode());
		result = prime * result + ((bookingDate == null) ? 0 : bookingDate.hashCode());
		result = prime * result + ((claimType == null) ? 0 : claimType.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((employeeCode == null) ? 0 : employeeCode.hashCode());
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result
				+ ((excludeGstTotalConvertedAmount == null) ? 0 : excludeGstTotalConvertedAmount.hashCode());
		result = prime * result
				+ ((excludeGstTotalOriginalAmount == null) ? 0 : excludeGstTotalOriginalAmount.hashCode());
		result = prime * result + ((fxRate == null) ? 0 : fxRate.hashCode());
		result = prime * result + ((gstConvertedAmount == null) ? 0 : gstConvertedAmount.hashCode());
		result = prime * result + ((gstOriginalAmount == null) ? 0 : gstOriginalAmount.hashCode());
		result = prime * result + ((gstRate == null) ? 0 : gstRate.hashCode());
		result = prime * result + ((gstType == null) ? 0 : gstType.hashCode());
		result = prime * result
				+ ((includeGstTotalConvertedAmount == null) ? 0 : includeGstTotalConvertedAmount.hashCode());
		result = prime * result
				+ ((includeGstTotalOriginalAmount == null) ? 0 : includeGstTotalOriginalAmount.hashCode());
		result = prime * result + ((invoiceNo == null) ? 0 : invoiceNo.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((originalCurrencyCode == null) ? 0 : originalCurrencyCode.hashCode());
		result = prime * result + ((scheduledPaymentDate == null) ? 0 : scheduledPaymentDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReimbursementDTO other = (ReimbursementDTO) obj;
		if (accountPayableCode == null) {
			if (other.accountPayableCode != null)
				return false;
		} else if (!accountPayableCode.equals(other.accountPayableCode))
			return false;
		if (accountPayableName == null) {
			if (other.accountPayableName != null)
				return false;
		} else if (!accountPayableName.equals(other.accountPayableName))
			return false;
		if (bookingDate == null) {
			if (other.bookingDate != null)
				return false;
		} else if (!bookingDate.equals(other.bookingDate))
			return false;
		if (claimType == null) {
			if (other.claimType != null)
				return false;
		} else if (!claimType.equals(other.claimType))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (employeeCode == null) {
			if (other.employeeCode != null)
				return false;
		} else if (!employeeCode.equals(other.employeeCode))
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (excludeGstTotalConvertedAmount == null) {
			if (other.excludeGstTotalConvertedAmount != null)
				return false;
		} else if (!excludeGstTotalConvertedAmount.equals(other.excludeGstTotalConvertedAmount))
			return false;
		if (excludeGstTotalOriginalAmount == null) {
			if (other.excludeGstTotalOriginalAmount != null)
				return false;
		} else if (!excludeGstTotalOriginalAmount.equals(other.excludeGstTotalOriginalAmount))
			return false;
		if (fxRate == null) {
			if (other.fxRate != null)
				return false;
		} else if (!fxRate.equals(other.fxRate))
			return false;
		if (gstConvertedAmount == null) {
			if (other.gstConvertedAmount != null)
				return false;
		} else if (!gstConvertedAmount.equals(other.gstConvertedAmount))
			return false;
		if (gstOriginalAmount == null) {
			if (other.gstOriginalAmount != null)
				return false;
		} else if (!gstOriginalAmount.equals(other.gstOriginalAmount))
			return false;
		if (gstRate == null) {
			if (other.gstRate != null)
				return false;
		} else if (!gstRate.equals(other.gstRate))
			return false;
		if (gstType == null) {
			if (other.gstType != null)
				return false;
		} else if (!gstType.equals(other.gstType))
			return false;
		if (includeGstTotalConvertedAmount == null) {
			if (other.includeGstTotalConvertedAmount != null)
				return false;
		} else if (!includeGstTotalConvertedAmount.equals(other.includeGstTotalConvertedAmount))
			return false;
		if (includeGstTotalOriginalAmount == null) {
			if (other.includeGstTotalOriginalAmount != null)
				return false;
		} else if (!includeGstTotalOriginalAmount.equals(other.includeGstTotalOriginalAmount))
			return false;
		if (invoiceNo == null) {
			if (other.invoiceNo != null)
				return false;
		} else if (!invoiceNo.equals(other.invoiceNo))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (originalCurrencyCode == null) {
			if (other.originalCurrencyCode != null)
				return false;
		} else if (!originalCurrencyCode.equals(other.originalCurrencyCode))
			return false;
		if (scheduledPaymentDate == null) {
			if (other.scheduledPaymentDate != null)
				return false;
		} else if (!scheduledPaymentDate.equals(other.scheduledPaymentDate))
			return false;
		return true;
	}
}
