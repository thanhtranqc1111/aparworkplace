package com.ttv.cashflow.dto;

import java.math.BigInteger;
import java.util.Calendar;

public class EmployeeMasterDTO {

    private BigInteger id;
    private String code;
    private String name;
    private String roleTitle;
    private String team;
    private String divisionName;
    private String divisionCode;
    private Calendar joinDate;
    private Calendar resignDate;

    public EmployeeMasterDTO() {
    }

    public EmployeeMasterDTO(BigInteger id, String code, String name, String roleTitle, String team, String divisionName, String divisionCode, Calendar joinDate, Calendar resignDate) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.roleTitle = roleTitle;
        this.team = team;
        this.divisionName = divisionName;
        this.divisionCode = divisionCode;
        this.joinDate = joinDate;
        this.resignDate = resignDate;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

	public Calendar getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Calendar joinDate) {
		this.joinDate = joinDate;
	}

	public Calendar getResignDate() {
		return resignDate;
	}

	public void setResignDate(Calendar resignDate) {
		this.resignDate = resignDate;
	}
}
