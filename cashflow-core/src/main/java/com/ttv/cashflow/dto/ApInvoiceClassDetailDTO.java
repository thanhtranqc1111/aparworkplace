package com.ttv.cashflow.dto;

import java.math.BigDecimal;

/**
 * @author thoai.nh
 * summary info of ApInvoiceClassDetails group by amount
 * created date Oct 11, 2017
 */
public class ApInvoiceClassDetailDTO {
	private String projectName;
	private String description;
	private BigDecimal amount;
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}
