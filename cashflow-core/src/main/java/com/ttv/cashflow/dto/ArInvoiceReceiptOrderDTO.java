
package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * @author thoai.nh
 * created date Dec 6, 201
 * ArInvoice DTO used in Receipt Order Site
 */
public class ArInvoiceReceiptOrderDTO {
	private BigInteger id;
	private String payerName;
	private String arInvoiceNo;
	private String originalCurrencyCode;
	private BigDecimal fxRate;
	private BigDecimal gstRate;
	private Calendar invoiceIssuedDate;
	private Calendar month;
	private String claimType;
	private String gstType;
	private String approvalCode;
	private String accountReceivableCode;
	private String accountReceivableName;
	private String description;
	private BigDecimal excludeGstOriginalAmount;
	private BigDecimal excludeGstConvertedAmount;
	private BigDecimal gstOriginalAmount;
	private BigDecimal gstConvertedAmount;
	private BigDecimal includeGstConvertedAmount;
	private BigDecimal includeGstOriginalAmount;
	private String status;
	//ReceiptOrder info
	private BigDecimal settedIncludeGstOriginalAmount;
	private BigDecimal receiptRate;
	private BigDecimal settedIncludeGstConvertedAmount;
	private BigDecimal varianceAmount;
	private String varianceAccountName;
	private BigDecimal remainIncludeGstOriginalAmount;
	private boolean isApproved;
	private String invoiceNo;
	public ArInvoiceReceiptOrderDTO() {}
	public ArInvoiceReceiptOrderDTO(BigInteger id, String payerName, String arInvoiceNo, String originalCurrencyCode,
			BigDecimal fxRate, BigDecimal gstRate, Calendar invoiceIssuedDate, Calendar month, String claimType,
			String gstType, String approvalCode, String accountReceivableCode, String accountReceivableName,
			String description, BigDecimal excludeGstOriginalAmount, BigDecimal excludeGstConvertedAmount,
			BigDecimal gstOriginalAmount, BigDecimal gstConvertedAmount, BigDecimal includeGstConvertedAmount, BigDecimal includeGstOriginalAmount, BigDecimal remainIncludeGstOriginalAmount, boolean isApproved, String invoiceNo) {
		super();
		this.id = id;
		this.payerName = payerName;
		this.arInvoiceNo = arInvoiceNo;
		this.originalCurrencyCode = originalCurrencyCode;
		this.fxRate = fxRate;
		this.gstRate = gstRate;
		this.invoiceIssuedDate = invoiceIssuedDate;
		this.month = month;
		this.claimType = claimType;
		this.gstType = gstType;
		this.approvalCode = approvalCode;
		this.accountReceivableCode = accountReceivableCode;
		this.accountReceivableName = accountReceivableName;
		this.description = description;
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
		this.gstOriginalAmount = gstOriginalAmount;
		this.gstConvertedAmount = gstConvertedAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.includeGstOriginalAmount =  includeGstOriginalAmount;
		this.remainIncludeGstOriginalAmount =  remainIncludeGstOriginalAmount;
		this.isApproved = isApproved; 
		this.invoiceNo = invoiceNo;
	}
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getArInvoiceNo() {
		return arInvoiceNo;
	}
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public BigDecimal getFxRate() {
		return fxRate;
	}
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}
	public BigDecimal getGstRate() {
		return gstRate;
	}
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}
	public Calendar getInvoiceIssuedDate() {
		return invoiceIssuedDate;
	}
	public void setInvoiceIssuedDate(Calendar invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getGstType() {
		return gstType;
	}
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getAccountReceivableCode() {
		return accountReceivableCode;
	}
	public void setAccountReceivableCode(String accountReceivableCode) {
		this.accountReceivableCode = accountReceivableCode;
	}
	public String getAccountReceivableName() {
		return accountReceivableName;
	}
	public void setAccountReceivableName(String accountReceivableName) {
		this.accountReceivableName = accountReceivableName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}
	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}
	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}
	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}
	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}
	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}
	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}
	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public BigDecimal getSettedIncludeGstOriginalAmount() {
		return settedIncludeGstOriginalAmount;
	}

	public void setSettedIncludeGstOriginalAmount(BigDecimal settedIncludeGstOriginalAmount) {
		this.settedIncludeGstOriginalAmount = settedIncludeGstOriginalAmount;
	}

	public BigDecimal getReceiptRate() {
		return receiptRate;
	}

	public void setReceiptRate(BigDecimal receiptRate) {
		this.receiptRate = receiptRate;
	}

	public BigDecimal getSettedIncludeGstConvertedAmount() {
		return settedIncludeGstConvertedAmount;
	}

	public void setSettedIncludeGstConvertedAmount(BigDecimal settedIncludeGstConvertedAmount) {
		this.settedIncludeGstConvertedAmount = settedIncludeGstConvertedAmount;
	}

	public BigDecimal getVarianceAmount() {
		return varianceAmount;
	}

	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	public String getVarianceAccountName() {
		return varianceAccountName;
	}

	public void setVarianceAccountName(String varianceAccountName) {
		this.varianceAccountName = varianceAccountName;
	}
	public BigDecimal getRemainIncludeGstOriginalAmount() {
		return remainIncludeGstOriginalAmount;
	}
	public void setRemainIncludeGstOriginalAmount(BigDecimal remainIncludeGstOriginalAmount) {
		this.remainIncludeGstOriginalAmount = remainIncludeGstOriginalAmount;
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
}
