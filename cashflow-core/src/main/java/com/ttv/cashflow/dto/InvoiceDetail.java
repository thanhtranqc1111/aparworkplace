package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

public class InvoiceDetail {

    private BigInteger apInvoiceAccountDetailId;
    private String accountCode;
    private String accountName;
    private BigInteger apInvoiceClassDetailId;
    private String classCode;
    private String className;
    private String description;
    private String poNo;
    private String approvalCode;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal fxRate;
    private BigDecimal excludeGstConvertedAmount;
    private String gstType;
    private BigDecimal gstRate;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private Boolean isApproval;
    public InvoiceDetail(BigInteger apInvoiceAccountDetailId, String accountCode, String accountName, BigInteger apInvoiceClassDetailId,
            String classCode, String className, String description, String poNo, String approvalCode, BigDecimal excludeGstOriginalAmount, 
            BigDecimal fxRate, BigDecimal excludeGstConvertedAmount, String gstType, BigDecimal gstRate, 
            BigDecimal gstOriginalAmount, BigDecimal gstConvertedAmount, 
            BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount, Boolean isApproval) {
        super();
        this.apInvoiceAccountDetailId = apInvoiceAccountDetailId;
        this.accountCode = accountCode;
        this.accountName = accountName;
        this.apInvoiceClassDetailId = apInvoiceClassDetailId;
        this.classCode = classCode;
        this.className = className;
        this.description = description;
        this.poNo = poNo;
        this.approvalCode = approvalCode;
        this.excludeGstOriginalAmount = excludeGstOriginalAmount;
        this.fxRate = fxRate;
        this.excludeGstConvertedAmount = excludeGstConvertedAmount;
        this.gstType = gstType;
        this.gstRate = gstRate;
        this.gstOriginalAmount = gstOriginalAmount;
        this.gstConvertedAmount = gstConvertedAmount;
        this.includeGstOriginalAmount = includeGstOriginalAmount;
        this.includeGstConvertedAmount = includeGstConvertedAmount;
        this.isApproval = isApproval;
    }

    public BigInteger getApInvoiceAccountDetailId() {
        return apInvoiceAccountDetailId;
    }

    public void setApInvoiceAccountDetailId(BigInteger apInvoiceAccountDetailId) {
        this.apInvoiceAccountDetailId = apInvoiceAccountDetailId;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public BigInteger getApInvoiceClassDetailId() {
        return apInvoiceClassDetailId;
    }

    public void setApInvoiceClassDetailId(BigInteger apInvoiceClassDetailId) {
        this.apInvoiceClassDetailId = apInvoiceClassDetailId;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }
    
    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public BigDecimal getExcludeGstOriginalAmount() {
        return excludeGstOriginalAmount;
    }

    public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
        this.excludeGstOriginalAmount = excludeGstOriginalAmount;
    }

    public BigDecimal getFxRate() {
        return fxRate;
    }

    public void setFxRate(BigDecimal fxRate) {
        this.fxRate = fxRate;
    }

    public BigDecimal getExcludeGstConvertedAmount() {
        return excludeGstConvertedAmount;
    }

    public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
        this.excludeGstConvertedAmount = excludeGstConvertedAmount;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }
    
    public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
        this.gstOriginalAmount = gstOriginalAmount;
    }
    
    public BigDecimal getGstOriginalAmount() {
        return gstOriginalAmount;
    }

    public BigDecimal getGstConvertedAmount() {
        return gstConvertedAmount;
    }
    
    public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
        this.gstConvertedAmount = gstConvertedAmount;
    }
    
    public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
        this.includeGstOriginalAmount = includeGstOriginalAmount;
    }
    
    public BigDecimal getIncludeGstOriginalAmount() {
        return includeGstOriginalAmount;
    }
    
    public BigDecimal getIncludeGstConvertedAmount() {
        return includeGstConvertedAmount;
    }

    public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
        this.includeGstConvertedAmount = includeGstConvertedAmount;
    }

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}
    
}
