package com.ttv.cashflow.dto;

import java.util.List;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author thoai.nh
 * created date Oct 27, 2017
 */
public class UserAccountAdminForm {
	private Integer id;
	private Integer roleId;
	@Email(message = "{cashflow.userAccount.message1}")
	@NotEmpty(message = "{cashflow.userAccount.message1}")
	private String email;
	private String newPassword;
	private String reNewPassword;
	@NotEmpty(message = "{cashflow.userAccount.message2}")
	private String firstName;
	@NotEmpty(message = "{cashflow.userAccount.message3}")
	private String lastName;
	@Pattern(regexp = "\\d{8}|\\d{9}|\\d{10}|\\d{11}|\\d{12}|\\d{13}|\\d{14}|\\d{15}|\\d{16}|^[a-f0-9]{32}$", message = "{cashflow.userAccount.message4}")
	private String phone;
	private int isActive;
	private int updatePasswordStatus;
	private List<Integer> settedSubsidiaries;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getReNewPassword() {
		return reNewPassword;
	}
	public void setReNewPassword(String reNewPassword) {
		this.reNewPassword = reNewPassword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public int getUpdatePasswordStatus() {
		return updatePasswordStatus;
	}
	public void setUpdatePasswordStatus(int updatePasswordStatus) {
		this.updatePasswordStatus = updatePasswordStatus;
	}
	public List<Integer> getSettedSubsidiaries() {
		return settedSubsidiaries;
	}
	public void setSettedSubsidiaries(List<Integer> settedSubsidiaries) {
		this.settedSubsidiaries = settedSubsidiaries;
	}
	@Override
	public String toString() {
		return "UserAccountAdminForm [id=" + id + ", roleId=" + roleId + ", email=" + email + ", newPassword="
				+ newPassword + ", reNewPassword=" + reNewPassword + ", firstName=" + firstName + ", lastName="
				+ lastName + ", phone=" + phone + ", isActive=" + isActive + ", updatePasswordStatus="
				+ updatePasswordStatus + ", settedSubsidiaries=" + settedSubsidiaries + "]";
	}
}
