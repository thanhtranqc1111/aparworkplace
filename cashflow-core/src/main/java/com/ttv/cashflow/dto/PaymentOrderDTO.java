package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * @author thoai.nh
 * created date Oct 17, 2017
 */
public class PaymentOrderDTO{
	private BigInteger id;
	private String paymentOrderNo;
	private BigDecimal paymentTotalAmount;
	private String description;
	private Calendar valueDate;
	private String bankRefNo;
	private BigDecimal includeGstConvertedAmount;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}
	public BigDecimal getPaymentTotalAmount() {
		return paymentTotalAmount;
	}
	public void setPaymentTotalAmount(BigDecimal paymentTotalAmount) {
		this.paymentTotalAmount = paymentTotalAmount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
}
