package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author thoai.nh
 * created date May 2, 2018
 */
public class ProjectAllocationPrecentageDTO {
	private BigInteger id; //id of project allocation record
	BigDecimal percentAllocation;
	public ProjectAllocationPrecentageDTO(BigInteger id, BigDecimal percentAllocation) {
		super();
		this.id = id;
		this.percentAllocation = percentAllocation;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public BigDecimal getPercentAllocation() {
		return percentAllocation;
	}
	public void setPercentAllocation(BigDecimal percentAllocation) {
		this.percentAllocation = percentAllocation;
	}
	
}
