package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * @author tien.tm
 * created date Dec 5, 2017
 */
public class ArInvoiceDTO {
	private BigInteger arId;
	private String arInvoiceNo;
	private Calendar month;
	private String payerAccount;
	private String claimType;
	private String claimTypeVal;
	private String payerName;
	private String invoiceNo;
	private BigDecimal totalAmount;
	private BigDecimal remainAmount;
	private String statusCode;
	private String status;
	private BigInteger bankTrans;
	private String bankName;
	private String bankAccount;
	private Calendar transDate;
	private Calendar issuedDate;
	private String refNo;
	private BigDecimal creditAmount;
	private String originalCurrencyCode;
	private String bankCurrencyCode;
	
	public ArInvoiceDTO(BigInteger arId, String arInvoiceNo, Calendar month, String payerAccount, 
	        String claimType, String payerName,
			String invoiceNo, BigDecimal totalAmount, BigDecimal remainAmount, String statusCode, String status,
			BigInteger bankTrans, String bankName, String bankAccount, Calendar transDate, Calendar issuedDate,
			String refNo, BigDecimal creditAmount, String originalCurrencyCode, String bankCurrencyCode) {
		super();
		this.arId = arId;
		this.arInvoiceNo = arInvoiceNo;
		this.month = month;
		this.payerAccount = payerAccount;
		this.claimType = claimType;
		this.payerName = payerName;
		this.invoiceNo = invoiceNo;
		this.totalAmount = totalAmount;
		this.remainAmount = remainAmount;
		this.statusCode = statusCode;
		this.status = status;
		this.bankTrans = bankTrans;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.transDate = transDate;
		this.issuedDate = issuedDate;
		this.refNo = refNo;
		this.creditAmount = creditAmount;
		this.originalCurrencyCode = originalCurrencyCode;
		this.bankCurrencyCode = bankCurrencyCode;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public BigInteger getArId() {
		return arId;
	}
	public void setArId(BigInteger arId) {
		this.arId = arId;
	}
	public String getArInvoiceNo() {
		return arInvoiceNo;
	}
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getPayerAccount() {
		return payerAccount;
	}
	public void setPayerAccount(String payerAccount) {
		this.payerAccount = payerAccount;
	}
	
	public String getClaimType() {
        return claimType;
    }
	
	public void setClaimType(String claimType) {
        this.claimType = claimType;
    }
	
	public void setClaimTypeVal(String claimTypeVal) {
        this.claimTypeVal = claimTypeVal;
    }
	
	public String getClaimTypeVal() {
        return claimTypeVal;
    }
	
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getRemainAmount() {
		return remainAmount;
	}
	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigInteger getBankTrans() {
		return bankTrans;
	}
	public void setBankTrans(BigInteger bankTrans) {
		this.bankTrans = bankTrans;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public Calendar getTransDate() {
		return transDate;
	}
	public void setTransDate(Calendar transDate) {
		this.transDate = transDate;
	}
	public Calendar getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(Calendar issuedDate) {
		this.issuedDate = issuedDate;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public BigDecimal getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public String getBankCurrencyCode() {
		return bankCurrencyCode;
	}
	public void setBankCurrencyCode(String bankCurrencyCode) {
		this.bankCurrencyCode = bankCurrencyCode;
	}
	
}
