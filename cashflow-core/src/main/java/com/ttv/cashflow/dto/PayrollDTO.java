package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;

/**
 * @author thoai.nh
 * created date Mar 16, 2018
 */
public class PayrollDTO {
	private BigInteger id;
	private Calendar month;
	private String payrollNo;
	private String invoiceNo;
	private Calendar bookingDate;
	private String claimType;
	private String accountPayableCode;
	private String accountPayableName;
	private Calendar paymentScheduleDate;
	private BigDecimal totalLaborCostOriginal;
	private BigDecimal totalDeductionOriginal;
	private BigDecimal totalNetPaymentOriginal;
	private BigDecimal fxRate;
	private BigDecimal totalNetPaymentConverted;
	private String originalCurrencyCode;
	private String status;
	private Calendar createdDate;
	private String description;
	private Set<PayrollDetailsDTO> payrollDetailses;
	private String statusVal;
	private String importKey;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getPayrollNo() {
		return payrollNo;
	}
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}
	public Calendar getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Calendar bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getAccountPayableCode() {
		return accountPayableCode;
	}
	public void setAccountPayableCode(String accountPayableCode) {
		this.accountPayableCode = accountPayableCode;
	}
	public String getAccountPayableName() {
		return accountPayableName;
	}
	public void setAccountPayableName(String accountPayableName) {
		this.accountPayableName = accountPayableName;
	}
	public Calendar getPaymentScheduleDate() {
		return paymentScheduleDate;
	}
	public void setPaymentScheduleDate(Calendar paymentScheduleDate) {
		this.paymentScheduleDate = paymentScheduleDate;
	}
	public BigDecimal getTotalLaborCostOriginal() {
		return totalLaborCostOriginal;
	}
	public void setTotalLaborCostOriginal(BigDecimal totalLaborCostOriginal) {
		this.totalLaborCostOriginal = totalLaborCostOriginal;
	}
	public BigDecimal getTotalDeductionOriginal() {
		return totalDeductionOriginal;
	}
	public void setTotalDeductionOriginal(BigDecimal totalDeductionOriginal) {
		this.totalDeductionOriginal = totalDeductionOriginal;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Calendar getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}
	public Set<PayrollDetailsDTO> getPayrollDetailses() {
		return payrollDetailses;
	}
	public void setPayrollDetailses(Set<PayrollDetailsDTO> payrollDetailses) {
		this.payrollDetailses = payrollDetailses;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatusVal() {
		return statusVal;
	}
	public void setStatusVal(String statusVal) {
		this.statusVal = statusVal;
	}
	public String getImportKey() {
		return importKey;
	}
	public void setImportKey(String importKey) {
		this.importKey = importKey;
	}
	public BigDecimal getTotalNetPaymentOriginal() {
		return totalNetPaymentOriginal;
	}
	public void setTotalNetPaymentOriginal(BigDecimal totalNetPaymentOriginal) {
		this.totalNetPaymentOriginal = totalNetPaymentOriginal;
	}
	public BigDecimal getTotalNetPaymentConverted() {
		return totalNetPaymentConverted;
	}
	public void setTotalNetPaymentConverted(BigDecimal totalNetPaymentConverted) {
		this.totalNetPaymentConverted = totalNetPaymentConverted;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public BigDecimal getFxRate() {
		return fxRate;
	}
	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String toJSON() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	public List<PayrollDetailsDTO> getPayrollDetailsesList() {
		List<PayrollDetailsDTO> list = payrollDetailses.stream().collect(Collectors.toList());
		list.sort((PayrollDetailsDTO o1, PayrollDetailsDTO o2) -> o1.getId().compareTo(o2.getId()));
		return list;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountPayableCode == null) ? 0 : accountPayableCode.hashCode());
		result = prime * result + ((accountPayableName == null) ? 0 : accountPayableName.hashCode());
		result = prime * result + ((bookingDate == null) ? 0 : bookingDate.hashCode());
		result = prime * result + ((claimType == null) ? 0 : claimType.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((invoiceNo == null) ? 0 : invoiceNo.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((originalCurrencyCode == null) ? 0 : originalCurrencyCode.hashCode());
		result = prime * result + ((paymentScheduleDate == null) ? 0 : paymentScheduleDate.hashCode());
		result = prime * result + ((totalNetPaymentConverted == null) ? 0 : totalNetPaymentConverted.hashCode());
		result = prime * result + ((totalNetPaymentOriginal == null) ? 0 : totalNetPaymentOriginal.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayrollDTO other = (PayrollDTO) obj;
		if (accountPayableCode == null) {
			if (other.accountPayableCode != null)
				return false;
		} else if (!accountPayableCode.equals(other.accountPayableCode))
			return false;
		if (accountPayableName == null) {
			if (other.accountPayableName != null)
				return false;
		} else if (!accountPayableName.equals(other.accountPayableName))
			return false;
		if (bookingDate == null) {
			if (other.bookingDate != null)
				return false;
		} else if (!bookingDate.equals(other.bookingDate))
			return false;
		if (claimType == null) {
			if (other.claimType != null)
				return false;
		} else if (!claimType.equals(other.claimType))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (invoiceNo == null) {
			if (other.invoiceNo != null)
				return false;
		} else if (!invoiceNo.equals(other.invoiceNo))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (originalCurrencyCode == null) {
			if (other.originalCurrencyCode != null)
				return false;
		} else if (!originalCurrencyCode.equals(other.originalCurrencyCode))
			return false;
		if (paymentScheduleDate == null) {
			if (other.paymentScheduleDate != null)
				return false;
		} else if (!paymentScheduleDate.equals(other.paymentScheduleDate))
			return false;
		if (totalNetPaymentConverted == null) {
			if (other.totalNetPaymentConverted != null)
				return false;
		} else if (!totalNetPaymentConverted.equals(other.totalNetPaymentConverted))
			return false;
		if (totalNetPaymentOriginal == null) {
			if (other.totalNetPaymentOriginal != null)
				return false;
		} else if (!totalNetPaymentOriginal.equals(other.totalNetPaymentOriginal))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PayrollDTO [id=" + id + ", month=" + month + ", payrollNo=" + payrollNo + ", invoiceNo=" + invoiceNo
				+ ", bookingDate=" + bookingDate + ", claimType=" + claimType + ", accountPayableCode="
				+ accountPayableCode + ", accountPayableName=" + accountPayableName + ", paymentScheduleDate="
				+ paymentScheduleDate + ", totalLaborCostOriginal=" + totalLaborCostOriginal
				+ ", totalDeductionOriginal=" + totalDeductionOriginal + ", totalNetPaymentOriginal="
				+ totalNetPaymentOriginal + ", fxRate=" + fxRate + ", totalNetPaymentConverted="
				+ totalNetPaymentConverted + ", originalCurrencyCode=" + originalCurrencyCode + ", status=" + status
				+ ", createdDate=" + createdDate + ", description=" + description + ", payrollDetailses="
				+ payrollDetailses + ", statusVal=" + statusVal + ", importKey=" + importKey + "]";
	}
	
}
