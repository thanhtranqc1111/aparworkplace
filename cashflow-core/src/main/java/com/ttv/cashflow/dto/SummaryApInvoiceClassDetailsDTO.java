package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * @author thoai.nh
 * summary info of ApInvoiceClassDetails group by amount
 * created date Oct 11, 2017
 */
public class SummaryApInvoiceClassDetailsDTO {
	private BigInteger id;
	private String approvalCode;
	private Calendar applicationDate;
	private String currency;
	private BigDecimal amount;
	private BigInteger apInvoiceId;
	private String description;
	private String apInvoiceNo;
	public SummaryApInvoiceClassDetailsDTO() {}
	public SummaryApInvoiceClassDetailsDTO(BigInteger id, String approvalCode, Calendar applicationDate, String currency,
			BigDecimal amount, BigInteger apInvoiceId) {
		super();
		this.id = id;
		this.approvalCode = approvalCode;
		this.applicationDate = applicationDate;
		this.currency = currency;
		this.amount = amount;
		this.apInvoiceId = apInvoiceId;
	}

	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public Calendar getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public BigInteger getApInvoiceId() {
		return apInvoiceId;
	}
	public void setApInvoiceId(BigInteger apInvoiceId) {
		this.apInvoiceId = apInvoiceId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApInvoiceNo() {
		return apInvoiceNo;
	}
	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}
	@Override
	public String toString() {
		return "ApInvoiceClassDetailsDTO [id=" + id + ", approvalCode=" + approvalCode + ", applicationDate="
				+ applicationDate + ", currency=" + currency + ", amount=" + amount + ", apInvoiceId=" + apInvoiceId
				+ ", description=" + description + "]";
	}
}
