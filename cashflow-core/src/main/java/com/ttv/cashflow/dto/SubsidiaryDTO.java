package com.ttv.cashflow.dto;

/**
 * @author thoai.nh
 * created date Oct 30, 2017
 */
public class SubsidiaryDTO {
	private Integer id;
	private String code;
	private String name;
	private String address;
	private boolean status;
	
	public SubsidiaryDTO() {
		super();
	}
	public SubsidiaryDTO(Integer id, String code, String name, String address, boolean status) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.address = address;
		this.status = status;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
