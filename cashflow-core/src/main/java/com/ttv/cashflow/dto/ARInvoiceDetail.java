package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ARInvoiceDetail {

    private BigInteger arInvoiceAccountDetailId;
    private String accountCode;
    private String accountName;
    private BigInteger arInvoiceClassDetailId;
    private String classCode;
    private String className;
    private String approvalCode;
    private String description;
    private BigDecimal excludeGstOriginalAmount;
    private BigDecimal fxRate;
    private BigDecimal excludeGstConvertedAmount;
    private String gstType;
    private BigDecimal gstRate;
    private BigDecimal gstOriginalAmount;
    private BigDecimal gstConvertedAmount;
    private BigDecimal includeGstOriginalAmount;
    private BigDecimal includeGstConvertedAmount;
    private Boolean isApproval;
    
	public ARInvoiceDetail() {
	}

	public ARInvoiceDetail(BigInteger arInvoiceAccountDetailId, String accountCode, String accountName,
			BigInteger arInvoiceClassDetailId, String classCode, String className, String approvalCode,
			String description, BigDecimal excludeGstOriginalAmount, BigDecimal fxRate,
			BigDecimal excludeGstConvertedAmount, String gstType, BigDecimal gstRate, BigDecimal gstOriginalAmount,
			BigDecimal gstConvertedAmount, BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount,
			Boolean isApproval) {
		this.arInvoiceAccountDetailId = arInvoiceAccountDetailId;
		this.accountCode = accountCode;
		this.accountName = accountName;
		this.arInvoiceClassDetailId = arInvoiceClassDetailId;
		this.classCode = classCode;
		this.className = className;
		this.approvalCode = approvalCode;
		this.description = description;
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
		this.fxRate = fxRate;
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
		this.gstType = gstType;
		this.gstRate = gstRate;
		this.gstOriginalAmount = gstOriginalAmount;
		this.gstConvertedAmount = gstConvertedAmount;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.isApproval = isApproval;
	}

	public BigInteger getArInvoiceAccountDetailId() {
		return arInvoiceAccountDetailId;
	}

	public void setArInvoiceAccountDetailId(BigInteger arInvoiceAccountDetailId) {
		this.arInvoiceAccountDetailId = arInvoiceAccountDetailId;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public BigInteger getArInvoiceClassDetailId() {
		return arInvoiceClassDetailId;
	}

	public void setArInvoiceClassDetailId(BigInteger arInvoiceClassDetailId) {
		this.arInvoiceClassDetailId = arInvoiceClassDetailId;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getExcludeGstOriginalAmount() {
		return excludeGstOriginalAmount;
	}

	public void setExcludeGstOriginalAmount(BigDecimal excludeGstOriginalAmount) {
		this.excludeGstOriginalAmount = excludeGstOriginalAmount;
	}

	public BigDecimal getFxRate() {
		return fxRate;
	}

	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}

	public BigDecimal getExcludeGstConvertedAmount() {
		return excludeGstConvertedAmount;
	}

	public void setExcludeGstConvertedAmount(BigDecimal excludeGstConvertedAmount) {
		this.excludeGstConvertedAmount = excludeGstConvertedAmount;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public BigDecimal getGstRate() {
		return gstRate;
	}

	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}

	public BigDecimal getGstOriginalAmount() {
		return gstOriginalAmount;
	}

	public void setGstOriginalAmount(BigDecimal gstOriginalAmount) {
		this.gstOriginalAmount = gstOriginalAmount;
	}

	public BigDecimal getGstConvertedAmount() {
		return gstConvertedAmount;
	}

	public void setGstConvertedAmount(BigDecimal gstConvertedAmount) {
		this.gstConvertedAmount = gstConvertedAmount;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public Boolean getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}

	
}
