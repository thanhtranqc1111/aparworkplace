package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author thoai.nh
 * created date Mar 16, 2018
 */
public class PayrollDetailsDTO {
	private BigInteger id;
	private String employeeCode;
	private String name;
	private String roleTitle;
	private String division;
	private String team;
	private BigDecimal laborBaseSalaryPayment;
	private BigDecimal laborAdditionalPayment1;
	private BigDecimal laborAdditionalPayment2;
	private BigDecimal laborAdditionalPayment3;
	private BigDecimal laborSocialInsuranceEmployer1;
	private BigDecimal laborSocialInsuranceEmployer2;
	private BigDecimal laborSocialInsuranceEmployer3;
	private BigDecimal laborOtherPayment1;
	private BigDecimal laborOtherPayment2;
	private BigDecimal laborOtherPayment3;
	private BigDecimal totalLaborCostOriginal;
	private BigDecimal deductionSocialInsuranceEmployer1;
	private BigDecimal deductionSocialInsuranceEmployer2;
	private BigDecimal deductionSocialInsuranceEmployer3;
	private BigDecimal deductionSocialInsuranceEmployee1;
	private BigDecimal deductionSocialInsuranceEmployee2;
	private BigDecimal deductionSocialInsuranceEmployee3;
	private BigDecimal deductionWht;
	private BigDecimal deductionOther1;
	private BigDecimal deductionOther2;
	private BigDecimal deductionOther3;
	private BigDecimal totalDeductionOriginal;
	private BigDecimal netPaymentOriginal;
	private BigDecimal netPaymentConverted;
	private String approvalCode;
	private Boolean isApproval;
	private String apprStatusCode;
    private String apprStatusVal;
    private BigDecimal approvalIncludeGstOriginalAmount;
    private boolean edited;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getRoleTitle() {
		return roleTitle;
	}
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public BigDecimal getLaborBaseSalaryPayment() {
		return laborBaseSalaryPayment;
	}
	public void setLaborBaseSalaryPayment(BigDecimal laborBaseSalaryPayment) {
		this.laborBaseSalaryPayment = laborBaseSalaryPayment;
	}
	public BigDecimal getLaborAdditionalPayment1() {
		return laborAdditionalPayment1;
	}
	public void setLaborAdditionalPayment1(BigDecimal laborAdditionalPayment1) {
		this.laborAdditionalPayment1 = laborAdditionalPayment1;
	}
	public BigDecimal getLaborAdditionalPayment2() {
		return laborAdditionalPayment2;
	}
	public void setLaborAdditionalPayment2(BigDecimal laborAdditionalPayment2) {
		this.laborAdditionalPayment2 = laborAdditionalPayment2;
	}
	public BigDecimal getLaborAdditionalPayment3() {
		return laborAdditionalPayment3;
	}
	public void setLaborAdditionalPayment3(BigDecimal laborAdditionalPayment3) {
		this.laborAdditionalPayment3 = laborAdditionalPayment3;
	}
	public BigDecimal getLaborSocialInsuranceEmployer1() {
		return laborSocialInsuranceEmployer1;
	}
	public void setLaborSocialInsuranceEmployer1(BigDecimal laborSocialInsuranceEmployer1) {
		this.laborSocialInsuranceEmployer1 = laborSocialInsuranceEmployer1;
	}
	public BigDecimal getLaborSocialInsuranceEmployer2() {
		return laborSocialInsuranceEmployer2;
	}
	public void setLaborSocialInsuranceEmployer2(BigDecimal laborSocialInsuranceEmployer2) {
		this.laborSocialInsuranceEmployer2 = laborSocialInsuranceEmployer2;
	}
	public BigDecimal getLaborSocialInsuranceEmployer3() {
		return laborSocialInsuranceEmployer3;
	}
	public void setLaborSocialInsuranceEmployer3(BigDecimal laborSocialInsuranceEmployer3) {
		this.laborSocialInsuranceEmployer3 = laborSocialInsuranceEmployer3;
	}
	public BigDecimal getLaborOtherPayment1() {
		return laborOtherPayment1;
	}
	public void setLaborOtherPayment1(BigDecimal laborOtherPayment1) {
		this.laborOtherPayment1 = laborOtherPayment1;
	}
	public BigDecimal getLaborOtherPayment2() {
		return laborOtherPayment2;
	}
	public void setLaborOtherPayment2(BigDecimal laborOtherPayment2) {
		this.laborOtherPayment2 = laborOtherPayment2;
	}
	public BigDecimal getLaborOtherPayment3() {
		return laborOtherPayment3;
	}
	public void setLaborOtherPayment3(BigDecimal laborOtherPayment3) {
		this.laborOtherPayment3 = laborOtherPayment3;
	}
	public BigDecimal getTotalLaborCostOriginal() {
		return totalLaborCostOriginal;
	}
	public void setTotalLaborCostOriginal(BigDecimal totalLaborCostOriginal) {
		this.totalLaborCostOriginal = totalLaborCostOriginal;
	}
	public BigDecimal getDeductionSocialInsuranceEmployer1() {
		return deductionSocialInsuranceEmployer1;
	}
	public void setDeductionSocialInsuranceEmployer1(BigDecimal deductionSocialInsuranceEmployer1) {
		this.deductionSocialInsuranceEmployer1 = deductionSocialInsuranceEmployer1;
	}
	public BigDecimal getDeductionSocialInsuranceEmployer2() {
		return deductionSocialInsuranceEmployer2;
	}
	public void setDeductionSocialInsuranceEmployer2(BigDecimal deductionSocialInsuranceEmployer2) {
		this.deductionSocialInsuranceEmployer2 = deductionSocialInsuranceEmployer2;
	}
	public BigDecimal getDeductionSocialInsuranceEmployer3() {
		return deductionSocialInsuranceEmployer3;
	}
	public void setDeductionSocialInsuranceEmployer3(BigDecimal deductionSocialInsuranceEmployer3) {
		this.deductionSocialInsuranceEmployer3 = deductionSocialInsuranceEmployer3;
	}
	public BigDecimal getDeductionSocialInsuranceEmployee1() {
		return deductionSocialInsuranceEmployee1;
	}
	public void setDeductionSocialInsuranceEmployee1(BigDecimal deductionSocialInsuranceEmployee1) {
		this.deductionSocialInsuranceEmployee1 = deductionSocialInsuranceEmployee1;
	}
	public BigDecimal getDeductionSocialInsuranceEmployee2() {
		return deductionSocialInsuranceEmployee2;
	}
	public void setDeductionSocialInsuranceEmployee2(BigDecimal deductionSocialInsuranceEmployee2) {
		this.deductionSocialInsuranceEmployee2 = deductionSocialInsuranceEmployee2;
	}
	public BigDecimal getDeductionSocialInsuranceEmployee3() {
		return deductionSocialInsuranceEmployee3;
	}
	public void setDeductionSocialInsuranceEmployee3(BigDecimal deductionSocialInsuranceEmployee3) {
		this.deductionSocialInsuranceEmployee3 = deductionSocialInsuranceEmployee3;
	}
	public BigDecimal getDeductionWht() {
		return deductionWht;
	}
	public void setDeductionWht(BigDecimal deductionWht) {
		this.deductionWht = deductionWht;
	}
	public BigDecimal getDeductionOther1() {
		return deductionOther1;
	}
	public void setDeductionOther1(BigDecimal deductionOther1) {
		this.deductionOther1 = deductionOther1;
	}
	public BigDecimal getDeductionOther2() {
		return deductionOther2;
	}
	public void setDeductionOther2(BigDecimal deductionOther2) {
		this.deductionOther2 = deductionOther2;
	}
	public BigDecimal getDeductionOther3() {
		return deductionOther3;
	}
	public void setDeductionOther3(BigDecimal deductionOther3) {
		this.deductionOther3 = deductionOther3;
	}
	public BigDecimal getTotalDeductionOriginal() {
		return totalDeductionOriginal;
	}
	public void setTotalDeductionOriginal(BigDecimal totalDeductionOriginal) {
		this.totalDeductionOriginal = totalDeductionOriginal;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public Boolean getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(Boolean isApproval) {
		this.isApproval = isApproval;
	}
	public String getApprStatusCode() {
		return apprStatusCode;
	}
	public void setApprStatusCode(String apprStatusCode) {
		this.apprStatusCode = apprStatusCode;
	}
	public String getApprStatusVal() {
		return apprStatusVal;
	}
	public void setApprStatusVal(String apprStatusVal) {
		this.apprStatusVal = apprStatusVal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getApprovalIncludeGstOriginalAmount() {
		return approvalIncludeGstOriginalAmount;
	}
	public void setApprovalIncludeGstOriginalAmount(BigDecimal approvalIncludeGstOriginalAmount) {
		this.approvalIncludeGstOriginalAmount = approvalIncludeGstOriginalAmount;
	}
	public BigDecimal getNetPaymentOriginal() {
		return netPaymentOriginal;
	}
	public void setNetPaymentOriginal(BigDecimal netPaymentOriginal) {
		this.netPaymentOriginal = netPaymentOriginal;
	}
	public BigDecimal getNetPaymentConverted() {
		return netPaymentConverted;
	}
	public void setNetPaymentConverted(BigDecimal netPaymentConverted) {
		this.netPaymentConverted = netPaymentConverted;
	}
	public boolean isEdited() {
		return edited;
	}
	public void setEdited(boolean edited) {
		this.edited = edited;
	}
}
