package com.ttv.cashflow.dto;

import java.math.BigDecimal;

public class ProjectPaymentDetail {

	private String employeeCode;
	private String employeeName;
	private BigDecimal employeeSalary;

	private String projectCode;
	private String projectName;
	private BigDecimal projectAssignedSalary;

	public ProjectPaymentDetail() {
		
	}

	public ProjectPaymentDetail(String employeeCode, String employeeName, BigDecimal employeeSalary,
			String projectCode, String projectName, BigDecimal projectAssignedSalary) {
		super();
		this.employeeCode = employeeCode;
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.projectAssignedSalary = projectAssignedSalary;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public BigDecimal getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(BigDecimal employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public BigDecimal getProjectAssignedSalary() {
		return projectAssignedSalary;
	}

	public void setProjectAssignedSalary(BigDecimal projectAssignedSalary) {
		this.projectAssignedSalary = projectAssignedSalary;
	}

}
