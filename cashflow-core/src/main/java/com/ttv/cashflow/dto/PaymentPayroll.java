package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import com.ttv.cashflow.domain.BankStatement;

/**
 * 
 * @author tientm
 *
 */
public class PaymentPayroll {

	private BigInteger paymentId;
	private String paymentOrderNo;
	private Calendar valueDate;
	private String bankName;
	private String bankAccount;
	private List<BankStatement> bankStatement;
	private String bankRefNo;
	private BigDecimal includeGstOriginalAmount; 
	private BigDecimal includeGstConvertedAmount; //net payment (payment Order)
	private BigInteger payrollId;
	private String payrollNo;
	private Calendar month;
	private BigDecimal totalNetPaymentAmountOriginal;
	private BigDecimal totalNetPaymentAmountConverted;
	private String paymentStatus;
	private String paymentStatusCode;
	private String payrollStatus;
	private String payrollStatusCode;
	private String paymentCurrency;
	private String payrollCurrency;
	public PaymentPayroll() {
		super();
	}
	
	public PaymentPayroll(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankName,
			String bankAccount, List<BankStatement> bankStatement, String bankRefNo,
			BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount, BigInteger payrollId,
			String payrollNo, Calendar month, BigDecimal totalNetPaymentAmountOriginal,
			BigDecimal totalNetPaymentAmountConverted, String paymentStatus, String paymentStatusCode,
			String payrollStatus, String payrollStatusCode, String paymentCurrency, String payrollCurrency) {
		super();
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankStatement = bankStatement;
		this.bankRefNo = bankRefNo;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.payrollId = payrollId;
		this.payrollNo = payrollNo;
		this.month = month;
		this.totalNetPaymentAmountOriginal = totalNetPaymentAmountOriginal;
		this.totalNetPaymentAmountConverted = totalNetPaymentAmountConverted;
		this.paymentStatus = paymentStatus;
		this.paymentStatusCode = paymentStatusCode;
		this.payrollStatus = payrollStatus;
		this.payrollStatusCode = payrollStatusCode;
		this.paymentCurrency = paymentCurrency;
		this.payrollCurrency = payrollCurrency;
	}



	// non bank-statement 
	public PaymentPayroll(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankName,
			String bankAccount, String bankRefNo, BigDecimal includeGstOriginalAmount,
			BigDecimal includeGstConvertedAmount, BigInteger payrollId, String payrollNo, Calendar month, BigDecimal totalNetPaymentAmountConverted,
			BigDecimal totalNetPaymentAmountOriginal, String paymentStatus, String payrollStatus, String paymentCurrency) {
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankRefNo = bankRefNo;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.payrollId = payrollId;
		this.payrollNo = payrollNo;
		this.month = month;
		this.totalNetPaymentAmountOriginal = totalNetPaymentAmountOriginal;
		this.totalNetPaymentAmountConverted = totalNetPaymentAmountConverted;
		this.paymentStatus = paymentStatus;
		this.payrollStatus = payrollStatus;
		this.paymentCurrency = paymentCurrency;
	}

	public BigInteger getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(BigInteger paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public List<BankStatement> getBankStatement() {
		return bankStatement;
	}
	public void setBankStatement(List<BankStatement> bankStatement) {
		this.bankStatement = bankStatement;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}
	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}
	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}
	public BigInteger getPayrollId() {
		return payrollId;
	}
	public void setPayrollId(BigInteger payrollId) {
		this.payrollId = payrollId;
	}
	public String getPayrollNo() {
		return payrollNo;
	}
	public void setPayrollNo(String payrollNo) {
		this.payrollNo = payrollNo;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	
	public BigDecimal getTotalNetPaymentAmountOriginal() {
		return totalNetPaymentAmountOriginal;
	}

	public void setTotalNetPaymentAmountOriginal(BigDecimal totalNetPaymentAmountOriginal) {
		this.totalNetPaymentAmountOriginal = totalNetPaymentAmountOriginal;
	}

	public BigDecimal getTotalNetPaymentAmountConverted() {
		return totalNetPaymentAmountConverted;
	}

	public void setTotalNetPaymentAmountConverted(BigDecimal totalNetPaymentAmountConverted) {
		this.totalNetPaymentAmountConverted = totalNetPaymentAmountConverted;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentStatusCode() {
		return paymentStatusCode;
	}
	public void setPaymentStatusCode(String paymentStatusCode) {
		this.paymentStatusCode = paymentStatusCode;
	}
	public String getPayrollStatus() {
		return payrollStatus;
	}
	public void setPayrollStatus(String payrollStatus) {
		this.payrollStatus = payrollStatus;
	}
	public String getPayrollStatusCode() {
		return payrollStatusCode;
	}
	public void setPayrollStatusCode(String payrollStatusCode) {
		this.payrollStatusCode = payrollStatusCode;
	}
	public String getPaymentCurrency() {
		return paymentCurrency;
	}
	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getPayrollCurrency() {
		return payrollCurrency;
	}

	public void setPayrollCurrency(String payrollCurrency) {
		this.payrollCurrency = payrollCurrency;
	}
	
	
}
