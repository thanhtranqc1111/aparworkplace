package com.ttv.cashflow.dto;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author thoai.nh
 * created date Oct 26, 2017
 */
public class UserAccountForm {
	private Integer id;
	
	private Integer defaultSubsidiaryId;
	
	private String password;
	
	private String newPassword;
	
	private String reNewPassword;
	
	@NotEmpty(message = "{cashflow.userAccount.message2}")
	private String firstName;
	
	@NotEmpty(message = "{cashflow.userAccount.message3}")
	private String lastName;
	
	@Pattern(regexp = "\\d{8}|\\d{9}|\\d{10}|\\d{11}|\\d{12}|\\d{13}|\\d{14}|\\d{15}|\\d{16}|^[a-f0-9]{32}$", message = "{cashflow.userAccount.message4}")
	private String phone;
	
	private int updatePasswordStatus;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getReNewPassword() {
		return reNewPassword;
	}
	public void setReNewPassword(String reNewPassword) {
		this.reNewPassword = reNewPassword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getUpdatePasswordStatus() {
		return updatePasswordStatus;
	}
	public void setUpdatePasswordStatus(int updatePasswordStatus) {
		this.updatePasswordStatus = updatePasswordStatus;
	}
	public Integer getDefaultSubsidiaryId() {
		return defaultSubsidiaryId;
	}
	public void setDefaultSubsidiaryId(Integer defaultSubsidiaryId) {
		this.defaultSubsidiaryId = defaultSubsidiaryId;
	}
}
