package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

/**
 * @author thoai.nh
 * created date Mar 20, 2018
 */
public class ReimbursementPaymentDTO {
	private BigInteger id;
	private Calendar month;
	private String reimbursementNo;
	private String claimType;
	private String employeeName;
	private String employeeCode;
	private String originalCurrencyCode;
	private String paymentOrderNo;
	private Calendar valueDate;
	private String bankName;
	private String bankAccount;
	private String bankRefNo;
	private BigDecimal includeGstTotalAmountConverted;
	private String paymentOrderStatus;
	private String reimbursementStatus;
	private BigDecimal includeGstConvertedAmount;
	private String paymentOrderStatusCode;
	private String reimbursementStatusCode;
	private String claimTypeVal;
	List<BigInteger> bankStatementIds;
	//tien.tm
	private String invoiceNo;
	
	public ReimbursementPaymentDTO(BigInteger id, Calendar month, String reimbursementNo, String claimType,
			String employeeName, String employeeCode, String originalCurrencyCode, String paymentOrderNo,
			Calendar valueDate, String bankName, String bankAccount, String bankRefNo,
			BigDecimal includeGstTotalAmountConverted, String paymentOrderStatus, String reimbursementStatus,
			BigDecimal includeGstConvertedAmount, String invoiceNo) {
		super();
		this.id = id;
		this.month = month;
		this.reimbursementNo = reimbursementNo;
		this.claimType = claimType;
		this.employeeName = employeeName;
		this.employeeCode = employeeCode;
		this.originalCurrencyCode = originalCurrencyCode;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankRefNo = bankRefNo;
		this.includeGstTotalAmountConverted = includeGstTotalAmountConverted;
		this.paymentOrderStatus = paymentOrderStatus;
		this.reimbursementStatus = reimbursementStatus;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.invoiceNo = invoiceNo;
	}
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getReimbursementNo() {
		return reimbursementNo;
	}
	public void setReimbursementNo(String reimbursementNo) {
		this.reimbursementNo = reimbursementNo;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}
	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}
	public String getPaymentOrderStatus() {
		return paymentOrderStatus;
	}
	public void setPaymentOrderStatus(String paymentOrderStatus) {
		this.paymentOrderStatus = paymentOrderStatus;
	}
	public String getReimbursementStatus() {
		return reimbursementStatus;
	}
	public void setReimbursementStatus(String reimbursementStatus) {
		this.reimbursementStatus = reimbursementStatus;
	}
	public String getPaymentOrderStatusCode() {
		return paymentOrderStatusCode;
	}
	public void setPaymentOrderStatusCode(String paymentOrderStatusCode) {
		this.paymentOrderStatusCode = paymentOrderStatusCode;
	}
	public String getReimbursementStatusCode() {
		return reimbursementStatusCode;
	}

	public void setReimbursementStatusCode(String reimbursementStatusCode) {
		this.reimbursementStatusCode = reimbursementStatusCode;
	}

	public String getClaimTypeVal() {
		return claimTypeVal;
	}
	public void setClaimTypeVal(String claimTypeVal) {
		this.claimTypeVal = claimTypeVal;
	}

	public List<BigInteger> getBankStatementIds() {
		return bankStatementIds;
	}

	public void setBankStatementIds(List<BigInteger> bankStatementIds) {
		this.bankStatementIds = bankStatementIds;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}
	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public BigDecimal getIncludeGstTotalAmountConverted() {
		return includeGstTotalAmountConverted;
	}

	public void setIncludeGstTotalAmountConverted(BigDecimal includeGstTotalAmountConverted) {
		this.includeGstTotalAmountConverted = includeGstTotalAmountConverted;
	}
	
}	
