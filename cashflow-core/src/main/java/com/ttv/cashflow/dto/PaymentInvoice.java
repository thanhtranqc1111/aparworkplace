package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ttv.cashflow.domain.BankStatement;

/**
 * 
 * @author tientm
 *
 */
public class PaymentInvoice {

	private BigInteger paymentId;
	private String paymentOrderNo;
	private Calendar valueDate;
	private String bankName;
	private String bankAccount;
	private String bankRefNo;
	private BigDecimal includeGstOriginalAmount;
	private BigDecimal includeGstConvertedAmount;
	private String paymentCurrency;
	private String paymentStatus;

	//binh.lv
	private String paymentType;
	private BigInteger accountPayableId;
	private String accountPayableNo;
	private Calendar month;
	private String claimType;
	private BigDecimal totalAmount;
	private String invoiceStatus;
	private String accountPayableCurrency;
	private List<BankStatement> bankStatement;
	private BigInteger apInvoiceId;
	private String apInvoiceNo;

	private Date issuedDate;
	private String payeeName;
	private String payeeCode;
	private String invoiceNo;

	private String payeeAccount;

	// thoai.nh
	private String paymentStatusCode;
	private String invoiceStatusCode;
	public PaymentInvoice(String paymentOrderNo, String paymentType) {
		super();
		this.paymentOrderNo = paymentOrderNo;
		this.paymentType = paymentType;
	}
	
	public PaymentInvoice() {
		super();
	}
	
	// binh.lv - Account Payable Detail
	public PaymentInvoice(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankName,
			String bankAccount, String bankRefNo,
			BigDecimal includeGstConvertedAmount, String paymentStatus, String paymentType,
			BigInteger accountPayableId, String accountPayableNo, Calendar month, String claimType,
			BigDecimal totalAmount, String invoiceStatus) {
		super();
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankRefNo = bankRefNo;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.paymentStatus = paymentStatus;
		this.paymentType = paymentType;
		this.accountPayableId = accountPayableId;
		this.accountPayableNo = accountPayableNo;
		this.month = month;
		this.claimType = claimType;
		this.totalAmount = totalAmount;
		this.invoiceStatus = invoiceStatus;
	}

	
	
	
	public PaymentInvoice(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankRefNo,
			BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount, String paymentStatus,
			BigInteger apInvoiceId, String apInvoiceNo, Date issuedDate, String payeeName, String payeeCode,
			String invoiceNo, BigDecimal totalAmount, String invoiceStatus, String payeeAccount, Calendar month,
			String bankName, String bankAccount, List<BankStatement> bankStatement, String accountPayableCurrency,
			String paymentCurrency) {
		super();
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankRefNo = bankRefNo;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.paymentStatus = paymentStatus;
		this.apInvoiceId = apInvoiceId;
		this.apInvoiceNo = apInvoiceNo;
		this.issuedDate = issuedDate;
		this.payeeName = payeeName;
		this.payeeCode = payeeCode;
		this.invoiceNo = invoiceNo;
		this.totalAmount = totalAmount;
		this.invoiceStatus = invoiceStatus;
		this.payeeAccount = payeeAccount;
		this.month = month;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankStatement = bankStatement;
		this.accountPayableCurrency = accountPayableCurrency;
		this.paymentCurrency = paymentCurrency;
	}

	public BigInteger getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(BigInteger paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}

	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	public Calendar getValueDate() {
		return valueDate;
	}

	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public BigInteger getApInvoiceId() {
		return apInvoiceId;
	}

	public void setApInvoiceId(BigInteger apInvoiceId) {
		this.apInvoiceId = apInvoiceId;
	}

	public String getApInvoiceNo() {
		return apInvoiceNo;
	}

	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getPayeeCode() {
		return payeeCode;
	}

	public void setPayeeCode(String payeeCode) {
		this.payeeCode = payeeCode;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getPayeeAccount() {
		return payeeAccount;
	}

	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public List<BankStatement> getBankStatement() {
		return bankStatement;
	}

	public void setBankStatement(List<BankStatement> bankStatement) {
		this.bankStatement = bankStatement;
	}

	public String getAccountPayableCurrency() {
		return accountPayableCurrency;
	}

	public void setAccountPayableCurrency(String accountPayableCurrency) {
		this.accountPayableCurrency = accountPayableCurrency;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getPaymentStatusCode() {
		return paymentStatusCode;
	}

	public void setPaymentStatusCode(String paymentStatusCode) {
		this.paymentStatusCode = paymentStatusCode;
	}

	public String getInvoiceStatusCode() {
		return invoiceStatusCode;
	}

	public void setInvoiceStatusCode(String invoiceStatusCode) {
		this.invoiceStatusCode = invoiceStatusCode;
	}

	public String getAccountPayableNo() {
		return accountPayableNo;
	}

	public void setAccountPayableNo(String accountPayableNo) {
		this.accountPayableNo = accountPayableNo;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public BigInteger getAccountPayableId() {
		return accountPayableId;
	}

	public void setAccountPayableId(BigInteger accountPayableId) {
		this.accountPayableId = accountPayableId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
}