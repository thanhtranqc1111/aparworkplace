package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * @author thoai.nh
 * created date Dec 5, 2017
 */
public class ReceiptOrderDTO {
	private BigInteger bankStatementId;
	private Calendar transactionDate;
	private Calendar valueDate;
	private String currencyCode;
	private BigDecimal credit;
	private String arStatus;
	private String arInvoiceNo;
	private String arStatusValue;
	private Calendar month;
	private String payerName;
	private String invoiceNo;
	private BigDecimal totalAmount;
	private BigDecimal totalAmountConverted;
	private BigDecimal remainAmount;
	private BigDecimal varianceAmount;
	private String originalCurrencyCode;
	public ReceiptOrderDTO(BigInteger bankStatementId, Calendar transactionDate, Calendar valueDate,
			String currencyCode, BigDecimal credit, String arStatus, String arStatusValue, String arInvoiceNo, Calendar month, String payerName,
			String invoiceNo, BigDecimal totalAmount, BigDecimal totalAmountConverted, BigDecimal remainAmount,
			BigDecimal varianceAmount, String originalCurrencyCode) {
		super();
		this.bankStatementId = bankStatementId;
		this.transactionDate = transactionDate;
		this.valueDate = valueDate;
		this.currencyCode = currencyCode;
		this.credit = credit;
		this.arStatus = arStatus;
		this.arStatusValue = arStatusValue;
		this.arInvoiceNo = arInvoiceNo;
		this.month = month;
		this.payerName = payerName;
		this.invoiceNo = invoiceNo;
		this.totalAmount = totalAmount;
		this.totalAmountConverted = totalAmountConverted;
		this.remainAmount = remainAmount;
		this.varianceAmount = varianceAmount;
		this.originalCurrencyCode = originalCurrencyCode;
	}
	public BigInteger getBankStatementId() {
		return bankStatementId;
	}
	public void setBankStatementId(BigInteger bankStatementId) {
		this.bankStatementId = bankStatementId;
	}
	public Calendar getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Calendar transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Calendar getValueDate() {
		return valueDate;
	}
	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public BigDecimal getCredit() {
		return credit;
	}
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}
	public String getArStatus() {
		return arStatus;
	}
	public void setArStatus(String arStatus) {
		this.arStatus = arStatus;
	}
	public String getArStatusValue() {
		return arStatusValue;
	}
	public void setArStatusValue(String arStatusValue) {
		this.arStatusValue = arStatusValue;
	}
	public String getArInvoiceNo() {
		return arInvoiceNo;
	}
	public void setArInvoiceNo(String arInvoiceNo) {
		this.arInvoiceNo = arInvoiceNo;
	}
	public Calendar getMonth() {
		return month;
	}
	public void setMonth(Calendar month) {
		this.month = month;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getTotalAmountConverted() {
		return totalAmountConverted;
	}
	public void setTotalAmountConverted(BigDecimal totalAmountConverted) {
		this.totalAmountConverted = totalAmountConverted;
	}
	public BigDecimal getRemainAmount() {
		return remainAmount;
	}
	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}
	public BigDecimal getVarianceAmount() {
		return varianceAmount;
	}
	public void setVarianceAmount(BigDecimal varianceAmount) {
		this.varianceAmount = varianceAmount;
	}
	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}
	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}
}
