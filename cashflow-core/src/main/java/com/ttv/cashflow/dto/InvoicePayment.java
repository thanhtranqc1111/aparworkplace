package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ttv.cashflow.domain.BankStatement;

public class InvoicePayment {

	private BigInteger paymentId;
	private String paymentOrderNo;
	private Calendar valueDate;
	private String bankRefNo;
	private BigDecimal includeGstOriginalAmount;
	private BigDecimal includeGstConvertedAmount;
	private String paymentStatus;
	private BigInteger apInvoiceId;
	private String apInvoiceNo;
	private Date issuedDate;
	private String payeeName;
	private String payeeCode;
	private String invoiceNo;
	private BigDecimal invIncludeGstOriginalAmount;
	private BigDecimal invIncludeGstConvertedAmount;
	private String invoiceStatus;
	private String payeeAccount;
	private String claimType;
	private Calendar month;
	private String bankName;
	private String bankAccount;
	private List<BankStatement> bankStatement;
	private String apInvoiceCurrency;
	private String paymentCurrency;
	//thoai.nh
	private String paymentStatusCode;
	private String invoiceStatusCode;
	
	private String claimTypeVal;
	//binh.lv

	
	public InvoicePayment() {
		super();
	}
	
	// non bank-statement
	public InvoicePayment(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankRefNo,
			BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount, String paymentStatus,
			BigInteger apInvoiceId, String apInvoiceNo, Date issuedDate, String payeeName, String payeeCode,
			String invoiceNo, BigDecimal invIncludeGstOriginalAmount, BigDecimal invIncludeGstConvertedAmount, String invoiceStatus, String payeeAccount, 
			String claimType, Calendar month,
			String bankName, String bankAccount, String apInvoiceCurrency, String paymentCurrency) {
		super();
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankRefNo = bankRefNo;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.paymentStatus = paymentStatus;
		this.apInvoiceId = apInvoiceId;
		this.apInvoiceNo = apInvoiceNo;
		this.issuedDate = issuedDate;
		this.payeeName = payeeName;
		this.payeeCode = payeeCode;
		this.invoiceNo = invoiceNo;
		this.invIncludeGstOriginalAmount = invIncludeGstOriginalAmount;
		this.invIncludeGstConvertedAmount = invIncludeGstConvertedAmount;
		this.invoiceStatus = invoiceStatus;
		this.payeeAccount = payeeAccount;
		this.claimType = claimType;
		this.month = month;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.apInvoiceCurrency = apInvoiceCurrency;
		this.paymentCurrency = paymentCurrency;
	}

	public InvoicePayment(BigInteger paymentId, String paymentOrderNo, Calendar valueDate, String bankRefNo,
			BigDecimal includeGstOriginalAmount, BigDecimal includeGstConvertedAmount, String paymentStatus,
			BigInteger apInvoiceId, String apInvoiceNo, Date issuedDate, String payeeName, String payeeCode,
			String invoiceNo, BigDecimal invIncludeGstOriginalAmount, String invoiceStatus, String payeeAccount, Calendar month,
			String bankName, String bankAccount, List<BankStatement> bankStatement, String apInvoiceCurrency,
			String paymentCurrency) {
		super();
		this.paymentId = paymentId;
		this.paymentOrderNo = paymentOrderNo;
		this.valueDate = valueDate;
		this.bankRefNo = bankRefNo;
		this.includeGstOriginalAmount = includeGstOriginalAmount;
		this.includeGstConvertedAmount = includeGstConvertedAmount;
		this.paymentStatus = paymentStatus;
		this.apInvoiceId = apInvoiceId;
		this.apInvoiceNo = apInvoiceNo;
		this.issuedDate = issuedDate;
		this.payeeName = payeeName;
		this.payeeCode = payeeCode;
		this.invoiceNo = invoiceNo;
		this.invIncludeGstOriginalAmount = invIncludeGstOriginalAmount;
		this.invoiceStatus = invoiceStatus;
		this.payeeAccount = payeeAccount;
		this.month = month;
		this.bankName = bankName;
		this.bankAccount = bankAccount;
		this.bankStatement = bankStatement;
		this.apInvoiceCurrency = apInvoiceCurrency;
		this.paymentCurrency = paymentCurrency;
	}

	public BigInteger getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(BigInteger paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentOrderNo() {
		return paymentOrderNo;
	}

	public void setPaymentOrderNo(String paymentOrderNo) {
		this.paymentOrderNo = paymentOrderNo;
	}

	public Calendar getValueDate() {
		return valueDate;
	}

	public void setValueDate(Calendar valueDate) {
		this.valueDate = valueDate;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public BigDecimal getIncludeGstOriginalAmount() {
		return includeGstOriginalAmount;
	}

	public void setIncludeGstOriginalAmount(BigDecimal includeGstOriginalAmount) {
		this.includeGstOriginalAmount = includeGstOriginalAmount;
	}

	public BigDecimal getIncludeGstConvertedAmount() {
		return includeGstConvertedAmount;
	}

	public void setIncludeGstConvertedAmount(BigDecimal includeGstConvertedAmount) {
		this.includeGstConvertedAmount = includeGstConvertedAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public BigInteger getApInvoiceId() {
		return apInvoiceId;
	}

	public void setApInvoiceId(BigInteger apInvoiceId) {
		this.apInvoiceId = apInvoiceId;
	}

	public String getApInvoiceNo() {
		return apInvoiceNo;
	}

	public void setApInvoiceNo(String apInvoiceNo) {
		this.apInvoiceNo = apInvoiceNo;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getPayeeCode() {
		return payeeCode;
	}

	public void setPayeeCode(String payeeCode) {
		this.payeeCode = payeeCode;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BigDecimal getInvIncludeGstOriginalAmount() {
		return invIncludeGstOriginalAmount;
	}

	public void setInvIncludeGstOriginalAmount(BigDecimal invIncludeGstOriginalAmount) {
		this.invIncludeGstOriginalAmount = invIncludeGstOriginalAmount;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getPayeeAccount() {
		return payeeAccount;
	}

	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}
	
	public String getClaimType() {
        return claimType;
    }
	
	public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

	public Calendar getMonth() {
		return month;
	}

	public void setMonth(Calendar month) {
		this.month = month;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public List<BankStatement> getBankStatement() {
		return bankStatement;
	}

	public void setBankStatement(List<BankStatement> bankStatement) {
		this.bankStatement = bankStatement;
	}

	public String getApInvoiceCurrency() {
		return apInvoiceCurrency;
	}

	public void setApInvoiceCurrency(String apInvoiceCurrency) {
		this.apInvoiceCurrency = apInvoiceCurrency;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getPaymentStatusCode() {
		return paymentStatusCode;
	}

	public void setPaymentStatusCode(String paymentStatusCode) {
		this.paymentStatusCode = paymentStatusCode;
	}

	public String getInvoiceStatusCode() {
		return invoiceStatusCode;
	}

	public void setInvoiceStatusCode(String invoiceStatusCode) {
		this.invoiceStatusCode = invoiceStatusCode;
	}
	
	public String getClaimTypeVal() {
        return claimTypeVal;
    }
	
	public void setClaimTypeVal(String claimTypeVal) {
        this.claimTypeVal = claimTypeVal;
    }

	public BigDecimal getInvIncludeGstConvertedAmount() {
		return invIncludeGstConvertedAmount;
	}

	public void setInvIncludeGstConvertedAmount(BigDecimal invIncludeGstConvertedAmount) {
		this.invIncludeGstConvertedAmount = invIncludeGstConvertedAmount;
	}
}
