package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author thoai.nh
 * created date May 30, 2018
 */
public class DashboardChartOneDTO implements Comparable<DashboardChartOneDTO>{
	private List<BigDecimal> data;
	private String stack;
	private String name;
	private int index;
	public DashboardChartOneDTO(List<BigDecimal> data, String name, String stack, int index) {
		super();
		this.data = data;
		this.stack = stack;
		this.name = name;
		this.index = index;
	}
	public List<BigDecimal> getData() {
		return data;
	}
	public void setData(List<BigDecimal> data) {
		this.data = data;
	}
	public String getStack() {
		return stack;
	}
	public void setStack(String stack) {
		this.stack = stack;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public int compareTo(DashboardChartOneDTO o) {
		return this.index - o.index;
	}
	
}
