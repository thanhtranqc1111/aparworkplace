package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public class PaymentEmployeeDTO {
	private String employeeCode;
	private String employeeName;
	private BigDecimal employeeSalary;
	private Map<String, BigDecimal> mapProjectAssignedSalary = new LinkedHashMap<>();

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public void setEmployeeSalary(BigDecimal employeeSalary) {
		this.employeeSalary = employeeSalary;
	}
	
	public BigDecimal getEmployeeSalary() {
		return employeeSalary;
	}

	public Map<String, BigDecimal> getMapProjectAssignedSalary() {
		return mapProjectAssignedSalary;
	}

	public void setMapProjectAssignedSalary(Map<String, BigDecimal> mapProjectAssignedSalary) {
		this.mapProjectAssignedSalary = mapProjectAssignedSalary;
	}

}
