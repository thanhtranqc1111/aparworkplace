package com.ttv.cashflow.dto;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * @author thoai.nh 
 * created date Apr 27, 2018
 * this DTO using to load project allocation info to main screen
 */
public class ProjectAllocationDTO implements Comparable<ProjectAllocationDTO>{
	private BigInteger employeeId;
	private String employeeCode;
	private String employeeName;
	private String projectName;	
	private String projectCode;
	private String team;
	private Map<String, ProjectAllocationPrecentageDTO> mapMonthPercent;
	public ProjectAllocationDTO() {
		mapMonthPercent = new HashMap<>();
	}
	public ProjectAllocationDTO(BigInteger employeeId, String employeeCode, String employeeName, String projectName,
			String projectCode, String team) {
		super();
		this.employeeId = employeeId;
		this.employeeCode = employeeCode;
		this.employeeName = employeeName;
		this.projectName = projectName;
		this.projectCode = projectCode;
		this.team = team;
		mapMonthPercent = new HashMap<>();
	}
	public BigInteger getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(BigInteger employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Map<String, ProjectAllocationPrecentageDTO> getMapMonthPercent() {
		return mapMonthPercent;
	}
	public void setMapMonthPercent(Map<String, ProjectAllocationPrecentageDTO> mapMonthPercent) {
		this.mapMonthPercent = mapMonthPercent;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employeeCode == null) ? 0 : employeeCode.hashCode());
		result = prime * result + ((employeeId == null) ? 0 : employeeId.hashCode());
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result + ((projectCode == null) ? 0 : projectCode.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		result = prime * result + ((team == null) ? 0 : team.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectAllocationDTO other = (ProjectAllocationDTO) obj;
		if (employeeCode == null) {
			if (other.employeeCode != null)
				return false;
		} else if (!employeeCode.equals(other.employeeCode))
			return false;
		if (employeeId == null) {
			if (other.employeeId != null)
				return false;
		} else if (!employeeId.equals(other.employeeId))
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (projectCode == null) {
			if (other.projectCode != null)
				return false;
		} else if (!projectCode.equals(other.projectCode))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		if (team == null) {
			if (other.team != null)
				return false;
		} else if (!team.equals(other.team))
			return false;
		return true;
	}
	@Override
	public int compareTo(ProjectAllocationDTO o) {
		if(!this.equals(o))
		{
			int rs = this.getEmployeeId().compareTo(o.getEmployeeId());
			return rs == 0? -1 : rs;
		}
		else {
			return 0;
		}
	}
}
