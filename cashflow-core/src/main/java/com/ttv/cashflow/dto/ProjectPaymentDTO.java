package com.ttv.cashflow.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectPaymentDTO {
	private String month;
	private Map<String, String> mapProjectCodeAndProjectName = new LinkedHashMap<>();
	List<PaymentEmployeeDTO> listPaymentEmployeeDTO = new ArrayList<>();

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Map<String, String> getMapProjectCodeAndProjectName() {
		return mapProjectCodeAndProjectName;
	}

	public void setMapProjectCodeAndProjectName(Map<String, String> mapProjectCodeAndProjectName) {
		this.mapProjectCodeAndProjectName = mapProjectCodeAndProjectName;
	}

	public List<PaymentEmployeeDTO> getListPaymentEmployeeDTO() {
		return listPaymentEmployeeDTO;
	}

	public void setListPaymentEmployeeDTO(List<PaymentEmployeeDTO> listPaymentEmployeeDTO) {
		this.listPaymentEmployeeDTO = listPaymentEmployeeDTO;
	}
	
	public BigDecimal getTotalSalaryAllEmplpoyee(){
	    BigDecimal ret = BigDecimal.valueOf(0d);
	    
        for (PaymentEmployeeDTO paymentEmployeeDTO : listPaymentEmployeeDTO) {
            ret = ret.add(paymentEmployeeDTO.getEmployeeSalary());
        }
	    
	    return ret;
	}
	
	public BigDecimal getTotalAmountOfProject(String projectCode){
        BigDecimal ret = BigDecimal.valueOf(0d);
        
        for (PaymentEmployeeDTO paymentEmployeeDTO : listPaymentEmployeeDTO) {
            
            for (Map.Entry<String, BigDecimal> projectAssignedSalary : paymentEmployeeDTO.getMapProjectAssignedSalary().entrySet()) {
                if (projectCode.equals(projectAssignedSalary.getKey())) {
                    ret = ret.add(projectAssignedSalary.getValue());
                }
            }
        }
        
        return ret;
    }

}
