package com.ttv.cashflow.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.impl.PaymentOrderDAOImpl;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.service.impl.PaymentOrderServiceImpl;
import com.ttv.cashflow.util.Constant;

@RunWith(MockitoJUnitRunner.class)
public class PaymentOrderCoreTest {
	
	@Mock
	private PaymentOrderDAO paymentOrderDAO;
	
	@InjectMocks
	private PaymentOrderDAOImpl paymentOrderDAOImpl;

	@InjectMocks
	private PaymentOrderServiceImpl paymentOrderServiceImpl;
	
	@Mock
	private PaymentOrder paymentOrder;
	
	@Captor
    private ArgumentCaptor<PaymentOrder> paymentArgument;
	
	@Before
    public void setupMock() {
		MockitoAnnotations.initMocks(this);
    }

	@Test
	public void testSavePaymentOrder_createNew() {
		Mockito.when(paymentOrderDAO.store(Mockito.any(PaymentOrder.class))).thenReturn(new PaymentOrder());
		assertThat(paymentOrderServiceImpl.savePaymentOrder(new PaymentOrder()), is(notNullValue()));
	}
	
	@Test
	public void testSavePaymentOrder_update() {
		PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	payment.setBankName("ACB");
    	
    	PaymentOrder payment2 = new PaymentOrder();
    	payment2.setPaymentOrderNo("PAY999");
    	payment2.setId(BigInteger.ONE);
    	payment2.setBankName("HSBC");
    	
    	// find result payment order have data 
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPrimaryKey(BigInteger.ONE)).thenReturn(payment);
    	Mockito.when(paymentOrderDAO.store(payment)).thenReturn(payment2);
    	
    	PaymentOrder pay = paymentOrderServiceImpl.savePaymentOrder(payment2);
		assertThat(pay, is(payment2));
	}
	
	@Test
	public void testSavePaymentOrder_updatewithinStatus() {
		PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	payment.setBankName("ACB");
    	payment.setStatus(Constant.PAY_PROCESSING);
    	
    	PaymentOrder payment2 = new PaymentOrder();
    	payment2.setPaymentOrderNo("PAY999");
    	payment2.setId(BigInteger.ONE);
    	payment2.setBankName("HSBC");
    	payment2.setStatus(Constant.PAY_PROCESSED);
    	
    	// find result payment order have data 
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPrimaryKey(BigInteger.ONE)).thenReturn(payment);
    	Mockito.when(paymentOrderDAO.store(payment)).thenReturn(payment2);
    	
    	PaymentOrder pay = paymentOrderServiceImpl.savePaymentOrder(payment2);

    	
    	assertThat(pay, is(payment2));
//		assertThat(pay.getStatus()).isEqualTo(payment2.getStatus());
		assertThat(pay.getStatus(), is(Constant.PAY_PROCESSED));
	}
	
}
