package com.ttv.cashflow.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.ttv.cashflow.dao.PaymentOrderDAO;
import com.ttv.cashflow.dao.impl.PaymentOrderDAOImpl;
import com.ttv.cashflow.domain.PaymentOrder;
import com.ttv.cashflow.service.impl.PaymentOrderServiceImpl;
import com.ttv.cashflow.util.Constant;

@RunWith(MockitoJUnitRunner.class)
public class PaymentOrderTest {
	
	@Mock
	private PaymentOrderDAO paymentOrderDAO;
	
	@InjectMocks
	private PaymentOrderDAOImpl paymentOrderDAOImpl;

	@InjectMocks
	private PaymentOrderServiceImpl paymentOrderServiceImpl;
	
	@Mock
	private PaymentOrder paymentOrder;
	
	@Captor
    private ArgumentCaptor<PaymentOrder> paymentArgument;
	
	@Before
    public void setupMock() {
		MockitoAnnotations.initMocks(this);
    }

	@Test
	public void testSavePaymentOrder_createNew() {
		Mockito.when(paymentOrderDAO.store(Mockito.any(PaymentOrder.class))).thenReturn(new PaymentOrder());
		assertThat(paymentOrderServiceImpl.savePaymentOrder(new PaymentOrder()), is(notNullValue()));
	}
	
	@Test
	public void testSavePaymentOrder_update() {
		PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	payment.setBankName("ACB");
    	
    	PaymentOrder payment2 = new PaymentOrder();
    	payment2.setPaymentOrderNo("PAY999");
    	payment2.setId(BigInteger.ONE);
    	payment2.setBankName("HSBC");
    	
    	// find result payment order have data 
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPrimaryKey(BigInteger.ONE)).thenReturn(payment);
    	Mockito.when(paymentOrderDAO.store(payment)).thenReturn(payment2);
    	
    	PaymentOrder pay = paymentOrderServiceImpl.savePaymentOrder(payment2);
		assertThat(pay, is(payment2));
	}
	
	@Test
	public void testSavePaymentOrder_updatewithinStatus() {
		PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	payment.setBankName("ACB");
    	payment.setStatus(Constant.PAY_PROCESSING);
    	
    	PaymentOrder payment2 = new PaymentOrder();
    	payment2.setPaymentOrderNo("PAY999");
    	payment2.setId(BigInteger.ONE);
    	payment2.setBankName("HSBC");
    	payment2.setStatus(Constant.PAY_PROCESSED);
    	
    	// find result payment order have data 
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPrimaryKey(BigInteger.ONE)).thenReturn(payment);
    	Mockito.when(paymentOrderDAO.store(payment)).thenReturn(payment2);
    	
    	PaymentOrder pay = paymentOrderServiceImpl.savePaymentOrder(payment2);

    	
    	assertThat(pay, is(payment2));
//		assertThat(pay.getStatus()).isEqualTo(payment2.getStatus());
		assertThat(pay.getStatus(), is(Constant.PAY_PROCESSED));
	}
	
    //Throwing an exception from the mocked method
    @SuppressWarnings("unchecked")
	@Test(expected = RuntimeException.class)
    public void testSavePaymentOrde_throwsException() {
    	Mockito.when(paymentOrderDAO.store(Mockito.any(PaymentOrder.class))).thenThrow(RuntimeException.class);
    	paymentOrderServiceImpl.savePaymentOrder(new PaymentOrder());
    }
    
    
    @Test
    public void testSavePaymentOrder_returnsNewPaymentOrderWithId() {
    	Mockito.when(paymentOrderDAO.store(Mockito.any(PaymentOrder.class))).thenAnswer(new Answer<PaymentOrder>() {
			@Override
			public PaymentOrder answer(InvocationOnMock invocation) throws Throwable {
				 Object[] arguments = invocation.getArguments();
				 if (arguments != null && arguments.length > 0 && arguments[0] != null) {
					 PaymentOrder payment = (PaymentOrder) arguments[0];
					 payment.setId(BigInteger.ONE);
					 payment.setPaymentOrderNo("PAY999");
					 return payment;
                }
				return null;
			}
		});
    	
    	PaymentOrder payment = new PaymentOrder();
    	assertThat(paymentOrderServiceImpl.savePaymentOrder(payment), is(notNullValue()));
    }
    
    @Test
    public void testFindPaymentOrderByPrimaryKey(){
    	Mockito.when(paymentOrderDAO.store(Mockito.any(PaymentOrder.class))).thenAnswer(new Answer<PaymentOrder>() {
			@Override
			public PaymentOrder answer(InvocationOnMock invocation) throws Throwable {
				 Object[] arguments = invocation.getArguments();
				 if (arguments != null && arguments.length > 0 && arguments[0] != null) {
					 PaymentOrder payment = (PaymentOrder) arguments[0];
					 payment.setId(BigInteger.ONE);
					 payment.setPaymentOrderNo("PAY999");
					 return payment;
                }
				return null;
			}
		});
    	
    	PaymentOrder payment = new PaymentOrder(); 
    	paymentOrderServiceImpl.savePaymentOrder(payment);
    	
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPrimaryKey(BigInteger.ONE)).thenReturn(payment);
    	PaymentOrder foundPayment = paymentOrderServiceImpl.findPaymentOrderByPrimaryKey(payment.getId());
    	assertThat(foundPayment).isEqualTo(payment);
    }
    
    @Test
    public void testFindPaymentOrderByPaymentNo() {
    	PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	paymentOrderServiceImpl.savePaymentOrder(payment);
    	Mockito.when(paymentOrderDAO.findPaymentOrderByPaymentOrderNoOnly(Mockito.anyString())).thenReturn(payment);
    	
    	PaymentOrder paymentOrder = paymentOrderServiceImpl.findPaymentOrderByPaymentOrderNoOnly("PAY999");
    	assertThat(paymentOrder.getPaymentOrderNo()).isEqualTo("PAY999");
    }
    
    @Test
    public void testSearchPaymentOrder(){
    	PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	
    	PaymentOrder payment2 = new PaymentOrder();
    	payment2.setPaymentOrderNo("PAY1000");
    	payment2.setId(BigInteger.valueOf(2));
    	
    	Set<PaymentOrder> resultSet = new HashSet<PaymentOrder>();
    	resultSet.add(payment);
    	resultSet.add(payment2);
    	
    	Mockito.when(paymentOrderDAO.findAllPaymentOrders()).thenReturn(resultSet);
    	
    	Set<PaymentOrder> list = paymentOrderServiceImpl.loadPaymentOrders();
    	assertThat(list.size()).isEqualTo(2);
    	
    	//verify that the delete method has never been  invoked
    	Mockito.verify(paymentOrderDAO, Mockito.never()).remove(Mockito.any(PaymentOrder.class));
    	
    	//verify that the findAllPaymentOrders method has most 1 time been invoked
    	Mockito.verify(paymentOrderDAO, Mockito.atMost(1)).findAllPaymentOrders();
    	
    	//verify that the findAllPaymentOrders method has least 1 time been invoked
    	Mockito.verify(paymentOrderDAO, Mockito.atLeast(1)).findAllPaymentOrders();
    	
    	//verify that the findAllPaymentOrders method has 1 time been invoked
    	Mockito.verify(paymentOrderDAO, Mockito.times(1)).findAllPaymentOrders();
    	
    	//verify that has only the findAllPaymentOrders method
    	Mockito.verify(paymentOrderDAO, Mockito.only()).findAllPaymentOrders();
    	
    	Mockito.verify(paymentOrderDAO, Mockito.atLeastOnce()).findAllPaymentOrders();
    	
    }
    
    @Test
    public void testSavePaymentOrder_Captor() {
    	PaymentOrder payment = new PaymentOrder();
    	payment.setPaymentOrderNo("PAY999");
    	payment.setId(BigInteger.ONE);
    	
    	paymentOrderServiceImpl.savePaymentOrder(payment);
    	Mockito.verify(paymentOrderDAO).store(paymentArgument.capture());
    	assertThat(paymentArgument.getValue().getPaymentOrderNo()).isEqualTo("PAY999");
    }
    
    @Test
    public void testSearchPaymentOrder_Captor(){
        	PaymentOrder payment = new PaymentOrder();
        	payment.setPaymentOrderNo("PAY999");
        	payment.setId(BigInteger.ONE);
        	
        	PaymentOrder payment2 = new PaymentOrder();
        	payment2.setPaymentOrderNo("PAY1000");
        	payment2.setId(BigInteger.valueOf(2));
        	
        	paymentOrderServiceImpl.savePaymentOrder(payment);
        	paymentOrderServiceImpl.savePaymentOrder(payment2);
        	Mockito.verify(paymentOrderDAO, Mockito.times(2)).store(paymentArgument.capture());
        	
        	List<PaymentOrder> capturedPayment = paymentArgument.getAllValues();
        	assertThat(paymentArgument.getAllValues().size()).isEqualTo(2);
        	assertThat(capturedPayment.get(0).getPaymentOrderNo()).isEqualTo("PAY999");
        	assertThat(capturedPayment.get(1).getPaymentOrderNo()).isEqualTo("PAY1000");
    }
   
}
